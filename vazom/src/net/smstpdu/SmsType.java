package net.smstpdu;

import java.util.HashMap;


public enum SmsType {
	SINGLE(1),
	CONCATENATED(2),
	UNKNOWN(3);
	
	private int id;
	private static final HashMap<Integer, SmsType> lookup = new HashMap<Integer, SmsType>();
	static{
		for(SmsType td : SmsType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static SmsType get(int id){ return lookup.get(id); }
	private SmsType(int _id){ id = _id; }
	
}
