package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Sms_signal extends OptionalParameterBase {

	public Sms_signal(){
		type = OptionalParameterType.SMS_SIGNAL;
	}
	int value;
	public void init(byte[] data) {
		value = ((data[0] << 8) & 0xff) + (data[1] & 0xff);

	}

}
