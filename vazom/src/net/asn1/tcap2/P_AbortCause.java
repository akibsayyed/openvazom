package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class P_AbortCause extends INTEGER{

	public static final int _unrecognizedMessageType = 0;
	public static final int _unrecognizedTransactionID = 1;
	public static final int _badlyFormattedTransactionPortion = 2;
	public static final int _incorrectTransactionPortion = 3;
	public static final int _resourceLimitation = 4;
	public P_AbortCause(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
		asn_class = ASNTagClass.APPLICATION;
	}
}
