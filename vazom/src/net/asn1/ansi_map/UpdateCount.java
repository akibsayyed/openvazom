package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class UpdateCount extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _update_COUNT =  1 ;
	public UpdateCount(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
