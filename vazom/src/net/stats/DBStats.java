package net.stats;

public class DBStats {
	public long DB_CONNECTION_LOST;
	public long DB_QUEUE_FULL_COUNT;
	public long DB_SMS_BATCH_COUNT;
	public long DB_SRI_BATCH_COUNT;
	public long DB_ISUP_BATCH_COUNT;
	
	public long LAST_SMS_BATCH_ELAPSED;
	public long LAST_SRI_BATCH_ELAPSED;
	public long LAST_ISUP_BATCH_ELAPSED;
	
	public long MAX_SMS_BATCH_ELAPSED;
	public long MAX_SRI_BATCH_ELAPSED;
	public long MAX_ISUP_BATCH_ELAPSED;
	
	public void isup_setMaxTS(long ts){
		if(ts > MAX_ISUP_BATCH_ELAPSED) MAX_ISUP_BATCH_ELAPSED = ts;
	}
	
	public void sms_setMaxTS(long ts){
		if(ts > MAX_SMS_BATCH_ELAPSED) MAX_SMS_BATCH_ELAPSED = ts;
	}
	public void sri_setMaxTS(long ts){
		if(ts > MAX_SRI_BATCH_ELAPSED) MAX_SRI_BATCH_ELAPSED = ts;
	}
	
	

}
