package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AmmDescriptorSequenceOf extends SEQUENCE{

	public AmmDescriptor new_child(){
		return new AmmDescriptor();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public AmmDescriptor getChild(int index){
		return (AmmDescriptor)of_children.get(index);
	}
	public AmmDescriptorSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
