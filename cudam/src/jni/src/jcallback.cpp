#include <iostream>
#include <jni.h>
#include <jcallback.h>
using namespace std;

void java_callback_method(void *args){
	jcallback_args *tc = (jcallback_args *)args;
	JNIEnv *env = NULL;
	jbyteArray res_arr = NULL;
	jvalue callback_args[1];

	// attach current pthread
	tc->vm->AttachCurrentThread((void **)&env, NULL);
	// get jnienv pointer
	int err = tc->vm->GetEnv((void **)&env, JNI_VERSION_1_6);
	jmethodID mid = NULL;
	jclass cls = NULL;
	//cout << "C++ CALLBACK11!!" << endl;
	cls = env->FindClass("net/gpu/cuda/CudaCallback");
	if(cls != NULL){
		mid = env->GetMethodID(cls, "execute", "([B)V");
		if(mid != NULL){
			// LD callback
			if(tc->cb_type == CB_LD){
				// create java int[] array
				res_arr = env->NewByteArray(tc->ldp->result_size);
				// copy data
				env->SetByteArrayRegion(res_arr, 0, tc->ldp->result_size, (jbyte *)tc->ldp->result);
			// MD5 callback
			}else if(tc->cb_type == CB_MD5){
				// create java int[] array
				res_arr = env->NewByteArray(tc->md5p->result_size);
				// copy data
				env->SetByteArrayRegion(res_arr, 0, tc->md5p->result_size, (jbyte *)tc->md5p->result);

			}
			//cout << "execute found" << endl;
			//env->CallVoidMethod(tc->obj, mid);
			callback_args[0].l = res_arr;
			env->CallVoidMethodA(tc->obj, mid, callback_args);
			// delete global reference for callback method
			env->DeleteGlobalRef(tc->obj);
			// deallocate memory for local jintarray
            		callback_args[0].l = NULL;
            		env->DeleteLocalRef(res_arr);
			// free jcallback args
			delete tc;
		}
	}

}
