package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UserInformation extends SEQUENCE{

	public void initElements(){
	}
	public UserInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 29;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
