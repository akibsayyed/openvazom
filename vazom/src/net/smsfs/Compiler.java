package net.smsfs;

import java.io.ByteArrayInputStream;
import java.io.FileNotFoundException;

import net.utils.Utils;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;

public class Compiler {

	public static boolean compile(String fname){
		boolean res = false;
		try{
	    	SmsFilterScriptLexer lex = new SmsFilterScriptLexer(new ANTLRFileStream(fname, "UTF8"));
	        CommonTokenStream tokens = new CommonTokenStream(lex);
	        SmsFilterScriptParser g = new SmsFilterScriptParser(tokens);
	        g.input();
	        res = g.getNumberOfSyntaxErrors() == 0;
		}catch(FileNotFoundException e2){
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}
	public static boolean compile_str(String data){
		boolean res = false;
		try{
	    	SmsFilterScriptLexer lex = new SmsFilterScriptLexer(new ANTLRInputStream(new ByteArrayInputStream(data.getBytes()), "UTF8"));
	        CommonTokenStream tokens = new CommonTokenStream(lex);
	        SmsFilterScriptParser g = new SmsFilterScriptParser(tokens);
	        g.input();
	        res = g.getNumberOfSyntaxErrors() == 0;
		}catch(FileNotFoundException e2){
			
		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return res;
	}
	public static void main(String[] args) {
		if(args.length >0){
			byte[] fdata = Utils.file_get_contents(args[0]);
			if(fdata != null){
				if(Utils.is_ascii(fdata)){
					if(!compile(args[0])) System.out.println("Invalid SMSFilterScript file!");
					else System.out.println("SMSFilterScript file is valid, ready for activation!");
					
				}else System.out.println("Invalid SMSFilterScript file, unsupported character(s) detected!");
			}else System.out.println("Invalid SMSFilterScript file!");
		}
		else System.out.println("Invalid filename!");
	}	
}
