package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ContextRequest extends SEQUENCE{

	public INTEGER get_priority(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_priority(){
		return new INTEGER();
	}

	public BOOLEAN get_emergency(){
		return (BOOLEAN)elements.get(1).data;
	}
	public BOOLEAN new_emergency(){
		return new BOOLEAN();
	}

	public TopologyRequestSequenceOf get_topologyReq(){
		return (TopologyRequestSequenceOf)elements.get(2).data;
	}
	public TopologyRequestSequenceOf new_topologyReq(){
		return new TopologyRequestSequenceOf();
	}

	public BOOLEAN get_iepscallind(){
		return (BOOLEAN)elements.get(3).data;
	}
	public BOOLEAN new_iepscallind(){
		return new BOOLEAN();
	}

	public PropertyParmSequenceOf get_contextProp(){
		return (PropertyParmSequenceOf)elements.get(4).data;
	}
	public PropertyParmSequenceOf new_contextProp(){
		return new PropertyParmSequenceOf();
	}

	public ContextIDinListSequenceOf get_contextList(){
		return (ContextIDinListSequenceOf)elements.get(5).data;
	}
	public ContextIDinListSequenceOf new_contextList(){
		return new ContextIDinListSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_priority(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_emergency(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_topologyReq(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_iepscallind(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_contextProp(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_contextList(); }});
	}
	public ContextRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
