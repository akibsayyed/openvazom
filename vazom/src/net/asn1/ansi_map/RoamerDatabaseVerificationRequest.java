package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RoamerDatabaseVerificationRequest extends SET{

	public InvokingNEType get_invokingNEType(){
		return (InvokingNEType)elements.get(0).data;
	}
	public InvokingNEType new_invokingNEType(){
		return new InvokingNEType();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(1).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSID get_msid(){
		return (MSID)elements.get(3).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public Range get_range(){
		return (Range)elements.get(4).data;
	}
	public Range new_range(){
		return new Range();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(353, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_invokingNEType(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(352, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_range(); }});
	}
	public RoamerDatabaseVerificationRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
