package net.asn1.compiler;

import java.lang.annotation.Retention;
import java.lang.annotation.RetentionPolicy;

@Retention(RetentionPolicy.RUNTIME)
public @interface TagDescriptor {
	public int tagValue();
	public ASNTagClass tagClass();
	public boolean optional() default false;
	public boolean extAddition() default false;
	
}
