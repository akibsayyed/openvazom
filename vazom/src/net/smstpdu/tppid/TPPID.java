package net.smstpdu.tppid;


public class TPPID {
	public TPPID_Format format;

	// depending on format, only one device type is used
	// interworking is used only in DEFAULT1 scheme
	public TPPID_Interworking interworking;
	public TelematicDeviceType1 device_type1;
	public TelematicDeviceType2 device_type2;
	
	public void init(byte data){
		format = TPPID_Format.get(data & 0xC0);
		// bit 5 is used for interworking only in DEFAULT1 scheme
		switch(format){
			case DEFAULT1:
				interworking = TPPID_Interworking.get(data & 0x20);
				if(interworking == TPPID_Interworking.TELEMATIC_INTERWORKING) device_type1 = TelematicDeviceType1.get(data & 0x1F);
				break;
			case DEFAULT2:
				device_type2 = TelematicDeviceType2.get(data & 0x3F);
				break;
		
		}
		
		
	}
	public TPPID(){
		
		
	}
}
