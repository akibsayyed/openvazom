package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TimeOfDay extends INTEGER{

	public TimeOfDay(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
