package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SendAuthenticationInfoResOldSeq extends SEQUENCE{

	public RAND get_rand(){
		return (RAND)elements.get(0).data;
	}
	public RAND new_rand(){
		return new RAND();
	}

	public SRES get_sres(){
		return (SRES)elements.get(1).data;
	}
	public SRES new_sres(){
		return new SRES();
	}

	public Kc get_kc(){
		return (Kc)elements.get(2).data;
	}
	public Kc new_kc(){
		return new Kc();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_rand(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sres(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_kc(); }});
	}
	public SendAuthenticationInfoResOldSeq(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
