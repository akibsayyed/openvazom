package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.SubsequentNumber;

public class SubsequentAddress extends MessageBase {
	public SubsequentNumber subsequent_number;
	
	public SubsequentAddress(){
		super();
		type = MessageType.SUBSEQUENT_ADDRESS;
		pointers = new Pointer[2];
		
	}

	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			pointers[1] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			subsequent_number = new SubsequentNumber();
			subsequent_number.init(getPointerData(data, 0));
			
			processOptional(data, 1);
		
		}
	}

}
