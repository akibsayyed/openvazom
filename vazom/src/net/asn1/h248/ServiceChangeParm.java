package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceChangeParm extends SEQUENCE{

	public ServiceChangeMethod get_serviceChangeMethod(){
		return (ServiceChangeMethod)elements.get(0).data;
	}
	public ServiceChangeMethod new_serviceChangeMethod(){
		return new ServiceChangeMethod();
	}

	public ServiceChangeAddressWrapper get_serviceChangeAddress(){
		return (ServiceChangeAddressWrapper)elements.get(1).data;
	}
	public ServiceChangeAddressWrapper new_serviceChangeAddress(){
		return new ServiceChangeAddressWrapper();
	}

	public INTEGER get_serviceChangeVersion(){
		return (INTEGER)elements.get(2).data;
	}
	public INTEGER new_serviceChangeVersion(){
		return new INTEGER();
	}

	public ServiceChangeProfile get_serviceChangeProfile(){
		return (ServiceChangeProfile)elements.get(3).data;
	}
	public ServiceChangeProfile new_serviceChangeProfile(){
		return new ServiceChangeProfile();
	}

	public SCreasonValue get_serviceChangeReason(){
		return (SCreasonValue)elements.get(4).data;
	}
	public SCreasonValue new_serviceChangeReason(){
		return new SCreasonValue();
	}

	public INTEGER get_serviceChangeDelay(){
		return (INTEGER)elements.get(5).data;
	}
	public INTEGER new_serviceChangeDelay(){
		return new INTEGER();
	}

	public MIdWrapper get_serviceChangeMgcId(){
		return (MIdWrapper)elements.get(6).data;
	}
	public MIdWrapper new_serviceChangeMgcId(){
		return new MIdWrapper();
	}

	public TimeNotation get_timeStamp(){
		return (TimeNotation)elements.get(7).data;
	}
	public TimeNotation new_timeStamp(){
		return new TimeNotation();
	}

	public NonStandardData get_nonStandardData(){
		return (NonStandardData)elements.get(8).data;
	}
	public NonStandardData new_nonStandardData(){
		return new NonStandardData();
	}

	public AuditDescriptor get_serviceChangeInfo(){
		return (AuditDescriptor)elements.get(9).data;
	}
	public AuditDescriptor new_serviceChangeInfo(){
		return new AuditDescriptor();
	}

	public NULL get_serviceChangeIncompleteFlag(){
		return (NULL)elements.get(10).data;
	}
	public NULL new_serviceChangeIncompleteFlag(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceChangeMethod(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeAddress(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeVersion(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeProfile(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceChangeReason(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeDelay(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceChangeMgcId(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_timeStamp(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nonStandardData(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_serviceChangeInfo(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_serviceChangeIncompleteFlag(); }});
	}
	public ServiceChangeParm(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
