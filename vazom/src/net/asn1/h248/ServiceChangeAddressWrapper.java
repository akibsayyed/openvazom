package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceChangeAddressWrapper extends SEQUENCE{

	public ServiceChangeAddress get_serviceChangeAddress(){
		return (ServiceChangeAddress)elements.get(0).data;
	}
	public ServiceChangeAddress new_serviceChangeAddress(){
		return new ServiceChangeAddress();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_serviceChangeAddress(); }});
	}
	public ServiceChangeAddressWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
