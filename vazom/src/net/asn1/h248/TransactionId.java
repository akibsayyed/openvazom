package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TransactionId extends INTEGER{

	public TransactionId(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
