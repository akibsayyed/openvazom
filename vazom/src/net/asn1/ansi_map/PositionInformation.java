package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PositionInformation extends SET{

	public GENERALIZEDTIME get_generalizedTime(){
		return (GENERALIZEDTIME)elements.get(0).data;
	}
	public GENERALIZEDTIME new_generalizedTime(){
		return new GENERALIZEDTIME();
	}

	public GeographicPosition get_geographicPosition(){
		return (GeographicPosition)elements.get(1).data;
	}
	public GeographicPosition new_geographicPosition(){
		return new GeographicPosition();
	}

	public PositionSource get_positionSource(){
		return (PositionSource)elements.get(2).data;
	}
	public PositionSource new_positionSource(){
		return new PositionSource();
	}

	public Horizontal_Velocity get_horizontal_Velocity(){
		return (Horizontal_Velocity)elements.get(3).data;
	}
	public Horizontal_Velocity new_horizontal_Velocity(){
		return new Horizontal_Velocity();
	}

	public Vertical_Velocity get_vertical_Velocity(){
		return (Vertical_Velocity)elements.get(4).data;
	}
	public Vertical_Velocity new_vertical_Velocity(){
		return new Vertical_Velocity();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(331, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_generalizedTime(); }});
		elements.add(new ElementDescriptor(333, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_geographicPosition(); }});
		elements.add(new ElementDescriptor(339, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_positionSource(); }});
		elements.add(new ElementDescriptor(379, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_horizontal_Velocity(); }});
		elements.add(new ElementDescriptor(380, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_vertical_Velocity(); }});
	}
	public PositionInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
