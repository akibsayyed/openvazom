package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class OrigTransactionID extends OCTET_STRING{

	public OrigTransactionID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 8;
		asn_class = ASNTagClass.APPLICATION;
	}
}
