package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;
import net.utils.Utils;

public class Additional_status_info_text extends OptionalParameterBase {

	public Additional_status_info_text(){
		type = OptionalParameterType.ADDITIONAL_STATUS_INFO_TEXT;
	}
	public String value;
	public void init(byte[] data) {
			value = Utils.Cstr_decode(data, 0);
	}

}
