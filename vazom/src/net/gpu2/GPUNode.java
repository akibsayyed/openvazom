package net.gpu2;


import java.util.concurrent.ConcurrentHashMap;

import net.config.ConfigManagerV2;
import net.config.GPUNConfigData;
import net.gpu.GridDescriptor;
import net.gpu.cli.CLIService;
import net.gpu.cuda.CudaCallback;
import net.gpu.cuda.CudaManager;
import net.gpu.cuda.KernelStats;
import net.logging.LoggingManager;
import net.sctp.SSctp;
import net.security.SecurityManager;
import net.stats.StatsManager;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class GPUNode {
	public class Listener_r implements Runnable{
		private ClientConnection con;
		private int client_sctp_id;
		public void run() {
			while(!stopping){
				try{
					client_sctp_id = SSctp.clientAccept(sctp_id);
					if(client_sctp_id > -1){
						con = new ClientConnection(client_sctp_id, connection_lst);
						connection_lst.put(client_sctp_id, con);
						logger.info("New client connection, sctp id = [ " + client_sctp_id + " ]!");
						
					}
				}catch(Exception e){
					logger.error("Error while accepting new connection!");
				}
				
			}
			
		}
		
	}

	
	
	private static Logger logger;
	public boolean stopping;
	public Listener_r listener_r;
	public Thread listener_t;
	public int sctp_id;
	public static ConcurrentHashMap<Integer, ClientConnection> connection_lst;

	
	public static KernelStats getStats(int gpu_id){
		return CudaManager.getStats(gpu_id);
	}	
	public static void reset(int lst_id){
		for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
			if(GPUNConfigData.debug) logger.debug("CUDA RESET, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "]!");
			CudaManager.reset(GPUNConfigData.cuda_devices[i], lst_id);
		}
	}
	
	public static void remove(int lst_id, byte[] msg_bytes){
		// push to all devices
		for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
			if(GPUNConfigData.debug) logger.debug("CUDA Remove, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "]");
			//CudaManager.enqueueRemove(GPUNConfigData.cuda_devices[i], lst_id, item_id);
			CudaManager.remove(GPUNConfigData.cuda_devices[i], lst_id, 0, msg_bytes);
			
		
		}
	}
	public static void update(int lst_id, byte[] data){
		// push to all devices
		if(lst_id == CudaManager.MD5_QUARANTINE_LST || lst_id == CudaManager.MD5_NORMAL_LST){
			for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
				if(GPUNConfigData.debug) logger.debug("CUDA MD5 Update, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
				CudaManager.pushToMD5UpdateQueue(GPUNConfigData.cuda_devices[i], lst_id, data);
			}			
			
		}else{
			for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
				if(GPUNConfigData.debug) logger.debug("CUDA Update, GPU: [" + GPUNConfigData.cuda_devices[i] + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
				CudaManager.pushToUPDATEQueue(GPUNConfigData.cuda_devices[i], lst_id, data);
			}			
		}
	}
	public static void check(int lst_id, byte[] data, CudaCallback callback){
		int dev = CudaManager.getFreeDevice();
		if(lst_id == CudaManager.MD5_QUARANTINE_LST || lst_id == CudaManager.MD5_NORMAL_LST){
			if(GPUNConfigData.debug) logger.debug("CUDA MD5, GPU: [" + dev + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
			CudaManager.pushToMD5Queue(dev, lst_id, data, callback);
		}else{
			if(GPUNConfigData.debug) logger.debug("CUDA LD, GPU: [" + dev + "], lst id: [" + lst_id + "], msg length: [" + data.length + "]");
			CudaManager.pushToLDQueue(dev, lst_id, data, callback);
		}
	}
	
	
	public GPUNode(){
		String tmp;
		String[] tmp_lst;
		int tmp_id;
		try{
			// logging
			LoggingManager.init();

			// security
			/*
			SecurityManager.init();
			SecurityManager.set_critical_feature(SecurityManager.FEATURE_PDN);
			if(!SecurityManager.check_feature(SecurityManager.FEATURE_PDN)){
				LoggingManager.error(logger, "Invalid license, shutting down!");
				System.exit(0);
				
			}
			*/
			ConfigManagerV2.init("conf/gpun.properties");
			connection_lst = new ConcurrentHashMap<Integer, ClientConnection>();
			// get config data
			GPUNConfigData.id = ConfigManagerV2.get("id");
			GPUNConfigData.cuda_max_repetition_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.repetition.size"));
			GPUNConfigData.cuda_max_spam_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.spam.size"));
			GPUNConfigData.cuda_max_quarantine_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.quarantine.size"));
			GPUNConfigData.cuda_max_md5_size = Integer.parseInt(ConfigManagerV2.get("cuda.max.md5.size"));
			GPUNConfigData.cuda_max_i_size = Integer.parseInt(ConfigManagerV2.get("cuda.max_i_size"));
			GPUNConfigData.cuda_max_i_size_variation = Integer.parseInt(ConfigManagerV2.get("cuda.max_i_size.variation"));
			GPUNConfigData.cuda_max_q_size = Integer.parseInt(ConfigManagerV2.get("cuda.max_q_size"));
			// grid configurations
			// Repetition grid
			GPUNConfigData.repetition_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.repetition.grid").split(":");
			GPUNConfigData.repetition_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.repetition_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.repetition_grid.threads = Integer.parseInt(tmp_lst[2]);
			// Spam grid
			GPUNConfigData.spam_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.spam.grid").split(":");
			GPUNConfigData.spam_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.spam_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.spam_grid.threads = Integer.parseInt(tmp_lst[2]);
			// Quarantine grid
			GPUNConfigData.quarantine_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.quarantine.grid").split(":");
			GPUNConfigData.quarantine_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.quarantine_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.quarantine_grid.threads = Integer.parseInt(tmp_lst[2]);
			// MD5 grid
			GPUNConfigData.md5_grid = new GridDescriptor();
			tmp_lst = ConfigManagerV2.get("cuda.md5.grid").split(":");
			GPUNConfigData.md5_grid.blocks_w = Integer.parseInt(tmp_lst[0]);
			GPUNConfigData.md5_grid.blocks_h = Integer.parseInt(tmp_lst[1]);
			GPUNConfigData.md5_grid.threads = Integer.parseInt(tmp_lst[2]);
			
			// other
			GPUNConfigData.port = Integer.parseInt(ConfigManagerV2.get("port"));
			GPUNConfigData.ld_threshold = Integer.parseInt(ConfigManagerV2.get("ld.threshold"));
			//REPConfigData.ld_quarantine_threshold = Integer.parseInt(ConfigManagerV2.get("ld.quarantine.threshold"));
			GPUNConfigData.debug = (ConfigManagerV2.get("debug").equalsIgnoreCase("ON") ? true : false);
			// cli port
			GPUNConfigData.cli_port = Integer.parseInt(ConfigManagerV2.get("cli.port"));

			// Stats manager
			StatsManager.init();
			
			
			
			// cuda devices
			tmp = ConfigManagerV2.get("cuda.devices");
			tmp_lst = tmp.split(":");
			GPUNConfigData.cuda_devices = new int[tmp_lst.length];
			for(int i = 0; i<tmp_lst.length; i++){
				tmp_id = Integer.parseInt(tmp_lst[i]);
				GPUNConfigData.cuda_devices[i] = tmp_id;
			}
			logger.info("GPU Node id = [" + GPUNConfigData.id + "]!");
			// CUDA
			logger.info("Initializing CUDA...");
			logger.info("Max repetition list size: [" + GPUNConfigData.cuda_max_repetition_size + "]!");
			logger.info("Max known spam list size: [" + GPUNConfigData.cuda_max_spam_size + "]!");
			logger.info("Max Quarantine list size: [" + GPUNConfigData.cuda_max_quarantine_size + "]!");
			logger.info("Max MD5 list size: [" + GPUNConfigData.cuda_max_md5_size + "]!");
			logger.info("Max list item size: [" + GPUNConfigData.cuda_max_i_size + "]!");
			logger.info("Max list item size variation : [+/- " + GPUNConfigData.cuda_max_i_size_variation + "]!");
			logger.info("Max device queue size: [" + GPUNConfigData.cuda_max_q_size + "]!");
			logger.info("LD threshold: [" + GPUNConfigData.ld_threshold + "]!");
			logger.info("REPETITION Grid configuration: [" + GPUNConfigData.repetition_grid.blocks_w + " x " + GPUNConfigData.repetition_grid.blocks_h + " x " + GPUNConfigData.repetition_grid.threads + "]!");
			logger.info("SPAM Grid configuration: [" + GPUNConfigData.spam_grid.blocks_w + " x " + GPUNConfigData.spam_grid.blocks_h + " x " + GPUNConfigData.spam_grid.threads + "]!");
			logger.info("QUARANTINE Grid configuration: [" + GPUNConfigData.quarantine_grid.blocks_w + " x " + GPUNConfigData.quarantine_grid.blocks_h + " x " + GPUNConfigData.quarantine_grid.threads + "]!");
			logger.info("MD5 Grid configuration: [" + GPUNConfigData.md5_grid.blocks_w + " x " + GPUNConfigData.md5_grid.blocks_h + " x " + GPUNConfigData.md5_grid.threads + "]!");
			
			// init cli
			CLIService.init(GPUNConfigData.cli_port);
			
			// init CUDAM
			CudaManager.init(GPUNConfigData.cuda_max_repetition_size,
							GPUNConfigData.cuda_max_spam_size,
							GPUNConfigData.cuda_max_quarantine_size,
							GPUNConfigData.cuda_max_md5_size,
							GPUNConfigData.cuda_max_i_size, 
							GPUNConfigData.cuda_max_i_size_variation,
							GPUNConfigData.cuda_max_q_size); 
			// init CUDAM grids
			CudaManager.initGrid(CudaManager.LD_REPETITION, GPUNConfigData.repetition_grid.blocks_w, GPUNConfigData.repetition_grid.blocks_h, GPUNConfigData.repetition_grid.threads);
			CudaManager.initGrid(CudaManager.LD_SPAM, GPUNConfigData.spam_grid.blocks_w, GPUNConfigData.spam_grid.blocks_h, GPUNConfigData.spam_grid.threads);
			CudaManager.initGrid(CudaManager.MD5_QUARANTINE_LST, GPUNConfigData.quarantine_grid.blocks_w, GPUNConfigData.quarantine_grid.blocks_h, GPUNConfigData.quarantine_grid.threads);
			CudaManager.initGrid(CudaManager.MD5_NORMAL_LST, GPUNConfigData.md5_grid.blocks_w, GPUNConfigData.md5_grid.blocks_h, GPUNConfigData.md5_grid.threads);
			
			logger.info("Initializing GPUs...");
			for(int i = 0; i<GPUNConfigData.cuda_devices.length; i++){
				logger.info("Initializing GPU:[" + GPUNConfigData.cuda_devices[i] + "]...");
				CudaManager.initDevice(GPUNConfigData.cuda_devices[i]);
			}
			logger.info("CUDA ready...");
			logger.info("GPU Node ready!");

			// SCTP
			sctp_id = SSctp.initServer("0.0.0.0", GPUNConfigData.port);
			if(sctp_id > 0){
				// init thread
				listener_r = new Listener_r();
				listener_t = new Thread(listener_r, "GPUN_LISTENER");
				listener_t.start();
			}else{
				logger.error("Error while initializing SCTP listener!");
				System.exit(0);
			}
			
			
			
			
		}catch(Exception e){
			e.printStackTrace();
			logger.error("Error while starting GPU Node!");
			System.exit(0);
			
		}


	
	}
	
	
	public static void main(String[] args) {
		PropertyConfigurator.configure(GPUNode.class.getResource("/log4j.gpun.properties"));
		logger = Logger.getLogger(GPUNode.class);
		logger.info("Initializing GPU Node...");
		new GPUNode();
		
	
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				CudaManager.shutdown();
				try{ Thread.sleep(5000); }catch(Exception e){e.printStackTrace();}
				logger.info("Shutdown complete...");

				
			}
			
		});
		

	}	
}
