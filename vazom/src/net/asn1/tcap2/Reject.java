package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Reject extends SEQUENCE{

	public RejectInvokeIDRej get_invokeIDRej(){
		return (RejectInvokeIDRej)elements.get(0).data;
	}
	public RejectInvokeIDRej new_invokeIDRej(){
		return new RejectInvokeIDRej();
	}

	public RejectProblem get_problem(){
		return (RejectProblem)elements.get(1).data;
	}
	public RejectProblem new_problem(){
		return new RejectProblem();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeIDRej(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_problem(); }});
	}
	public Reject(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
