package net.spcap;

import java.util.HashMap;

public enum IPProtocolType {
	TCP(0x06),
	UDP(0x11),
	SCTP(0x84);

	
	private int id;
	private static final HashMap<Integer, IPProtocolType> lookup = new HashMap<Integer, IPProtocolType>();
	static{
		for(IPProtocolType td : IPProtocolType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static IPProtocolType get(int id){ return lookup.get(id); }
	private IPProtocolType(int _id){ id = _id; }
}
