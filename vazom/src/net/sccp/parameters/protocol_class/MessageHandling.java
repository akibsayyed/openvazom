package net.sccp.parameters.protocol_class;

import java.util.HashMap;

public enum MessageHandling {
	NO_SPECIAL_OPTIONS(0x00),
	RETURN_MESSAGE_ON_ERROR(0x80);
	
	// bits 7..4
	
	private int id;
	private static final HashMap<Integer, MessageHandling> lookup = new HashMap<Integer, MessageHandling>();
	static{
		for(MessageHandling td : MessageHandling.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static MessageHandling get(int id){ return lookup.get(id); }
	private MessageHandling(int _id){ id = _id; }
	
}
