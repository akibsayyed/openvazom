package net.sccp.parameters.global_title;

import java.util.ArrayList;
import java.util.Arrays;


public class GlobalTitle_TTNPE extends GlobalTitleBase{
	public int translationType;
	public NumberingPlan numberingPlan;
	public EncodingScheme encodingScheme;
	public byte[] addressInformation;
	
	
	public GlobalTitle_TTNPE(){
		super();
		type = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING;
	}
	
	public byte[] encode(){
		byte[] res = null;
		int tmp;
		ArrayList<Byte> packet = new ArrayList<Byte>();
		packet.add((byte)translationType);
		
		tmp = numberingPlan.getId() + encodingScheme.getId();
		packet.add((byte)tmp);
		for(int i = 0; i<addressInformation.length; i++) packet.add(addressInformation[i]);
		
		res = new byte[packet.size()];
		for(int i = 0; i<packet.size(); i++) res[i] = packet.get(i);
		return res;
	}	
	public void init(byte[] data) {
		translationType = data[0] & 0xFF;
		numberingPlan = NumberingPlan.get(data[1] & 0xF0);
		encodingScheme = EncodingScheme.get(data[1] & 0x0F);
		addressInformation = Arrays.copyOfRange(data, 2, data.length);
		
	}

}
