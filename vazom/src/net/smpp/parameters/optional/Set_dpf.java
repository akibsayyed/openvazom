package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Set_dpf extends OptionalParameterBase {

	public Set_dpf(){
		type = OptionalParameterType.SET_DPF;
	}
	public int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
