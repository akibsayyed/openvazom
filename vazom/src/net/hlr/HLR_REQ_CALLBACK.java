package net.hlr;

import net.ds.DataSourceType;
import net.routing.CallbackBase;
import net.sgn.FNDescriptor;
import net.sgn.FNManager;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;

public class HLR_REQ_CALLBACK extends CallbackBase{
	public String id;
	public String f_id;
	public long tcap_id;
	public HLRPacket hlrp_reply;
	
	public HLR_REQ_CALLBACK(String _msg_id, String _f_id, long _tcap_id){
		id = _msg_id;
		f_id = _f_id;
		tcap_id = _tcap_id;
		
	}
	public void setHLRP_REPLY(HLRPacket hlrp){
		hlrp_reply = hlrp;
	}
	public void run() {
		FNDescriptor fnd = FNManager.getNode(f_id);
		MessageDescriptor md = null;
		if(fnd != null && hlrp_reply != null){
			md = new MessageDescriptor();
			// header
			md.header.destination = LocationType.FN;
			md.header.source = LocationType.SGN;
			md.header.ds = DataSourceType.NA;
			md.header.msg_id = id;
			md.header.msg_type = MessageType.HLRRPL;
			// body
			if(hlrp_reply.imsi != null) md.values.put(VSTPDataItemType.HLR_IMSI.getId(), hlrp_reply.imsi);
			if(hlrp_reply.nnn != null) md.values.put(VSTPDataItemType.HLR_NNN.getId(), hlrp_reply.nnn);
			if(hlrp_reply.annn != null) md.values.put(VSTPDataItemType.HLR_ANNN.getId(), hlrp_reply.annn);
			// send to out queue
			fnd.private_out_queue.offer(md);
		}
	}
	public long get_callback_id() {
		return tcap_id;
	}
	public void set_callback_id(long _id) {
		// not used
	}
	public void set_params(Object[] params) {
		// not used
	}
	
}
