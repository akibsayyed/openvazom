package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ErrorDescriptor extends SEQUENCE{

	public ErrorCode get_errorCode(){
		return (ErrorCode)elements.get(0).data;
	}
	public ErrorCode new_errorCode(){
		return new ErrorCode();
	}

	public ErrorText get_errorText(){
		return (ErrorText)elements.get(1).data;
	}
	public ErrorText new_errorText(){
		return new ErrorText();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_errorCode(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_errorText(); }});
	}
	public ErrorDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
