package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SM_DeliveryNotIntended extends ENUMERATED{

	public static final int _onlyIMSI_requested = 0;
	public static final int _onlyMCC_MNC_requested = 1;
	public SM_DeliveryNotIntended(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
