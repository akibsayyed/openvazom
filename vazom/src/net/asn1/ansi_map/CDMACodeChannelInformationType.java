package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMACodeChannelInformationType extends CDMACodeChannelInformation{

	public CDMACodeChannelInformationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 131;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
