package net.isup.parameters;

import java.util.Arrays;

import net.isup.ParameterType;

public class ClosedUserGroupInterlock extends ParameterBase {
	public int NI_digit_1;
	public int NI_digit_2;
	public int NI_digit_3;
	public int NI_digit_4;
	public byte[] binary_code;
	
	public ClosedUserGroupInterlock(){
		type = ParameterType.CLOSED_USER_GROUP_INTERLOCK_CODE;
	}

	public void init(byte[] data) {
		if(data != null){
			NI_digit_1 = (data[byte_position] & 0xf0) >> 4;
			NI_digit_2 = (data[byte_position] & 0x0f);
			byte_position++;
			NI_digit_3 = (data[byte_position] & 0xf0) >> 4;
			NI_digit_4 = (data[byte_position] & 0x0f);
			byte_position++;
			binary_code = Arrays.copyOfRange(data, byte_position, data.length);
		}
		
	}

}
