package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class InvokeProblem extends INTEGER{

	public static final int _duplicateInvokeID = 0;
	public static final int _unrecognizedOperation = 1;
	public static final int _mistypedParameter = 2;
	public static final int _resourceLimitation = 3;
	public static final int _initiatingRelease = 4;
	public static final int _unrecognizedLinkedID = 5;
	public static final int _linkedResponseUnexpected = 6;
	public static final int _unexpectedLinkedOperation = 7;
	public InvokeProblem(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
