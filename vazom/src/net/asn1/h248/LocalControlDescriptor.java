package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocalControlDescriptor extends SEQUENCE{

	public StreamMode get_streamMode(){
		return (StreamMode)elements.get(0).data;
	}
	public StreamMode new_streamMode(){
		return new StreamMode();
	}

	public BOOLEAN get_reserveValue(){
		return (BOOLEAN)elements.get(1).data;
	}
	public BOOLEAN new_reserveValue(){
		return new BOOLEAN();
	}

	public BOOLEAN get_reserveGroup(){
		return (BOOLEAN)elements.get(2).data;
	}
	public BOOLEAN new_reserveGroup(){
		return new BOOLEAN();
	}

	public PropertyParmSequenceOf get_propertyParms(){
		return (PropertyParmSequenceOf)elements.get(3).data;
	}
	public PropertyParmSequenceOf new_propertyParms(){
		return new PropertyParmSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamMode(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reserveValue(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reserveGroup(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_propertyParms(); }});
	}
	public LocalControlDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
