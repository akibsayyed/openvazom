package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudMediaDescriptorStreamsChoice extends CHOICE{

	public IndAudStreamParms get_oneStream(){
		return (IndAudStreamParms)elements.get(0).data;
	}
	public IndAudStreamParms new_oneStream(){
		return new IndAudStreamParms();
	}

	public IndAudStreamDescriptorSequenceOf get_multiStream(){
		return (IndAudStreamDescriptorSequenceOf)elements.get(1).data;
	}
	public IndAudStreamDescriptorSequenceOf new_multiStream(){
		return new IndAudStreamDescriptorSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_oneStream(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_multiStream(); }});
	}
	public IndAudMediaDescriptorStreamsChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
