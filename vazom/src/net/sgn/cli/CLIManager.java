package net.sgn.cli;


import net.cli.AuthManager;
import net.cli.CLIBase;
import net.cli.GroupDescriptor;
import net.cli.MethodDescriptor;
import net.cli.ParamDescriptor;
import net.config.ConfigManagerV2;
import net.config.SGNConfigData;
import net.ds.DSDescriptor;
import net.ds.DSManager;
import net.ds.DataSourceType;
import net.hplmnr.HPLMNRManager;
import net.m3ua_conn.M3UAConnManager;
import net.m3ua_conn.M3UAConnection;
import net.routing.RoutingConnectionDirectionType;
import net.routing.RoutingConnectionType;
import net.sgn.FNDescriptor;
import net.sgn.FNManager;
import net.sgn.SGNode;
import net.smpp_conn.SMPPConnManager;
import net.smpp_conn.SMPPConnection;
import net.spcap.CapStats;
import net.spcap.SPcap;
import net.stats.StatsManager;
import net.utils.Utils;

public class CLIManager extends CLIBase{
	// SMPP
	public class GRP_SMPP extends GroupDescriptor {

		public GRP_SMPP() {
			super("SMPP", "SMPP Connections ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_SMPP _grp_SMPP = new GRP_SMPP();


	public class CLI_smpp_info extends MethodDescriptor {

		public CLI_smpp_info() {
			super("INFO", "Connection information ", "SMPP", "");
		}

		public String execute(Object[] params) {
			String res = "[BLUE]SMPP Connections:\n";
			SMPPConnection smpp_conn = null;
			for(String s : SMPPConnManager.conn_map.keySet()){
				smpp_conn = SMPPConnManager.conn_map.get(s);
				
				res += "[GREEN]                   ID = [YELLOW]" + smpp_conn.id + "\n";
				res += "[GREEN]               ACTIVE = [YELLOW]" + smpp_conn.active + "\n";
				res += "[GREEN]          REMOTE ADDR = [YELLOW]" + smpp_conn.ip + "\n";
				res += "[GREEN]          REMOTE PORT = [YELLOW]" + smpp_conn.port + "\n";
				res += "[GREEN]ENQUIRE_LINK INTERVAL = [YELLOW]" + (smpp_conn.el_interval / 1000) + "\n";
				res += "[BLUE]Bandwith summary:\n";

				// calculate stats
				long ts = System.currentTimeMillis();
				smpp_conn.stats.AVG_PPS = (double)(smpp_conn.stats.OUT + smpp_conn.stats.IN)  / ((double)(ts - smpp_conn.stats.ts) / 1000);
				smpp_conn.stats.AVG_BYTES_S = (double)(smpp_conn.stats.BYTES)  / ((double)(ts - smpp_conn.stats.ts) / 1000);
				smpp_conn.stats.AVG_MBIT_S = (double)(smpp_conn.stats.AVG_BYTES_S * 8) / 1000000;
				
				
				res += "[WHITE]         Packets: [[GREEN]" + String.format("%,d", smpp_conn.stats.OUT + smpp_conn.stats.IN) + "[WHITE]]\n";
				res += "[WHITE]           Bytes: [[GREEN]" + String.format("%,d", smpp_conn.stats.BYTES) + "[WHITE]]\n";
				res += "[WHITE]Avg. packets/sec: [[GREEN]" + String.format("%,.2f", smpp_conn.stats.AVG_PPS) + "[WHITE]]\n";
				res += "[WHITE]  Avg. bytes/sec: [[GREEN]" + String.format("%,.2f", smpp_conn.stats.AVG_BYTES_S) + "[WHITE]]\n";
				res += "[WHITE]   Avg. MBit/sec: [[GREEN]" + String.format("%,.2f", smpp_conn.stats.AVG_MBIT_S) + "[WHITE]]\n";
				res += "[WHITE]   RX queue limit: [[RED]" + smpp_conn.IN_QUEUE_MAX + "[WHITE]]\n";
				res += "[WHITE]   TX queue limit: [[RED]" + smpp_conn.OUT_QUEUE_MAX + "[WHITE]]\n";

				res += "\n\n";
			}
			return res;		
		}

	}
	public CLI_smpp_info _cli_smpp_info = new CLI_smpp_info();

	public class CLI_smpp_shut extends MethodDescriptor {

		public CLI_smpp_shut() {
			super("SHUT", "Shutdown connection ", "SMPP", "conn_id");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				SMPPConnection smpp_conn = SMPPConnManager.shutConnection(pd.value);
				if(smpp_conn != null) return "[WHITE]SMPP Connection [[GREEN]" + pd.value + "[WHITE]] shutting down!";
				else return "[WHITE]SMPP Connection [[GREEN]" + pd.value + "[WHITE]] does not exist!";

			}
			return "";
		}

	}
	public CLI_smpp_shut _cli_smpp_shut = new CLI_smpp_shut();

	public class CLI_smpp_noshut extends MethodDescriptor {

		public CLI_smpp_noshut() {
			super("NOSHUT", "Reactivate connection ", "SMPP", "conn_id");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				SMPPConnection smpp_conn = SMPPConnManager.noshutConnection(pd.value);
				if(smpp_conn != null) return "[WHITE]SMPP Connection [[GREEN]" + pd.value + "[WHITE]] reinitializing!\n";
				else return "[WHITE]SMPP Connection [[GREEN]" + pd.value + "[WHITE]] does not exist!\n";
				
			}
			return "";
		}
	}
	public CLI_smpp_noshut _cli_smpp_noshut = new CLI_smpp_noshut();

	public class CLI_smpp_add extends MethodDescriptor {

		public CLI_smpp_add() {
			super("ADD", "Add new connection ", "SMPP", "conn_id:conn_ip:conn_port:system_id:password:el_interval:direction");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = null;
				// validate
				for(int i = 0; i<params.length - 1; i++){
					pd = (ParamDescriptor)params[i];
					if(pd.value == null) return "[RED]Missing parameter [[GREEN]" + pd.name + "[RED]]!";
				}
				// add connection
				SMPPConnManager.addConnection(	((ParamDescriptor)params[0]).value, 
												((ParamDescriptor)params[1]).value, 
												Utils.safeStr2int(((ParamDescriptor)params[2]).value), 
												((ParamDescriptor)params[3]).value, 
												((ParamDescriptor)params[4]).value, 
												Utils.safeStr2int(((ParamDescriptor)params[5]).value)*1000,
												RoutingConnectionDirectionType.valueOf(((ParamDescriptor)params[6]).value.toUpperCase().trim()));
				
				
				// update config file
				ConfigManagerV2.add_after(
											"smpp." + ((ParamDescriptor)params[0]).value, 
											((ParamDescriptor)params[6]).value + ":" +
											((ParamDescriptor)params[1]).value + ":" +
											((ParamDescriptor)params[2]).value + ":" + 
											((ParamDescriptor)params[3]).value + ":" + 
											((ParamDescriptor)params[4]).value + ":" + 
											((ParamDescriptor)params[5]).value, 
											null, 
											"smpp.connections");
				
				return "[WHITE]Adding SMPP Connection...";
				
			}
			return "";		
		}

	}
	public CLI_smpp_add _cli_smpp_add = new CLI_smpp_add();
	
	
	// SMPP end
	
	
	
	// AUTH 
    public class GRP_AUTH extends GroupDescriptor {

        public GRP_AUTH() {
        	super("AUTH", "CLI Authentication management ", "", "");
        }

        public String execute(Object[] params) {
        	return null;
        }

    }
	public GRP_AUTH _grp_AUTH = new GRP_AUTH();
	
	
	public class CLI_user_list extends MethodDescriptor {
	
		public CLI_user_list() {
			super("USRLST", "List users ", "AUTH", "");
		}
		
		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.auth_user_group == 0){
					for(String s : AuthManager.auth_map.keySet()){
						res += "[WHITE]Username = [[GREEN]" + s + "[WHITE]], credentials = [[GREEN]" + (AuthManager.checkGroup(s) == 0 ? "admin" : "user") + "[WHITE]]\n";
					}
				}else{
					return "[RED]Insufficient credentials!";
				}

			}
			return res;
			
		}
	
	}
	public CLI_user_list _cli_user_list = new CLI_user_list();
	
	public class CLI_grp_list extends MethodDescriptor {
	
		public CLI_grp_list() {
	        super("GRPLST", "List groups ", "AUTH", "");
		}
		
		public String execute(Object[] params) {
			return "[WHITE] Groups = [[RED]0 [WHITE]= [GREEN]admin[WHITE], [RED]1[WHITE] = [GREEN]user[WHITE]]!";
		}
	
	}
	public CLI_grp_list _cli_grp_list = new CLI_grp_list();
	
	public class CLI_user_add extends MethodDescriptor {
	
	    public CLI_user_add() {
            super("USRADD", "Add user ", "AUTH", "group_id:username:password:password_repeat");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				ParamDescriptor pd4 = (ParamDescriptor)params[3];
				CLIConnection conn = (CLIConnection)params[4];
				if(conn.auth_user_group == 0){
					if(pd1.value != null && pd2.value != null && pd3.value != null && pd4.value != null){
						if(!pd3.value.equals(pd4.value)) return "[RED]Passwords do not match!";
						if(!Utils.isInt(pd1.value)) return "[RED]Unknown group id = [" + pd1.value +"]!";
						if(Integer.parseInt(pd1.value) != 0 && Integer.parseInt(pd1.value) != 1) return "[RED]Unknown group id = [" + pd1.value +"]!";
						// add user
						if(!AuthManager.userExists(pd2.value)){
							AuthManager.set(pd2.value, pd3.value, Integer.parseInt(pd1.value));
							return "[WHITE]Adding user [[GREEN]" + pd2.value + "[WHITE]], credentials = [[GREEN]" + AuthManager.grpId2Name(Integer.parseInt(pd1.value)) + "[WHITE]]";
							
						}else{
							return "[RED]User [[GREEN]" + pd2.value +"[RED]] already exists!";
							
						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }
	
	}
	public CLI_user_add _cli_user_add = new CLI_user_add();
	
    public class CLI_user_set extends MethodDescriptor {

	    public CLI_user_set() {
            super("USRSET", "Set user information ", "AUTH", "group_id:username:password:password_repeat");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				ParamDescriptor pd4 = (ParamDescriptor)params[3];
				CLIConnection conn = (CLIConnection)params[4];
				if(conn.auth_user_group == 0){
					if(pd1.value != null && pd2.value != null && pd3.value != null && pd4.value != null){
						if(!pd3.value.equals(pd4.value)) return "[RED]Passwords do not match!";
						if(!Utils.isInt(pd1.value)) return "[RED]Unknown group id = [" + pd1.value +"]!";
						if(Integer.parseInt(pd1.value) != 0 && Integer.parseInt(pd1.value) != 1) return "[RED]Unknown group id = [" + pd1.value +"]!";
						// set user
						if(AuthManager.userExists(pd2.value)){
							AuthManager.set(pd2.value, pd3.value, Integer.parseInt(pd1.value));
							return "[WHITE]Setting user [[GREEN]" + pd2.value + "[WHITE]], credentials = [[GREEN]" + AuthManager.grpId2Name(Integer.parseInt(pd1.value)) + "[WHITE]]";
							
						}else{
							return "[RED]User [[GREEN]" + pd2.value +"[RED]] does not exist!";

						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }

	}
	public CLI_user_set _cli_user_set = new CLI_user_set();
	
	public class CLI_user_rm extends MethodDescriptor {
	
	    public CLI_user_rm() {
            super("USRRM", "Remove user ", "AUTH", "username");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				CLIConnection conn = (CLIConnection)params[1];
				if(conn.auth_user_group == 0){
					if(pd1.value != null){
						// set user
						if(AuthManager.userExists(pd1.value)){
							AuthManager.remove(pd1.value);
							return "[WHITE]Removing user [[GREEN]" + pd1.value + "[WHITE]]!";
							
						}else{
							return "[RED]User [[GREEN]" + pd1.value +"[RED]] does not exist!";

						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }
	
	}
	public CLI_user_rm _cli_user_rm = new CLI_user_rm();
	
	public class CLI_users_passwd extends MethodDescriptor {
	
        public CLI_users_passwd() {
            super("PASSWD", "Change password ", "AUTH", "password:password_repeat");
        }

        public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				CLIConnection conn = (CLIConnection)params[2];
				if(pd1.value != null && pd2.value != null){
					if(!pd1.value.equals(pd2.value)) return "[RED]Passwords do not match!";
					
					AuthManager.set(conn.auth_user, pd1.value, AuthManager.checkGroup(conn.auth_user));
					return "[WHITE]Changing password for user [[GREEN]" + conn.auth_user + "[WHITE]]!";
					
				}else{
					return "[RED]Missing parameters!";
					
				}

	
			}
			return res;

        }
	
	}
	public CLI_users_passwd _cli_users_passwd = new CLI_users_passwd();
	
	
	// AUTH end
	
	
	
	public class GRP_SHOW extends GroupDescriptor {
		public GRP_SHOW() {
			super("SHOW", "Statistics and memory usage ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_SHOW _grp_SHOW = new GRP_SHOW();


	public class CLI_show_memory extends MethodDescriptor {

		public CLI_show_memory() {
			super("MEMORY", "Memory usage ", "SHOW", "");
		}

		public String execute(Object[] params) {
			double used_mem =  (double)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024/1024;
			double total_mem =  (double)Runtime.getRuntime().totalMemory() / 1024/1024;
			return String.format("[ [GREEN]%7.2fMb[RESET] / [RED]%7.2fMb[RESET] ]", used_mem, total_mem);
		}

	}
	public CLI_show_memory _cli_show_memory = new CLI_show_memory();

	public class GRP_STATS extends GroupDescriptor {

		public GRP_STATS() {
			super("STATS", "Various statistics ", "SHOW", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_STATS _grp_STATS = new GRP_STATS();


	public class CLI_show_stats_smpp extends MethodDescriptor {
		public CLI_show_stats_smpp(){
			super("SMPPP", "SMPP Proxy statistics ", "STATS,SHOW", "");
			
		}

		public String execute(Object[] params) {
			String res = "";
			if(SGNConfigData.smpp_proxy_status){
				res += "[BLUE]SMPP Proxy statistics...\n";
				res += "[WHITE]SMPP CLIENT_CONNECTION COUNT: [GREEN]" + StatsManager.SMPP_STATS.CLIENT_CONNECTION_COUNT + "\n";
				res += "[WHITE]SMPP END_POINT_CONNECTION COUNT: [GREEN]" + StatsManager.SMPP_STATS.END_POINT_CONNECTION_COUNT + "\n";
				res += "[WHITE]SMPP SUBMIT_SM COUNT: [GREEN]" + StatsManager.SMPP_STATS.SUBMIT_SM_COUNT + "\n";
				res += "[WHITE]SMPP SUBMIT_MULTI COUNT: [GREEN]" + StatsManager.SMPP_STATS.SUBMIT_MULTI_COUNT + "\n";
				res += "[WHITE]SMPP DELIVER_SM COUNT: [GREEN]" + StatsManager.SMPP_STATS.DELIVER_SM_COUNT + "\n";
				res = Utils.str_align(res, ":");
			}else res = "[RED]SMPP Proxy if OFF!";
			return res;
		}
	}

	public CLI_show_stats_smpp _cli_show_stats_smpp = new CLI_show_stats_smpp();
	
	public class CLI_show_stats_system extends MethodDescriptor{
		public CLI_show_stats_system(){
			super("SYSTEM", "Vazom Various System statistics ", "STATS,SHOW", "");
			
		}
		
		public String execute(Object[] params) {
			String res = "";
			res += "[WHITE]SGN_IN_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.SGN_IN_QUEUE_MAX + "\n";
			res += "[WHITE]SGN_OUT_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.SGN_OUT_QUEUE_MAX + "\n";
			res += "[WHITE]SGC_IN_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.SGC_IN_QUEUE_MAX + "\n";
			res += "[WHITE]SGC_OUT_QUEUE_MAX: [RED]" + StatsManager.SYSTEM_STATS.SGC_OUT_QUEUE_MAX + "\n";
			return Utils.str_align(res, ":");
		}
		
	}
	public CLI_show_stats_system _cli_show_stats_system = new CLI_show_stats_system();
	
	public class CLI_show_stats_vstp extends MethodDescriptor {

		public CLI_show_stats_vstp() {
			super("VSTP", "Vazom Signaling Transfer Protocol statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			res += "[WHITE]VSTP_SGF_TIMEOUT COUNT: [RED]" + StatsManager.VSTP_STATS.VSTP_SGF_TIMEOUT + "\n";
			res += "[WHITE]VSTP_CRL_ADD COUNT: [GREEN]" + StatsManager.VSTP_STATS.VSTP_CRL_ADD + "\n";
			res += "[WHITE]VSTP_CRL_REMOVE COUNT: [GREEN]" + StatsManager.VSTP_STATS.VSTP_CRL_REMOVE + "\n";
			res += "[WHITE]VSTP_SGC_CONNECTION_LOST COUNT: [RED]" + StatsManager.VSTP_STATS.VSTP_SGC_CONNECTION_LOST + "\n";
			res += "[WHITE]VSTP_F_CONNECTION_LOST COUNT: [RED]" + StatsManager.VSTP_STATS.VSTP_F_CONNECTION_LOST + "\n";
			res += "[WHITE]VSTP_NO_F_NODES COUNT: [RED]" + StatsManager.VSTP_STATS.VSTP_NO_F_NODES + "\n";
			return Utils.str_align(res, ":");
		}

	}
	public CLI_show_stats_vstp _cli_show_stats_vstp = new CLI_show_stats_vstp();

	public class CLI_show_stats_pds extends MethodDescriptor {

		public CLI_show_stats_pds() {
			super("PDS", "Passive Data Source statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			DSDescriptor dsd = null;
			if(SGNConfigData.pds_status){
				res += "[BLUE]IP Fragmentation...\n";
				res += "[WHITE]Reassembled: [GREEN]" + StatsManager.IP_FRAG_STATS.IP_FRAG_ASSEMBLED + "\n";
				res += "[WHITE]Missing fragments: [RED]" + StatsManager.IP_FRAG_STATS.IP_FRAG_MISSING + "\n";
				res += "[WHITE]Max fragment count: [GREEN]" + StatsManager.IP_FRAG_STATS.IP_FRAG_MAX_FRAGMENTS + "\n";
				res += "[WHITE]Min fragment count: [GREEN]" + StatsManager.IP_FRAG_STATS.IP_FRAG_MIN_FRAGMENTS + "\n";
				res += "[BLUE]Passive data sources...\n";
				for(String s : DSManager.ds_map.keySet()){
					dsd = DSManager.ds_map.get(s);
					
					if(dsd.type == DataSourceType.SPCAP_SCTP || dsd.type == DataSourceType.SPCAP_SMPP || dsd.type == DataSourceType.SPCAP_E1){
						CapStats cs = null;
						res += "[WHITE]Type: [GREEN]" + dsd.type +"\n";
						res += "[WHITE]ID: [GREEN]" + dsd.id + "\n";
						for(int i = 0; i<dsd.spcap_id.length; i++){
							cs = SPcap.getStats(dsd.spcap_id[i]);
							if(cs != null){
								res += "[WHITE]IF: [GREEN]" + dsd.if_lst[i] + "\n";
								res += "[WHITE]SPCAP ID: [GREEN]" + dsd.spcap_id[i] + "\n";
								res += "[WHITE]recv: [GREEN]" + cs.ps_recv + "\n"; 
								res += "[WHITE]drop: [RED]" + cs.ps_drop + "\n";
								res += "[WHITE]queue full: [RED]" + cs.q_full + "\n";
								res += "[WHITE]RX/TX queue limit: [RED]" + dsd.data_source.QUEUE_FULL +"\n"; 
							}
							res += "\n";
							
						}
					}
					res += "\n\n";
				}
				res = Utils.str_align(res, ":");
			}else res = "[RED]PDS module is OFF!";	
			return res;
		}

	}
	public CLI_show_stats_pds _cli_show_stats_pds = new CLI_show_stats_pds();

	public class CLI_show_stats_pdsd extends MethodDescriptor {

		public CLI_show_stats_pdsd() {
			super("PDSD", "Passive Data Source Decoder statistics ", "STATS,SHOW", "");
		}

		public String execute(Object[] params) {
			String res = "";
			if(SGNConfigData.pds_status){
				// Other
				res += "[BLUE]Other...\n";
				res += "[WHITE]SKIPPED: [YELLOW]"  + Long.toString(StatsManager.DECODER_STATS.SKIPPED_COUNT) + "\n";
				// Mtp3
				res += "[BLUE]MTP3 Related...\n";
				res += "[WHITE]SCCP: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SCCP) + "\n";
				res += "[WHITE]GCP: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.GCP) + "\n";
				res += "[WHITE]ISUP: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.ISUP) + "\n";
				
				// SCCP UDT
				res += "[BLUE]SCCP Related...\n";
				res += "[WHITE]SCCP UDT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.UDT_COUNT) + "\n";
				
				// TCAP
				res += "[BLUE]TCAP Related...\n";
				res += "[WHITE]TCAP TOTAL: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_COUNT) + "\n";
				res += "[WHITE]TCAP BEGIN: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_BEGIN) + "\n";
				res += "[WHITE]TCAP CONTINUE: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_CONTINUE) + "\n";
				res += "[WHITE]TCAP END: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_END) + "\n";
				res += "[WHITE]TCAP ABORT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_ABORT) + "\n";
				res += "[WHITE]TCAP RETURN_RESULT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_RETURN_RESULT) + "\n";
				res += "[WHITE]TCAP RETURN_ERROR: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.TCAP_RETURN_ERROR) + "\n";

				// MAP
				res += "[BLUE]GSM MAP Related...\n";
				res += "[WHITE]mo-forwardSM: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.MO_COUNT) + "\n";
				res += "[WHITE]mt-forwardSM: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.MT_COUNT) + "\n";
				
				// SMS
				res += "[BLUE]SMS-TPDU Related...\n";
				res += "[WHITE]SMS_SUBMIT COUNT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMS_SUBMIT_COUNT) + "\n";
				res += "[WHITE]SMS_DELIVER COUNT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMS_DELIVER_COUNT) + "\n";
				res += "[WHITE]MO_CONCATENATED COUNT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.MO_CONCATENATED_COUNT) + "\n";
				res += "[WHITE]MT_CONCATENATED COUNT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.MT_CONCATENATED_COUNT) + "\n";

				// HLR
				res += "[BLUE]HLR Related...\n";
				res += "[WHITE]SRI BEGIN: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SRI_BEGIN) + "\n";
				res += "[WHITE]SRI END: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SRI_END) + "\n";
				//res += "[WHITE]SRI NO_REPLY COUNT: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SRI_NO_REPLY) + "\n";

				// SMPP
				res += "[BLUE]SMPP Related...\n";
				res += "[WHITE]SMPP_SUBMIT_SM: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_SUBMIT_SM) + "\n";
				res += "[WHITE]SMPP_SUBMIT_MULTI: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_SUBMIT_MULTI) + "\n";
				res += "[WHITE]SMPP_DELIVER_SM: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_DELIVER_SM) + "\n";
				res += "[WHITE]SMPP_BIND_RECEIVER: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_BIND_RECEIVER) + "\n";
				res += "[WHITE]SMPP_BIND_TRANSMITTER: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_BIND_TRANSMITTER) + "\n";
				res += "[WHITE]SMPP_BIND_TRANSCEIVER: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_BIND_TRANSCEIVER) + "\n";
				res += "[WHITE]SMPP_UNBIND: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.SMPP_UNBIND) + "\n";
				// isup
				res += "[BLUE]ISUP Related...\n";
				res += "[WHITE]INITIAL ADDRESS: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.ISUP_IAM) + "\n";
				res += "[WHITE]ADDRESS COMPLETE: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.ISUP_ACM) + "\n";
				res += "[WHITE]ANSWER: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.ISUP_ANM) + "\n";
				res += "[WHITE]RELEASE: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.ISUP_REL) + "\n";
				res += "[WHITE]RELEASE COMPLETE: [GREEN]"  + Long.toString(StatsManager.DECODER_STATS.ISUP_RLC) + "\n";
				
				
				res = Utils.str_align(res, ":");
			}else res = "[RED]DS is OFF!";
			return res;
		}

	}
	public CLI_show_stats_pdsd _cli_show_stats_pdsd = new CLI_show_stats_pdsd();

	public class CLI_show_stats_hplmnr extends MethodDescriptor {

		public CLI_show_stats_hplmnr() {
			super("HPLMNR", "Home PLMN Routing Statistics ", "STATS,SHOW", "details");
		}

		public String execute(Object[] params) {
			String res = "";
			String res_data = "";
			if(SGNConfigData.hplmnr_status){
				if(params != null){
					ParamDescriptor pd = (ParamDescriptor)params[0];
					// flow
					String flow =
						"[WHITE]+===============================================================================================+\n" +
						"|[GREEN]	MSC			[YELLOW]VAZOM			[BLUE]HLR			[RED]SMS-GMSC[WHITE]	|\n" +
						"[WHITE]+===============================================================================================+\n" +
						"	|			|  [[BLUE]X1[WHITE]]|		 	|\n" + 
						"	|			|[BLUE]<<---------------------[WHITE]|                       |\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|[[YELLOW]X2[WHITE]]	|			|\n" + 
						"	|			|[YELLOW]--------------------->>[WHITE]|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|  [[BLUE]X3[WHITE]]|			|\n" + 
						"	|			|[BLUE]<<---------------------[WHITE]|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|  [[YELLOW]X4[WHITE]]|			|\n" + 
						"	|			|[YELLOW]--------------------->>[WHITE]|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|                          [[RED]X5[WHITE]]|\n" +
						"	|			|[RED]<<---------------------------------------------[WHITE]|\n" +
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|  [[YELLOW]X6[WHITE]]|			|			|\n" + 
						"	|[YELLOW]<<---------------------[WHITE]|			| 			|\n" +
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|  [[GREEN]X7[WHITE]]|			| 			|\n" +
						"	|[GREEN]--------------------->>[WHITE]|			| 			|\n" +
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|			|			|\n" + 
						"	|			|[[YELLOW]X8[WHITE]]	 			|\n" + 
						"	|			|[YELLOW]--------------------------------------------->>[WHITE]|\n" +
						"	|			|			|			|\n" +
						"	|			|			|			|\n" +
						"	|			|			|			|\n";
					res = "[BLUE]HPLMN Routing Flowchart:\n" + flow;
					res = res.replace("X1", String.format("%19s", StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REQUEST));
					
					res = res.replace("X2", String.format("%19s", StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_REQUEST));
					
					res = res.replace("X3", String.format("%19s", 	StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ACK + 
																	StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ERR));
					
					res = res.replace("X4", String.format("%19s", 	StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ACK + 
																	StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ERR + 
																	StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ABORT));
					
					res = res.replace("X5", String.format("%19s", 	StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_BEGIN + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_CONTINUE + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_END +
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_BEGIN +
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_CONTINUE +
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_END +
																	StatsManager.HPLMNR_STATS.HPLMNR_MO_IN));
					
					res = res.replace("X6", String.format("%19s", 	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_BEGIN + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_BEGIN +
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_CONTINUE +
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_END +
																	StatsManager.HPLMNR_STATS.HPLMNR_MO_OUT));
					
					res = res.replace("X7", String.format("%19s", 	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_ACK + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_ERR + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_TCAP_REPLY + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ACK + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ERR + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_TCAP_REPLY +
																	StatsManager.HPLMNR_STATS.HPLMNR_MO_OUT_REPLY));
					
					res = res.replace("X8", String.format("%19s", 	StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_ACK + 
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_ERR +
																	StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_TCAP_REPLY +
																	StatsManager.HPLMNR_STATS.HPLMNR_MO_IN_REPLY));
					if(pd.value != null){
						res += "[BLUE]HPLMN Routing Data:\n";
						res_data += "[WHITE]SCTP PACKET COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.SCTP_PACKET_COUNT) + "\n";
						res_data += "[WHITE]HPLMNR_NOT_ROUTABLE COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_NOT_ROUTABLE) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_IN_REQUEST COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REQUEST) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_OUT_REQUEST COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_REQUEST) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_OUT_ACK COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ACK) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_OUT_ERR COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ERR) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_IN_REPLY_ACK COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ACK) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_IN_REPLY_ERR COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ERR) + "\n";
						res_data += "[WHITE]HPLMNR_HLR_IN_REPLY_ABORT COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ABORT) + "\n";
						res_data += "[WHITE]HPLMNR_MT_IN_BEGIN COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_BEGIN) + "\n";
						res_data += "[WHITE]HPLMNR_MT_IN_CONTINUE COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_CONTINUE) + "\n";
						res_data += "[WHITE]HPLMNR_MT_IN_END COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_END) + "\n";
						res_data += "[WHITE]HPLMNR_MT_TCAP_IN_BEGIN COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_BEGIN) + "\n";
						res_data += "[WHITE]HPLMNR_MT_TCAP_IN_CONTINUE COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_CONTINUE) + "\n";
						res_data += "[WHITE]HPLMNR_MT_TCAP_IN_END COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_END) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_BEGIN COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_BEGIN) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_CONTINUE COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_END COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END) + "\n";
						res_data += "[WHITE]HPLMNR_MT_TCAP_OUT_BEGIN COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_BEGIN) + "\n";
						res_data += "[WHITE]HPLMNR_MT_TCAP_OUT_CONTINUE COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_CONTINUE) + "\n";
						res_data += "[WHITE]HPLMNR_MT_TCAP_OUT_END COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_END) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_CONTINUE_ACK COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ACK) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_CONTINUE_ERR COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ERR) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_CONTINUE_TCAP_REPLY COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_TCAP_REPLY) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_END_ACK COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_ACK) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_END_ERR COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_ERR) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_END_TCAP_REPLY COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_TCAP_REPLY) + "\n";
						res_data += "[WHITE]HPLMNR_MT_OUT_ABORT_REPLY COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_ABORT_REPLY) + "\n";
						res_data += "[WHITE]HPLMNR_MT_IN_ACK COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_ACK) + "\n";
						res_data += "[WHITE]HPLMNR_MT_IN_ERR COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_ERR) + "\n";
						res_data += "[WHITE]HPLMNR_MT_IN_TCAP_REPLY COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_TCAP_REPLY) + "\n";
						res_data += "[WHITE]HPLMNR_MO_IN COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MO_IN) + "\n";
						res_data += "[WHITE]HPLMNR_MO_OUT COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MO_OUT) + "\n";
						res_data += "[WHITE]HPLMNR_MO_OUT_REPLY COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MO_OUT_REPLY) + "\n";
						res_data += "[WHITE]HPLMNR_MO_IN_REPLY COUNT: [GREEN]" + Long.toString(StatsManager.HPLMNR_STATS.HPLMNR_MO_IN_REPLY) + "\n";
						res = res + Utils.str_align(res_data, ":");
						
					}
				}else res = "HPLMNR module is OFF!";
				return res;
				
			}
			return "";
		}

	}
	public CLI_show_stats_hplmnr _cli_show_stats_hplmnr = new CLI_show_stats_hplmnr();

	public class GRP_FGN extends GroupDescriptor {

		public GRP_FGN() {
			super("FGN", "Filtering Nodes ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_FGN _grp_FGN = new GRP_FGN();


	public class CLI_f_info extends MethodDescriptor {

		public CLI_f_info() {
			super("INFO", "Show Filtering Node List ", "FGN", "");
		}

		public String execute(Object[] params) {
			String res = "";
			FNDescriptor fnd = null;
			res += "[BLUE]Filtering Gateway Nodes:\n";
			for(Integer k : FNManager.fn_map.keySet()){
				fnd = FNManager.fn_map.get(k);
				res += "[WHITE]Node ID: [[GREEN]" + fnd.id+ "[WHITE]], client id: [[GREEN]" + fnd.sctp_id + "[WHITE]]!\n";  

			}
			return res;
		}

	}
	public CLI_f_info _cli_f_info = new CLI_f_info();

	public class GRP_CONF extends GroupDescriptor {

		public GRP_CONF() {
			super("CONF", "Configuration management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_CONF _grp_CONF = new GRP_CONF();


	public class CLI_conf_get extends MethodDescriptor {

		public CLI_conf_get() {
			super("GET", "Show configuration item ", "CONF", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				if(pd.value != null){
					String val = ConfigManagerV2.get(pd.value);
					if(val != null) return "[WHITE]CONF Item: [[GREEN]" + pd.value + "[WHITE]], value: [[GREEN]" + val + "[WHITE]]!";
				
				}				
			}
			return "";		
		}

	}
	public CLI_conf_get _cli_conf_get = new CLI_conf_get();

	public class CLI_conf_set extends MethodDescriptor {

		public CLI_conf_set() {
			super("SET", "Set configuration item ", "CONF", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				if(params.length >= 2){
					ParamDescriptor pd1 = (ParamDescriptor)params[0];
					ParamDescriptor pd2 = (ParamDescriptor)params[1];
					if(pd1.value != null && pd2.value != null){
						ConfigManagerV2.set(pd1.value, pd2.value, null);
						SGNConfigData.set(pd1.value, pd2.value);
						return "[WHITE]CONF Item: [[GREEN]" + pd1.value + "[WHITE]], value: [[GREEN]" + SGNConfigData.get(pd2.value) + "[WHITE]] set!";
					
					}				
				}
			}
			return "";
		}

	}
	public CLI_conf_set _cli_conf_set = new CLI_conf_set();

	public class CLI_conf_search extends MethodDescriptor {

		public CLI_conf_search() {
			super("SEARCH", "Search configuration ", "CONF", "conf_item");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				int max_l = 0;
				if(pd.value != null){
					String[] res_lst = SGNConfigData.search(pd.value);
					// max conf item length
					for(int i = 0; i<res_lst.length; i++) if(res_lst[i].length() > max_l) max_l = res_lst[i].length();
					String res = "";
					// result
					for(int i = 0; i<res_lst.length; i++) res += "[WHITE]" + String.format("%" + max_l + "s", res_lst[i]) + " = [[GREEN]" + SGNConfigData.get(res_lst[i]) + "[WHITE]]\n";
					return res;
					
				}
				
			}
			return "";
		}

	}
	public CLI_conf_search _cli_conf_search = new CLI_conf_search();

	public class GRP_M3UA extends GroupDescriptor {

		public GRP_M3UA() {
			super("M3UA", "M3UA Connections ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_M3UA _grp_M3UA = new GRP_M3UA();


	public class CLI_m3ua_info extends MethodDescriptor {

		public CLI_m3ua_info() {
			super("INFO", "Connection information ", "M3UA", "");
		}

		public String execute(Object[] params) {
			String res = "[BLUE]M3UA Connections:\n";
			M3UAConnection m3ua_conn = null;
			for(String s : M3UAConnManager.conn_map.keySet()){
				m3ua_conn = M3UAConnManager.conn_map.get(s);
				//res += Utils.ansi_box(m3ua_conn.label, "[WHITE]", "[GREEN]") + "\n";
				//res += "[WHITE] [[GREEN]ID = [YELLOW]" + m3ua_conn.label + "[WHITE]]\n";
				
				res += "[GREEN]                 ID = [YELLOW]" + m3ua_conn.label + "\n";
				res += "[GREEN]             ACTIVE = [YELLOW]" + m3ua_conn.active + "\n";
				res += "[GREEN]          BIND ADDR = [YELLOW]" + m3ua_conn.local_ip + "\n";
				res += "[GREEN]          BIND PORT = [YELLOW]" + m3ua_conn.local_port + "\n";
				res += "[GREEN]        REMOTE ADDR = [YELLOW]" + m3ua_conn.remote_ip + "\n";
				res += "[GREEN]        REMOTE PORT = [YELLOW]" + m3ua_conn.remote_port + "\n";
				res += "[GREEN]                DPC = [YELLOW]" + m3ua_conn.dpc + "\n";
				res += "[GREEN]                OPC = [YELLOW]" + m3ua_conn.opc + "\n";
				res += "[GREEN]        NETWORK APP = [YELLOW]" + m3ua_conn.network_app + "\n";
				res += "[GREEN]        ROUTING CTX = [YELLOW]" + m3ua_conn.routing_ctx + "\n";
				res += "[GREEN]  SCTP STREAM COUNT = [YELLOW]" + m3ua_conn.stream_count + "\n";
				res += "[GREEN] M3UA BEAT INTERVAL = [YELLOW]" + m3ua_conn.beat_interval + "\n";
				res += "[GREEN]MAX BEAT MISS COUNT = [YELLOW]" + m3ua_conn.max_beat_miss_count + "\n";
				res += "[GREEN]          DIRECTION = [YELLOW]" + m3ua_conn.direction + "\n";
				//res += "\n";
				res += "[BLUE]Bandwith summary:\n";

				// calculate stats
				long ts = System.currentTimeMillis();
				m3ua_conn.stats.AVG_PPS = (double)(m3ua_conn.stats.OUT + m3ua_conn.stats.IN)  / ((double)(ts - m3ua_conn.stats.ts) / 1000);
				m3ua_conn.stats.AVG_BYTES_S = (double)(m3ua_conn.stats.BYTES)  / ((double)(ts - m3ua_conn.stats.ts) / 1000);
				m3ua_conn.stats.AVG_MBIT_S = (double)(m3ua_conn.stats.AVG_BYTES_S * 8) / 1000000;
				
				
				res += "[WHITE]         Packets: [[GREEN]" + String.format("%,d", m3ua_conn.stats.OUT + m3ua_conn.stats.IN) + "[WHITE]]\n";
				res += "[WHITE]           Bytes: [[GREEN]" + String.format("%,d", m3ua_conn.stats.BYTES) + "[WHITE]]\n";
				res += "[WHITE]Avg. packets/sec: [[GREEN]" + String.format("%,.2f", m3ua_conn.stats.AVG_PPS) + "[WHITE]]\n";
				res += "[WHITE]  Avg. bytes/sec: [[GREEN]" + String.format("%,.2f", m3ua_conn.stats.AVG_BYTES_S) + "[WHITE]]\n";
				res += "[WHITE]   Avg. MBit/sec: [[GREEN]" + String.format("%,.2f", m3ua_conn.stats.AVG_MBIT_S) + "[WHITE]]\n";
				res += "[WHITE]   RX queue limit: [[RED]" + m3ua_conn.IN_QUEUE_MAX + "[WHITE]]\n";
				res += "[WHITE]   TX queue limit: [[RED]" + m3ua_conn.OUT_QUEUE_MAX + "[WHITE]]\n";
				res += "[BLUE]\n\n";

			}
			return res;
		}

	}
	public CLI_m3ua_info _cli_m3ua_info = new CLI_m3ua_info();

	public class CLI_m3ua_shut extends MethodDescriptor {

		public CLI_m3ua_shut() {
			super("SHUT", "Shutdown connection ", "M3UA", "conn_id:shut_sctp");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				boolean shut_sctp = false;
				if(pd2.value != null){
					if(pd2.value.equals("1")) shut_sctp = true;
				}
				M3UAConnection m3ua_conn = M3UAConnManager.shutConnection(pd1.value, shut_sctp);
				if(m3ua_conn != null) return "[WHITE]M3UA Connection [[GREEN]" + pd1.value + "[WHITE]] shutting down!";
				else return "[WHITE]M3UA Connection [[GREEN]" + pd1.value + "[WHITE]] does not exist!";

			}
			return "";
		}

	}
	public CLI_m3ua_shut _cli_m3ua_shut = new CLI_m3ua_shut();

	public class CLI_m3ua_noshut extends MethodDescriptor {

		public CLI_m3ua_noshut() {
			super("NOSHUT", "Reactivate connection ", "M3UA", "conn_id");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				M3UAConnection m3ua_conn = M3UAConnManager.noshutConnection(pd.value);
				if(m3ua_conn != null) return "[WHITE]M3UA Connection [[GREEN]" + pd.value + "[WHITE]] reinitializing!\n";
				else return "[WHITE]M3UA Connection [[GREEN]" + pd.value + "[WHITE]] does not exist!\n";
				
			}
			return "";
		}

	}
	public CLI_m3ua_noshut _cli_m3ua_noshut = new CLI_m3ua_noshut();

	public class CLI_m3ua_add extends MethodDescriptor {

		public CLI_m3ua_add() {
			super("ADD", "Add new connection ", "M3UA", "conn_id:bind_ip:bind_port:remote_ip:remote_port:dpc:opc:napp:rctx:sctp_sc:beat_interval:max_beat_miss:direction");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = null;
				// validate
				for(int i = 0; i<params.length - 1; i++){
					pd = (ParamDescriptor)params[i];
					if(pd.value == null) return "[RED]Missing parameter [[GREEN]" + pd.name + "[RED]]!";
				}
				// add connection
				M3UAConnManager.addConnection(((ParamDescriptor)params[0]).value, 
						((ParamDescriptor)params[0]).value, 
						((ParamDescriptor)params[1]).value, 
						Integer.parseInt(((ParamDescriptor)params[2]).value), 
						((ParamDescriptor)params[3]).value, 
						Integer.parseInt(((ParamDescriptor)params[4]).value),
						Integer.parseInt(((ParamDescriptor)params[5]).value), 
						Integer.parseInt(((ParamDescriptor)params[6]).value), 
						Integer.parseInt(((ParamDescriptor)params[7]).value), 
						Integer.parseInt(((ParamDescriptor)params[8]).value), 
						Integer.parseInt(((ParamDescriptor)params[9]).value), 
						Integer.parseInt(((ParamDescriptor)params[10]).value)*1000,
						Integer.parseInt(((ParamDescriptor)params[11]).value),
						RoutingConnectionDirectionType.valueOf(((ParamDescriptor)params[12]).value.toUpperCase().trim()));
				// update config file
				ConfigManagerV2.add_after(
											"m3ua." + ((ParamDescriptor)params[0]).value, 
											((ParamDescriptor)params[1]).value + ":" + 
											((ParamDescriptor)params[2]).value + ":" +
											((ParamDescriptor)params[3]).value + ":" +
											((ParamDescriptor)params[4]).value + ":" +
											((ParamDescriptor)params[5]).value + ":" +
											((ParamDescriptor)params[6]).value + ":" +
											((ParamDescriptor)params[7]).value + ":" +
											((ParamDescriptor)params[8]).value + ":" +
											((ParamDescriptor)params[9]).value + ":" + 
											((ParamDescriptor)params[10]).value + ":" + 
											((ParamDescriptor)params[11]).value + ":" +
											((ParamDescriptor)params[12]).value, 
											null, 
											"m3ua.connections");
				
				if(SGNConfigData.hplmnr_status) HPLMNRManager.addConverter(((ParamDescriptor)params[0]).value, RoutingConnectionType.M3UA);
				
				return "[WHITE]Adding M3UA Connection...";
				
			}
			return "";
		}

	}
	public CLI_m3ua_add _cli_m3ua_add = new CLI_m3ua_add();

	public class CLI_uptime_get extends MethodDescriptor {

		public CLI_uptime_get() {
			super("UPTIME", "Show running time ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]UPTIME: [GREEN]" + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.SYSTEM_STATS.STARTUP_TS);
		}

	}
	public CLI_uptime_get _cli_uptime_get = new CLI_uptime_get();

	public class CLI_version_get extends MethodDescriptor {

		public CLI_version_get() {
			super("VERSION", "Show system version ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]VAZOM VERSION: [[GREEN]" + SGNode.class.getPackage().getSpecificationVersion() + ".[YELLOW]" + SGNode.class.getPackage().getImplementationVersion() + "[WHITE]]";
		}

	}
	public CLI_version_get _cli_version_get = new CLI_version_get();

	public class CLI_bye_execute extends MethodDescriptor {

		public CLI_bye_execute() {
			super("BYE", "Disconnect ", "", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				conn.stopping = true;
			}
			return "";
		}

	}
	public CLI_bye_execute _cli_bye_execute = new CLI_bye_execute();

	public class CLI_help_execute extends MethodDescriptor {

		public CLI_help_execute() {
			super("HELP", "Show help ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]Help pending...";
		}

	}
	public CLI_help_execute _cli_help_execute = new CLI_help_execute();

	public class CLI_halt_execute extends MethodDescriptor {

		public CLI_halt_execute() {
			super("HALT", "Stop current node ", "", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.halt_counter >= 2){
					new Thread(){
						public void run(){
							try{ Thread.sleep(2000); }catch(Exception e){ e.printStackTrace(); }
							System.exit(0);
						}
					}.start();
					return "[RED]Node shutting down...";
				}else{
					conn.halt_counter++;
					return "[RED]Node shutdown initiated, please confirm by executing [[GREEN]HALT[RED]] for [[GREEN]" + (3 - conn.halt_counter) + "[RED]] more times!";
				}
			}			
			return "";
		}

	}
	public CLI_halt_execute _cli_halt_execute = new CLI_halt_execute();


    
	public CLIManager(){
		super();
		// methods
		method_lst.add(_cli_show_memory);
		method_lst.add(_cli_show_stats_system);
		method_lst.add(_cli_show_stats_vstp);
		method_lst.add(_cli_show_stats_smpp);
		method_lst.add(_cli_show_stats_pds);
		method_lst.add(_cli_show_stats_pdsd);
		method_lst.add(_cli_show_stats_hplmnr);
		method_lst.add(_cli_f_info);
		method_lst.add(_cli_conf_get);
		method_lst.add(_cli_conf_set);
		method_lst.add(_cli_conf_search);
		method_lst.add(_cli_m3ua_info);
		method_lst.add(_cli_m3ua_shut);
		method_lst.add(_cli_m3ua_noshut);
		method_lst.add(_cli_m3ua_add);
		method_lst.add(_cli_uptime_get);
		method_lst.add(_cli_version_get);
		method_lst.add(_cli_bye_execute);
		method_lst.add(_cli_help_execute);
		method_lst.add(_cli_halt_execute);

		// auth
        method_lst.add(_cli_user_list);
        method_lst.add(_cli_grp_list);
        method_lst.add(_cli_user_add);
        method_lst.add(_cli_user_set);
        method_lst.add(_cli_user_rm);
        method_lst.add(_cli_users_passwd);
		
		// smpp
		method_lst.add(_cli_smpp_info);
		method_lst.add(_cli_smpp_shut);
		method_lst.add(_cli_smpp_noshut);
		method_lst.add(_cli_smpp_add);     
        
        
		// groups
		group_lst.add(_grp_STATS);
		group_lst.add(_grp_SHOW);
		group_lst.add(_grp_FGN);
		group_lst.add(_grp_CONF);
		group_lst.add(_grp_M3UA);
		group_lst.add(_grp_SMPP);
		group_lst.add(_grp_AUTH);

	}
}
