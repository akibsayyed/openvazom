package net.isup.parameters;

import net.isup.ParameterType;

public class UserToUserInformation extends ParameterBase {
	public UserToUserInformation(){
		type = ParameterType.USER_TO_USER_INFORMATION;
	}
}
