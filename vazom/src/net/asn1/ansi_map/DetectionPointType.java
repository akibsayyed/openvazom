package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DetectionPointType extends ENUMERATED{

	public static final int _tDP_R = 1;
	public static final int _tDP_N = 2;
	public static final int _eDP_R = 3;
	public static final int _eDP_N = 4;
	public DetectionPointType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
