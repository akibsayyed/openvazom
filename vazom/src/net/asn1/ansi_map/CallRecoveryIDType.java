package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallRecoveryIDType extends CallRecoveryID{

	public CallRecoveryIDType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 303;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
