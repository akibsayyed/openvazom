package net.smsfs.cli;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import net.cli.CLIHistoryItem;
import net.config.SGNConfigData;
import net.smsfs.FilterDescriptor;
import net.utils.Utils;

import org.apache.log4j.Logger;

public class CLIConnection{
	public Logger logger=Logger.getLogger(CLIConnection.class);
	private Socket tcp;
	private ConcurrentHashMap<String, CLIConnection> conn_list;
	private Reader_r reader_r;
	private Thread reader_t;
	public boolean stopping;
	private BufferedReader in;
	private PrintWriter out;
	private OutputStream outb;
	private InputStream inb;
	private String con_ip;
	private int con_port;
	//private String uname;
	public String tmp_smsfs_filter = "";
	public ArrayList<FilterDescriptor> current_filter;
	
	private class Reader_r implements Runnable{
		private String cmd = "";
		private int last_cmd_index = 0;
		private ArrayList<String> last_cmd_lst = new ArrayList<String>();
		private String res;
		boolean history_found;
		CLIConnection current_conn;
		
		CLIManager cli = new CLIManager();
		ArrayList<CLIHistoryItem> his_lst = new ArrayList<CLIHistoryItem>();
		CLIHistoryItem his = null;
		
		public ArrayList<CLIHistoryItem> rebuildCLI(String cmd){
			return cli.processLine(cmd);
		}
		
		public Reader_r(CLIConnection conn){
			current_conn = conn;
		}
		
		public void run() {
			byte[] buff = new byte[512];
			byte[] buff_special = new byte[2];
			boolean special = false;
			int c = 0;
			int bc;
			logger.info("VAZOM CLI READER STARTED: [ " + con_ip + ":" + con_port + " ]");
			
			//out.print(new char[]{(char)0xff, (char)0xfc, (char)0x22});
			try{
				// IAC WILL ECHO IAC WILL SUPPRESS_GO_AHEAD IAC WONT LINEMODE
				outb.write(new byte[]{(byte)255, (byte)251, 1, (byte)255, (byte)251, 3, (byte)255, (byte)252, 34});
				out.flush();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				
			}catch(Exception e){}
			//out.flush();
			out.println();
			out.println();
			String vl = "^[[0;37m     ^[[0;1;37;47m  ^[[0;37m    ^[[0;5;37;47mS^[[0;37m ^[[0;1;37;47m;^[[0;5;37;47m8888%%^[[0;37m      ^[[0;1;37;47m%^[[0;37m  ^[[0;5;37;47mXX888@8^[[0;37m ^[[0;5;37;47m%888S8@8^[[0;37m     ^[[0m \n" + 
			"^[[0;37m     ^[[0;1;37;47m; ^[[0;37m   ^[[0;5;37;47m@^[[0;1;37;47m ^[[0;37m     ^[[0;5;37;47m%8:^[[0;37m    ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m   ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m   ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m ^[[0;5;37;47m ^[[0;37m   ^[[0;5;37;47m ^[[0;37m  ^[[0;5;37;47m8^[[0;37m     ^[[0m \n" +
			"^[[0;37m     ^[[0;1;37;47m;^[[0;5;37;47m8^[[0;37m  ^[[0;5;37;47m%^[[0;1;37;47m ^[[0;37m    ^[[0;5;37;47m88^[[0;37m  ^[[0;5;37;47m ^[[0;37m   ^[[0;5;37;47m%^[[0;1;37;47m ^[[0;37m     ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m  ^[[0;1;37;47m ^[[0;5;37;47m8^[[0;37m    ^[[0;5;37;47mt^[[0;37m   ^[[0;5;37;47m8^[[0;37m     ^[[0m \n" + 
			"^[[0;37m     ^[[0;1;37;47m;^[[0;5;37;47m88^[[0;1;37;47m ^[[0;37m     ^[[0;5;37;47m8^[[0;37m    ^[[0;5;37;47m ^[[0;37m ^[[0;1;37;47m ^[[0;5;37;47m;888^[[0;1;37;47m. ^[[0;37m    ^[[0;1;37;47m ^[[0;5;37;47m8X8^[[0;37m ^[[0;5;37;47m88^[[0;1;37;47m ^[[0;37m    ^[[0;5;37;47m8^[[0;37m     ^[[0m \n";
			vl = vl.replaceAll("\\^\\[", "\u001b");
			vl = vl.replaceAll("\n", "\n" + (char)13);
			out.print(vl);

			out.println();
			out.println();
			out.print("\u001B[1;34m");
			out.println("Welcome to VAZOM SMSFS CLI!" + (char)13);
			out.print("\u001B[0m");
			out.println("\u001B[1;37mType '\u001B[1;32mHELP\u001B[1;37m' for help!" + (char)13);
			out.print("\u001B[1;33m");
			out.print("\u001B[1;33mVAZOM-SMSFS[\u001B[1;34m" + SGNConfigData.sgn_id + "\u001B[1;33m]> ");
			out.print("\u001B[0m");
			out.print("\u001B7"); // save cursor pos
			out.flush();
			while(!stopping){
				try{
					bc = inb.read(buff);
					for(int i = 0; i<bc; i++){
						c = buff[i];
						if(c == 13){
							// echo
							out.println();
							out.print((char)13);
							out.flush();

							for(int j = 0; j<last_cmd_lst.size(); j++) if(last_cmd_lst.get(j).equals(cmd)){
								history_found = true;
								last_cmd_index = j;
							}
							if(!history_found){
								last_cmd_lst.add(cmd);
								last_cmd_index = last_cmd_lst.size() - 1;
							}
							history_found = false;
							his_lst = rebuildCLI(cmd);
							res = "";
							if(his_lst.size() > 0){
								Object[] tmp_params;
								his = his_lst.get(his_lst.size() - 1);
								if(his.descriptor != null) {
									if(his.params != null){
										tmp_params = new Object[his.params.size() + 1];
										for(int j = 0; j<his.params.size(); j++) tmp_params[j] = his.params.get(j);
										tmp_params[his.params.size()] = this.current_conn;
										res += Utils.ansi_paint(his.descriptor.execute(tmp_params));
									}
									else{
										tmp_params = new Object[1];
										tmp_params[0] = this.current_conn;
										res += Utils.ansi_paint(his.descriptor.execute(tmp_params));
									}
								}
								
							}			
							if(res != null) out.println(res.replaceAll("\n", "\n" + (char)13) + (char)13);
							out.print("\u001B[1;33m");
							out.print("\u001B[1;33mVAZOM-SMSFS[\u001B[1;34m" + SGNConfigData.sgn_id + "\u001B[1;33m]> ");
							out.print("\u001B[0m");
							out.print("\u001B7"); // save cursor pos
							res = "";
							out.flush();
							cmd = "";
						}else{
							// start of special key
							if(c == 0x1b){
								special = true;
								
							// tab
							}else if(c == 9){
								String[] tab_process = cli.tab_process(cmd);
								cmd = tab_process[0];
								out.println(tab_process[1].replaceAll("\n", "\n" + (char)13) + (char)13);
								
								out.print("\u001B[1;33m");
								out.print("\u001B[1;33mVAZOM-SMSFS[\u001B[1;34m" + SGNConfigData.sgn_id + "\u001B[1;33m]> \u001B[0m" + tab_process[0]);
								out.print("\u001B[0m");
								out.print("\u001B7"); // save cursor pos
								out.flush();
								
							// bye
							}else if(c == 4){
								stopping = true;
							// backspace
							}else if(c == 127){
								if(cmd.length() > 0){
									//System.out.println("AAA");
									cmd = cmd.substring(0, cmd.length() - 1);
									out.print("\u001B[1D");
									out.print("\u001B[K");
									out.flush();
								}
							// clear
							}else if(c == 12){
								out.print("\u001B[2J");
								out.print("\u001B[0;0H");
								out.print("\u001B[1;33m");
								out.print("\u001B[1;33mVAZOM-SMSFS[\u001B[1;34m" + SGNConfigData.sgn_id + "\u001B[1;33m]> ");
								out.print("\u001B[0m");
								out.print(cmd);
								out.print("\u001B7"); // save cursor pos
								out.print("\u001B[0m");
								out.flush();
							// normal key
							}else if(c != 0){
								// special started
								if(special){
									if(buff[i] == 0x5b) buff_special[0] = 0x5b;
									else if(buff[i] == 0x41) buff_special[1] = 0x41;
									else if(buff[i] == 0x42) buff_special[1] = 0x42;
									else special = false;
									
									
									// up key
									if(buff_special[0] == 0x5b && buff_special[1] == 0x41 && last_cmd_lst.size() > 0){
										if(last_cmd_index < 0 || last_cmd_index >= last_cmd_lst.size()) last_cmd_index = last_cmd_lst.size() - 1;
										//System.out.println("LAST: " + last_cmd_lst.get(last_cmd_index));
										
										out.print("\u001B8"); // get cursor pos
										out.print("\u001B[K"); //clear line from cursor right
										
										out.print(last_cmd_lst.get(last_cmd_index));
										out.flush();
										//System.out.println("AAA");
										cmd = last_cmd_lst.get(last_cmd_index--);
										c = 0;
										special = false;
										buff_special[0] = -1;
										buff_special[1] = -1;
											
									// down key
									}else if(buff_special[0] == 0x5b && buff_special[1] == 0x42 && last_cmd_lst.size() > 0){
										if(last_cmd_index >= last_cmd_lst.size() || last_cmd_index < 0) last_cmd_index = 0;
										//System.out.println(last_cmd_index);
										
										out.print("\u001B8"); // get cursor pos
										out.print("\u001B[K"); //clear line from cursor right
										
										out.print(last_cmd_lst.get(last_cmd_index));
										out.flush();
										//System.out.println("AAA");
										cmd = last_cmd_lst.get(last_cmd_index++);
										c = 0;
										special = false;
										buff_special[0] = -1;
										buff_special[1] = -1;
	
									}
								// normal keuy
								}else{
									special = false;
									cmd += (char)c;
									// echo
									out.print((char)c);
									out.flush();
									
								}
							}
						}
					}
					
				}catch(SocketTimeoutException e){
					logger.error("CONNECTION TIMEOUT! [ " + con_ip + ":" + con_port + " ]");
					//e.printStackTrace();
					stopping = true;
					
				}catch(Exception e){
					logger.error("ERROR WHILE READING INPUT STREAM FOR CLI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
					e.printStackTrace();
					stopping = true;
					
				}
				
			}
			logger.info("CLI READER STOPPED: [ " + con_ip + ":" + con_port + " ]");
			logger.info("CLOSING CLI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
			conn_list.remove(con_ip + ":" + con_port); 
			try{ tcp.close(); }catch(Exception e){}
			
		}
		
		
	}
	private class Writer_r implements Runnable{

		public void run() {
			while(!stopping){
				
				
			}
			
		}
		
		
	}
	
	private void init_threads(){
		reader_r = new Reader_r(this);
		//writer_r = new Writer_r();
		
		reader_t = new Thread(reader_r, "CLI_CONN_R_" + con_ip + ":" + con_port);
		//writer_t = new Thread(writer_r, "CLI_CONN_W_" + tcp.getInetAddress().toString() + ":" + tcp.getPort());
		
		reader_t.start();
		//writer_t.start();
		
		
	}
	
	
	
	public CLIConnection(Socket socket, ConcurrentHashMap<String, CLIConnection> _conn_list){
		tcp = socket;
		try{
			tcp.setSoTimeout(300000);
			in = new BufferedReader(new InputStreamReader(tcp.getInputStream()));
			inb = tcp.getInputStream();
			out = new PrintWriter(tcp.getOutputStream());
			outb = tcp.getOutputStream();
			con_ip = tcp.getInetAddress().toString();
			conn_list = _conn_list;
			con_port = tcp.getPort();

			current_filter = new ArrayList<FilterDescriptor>();

			
			init_threads();
		}catch(Exception e){
			logger.error("ERROR WHILE INITIALIZING INPUT/OUTPUT STREAMS FOR CLI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
}
