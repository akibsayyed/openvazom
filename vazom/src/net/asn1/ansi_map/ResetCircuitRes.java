package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ResetCircuitRes extends SET{

	public TrunkStatus get_trunkStatus(){
		return (TrunkStatus)elements.get(0).data;
	}
	public TrunkStatus new_trunkStatus(){
		return new TrunkStatus();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(16, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_trunkStatus(); }});
	}
	public ResetCircuitRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
