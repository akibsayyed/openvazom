package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ExtensionContainer extends SEQUENCE{

	public PrivateExtensionList get_privateExtensionList(){
		return (PrivateExtensionList)elements.get(0).data;
	}
	public PrivateExtensionList new_privateExtensionList(){
		return new PrivateExtensionList();
	}

	public PCS_Extensions get_pcs_Extensions(){
		return (PCS_Extensions)elements.get(1).data;
	}
	public PCS_Extensions new_pcs_Extensions(){
		return new PCS_Extensions();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_privateExtensionList(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pcs_Extensions(); }});
	}
	public ExtensionContainer(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
