package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventParameterSequenceOf extends SEQUENCE{

	public EventParameter new_child(){
		return new EventParameter();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public EventParameter getChild(int index){
		return (EventParameter)of_children.get(index);
	}
	public EventParameterSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
