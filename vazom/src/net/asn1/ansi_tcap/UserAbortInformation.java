package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UserAbortInformation extends EXTERNAL{

	public void initElements(){
	}
	public UserAbortInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 24;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
