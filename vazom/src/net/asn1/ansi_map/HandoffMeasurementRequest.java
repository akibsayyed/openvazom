package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffMeasurementRequest extends SET{

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(0).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(1).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public StationClassMark get_stationClassMark(){
		return (StationClassMark)elements.get(2).data;
	}
	public StationClassMark new_stationClassMark(){
		return new StationClassMark();
	}

	public TargetCellIDList get_targetCellIDList(){
		return (TargetCellIDList)elements.get(3).data;
	}
	public TargetCellIDList new_targetCellIDList(){
		return new TargetCellIDList();
	}

	public TDMACallMode get_tdmaCallMode(){
		return (TDMACallMode)elements.get(4).data;
	}
	public TDMACallMode new_tdmaCallMode(){
		return new TDMACallMode();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(5).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TDMATerminalCapability get_tdmaTerminalCapability(){
		return (TDMATerminalCapability)elements.get(6).data;
	}
	public TDMATerminalCapability new_tdmaTerminalCapability(){
		return new TDMATerminalCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_stationClassMark(); }});
		elements.add(new ElementDescriptor(207, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetCellIDList(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaCallMode(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(179, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaTerminalCapability(); }});
	}
	public HandoffMeasurementRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
