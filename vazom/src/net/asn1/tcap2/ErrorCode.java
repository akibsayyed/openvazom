package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ErrorCode extends CHOICE{

	public INTEGER get_nationaler(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_nationaler(){
		return new INTEGER();
	}

	public INTEGER get_privateer(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_privateer(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(19, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_nationaler(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_privateer(); }});
	}
	public ErrorCode(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
