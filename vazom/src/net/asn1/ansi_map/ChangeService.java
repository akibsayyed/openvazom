package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ChangeService extends SET{

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(0).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(1).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public ChangeServiceAttributes get_changeServiceAttributes(){
		return (ChangeServiceAttributes)elements.get(2).data;
	}
	public ChangeServiceAttributes new_changeServiceAttributes(){
		return new ChangeServiceAttributes();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(3).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public ISLPInformation get_ilspInformation(){
		return (ISLPInformation)elements.get(4).data;
	}
	public ISLPInformation new_ilspInformation(){
		return new ISLPInformation();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(5).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public TDMABandwidth get_tdmaBandwidth(){
		return (TDMABandwidth)elements.get(6).data;
	}
	public TDMABandwidth new_tdmaBandwidth(){
		return new TDMABandwidth();
	}

	public TDMADataMode get_tdmaDataMode(){
		return (TDMADataMode)elements.get(7).data;
	}
	public TDMADataMode new_tdmaDataMode(){
		return new TDMADataMode();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(8).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TDMAVoiceMode get_tdmaVoiceMode(){
		return (TDMAVoiceMode)elements.get(9).data;
	}
	public TDMAVoiceMode new_tdmaVoiceMode(){
		return new TDMAVoiceMode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(214, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_changeServiceAttributes(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(217, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ilspInformation(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(220, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBandwidth(); }});
		elements.add(new ElementDescriptor(222, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaDataMode(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(223, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaVoiceMode(); }});
	}
	public ChangeService(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
