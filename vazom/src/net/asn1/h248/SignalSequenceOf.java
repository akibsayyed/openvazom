package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SignalSequenceOf extends SEQUENCE{

	public Signal new_child(){
		return new Signal();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public Signal getChild(int index){
		return (Signal)of_children.get(index);
	}
	public SignalSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
