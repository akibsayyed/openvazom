
/*
 SPcap - Simple Packet capture
 v1.0
  - requires libspcap.so library
 */

package net.spcap;

import java.util.Arrays;

import net.spcap.vlan.VLANHeader;
import net.spcap.vlan.VLANPriority;
import net.utils.Utils;

public class SPcap {
	static{ System.loadLibrary("spcap"); }

	
	public static native void init();
	public static native String[] getDevices();
	public static native int initCapture(String device, int snaplen, int max_q_size);
	public static native void setBPF(String filter, int id);
	public static native void stopCapture(int id);
	public static native void startCapture(int id);
	public static native CapStats getStats(int id);
	public static native byte[] getPacket(int id);
	
	
	// Linux Cooked Capture Header - SLL
	public static SLLHeader getSLLHeader(byte[] data){
		try{
			SLLHeader sll = new SLLHeader();
			sll.packetType = ((data[0]  & 0xff)<< 8) + (data[1] & 0xff);
			sll.addressType = ((data[2]  & 0xff)<< 8) + (data[3] & 0xff);
			sll.addressLength = ((data[4]  & 0xff)<< 8) + (data[5] & 0xff);
			sll.source = Arrays.copyOfRange(data, 6, 14);
			sll.protocol = ((data[14]  & 0xff)<< 8) + (data[15] & 0xff);
			sll.payload = Arrays.copyOfRange(data, 16, data.length);
			return sll;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static VLANHeader getVLANHeader(byte[] data){
		try{
			VLANHeader vlanh = new VLANHeader();
			vlanh.priority = VLANPriority.get(data[0] & 0xe0);
			vlanh.cfi_indicator = (data[0] & 0x10) == 0x10;
			vlanh.vlan_identifier = ((data[0] & 0x0f) << 8) + (data[1] & 0xff);
			vlanh.ether_type = EtherType.get(((data[2] & 0xff) << 8) + (data[3] & 0xff));
			vlanh.payload = Arrays.copyOfRange(data, 4, data.length);
			return vlanh;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	public static ETHHeader getETHHeader(byte[] data){
		try{
			ETHHeader ethh = new ETHHeader();
			ethh.destination = Arrays.copyOfRange(data, 0, 6);
			ethh.source = Arrays.copyOfRange(data, 6, 12);
			ethh.ether_type = EtherType.get(((data[12] & 0xff) << 8) + (data[13] & 0xff));
			ethh.payload = Arrays.copyOfRange(data, 14, data.length);
			return ethh;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static TCPHeader getTCPHeader(byte[] data){
		try{
			TCPHeader tcph = new TCPHeader();
			tcph.source = ((data[0] & 0xff) << 8) + (data[1] & 0xff);
			tcph.destination = ((data[2] & 0xff) << 8) + (data[3] & 0xff);
			tcph.payload = Arrays.copyOfRange(data, ((data[12] & 0xff) >> 4)* 32 / 8, data.length); 
			return tcph;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	public static IPHeader getIPHeader(byte[] data){
		try{
			IPHeader iph = new IPHeader();
			// last 4 bits are header length in 32bit words
			iph.headerLength = (data[0] & 0x0f)* 32 / 8;
			// first four bits are IP version, we are shifting them 4 bits to the right
			iph.vesion = data[0] >> 4;
			//IP total length(2 bytes) = IP header length + all the other following headers
			iph.totalLength = ((data[2] & 0xff) << 8) + (data[3] & 0xff);
			iph.identification = ((data[4] & 0xff) << 8) + (data[5] & 0xff);
			iph.do_not_fragment = (data[6] & 0x40) == 0x40;
			iph.more_fragments = (data[6] & 0x20) == 0x20;
			iph.fragment_offset = (((data[6] & 0x1f) << 8) + (data[7] & 0xff)) * 8;
			iph.protocol = IPProtocolType.get(data[9] & 0xff);
			iph.source = Utils.bytes2IPv4(Arrays.copyOfRange(data, 12, 16));
			iph.destination = Utils.bytes2IPv4(Arrays.copyOfRange(data, 16, 20));
			iph.payload = Arrays.copyOfRange(data, iph.headerLength, iph.headerLength + (iph.totalLength - iph.headerLength));
			return iph;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
	
	
	public static byte[] getPayload(byte[] data){
		try{
			byte[] res = null;
			int pl_start;
			int ipl = (data[14] & 0x0F) * 32 / 8;
			pl_start = 14 + ipl;
			int pl_l = ((data[16] << 8) + (data[17] & 0xFF) & 0xFFFF) - ipl;
			//System.out.println(pl_l);
			res = Arrays.copyOfRange(data, pl_start, pl_start + pl_l);
			return res;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}


}
