package net.sctp;

import java.util.ArrayList;

import net.sctp.chunk.ChunkBase;

public class SCTPPacket {
	public int sourcePort;
	public int destinationPort;
	public byte[] verificationTag;
	public byte[] checksum;
	public ArrayList<ChunkBase> chunks;
	
	
	public SCTPPacket(){
		chunks = new ArrayList<ChunkBase>();
	}
}
