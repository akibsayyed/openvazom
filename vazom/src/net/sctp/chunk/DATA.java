package net.sctp.chunk;

import java.util.Arrays;

import net.sctp.ChunkType;
import net.sctp.chunk.data.PayloadProtocolType;

public class DATA extends ChunkBase {
	public byte[] tsn;
	public int streamIdentifier;
	public int sequenceNumber;
	public PayloadProtocolType payloadProtocolType;
	public byte[] userData;

	public DATA(){
		type = ChunkType.DATA;
	}

	public void decode(byte[] data){
		super.decode(data);
		// chunk with no data has length of 4
		if(length > 4){
			tsn = Arrays.copyOfRange(data, byte_pos, byte_pos + 4);
			byte_pos += 4;
			streamIdentifier = ((data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF)) & 0xFFFF;
			sequenceNumber = ((data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF)) & 0xFFFF;
			payloadProtocolType = PayloadProtocolType.get(((data[byte_pos++] << 24) + (data[byte_pos++] << 16) + (data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF)) & 0xFFFF);
			userData = Arrays.copyOfRange(data, byte_pos, byte_pos + length - 16);
			
		}
		//System.out.println(sequenceNumber);
		//System.out.println(((data[byte_pos++] << 24) + (data[byte_pos++] << 16) + (data[byte_pos++] << 8) + (data[byte_pos++] & 0xFF)) & 0xFFFF);
		
		//System.out.println(data.length);
		//System.out.println(payloadProtocolType);
		//System.out.println(length);
		//System.out.println("BP: " + byte_pos);
		
		//for(int i = 0; i<userData.length; i++) System.out.print(String.format("%02x", userData[i]) + ":");
		//System.out.println();
		
		
		
		
	}
	
}
