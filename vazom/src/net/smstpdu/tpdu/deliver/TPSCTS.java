package net.smstpdu.tpdu.deliver;

import net.utils.Utils;


public class TPSCTS {
	public int year;
	public int month;
	public int day;
	public int hour;
	public int minute;
	public int second;
	public int time_zone;

	
	public byte[] encode(){
		byte[] res = new byte[7];
		res[0] = Utils.semiOctetEncode(year);
		res[1] = Utils.semiOctetEncode(month);
		res[2] = Utils.semiOctetEncode(day);
		res[3] = Utils.semiOctetEncode(hour);
		res[4] = Utils.semiOctetEncode(minute);
		res[5] = Utils.semiOctetEncode(second);
		if(time_zone < 0){
			res[6] = (byte)(((time_zone * 4) << 4) + 0x08);
		}else{
			res[6] = (byte)((time_zone * 4) << 4);
		}
		return res;
	}
	public static TPSCTS decode(byte[] data){
		TPSCTS res = new TPSCTS();
		res.year = Utils.semiOctetDecode(data[0]); 
		res.month = Utils.semiOctetDecode(data[1]); 
		res.day = Utils.semiOctetDecode(data[2]);  
		res.hour = Utils.semiOctetDecode(data[3]); 
		res.minute = Utils.semiOctetDecode(data[4]); 
		res.second = Utils.semiOctetDecode(data[5]); 
		if((data[6] & 0x08) == 0x00){
			res.time_zone = ((data[6] & 0xf0) >> 4) / 4; 
			
		}else{
			res.time_zone = ((data[6] & 0xf0) >> 4) / -4; 
			
		}
		return res;
		
	}
}
