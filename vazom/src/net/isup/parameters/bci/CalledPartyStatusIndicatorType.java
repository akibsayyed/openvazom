package net.isup.parameters.bci;

import java.util.HashMap;

public enum CalledPartyStatusIndicatorType {
	NO_INDICATION(0x00),
	SUBSCRIBER_FREE(0x04),
	CONNECT_WHEN_FREE(0x08);
	
	
	private int id;
	private static final HashMap<Integer, CalledPartyStatusIndicatorType> lookup = new HashMap<Integer, CalledPartyStatusIndicatorType>();
	static{
		for(CalledPartyStatusIndicatorType td : CalledPartyStatusIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CalledPartyStatusIndicatorType get(int id){ return lookup.get(id); }
	private CalledPartyStatusIndicatorType(int _id){ id = _id; }
}
