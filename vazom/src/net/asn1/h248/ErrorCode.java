package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ErrorCode extends INTEGER{

	public ErrorCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
