package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CCBS_SubscriberStatus extends ENUMERATED{

	public static final int _ccbsNotIdle = 0;
	public static final int _ccbsIdle = 1;
	public static final int _ccbsNotReachable = 2;
	public CCBS_SubscriberStatus(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
