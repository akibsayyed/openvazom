package net.smpp.pdu;

import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.smpp.parameters.Data_codingType;
import net.smpp.parameters.GSMNetworkSpecific;
import net.smpp.parameters.MessageMode;
import net.smpp.parameters.MessageType;
import net.smpp.parameters.ServiceType;
import net.utils.Utils;

public class Data_sm extends PDUBase {
	public ServiceType service_type;
	public TypeOfNumber source_addr_ton;
	public NumberingPlan source_addr_npi;
	public String source_addr;
	public TypeOfNumber dest_addr_ton;
	public NumberingPlan dest_addr_npi;
	public String destination_addr;
	public MessageMode message_mode;
	public MessageType message_type;
	public GSMNetworkSpecific ns_features;
	public int registered_delivery;
	public Data_codingType data_coding;

	public Data_sm(){
		type = PDUType.DATA_SM;
	}
	public void init(byte[] data) {
		// Service type
		if(Utils.Cstr_decode(data, byte_pos).equals("")){
			service_type = ServiceType.DEFAULT;
			byte_pos++;
		}
		else{
			service_type = ServiceType.valueOf(Utils.Cstr_decode(data, byte_pos));
			byte_pos += service_type.toString().length() + 1;
		}
		// Source addr ton
		source_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Source addr npi
		source_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Source addr
		source_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += source_addr.length() + 1;
		// Dest addr ton
		dest_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Dest addr npi
		dest_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Dest addr
		destination_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += destination_addr.length() + 1;
		// Message mode
		message_mode = MessageMode.get(data[byte_pos] & 0x03);
		// Message type
		message_type = MessageType.get(data[byte_pos] & 0x3c);
		// Network specific features
		ns_features = GSMNetworkSpecific.get(data[byte_pos] & 0xc0);
		byte_pos++;
		// registered delivery
		registered_delivery = data[byte_pos++] & 0xff;
		// data coding
		data_coding = Data_codingType.get(data[byte_pos++] & 0xff);

		// optional
		initOptional(data);

		
	}

}
