package net.smstpdu;

import java.util.HashMap;


public enum TPValidityPeriodFormat {
	TP_VP_Not_Present(0x00),
	TP_VP_Relative(0x10),
	TP_VP_Enhanced(0x08),
	TP_VP_Absolute(0x18);
	
	
	// BITS 4 and 3 = 000XX000
	private int id;
	private static final HashMap<Integer, TPValidityPeriodFormat> lookup = new HashMap<Integer, TPValidityPeriodFormat>();
	static{
		for(TPValidityPeriodFormat td : TPValidityPeriodFormat.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static TPValidityPeriodFormat get(int id){ return lookup.get(id); }
	private TPValidityPeriodFormat(int _id){ id = _id; }
	
}
