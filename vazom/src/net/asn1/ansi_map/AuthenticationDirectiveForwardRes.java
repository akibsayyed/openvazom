package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationDirectiveForwardRes extends SET{

	public CountUpdateReport get_countUpdateReport(){
		return (CountUpdateReport)elements.get(0).data;
	}
	public CountUpdateReport new_countUpdateReport(){
		return new CountUpdateReport();
	}

	public UniqueChallengeReport get_uniqueChallengeReport(){
		return (UniqueChallengeReport)elements.get(1).data;
	}
	public UniqueChallengeReport new_uniqueChallengeReport(){
		return new UniqueChallengeReport();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(138, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_countUpdateReport(); }});
		elements.add(new ElementDescriptor(124, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uniqueChallengeReport(); }});
	}
	public AuthenticationDirectiveForwardRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
