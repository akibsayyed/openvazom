package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class StreamMode extends ENUMERATED{

	public static final int _sendOnly = 0;
	public static final int _recvOnly = 1;
	public static final int _sendRecv = 2;
	public static final int _inactive = 3;
	public static final int _loopBack = 4;
	public StreamMode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
