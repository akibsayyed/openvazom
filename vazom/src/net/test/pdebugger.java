package net.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.util.ArrayList;

import net.asn1.ansi_map.OriginationRequest;
import net.asn1.ansi_tcap.PackageType;
import net.asn1.compiler.ASNType;
import net.asn1.compiler.Asn2Javav2;
import net.asn1.gsmmap2.LocationInfoWithLMSI;
import net.asn1.gsmmap2.RoutingInfoForSM_Res;
import net.asn1.gsmmap_ch.SendRoutingInfoArg;
import net.asn1.gsmmap_ch.SendRoutingInfoRes;
import net.asn1.h248.MegacoMessage;
import net.asn1.tcap2.Invoke;
import net.asn1.tcap2.TCMessage;
import net.ber.BerTranscoderv2;
import net.config.FNConfigData;
import net.db.DBManager;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.sccp.MessageType;
import net.sccp.SCCP;
import net.sccp.messages.MessageBase;
import net.sccp.messages.UDT_UnitData;
import net.sctp.SCTP;
import net.smsfs.list.ListManagerV2;
import net.smsfs.list.ListType;
import net.smsfs.list.lists.DictionaryList;
import net.smsfs.list.lists.SmsfsList;
import net.smstpdu.AddressString;
import net.smstpdu.GSMAlphabet;
import net.smstpdu.TBCD;
import net.spcap.ETHHeader;
import net.spcap.IPHeader;
import net.spcap.SPcap;
import net.spcap.ipfrag.IPFragmentationManager;
import net.stats.StatsManager;
import net.utils.Utils;

public class pdebugger {

	public static int int_test = 10;
	public static int test(){
		return ++int_test;
		
	}
	public static void main(String[] args) {
		String tstr = "this is a test message.";
		byte[] encoded = GSMAlphabet.encode(tstr, 0);
		System.out.println("Enc: " + Utils.bytes2hexstr(encoded, ""));
		System.out.println("Org: " + tstr);
		
		System.out.println("Dec: " + GSMAlphabet.decode(encoded, 23));
		
		System.out.println( GSMAlphabet.getPrePaddingBits(6));
		
		
		
		ArrayList<Object> test_arr = new ArrayList<Object>();
		long test_long = 10;
		test_arr.add(test_long);
		long test_long2 = (Long)test_arr.get(0);
		System.out.println(test_long2);
		System.exit(0);
		
			GSMAlphabet.decode(encoded, 23);
			try{
				Thread.sleep(2000);
				
			}catch(Exception e){}
		long ts = System.currentTimeMillis();
		for(int i = 0; i<5000; i++){
			GSMAlphabet.decode(encoded, 23);
		}
		System.out.println(System.currentTimeMillis() - ts);
		
		String decoded = GSMAlphabet.decode(encoded, 23);
		System.out.println(decoded);
		/*
		Asn2Javav2 cmpl = new Asn2Javav2();
		cmpl.setOutputOptions("net.asn1.h248", "/tmp/out");
		cmpl.compile("/tmp/h248v3.asn");
		cmpl.getDataTypes();
		cmpl.generate();
		System.exit(0);
		/*
		//byte[] test_data = Utils.strhex2bytes("0b590b5925d4fa4aec065e2300030094004ad8b20001c31e00000003010001010000008400060008000008fd02100071000008fd0000046");
/*
		FNConfigData.db_host = "127.0.0.1";
		FNConfigData.db_name = "vazom";
		FNConfigData.db_packet_size = 1000;
		FNConfigData.db_port = 3306;
		FNConfigData.db_username = "root";
		FNConfigData.db_password = "";
		FNConfigData.smsfs_lists_sync_interval = 5000;
		FNConfigData.smsfs_lists_max = 1000000;
		
		DBManager.init();
		DBManager.init_connections(10);

		ListManagerV2.init();
		SmsfsList sl = new SmsfsList("TEST", 10000);
		DictionaryList dl = new DictionaryList(10000);

		
		
		
		ListManagerV2.add_list(sl);
		ListManagerV2.add_list(dl);
		
		sl.add(new String("explicit test").getBytes(), false, true);
		dl.add(new String("explicit dict").getBytes(), false, true);
		
		try{
			Thread.sleep(6000);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		sl.remove(new String("explicit test").getBytes(), false);
		
		System.out.println(sl.exists(new String("explicit test").getBytes(), false));
		
		try{
			Thread.sleep(50000);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		System.exit(0);
*/
		//SCTP.decode(test_data);
		//ListManagerV2.init();
		//SmsfsList sl = new SmsfsList("TEST_LST", 10000);
		//String test_data = "test data string";
		//ListManagerV2.add_list(sl);
		//sl.add(test_data.getBytes(), false);
		//sl.add(test_data.getBytes(), false);
		//sl.remove(test_data.getBytes(), false);
		//sl.get_list(false);
		
		
		
		//System.out.println(ListManagerV2.get_list(ListType.SMSFS_LIST, "TEST_LST").get_list(false).size());
		
		
		//System.out.println(test());
		
		//System.exit(0);
		
		//File f = new File(args[0]);
		File f = new File("/tmp/1316430800495.raw");
		byte[] data = new byte[(int)f.length()];
		int i = 0;

		try{
			FileInputStream fi = new FileInputStream(f);
			int a;
			while( (a = fi.read()) != -1){
				data[i++] = (byte)a;
			}
		}catch(Exception e){
			//e.printStackTrace();
		}
		
		BerTranscoderv2 nber21 = new BerTranscoderv2();
		

		MegacoMessage nntcm1 = (MegacoMessage)nber21.decode(new MegacoMessage(), data);

		//		ASNType param =  nntcm1.get_begin().get_components().getChild(0).get_invoke().get_parameter();
		//ASNType param =  nntcm1.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_parameter();
		
		//SendRoutingInfoArg ra = (SendRoutingInfoArg)nber21.decode(new SendRoutingInfoArg(), param.getDataBytes());
		//SendRoutingInfoRes rr = (SendRoutingInfoRes)nber21.decode(new SendRoutingInfoRes(), param.getDataBytes());

		nber21.showTree(nber21.document, 0);
		//System.out.println(TBCD.decode(AddressString.decode(rr.get_extendedRoutingInfo().get_routingInfo().get_roamingNumber().value).digits));
		//System.out.println(TBCD.decode(AddressString.decode(rr.get_vmsc_Address().value).digits));
		//System.out.println(TBCD.decode(rr.get_imsi().value));

		//System.out.println(oreq.get_billingID().getHex());
		System.exit(0);
		
		
		
		byte[] frag1_data = Utils.file_get_contents("/tmp/frag1.raw");
		byte[] frag2_data = Utils.file_get_contents("/tmp/frag2.raw");
		byte[] frag3_data = Utils.file_get_contents("/tmp/frag3.raw");
		byte[] frag4_data = Utils.file_get_contents("/tmp/frag4.raw");
		byte[] frag5_data = Utils.file_get_contents("/tmp/frag5.raw");
		byte[] frag6_data = Utils.file_get_contents("/tmp/frag6.raw");
		
		
		StatsManager.init();
		IPFragmentationManager.init(10000);
		ETHHeader ethh = null;
		IPHeader iph = null;
		
		ethh = SPcap.getETHHeader(frag1_data);
		iph = SPcap.getIPHeader(ethh.payload);
		IPFragmentationManager.process(iph);		
		
		ethh = SPcap.getETHHeader(frag2_data);
		iph = SPcap.getIPHeader(ethh.payload);
		IPFragmentationManager.process(iph);		
		
		ethh = SPcap.getETHHeader(frag3_data);
		iph = SPcap.getIPHeader(ethh.payload);
		IPFragmentationManager.process(iph);		

		ethh = SPcap.getETHHeader(frag4_data);
		iph = SPcap.getIPHeader(ethh.payload);
		IPFragmentationManager.process(iph);		
		
		
		ethh = SPcap.getETHHeader(frag5_data);
		iph = SPcap.getIPHeader(ethh.payload);
		IPFragmentationManager.process(iph);		
		
		ethh = SPcap.getETHHeader(frag6_data);
		iph = SPcap.getIPHeader(ethh.payload);
		IPHeader tmp_iph = IPFragmentationManager.process(iph);
		Utils.file_save_contents("/tmp/full.raw", tmp_iph.payload);
		//System.out.println(Utils.bytes2hexstr(tmp_iph.payload, ""));
		
		
		
		System.exit(0);
		MessageBase mb1 = SCCP.decode(data);
		UDT_UnitData aaaa = (UDT_UnitData)mb1;
		System.out.println(Utils.getGT(aaaa.calledPartyAddress.globalTitle));
		System.out.println(Utils.getGT(aaaa.callingPartyAddress.globalTitle));
		//M3UA.decode(data);
		
		
		
		UDT_UnitData udt1 = (UDT_UnitData)mb1;
		BerTranscoderv2 ber21 = new BerTranscoderv2();
		TCMessage ntcm1 = (TCMessage)ber21.decode(new TCMessage(), aaaa.data);
		//ber21.showTree(ber21.document, 0);
		System.out.println(Utils.isReturnResultLast(ntcm1));
		// Dialogue 
		if(ntcm1.get_end().get_dialoguePortion() != null){
			byte[] tmp_oid = Utils.getTcapDialogueAppCtx(ntcm1.get_end().get_dialoguePortion());
			if(tmp_oid != null){
				System.out.println(Utils.decodeOID(tmp_oid));
			}
				
		}		
		System.out.println(Utils.isEndSRI(ntcm1));
		ASNType returnResultLast = Utils.get_RETURN_RESULT_LAST_RETRES_PARAMETER(ntcm1);
		//System.out.println(returnResultLast);
		RoutingInfoForSM_Res sm_res = (RoutingInfoForSM_Res)ber21.decode(new RoutingInfoForSM_Res(), returnResultLast.getDataBytes());
		LocationInfoWithLMSI li_wlmsi = sm_res.get_locationInfoWithLMSI();
		if(li_wlmsi.get_additional_Number() != null){
			if(li_wlmsi.get_additional_Number().get_msc_Number() != null){
				System.out.println(TBCD.decode(AddressString.decode(li_wlmsi.get_additional_Number().get_msc_Number().getValueBytes()).digits));
			}else if(li_wlmsi.get_additional_Number().get_sgsn_Number() != null){
				System.out.println(TBCD.decode(AddressString.decode(li_wlmsi.get_additional_Number().get_sgsn_Number().getValueBytes()).digits));
			}
			
		}
		System.out.println(li_wlmsi.get_networkNode_Number());
		
		System.exit(0);

		
		try{
			FileOutputStream foo = new FileOutputStream(new File("/tmp/err.err"));
			foo.write(udt1.data);
		}catch(Exception e){}
		
		
		
		//ber21.showTree(ber21.document, 0);
		//ntcm1.get_begin().get_components().getChild(0).get
		//System.out.println(Utils.isUDT(data));
		System.out.println(Utils.isReturnResultLast(ntcm1));
		System.out.println(Utils.getTcap_did(ntcm1));
		int invik = (int)Utils.bytes2num(Utils.get_INVOKE_ID(ntcm1));
		System.out.println(Utils.get_calling_gt_nai(udt1));
		Utils.isTcapAbort(ntcm1);
		System.out.println(udt1.calledPartyAddress.globalTitleIndicator.getId());
		System.exit(0);
		
		
		M3UAPacket m3ua = M3UA.decode(data);
		System.out.println("M3UA message type = [" + m3ua.message.type + "]");
		if(m3ua.message.type == M3UAMessageType.DATA){
			DATA m3ua_data = (DATA)m3ua.message;
			System.out.println("	|-> [M3UA DATA].DPC = [" + m3ua_data.protocolData.destinationPointCode + "]");
			System.out.println("	|-> [M3UA DATA].OPC = [" + m3ua_data.protocolData.originatingPointCode + "]");
			System.out.println("	|-> [M3UA DATA].SI  = [" + m3ua_data.protocolData.serviceIndicator + "]");
			
			if(m3ua_data.protocolData.serviceIndicator == ServiceIndicatorType.SCCP){
				MessageBase mb = SCCP.decode(m3ua_data.protocolData.userProtocolData);
				if(mb.type == MessageType.UDT_UNITDATA){
					System.out.println();
					System.out.println("SCCP messasge type = [" + mb.type + "]");
					UDT_UnitData udt = (UDT_UnitData)mb;
					System.out.println("	|-> [SCCP UNITDATA].Called GT  = [" + Utils.getGT(udt.calledPartyAddress.globalTitle) + "]");
					System.out.println("	|-> [SCCP UNITDATA].Calling GT = [" + Utils.getGT(udt.callingPartyAddress.globalTitle) + "]");
					System.out.println();
					if(udt.data != null){
						BerTranscoderv2 ber2 = new BerTranscoderv2();
						TCMessage ntcm = (TCMessage)ber2.decode(new TCMessage(), udt.data);
						System.out.println("TCAP message type");
						System.out.println("	|-> [TCAP].SID = [" + (Utils.getTcap_sid(ntcm) == -1 ? "" : Utils.getTcap_sid(ntcm)) + "]");
						System.out.println("	|-> [TCAP].DID = [" + (Utils.getTcap_did(ntcm) == -1 ? "" : Utils.getTcap_did(ntcm)) + "]");
						System.out.println();
						System.out.println("TCAP Structure:");
						ber2.showTree(ber2.document, 0);
						//System.out.println(Utils.bytes2num(ntcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_opCode().get_localValue().value));
						
					}
				}
				
				
			}
			
		}
	}

}
