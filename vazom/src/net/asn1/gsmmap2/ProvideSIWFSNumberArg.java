package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ProvideSIWFSNumberArg extends SEQUENCE{

	public ExternalSignalInfo get_gsm_BearerCapability(){
		return (ExternalSignalInfo)elements.get(0).data;
	}
	public ExternalSignalInfo new_gsm_BearerCapability(){
		return new ExternalSignalInfo();
	}

	public ExternalSignalInfo get_isdn_BearerCapability(){
		return (ExternalSignalInfo)elements.get(1).data;
	}
	public ExternalSignalInfo new_isdn_BearerCapability(){
		return new ExternalSignalInfo();
	}

	public CallDirection get_call_Direction(){
		return (CallDirection)elements.get(2).data;
	}
	public CallDirection new_call_Direction(){
		return new CallDirection();
	}

	public ISDN_AddressString get_b_Subscriber_Address(){
		return (ISDN_AddressString)elements.get(3).data;
	}
	public ISDN_AddressString new_b_Subscriber_Address(){
		return new ISDN_AddressString();
	}

	public ExternalSignalInfo get_chosenChannel(){
		return (ExternalSignalInfo)elements.get(4).data;
	}
	public ExternalSignalInfo new_chosenChannel(){
		return new ExternalSignalInfo();
	}

	public ExternalSignalInfo get_lowerLayerCompatibility(){
		return (ExternalSignalInfo)elements.get(5).data;
	}
	public ExternalSignalInfo new_lowerLayerCompatibility(){
		return new ExternalSignalInfo();
	}

	public ExternalSignalInfo get_highLayerCompatibility(){
		return (ExternalSignalInfo)elements.get(6).data;
	}
	public ExternalSignalInfo new_highLayerCompatibility(){
		return new ExternalSignalInfo();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(7).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_gsm_BearerCapability(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_isdn_BearerCapability(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_call_Direction(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_b_Subscriber_Address(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_chosenChannel(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lowerLayerCompatibility(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_highLayerCompatibility(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public ProvideSIWFSNumberArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
