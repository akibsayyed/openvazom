package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceDataResult extends SEQUENCE{

	public DataUpdateResultList get_dataUpdateResultList(){
		return (DataUpdateResultList)elements.get(0).data;
	}
	public DataUpdateResultList new_dataUpdateResultList(){
		return new DataUpdateResultList();
	}

	public ServiceID get_serviceID(){
		return (ServiceID)elements.get(1).data;
	}
	public ServiceID new_serviceID(){
		return new ServiceID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(255, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataUpdateResultList(); }});
		elements.add(new ElementDescriptor(246, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceID(); }});
	}
	public ServiceDataResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
