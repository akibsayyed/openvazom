package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudPropertyGroup extends SEQUENCE{

	public IndAudPropertyParm new_child(){
		return new IndAudPropertyParm();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public IndAudPropertyParm getChild(int index){
		return (IndAudPropertyParm)of_children.get(index);
	}
	public IndAudPropertyGroup(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
