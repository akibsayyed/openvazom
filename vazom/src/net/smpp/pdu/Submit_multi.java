package net.smpp.pdu;

import java.util.Arrays;

import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.smpp.parameters.Data_codingType;
import net.smpp.parameters.DestFlagType;
import net.smpp.parameters.GSMNetworkSpecific;
import net.smpp.parameters.IntermediateNotificationType;
import net.smpp.parameters.MessageMode;
import net.smpp.parameters.MessageType;
import net.smpp.parameters.SMEOrigAckType;
import net.smpp.parameters.SMSCDeliveryReceiptType;
import net.smpp.parameters.ServiceType;
import net.utils.Utils;

public class Submit_multi extends PDUBase {
	public class DestinationAddress{
		public DestFlagType type;
		public TypeOfNumber dest_addr_ton;
		public NumberingPlan dest_addr_npi;
		public String destination_addr;
		
	}
	public ServiceType service_type;
	public TypeOfNumber source_addr_ton;
	public NumberingPlan source_addr_npi;
	public String source_addr;
	public int number_of_dests;
	public DestinationAddress[] dest_address_lst;
	public MessageMode message_mode;
	public MessageType message_type;
	public GSMNetworkSpecific ns_features;
	public int protocol_id;
	public int priority_flag;
	public String schedule_delivery_time;
	public String validity_period;
	
	//public int registered_delivery;
	public SMSCDeliveryReceiptType rd_smsc_delivery_receipt;
	public SMEOrigAckType rd_sme_orig_ack;
	public IntermediateNotificationType rd_intermediate_notification;;
	
	public int replace_if_present_flag;
	public Data_codingType data_coding;
	public int sm_default_msg_id;
	public int sm_length;
	public byte[] short_message;

	public Submit_multi(){
		type = PDUType.SUBMIT_MULTI;
	}

	public void init(byte[] data) {
		DestinationAddress da = null;
		// Service type
		if(Utils.Cstr_decode(data, byte_pos).equals("")){
			service_type = ServiceType.DEFAULT;
			byte_pos++;
		}
		else{
			service_type = ServiceType.valueOf(Utils.Cstr_decode(data, byte_pos));
			byte_pos += service_type.toString().length() + 1;
		}
		// Source addr ton
		source_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Source addr npi
		source_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Source addr
		source_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += source_addr.length() + 1;
		// number of dests
		number_of_dests = data[byte_pos++] & 0xff;
		// dest address list
		dest_address_lst = new DestinationAddress[number_of_dests];
		if(number_of_dests > 0){
			for(int i = 0; i<number_of_dests; i++){
				da = new DestinationAddress();
				da.type = DestFlagType.get(data[byte_pos++] & 0xff);
				if(da.type == DestFlagType.SME_ADDRESS){
					da.dest_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
					da.dest_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
				}
				da.destination_addr = Utils.Cstr_decode(data, byte_pos);
				// add to list
				dest_address_lst[i] = da;
				byte_pos += da.destination_addr.length() + 1;
			}
			
		}
		// Message mode
		message_mode = MessageMode.get(data[byte_pos] & 0x03);
		// Message type
		message_type = MessageType.get(data[byte_pos] & 0x3c);
		// Network specific features
		ns_features = GSMNetworkSpecific.get(data[byte_pos] & 0xc0);
		byte_pos++;
		// protocol id
		protocol_id = data[byte_pos++] & 0xff;
		// priority flag
		priority_flag = data[byte_pos++] & 0xf;;
		// schedule delivery time
		schedule_delivery_time = Utils.Cstr_decode(data, byte_pos);
		byte_pos += schedule_delivery_time.length() + 1;
		// validity period
		validity_period = Utils.Cstr_decode(data, byte_pos);
		byte_pos += validity_period.length() + 1;
		
		// registered delivery
		//registered_delivery = data[byte_pos++] & 0xff;
		rd_smsc_delivery_receipt = SMSCDeliveryReceiptType.get(data[byte_pos] & 0x03);
		rd_sme_orig_ack = SMEOrigAckType.get(data[byte_pos] & 0x0c);
		rd_intermediate_notification = IntermediateNotificationType.get(data[byte_pos] & 0x10);
		byte_pos++;
		
		// replace if present flat
		replace_if_present_flag = data[byte_pos++] & 0xff;
		
		// data coding
		data_coding = Data_codingType.get(data[byte_pos++] & 0xff);
		
		// sm_default_msg_id
		sm_default_msg_id = data[byte_pos++] & 0xff;
		
		// sm_length
		sm_length = data[byte_pos++] & 0xff;
		
		// sm
		short_message = Arrays.copyOfRange(data, byte_pos, byte_pos + sm_length);
		byte_pos += sm_length;
		
		// optional
		initOptional(data);
		
	}

}
