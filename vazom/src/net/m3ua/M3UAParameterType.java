package net.m3ua;

import java.util.HashMap;
public enum M3UAParameterType {
	// Common parameters
	INFO_STRING(0x0004),
	ROUTING_CONTEXT(0x0006),
	DIAGNOSTIC_INFO(0x0007),
	HEARTBEAT(0x0009),
	TRAFFIC_MODE_TYPE(0x000b),
	ERROR_CODE(0x000c),
	STATUS(0x000d),
	ASP_IDENTIFIER(0x0011),
	AFFECTED_POINT_CODE(0x0012),
	CORRELATION_ID(0x0013),
	// m3ua specific
	NETWORK_APPEARANCE(0x0200),
	USER_CAUSE(0x0204),
	CONGESTION_INDICATIONS(0x0205),
	CONCERNED_DESTINATION(0x0206),
	ROUTING_KEY(0x0207),
	REGISTRATION_RESULT(0x0208),
	DEREGISTRATION_RESULT(0x0209),
	LOCAL_ROUTING_KEY_IDENTIFIER(0x020A),
	DESTINATION_POINT_CODE(0x020B),
	SERVICE_INDICATORS(0x020C),
	ORIGINATING_POINT_CODE_LIST(0x020E),
	CIRCUIT_RANGE(0x020F),
	PROTOCOL_DATA(0x0210),
	REGISTRATION_STATUS(0x0212),
	DEREGISTRATION_STATUS(0x0213);	
	
	
	private int id;
	private static final HashMap<Integer, M3UAParameterType> lookup = new HashMap<Integer, M3UAParameterType>();
	static{
		for(M3UAParameterType td : M3UAParameterType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static M3UAParameterType get(int id){ return lookup.get(id); }
	private M3UAParameterType(int _id){ id = _id; }	
}
