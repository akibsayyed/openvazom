package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallStatus extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _call_Setup_in_Progress = 1;
	public static final int _called_Party = 2;
	public static final int _locally_Allowed_Call_No_Action = 3;
	public CallStatus(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
