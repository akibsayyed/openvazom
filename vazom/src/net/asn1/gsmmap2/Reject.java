package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Reject extends SEQUENCE{

	public RejectinvokeIDRej get_invokeIDRej(){
		return (RejectinvokeIDRej)elements.get(0).data;
	}
	public RejectinvokeIDRej new_invokeIDRej(){
		return new RejectinvokeIDRej();
	}

	public Rejectproblem get_problem(){
		return (Rejectproblem)elements.get(1).data;
	}
	public Rejectproblem new_problem(){
		return new Rejectproblem();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeIDRej(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_problem(); }});
	}
	public Reject(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
