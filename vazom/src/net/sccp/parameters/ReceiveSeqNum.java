package net.sccp.parameters;

public class ReceiveSeqNum extends ParamBase {
	public int receiveSeqNum;
	
	public ReceiveSeqNum(){
		super();
		type = ParamType.RECEIVE_SEQUENCE_NUMBER;
	}
	
	public void init(byte[] data) {
		receiveSeqNum = (data[0] & 0xFE) >> 1;
	}

}
