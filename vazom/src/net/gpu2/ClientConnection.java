package net.gpu2;

import java.net.ConnectException;
import java.util.concurrent.ConcurrentHashMap;

import org.apache.log4j.Logger;

import net.config.GPUNConfigData;
import net.ds.DataSourceType;
import net.gpu.cuda.CudaCallback;
import net.gpu.cuda.KernelStats;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;

public class ClientConnection {
	public static Logger logger=Logger.getLogger(ClientConnection.class);

	public boolean connectNode(int _sctp_id){
		SSCTPDescriptor sd = null;
		boolean vstp_ready = false;
		int res;
		String pl_str;
		String tmp;
		logger.info("Negotiating VSTP Host Connection, client sctp id = [" + _sctp_id + "]!");
		if(_sctp_id > -1){
			// negotiate VSTP connect
			sd = SSctp.receive(_sctp_id);
			if(sd != null){
				if(sd.payload != null){
					pl_str = new String(sd.payload);
					if(VSTP.check_init(pl_str)){
						// node id
						tmp = VSTP.get_init_node_id(pl_str);
						id = tmp;
						res = SSctp.send(_sctp_id, VSTP.generate_ack(LocationType.GPUN, tmp).getBytes(), 0);
						if(res > -1){
							logger.info("VSTP Connection initialized, sctp id = [" + _sctp_id + "], node id = [" + id + "]!");
							vstp_ready = true;
						}
					}
				}
			}
			if(!vstp_ready){
				logger.warn("Error while negotiating VSTP Connection, removing FN Connection, sctp id = [" + _sctp_id + "]!");
				stopping = true;
				con_lst.remove(sctp_id); 
				SSctp.shutdownClient(sctp_id);
				return false;
				
			}else return true;
			
			
		}else{
			logger.warn("Error while initializing SCTP Connection, client id = [" + _sctp_id + "]!");
			stopping = true;
			con_lst.remove(sctp_id); 
			SSctp.shutdownClient(sctp_id);
			return false;
		}

	}	
	
	
	
	public class MD5Callback extends CudaCallback{
		public String dest_id;
		public String msg_id;
		
		public MD5Callback(String _dest_id, String _msg_id){
			dest_id = _dest_id;
			msg_id = _msg_id;
			
		}
		
		public void execute(byte[] data) {
			MessageDescriptor md = null;
			try{
				// check for error
				// if CUDA error encountered, data will be of length 1, and item 0 will be the error code
				int rep_c = 0;
				if(data.length == 1 && (data[0] == -100 || data[0] == -110)){
					md = new MessageDescriptor();
					md.header.destination = LocationType.FN;
					md.header.destination_id = dest_id;
					md.header.ds = DataSourceType.NA;
					md.header.msg_id = msg_id;
					md.header.msg_type = MessageType.SGC;
					md.header.source = LocationType.GPUN;
					md.header.source_id = GPUNConfigData.id;
					
					
					if(data[0] == -100){
						md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "CERR");
						//SSctp.send(sctp_id, data, sid)
						//out.println("CERR");
						//out.flush();
					}else if(data[0] == -110){
						md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "ERR110");
						//out.println("ERR110");
						//out.flush();
						
					}
					// send
					SSctp.send(sctp_id, md.encode(), sctp_sid);

				}else{
					for(int i = 0; i<data.length; i++) if(data[i] == 1) rep_c++;
					md = new MessageDescriptor();
					md.header.destination = LocationType.FN;
					md.header.destination_id = dest_id;
					md.header.ds = DataSourceType.NA;
					md.header.msg_id = msg_id;
					md.header.msg_type = MessageType.SGC;
					md.header.source = LocationType.GPUN;
					md.header.source_id = GPUNConfigData.id;
					md.values.put(VSTPDataItemType.GPU_RESULT.getId(), Integer.toString(rep_c));
					// send
					SSctp.send(sctp_id, md.encode(), sctp_sid);
					//out.println("R." + rep_c);
					//out.flush();
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
	}
	
	
	
	public class LDCallback extends CudaCallback{
		public String dest_id;
		public String msg_id;
		
		public LDCallback(String _dest_id, String _msg_id){
			dest_id = _dest_id;
			msg_id = _msg_id;
		}
		public void execute(byte[] data) {
			MessageDescriptor md = null;
			try{
				// check for error
				// if CUDA error encountered, data will be of length 1, and item 0 will be the error code
				int rep_c = 0;
				if(data.length == 1 && (data[0] == -100 || data[0] == -110)){
					md = new MessageDescriptor();
					md.header.destination = LocationType.FN;
					md.header.destination_id = dest_id;
					md.header.ds = DataSourceType.NA;
					md.header.msg_id = msg_id;
					md.header.msg_type = MessageType.SGC;
					md.header.source = LocationType.GPUN;
					md.header.source_id = GPUNConfigData.id;

					if(data[0] == -100){
						md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "CERR");
						//out.println("CERR");
						//out.flush();
					}else if(data[0] == -110){
						md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "ERR110");
						//out.println("ERR110");
						//out.flush();
						
					}
					// send
					SSctp.send(sctp_id, md.encode(), sctp_sid);
					
				}else{
					for(int i = 0; i<data.length; i++) if(data[i] >= GPUNConfigData.ld_threshold) rep_c++;
					md = new MessageDescriptor();
					md.header.destination = LocationType.FN;
					md.header.destination_id = dest_id;
					md.header.ds = DataSourceType.NA;
					md.header.msg_id = msg_id;
					md.header.msg_type = MessageType.SGC;
					md.header.source = LocationType.GPUN;
					md.header.source_id = GPUNConfigData.id;
					md.values.put(VSTPDataItemType.GPU_RESULT.getId(), Integer.toString(rep_c));
					// send
					SSctp.send(sctp_id, md.encode(), sctp_sid);

					//out.println("R." + rep_c);
					//out.flush();
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
	}
	
	public class Reader_r implements Runnable{
		public void run() {
			String[] tmp_lst;
			SSCTPDescriptor sd = null;
			MessageDescriptor md = null;
			String gpu_cmd = null;
			String gpu_param = null;
			GPUDataItemType gpu_item = null;
			KernelStats ks = null;
			boolean ready_to_send = false;
			int sctp_res;
			LDCallback ld_cb = null;
			MD5Callback md5_cb = null;
			while(!stopping){
				try{
					sd = SSctp.receive(sctp_id);
					if(sd != null){
						sctp_sid = sd.sid;
						if(sd.payload != null){
							md = VSTP.decode(sd.payload);
							// SGC - node communication
							// source = FN 
							if(md.header.msg_type == MessageType.SGC && md.header.source == LocationType.FN){
								gpu_cmd = md.values.get(VSTPDataItemType.GPU_CMD.getId());
								if(gpu_cmd != null){
									gpu_param = md.values.get(VSTPDataItemType.GPU_PARAM.getId());
									gpu_item = GPUDataItemType.valueOf(gpu_cmd);
									tmp_lst = gpu_param.split(":");
									md.values.clear();
									switch(gpu_item){
										case LD:
											ready_to_send = false;
											ld_cb = new LDCallback(md.header.source_id, md.header.msg_id);
											GPUNode.check(Integer.parseInt(tmp_lst[0]), Utils.strhex2bytes(tmp_lst[1]), ld_cb);
											break;
										case MD5:
											ready_to_send = false;
											md5_cb = new MD5Callback(md.header.source_id, md.header.msg_id);
											GPUNode.check(Integer.parseInt(tmp_lst[0]), Utils.strhex2bytes(tmp_lst[1]), md5_cb);
											break;
										case S:
											ready_to_send = true;
											ks = GPUNode.getStats(Integer.parseInt(tmp_lst[0]));
											md.values.put(VSTPDataItemType.GPU_RESULT.getId(), 	ks.execution_count + ":" + 
																								ks.last_execution_time + ":" + 
																								ks.max_execution_time + ":" + 
																								ks.update_q_size + ":" + 
																								ks.ld_q_size + ":" + 
																								ks.md5_q_size);
											break;
										case R:
											ready_to_send = true;
											GPUNode.remove(Integer.parseInt(tmp_lst[0]), Utils.strhex2bytes(tmp_lst[1]));
											md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "1");
											break;
										case RST:
											ready_to_send = true;
											GPUNode.reset(Integer.parseInt(tmp_lst[0]));
											md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "1");
											break;
										case U:
											ready_to_send = true;
											GPUNode.update(Integer.parseInt(tmp_lst[0]), Utils.strhex2bytes(tmp_lst[1]));
											md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "1");
											break;
										default:
											md.values.put(VSTPDataItemType.GPU_RESULT.getId(), "ERROR");
											ready_to_send = true;
											break;
									}
									
									if(ready_to_send){
										md.header.source = LocationType.GPUN;
										md.header.destination = LocationType.FN;
										md.header.destination_id = md.header.source_id;
										md.header.source_id = GPUNConfigData.id;
										// send
										sctp_res = SSctp.send(sctp_id, md.encode(), sctp_sid);
										if(sctp_res == -1) stopping = true;
										// reset
										ready_to_send = false;
									}
								}
							}
						}else throw new ConnectException();
					}else throw new ConnectException();

				}catch(ConnectException e){
					stopping = true;
				}catch(Exception e){
					e.printStackTrace();
				}
			}
			logger.info("Closing connection, sctp id = [" + sctp_id + "]!");
			con_lst.remove(sctp_id); 
			SSctp.shutdownClient(sctp_id);
			
		}
		
	}
	//public LDCallback callback;
	//public MD5Callback md5_callback;
	private ConcurrentHashMap<Integer, ClientConnection> con_lst;
	public int sctp_id;
	private int sctp_sid;
	public String id;
	private Reader_r reader_r;
	private Thread reader_t;
	public boolean stopping;
	
	public ClientConnection(int _sctp_id, ConcurrentHashMap<Integer, ClientConnection> _lst){
		try{
			con_lst = _lst;
			sctp_id = _sctp_id;
			// VSTP connect
			if(connectNode(sctp_id)){
				//callback = new LDCallback();
				//md5_callback = new MD5Callback();
				reader_r = new Reader_r();
				reader_t = new Thread(reader_r, "GPUN_READER");
				reader_t.start();
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
