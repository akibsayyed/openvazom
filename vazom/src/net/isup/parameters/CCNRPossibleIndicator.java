package net.isup.parameters;

import net.isup.ParameterType;

public class CCNRPossibleIndicator extends ParameterBase {
	public CCNRPossibleIndicator(){
		type = ParameterType.CCNR_POSSIBLE_INDICATOR;
	}
}
