package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SeizeResourceRes extends SET{

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(0).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_destinationDigits(); }});
	}
	public SeizeResourceRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
