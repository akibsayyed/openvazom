package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MEID extends OCTET_STRING{

	public MEID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
