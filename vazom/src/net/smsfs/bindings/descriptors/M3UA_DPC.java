package net.smsfs.bindings.descriptors;

import net.hplmnr.HPLMNRPacket;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.smsfs.FilterMode;
import net.smsfs.SmsfsPacket;
import net.smsfs.bindings.BindingDescriptor;

public class M3UA_DPC extends BindingDescriptor {
	public M3UA_DPC(){
		super(6);
	}
	public M3UA_DPC(int index){
		super(index);
	}

	public void modify(SmsfsPacket fp, String new_value) {
		byte[] m3ua_packet;
		HPLMNRPacket sp;
		// not allowed in REFILTER mode
		if(fp.filterMode == FilterMode.NORMAL){
			// get SRIPacket reference
			sp = (HPLMNRPacket)fp.extra_data.get(1);
			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			//ndata.message
			DATA ndd = (DATA)sp.m3ua_packet.message;
			ndata.message = ndd;
			ndd.protocolData.destinationPointCode = Integer.parseInt(new_value);
			m3ua_packet = ndata.encode();

			// final combined result(m3ua + sccp + tcap)
			fp.packet = m3ua_packet;
			
		}
		
	}

}
