package net.dr;

import net.config.FNConfigData;
import net.db.DBRecordBase;
import net.db.DBRecordISUP;
import net.db.DBRecordSMS;
import net.db.DBRecordSRI;
import net.db.DBRecordType;
import net.ds.DataSourceType;
import net.flood.FloodManager;
import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.NumberingPlan;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;
import org.apache.log4j.Logger;

public class DRWorker_vstp {
	private static Logger logger=Logger.getLogger(DRWorker_vstp.class);
	private Dw_r dw_r;
	private Thread dw_t;
	private String worker_name;
	private boolean stopping;
	
	
	private DBRecordSMS process_SMPP_SUBMIT_SM(MessageDescriptor md){
		DBRecordSMS dbr = Utils.convert_VSTP_SMPP2DBR(md);
		// DB queue
		//DRManager.processDBR(dbr, (dbr.dataSource == DataSourceType.SMPP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE));
		return dbr;

	}
	

	private DBRecordSMS process_SMS_SUBMIT(MessageDescriptor md){
		DBRecordSMS dbr = Utils.convert_VSTP_TPDU2DBR(md);
		// DB queue
		//DRManager.processDBR(dbr, (dbr.dataSource == DataSourceType.SCTP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE));
		return dbr;
		
	}
	
	private DBRecordSMS process_SMPP_DELIVER_SM(MessageDescriptor md){
		DBRecordSMS dbr = Utils.convert_VSTP_SMPP2DBR(md);
		// DB queue
		//DRManager.processDBR(dbr, (dbr.dataSource == DataSourceType.SMPP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE));
		return dbr;
	}
	
	private DBRecordSMS process_SMS_DELIVER(MessageDescriptor md){
		DBRecordSMS dbr = Utils.convert_VSTP_TPDU2DBR(md);
		// DB queue
		//DRManager.processDBR(dbr, (dbr.dataSource == DataSourceType.SCTP ? ModeOfOperation.INTRUSIVE : ModeOfOperation.NON_INTRUSIVE));
		return dbr;
	
	}
	private DBRecordISUP processISUP(MessageDescriptor md, DBRecordISUP dbr){
		if(md != null && dbr != null){
			switch(md.header.msg_type){
				// AddressComplee
				case ISUP_ACM:
					dbr.timestamp = System.currentTimeMillis();
					break;
				// Answer
				case ISUP_ANM:
					dbr.ringing_duration = System.currentTimeMillis() - dbr.timestamp;
					dbr.timestamp = System.currentTimeMillis();
					break;
				// Release
				case ISUP_REL:
					dbr.call_duration = System.currentTimeMillis() - dbr.timestamp;
					dbr.timestamp = System.currentTimeMillis();
					dbr.release_cause = Integer.parseInt(md.values.get(VSTPDataItemType.ISUP_RELEASE_CAUSE.getId()));
					dbr.release_cause_class = Integer.parseInt(md.values.get(VSTPDataItemType.ISUP_RELEASE_CLASS.getId()));
					// remove correlation
					DRManager.isup_correlation_remove(dbr.cic, dbr.dpc, dbr.opc);
					// push to db queue
					// DB queue
					DRManager.processDBR(dbr, Utils.convert_DS2MOO(dbr.dataSource));
					break;
			}
			
		}
		
		return null;

	}
	private DBRecordISUP processISUP_IAM(MessageDescriptor md){
		DBRecordISUP res = null;
		if(md != null){
			if(md.header.msg_type == MessageType.ISUP_IAM){
				res = new DBRecordISUP();
				res.dataSource = md.header.ds;
				res.called_party = md.values.get(VSTPDataItemType.ISUP_CALLED_PARTY.getId());
				// called nai
				if(md.values.get(VSTPDataItemType.ISUP_CALLED_PARTY_NAI.getId()) != null) res.called_party_nai = Integer.parseInt(md.values.get(VSTPDataItemType.ISUP_CALLED_PARTY_NAI.getId()));
				res.calling_party = md.values.get(VSTPDataItemType.ISUP_CALLING_PARTY.getId());
				// calling nai
				if(md.values.get(VSTPDataItemType.ISUP_CALLING_PARTY_NAI.getId()) != null) res.calling_party_nai = Integer.parseInt(md.values.get(VSTPDataItemType.ISUP_CALLING_PARTY_NAI.getId()));

				res.cic = Integer.parseInt(md.values.get(VSTPDataItemType.ISUP_CIC.getId()));
				res.dpc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
				res.opc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
				res.timestamp = System.currentTimeMillis();
				return res;
			}		
		
		}
		return null;
	}
	
	private DBRecordSMS processSMS(MessageDescriptor md){
		if(md != null){
			switch(md.header.msg_type){
				// SMS-DELIVER
				case SMS_MT:
					StatsManager.DECODER_STATS.SMS_DELIVER_COUNT++;
					return process_SMS_DELIVER(md);
					//break;
				// SMS-SUBMIT
				case SMS_MO:
					StatsManager.DECODER_STATS.SMS_SUBMIT_COUNT++;
					return process_SMS_SUBMIT(md);
					//break;
				// SMPP SUBMIT-SM
				case SMPP_MO:
					return process_SMPP_SUBMIT_SM(md);
					//break;
				// SMPP DELIVER-SM
				case SMPP_MT:
					return process_SMPP_DELIVER_SM(md);
					//break;
					
			}
		}
		return null;
	}
	private DBRecordSRI processHLR(MessageDescriptor md){
		DBRecordSRI dbr_sri = null;
		try{
			//System.out.println(new String(md.encode()));
			dbr_sri = new DBRecordSRI();
			dbr_sri.timestamp = System.currentTimeMillis();
			dbr_sri.dataSource = md.header.ds;
			// routing type (SM or CH)
			if(md.values.get(VSTPDataItemType.HLR_ROUTING_TYPE.getId()) != null){
				dbr_sri.routing_type = Integer.parseInt(md.values.get(VSTPDataItemType.HLR_ROUTING_TYPE.getId()));
				
			}
			
			dbr_sri.gt_called = md.values.get(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId());
			dbr_sri.gt_calling = md.values.get(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId());
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId()) != null) dbr_sri.gt_called_gti = GlobalTitleIndicator.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId()) != null) dbr_sri.gt_calling_gti = GlobalTitleIndicator.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_TT.getId()) != null) dbr_sri.gt_called_tt = Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_TT.getId()));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_TT.getId()) != null) dbr_sri.gt_calling_tt = Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_TT.getId()));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId()) != null) dbr_sri.gt_called_np = NumberingPlan.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId()) != null) dbr_sri.gt_calling_np = NumberingPlan.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId()) != null) dbr_sri.gt_called_nai = NatureOfAddress.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId()) != null) dbr_sri.gt_calling_nai = NatureOfAddress.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId())));
			
			if(md.values.get(VSTPDataItemType.M3UA_DPC.getId()) != null) dbr_sri.m3ua_data_dpc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
			if(md.values.get(VSTPDataItemType.M3UA_OPC.getId()) != null) dbr_sri.m3ua_data_opc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_SID.getId()) != null) dbr_sri.tcap_sid = Long.parseLong(md.values.get(VSTPDataItemType.TCAP_SID.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_DID.getId()) != null) dbr_sri.tcap_did = Long.parseLong(md.values.get(VSTPDataItemType.TCAP_DID.getId()));
			dbr_sri.msisdn = md.values.get(VSTPDataItemType.HLR_MSISDN.getId());
			dbr_sri.msisdn_nai = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.HLR_MSISDN_NAI.getId()), "0"));
			dbr_sri.imsi = md.values.get(VSTPDataItemType.HLR_IMSI.getId());
			dbr_sri.nnn = md.values.get(VSTPDataItemType.HLR_NNN.getId());
			dbr_sri.annn = md.values.get(VSTPDataItemType.HLR_ANNN.getId());
			dbr_sri.sca = md.values.get(VSTPDataItemType.HLR_SCA.getId());
			dbr_sri.app_ctx_oid = md.values.get(VSTPDataItemType.TCAP_DIALOGUE_OID.getId());

			dbr_sri.gsmc = md.values.get(VSTPDataItemType.HLR_GSMC.getId());
			dbr_sri.gsmc_nai = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.HLR_GSMC_NAI.getId()), "0"));
			dbr_sri.vmsc = md.values.get(VSTPDataItemType.HLR_VMSC.getId());
			dbr_sri.vmsc_nai = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.HLR_VMSC_NAI.getId()), "0"));
			dbr_sri.roaming_number = md.values.get(VSTPDataItemType.HLR_ROAMING_NUMBER.getId());
			dbr_sri.roaming_number_nai = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.HLR_ROAMING_NUMBER_NAI.getId()), "0"));
			dbr_sri.fwd_to_number = md.values.get(VSTPDataItemType.HLR_FWD_TO_NUMBER.getId());
			dbr_sri.fwd_to_number_nai = Integer.parseInt((String)Utils.isNull(md.values.get(VSTPDataItemType.HLR_FWD_TO_NUMBER_NAI.getId()), "0"));
			
			// error codes
			if(md.values.get(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId()) != null) dbr_sri.gsm_map_error_code = Integer.parseInt(md.values.get(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_ERROR_CODE.getId()) != null) dbr_sri.tcap_abort_error_code = Integer.parseInt(md.values.get(VSTPDataItemType.TCAP_ERROR_CODE.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId()) != null) dbr_sri.tcap_dialogue_error_code = Integer.parseInt(md.values.get(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId()));
		
			
			
			
		}catch(Exception e){
			System.out.println("---------HLR ERROR-------------");
			e.printStackTrace();
			System.out.println(new String(md.encode()));
			System.out.println("---------HLR ERROR END-------------");

		}
		return dbr_sri;
	}
	
	
	private class Dw_r implements Runnable{
		DRPacket packet;
		
		public void run() {
			MessageDescriptor vstp_md = null;
			DBRecordSRI dbr_sri = null;
			//DBRecordSRI tmp_dbr_sri = null;
			DBRecordBase tmp_dbr = null;
			DBRecordSMS dbr_sms = null;
			DBRecordISUP db_isup = null;
			boolean is_begin_sri = false;
			//boolean is_end_sri = false;
			//boolean is_end_err_sri = false;
			//boolean is_tcap_abort = false;
			//boolean is_continue_req_sri = false;
			int invoke_id;
			int cic;
			int dpc;
			int opc;
			int tcap_id;
			int smpp_command_status;
			logger.info(worker_name + " STARTING!");
			while(!stopping){
				packet = DRManager.queue.poll();
				if(packet != null){
					try{
						vstp_md = packet.vstp_data;
					
						// SMS
						if(		vstp_md.header.msg_type == MessageType.SMS_MO || 
								vstp_md.header.msg_type == MessageType.SMS_MT ||
								vstp_md.header.msg_type == MessageType.SMPP_MO ||
								vstp_md.header.msg_type == MessageType.SMPP_MT){

							// create dbr
							dbr_sms = processSMS(vstp_md);

							// TCAP Error Correlation
							if(FNConfigData.dr_sms_errors_status){
								
								switch(vstp_md.header.msg_type){
									case SMS_MO:
									case SMS_MT:
										invoke_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_INVOKE_ID.getId()));
										if(!DRManager.errorCorrelationPut(dbr_sms.tcap_sid, invoke_id, dbr_sms)){
											// flood fix
											if(FNConfigData.flood_status){
												FloodManager.calcDec(vstp_md.header.msg_type);
												if(vstp_md.header.msg_type == MessageType.SMS_MT){
													FloodManager.decItem(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), vstp_md.header.msg_type);
													FloodManager.decItem(VSTPDataItemType.MAP_SCOA.getId(), vstp_md.header.msg_type);
													FloodManager.decItem(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId(), vstp_md.header.msg_type);
												}else if(vstp_md.header.msg_type == MessageType.SMS_MO){
													FloodManager.decItem(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), vstp_md.header.msg_type);
													FloodManager.decItem(VSTPDataItemType.MAP_SCDA.getId(), vstp_md.header.msg_type);
													FloodManager.decItem(VSTPDataItemType.SMS_TPDU_DESTINATION.getId(), vstp_md.header.msg_type);
													
												}
											}
										}
										break;
									case SMPP_MO:
									case SMPP_MT:
										//System.out.println(new String(vstp_md.encode()));
										/*
										System.out.println("SMS: " + vstp_md.values.get(VSTPDataItemType.IP_SOURCE.getId()) + ":" +
												vstp_md.values.get(VSTPDataItemType.TCP_SOURCE.getId()) + ":" +
												vstp_md.values.get(VSTPDataItemType.SMPP_SEQ_NUM.getId()));
											*/
										if(!DRManager.errorCorrelationPut(	vstp_md.values.get(VSTPDataItemType.IP_SOURCE.getId()) + ":" +
																		vstp_md.values.get(VSTPDataItemType.TCP_SOURCE.getId()) + ":" +
																		vstp_md.values.get(VSTPDataItemType.SMPP_SEQ_NUM.getId())
																	, dbr_sms)){
											// flood fix
											if(FNConfigData.flood_status){
												FloodManager.calcDec(vstp_md.header.msg_type);
											}
											
										}
										break;
								}
								
							}else{
								// DB queue
								DRManager.processDBR(dbr_sms, Utils.convert_DS2MOO(dbr_sms.dataSource));
							}

							
							
						// SRI SM
						}else if(vstp_md.header.msg_type == MessageType.HLR){
							if(FNConfigData.dr_non_intrusive_hlr_status || (FNConfigData.dr_intrusive_hlr_status && packet.dataSource == DataSourceType.SCTP)){
								// sri packet type
								is_begin_sri = (vstp_md.values.get(VSTPDataItemType.TCAP_SID.getId()) != null);
								//is_end_sri = (vstp_md.header.msg_type == MessageType.MAP_RETURN_RESULT);
								//is_continue_req_sri = (vstp_md.values.get(VSTPDataItemType.TCAP_DID.getId()) != null && vstp_md.values.get(VSTPDataItemType.TCAP_SID.getId()) != null);
								//is_end_err_sri = (vstp_md.header.msg_type == MessageType.MAP_RETURN_ERROR);
								//is_tcap_abort = (vstp_md.header.msg_type == MessageType.TCAP_ABORT);
								
								//if(is_begin_sri || is_end_sri || is_continue_req_sri || is_end_err_sri || is_tcap_abort){
								if(is_begin_sri){
									// get data
									dbr_sri = processHLR(vstp_md);
									/*
									System.out.println("----DR_WORKER_VSTP ----------------");
									System.out.println(new String(vstp_md.encode()));
									System.out.println("is_begin_sri       : " + is_begin_sri);
									System.out.println("is_end_sri         : " + is_end_sri);
									System.out.println("is_continue_req_sri: " + is_continue_req_sri);
									System.out.println("is_end_err_sri     : " + is_end_err_sri);
									System.out.println("is_tcap_abort      : " + is_tcap_abort);
									System.out.println("--------------------");
									*/
									// TCAP Begin SRI
									if(is_begin_sri){
										// TCAP Error Correlation
										if(FNConfigData.dr_sri_errors_status){
											invoke_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_INVOKE_ID.getId()));
											DRManager.errorCorrelationPut(dbr_sri.tcap_sid, invoke_id, dbr_sri);
											
										}else{
											// DB queue
											DRManager.processDBR(dbr_sri, Utils.convert_DS2MOO(dbr_sri.dataSource));
										}

									}
								}
							}

						}else if(vstp_md.header.msg_type == MessageType.TCAP_ABORT){
							tcap_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_DID.getId()));
							tmp_dbr = DRManager.errorCorrelationGetById((long)tcap_id);
							if(tmp_dbr != null){
								// remove correlation
								DRManager.errorCorrelationRemoveById((long)tcap_id);
								// get error code
								if(vstp_md.values.get(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId()) != null){
									tmp_dbr.sms_status = 3;
									tmp_dbr.tcap_dialogue_error_code = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId()));
									
								}else if(vstp_md.values.get(VSTPDataItemType.TCAP_ERROR_CODE.getId()) != null){
									tmp_dbr.sms_status = 2;
									tmp_dbr.tcap_abort_error_code = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_ERROR_CODE.getId()));
									
								}
								// push
								DRManager.processDBR(tmp_dbr, Utils.convert_DS2MOO(tmp_dbr.dataSource));
								// if SRI, write the current one also
								if(tmp_dbr.recordType == DBRecordType.SRI){
									dbr_sri = processHLR(vstp_md);
									dbr_sri.routing_type = ((DBRecordSRI)tmp_dbr).routing_type;
									DRManager.processDBR(dbr_sri, Utils.convert_DS2MOO(dbr_sri.dataSource));
								}
							}
							
							
						// gsm map return error
						}else if(vstp_md.header.msg_type == MessageType.MAP_RETURN_ERROR){
							invoke_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_INVOKE_ID.getId()));
							tcap_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_DID.getId()));
							// TCAP Error Correlation
							// get correlation
							tmp_dbr = DRManager.errorCorrelationGet((long)tcap_id, invoke_id);
							// check if type is valid
							
							if(tmp_dbr != null){
								tmp_dbr.sms_status = 4;

								// get error code
								if(vstp_md.values.get(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId()) != null){
									tmp_dbr.gsm_map_error_code = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId()));
								}
								// remove correlation
								DRManager.errorCorrelationRemove((long)tcap_id, invoke_id);
								// push to db queue
								DRManager.processDBR(tmp_dbr, Utils.convert_DS2MOO(tmp_dbr.dataSource));
								// if SRI, write the current one also
								if(tmp_dbr.recordType == DBRecordType.SRI){
									dbr_sri = processHLR(vstp_md);
									dbr_sri.routing_type = ((DBRecordSRI)tmp_dbr).routing_type;
									DRManager.processDBR(dbr_sri, Utils.convert_DS2MOO(dbr_sri.dataSource));
								}

							}
							
							
						// gsm map return result
						}else if(vstp_md.header.msg_type == MessageType.MAP_RETURN_RESULT){
							invoke_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_INVOKE_ID.getId()));
							tcap_id = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.TCAP_DID.getId()));
							// TCAP Error Correlation
							// get correlation
							tmp_dbr = DRManager.errorCorrelationGet((long)tcap_id, invoke_id);
							if(tmp_dbr != null){
								tmp_dbr.sms_status = 1;
								// remove correlation
								DRManager.errorCorrelationRemove((long)tcap_id, invoke_id);
								// push to db queue
								// DB queue
								DRManager.processDBR(tmp_dbr, Utils.convert_DS2MOO(tmp_dbr.dataSource));
								// if SRI, write the current one also
								if(tmp_dbr.recordType == DBRecordType.SRI){
									//System.out.println(new String(vstp_md.encode()));
									dbr_sri = processHLR(vstp_md);
									dbr_sri.routing_type = ((DBRecordSRI)tmp_dbr).routing_type;
									DRManager.processDBR(dbr_sri, Utils.convert_DS2MOO(dbr_sri.dataSource));
								}
							
							}

						// smpp resp pdu
						}else if(vstp_md.header.msg_type == MessageType.SMPP_RESP){
							/*
							System.out.println("SMS RESP: " + vstp_md.values.get(VSTPDataItemType.IP_DESTINATION.getId()) + ":" +
									vstp_md.values.get(VSTPDataItemType.TCP_DESTINATION.getId()) + ":" +
									vstp_md.values.get(VSTPDataItemType.SMPP_SEQ_NUM.getId()));
							*/
							tmp_dbr = DRManager.errorCorrelationGet(	vstp_md.values.get(VSTPDataItemType.IP_DESTINATION.getId()) + ":" +
																		vstp_md.values.get(VSTPDataItemType.TCP_DESTINATION.getId()) + ":" +
																		vstp_md.values.get(VSTPDataItemType.SMPP_SEQ_NUM.getId()));
							if(tmp_dbr != null){
								smpp_command_status = Integer.parseInt((String)Utils.isNull(vstp_md.values.get(VSTPDataItemType.SMPP_COMMAND_STATUS.getId()), "0"));
								if(smpp_command_status != 0){
									tmp_dbr.sms_status = 5;
									tmp_dbr.smpp_error_code = smpp_command_status;
									
								}else{
									tmp_dbr.sms_status = 1;
								}
								// remove correlation
								DRManager.errorCorrelationRemove(	vstp_md.values.get(VSTPDataItemType.IP_DESTINATION.getId()) + ":" +
																	vstp_md.values.get(VSTPDataItemType.TCP_DESTINATION.getId()) + ":" +
																	vstp_md.values.get(VSTPDataItemType.SMPP_SEQ_NUM.getId()));
								// push to db queue
								// DB queue
								DRManager.processDBR(tmp_dbr, Utils.convert_DS2MOO(tmp_dbr.dataSource));
							
							}
						// ISUP
						}else if(	vstp_md.header.msg_type == MessageType.ISUP_ACM ||
									vstp_md.header.msg_type == MessageType.ISUP_ANM ||
									vstp_md.header.msg_type == MessageType.ISUP_IAM ||
									vstp_md.header.msg_type == MessageType.ISUP_REL ||
									vstp_md.header.msg_type == MessageType.ISUP_RLC){
							
							// new call
							if(vstp_md.header.msg_type == MessageType.ISUP_IAM){
								db_isup = processISUP_IAM(vstp_md);
								DRManager.isup_correlation_put(db_isup.cic, db_isup.dpc, db_isup.opc, db_isup);
							// correlation
							}else{
								cic = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.ISUP_CIC.getId()));
								opc = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
								dpc = Integer.parseInt(vstp_md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
								tmp_dbr = DRManager.isup_correlation_get(cic, dpc, opc);
								if(tmp_dbr != null){
									db_isup = (DBRecordISUP)tmp_dbr;
									processISUP(vstp_md, db_isup);
									
								}
							}
							
							
							
							
						}
						
						
					}catch(Exception e){
						System.out.println("---------UNKNOWN ERROR-------------");
						e.printStackTrace();
						System.out.println(new String(vstp_md.encode()));
						System.out.println("---------UNKNOWN END-------------");

						
					}
					
				}else{
					//logger.info(worker_name + " sleeping for 1000 msec!");
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
				
			}
			logger.info(worker_name + " ENDING!");
			
		}
	
	
	}
	public void stop(){
		stopping = true;
		
	}
	
	public DRWorker_vstp(int id){
		dw_r = new Dw_r();
		worker_name = "DECODER_WORKER_" + id;
		dw_t = new Thread(dw_r, worker_name);
		dw_t.start();
		
		
	}
}