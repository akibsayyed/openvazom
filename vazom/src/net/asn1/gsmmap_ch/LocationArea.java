package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LocationArea extends CHOICE{

	public LAIFixedLength get_laiFixedLength(){
		return (LAIFixedLength)elements.get(0).data;
	}
	public LAIFixedLength new_laiFixedLength(){
		return new LAIFixedLength();
	}

	public LAC get_lac(){
		return (LAC)elements.get(1).data;
	}
	public LAC new_lac(){
		return new LAC();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_laiFixedLength(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_lac(); }});
	}
	public LocationArea(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
