package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Additional_Number extends CHOICE{

	public ISDN_AddressString get_msc_Number(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msc_Number(){
		return new ISDN_AddressString();
	}

	public ISDN_AddressString get_sgsn_Number(){
		return (ISDN_AddressString)elements.get(1).data;
	}
	public ISDN_AddressString new_sgsn_Number(){
		return new ISDN_AddressString();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_msc_Number(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sgsn_Number(); }});
	}
	public Additional_Number(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
