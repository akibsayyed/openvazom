__global__ void LD2(int *data_lst, int *str, int *str_len, int *result){
        char ch;
        char ch2;
        int i;
        int j;
        int z;
        int thread_id = (blockIdx.y * 5*500) + (blockIdx.x*500) + threadIdx.x;
        int msg_length = data_lst[thread_id*41] >> 24  & 0xff;
        int lst_msg_id = thread_id*41;
        int matrix_buffer[2][41]; // 2 rows, 161 columns
        int tmp_min;
        int m1;
        int m2;
        int m3;
        int y_byte_pos;
        int x_byte_pos;
        int x_byte_counter;
        int y_byte_counter;
        int x_packet_len = (int)ceil((float)*str_len/4);
        int x_msg;
        int y_msg;
        // init buffer matrix
        for(i = 0; i<=x_packet_len; i++) matrix_buffer[0][i] = 0x00000000 + ((i*4) << 24) + ((i*4+1) << 16) + ((i*4+2) << 8) + (i*4+3);
        matrix_buffer[1][0] = 0x01024000;

        // y axis
        i = 0;
        j = 0;
        y_byte_pos = 0;
        x_byte_pos = 0;
        x_byte_counter = 0;
        y_byte_counter = 0;
        while(y_byte_counter < msg_length){
                //get packed int if positioned on first packed byte
                if(y_byte_pos == 0) y_msg = data_lst[lst_msg_id + i + 1];
                // get packed byte
                ch = ((y_msg >> (y_byte_pos)*8) & 0x000000ff);
                // x axis
                while(x_byte_counter < *str_len){
                        //get packed int if positioned on first packed byte
                        if(x_byte_pos == 0) x_msg = str[j];
                        // get packed byte
                        ch2 = (x_msg >> (x_byte_pos)*8) & 0x000000ff;

                        m1 = ((matrix_buffer[0][(x_byte_pos < 3 ? j : j + 1)] >> ((4-x_byte_pos-2)*8)) & 0x000000ff); //U
                        m2 = ((matrix_buffer[1][(x_byte_pos < 3 ? j : j )] >> ((4-x_byte_pos-1)*8)) & 0x000000ff); //L
                       	m3 = ((matrix_buffer[0][(x_byte_pos < 4 ? j : j + 1)] >> ((4-x_byte_pos-1)*8)) & 0x000000ff); //UL
                        // min
                        if(ch == ch2) tmp_min = m3; else tmp_min = min(min(m1+1, m2+1), m3+1);
                        // set current matrix buffer position value to tmp_min
                        matrix_buffer[1][(x_byte_pos < 3 ? j : j + 1)] &= ~((0x000000ff << ((x_byte_pos < 3 ? 2-x_byte_pos : x_byte_pos))*8));
                        matrix_buffer[1][(x_byte_pos < 3 ? j : j + 1)] |=     ((tmp_min << ((x_byte_pos < 3 ? 2-x_byte_pos : x_byte_pos))*8));

                        // next packed byte                     
                        x_byte_pos++;
                        // next packed int
                        if(x_byte_pos == 4){ j++; x_byte_pos = 0; }
                        //x  byte counter
                        x_byte_counter++;

                }
                // update matrix buffer
                // move row 1 to row 0, update row 1
                for(z = 0; z<=x_packet_len; z++) matrix_buffer[0][z] = matrix_buffer[1][z];
                matrix_buffer[1][0] = ((y_byte_counter+2)<<24); //i + 1;


                // reset x axis counters
                j = 0;
                x_byte_pos = 0;
                x_byte_counter = 0;

                // next packed byte
                y_byte_pos++;
                // next packed int
                if(y_byte_pos == 4){ i++; y_byte_pos = 0; }
                // y byte counter
                y_byte_counter++;

        }
       result[thread_id] = tmp_min;//matrix_buffer[1][*str_len/4] & 0xff000000;

}



__global__ void LD(char *data_lst, int *data_lst_size, int *max_i_size, int *max_i_variation, char *str, int *str_len, char *result){
        char ch;
        char ch2;
        int i;
        int j;
        int z;
        int tmp_min;
        volatile unsigned int thread_id = (blockIdx.y * gridDim.x*blockDim.x) + (blockIdx.x*blockDim.x) + threadIdx.x;
        volatile unsigned int lst_msg_id = thread_id * (*max_i_size+1);
        int msg_length;
        int m1;
        int m2;
        int m3;
	// max message length = 164
        unsigned char matrix_buffer[2][165];
	
	// check if current thread id is >= data_lst_size
	if(thread_id >= *data_lst_size) return;
	// get message length
	msg_length = data_lst[lst_msg_id] & 0xff;
	// only process messages that are of similar length(+/- max_i_variation)
	if(abs(*str_len - msg_length) > *max_i_variation) { result[thread_id] = -1; return; }
        // init matrix buffer
        for(i = 0; i<=*str_len; i++) matrix_buffer[0][i] = i;
        matrix_buffer[1][0] = 1;

        // y axis 
        for(i = 0; i < msg_length; i++){
                // get next char from message list
                ch = data_lst[lst_msg_id + i + 1];
                // x axis
                #pragma unroll 80
                for(j = 0; j <*str_len; j++){
                        // get next char from current message
                        ch2 = str[j];
                        // get matrix buffer values     
                        m1 = matrix_buffer[0][j+1]; //U
                        m2 = matrix_buffer[1][j]; //L
                        m3 = matrix_buffer[0][j]; //UL
                        // calculate min        
                        if(ch == ch2) tmp_min = m3; else tmp_min = min(min(m1+1, m2+1), m3+1);

                        // set current matrix buffer value
                        matrix_buffer[1][j+1] = tmp_min;

                }
                // update matrix buffer
                #pragma unroll
                for(z = 0; z<165; z++) matrix_buffer[0][z] = matrix_buffer[1][z];
                matrix_buffer[1][0] = i+2;

        }
	// percentage of LD / LENGTH
        result[thread_id] = 100 - (int)ceil(((float)tmp_min / (*str_len > msg_length ? *str_len : msg_length))*100);
}
void LD_kernel(char *data_lst, int *data_lst_size, int *max_i_size, int *max_i_variation, char *str, int *str_len, char *result, dim3 blocks, dim3 threads){
	LD<<<blocks, threads>>>(data_lst, data_lst_size, max_i_size, max_i_variation, str, str_len, result);

}
