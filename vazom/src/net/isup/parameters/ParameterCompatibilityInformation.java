package net.isup.parameters;

import net.isup.ParameterType;

public class ParameterCompatibilityInformation extends ParameterBase {
	public ParameterCompatibilityInformation(){
		type = ParameterType.PARAMETER_COMPATIBILITY_INFORMATION;
	}
}
