package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ExtendedMSCID extends OCTET_STRING{

	public ExtendedMSCID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
