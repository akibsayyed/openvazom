package net.routing;

public enum RoutingConnectionDirectionType {
	CLIENT,
	SERVER;
}
