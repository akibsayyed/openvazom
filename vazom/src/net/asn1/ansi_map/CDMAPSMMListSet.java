package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAPSMMListSet extends SET{

	public CDMAServingOneWayDelay2 get_cdmaServingOneWayDelay2(){
		return (CDMAServingOneWayDelay2)elements.get(0).data;
	}
	public CDMAServingOneWayDelay2 new_cdmaServingOneWayDelay2(){
		return new CDMAServingOneWayDelay2();
	}

	public CDMATargetMAHOList get_cdmaTargetMAHOList(){
		return (CDMATargetMAHOList)elements.get(1).data;
	}
	public CDMATargetMAHOList new_cdmaTargetMAHOList(){
		return new CDMATargetMAHOList();
	}

	public CDMATargetMAHOList get_cdmaTargetMAHOList2(){
		return (CDMATargetMAHOList)elements.get(2).data;
	}
	public CDMATargetMAHOList new_cdmaTargetMAHOList2(){
		return new CDMATargetMAHOList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(347, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaServingOneWayDelay2(); }});
		elements.add(new ElementDescriptor(136, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaTargetMAHOList(); }});
		elements.add(new ElementDescriptor(136, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMAHOList2(); }});
	}
	public CDMAPSMMListSet(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
