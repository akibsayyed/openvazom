package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReleaseResourcesArg extends SEQUENCE{

	public ISDN_AddressString get_msrn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msrn(){
		return new ISDN_AddressString();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(1).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_msrn(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public ReleaseResourcesArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
