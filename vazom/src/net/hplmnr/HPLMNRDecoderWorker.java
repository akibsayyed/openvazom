package net.hplmnr;

import java.util.ArrayList;

import net.asn1.compiler.ASNType;
import net.asn1.gsmmap2.GSMMAPOperationLocalvalue;
import net.asn1.gsmmap2.LocationInfoWithLMSI;
import net.asn1.gsmmap2.MO_ForwardSM_Arg;
import net.asn1.gsmmap2.MT_ForwardSM_Arg;
import net.asn1.gsmmap2.ReportSM_DeliveryStatusArg;
import net.asn1.gsmmap2.RoutingInfoForSM_Arg;
import net.asn1.gsmmap2.RoutingInfoForSM_Res;
import net.asn1.tcap2.Invoke;
import net.asn1.tcap2.ReturnError;
import net.asn1.tcap2.TCMessage;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
import net.logging.LoggingManager;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.mtp3.MTP3;
import net.mtp3.MTP3Packet;
import net.mtp3.messages.normal.SCCPDataMessage;
import net.mtp3.types.ServiceIndicatorType;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionType;
import net.sccp.MessageType;
import net.sccp.SCCP;
import net.sccp.messages.UDT_UnitData;
import net.sctp.SSCTPDescriptor;
import net.smstpdu.AddressString;
import net.smstpdu.TBCD;
import net.stats.StatsManager;
import net.utils.Utils;

import org.apache.log4j.Logger;


public class HPLMNRDecoderWorker {
	public static Logger logger=Logger.getLogger(HPLMNRDecoderWorker.class);
	private Sriw_r sriw_r;
	private Thread sriw_t;
	private int worker_id;
	private boolean stopping;
	
	private class Sriw_r implements Runnable{

		
		
		public UDT_UnitData getUDT(M3UAPacket m3ua, MTP3Packet mtp3){
			if(m3ua != null){
				if(m3ua.message.type == M3UAMessageType.DATA){
					DATA m3ua_data = (DATA)m3ua.message;
					// check for actual protocol data
					if(m3ua_data.protocolData != null){
						if(m3ua_data.protocolData.serviceIndicator == net.m3ua.parameters.protocol_data.ServiceIndicatorType.SCCP){
							// check for SCCP UNITDATA
							if(SCCP.decodeType(m3ua_data.protocolData.userProtocolData) == MessageType.UDT_UNITDATA){
								return (UDT_UnitData)SCCP.decode(m3ua_data.protocolData.userProtocolData);
							}
							
						}
					}
				}
			}else if(mtp3 != null){
				if(mtp3.serviceInformation.serviceIndicator == ServiceIndicatorType.SCCP){
					if(SCCP.decodeType(((SCCPDataMessage)mtp3.message).data) == MessageType.UDT_UNITDATA){
						return (UDT_UnitData)SCCP.decode(((SCCPDataMessage)mtp3.message).data);
					}
					
				}
			}
			return null;
		}
		
		public void process_m3ua_mtp3(M3UAPacket m3ua, MTP3Packet mtp3, SSCTPDescriptor sd){
			//SSCTPDescriptor sd = null;
			String gt;
			HPLMNRPacket sri_packet;
			TCMessage tcm;
			BerTranscoderv2 ber = new BerTranscoderv2();
			ASNType invoke = null;
			ASNType returnResultLast;
			ReturnError returnErrorTcap;
			net.asn1.gsmmap2.ReturnError returnErrorGSMMAP;
			RoutingInfoForSM_Arg sm_arg;
			ReportSM_DeliveryStatusArg report_sm_arg;
			RoutingInfoForSM_Res sm_res;
			MO_ForwardSM_Arg mo_arg;
			MT_ForwardSM_Arg mt_arg;
			String oid;
			byte[] localValueData;
			int localValue = 0;
			LocationInfoWithLMSI li_wlmsi;
			TCAPCorrelationPacket tcap_corr = null;
			TCAPCorrelationPacket tmp_tcap_corr = null;
			boolean is_begin_sri;
			boolean is_end_sri;
			boolean is_end_err_sri;
			boolean is_tcap_abort;
			boolean is_report_sm;
			boolean is_return_result_last;
			
			// get SCCP UDT
			UDT_UnitData udt = getUDT(m3ua, mtp3);
			if(udt != null){
				gt = Utils.getGT(udt.calledPartyAddress.globalTitle);
				LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet SCCP CALLED GT = [" + gt + "]");

				// check if SRI, MO, or MT based on Called GT
				if(gt.equalsIgnoreCase(SGNConfigData.hplmnr_sri_gt)){
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet SCCP GT == [" + SGNConfigData.hplmnr_sri_gt + "], GT type = [SRI]");

					sri_packet = new HPLMNRPacket();
					sri_packet.routingType = (m3ua != null ? RoutingConnectionType.M3UA : RoutingConnectionType.MTP3);
					
					// current m3ua connection
					sri_packet.extra_params = new ArrayList<Object>();
					sri_packet.extra_params.add(sd.extra_params.get(0));
					
					sri_packet.calledPartyGT = Utils.getGT(udt.calledPartyAddress.globalTitle);
					sri_packet.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
					sri_packet.sccp_udt = udt;
					sri_packet.m3ua_packet = m3ua;
					sri_packet.mtp3_packet = mtp3;
					tcm = (TCMessage)ber.decode(new TCMessage(), udt.data);
					// check if TCAP
					if(Utils.isTCAP(udt.data)){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet containts [TCAP]");
						// detect type
						is_begin_sri = Utils.isBeginSRI(tcm);
						is_end_sri = Utils.isEndSRI(tcm);
						is_end_err_sri = Utils.isEndERRSRI(tcm);
						is_tcap_abort = Utils.isAbort(tcm);
						is_report_sm = Utils.isReportSM_DeliveryStatus(tcm);
						is_return_result_last = Utils.isReturnResultLast(tcm);
						// process
						if(is_begin_sri || is_end_sri || is_end_err_sri || is_tcap_abort || is_report_sm || is_return_result_last){
							// Check Direction
							tcap_corr = new TCAPCorrelationPacket();
							tcap_corr.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
							if(Utils.isBegin(tcm)){
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [BEGIN]");
								tcap_corr.tcap_sid = Utils.bytes2num(tcm.get_begin().get_otid().value);
								sri_packet.tcap_sid = tcm.get_begin().get_otid().value;
							}else if(Utils.isContinue(tcm)){
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger,"SCTP Packet TCAP is [CONTINUE]");
								tcap_corr.tcap_sid = Utils.bytes2num(tcm.get_continue().get_otid().value);
								tcap_corr.tcap_did = Utils.bytes2num(tcm.get_continue().get_dtid().value);
								sri_packet.tcap_sid = tcm.get_continue().get_otid().value;
								sri_packet.tcap_did = tcm.get_continue().get_dtid().value;
							}else if(Utils.isEnd(tcm)){
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END]");
								tcap_corr.tcap_did = Utils.bytes2num(tcm.get_end().get_dtid().value);
								sri_packet.tcap_did = tcm.get_end().get_dtid().value;
							}else if(Utils.isAbort(tcm)){
								tcap_corr.tcap_did = Utils.bytes2num(tcm.get_abort().get_dtid().value);
								sri_packet.tcap_did = tcm.get_abort().get_dtid().value;
							}
							
							// check if TCAP IDs exist
							if(tcap_corr.tcap_sid != null || tcap_corr.tcap_did != null){
								// Get if exists, else put new
								if((tmp_tcap_corr = HPLMNRManager.TCAPCheckCorrelation(tcap_corr.tcap_sid, tcap_corr.tcap_did)) == null){
									StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REQUEST++;
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP Correlation not found!: [sid: " + tcap_corr.tcap_sid + ", did: " + tcap_corr.tcap_did + "]");

									// report-sm
									if(is_report_sm){
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP COMPONENT is [invoke ReportSM_DeliveryStatus-Arg]");
										// save TCAP correlation
										HPLMNRManager.TCAPCorrelationPut(tcap_corr.tcap_sid, tcap_corr.tcap_did, tcap_corr);
										sri_packet.replyType = HPLMNRReplyType.SRI_PHASE_1_REPORT_SM;
										sri_packet.sri_tcm = tcm;
										//invoke = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
										invoke = Utils.get_INVOKE_PARAMETER(tcm);
										report_sm_arg = (ReportSM_DeliveryStatusArg)ber.decode(new ReportSM_DeliveryStatusArg(), invoke.getDataBytes());
										// MSISDN
										sri_packet.msisdn = AddressString.decode(report_sm_arg.get_msisdn().getValueBytes());
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI MISIDN = [" + TBCD.decode(sri_packet.msisdn.digits) + "]");
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										sri_packet.tcap_packet = udt.data;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
										
									// sendRoutingInfoForSM
									}else if(is_begin_sri){
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP COMPONENT is [invoke RoutingInfoForSM-Arg]");
										// save TCAP correlation
										HPLMNRManager.TCAPCorrelationPut(tcap_corr.tcap_sid, tcap_corr.tcap_did, tcap_corr);
										sri_packet.replyType = HPLMNRReplyType.SRI_PHASE_1;
										sri_packet.sri_tcm = tcm;
										//invoke = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
										invoke = Utils.get_INVOKE_PARAMETER(tcm);
										sm_arg = (RoutingInfoForSM_Arg)ber.decode(new RoutingInfoForSM_Arg(), invoke.getDataBytes());
										// MSISDN
										sri_packet.msisdn = AddressString.decode(sm_arg.get_msisdn().getValueBytes());
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI MISIDN = [" + TBCD.decode(sri_packet.msisdn.digits) + "]");
										// Service Centre Address
										sri_packet.smsc = AddressString.decode(sm_arg.get_serviceCentreAddress().getValueBytes());
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI SMSC = [" + TBCD.decode(sri_packet.smsc.digits) + "]");
										// Invoke ID
										//sri_packet.invoke_id = tcm.get_begin().get_components().getChild(0).get_invoke().get_invokeID().value;
										sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
										// Routing info Arg
										sri_packet.sri_arg = sm_arg;
										sri_packet.sri_tcm = tcm;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									}
									
								
								}else{
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP Correlation found!: [sid: " + tmp_tcap_corr.tcap_sid + ", did: " + tmp_tcap_corr.tcap_did + "]");

									// get SRI
									if(is_end_sri){
										StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ACK++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP COMPONENT is [returnResultLast RoutingInfoForSM-Res]");
										sri_packet.tcap_did = tcm.get_end().get_dtid().getValueBytes();
										sri_packet.replyType = HPLMNRReplyType.SRI_PHASE_2_ACK;
										//returnResultLast = tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_parameter();
										returnResultLast = Utils.get_RETURN_RESULT_LAST_RETRES_PARAMETER(tcm);
										sm_res = (RoutingInfoForSM_Res)ber.decode(new RoutingInfoForSM_Res(), returnResultLast.getDataBytes());
										// Invoke ID
										sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
										// IMSI
										sri_packet.imsi = TBCD.decode(sm_res.get_imsi().getValueBytes());
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI IMSI = [" + sri_packet.imsi + "]");
										li_wlmsi = sm_res.get_locationInfoWithLMSI();
										if(li_wlmsi != null){
											// Network Node Number
											sri_packet.networkNodeNumber =  AddressString.decode(li_wlmsi.get_networkNode_Number().getValueBytes());
											LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI NNN = [" + TBCD.decode(sri_packet.networkNodeNumber.digits) + "]");
											// Additional Number
											if(li_wlmsi.get_additional_Number() != null){
												if(li_wlmsi.get_additional_Number().get_msc_Number() != null){
													sri_packet.additonalNumber =  AddressString.decode(li_wlmsi.get_additional_Number().get_msc_Number().getValueBytes());
												}else if(li_wlmsi.get_additional_Number().get_sgsn_Number() != null){
													sri_packet.additonalNumber = AddressString.decode(li_wlmsi.get_additional_Number().get_sgsn_Number().getValueBytes());
												}
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI ANNN = [" + TBCD.decode(sri_packet.additonalNumber.digits) + "]");
												
											}											
										}

										// Routing info Res
										sri_packet.sri_res = sm_res;
										sri_packet.sri_tcm = tcm;
										
										
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									// SRI ERR
									}else if(is_end_err_sri){
										StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ERR++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP COMPONENT is [returnError]");
										sri_packet.replyType = HPLMNRReplyType.SRI_PHASE_2_ERR;
										sri_packet.tcap_did = tcm.get_end().get_dtid().getValueBytes();
										sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
										sri_packet.sri_tcm = tcm;
										//returnErrorTcap = tcm.get_end().get_components().getChild(0).get_returnError();
										returnErrorTcap = Utils.get_RETURN_ERROR(tcm);
										returnErrorGSMMAP = (net.asn1.gsmmap2.ReturnError)ber.decode(new net.asn1.gsmmap2.ReturnError(), returnErrorTcap.getDataBytes());
										sri_packet.sri_gsmmap_err = returnErrorGSMMAP;
										if(returnErrorGSMMAP.get_errorCode() != null){
											if(returnErrorGSMMAP.get_errorCode().get_localValue() != null){
												localValue = (int)Utils.bytes2num(returnErrorGSMMAP.get_errorCode().get_localValue().value);
											// Unknown ErrorCode
											}else localValue = 9999;
										}
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "ErrorCode = [" + localValue + "]");
										
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									// TCAP Abort
									}else if(is_tcap_abort){
										StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_ERR++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP is [ABORT]");
										sri_packet.replyType = HPLMNRReplyType.SRI_PHASE_2_ABORT;
										sri_packet.tcap_did = tcm.get_abort().get_dtid().getValueBytes();
										sri_packet.sri_tcm = tcm;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									// Empty ReturnResultLast
									}else if(is_return_result_last){
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP COMPONENT is [returnResultLast]");
										sri_packet.replyType = HPLMNRReplyType.SRI_PHASE_2_REPORT_SM_ACK;
										sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
										sri_packet.sri_tcm = tcm;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										sri_packet.tcap_packet = udt.data;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									}											

									
									// remove TCAP correlation
									HPLMNRManager.TCAPCorrelationRemove(tmp_tcap_corr.tcap_sid, tmp_tcap_corr.tcap_did);
									
									
								}												
							}												
						}
					
					}
					
					
				}else if(gt.equalsIgnoreCase(SGNConfigData.hplmnr_mo_gt)){
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet SCCP GT == [" + SGNConfigData.hplmnr_mo_gt + "], GT type = [MO]");
					
					// check if TCAP
					if(Utils.isTCAP(udt.data)){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet containts [TCAP]");
						sri_packet = new HPLMNRPacket();
						sri_packet.routingType = (m3ua != null ? RoutingConnectionType.M3UA : RoutingConnectionType.MTP3);

						// current m3ua connection
						sri_packet.extra_params = new ArrayList<Object>();
						sri_packet.extra_params.add(sd.extra_params.get(0));
						
						
						tcm = (TCMessage)ber.decode(new TCMessage(), udt.data);
						
						// Check Direction
						tcap_corr = new TCAPCorrelationPacket();
						tcap_corr.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
						if(Utils.isBegin(tcm)){
							LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [BEGIN MO]");
							tcap_corr.tcap_sid = Utils.bytes2num(tcm.get_begin().get_otid().value);
							sri_packet.tcap_sid = tcm.get_begin().get_otid().value;
						}else if(Utils.isContinue(tcm)){
							LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE MO]");
							tcap_corr.tcap_sid = Utils.bytes2num(tcm.get_continue().get_otid().value);
							tcap_corr.tcap_did = Utils.bytes2num(tcm.get_continue().get_dtid().value);
							sri_packet.tcap_sid = tcm.get_continue().get_otid().value;
							sri_packet.tcap_did = tcm.get_continue().get_dtid().value;

						}else if(Utils.isEnd(tcm)){
							LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END MO]");
							tcap_corr.tcap_did = Utils.bytes2num(tcm.get_end().get_dtid().value);
							sri_packet.tcap_did = tcm.get_end().get_dtid().value;
						}
						// check if TCAP IDs exist
						if(tcap_corr.tcap_sid != null || tcap_corr.tcap_did != null){
							// Get if exists, else put new
							if((tmp_tcap_corr = HPLMNRManager.TCAPCheckCorrelation(tcap_corr.tcap_sid, tcap_corr.tcap_did)) == null){
								StatsManager.HPLMNR_STATS.HPLMNR_MO_IN++;
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP Correlation not found!: [sid: " + tcap_corr.tcap_sid + ", did: " + tcap_corr.tcap_did + "]");
								// save TCAP correlation
								HPLMNRManager.TCAPCorrelationPut(tcap_corr.tcap_sid, tcap_corr.tcap_did, tcap_corr);
								
								// get Invoke
								if(Utils.isBeginMO(tcm)){
									//invoke = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
									invoke = Utils.get_INVOKE_PARAMETER(tcm);

								}else if(Utils.isContinueMO(tcm)){
									//invoke = tcm.get_continue().get_components().getChild(0).get_invoke().get_parameter();
									invoke = Utils.get_INVOKE_PARAMETER(tcm);
									
								}else if(Utils.isEndMO(tcm)){
									//invoke = tcm.get_end().get_components().getChild(0).get_invoke().get_parameter();
									invoke = Utils.get_INVOKE_PARAMETER(tcm);
									
								}
								
								// reply type
								sri_packet.replyType = HPLMNRReplyType.MO_PHASE_1;
								sri_packet.mo_tcm = tcm;
								//MO-Forward-SM
								if(invoke != null){
									sri_packet.mo_arg = (MO_ForwardSM_Arg)ber.decode(new MO_ForwardSM_Arg(), invoke.getDataBytes());
									
									sri_packet.tcap_packet = udt.data;
									sri_packet.calledPartyGT = SGNConfigData.hplmnr_mo_smsc;
									sri_packet.sccp_udt = udt;
									sri_packet.m3ua_packet = m3ua;
									sri_packet.mtp3_packet = mtp3;
									/*
									// Dialogue info
									sri_packet.hasDialogue = tcm.get_begin().get_dialoguePortion() != null;
									// Decode dialogue, get application-context-name OID
									if(sri_packet.hasDialogue){
										sri_packet.app_ctx_oid = Utils.getTcapDialogueAppCtx(tcm.get_begin().get_dialoguePortion());
										oid = Utils.decodeOID(sri_packet.app_ctx_oid);
									}
									*/
									
									
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);

									invoke = null;

								}

								
								
								
							
							// TCAP correlation exists
							}else{
								
								
								StatsManager.HPLMNR_STATS.HPLMNR_MO_OUT_REPLY++;
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP Correlation found!: [sid: " + tmp_tcap_corr.tcap_sid + ", did: " + tmp_tcap_corr.tcap_did + "]");
								sri_packet.replyType = HPLMNRReplyType.MO_PHASE_2;
								sri_packet.tcap_packet = udt.data;
								sri_packet.calledPartyGT = tmp_tcap_corr.callingPartyGT;
								sri_packet.sccp_udt = udt;
								sri_packet.m3ua_packet = m3ua;
								sri_packet.mtp3_packet = mtp3;
								// send to SRI queue
								sri_packet.ssctp_sid = sd.sid;
								HPLMNRManager.sri_queue.offer(sri_packet);

								// remove TCAP correlation
								HPLMNRManager.TCAPCorrelationRemove(tmp_tcap_corr.tcap_sid, tmp_tcap_corr.tcap_did);
								
							}											
						}											

						
					}									
					
					
				}else if(gt.equalsIgnoreCase(SGNConfigData.hplmnr_mt_gt)){
					LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet SCCP GT == [" + SGNConfigData.hplmnr_mt_gt + "], GT type = [MT]");
					
					// check if TCAP
					if(Utils.isTCAP(udt.data)){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet containts [TCAP]");
						sri_packet = new HPLMNRPacket();
						sri_packet.routingType = (m3ua != null ? RoutingConnectionType.M3UA : RoutingConnectionType.MTP3);

						// current m3ua connection
						sri_packet.extra_params = new ArrayList<Object>();
						sri_packet.extra_params.add(sd.extra_params.get(0));
						
						tcm = (TCMessage)ber.decode(new TCMessage(), udt.data);
						
						// Check Direction
						tcap_corr = new TCAPCorrelationPacket();
						tcap_corr.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
						if(Utils.isBegin(tcm)){
							tcap_corr.tcap_sid = Utils.bytes2num(tcm.get_begin().get_otid().value);
							sri_packet.tcap_sid = tcm.get_begin().get_otid().value;
						}else if(Utils.isContinue(tcm)){
							tcap_corr.tcap_sid = Utils.bytes2num(tcm.get_continue().get_otid().value);
							tcap_corr.tcap_did = Utils.bytes2num(tcm.get_continue().get_dtid().value);
							sri_packet.tcap_sid = tcm.get_continue().get_otid().value;
							sri_packet.tcap_did = tcm.get_continue().get_dtid().value;
						}else if(Utils.isEnd(tcm)){
							tcap_corr.tcap_did = Utils.bytes2num(tcm.get_end().get_dtid().value);
							sri_packet.tcap_did = tcm.get_end().get_dtid().value;
						}else if(Utils.isAbort(tcm)){
							tcap_corr.tcap_did = Utils.bytes2num(tcm.get_abort().get_dtid().value);
							sri_packet.tcap_did = tcm.get_abort().get_dtid().value;
							
						}
						// check if TCAP IDs exist
						if(tcap_corr.tcap_sid != null || tcap_corr.tcap_did != null){
							// Get if exists, else put new
							if((tmp_tcap_corr = HPLMNRManager.TCAPCheckCorrelation(tcap_corr.tcap_sid, tcap_corr.tcap_did)) == null){
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP Correlation not found!: [sid: " + tcap_corr.tcap_sid + ", did: " + tcap_corr.tcap_did + "]");

								// save M3UA DATA and SCCP needed for filter
								sri_packet.sccp_udt = udt;
								sri_packet.m3ua_packet = m3ua;
								sri_packet.mtp3_packet = mtp3;
								// save TCAP correlation
								HPLMNRManager.TCAPCorrelationPut(tcap_corr.tcap_sid, tcap_corr.tcap_did, tcap_corr);
								
								// MT invoke BEGIN
								if(Utils.isBeginMT(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_BEGIN++;
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_1_BEGIN;
									//invoke = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
									invoke = Utils.get_INVOKE_PARAMETER(tcm);
									sri_packet.mt_tcm = tcm;
									sri_packet.tcap_sid = tcm.get_begin().get_otid().getValueBytes();
									sri_packet.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
									sri_packet.calledPartyGT = Utils.getGT(udt.calledPartyAddress.globalTitle);
									//sri_packet.invoke_id = tcm.get_begin().get_components().getChild(0).get_invoke().get_invokeID().value;
									sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
									
									// Get localValue
									//localValueData = tcm.get_begin().get_components().getChild(0).get_invoke().get_opCode().get_localValue().getValueBytes();
									localValueData = ((Invoke)Utils.get_INVOKE(tcm)).get_opCode().get_localValue().getValueBytes();
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [BEGIN MT]");

									// MO or MT
									if(invoke != null){
										switch((int)Utils.bytes2num(localValueData)){
											case GSMMAPOperationLocalvalue._mo_forwardSM: 
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "GSM MAP is [mo-forwardSM]");
												mo_arg = (MO_ForwardSM_Arg)ber.decode(new MO_ForwardSM_Arg(), invoke.getDataBytes());
												sri_packet.imsi = TBCD.decode(mo_arg.get_sm_RP_DA().get_imsi().getValueBytes());
												sri_packet.mo_arg = mo_arg;
												break;
											case GSMMAPOperationLocalvalue._mt_forwardSM: 
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "GSM MAP is [mt-forwardSM]");
												mt_arg = (MT_ForwardSM_Arg)ber.decode(new MT_ForwardSM_Arg(), invoke.getDataBytes());
												sri_packet.imsi = TBCD.decode(mt_arg.get_sm_RP_DA().get_imsi().getValueBytes());
												sri_packet.mt_arg = mt_arg;
												break;
										}
									}
									/*
									// Dialogue info
									sri_packet.hasDialogue = tcm.get_begin().get_dialoguePortion() != null;
									// Decode dialogue, get application-context-name OID
									if(sri_packet.hasDialogue){
										sri_packet.app_ctx_oid = Utils.getTcapDialogueAppCtx(tcm.get_begin().get_dialoguePortion());
										oid = Utils.decodeOID(sri_packet.app_ctx_oid);
									}
									*/
									
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									
								// MT invoke CONTINUE
								}else if(Utils.isContinueMT(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_CONTINUE++;
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_1_CONTINUE;
									//invoke = tcm.get_continue().get_components().getChild(0).get_invoke().get_parameter();
									invoke = Utils.get_INVOKE_PARAMETER(tcm);
									sri_packet.mt_tcm = tcm;
									sri_packet.tcap_sid = tcm.get_continue().get_otid().getValueBytes();
									sri_packet.tcap_did = tcm.get_continue().get_dtid().getValueBytes();
									sri_packet.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
									sri_packet.calledPartyGT = Utils.getGT(udt.calledPartyAddress.globalTitle);
									//sri_packet.invoke_id = tcm.get_continue().get_components().getChild(0).get_invoke().get_invokeID().value;
									sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
									
									// Get localValue
									//localValueData = tcm.get_continue().get_components().getChild(0).get_invoke().get_opCode().get_localValue().getValueBytes();
									localValueData = ((Invoke)Utils.get_INVOKE(tcm)).get_opCode().get_localValue().getValueBytes();
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE MT]");
									
									// MO or MT
									if(invoke != null){
										switch((int)Utils.bytes2num(localValueData)){
											case GSMMAPOperationLocalvalue._mo_forwardSM: 
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "GSM MAP is [mo-forwardSM]");
												mo_arg = (MO_ForwardSM_Arg)ber.decode(new MO_ForwardSM_Arg(), invoke.getDataBytes());
												sri_packet.imsi = TBCD.decode(mo_arg.get_sm_RP_DA().get_imsi().getValueBytes());
												sri_packet.mo_arg = mo_arg;
												break;
											case GSMMAPOperationLocalvalue._mt_forwardSM: 
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "GSM MAP is [mt-forwardSM]");
												mt_arg = (MT_ForwardSM_Arg)ber.decode(new MT_ForwardSM_Arg(), invoke.getDataBytes());
												sri_packet.imsi = TBCD.decode(mt_arg.get_sm_RP_DA().get_imsi().getValueBytes());
												sri_packet.mt_arg = mt_arg;
												break;
										}														
									}
									/*
									// Dialogue info
									sri_packet.hasDialogue = tcm.get_continue().get_dialoguePortion() != null;
									// Decode dialogue, get application-context-name OID
									if(sri_packet.hasDialogue){
										sri_packet.app_ctx_oid = Utils.getTcapDialogueAppCtx(tcm.get_continue().get_dialoguePortion());
										oid = Utils.decodeOID(sri_packet.app_ctx_oid);
									}
									*/

									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									
								// MT invoke END
								}else if(Utils.isEndMT(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_END++;
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_1_END;
									//invoke = tcm.get_end().get_components().getChild(0).get_invoke().get_parameter();
									invoke = Utils.get_INVOKE_PARAMETER(tcm);
									sri_packet.mt_tcm = tcm;
									sri_packet.tcap_did = tcm.get_end().get_dtid().getValueBytes();
									sri_packet.callingPartyGT = Utils.getGT(udt.callingPartyAddress.globalTitle);
									sri_packet.calledPartyGT = Utils.getGT(udt.calledPartyAddress.globalTitle);
									//sri_packet.invoke_id = tcm.get_end().get_components().getChild(0).get_invoke().get_invokeID().value;
									sri_packet.invoke_id = Utils.get_INVOKE_ID(tcm);
									
									// Get localValue
									//localValueData = tcm.get_end().get_components().getChild(0).get_invoke().get_opCode().get_localValue().getValueBytes();
									localValueData = ((Invoke)Utils.get_INVOKE(tcm)).get_opCode().get_localValue().getValueBytes();
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END MT]");

									// MO or MT
									if(invoke != null){
										switch((int)Utils.bytes2num(localValueData)){
											case GSMMAPOperationLocalvalue._mo_forwardSM: 
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "GSM MAP is [mo-forwardSM]");
												mo_arg = (MO_ForwardSM_Arg)ber.decode(new MO_ForwardSM_Arg(), invoke.getDataBytes());
												sri_packet.imsi = TBCD.decode(mo_arg.get_sm_RP_DA().get_imsi().getValueBytes());
												sri_packet.mo_arg = mo_arg;
												break;
											case GSMMAPOperationLocalvalue._mt_forwardSM: 
												LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "GSM MAP is [mt-forwardSM]");
												mt_arg = (MT_ForwardSM_Arg)ber.decode(new MT_ForwardSM_Arg(), invoke.getDataBytes());
												sri_packet.imsi = TBCD.decode(mt_arg.get_sm_RP_DA().get_imsi().getValueBytes());
												sri_packet.mt_arg = mt_arg;
												break;
										}
									}
									/*
									// Dialogue info
									sri_packet.hasDialogue = tcm.get_end().get_dialoguePortion() != null;
									// Decode dialogue, get application-context-name OID
									if(sri_packet.hasDialogue){
										sri_packet.app_ctx_oid = Utils.getTcapDialogueAppCtx(tcm.get_end().get_dialoguePortion());
										oid = Utils.decodeOID(sri_packet.app_ctx_oid);
									}
									*/
									
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									
								// TCAP ONLY BEGIN / NO GMAP MAP
								}else if(Utils.isTCAPOnlyBegin(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_BEGIN++;
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [BEGIN Only / NO GSM MAP]");
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_1_TCAP_BEGIN;
									sri_packet.tcap_packet = udt.data;
									sri_packet.tcap_sid = tcm.get_begin().get_otid().getValueBytes();
									sri_packet.calledPartyGT = SGNConfigData.hplmnr_msc_gt;
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									
								// TCAP ONLY CONTINUE / NO GMAP MAP
								}else if(Utils.isTCAPOnlyContinue(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_CONTINUE++;
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE Only / NO GSM MAP]");
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_1_TCAP_CONTINUE;
									sri_packet.tcap_packet = udt.data;
									sri_packet.tcap_sid = Utils.num2bytes(tmp_tcap_corr.tcap_did);
									sri_packet.tcap_did = Utils.num2bytes(tmp_tcap_corr.tcap_sid);
									sri_packet.calledPartyGT = SGNConfigData.hplmnr_msc_gt;
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									

								// TCAP ONLY END / NO GMAP MAP
								}else if(Utils.isTCAPOnlyEnd(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_IN_END++;
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END Only / NO GSM MAP]");
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_1_TCAP_END;
									sri_packet.tcap_packet = udt.data;
									sri_packet.tcap_did = tcm.get_end().get_dtid().getValueBytes();
									sri_packet.calledPartyGT = SGNConfigData.hplmnr_msc_gt;
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									
								}
									
							// TCAP correlation exists	
							}else{
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "TCAP Correlation found!: [sid: " + tmp_tcap_corr.tcap_sid + ", did: " + tmp_tcap_corr.tcap_did + "]");

								// save SCCP and M3UA data
								sri_packet.sccp_udt = udt;
								sri_packet.m3ua_packet = m3ua;
								sri_packet.mtp3_packet = mtp3;
								// MT ACK/ERR
								// Continue
								if(Utils.isContinue(tcm)){
									// ACK
									if(Utils.isContinueReturnResultLast(tcm)){
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ACK++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE returnResultLast]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_ACK;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									}else if(Utils.isContinueReturnResultNotLast(tcm)){
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ACK++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE returnResultNotLast]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_ACK;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									// ERR	
									}else if(Utils.isContinueReturnError(tcm)){
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_ERR++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE ReturnError]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_ERR;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
										
									// OTHER (NO GSM MAP)
									}else{
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE_TCAP_REPLY++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [CONTINUE / NO GSM MAP]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_TCAP;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									}
									
								
								// End
								}if(Utils.isEnd(tcm)){
									// ACK
									if(Utils.isReturnResultLast(tcm)){
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_ACK++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END returnResultLast]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_ACK;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									// ERR
									}else if(Utils.isReturnError(tcm)){
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_ERR++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END ReturnError]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_ERR;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									// OTHER (NO GSM MAP)
									}else{
										StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END_TCAP_REPLY++;
										LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [END / NO GSM MAP]");
										sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_TCAP;
										sri_packet.tcap_packet = udt.data;
										sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
										// send to SRI queue
										sri_packet.ssctp_sid = sd.sid;
										HPLMNRManager.sri_queue.offer(sri_packet);
										
									}
								// TCAP Abort
								}else if(Utils.isAbort(tcm)){
									StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_ABORT_REPLY++;
									LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet TCAP is [ABORT / NO GSM MAP]");
									sri_packet.replyType = HPLMNRReplyType.MT_PHASE_2_TCAP;
									sri_packet.tcap_packet = udt.data;
									sri_packet.callingPartyGT = tmp_tcap_corr.callingPartyGT;
									// send to SRI queue
									sri_packet.ssctp_sid = sd.sid;
									HPLMNRManager.sri_queue.offer(sri_packet);
									
								}
								
								// remove TCAP correlation
								HPLMNRManager.TCAPCorrelationRemove(tmp_tcap_corr.tcap_sid, tmp_tcap_corr.tcap_did);
								
							}												
						}

					}									
				}
			}
		}

		
		public void run() {
			SSCTPDescriptor sd = null;
			RoutingConnection r_conn = null;
			M3UAPacket _m3ua = null;
			MTP3Packet _mtp3 = null;
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Starting SRIWorker thread: [ " + worker_id + " ]!");
			while(!stopping){
				try{
					sd = HPLMNRManager.in_queue.poll();
					if(sd != null){
						LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SCTP Packet read from in_queue!");
						// get connection type
						r_conn = (RoutingConnection)sd.extra_params.get(0);
						switch(r_conn.type){
							case M3UA: 
								_m3ua = M3UA.decode(sd.payload);
								_mtp3 = null;
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "M3UA header detected!");
								break;
							case MTP3: 
								_mtp3 = MTP3.decode(sd.payload);
								_m3ua = null;
								LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "MTP3 header detected!");
								break;
						}
						// process
						process_m3ua_mtp3(_m3ua, _mtp3, sd);
						
					}else{
						try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					}
					
					
				}catch(Exception e){
					if(SGNConfigData.hplmnr_debug){
						e.printStackTrace();
						LoggingManager.error(logger, "Error while decoding packet, malformed packet!");
						Utils.bytes2file(sd.payload, "/tmp/" + System.currentTimeMillis() + ".raw");
						
					}
				}
				
			}
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Stopping SRIWorker thread: [ " + worker_id + " ]!");
		}
		
		
	}
	
	public HPLMNRDecoderWorker(int id){
		worker_id = id;
		LoggingManager.info(logger, "Creating HPLMNR DECODER worker thread: [ " + id + " ]!");
		sriw_r = new Sriw_r();
		sriw_t = new Thread(sriw_r, "HPLMNR_WORKER_" + id);
		sriw_t.start();
		
		
	}
}
