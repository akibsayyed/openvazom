package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class OBJECT_IDENTIFIER extends ASNType {

	public OBJECT_IDENTIFIER(){
		super();
		universalTag = ASNTag.OBJECT_IDENTIFIER;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Primitive;
		
	}
}
