package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallingPartyCategory extends OCTET_STRING{

	public CallingPartyCategory(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
