package net.routing;

public abstract class CallbackBase implements Runnable{

	public abstract long get_callback_id();
	public abstract void set_callback_id(long _id);
	
	public abstract void set_params(Object[] params);
}
