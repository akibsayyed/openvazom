package net.m3ua.parameters.tmt;

import java.util.HashMap;

public enum TMTType {
	OVERRIDE(0x01),
	LOADSHARE(0x02),
	BROADCAST(0x03);
	
	
	private int id;
	
	private static final HashMap<Integer, TMTType> lookup = new HashMap<Integer, TMTType>();
	static{
		for(TMTType td : TMTType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static TMTType get(int id){ return lookup.get(id); }
	private TMTType(int _id){ id = _id; }			
}
