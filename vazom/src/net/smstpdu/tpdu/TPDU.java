package net.smstpdu.tpdu;

import net.smstpdu.MessageDirection;
import net.smstpdu.MessageType;

public abstract class TPDU {
	public MessageType type;
	protected int byte_pos = 0;
	
	public byte[] encode(){
		return null;
	}
	public static TPDU decode(byte[] data, MessageDirection msgd){
		TPDU res = null;
		// decode message type
		MessageType mt = MessageType.fromId(data[0] & 0x03, msgd);
		// create packet accordingly 
		switch(mt){
			case SMS_SUBMIT: res = new SmsSubmit(); break;
			case SMS_DELIVER: res = new SmsDeliver(); break;
		
		}
		if(res != null) res.init(data);
		return res;
		
	}
	public abstract void init(byte[] data);
}
