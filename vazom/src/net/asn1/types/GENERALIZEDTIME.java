package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class GENERALIZEDTIME extends ASNType {

	public GENERALIZEDTIME(){
		super();
		universalTag = ASNTag.GENERALIZEDTIME;
		tag = ASNTag.name2Tag(universalTag.toString());
		
	}
}
