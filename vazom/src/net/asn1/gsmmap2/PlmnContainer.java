package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PlmnContainer extends SEQUENCE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public Category get_category(){
		return (Category)elements.get(1).data;
	}
	public Category new_category(){
		return new Category();
	}

	public BasicServiceCode get_basicService(){
		return (BasicServiceCode)elements.get(2).data;
	}
	public BasicServiceCode new_basicService(){
		return new BasicServiceCode();
	}

	public PlmnContaineroperatorSS_Code get_operatorSS_Code(){
		return (PlmnContaineroperatorSS_Code)elements.get(3).data;
	}
	public PlmnContaineroperatorSS_Code new_operatorSS_Code(){
		return new PlmnContaineroperatorSS_Code();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_category(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_basicService(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_operatorSS_Code(); }});
	}
	public PlmnContainer(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 2;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
