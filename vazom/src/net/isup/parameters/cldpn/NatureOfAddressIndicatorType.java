package net.isup.parameters.cldpn;

import java.util.HashMap;

public enum NatureOfAddressIndicatorType {
	SUBSCRIBER_NUMBER(0x01),
	UNKNOWN(0x02),
	NATIONAL(0x03),
	INTERNATIONAL(0x04),
	NETWORK_SPECIFIC(0x05),
	NETWORK_ROUTING_NUMBER_IN_NATIONAL_NUMBER_FORMAT(0x06),
	NETWORK_ROUTING_NUMBER_IN_NETWORK_SPECIFIC_NUMBER_FORMAT(0x07),
	NETWORK_ROUTING_NUMBER_CONCATENATED_WITH_CALLED_DIRECTORY_NUMBER(0x08);
	

	
	
	private int id;
	private static final HashMap<Integer, NatureOfAddressIndicatorType> lookup = new HashMap<Integer, NatureOfAddressIndicatorType>();
	static{
		for(NatureOfAddressIndicatorType td :NatureOfAddressIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NatureOfAddressIndicatorType get(int id){ return lookup.get(id); }
	private NatureOfAddressIndicatorType(int _id){ id = _id; }	
}
