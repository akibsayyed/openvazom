package net.isup.messages;

import net.isup.MessageType;

public class ResetCircuit extends MessageBase {
	public ResetCircuit(){
		super();
		type = MessageType.RESET_CIRCUIT;
	}

	public void init(byte[] data) {
		// no data
	}

}
