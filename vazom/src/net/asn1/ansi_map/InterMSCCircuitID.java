package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class InterMSCCircuitID extends OCTET_STRING{

	public InterMSCCircuitID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
