package net.smsfs.list;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CopyOnWriteArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class ListDescriptor {
	public int max_size;
	public int list_pos;
	public CopyOnWriteArrayList<ListItemBase> list;
	public ListDescriptor(int _max_size){
		list = new CopyOnWriteArrayList<ListItemBase>();
		max_size = _max_size;
		
	}
	
	public void reset_updated(){
		for(ListItemBase li : list) li.updated = false;
	}
	
	public void clear_obsolete(){
		for(ListItemBase li : list) if(!li.updated && !li.explicit){
			list.remove(li);
		}
		
	}
	public void clear_obsolete(ListBase list_base){
		for(ListItemBase li : list) if(!li.updated && !li.explicit){
			list.remove(li);
			// special on_delete action
			list_base.on_delete(li.data);
		}
		
	}
	public List<ListItemBase> get_explicit(){
		ArrayList<ListItemBase> res = new ArrayList<ListItemBase>();
		for(ListItemBase li : list) if(li.explicit) res.add(li);
		return res;
		
	}
	
	
	public void add(byte[] data, boolean explicit){
		ListItemBase li = null;
		if(data != null){
			// add new
			if(list.size() < max_size){
				li = new ListItemBase();
				li.updated = true;
				li.data = data;
				li.explicit = explicit;
				list.add(li);
			// replace
			}else{
				synchronized(this){
					if(list_pos >= list.size()) list_pos = 0;
					li = list.get(list_pos++);
					if(li != null){
						li.updated = true;
						li.explicit = explicit;
						li.data = li.data;
					}
				}
			}
		}
	}
	public int pattern_search(String source){
		int res = 0;
		if(source != null){
			for(int i = 0; i<list.size(); i++){
				Pattern pt = Pattern.compile(new String(list.get(i).data), Pattern.CASE_INSENSITIVE);
				Matcher mt = pt.matcher(source);
				while(mt.find()) res ++;
			}
			
		}
		return res;
		
	}
	
	public void clear(){
		list.clear();
		list_pos = 0;
	}

}
