package net.smsfs.list;

import java.sql.Connection;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.ConcurrentHashMap;

import net.config.ConfigManagerV2;
import net.config.FNConfigData;
import net.db.DBManager;
import net.logging.LoggingManager;
import net.smsfs.list.lists.DictionaryList;
import net.smsfs.list.lists.LDSpamList;
import net.smsfs.list.lists.MD5NormalList;
import net.smsfs.list.lists.MD5QuarantineList;
import net.smsfs.list.lists.SmsfsList;

import org.apache.log4j.Logger;


public class ListManagerV2 {
	private static Logger logger=Logger.getLogger(ListManagerV2.class);
	public static ConcurrentHashMap<String, ListBase> list_map;

	private static DbSync db_sync_r;
	private static Thread db_sync_t;
	private static boolean stopping;
	
	private static class DbSync implements Runnable{
		public void run() {
			String sql = null;
			Connection con = null;
			Statement st = null;
			Statement st2 = null;
			PreparedStatement prep_st = null;
			ResultSet res = null;
			ResultSet res2 = null;
			ListBase smsfs_lst = null;
			String tmp = null;
			ListItemBase li = null;
			int tmp_lst_id;
			List<ListItemBase> tmp_lst = null;
			LoggingManager.info(logger, "Starting...");
			while(!stopping){
				try{ Thread.sleep(FNConfigData.smsfs_lists_sync_interval); }catch(Exception e){ e.printStackTrace();	}
				// get connection
				con = DBManager.get_connection();
				if(con != null){
					try{
						// create new lists
						prep_st = con.prepareStatement("insert into smsfs_list(name) values(?)");
						if(prep_st != null){
							for(String id : list_map.keySet()){
								smsfs_lst = list_map.get(id);
								prep_st.setString(1, smsfs_lst.list_type_id);
								try{
									prep_st.execute();
								}catch(Exception e){
									// do nothing
								}
							}
							prep_st.close();
						}
						// sync
						st = con.createStatement();
						if(st != null){
							sql = "select name, id from smsfs_list";
							res = st.executeQuery(sql);
							while(res.next()){
								tmp = res.getString(1).toUpperCase();
								tmp_lst_id = res.getInt(2);
								// crete new list 
								if(!list_exists(ListType.SMSFS_LIST, tmp)){
									smsfs_lst = new SmsfsList(tmp, FNConfigData.smsfs_lists_max);
									add_list(smsfs_lst);
									//System.out.println("Adding list: " + tmp);
								// list ecists	
								}else{
									smsfs_lst = get_list(ListType.SMSFS_LIST, tmp);
									//System.out.println("List exists: " + tmp);
									
								}
								// remove deleted from db
								if(smsfs_lst != null){
									// process deleted
									tmp_lst = smsfs_lst.get_list_desc(false).list;
									prep_st = con.prepareStatement("delete from smsfs_list_item where name = ?");
									for(ListItemBase tmp_lst_item : tmp_lst){
										if(tmp_lst_item.deleted){
											//System.out.println("Deleting: " + new String(tmp_lst_item.data));
											// remove from db
											prep_st.setString(1, new String(tmp_lst_item.data));
											prep_st.execute();
											// remove from list
											tmp_lst.remove(tmp_lst_item);
										}
									}
									prep_st.close();
									
									// add new to list
									st2 = con.createStatement();
									smsfs_lst.get_list_desc(false).reset_updated();
									sql = "select name from smsfs_list_item where smsfs_list_id = " + tmp_lst_id;
									res2 = st2.executeQuery(sql);
									while(res2.next()){
										if((li = smsfs_lst.exists(res2.getString(1).getBytes(), false)) != null){
											li.updated = true;
											//System.out.println("EXISTS");
										}else{
											smsfs_lst.add(res2.getString(1).getBytes(), false, false);
											// special on_add action
											smsfs_lst.on_add(res2.getString(1).getBytes());
											//System.out.println("ADD");
										}
									}
									
									// remove from list if deleted in db
									smsfs_lst.get_list_desc(false).clear_obsolete(smsfs_lst);
									// check for explicit entries, should be added to db
									tmp_lst = (ArrayList<ListItemBase>)smsfs_lst.get_list_desc(false).get_explicit();
									if(tmp_lst != null){
										if(tmp_lst.size() > 0){
											for(int i = 0; i<tmp_lst.size(); i++){
												prep_st = con.prepareStatement("insert into smsfs_list_item(smsfs_list_id, name) values(?, ?)");
												if(prep_st != null){
													li = tmp_lst.get(i);
													prep_st.setInt(1, tmp_lst_id);
													prep_st.setString(2, new String(li.data));
													try{
														prep_st.execute();
													}catch(Exception e){
														// do nothing
													}
													// mark as done
													li.explicit = false;
													prep_st.close();
													
												}
											}
											tmp_lst = null;
											
										}
									}
									

								}
								
								st2.close();
							}
							st.close();
							// release connection
							DBManager.offer_connection(con);
						}
						
					}catch(Exception e){
						e.printStackTrace();
						if(con != null)	DBManager.offer_connection(con);
					}
					
					
				}else{
					LoggingManager.warn(logger, "Cannot acquire free DB connection!");
					
				}
			}
			LoggingManager.info(logger, "Ending...");
		}
		
	}
	
	public static ListBase get_list_manager(ListType type){
		return list_map.get(type + ":" + null);
	}
	public static ListBase get_list(ListType type, String id){
		return list_map.get(type + ":" + id); 
	}
	
	public static boolean list_exists(ListType type, String id){
		return list_map.get(type + ":" + id) != null;
	}
	public static void add_list(ListBase list){
		list_map.put(list.list_type + ":" + list.list_type_id, list);
	}
	public static void del_list(ListType list_type, String list_id){
		list_map.remove(list_type + ":" + list_id);
	}
	
	public static void init_predefined_lists(){
		LoggingManager.info(logger, "Initializing SMSFS predefined lists...");
		LoggingManager.info(logger, "Initializing SMSFS list = [" + ListId.LD_SPAM + "]...");
		add_list(new LDSpamList(FNConfigData.smsfs_spam_max));
		LoggingManager.info(logger, "Initializing SMSFS list = [" + ListId.DICTIONARY + "]...");
		add_list(new DictionaryList(FNConfigData.smsfs_lists_max));
		LoggingManager.info(logger, "Initializing SMSFS list = [" + ListId.MD5_NORMAL + "]...");
		add_list(new MD5NormalList(FNConfigData.smsfs_md5_max));
		LoggingManager.info(logger, "Initializing SMSFS list = [" + ListId.MD5_QUARANTINE + "]...");
		add_list(new MD5QuarantineList(FNConfigData.smsfs_quarantine_max));
		
		
	}
	public static void init_user_lists(){
		String tmp = null;
		String[] tmp_lst = null;
		LoggingManager.info(logger, "Initializing SMSFS user lists...");
		// init user lists
		tmp = ConfigManagerV2.get("smsfs.lists");
		tmp_lst = tmp.split(":");
		for(int i = 0; i<tmp_lst.length; i++){
			if(tmp_lst[i].trim().length() > 0){
				add_list(new SmsfsList(tmp_lst[i].toUpperCase(), FNConfigData.smsfs_lists_max));
				logger.info("SMSFS User list [" + tmp_lst[i] + "] initialized...");
				
			}
		}
		
	}
	public static void init(){
		LoggingManager.info(logger, "Initializing SMSFS List Manager...");
		list_map = new ConcurrentHashMap<String, ListBase>();
		init_predefined_lists();
		init_user_lists();
		
		db_sync_r = new DbSync();
		db_sync_t = new Thread(db_sync_r, "LIST_MANAGER_DB_SYNC");
		db_sync_t.start();
		
	}
}
