package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class EMLPP_Priority extends INTEGER{

	public EMLPP_Priority(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
