package net.hplmnr;

import net.smstpdu.AddressString;

public class MTCorrelationPacket {
	public String imsi; 
	public AddressString nnn;
	public AddressString annn;
	public boolean hasDialogue;
	
	public long ts;

}
