package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class NUMERICSTRING extends ASNType {

	public NUMERICSTRING(){
		super();
		universalTag = ASNTag.NUMERICSTRING;
		tag = ASNTag.name2Tag(universalTag.toString());
		
	}
}
