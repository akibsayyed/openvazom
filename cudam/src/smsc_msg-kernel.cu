__global__ void SMSC_MSG(char *data_lst, int *data_lst_size, int *max_msg_size, int *max_smsc_size, char *msg, int *msg_length, char *smsc, int *smsc_length, char *result){
// message
// MD5 hash list
        char ch;
        char ch2;
        volatile unsigned int thread_id = (blockIdx.y * gridDim.x*blockDim.x) + (blockIdx.x*blockDim.x) + threadIdx.x;
        volatile unsigned int lst_msg_id = thread_id * (*max_msg_size + *max_smsc_size + 2); // +2 for msg length and smsc length, one byte each
        int lst_msg_length;
	int lst_smsc_length;
	int lst_msg_data_start;
	
        // check if current thread id is >= data_lst_size
        if(thread_id >= *data_lst_size) return;
        // get message length
        lst_smsc_length = data_lst[lst_msg_id] & 0xff;
        lst_msg_length = data_lst[lst_msg_id + lst_smsc_length + 1] & 0xff;
	// check smsc length
	if(lst_smsc_length != *smsc_length){
		result[thread_id] = 0;
		return; 
	}
	// check message length
	if(lst_msg_length != *msg_length){
		result[thread_id] = 0;
		return;
	}
	// check message data
	// message data start position
	msg_data_start = lst_msg_id + lst_smsc_length + 2;
	for(int i = 0; i<*lst_msg_length; i++) if(data_lst[msg_data_start + i] != msg[i]){
		result[thread_id] = 0;
		return;
	}
	
	// if all the above checks out, messages are 100% match
	result[thread_id] = 1;

}
