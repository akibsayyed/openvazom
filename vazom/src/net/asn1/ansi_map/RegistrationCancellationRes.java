package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RegistrationCancellationRes extends SET{

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(0).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CancellationDenied get_cancellationDenied(){
		return (CancellationDenied)elements.get(1).data;
	}
	public CancellationDenied new_cancellationDenied(){
		return new CancellationDenied();
	}

	public ControlChannelData get_controlChannelData(){
		return (ControlChannelData)elements.get(2).data;
	}
	public ControlChannelData new_controlChannelData(){
		return new ControlChannelData();
	}

	public ReceivedSignalQuality get_receivedSignalQuality(){
		return (ReceivedSignalQuality)elements.get(3).data;
	}
	public ReceivedSignalQuality new_receivedSignalQuality(){
		return new ReceivedSignalQuality();
	}

	public SMS_MessageWaitingIndicator get_sms_MessageWaitingIndicator(){
		return (SMS_MessageWaitingIndicator)elements.get(4).data;
	}
	public SMS_MessageWaitingIndicator new_sms_MessageWaitingIndicator(){
		return new SMS_MessageWaitingIndicator();
	}

	public SystemAccessData get_systemAccessData(){
		return (SystemAccessData)elements.get(5).data;
	}
	public SystemAccessData new_systemAccessData(){
		return new SystemAccessData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(57, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cancellationDenied(); }});
		elements.add(new ElementDescriptor(55, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelData(); }});
		elements.add(new ElementDescriptor(72, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_receivedSignalQuality(); }});
		elements.add(new ElementDescriptor(118, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_MessageWaitingIndicator(); }});
		elements.add(new ElementDescriptor(56, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemAccessData(); }});
	}
	public RegistrationCancellationRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
