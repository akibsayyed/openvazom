package net.sccp.parameters.protocol_class;

import java.util.HashMap;

public enum ProtocolClassType {
	CLASS0(0x00),
	CLASS1(0x01),
	CLASS2(0x02),
	CLASS3(0x03);
	
	// bits 3..0
	private int id;
	private static final HashMap<Integer, ProtocolClassType> lookup = new HashMap<Integer, ProtocolClassType>();
	static{
		for(ProtocolClassType td : ProtocolClassType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static ProtocolClassType get(int id){ return lookup.get(id); }
	private ProtocolClassType(int _id){ id = _id; }
	
}
