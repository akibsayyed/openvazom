package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Sc_interface_version extends OptionalParameterBase {

	public Sc_interface_version(){
		type = OptionalParameterType.SC_INTERFACE_VERSION;
	}
	public int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
