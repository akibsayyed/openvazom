package net.mtp3.messages.signalling;

public class Changeover extends SignallingMessageBase {
	public int forwardSequenceNumber;
	
	public Changeover(){
		super();
	}
	
	
	public void init(byte[] data) {
		super.init(data);
		// FSN
		forwardSequenceNumber = (data[byte_pos] & 0x7f) >> 1;
		
		
	}
}
