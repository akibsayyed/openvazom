package net.routing;

public class RoutingStats {
	public long IN;
	public long OUT;
	public long BYTES;

	public double AVG_PPS; 
	public double AVG_MBIT_S; 
	public double AVG_BYTES_S;
	
	public long ts;
	
	public void reset(){
		IN = 0;
		OUT = 0;
		AVG_MBIT_S = 0;
		AVG_PPS = 0;
		AVG_BYTES_S = 0;
		
		BYTES = 0;
		ts = System.currentTimeMillis();
	}
}
