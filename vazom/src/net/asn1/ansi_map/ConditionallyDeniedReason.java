package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ConditionallyDeniedReason extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _waitable =  1 ;
	public ConditionallyDeniedReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
