package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MessageDirective extends SET{

	public MessageWaitingNotificationCount get_messageWaitingNotificationCount(){
		return (MessageWaitingNotificationCount)elements.get(0).data;
	}
	public MessageWaitingNotificationCount new_messageWaitingNotificationCount(){
		return new MessageWaitingNotificationCount();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(1).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(2).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(92, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_messageWaitingNotificationCount(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
	}
	public MessageDirective(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
