package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NotifyBehaviour extends CHOICE{

	public NULL get_notifyImmediate(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_notifyImmediate(){
		return new NULL();
	}

	public RegulatedEmbeddedDescriptor get_notifyRegulated(){
		return (RegulatedEmbeddedDescriptor)elements.get(1).data;
	}
	public RegulatedEmbeddedDescriptor new_notifyRegulated(){
		return new RegulatedEmbeddedDescriptor();
	}

	public NULL get_neverNotify(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_neverNotify(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_notifyImmediate(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_notifyRegulated(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_neverNotify(); }});
	}
	public NotifyBehaviour(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
