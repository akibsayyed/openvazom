package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Invoke extends SEQUENCE{

	public OCTET_STRING get_componentIDs(){
		return (OCTET_STRING)elements.get(0).data;
	}
	public OCTET_STRING new_componentIDs(){
		return new OCTET_STRING();
	}

	public OPERATION get_operationCode(){
		return (OPERATION)elements.get(1).data;
	}
	public OPERATION new_operationCode(){
		return new OPERATION();
	}

	public PARAMETER get_parameter(){
		return (PARAMETER)elements.get(2).data;
	}
	public PARAMETER new_parameter(){
		return new PARAMETER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(15, ASNTagClass.PRIVATE, true, false){public void set(){ data = new_componentIDs(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_operationCode(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_parameter(); }});
	}
	public Invoke(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
