package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Modify extends SET{

	public DatabaseKey get_databaseKey(){
		return (DatabaseKey)elements.get(0).data;
	}
	public DatabaseKey new_databaseKey(){
		return new DatabaseKey();
	}

	public ModificationRequestList get_modificationRequestList(){
		return (ModificationRequestList)elements.get(1).data;
	}
	public ModificationRequestList new_modificationRequestList(){
		return new ModificationRequestList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(252, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_databaseKey(); }});
		elements.add(new ElementDescriptor(263, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_modificationRequestList(); }});
	}
	public Modify(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
