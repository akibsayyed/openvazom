package net.vstp;

public enum FilterResultType {
	ALLOW,
	DENY;
}
