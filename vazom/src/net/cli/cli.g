/*
 CLI generator v1.0 
*/

grammar cli;


options{
  output=AST;
  language=Java;
  backtrack=true;
}

tokens {
	L_PAREN					= '(';
	R_PAREN					= ')';
	R_SQ_B					= ']';
	L_SQ_B					= '[';
	L_CR_B					= '{';
	R_CR_B					= '}';
	ANNT					= '@';
	EQUAL					= '==';
	ASSIGN					= '=';
	OR					= '||';
	NEQUAL					= '!=';
	COLON					= ':';
	STMTSEP					= ';';
	PERCENT					= '%';
	HEX_P					= '0x';
	LT					= '<';
	GT					= '>';
	LTE					= '<=';
	GTE					= '>=';
	PLUS					= '+';
	MINUS					= '-';
	AND					= '&&';
	OR					= '||';
	ASSIGN					= '=';
	SQUOTE					= '\'';
	REGEX_BLOCK				= '/';
	ASTERISK				= '*';
	COMMA					= ',';
	// AST
	CLI_FINAL;
	CLI_COMPLEX;
	CLI_METHOD;
	CLI_DEPTH;
	CLI_PARENT;
	CLI_PARAMS;
	CLI_PARAM;
	CLI_DESC;
}
@parser::header{
package net.cli;
}
@lexer::header{
package net.cli;
}


@members{
  int depth = 0;
  String parent = "";
  ArrayList<String> parent_lst = new ArrayList<String>();
}
input
  : (cliitem)* EOF!
  ;

finalitem
  : cmd=WORD WS* dsc=desc* WS* COLON WS* method=WORD WS* STMTSEP 
  -> ^(CLI_FINAL $cmd $method ^(CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll("[\\[\\] ]", "")]) ^(CLI_DESC ^(CLI_DESC[$dsc.res])))
  | cmd=WORD WS* dsc=desc* WS* L_SQ_B params* R_SQ_B WS* COLON WS* method=WORD WS* STMTSEP 
  -> ^(CLI_FINAL $cmd $method ^(CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll("[\\[\\] ]", "")]) ^(CLI_PARAMS params*) ^(CLI_DESC ^(CLI_DESC[$dsc.res])))
  ;

params
  : WORD COLON!*
  ;

complexitem
  : cmd=WORD { parent_lst.add(0, $cmd.text); } WS* dsc=desc* WS* curlyblock 
  -> ^(CLI_COMPLEX^($cmd curlyblock) ^(CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll("[\\[\\] ]", "")]) ^(CLI_DESC ^(CLI_DESC[$dsc.res])))
  | cmd=WORD { parent_lst.add(0, $cmd.text); } WS* dsc=desc* WS* L_SQ_B params* R_SQ_B WS* curlyblock 
  -> ^(CLI_COMPLEX^($cmd curlyblock) ^(CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll("[\\[\\] ]", "")]) ^(CLI_PARAMS params*) ^(CLI_DESC ^(CLI_DESC[$dsc.res])))
  ;

curlyblock
  : L_CR_B! cliitem* R_CR_B! { parent_lst.remove(0); }
  ;

cliitem
  : finalitem 
  | complexitem
  ;

SL_COMMENT
  : '//' (options {greedy=false;} : .)* '\n' {$channel=HIDDEN;}
  ;

desc returns [String res]
  : { $res = ""; }L_PAREN! (a=WORD { $res += $a.text + " "; }| WS)* R_PAREN!
  ;

WORD
  : ('a'..'z'|'A'..'Z'|'0'..'9'|'.'|'_')*
  ;


WS
  : (' ' | '\t' | '\r' | '\n'){$channel=HIDDEN;}
  ;


