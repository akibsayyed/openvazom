package net.fn.cmdi;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import net.config.ConfigManagerV2;
import net.config.FNConfigData;
import net.db.DBManager;
import net.db.DBRecordSMS;
import net.fn.FNode;
import net.fn.SGDescriptor;
import net.fn.SGNManager;
import net.gpu.GPUBalancer;
import net.gpu.NodeDescriptor;
import net.smsfs.SmsfsManager;
import net.stats.CMDIStats;
import net.stats.StatsManager;
import net.utils.Utils;

import org.apache.log4j.Logger;


public class CMDIListener {
	public static Logger logger=Logger.getLogger(CMDIListener.class);
	
	private static ServerSocket tcp;
	private static int listener_port;
	private static ConcurrentHashMap<String, CMDIConnection> connection_lst;
	private static Listener_r listener_r;
	private static Thread listener_t;
	private static boolean stopping;
	public static CMDIStats STATS;
	
	
	
	private static class Listener_r implements Runnable{
		private Socket connection;
		private CMDIConnection con;
		
		public void run() {
			while(!stopping){
				try{
					connection = tcp.accept();
					con = new CMDIConnection(connection, connection_lst);
					connection_lst.put(connection.getInetAddress().toString() + ":" + connection.getPort(), con);
					
					logger.info("NEW CONNECTION TO CMDI! [ " + connection.getInetAddress().toString() + ":" + connection.getPort() + " ]");
					
				}catch(Exception e){
					logger.error("ERROR WHILE ACCEPTING NEW CONNECTION TO CMDI! [ " + listener_port + " ]");
					e.printStackTrace();
					stopping = true;
				}
			}			
		}
		
		
	}
	
	
	
	private static void init_listener_thread(){
		listener_r = new Listener_r();
		listener_t = new Thread(listener_r, "CMDI_LISTENER");
		listener_t.start();
		
	}
	
	private static void init_server(int port){
		try{
			tcp = new ServerSocket(port);
			listener_port = port;
			logger.info("CMDI INITIALIZED ! [ " + port + " ]");
			init_listener_thread();
			
		}catch(Exception e){
			logger.error("ERROR WHILE INITIALIZING CMDI! [ " + port + " ]");
			e.printStackTrace();
			
		}
		
		
	}
	public static synchronized String process_cmd_raw(String cmd){
		String res = new String();
		DBRecordSMS dbr = null;
		if(cmd.toUpperCase().startsWith("CONF")){
			String[] sp = cmd.split(" ");
			String val;
			if(sp.length >= 3){
				if(sp[1].equalsIgnoreCase("GET")){
					val = ConfigManagerV2.get(sp[2]);
					if(val != null) res = "CONF Item: [" + sp[2] + "], value: [" + val + "]!";
				}else if(sp[1].equalsIgnoreCase("SET") && sp.length == 4){
					val = ConfigManagerV2.get(sp[2]);
					if(val != null){
						ConfigManagerV2.set(sp[2], sp[3], null);
						FNConfigData.set(sp[2], sp[3]);
						res = "CONF Item: [" + sp[2] + "], value: [" + FNConfigData.get(sp[2]) + "] set!";
					}else res = "Invalid CONF Item!";
					
				}else res = "Invalid CONF statement!";
			}else res = "Invalid CONF statement!";
		}else if(cmd.toUpperCase().startsWith("DB")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length > 1){
				if(sp[1].equalsIgnoreCase("INFO")){
					res += "DB Info:\n";
					res += "Name: [" + FNConfigData.db_name + "], Host: [" + FNConfigData.db_host + "], Port: [" + FNConfigData.db_port + "], Username: [" + FNConfigData.db_username + "], Password: [" + FNConfigData.db_password + "]!\n";
					res += "SMS buffer size: [" + DBManager.dbq_sms.size() + "]!\n";
					res += "SRI buffer size: [" + DBManager.dbq_sri.size() + "]!\n";
				}else if(sp[1].equalsIgnoreCase("FLUSH")){
					res += "Flushing DB buffer...\n";
					res += "SMS buffer size: [" + DBManager.dbq_sms.size() + "]!\n";
					res += "SRI buffer size: [" + DBManager.dbq_sri.size() + "]!\n";
					DBManager.buffer_flush();
				}else res = "Unknown DB statement!";			
			}else res = "Unknown DB statement!";			
			
			
		}else if(cmd.toUpperCase().startsWith("SG")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length > 1){
				if(sp[1].equalsIgnoreCase("INFO")){
					SGDescriptor sgd = null;
					res += "SG(Signaling) Nodes:\n";
					for(String k : SGNManager.sgn_map.keySet()){
						sgd = SGNManager.sgn_map.get(k);
						res += "Node ID: [" + sgd.id + "], host: [" + sgd.address + "], port: [" + sgd.port + "], active: [" + sgd.active + "]!\n";  
						
					}
				}			
			}			
		}else if(cmd.toUpperCase().startsWith("GPUB")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length > 1){
				if(sp[1].equalsIgnoreCase("INFO") && FNConfigData.gpub_status){
					NodeDescriptor nd = null;
					res += "GPU Nodes:\n";
					for(int i = 0; i<GPUBalancer.node_lst.size(); i++){
						nd = GPUBalancer.node_lst.get(i);
						res += "Node ID: [" + nd.id + "], host: [" + nd.host + "], port: [" + nd.port + "], gpu count: [" + (nd.connections.size()-1) + "]!\n";  
					}
				}else if(sp[1].equalsIgnoreCase("START")){
					GPUBalancer.init();
					FNConfigData.gpub_status = true;
					res = "Starting GPU Balancer...\n";
					
				}else if(sp[1].equalsIgnoreCase("STOP")){
					GPUBalancer.stop();
					FNConfigData.gpub_status = false;
					res = "Stopping GPU Balancer...\n";
				}else if(sp[1].equalsIgnoreCase("STATUS")){
					res = "GPU Balancer status: [" + (FNConfigData.gpub_status ? "ACTIVE" : "INACTIVE") + "]!";
					
				}else res = "Unknown GPUB Statement!";
			}else res = "GPUB Statement missing!";
		}else if(cmd.toUpperCase().startsWith("FILTER GET")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length == 3){
				if(sp[2].equalsIgnoreCase("XML")){
					res += SmsfsManager.currentFilter2xml();
				}else res += SmsfsManager.current_filter_data;
			}else res += SmsfsManager.current_filter_data;

		}else if(cmd.toUpperCase().startsWith("REFILTER")){
			String[] sp = cmd.toUpperCase().split(" ", 3);
			if(sp.length >= 2){
				if(sp[1].equalsIgnoreCase("STATUS")){
					res += "Retained SMS Refiltering status: [ " + (DBManager.refiltering_active ? "ACTIVE" : "INACTIVE") + " ]!";
					
				}else if(sp[1].equalsIgnoreCase("POS")){
					res += "Retained SMS Refiltering position (CURRENT ID / MAX ID): [ " + StatsManager.REFILTER_STATS.POSITION + " / " + StatsManager.REFILTER_STATS.MAX_ID +  " ]!";
					
				}else if(sp[1].equalsIgnoreCase("STOP")){
					DBManager.stopRefiltering();
					res += "Stopping Retained SMS Refiltering process...";
				}else if(sp[1].equalsIgnoreCase("TIME")){
					if(DBManager.refiltering_active){
						res += "Current Retained SMS Refiltering elapsed time, still working...: [ " + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.REFILTER_STATS.START_TIME) +  " ]!";
						
					}else{
						res += "Last Retained SMS Refiltering elapsed time: [ " + Utils.tsElapsed2String(StatsManager.REFILTER_STATS.ELAPSED_TIME) +  " ]!";
						
					}

				}else if(sp[1].equalsIgnoreCase("START")){
					if(!DBManager.refiltering_active){
						String[] params = null;
						if(sp.length == 3){
							params = sp[2].split(",");
							res += "Retained SMS Refiltering timespan: [ " + params[0].trim() + " - " + params[1].trim() + " ]!" + "\n";
							DBManager.refilterRetainedSMS(params[0].trim(), params[1].trim());
						}else DBManager.refilterRetainedSMS(null, null);
						res += "Starting Retained SMS Refiltering process...";
					}else res = "Retained SMS Refiltering process already started!";
				}else res = "Unknown refilter method!";
				
			}else res = "Refilter method missing!";
			
		}else if(cmd.toUpperCase().startsWith("SMS")){
			String[] sp = cmd.toUpperCase().split(" ");
			dbr = DBManager.getSMSByID(Long.parseLong(sp[1]), null);
			if(dbr != null){
				res += "Id: " + dbr.id + "\n";
				res += "Direction: " + dbr.direction + "\n";
				res += "Type: " + dbr.type + "\n";
				res += "GT Called: " + dbr.gt_called + "\n";
				res += "GT Calling: " + dbr.gt_calling + "\n";
				res += "Imsi: " + dbr.imsi + "\n";
				res += "Msisdn: " + dbr.msisdn + "\n";
				res += "Scda: " + dbr.scda + "\n";
				res += "Scoa: " + dbr.scoa + "\n";
				res += "Sms destination: " + dbr.sms_destination + "\n";
				res += "Sms destination enc: " + dbr.sms_destination_enc + "\n";
				res += "Sms originating: " + dbr.sms_originating + "\n";
				res += "Sms originating enc: " + dbr.sms_originating_enc + "\n";
				res += "Sms text: " + dbr.sms_text + "\n";
				res += "Sms text enc: " + dbr.sms_text_enc + "\n";
				res += "Tcap sid: " + dbr.tcap_sid + "\n";
				res += "Tcap did: " + dbr.tcap_did + "\n";
				
			}else res = "SMS not found!";
			
		}else if(cmd.equalsIgnoreCase("show memory")){
			double used_mem =  (double)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024/1024;
			double total_mem =  (double)Runtime.getRuntime().totalMemory() / 1024/1024;
			res = String.format("[ %7.2fMb / %7.2fMb ]", used_mem, total_mem);
			
		}else if(cmd.toUpperCase().startsWith("SHOW STATS")){
			String[] sp = cmd.toUpperCase().split(" +");
			if(sp.length > 2){
				String p = sp[2];
				if(p.equals("VSTP")){
					res += "VSTP_SG_CONNECTION_LOST COUNT: " + StatsManager.VSTP_STATS.VSTP_SG_CONNECTION_LOST + "\n";
					res += "VSTP_NO_SG_NODES COUNT: " + StatsManager.VSTP_STATS.VSTP_NO_SG_NODES + "\n";
					
				}else if(p.equals("DB")){
					res += "DB_CONNECTION_LOST COUNT: " + StatsManager.DB_STATS.DB_CONNECTION_LOST + "\n";
					
					res += "DB_SMS_BATCH_COUNT COUNT: " + StatsManager.DB_STATS.DB_SMS_BATCH_COUNT + "\n";
					res += "DB_SRI_BATCH_COUNT COUNT: " + StatsManager.DB_STATS.DB_SRI_BATCH_COUNT + "\n";
					
					res += "LAST_SMS_BATCH_ELAPSED TIME(msec): " + StatsManager.DB_STATS.LAST_SMS_BATCH_ELAPSED + "\n";
					res += "LAST_SRI_BATCH_ELAPSED TIME(msec): " + StatsManager.DB_STATS.LAST_SRI_BATCH_ELAPSED + "\n";

					res += "MAX_SMS_BATCH_ELAPSED TIME(msec): " + StatsManager.DB_STATS.MAX_SMS_BATCH_ELAPSED + "\n";
					res += "MAX_SRI_BATCH_ELAPSED TIME(msec): " + StatsManager.DB_STATS.MAX_SRI_BATCH_ELAPSED + "\n";

				}else if(p.equals("GPU")){
					if(FNConfigData.gpub_status){
						try{
							String rep_node = sp[3].split(":")[0];
							String rep_gpu = sp[3].split(":")[1];
							String gpu_stats_line = null;
							String[] gpu_result = null;
							try{
								Socket gpu_socket = new Socket(GPUBalancer.getNodeDescriptor(rep_node).host, GPUBalancer.getNodeDescriptor(rep_node).port);
								gpu_socket.setSoTimeout(10000);
								BufferedReader gpu_in = new BufferedReader(new InputStreamReader(gpu_socket.getInputStream()));
								PrintWriter gpu_out = new PrintWriter(gpu_socket.getOutputStream());
								gpu_out.println("S." + rep_gpu);
								gpu_out.flush();
								gpu_stats_line = gpu_in.readLine();
								gpu_result = gpu_stats_line.split(":");
								res += "KERNEL EXECUTION COUNT: " + gpu_result[0] + "\n";
								res += "LAST KERNEL EXECUTION TIME: " + gpu_result[1] + "msec\n";
								res += "MAX KERNEL EXECUTION TIME: " + gpu_result[2] + "msec\n";
								res += "CURRENT UPDATE QUEUE SIZE: " + gpu_result[3] + "\n";
								res += "CURRENT LD QUEUE SIZE: " + gpu_result[4] + "\n";
								res += "CURRENT MD5 QUEUE SIZE: " + gpu_result[5] + "\n";
								
								gpu_socket.close();
							}catch(Exception e){
								res = "Node connection not available...";
								e.printStackTrace();
							}
							
						}catch(Exception e){
							res = "Invalid statement...";
						}
						
					}else res = "GPUB module if OFF";
				}else if(p.equals("GPUB")){
					if(FNConfigData.gpub_status){
						// LD stats
						res += "LD_REQUEST COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LD_REQUEST_COUNT) + "\n";
						res += "LD_REPLY_OK COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LD_REPLY_OK_COUNT) + "\n";
						res += "LD_REPLY_ERROR COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LD_REPLY_ERROR_COUNT) + "\n";
						res += "LD_MAX_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.LD_MAX_REPLY_TIME) + "msec\n";
						res += "LD_LAST_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME) + "msec\n";
						res += "LD_CONNECTION_DOWN COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LD_CONNECTION_DOWN_COUNT) + "\n";
						res += "LD_NODE_BUSY COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT) + "\n";
						// UPDATE stats
						res += "UPDATE_REQUEST COUNT: "  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REQUEST_COUNT) + "\n";
						res += "UPDATE_REPLY_OK COUNT: "  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REPLY_OK_COUNT) + "\n";
						res += "UPDATE_REPLY_ERROR COUNT: "  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REPLY_ERROR_COUNT) + "\n";
						res += "UPDATE_MAX_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.UPDATE_MAX_REPLY_TIME) + "msec\n";
						res += "UPDATE_LAST_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME) + "msec\n";
						res += "UPDATE_CONNECTION_DOWN COUNT: "  + Long.toString(StatsManager.GPUB_STATS.UPDATE_CONNECTION_DOWN_COUNT) + "\n";

						// MD5 stats
						res += "MD5_REQUEST COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_REQUEST_COUNT) + "\n";
						res += "MD5_REPLY_OK COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_REPLY_OK_COUNT) + "\n";
						res += "MD5_REPLY_ERROR COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_REPLY_ERROR_COUNT) + "\n";
						res += "MD5_MAX_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.MD5_MAX_REPLY_TIME) + "msec\n";
						res += "MD5_LAST_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME) + "msec\n";
						res += "MD5_CONNECTION_DOWN COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_CONNECTION_DOWN_COUNT) + "\n";
						res += "MD5_NODE_BUSY COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_NODE_BUSY_COUNT) + "\n";
						// MD5 UPDATE stats
						res += "MD5_UPDATE_REQUEST COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REQUEST_COUNT) + "\n";
						res += "MD5_UPDATE_REPLY_OK COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_OK_COUNT) + "\n";
						res += "MD5_UPDATE_REPLY_ERROR COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_ERROR_COUNT) + "\n";
						res += "MD5_UPDATE_MAX_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_MAX_REPLY_TIME) + "msec\n";
						res += "MD5_UPDATE_LAST_REPLY TIME: "  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME) + "msec\n";
						res += "MD5_UPDATE_CONNECTION_DOWN COUNT: "  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_CONNECTION_DOWN_COUNT) + "\n";
						// LST REMOVE stats
						res += "LST_REMOVE COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LST_REMOVE_COUNT) + "\n";
						res += "LST_REMOVE_ERROR COUNT: "  + Long.toString(StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT) + "\n";
						
					}else res = "GPUB module if OFF";
				}else if(p.equals("SMSFS")){
					if(FNConfigData.smsfs_status){
						res += "\u001B[1;37mINTRUSIVE ACCEPTED COUNT: "  + Long.toString(StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED) + "\n";
						res += "\u001B[1;37mINTRUSIVE REJECTED COUNT: "  + Long.toString(StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED) + "\n";
						res += "\u001B[1;37mNON INTRUSIVE ACCEPTED COUNT: "  + Long.toString(StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED) + "\n";
						res += "\u001B[1;37mNON INTRUSIVE REJECTED COUNT: "  + Long.toString(StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED) + "\n";
					}else res = "SMSFS if OFF!";
				}

				
			}else res = "Unknown stats object!";
			
		}else if(cmd.equalsIgnoreCase("UPTIME")){
			res = "UPTIME: " + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.SYSTEM_STATS.STARTUP_TS);
			
		}else if(cmd.equalsIgnoreCase("ping")){
			res = "1";
			STATS.LAST_PING_TS = System.currentTimeMillis();
		}else res = "Unknown statement!" + (char)13;
		
		return res;
		
		

	}

	
	public static synchronized String process_cmd(String cmd){
		/*
			\u001B[1;37m white
			\u001B[1;31m red
			\u001B[1;32m green
			\u001B[1;33m yellow
			\u001B[1;34m blue
		 */
		String res = new String();
		DBRecordSMS dbr = null;
		if(cmd.equalsIgnoreCase("help")){
			res = "\u001B[1;34mSHOW MEMORY" + "\n" + (char)13 +
				"SHOW STATS \u001B[1;37m<\u001B[1;32mDB\u001B[1;37m |\u001B[1;32m VSTP\u001B[1;37m | \u001B[1;32mSMSFS \u001B[1;37m| \u001B[1;32mGPUB \u001B[1;37m| \u001B[1;32m(GPU NODE_ID:GPU_ID)\u001B[1;37m>" + "\n" + (char)13 +			
				"\u001B[1;34mGPUB \u001B[1;37m<\u001B[1;32mSTART \u001B[1;37m| \u001B[1;32mSTOP \u001B[1;37m| \u001B[1;32mSTATUS \u001B[1;37m| \u001B[1;32mINFO\u001B[1;37m>" + "\n" +	(char)13 +		
				"\u001B[1;34mSG \u001B[1;37m<\u001B[1;32mINFO\u001B[1;37m>" + "\n" + (char)13 +			
				"\u001B[1;34mDB \u001B[1;37m<\u001B[1;32mINFO\u001B[1;37m | \u001B[1;32mFLUSH\u001B[1;37m>" + "\n" + (char)13 +			
				"\u001B[1;34mCONF \u001B[1;37m<\u001B[1;32mGET [ITEM_ID] \u001B[1;37m| \u001B[1;32mSET [ITEM_ID] [VALUE]\u001B[1;37m>" + "\n" + (char)13 +			
				"\u001B[1;34mUPTIME" + "\n" + (char)13 +		
				"\u001B[1;34mSMS \u001B[1;37m<\u001B[1;32mSMS_ID\u001B[1;37m>" + "\n" +	(char)13 +		
				"\u001B[1;34mFILTER \u001B[1;37m<\u001B[1;32mSTART \u001B[1;37m| \u001B[1;32mEND \u001B[1;37m| \u001B[1;32mACTIVATE \u001B[1;37m| \u001B[1;32mSAVE \u001B[1;37m| \u001B[1;32mGET \u001B[1;32m[XML]\u001B[1;37m>" + "\n" + (char)13 +			
				"\u001B[1;34mREFILTER \u001B[1;37m<\u001B[1;32mSTATUS \u001B[1;37m| \u001B[1;32mSTART [YYYY-MM-DD HH:MM:SS, YYYY-MM-DD HH:MM:SS] \u001B[1;37m| \u001B[1;32mSTOP \u001B[1;37m| \u001B[1;32mPOS \u001B[1;37m| \u001B[1;32mTIME\u001B[1;37m>" + "\n" +	(char)13 +		
				"\u001B[1;34mVERSION" + "\n" + (char)13 +			
				"\u001B[1;34mBYE" + "\n" + (char)13 +
				"\u001B[1;34mHALT" + "\n" + (char)13;
		}else if(cmd.toUpperCase().equalsIgnoreCase("VERSION")){
			res = "\u001B[1;37mVAZOM F VERSION: [\u001B[1;32m" + FNode.class.getPackage().getSpecificationVersion() + ".\u001B[1;33m" + FNode.class.getPackage().getImplementationVersion() + "\u001B[1;37m]\n";

		}else if(cmd.toUpperCase().startsWith("CONF")){
			String[] sp = cmd.split(" ");
			String val;
			if(sp.length >= 3){
				if(sp[1].equalsIgnoreCase("GET")){
					val = ConfigManagerV2.get(sp[2]);
					if(val != null) res = "\u001B[1;37mCONF Item: [\u001B[1;32m" + sp[2] + "\u001B[1;37m], value: [\u001B[1;32m" + val + "\u001B[1;37m]!";
				}else if(sp[1].equalsIgnoreCase("SET") && sp.length == 4){
					val = ConfigManagerV2.get(sp[2]);
					if(val != null){
						ConfigManagerV2.set(sp[2], sp[3], null);
						FNConfigData.set(sp[2], sp[3]);
						res = "\u001B[1;37mCONF Item: [\u001B[1;32m" + sp[2] + "\u001B[1;37m], value: [\u001B[1;32m" + FNConfigData.get(sp[2]) + "\u001B[1;37m] set!";
					}else res = "Invalid CONF Item!";
					
				}else res = "Invalid CONF statement!";
			}else res = "Invalid CONF statement!";
		}else if(cmd.toUpperCase().startsWith("DB")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length > 1){
				if(sp[1].equalsIgnoreCase("INFO")){
					res += "\u001B[1;34mDB Info:\n";
					res += "\u001B[1;37mName: [\u001B[1;32m" + FNConfigData.db_name + "\u001B[1;37m], Host: [\u001B[1;32m" + FNConfigData.db_host + "\u001B[1;37m], Port: [\u001B[1;32m" + FNConfigData.db_port + "\u001B[1;37m], Username: [\u001B[1;32m" + FNConfigData.db_username + "\u001B[1;37m], Password: [\u001B[1;32m" + FNConfigData.db_password + "\u001B[1;37m]!\n";
					res += "\u001B[1;37mSMS buffer size: [\u001B[1;32m" + DBManager.dbq_sms.size() + "\u001B[1;37m]!\n";
					res += "\u001B[1;37mSRI buffer size: [\u001B[1;32m" + DBManager.dbq_sri.size() + "\u001B[1;37m]!\n";
				}else if(sp[1].equalsIgnoreCase("FLUSH")){
					res += "\u001B[1;34mFlushing DB buffer...\n";
					res += "\u001B[1;37mSMS buffer size: [\u001B[1;32m" + DBManager.dbq_sms.size() + "\u001B[1;37m]!\n";
					res += "\u001B[1;37mSRI buffer size: [\u001B[1;32m" + DBManager.dbq_sri.size() + "\u001B[1;37m]!\n";
					DBManager.buffer_flush();
				}else res = "Unknown DB statement!";			
			}else res = "Unknown DB statement!";			
			
			
		}else if(cmd.toUpperCase().startsWith("SG")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length > 1){
				if(sp[1].equalsIgnoreCase("INFO")){
					SGDescriptor sgd = null;
					res += "\u001B[1;34mSG(Signaling) Nodes:\n";
					for(String k : SGNManager.sgn_map.keySet()){
						sgd = SGNManager.sgn_map.get(k);
						res += "\u001B[1;37mNode ID: [\u001B[1;32m" + sgd.id + "\u001B[1;37m], host: [\u001B[1;32m" + sgd.address + "\u001B[1;37m], port: [\u001B[1;32m" + sgd.port + "\u001B[1;37m], active: [\u001B[1;32m" + sgd.active + "\u001B[1;37m]!\n";  
						
					}
				}			
			}			
		}else if(cmd.toUpperCase().startsWith("GPUB")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length > 1){
				if(sp[1].equalsIgnoreCase("INFO") && FNConfigData.gpub_status){
					NodeDescriptor nd = null;
					res += "\u001B[1;34mGPU Nodes:\n";
					for(int i = 0; i<GPUBalancer.node_lst.size(); i++){
						nd = GPUBalancer.node_lst.get(i);
						res += "\u001B[1;37mNode ID: [\u001B[1;32m" + nd.id + "\u001B[1;37m], host: [\u001B[1;32m" + nd.host + "\u001B[1;37m], port: [\u001B[1;32m" + nd.port + "\u001B[1;37m], gpu count: [\u001B[1;32m" + (nd.connections.size()-1) + "\u001B[1;37m]!\n";  
					}
				}else if(sp[1].equalsIgnoreCase("START")){
					GPUBalancer.init();
					FNConfigData.gpub_status = true;
					res = "\u001B[1;37mStarting GPU Balancer...\n";
					
				}else if(sp[1].equalsIgnoreCase("STOP")){
					GPUBalancer.stop();
					FNConfigData.gpub_status = false;
					res = "\u001B[1;37mStopping GPU Balancer...\n";
				}else if(sp[1].equalsIgnoreCase("STATUS")){
					res = "\u001B[1;37mGPU Balancer status: [" + (FNConfigData.gpub_status ? "\u001B[1;32mACTIVE" : "\u001B[1;31mINACTIVE") + "\u001B[1;37m]!";
					
				}else res = "Unknown GPUB Statement!";
			}else res = "GPUB Statement missing!";
		}else if(cmd.toUpperCase().startsWith("FILTER GET")){
			String[] sp = cmd.toUpperCase().split(" ");
			if(sp.length == 3){
				if(sp[2].equalsIgnoreCase("XML")){
					res += SmsfsManager.currentFilter2xml();
				}else res += SmsfsManager.current_filter_data;
			}else res += SmsfsManager.current_filter_data;

		}else if(cmd.toUpperCase().startsWith("REFILTER")){
			String[] sp = cmd.toUpperCase().split(" ", 3);
			if(sp.length >= 2){
				if(sp[1].equalsIgnoreCase("STATUS")){
					res += "\u001B[1;37mRetained SMS Refiltering status: [ " + (DBManager.refiltering_active ? "\u001B[1;32mACTIVE" : "\u001B[1;31mINACTIVE") + " ]!";
					
				}else if(sp[1].equalsIgnoreCase("POS")){
					res += "\u001B[1;37mRetained SMS Refiltering position (CURRENT ID / MAX ID): [ \u001B[1;32m" + StatsManager.REFILTER_STATS.POSITION + " / \u001B[1;31m" + StatsManager.REFILTER_STATS.MAX_ID +  "\u001B[1;37m ]!";
					
				}else if(sp[1].equalsIgnoreCase("STOP")){
					DBManager.stopRefiltering();
					res += "\u001B[1;37mStopping Retained SMS Refiltering process...";
				}else if(sp[1].equalsIgnoreCase("TIME")){
					if(DBManager.refiltering_active){
						res += "\u001B[1;37mCurrent Retained SMS Refiltering elapsed time, still working...: [ \u001B[1;32m" + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.REFILTER_STATS.START_TIME) +  "\u001B[1;37m ]!";
						
					}else{
						res += "\u001B[1;37mLast Retained SMS Refiltering elapsed time: [ \u001B[1;32m" + Utils.tsElapsed2String(StatsManager.REFILTER_STATS.ELAPSED_TIME) +  "\u001B[1;37m ]!";
						
					}

				}else if(sp[1].equalsIgnoreCase("START")){
					if(!DBManager.refiltering_active){
						String[] params = null;
						if(sp.length == 3){
							params = sp[2].split(",");
							res += "\u001B[1;37mRetained SMS Refiltering timespan: [ \u001B[1;32m" + params[0].trim() + " - " + params[1].trim() + "\u001B[1;37m ]!" + "\n";
							DBManager.refilterRetainedSMS(params[0].trim(), params[1].trim());
						}else DBManager.refilterRetainedSMS(null, null);
						res += "\u001B[1;37mStarting Retained SMS Refiltering process...";
					}else res = "\u001B[1;37mRetained SMS Refiltering process already started!";
				}else res = "\u001B[1;37mUnknown refilter method!";
				
			}else res = "\u001B[1;37mRefilter method missing!";
			
		}else if(cmd.toUpperCase().startsWith("SMS")){
			String[] sp = cmd.toUpperCase().split(" ");
			dbr = DBManager.getSMSByID(Long.parseLong(sp[1]), null);
			if(dbr != null){
				res += "\u001B[1;37mId: \u001B[1;32m" + dbr.id + "\n";
				res += "\u001B[1;37mDirection: \u001B[1;32m" + dbr.direction + "\n";
				res += "\u001B[1;37mType: \u001B[1;32m" + dbr.type + "\n";
				res += "\u001B[1;37mGT Called: \u001B[1;32m" + dbr.gt_called + "\n";
				res += "\u001B[1;37mGT Calling: \u001B[1;32m" + dbr.gt_calling + "\n";
				res += "\u001B[1;37mImsi: \u001B[1;32m" + dbr.imsi + "\n";
				res += "\u001B[1;37mMsisdn: \u001B[1;32m" + dbr.msisdn + "\n";
				res += "\u001B[1;37mScda: \u001B[1;32m" + dbr.scda + "\n";
				res += "\u001B[1;37mScoa: \u001B[1;32m" + dbr.scoa + "\n";
				res += "\u001B[1;37mSms destination: \u001B[1;32m" + dbr.sms_destination + "\n";
				res += "\u001B[1;37mSms destination enc: \u001B[1;32m" + dbr.sms_destination_enc + "\n";
				res += "\u001B[1;37mSms originating: \u001B[1;32m" + dbr.sms_originating + "\n";
				res += "\u001B[1;37mSms originating enc: \u001B[1;32m" + dbr.sms_originating_enc + "\n";
				res += "\u001B[1;37mSms text: \u001B[1;32m" + dbr.sms_text + "\n";
				res += "\u001B[1;37mSms text enc: \u001B[1;32m" + dbr.sms_text_enc + "\n";
				res += "\u001B[1;37mTcap sid: \u001B[1;32m" + dbr.tcap_sid + "\n";
				res += "\u001B[1;37mTcap did: \u001B[1;32m" + dbr.tcap_did + "\n";
				
			}else res = "\u001B[1;31mSMS not found!";
			
		}else if(cmd.equalsIgnoreCase("show memory")){
			double used_mem =  (double)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024/1024;
			double total_mem =  (double)Runtime.getRuntime().totalMemory() / 1024/1024;
			res = String.format("[ \u001B[1;32m%7.2fMb\u001B[0m / \u001B[1;31m%7.2fMb\u001B[0m ]", used_mem, total_mem);
			
		}else if(cmd.toUpperCase().startsWith("SHOW STATS")){
			String[] sp = cmd.toUpperCase().split(" +");
			if(sp.length > 2){
				String p = sp[2];
				if(p.equals("VSTP")){
					res += "\u001B[1;37mVSTP_SG_CONNECTION_LOST COUNT: \u001B[1;31m" + StatsManager.VSTP_STATS.VSTP_SG_CONNECTION_LOST + "\n";
					res += "\u001B[1;37mVSTP_NO_SG_NODES COUNT: \u001B[1;31m" + StatsManager.VSTP_STATS.VSTP_NO_SG_NODES + "\n";
					
				}else if(p.equals("DB")){
					res += "\u001B[1;37mDB_CONNECTION_LOST COUNT: \u001B[1;31m" + StatsManager.DB_STATS.DB_CONNECTION_LOST + "\n";
					
					res += "\u001B[1;37mDB_SMS_BATCH_COUNT COUNT: \u001B[1;32m" + StatsManager.DB_STATS.DB_SMS_BATCH_COUNT + "\n";
					res += "\u001B[1;37mDB_SRI_BATCH_COUNT COUNT: \u001B[1;32m" + StatsManager.DB_STATS.DB_SRI_BATCH_COUNT + "\n";
					
					res += "\u001B[1;37mLAST_SMS_BATCH_ELAPSED TIME(msec): \u001B[1;32m" + StatsManager.DB_STATS.LAST_SMS_BATCH_ELAPSED + "\n";
					res += "\u001B[1;37mLAST_SRI_BATCH_ELAPSED TIME(msec): \u001B[1;32m" + StatsManager.DB_STATS.LAST_SRI_BATCH_ELAPSED + "\n";

					res += "\u001B[1;37mMAX_SMS_BATCH_ELAPSED TIME(msec): \u001B[1;33m" + StatsManager.DB_STATS.MAX_SMS_BATCH_ELAPSED + "\n";
					res += "\u001B[1;37mMAX_SRI_BATCH_ELAPSED TIME(msec): \u001B[1;33m" + StatsManager.DB_STATS.MAX_SRI_BATCH_ELAPSED + "\n";

				}else if(p.equals("GPU")){
					if(FNConfigData.gpub_status){
						try{
							String rep_node = sp[3].split(":")[0];
							String rep_gpu = sp[3].split(":")[1];
							String gpu_stats_line = null;
							String[] gpu_result = null;
							try{
								Socket gpu_socket = new Socket(GPUBalancer.getNodeDescriptor(rep_node).host, GPUBalancer.getNodeDescriptor(rep_node).port);
								gpu_socket.setSoTimeout(10000);
								BufferedReader gpu_in = new BufferedReader(new InputStreamReader(gpu_socket.getInputStream()));
								PrintWriter gpu_out = new PrintWriter(gpu_socket.getOutputStream());
								gpu_out.println("S." + rep_gpu);
								gpu_out.flush();
								gpu_stats_line = gpu_in.readLine();
								gpu_result = gpu_stats_line.split(":");
								res += "\u001B[1;37mKERNEL EXECUTION COUNT: \u001B[1;32m" + gpu_result[0] + "\n";
								res += "\u001B[1;37mLAST KERNEL EXECUTION TIME: \u001B[1;32m" + gpu_result[1] + "msec\n";
								res += "\u001B[1;37mMAX KERNEL EXECUTION TIME: \u001B[1;33m" + gpu_result[2] + "msec\n";
								res += "\u001B[1;37mCURRENT UPDATE QUEUE SIZE: \u001B[1;32m" + gpu_result[3] + "\n";
								res += "\u001B[1;37mCURRENT LD QUEUE SIZE: \u001B[1;32m" + gpu_result[4] + "\n";
								res += "\u001B[1;37mCURRENT MD5 QUEUE SIZE: \u001B[1;32m" + gpu_result[5] + "\n";
								
								gpu_socket.close();
							}catch(Exception e){
								res = "\u001B[1;37mNode connection not available...";
								e.printStackTrace();
							}
							
						}catch(Exception e){
							res = "Invalid statement...";
						}
						
					}else res = "GPUB module if OFF";
				}else if(p.equals("GPUB")){
					if(FNConfigData.gpub_status){
						// LD stats
						res += "\u001B[1;37mLD_REQUEST COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.LD_REQUEST_COUNT) + "\n";
						res += "\u001B[1;37mLD_REPLY_OK COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.LD_REPLY_OK_COUNT) + "\n";
						res += "\u001B[1;37mLD_REPLY_ERROR COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.LD_REPLY_ERROR_COUNT) + "\n";
						res += "\u001B[1;37mLD_MAX_REPLY TIME: \u001B[1;33m"  + Long.toString(StatsManager.GPUB_STATS.LD_MAX_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mLD_LAST_REPLY TIME: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mLD_CONNECTION_DOWN COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.LD_CONNECTION_DOWN_COUNT) + "\n";
						res += "\u001B[1;37mLD_NODE_BUSY COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT) + "\n";
						// UPDATE stats
						res += "\u001B[1;37mUPDATE_REQUEST COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REQUEST_COUNT) + "\n";
						res += "\u001B[1;37mUPDATE_REPLY_OK COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REPLY_OK_COUNT) + "\n";
						res += "\u001B[1;37mUPDATE_REPLY_ERROR COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_REPLY_ERROR_COUNT) + "\n";
						res += "\u001B[1;37mUPDATE_MAX_REPLY TIME: \u001B[1;33m"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_MAX_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mUPDATE_LAST_REPLY TIME: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mUPDATE_CONNECTION_DOWN COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.UPDATE_CONNECTION_DOWN_COUNT) + "\n";

						// MD5 stats
						res += "\u001B[1;37mMD5_REQUEST COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.MD5_REQUEST_COUNT) + "\n";
						res += "\u001B[1;37mMD5_REPLY_OK COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.MD5_REPLY_OK_COUNT) + "\n";
						res += "\u001B[1;37mMD5_REPLY_ERROR COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.MD5_REPLY_ERROR_COUNT) + "\n";
						res += "\u001B[1;37mMD5_MAX_REPLY TIME: \u001B[1;33m"  + Long.toString(StatsManager.GPUB_STATS.MD5_MAX_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mMD5_LAST_REPLY TIME: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mMD5_CONNECTION_DOWN COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.MD5_CONNECTION_DOWN_COUNT) + "\n";
						res += "\u001B[1;37mMD5_NODE_BUSY COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.MD5_NODE_BUSY_COUNT) + "\n";
						// MD5 UPDATE stats
						res += "\u001B[1;37mMD5_UPDATE_REQUEST COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REQUEST_COUNT) + "\n";
						res += "\u001B[1;37mMD5_UPDATE_REPLY_OK COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_OK_COUNT) + "\n";
						res += "\u001B[1;37mMD5_UPDATE_REPLY_ERROR COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_ERROR_COUNT) + "\n";
						res += "\u001B[1;37mMD5_UPDATE_MAX_REPLY TIME: \u001B[1;33m"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_MAX_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mMD5_UPDATE_LAST_REPLY TIME: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME) + "msec\n";
						res += "\u001B[1;37mMD5_UPDATE_CONNECTION_DOWN COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.MD5_UPDATE_CONNECTION_DOWN_COUNT) + "\n";
						// LST REMOVE stats
						res += "\u001B[1;37mLST_REMOVE COUNT: \u001B[1;32m"  + Long.toString(StatsManager.GPUB_STATS.LST_REMOVE_COUNT) + "\n";
						res += "\u001B[1;37mLST_REMOVE_ERROR COUNT: \u001B[1;31m"  + Long.toString(StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT) + "\n";
						
					}else res = "GPUB module if OFF";
				}else if(p.equals("SMSFS")){
					if(FNConfigData.smsfs_status){
						res += "\u001B[1;37mINTRUSIVE ACCEPTED COUNT: \u001B[1;32m"  + Long.toString(StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED) + "\n";
						res += "\u001B[1;37mINTRUSIVE REJECTED COUNT: \u001B[1;31m"  + Long.toString(StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED) + "\n";
						res += "\u001B[1;37mNON INTRUSIVE ACCEPTED COUNT: \u001B[1;32m"  + Long.toString(StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED) + "\n";
						res += "\u001B[1;37mNON INTRUSIVE REJECTED COUNT: \u001B[1;31m"  + Long.toString(StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED) + "\n";
					}else res = "SMSFS if OFF!";
				}

				
			}else res = "Unknown stats object!";
			
		}else if(cmd.equalsIgnoreCase("UPTIME")){
			res = "\u001B[1;34mUPTIME: \u001B[1;32m" + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.SYSTEM_STATS.STARTUP_TS);
			
		}else if(cmd.equalsIgnoreCase("ping")){
			res = "1";
			STATS.LAST_PING_TS = System.currentTimeMillis();
		}else res = "Unknown statement!" + (char)13;
		
		return res;
		
		
	}
	public static void init(int _port){
		connection_lst = new ConcurrentHashMap<String, CMDIConnection>();
		init_server(_port);
		STATS = new CMDIStats();
		
		
		
	}
}
