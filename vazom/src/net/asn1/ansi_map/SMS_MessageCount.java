package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SMS_MessageCount extends OCTET_STRING{

	public SMS_MessageCount(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
