package net.smstpdu;

public class ValidityPeriod {
	

	public static int decode(int value){
		int res = -1;
		//(TP-VP+1) x 5min (i.e. 5min intervals up to 12hrs)
		if(value >= 0x00 && value <=0x8F){
			res = (value + 1) * 5;
		//12hr+((TP-VP-143) x 30min)	
		}else if(value >= 0x90 && value <= 0xA7){
			res = 720 + ((value - 143) * 30);
		//(TPVP-166) x 1day	
		}else if(value == 0xA8){
			res = (value - 166) * 1440;
		//(TP-VP-192) x 1week	
		}else if(value >= 0xC5 && value <=0xFF){
			res = (value - 192) * 10080;
		}
		
		
		return res;
	}
}
