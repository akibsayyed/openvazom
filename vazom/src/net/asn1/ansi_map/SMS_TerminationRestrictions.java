package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SMS_TerminationRestrictions extends OCTET_STRING{

	public SMS_TerminationRestrictions(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
