package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.cause.CauseValueClassType;
import net.isup.parameters.cause.CauseValueType;
import net.isup.parameters.cause.CodingStandardType;
import net.isup.parameters.cause.LocationType;

public class CauseIndicators extends ParameterBase {
	public CodingStandardType coding_standard;
	public LocationType location;
	public CauseValueClassType cause_class;
	public CauseValueType cause_value;
	public int class_code;
	public int cause_code;
	//public byte[] value;
	public CauseIndicators(){
		type = ParameterType.CAUSE_INDICATORS;
	}

	public void init(byte[] data) {
		if(data != null){
			coding_standard = CodingStandardType.get(data[byte_position] & 0x60);
			location = LocationType.get(data[byte_position] & 0x0f);
			byte_position++;
			cause_class = CauseValueClassType.get(data[byte_position] & 0x70);
			class_code = data[byte_position] & 0x70;
			cause_code = data[byte_position] & 0x0f;
			if(cause_class != null){
				cause_value = CauseValueType.get(data[byte_position] & 0x0f, cause_class.getId());
			}
			
		}
	}

}
