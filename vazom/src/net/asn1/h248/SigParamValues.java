package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SigParamValues extends SEQUENCE{

	public SigParamValue new_child(){
		return new SigParamValue();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SigParamValue getChild(int index){
		return (SigParamValue)of_children.get(index);
	}
	public SigParamValues(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
