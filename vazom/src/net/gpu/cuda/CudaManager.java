package net.gpu.cuda;

public class CudaManager {
	static{ System.loadLibrary("vazomgpu"); }
	// list types
	public static final int LD_REPETITION = 0;
	public static final int LD_SPAM = 1;
	public static final int MD5_QUARANTINE_LST = 2;
	public static final int MD5_NORMAL_LST = 3;
	public static final int ALL_LISTS = 9999;
	// actions
	public static final int ACT_DELETE = 10;
	public static final int ACT_NEW = 11;
	public static final int ACT_COMPARE = 12;
	public static final int ACT_RESET = 13;
	// callback types
	public static final int CB_LD = 100;
	public static final int CB_MD5 = 200;
	// methods
	public static native void init(int _max_rep_lst_size, 
									int _max_spam_lst_size, 
									int _max_md5_quarantine_lst_size,
									int _max_md5_normal_lst_size,
									int _max_i_size, 
									int _max_i_variation,
									int _max_q_size);
	public static native void initGrid(int lst_id, int blocks_w, int blocks_h, int threads);
	public static native void initAllDevices();
	public static native void initDevice(int device);
	public static native void shutdown();
	public static native int getDeviceCount();
	public static native int getFreeDevice();
	public static native void reset(int device, int lst_type);
	public static native void pushToLDQueue(int device, int lst_type, byte[] msg_bytes, CudaCallback callback);
	public static native void pushToUPDATEQueue(int device, int lst_type, byte[] msg_bytes);
	public static native void pushToMD5UpdateQueue(int device, int lst_type, byte[] hash);
	public static native void pushToMD5Queue(int device, int lst_type, byte[] hash, CudaCallback callback);
	public static native void enqueueRemove(int device, int lst_type, int item_id);
	public static native void remove(int device, int lst_type, int item_id, byte[] msg_bytes);
	public static native KernelStats getStats(int device);

	
}
