package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;

public class UserPartTest extends MessageBase {
	public UserPartTest(){
		super();
		type = MessageType.USER_PART_TEST;
		pointers = new Pointer[1];
	}
	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			processOptional(data, 0);
		}
	}

}
