package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CUG_CheckInfo extends SEQUENCE{

	public CUG_Interlock get_cug_Interlock(){
		return (CUG_Interlock)elements.get(0).data;
	}
	public CUG_Interlock new_cug_Interlock(){
		return new CUG_Interlock();
	}

	public NULL get_cug_OutgoingAccess(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_cug_OutgoingAccess(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_cug_Interlock(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_cug_OutgoingAccess(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public CUG_CheckInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
