package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class RoutingKey extends M3UAParameter {
	
	public RoutingKey(){
		type = M3UAParameterType.ROUTING_KEY;
	}

	public void init(byte[] data) {
		value = data;
	}



}
