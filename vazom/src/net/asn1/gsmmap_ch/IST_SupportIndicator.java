package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class IST_SupportIndicator extends ENUMERATED{

	public static final int _basicISTSupported = 0;
	public static final int _istCommandSupported = 1;
	public IST_SupportIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
