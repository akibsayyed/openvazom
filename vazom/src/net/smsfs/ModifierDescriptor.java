package net.smsfs;

import java.util.HashMap;

public class ModifierDescriptor extends FilterDescriptorBase{
	public boolean active;
	public String name;
	public String newValue;
	public String label;
	public HashMap<String, String> extra_params;
	public ModifierDescriptor(){
		type = FilterDescriptorType.MODIFIER;
	}
}
