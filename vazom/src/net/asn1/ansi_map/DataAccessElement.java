package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DataAccessElement extends SEQUENCE{

	public DataID get_dataID(){
		return (DataID)elements.get(0).data;
	}
	public DataID new_dataID(){
		return new DataID();
	}

	public Change get_change(){
		return (Change)elements.get(1).data;
	}
	public Change new_change(){
		return new Change();
	}

	public DataValue get_dataValue(){
		return (DataValue)elements.get(2).data;
	}
	public DataValue new_dataValue(){
		return new DataValue();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(251, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataID(); }});
		elements.add(new ElementDescriptor(248, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_change(); }});
		elements.add(new ElementDescriptor(256, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataValue(); }});
	}
	public DataAccessElement(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
