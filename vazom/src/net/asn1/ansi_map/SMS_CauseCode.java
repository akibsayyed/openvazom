package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SMS_CauseCode extends OCTET_STRING{

	public SMS_CauseCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
