package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RandomVariableRequest extends SET{

	public MSCID get_mscid(){
		return (MSCID)elements.get(0).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public RANDC get_randc(){
		return (RANDC)elements.get(1).data;
	}
	public RANDC new_randc(){
		return new RANDC();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(2).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_randc(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_servingCellID(); }});
	}
	public RandomVariableRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
