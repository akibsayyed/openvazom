package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMABandClassInformationType extends CDMABandClassInformation{

	public CDMABandClassInformationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 171;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
