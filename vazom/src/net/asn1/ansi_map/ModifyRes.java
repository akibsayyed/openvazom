package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModifyRes extends SET{

	public ModificationResultList get_modificationResultList(){
		return (ModificationResultList)elements.get(0).data;
	}
	public ModificationResultList new_modificationResultList(){
		return new ModificationResultList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(264, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_modificationResultList(); }});
	}
	public ModifyRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
