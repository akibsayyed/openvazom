package net.spcap.vlan;

import java.util.HashMap;



public enum VLANPriority {
	BACKGROUND(0x20),
	BEST_EFFORT(0x00),
	EXCELLENT_EFFORT(0x40),
	CRITICAL_APPLICATIONS(0x60),
	VIDEO_100MS_LATENCY(0x80),
	VOICE_10MS_LATENCY(0xa0),
	INTERNETWORK_CONTROL(0xc0),
	NETWORK_CONTROL(0xe0);
	
	
	private int id;
	private static final HashMap<Integer, VLANPriority> lookup = new HashMap<Integer, VLANPriority>();
	static{
		for(VLANPriority td : VLANPriority.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static VLANPriority get(int id){ return lookup.get(id); }
	private VLANPriority(int _id){ id = _id; }

}
