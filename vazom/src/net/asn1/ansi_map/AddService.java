package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AddService extends SET{

	public CDMAConnectionReferenceList get_cdmaConnectionReferenceList(){
		return (CDMAConnectionReferenceList)elements.get(0).data;
	}
	public CDMAConnectionReferenceList new_cdmaConnectionReferenceList(){
		return new CDMAConnectionReferenceList();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(1).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public Digits get_digits(){
		return (Digits)elements.get(2).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(3).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(4).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(5).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(212, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceList(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
	}
	public AddService(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
