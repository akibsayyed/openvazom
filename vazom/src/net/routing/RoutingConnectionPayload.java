package net.routing;

import java.util.ArrayList;

import net.vstp.VSTP_SGF_CorrelationPacket;

public abstract class RoutingConnectionPayload {
	public RoutingConnectionType type;
	// fail over connections
	public ArrayList<String> connections;
	public ArrayList<Object> extra_params;
	public ArrayList<Object> extra_conversion_params;
	public boolean callback_required;
	public int conn_counter = 0;
	// callback method
	public CallbackBase callback;
	
	
	public abstract void setParamsFromVSTP_SGF(VSTP_SGF_CorrelationPacket vstp_sgf);
	public abstract void setParams(Object[] params);

}
