package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMACodeChannelList extends SEQUENCE{

	public CDMACodeChannelInformationType new_child(){
		return new CDMACodeChannelInformationType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMACodeChannelInformationType getChild(int index){
		return (CDMACodeChannelInformationType)of_children.get(index);
	}
	public CDMACodeChannelList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
