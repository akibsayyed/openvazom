package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceDataResultList extends SEQUENCE{

	public ServiceDataResultType new_child(){
		return new ServiceDataResultType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ServiceDataResultType getChild(int index){
		return (ServiceDataResultType)of_children.get(index);
	}
	public ServiceDataResultList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
