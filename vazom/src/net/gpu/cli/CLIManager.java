package net.gpu.cli;

import net.cli.AuthManager;
import net.cli.CLIBase;
import net.cli.GroupDescriptor;
import net.cli.MethodDescriptor;
import net.cli.ParamDescriptor;
import net.config.ConfigManagerV2;
import net.config.GPUNConfigData;
import net.gpu.cuda.KernelStats;
import net.gpu2.ClientConnection;
import net.gpu2.GPUNode;
import net.stats.StatsManager;
import net.utils.Utils;

public class CLIManager extends CLIBase{
	// AUTH 
    public class GRP_AUTH extends GroupDescriptor {

        public GRP_AUTH() {
        	super("AUTH", "CLI Authentication management ", "", "");
        }

        public String execute(Object[] params) {
        	return null;
        }

    }
	public GRP_AUTH _grp_AUTH = new GRP_AUTH();
	
	
	public class CLI_user_list extends MethodDescriptor {
	
		public CLI_user_list() {
			super("USRLST", "List users ", "AUTH", "");
		}
		
		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.auth_user_group == 0){
					for(String s : AuthManager.auth_map.keySet()){
						res += "[WHITE]Username = [[GREEN]" + s + "[WHITE]], credentials = [[GREEN]" + (AuthManager.checkGroup(s) == 0 ? "admin" : "user") + "[WHITE]]\n";
					}
				}else{
					return "[RED]Insufficient credentials!";
				}

			}
			return res;
			
		}
	
	}
	public CLI_user_list _cli_user_list = new CLI_user_list();
	
	public class CLI_grp_list extends MethodDescriptor {
	
		public CLI_grp_list() {
	        super("GRPLST", "List groups ", "AUTH", "");
		}
		
		public String execute(Object[] params) {
			return "[WHITE] Groups = [[RED]0 [WHITE]= [GREEN]admin[WHITE], [RED]1[WHITE] = [GREEN]user[WHITE]]!";
		}
	
	}
	public CLI_grp_list _cli_grp_list = new CLI_grp_list();
	
	public class CLI_user_add extends MethodDescriptor {
	
	    public CLI_user_add() {
            super("USRADD", "Add user ", "AUTH", "group_id:username:password:password_repeat");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				ParamDescriptor pd4 = (ParamDescriptor)params[3];
				CLIConnection conn = (CLIConnection)params[4];
				if(conn.auth_user_group == 0){
					if(pd1.value != null && pd2.value != null && pd3.value != null && pd4.value != null){
						if(!pd3.value.equals(pd4.value)) return "[RED]Passwords do not match!";
						if(!Utils.isInt(pd1.value)) return "[RED]Unknown group id = [" + pd1.value +"]!";
						if(Integer.parseInt(pd1.value) != 0 && Integer.parseInt(pd1.value) != 1) return "[RED]Unknown group id = [" + pd1.value +"]!";
						// add user
						if(!AuthManager.userExists(pd2.value)){
							AuthManager.set(pd2.value, pd3.value, Integer.parseInt(pd1.value));
							return "[WHITE]Adding user [[GREEN]" + pd2.value + "[WHITE]], credentials = [[GREEN]" + AuthManager.grpId2Name(Integer.parseInt(pd1.value)) + "[WHITE]]";
							
						}else{
							return "[RED]User [[GREEN]" + pd2.value +"[RED]] already exists!";
							
						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }
	
	}
	public CLI_user_add _cli_user_add = new CLI_user_add();
	
    public class CLI_user_set extends MethodDescriptor {

	    public CLI_user_set() {
            super("USRSET", "Set user information ", "AUTH", "group_id:username:password:password_repeat");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				ParamDescriptor pd3 = (ParamDescriptor)params[2];
				ParamDescriptor pd4 = (ParamDescriptor)params[3];
				CLIConnection conn = (CLIConnection)params[4];
				if(conn.auth_user_group == 0){
					if(pd1.value != null && pd2.value != null && pd3.value != null && pd4.value != null){
						if(!pd3.value.equals(pd4.value)) return "[RED]Passwords do not match!";
						if(!Utils.isInt(pd1.value)) return "[RED]Unknown group id = [" + pd1.value +"]!";
						if(Integer.parseInt(pd1.value) != 0 && Integer.parseInt(pd1.value) != 1) return "[RED]Unknown group id = [" + pd1.value +"]!";
						// set user
						if(AuthManager.userExists(pd2.value)){
							AuthManager.set(pd2.value, pd3.value, Integer.parseInt(pd1.value));
							return "[WHITE]Setting user [[GREEN]" + pd2.value + "[WHITE]], credentials = [[GREEN]" + AuthManager.grpId2Name(Integer.parseInt(pd1.value)) + "[WHITE]]";
							
						}else{
							return "[RED]User [[GREEN]" + pd2.value +"[RED]] does not exist!";

						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }

	}
	public CLI_user_set _cli_user_set = new CLI_user_set();
	
	public class CLI_user_rm extends MethodDescriptor {
	
	    public CLI_user_rm() {
            super("USRRM", "Remove user ", "AUTH", "username");
	    }
	
	    public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				CLIConnection conn = (CLIConnection)params[1];
				if(conn.auth_user_group == 0){
					if(pd1.value != null){
						// set user
						if(AuthManager.userExists(pd1.value)){
							AuthManager.remove(pd1.value);
							return "[WHITE]Removing user [[GREEN]" + pd1.value + "[WHITE]]!";
							
						}else{
							return "[RED]User [[GREEN]" + pd1.value +"[RED]] does not exist!";

						}
						
					}else{
						return "[RED]Missing parameters!";
						
					}

				}else{
					return "[RED]Insufficient credentials!";
				}
	
			}
			return res;
	    }
	
	}
	public CLI_user_rm _cli_user_rm = new CLI_user_rm();
	
	public class CLI_users_passwd extends MethodDescriptor {
	
        public CLI_users_passwd() {
            super("PASSWD", "Change password ", "AUTH", "password:password_repeat");
        }

        public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd1 = (ParamDescriptor)params[0];
				ParamDescriptor pd2 = (ParamDescriptor)params[1];
				CLIConnection conn = (CLIConnection)params[2];
				if(pd1.value != null && pd2.value != null){
					if(!pd1.value.equals(pd2.value)) return "[RED]Passwords do not match!";
					
					AuthManager.set(conn.auth_user, pd1.value, AuthManager.checkGroup(conn.auth_user));
					return "[WHITE]Changing password for user [[GREEN]" + conn.auth_user + "[WHITE]]!";
					
				}else{
					return "[RED]Missing parameters!";
					
				}

	
			}
			return res;

        }
	
	}
	public CLI_users_passwd _cli_users_passwd = new CLI_users_passwd();
	
	
	// AUTH end
	
	public class GRP_SHOW extends GroupDescriptor {

		public GRP_SHOW() {
			super("SHOW", "Statistics and memory usage ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_SHOW _grp_SHOW = new GRP_SHOW();


	public class CLI_show_memory extends MethodDescriptor {

		public CLI_show_memory() {
			super("MEMORY", "Memory usage ", "SHOW", "");
		}

		public String execute(Object[] params) {
			double used_mem =  (double)(Runtime.getRuntime().totalMemory() - Runtime.getRuntime().freeMemory()) / 1024/1024;
			double total_mem =  (double)Runtime.getRuntime().totalMemory() / 1024/1024;
			return String.format("[ [GREEN]%7.2fMb[RESET] / [RED]%7.2fMb[RESET] ]", used_mem, total_mem);
		}

	}
	public CLI_show_memory _cli_show_memory = new CLI_show_memory();

	public class GRP_STATS extends GroupDescriptor {

		public GRP_STATS() {
			super("STATS", "Various statistics ", "SHOW", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_STATS _grp_STATS = new GRP_STATS();


	public class CLI_show_stats_kernel extends MethodDescriptor {

		public CLI_show_stats_kernel() {
			super("KERNEL", "CUDA Kernel execution statistics ", "STATS,SHOW", "gpu_id");
		}

		public String execute(Object[] params) {
			String res = "";
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				if(pd.value != null){
					try{
						KernelStats ks = GPUNode.getStats(Integer.parseInt(pd.value));
						res += "[BLUE]CUDA GPU ID = [" + pd.value + "]\n";
						res += "[WHITE]KERNEL EXECUTION COUNT: [GREEN]" + ks.execution_count + "\n";
						res += "[WHITE]KERNEL LAST EXECUTION TIME: [GREEN]" + ks.last_execution_time + "msec\n";
						res += "[WHITE]KERNEL MAX EXECUTION TIME: [GREEN]" + ks.max_execution_time + "msec\n";
						res += "[WHITE]KERNEL UPDATE QUEUE SIZE: [GREEN]" + ks.update_q_size + "\n";
						res += "[WHITE]KERNEL LD QUEUE SIZE: [GREEN]" + ks.ld_q_size + "\n";
						res += "[WHITE]KERNEL MD5 QUEUE SIZE: [GREEN]" + ks.md5_q_size + "\n";
						res = Utils.str_align(res, ":");
					}catch(Exception e){
						e.printStackTrace();
						res = "[RED]Error while fetching KERNEL stats for CUDA GPU ID = [" + pd.value + "]!";
					}
					
				}
			}
			return res;
		}

	}
	public CLI_show_stats_kernel _cli_show_stats_kernel = new CLI_show_stats_kernel();

	public class GRP_F extends GroupDescriptor {

		public GRP_F() {
			super("FGN", "Filtering Nodes ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_F _grp_F = new GRP_F();


	public class CLI_f_info extends MethodDescriptor {

		public CLI_f_info() {
			super("INFO", "Show Filtering Node List ", "FGN", "");
		}

		public String execute(Object[] params) {
			String res = "";
			ClientConnection cc = null;
			res += "[BLUE]Filtering Gateway Nodes:\n";
			for(Integer i : GPUNode.connection_lst.keySet()){
				cc = GPUNode.connection_lst.get(i);
				if(cc != null){
					res += "[WHITE]Node ID: [[GREEN]" + cc.id+ "[WHITE]], sctp id: [[GREEN]" + cc.sctp_id + "[WHITE]]!\n";
					
				}
			}
			return res;
		}

	}
	public CLI_f_info _cli_f_info = new CLI_f_info();

	public class GRP_CONF extends GroupDescriptor {

		public GRP_CONF() {
			super("CONF", "Configuration management ", "", "");
		}

		public String execute(Object[] params) {
			return null;
		}

	}
	public GRP_CONF _grp_CONF = new GRP_CONF();


	public class CLI_conf_get extends MethodDescriptor {

		public CLI_conf_get() {
			super("GET", "Show configuration item ", "CONF", "conf_item");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				if(pd.value != null){
					String val = ConfigManagerV2.get(pd.value);
					if(val != null) return "[WHITE]CONF Item: [[GREEN]" + pd.value + "[WHITE]], value: [[GREEN]" + val + "[WHITE]]!";
				
				}				
			}
			return "";
		}

	}
	public CLI_conf_get _cli_conf_get = new CLI_conf_get();

	public class CLI_conf_set extends MethodDescriptor {

		public CLI_conf_set() {
			super("SET", "Set configuration item ", "CONF", "conf_item:value");
		}

		public String execute(Object[] params) {
			if(params != null){
				if(params.length == 2){
					ParamDescriptor pd1 = (ParamDescriptor)params[0];
					ParamDescriptor pd2 = (ParamDescriptor)params[1];
					if(pd1.value != null && pd2.value != null){
						ConfigManagerV2.set(pd1.value, pd2.value, null);
						GPUNConfigData.set(pd1.value, pd2.value);
						return "[WHITE]CONF Item: [[GREEN]" + pd1.value + "[WHITE]], value: [[GREEN]" + GPUNConfigData.get(pd2.value) + "[WHITE]] set!";
					
					}				
				}
			}
			return "";		}

	}
	public CLI_conf_set _cli_conf_set = new CLI_conf_set();

	public class CLI_conf_search extends MethodDescriptor {

		public CLI_conf_search() {
			super("SEARCH", "Search configuration ", "CONF", "conf_item");
		}

		public String execute(Object[] params) {
			if(params != null){
				ParamDescriptor pd = (ParamDescriptor)params[0];
				int max_l = 0;
				if(pd.value != null){
					String[] res_lst = GPUNConfigData.search(pd.value);
					// max conf item length
					for(int i = 0; i<res_lst.length; i++) if(res_lst[i].length() > max_l) max_l = res_lst[i].length();
					String res = "";
					// result
					for(int i = 0; i<res_lst.length; i++) res += "[WHITE]" + String.format("%" + max_l + "s", res_lst[i]) + " = [GREEN][" + GPUNConfigData.get(res_lst[i]) + "[WHITE]]\n";
					return res;
					
				}
				
			}
			return "";		}

	}
	public CLI_conf_search _cli_conf_search = new CLI_conf_search();

	public class CLI_uptime_get extends MethodDescriptor {

		public CLI_uptime_get() {
			super("UPTIME", "Show running time ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]UPTIME: [GREEN]" + Utils.tsElapsed2String(System.currentTimeMillis() - StatsManager.SYSTEM_STATS.STARTUP_TS);
		}

	}
	public CLI_uptime_get _cli_uptime_get = new CLI_uptime_get();

	public class CLI_version_get extends MethodDescriptor {

		public CLI_version_get() {
			super("VERSION", "Show system version ", "", "");
		}

		public String execute(Object[] params) {
			return "[WHITE]VAZOM VERSION: [[GREEN]" + GPUNode.class.getPackage().getSpecificationVersion() + ".[YELLOW]" + GPUNode.class.getPackage().getImplementationVersion() + "[WHITE]]";
		}

	}
	public CLI_version_get _cli_version_get = new CLI_version_get();

	public class CLI_bye_execute extends MethodDescriptor {

		public CLI_bye_execute() {
			super("BYE", "Disconnect ", "", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				conn.stopping = true;
			}
			return "";
		}

	}
	public CLI_bye_execute _cli_bye_execute = new CLI_bye_execute();

	public class CLI_halt_execute extends MethodDescriptor {

		public CLI_halt_execute() {
			super("HALT", "Stop current node ", "", "");
		}

		public String execute(Object[] params) {
			if(params != null){
				CLIConnection conn = (CLIConnection)params[0];
				if(conn.halt_counter >= 2){
					new Thread(){
						public void run(){
							try{ Thread.sleep(2000); }catch(Exception e){ e.printStackTrace(); }
							System.exit(0);
						}
					}.start();
					return "[RED]Node shutting down...";
				}else{
					conn.halt_counter++;
					return "[RED]Node shutdown initiated, please confirm by executing [[GREEN]HALT[RED]] for [[GREEN]" + (3 - conn.halt_counter) + "[RED]] more times!";
				}
			}			
			return "";
		}

	}
	public CLI_halt_execute _cli_halt_execute = new CLI_halt_execute();


    
	public CLIManager(){
		super();
		// methods
		method_lst.add(_cli_show_memory);
		method_lst.add(_cli_show_stats_kernel);
		method_lst.add(_cli_f_info);
		method_lst.add(_cli_conf_get);
		method_lst.add(_cli_conf_set);
		method_lst.add(_cli_conf_search);
		method_lst.add(_cli_uptime_get);
		method_lst.add(_cli_version_get);
		method_lst.add(_cli_bye_execute);
		method_lst.add(_cli_halt_execute);

		// auth
        method_lst.add(_cli_user_list);
        method_lst.add(_cli_grp_list);
        method_lst.add(_cli_user_add);
        method_lst.add(_cli_user_set);
        method_lst.add(_cli_user_rm);
        method_lst.add(_cli_users_passwd);		
		
		// groups
		group_lst.add(_grp_STATS);
		group_lst.add(_grp_SHOW);
		group_lst.add(_grp_F);
		group_lst.add(_grp_CONF);
		group_lst.add(_grp_AUTH);

	}
}
