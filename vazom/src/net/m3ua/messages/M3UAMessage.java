package net.m3ua.messages;

import java.util.ArrayList;

import net.m3ua.M3UAMessageType;
import net.m3ua.parameters.M3UAParameter;

public abstract class M3UAMessage {
	public M3UAMessageType type;
	protected int byte_pos = 0;
	
	public abstract void init(byte[] data);
	//public abstract byte[] encode();
	public abstract int encode(ArrayList<Byte> buff);

	public void initNew(){
		
		
	}
	
	
	protected int processParameter(M3UAParameter param, ArrayList<Byte> buffer){
		byte[] res = null;
		int l = 0;
		int padding_count = 0;
		int m;
		if(param != null){
			res = param.encode();
			l = res.length;
			// parameter has to be a multiple of 4, if not, zero padding is added
			// padding has to be included in total message length
			m = l % 4;
			padding_count = (m > 0 ? 4 - m : 0);
			l += padding_count;
			// parameter value(tag + length + value)
			for(int i = 0; i<res.length; i++) buffer.add(res[i]);
			// padding
			for(int i = 0; i<padding_count; i++) buffer.add((byte)0x00);
			
		}
		return l;
	}
}
