package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class OriginationIndicator extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _prior_agreement =  1 ;
	public static final int _origination_denied =  2 ;
	public static final int _local_calls_only =  3 ;
	public static final int _selected_leading_digits_of_directorynumber_or_of_international_E164_number =  4 ;
	public static final int _selected_leading_digits_of_directorynumber_or_of_international_E164_number_and_local_calls_only =  5 ;
	public static final int _national_long_distance =  6 ;
	public static final int _international_calls =  7 ;
	public static final int _single_directory_number_or_international_E164_number =  8 ;
	public OriginationIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
