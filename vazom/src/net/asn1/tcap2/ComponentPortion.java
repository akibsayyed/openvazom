package net.asn1.tcap2;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
public class ComponentPortion extends SEQUENCE{

	public Component new_child(){
		return new Component();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public Component getChild(int index){
		return (Component)of_children.get(index);
	}
	public ComponentPortion(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 12;
		of_children = new ArrayList<ASNType>();
		asn_class = ASNTagClass.APPLICATION;
	}
}
