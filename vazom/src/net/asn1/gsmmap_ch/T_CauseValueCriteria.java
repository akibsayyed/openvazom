package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class T_CauseValueCriteria extends SEQUENCE{

	public CauseValue new_child(){
		return new CauseValue();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CauseValue getChild(int index){
		return (CauseValue)of_children.get(index);
	}
	public T_CauseValueCriteria(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
