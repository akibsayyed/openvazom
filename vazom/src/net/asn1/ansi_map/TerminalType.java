package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TerminalType extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _not_distinguished =  1 ;
	public static final int _iS_54_B =  2 ;
	public static final int _iS_136 =  3 ;
	public static final int _j_STD_011 =  4 ;
	public static final int _iS_136_A_or_TIA_EIA_136_Revision_0 =  5 ;
	public static final int _tIA_EIA_136_A =  6 ;
	public static final int _iA_EIA_136_B =  7 ;
	public static final int _iS_95 =  32 ;
	public static final int _iS_95B =  33 ;
	public static final int _j_STD_008 =  34 ;
	public static final int _tIA_EIA_95_B =  35 ;
	public static final int _iS_2000 =  36 ;
	public static final int _iS_88 =  64 ;
	public static final int _iS_94 =  65 ;
	public static final int _iS_91 =  66 ;
	public static final int _j_STD_014 =  67 ;
	public static final int _tIA_EIA_553_A =  68 ;
	public static final int _iS_91_A =  69 ;
	public TerminalType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
