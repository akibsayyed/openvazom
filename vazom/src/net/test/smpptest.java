package net.test;


import java.io.File;
import java.io.FileInputStream;
import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;


import net.smpp.ErrorCodeType;
import net.smpp.NumberingPlan;
import net.smpp.OptionalParameterType;
import net.smpp.PDUType;
import net.smpp.SMPP_PDU;
import net.smpp.TypeOfNumber;
import net.smpp.parameters.Data_codingType;
import net.smpp.parameters.GSMNetworkSpecific;
import net.smpp.parameters.IntermediateNotificationType;
import net.smpp.parameters.MessageMode;
import net.smpp.parameters.MessageType;
import net.smpp.parameters.SMEOrigAckType;
import net.smpp.parameters.SMSCDeliveryReceiptType;
import net.smpp.parameters.ServiceType;
import net.smpp.parameters.optional.OptionalParameterBase;
import net.smpp.parameters.optional.Sar_msg_ref_num;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Enquire_link;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_sm;

public class smpptest {
	public static InputStream in;
	public static OutputStream out;
	public static ConcurrentLinkedQueue<byte[]> out_queue;
	public static Reader reader_r;
	public static Thread reader_t;
	public static Writer writer_r;
	public static Thread writer_t;

	
	public static class Reader implements Runnable{
		private InputStream in;
		private ConcurrentLinkedQueue<byte[]> out_queue;
		
		public Reader(InputStream _in, ConcurrentLinkedQueue<byte[]> _out_queue){
			in = _in;
			out_queue = _out_queue;
		}
		public void run() {
			byte[] data = new byte[8192];
			int br;
			System.out.println("Starting...");
			while(true){
				try{
					br = in.read(data);
					if(br > 0){
						System.out.println("----- PACKET RECEIVED START -----");
						PDUBase[] pdu = SMPP_PDU.decode(Arrays.copyOfRange(data, 0, br));
						for(int i = 0; i<pdu.length; i++){
							System.out.println(i + ":" + pdu[i].type);
							if(pdu[i].type == PDUType.BIND_TRANSCEIVER_RESP){
								System.out.println("STATUS: " + pdu[i].command_status);
								if(pdu[i].command_status == ErrorCodeType.ESME_ROK){
									Enquire_link el = new Enquire_link();
									el.sequence_number = 2;
									out_queue.offer(el.encode());
									
								}
							}else if(pdu[i].type == PDUType.ENQUIRE_LINK_RESP){
								System.out.println("STATUS: " + pdu[i].command_status);
								if(pdu[i].command_status == ErrorCodeType.ESME_ROK){
									Submit_sm sm = new Submit_sm();
									sm.sequence_number = 3;
									sm.data_coding = Data_codingType.DEFAULT;
									sm.service_type = ServiceType.DEFAULT;
									sm.source_addr_ton = TypeOfNumber.ALPHANUMERIC;
									sm.source_addr_npi = NumberingPlan.ISDN_TELEPHONE;
									sm.source_addr = "1985";
									sm.dest_addr_ton = TypeOfNumber.ALPHANUMERIC;
									sm.dest_addr_npi = NumberingPlan.ISDN_TELEPHONE;
									sm.destination_addr = "1986";
									sm.message_mode = MessageMode.DEFAULT_SMSC_MODE;
									sm.message_type = MessageType.DEFAULT;
									sm.ns_features = GSMNetworkSpecific.NO_SPECIFIC_FEATURES;
									sm.protocol_id = 0;
									sm.priority_flag = 0;
									sm.schedule_delivery_time = "";
									sm.validity_period = "";
									sm.rd_smsc_delivery_receipt = SMSCDeliveryReceiptType.NO_SMSC_DELIVERY;
									sm.rd_sme_orig_ack = SMEOrigAckType.NO_SME_ACK;
									sm.rd_intermediate_notification = IntermediateNotificationType.NO;
									sm.replace_if_present_flag = 0;
									sm.sm_default_msg_id = 0;
									sm.short_message = (new String("test")).getBytes();

									out_queue.offer(sm.encode());
									
								
								}
							}
							
						}
						System.out.println("----- PACKET RECEIVED END -----");
					}
				}catch(Exception e){
					e.printStackTrace();
				}

			}
		}
		
	}
	public static class Writer implements Runnable{
		private OutputStream out;
		private ConcurrentLinkedQueue<byte[]> out_queue;
		public Writer(OutputStream _out, ConcurrentLinkedQueue<byte[]> _out_queue){
			out = _out;
			out_queue = _out_queue;
		}
		public void run() {
			byte[] data;
			System.out.println("Starting...");
			while(true){
				data = out_queue.poll();
				if(data != null){
					try{
						out.write(data);
						out.flush();
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
			}
			
		}
		
	}

	public static void main(String[] args) {
		try{

			
			out_queue = new ConcurrentLinkedQueue<byte[]>();
			Socket socket = new Socket("10.4.6.7", 5566);
			in = socket.getInputStream();
			out = socket.getOutputStream();

			reader_r = new Reader(in, out_queue);
			writer_r = new Writer(out, out_queue);
			reader_t = new Thread(reader_r, "READER");
			reader_t.start();
			writer_t = new Thread(writer_r, "WRITER");
			writer_t.start();
			
			Bind_transceiver bt = new Bind_transceiver();
			bt.system_id = args[0];
			bt.password = args[1];
			bt.system_type = "";
			bt.interface_version = 0x34;
			bt.addr_ton = TypeOfNumber.UNKNOWN;
			bt.addr_npi = NumberingPlan.UNKNOWN;
			bt.address_range = "";
			bt.sequence_number = 1;
			out_queue.offer(bt.encode());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
