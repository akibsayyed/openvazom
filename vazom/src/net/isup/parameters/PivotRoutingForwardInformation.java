package net.isup.parameters;

import net.isup.ParameterType;

public class PivotRoutingForwardInformation extends ParameterBase {
	public PivotRoutingForwardInformation(){
		type = ParameterType.PIVOT_ROUTING_FORWARD_INFORMATION;
	}
}
