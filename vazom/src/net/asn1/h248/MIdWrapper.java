package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MIdWrapper extends SEQUENCE{

	public MId get_serviceChangeMgcId(){
		return (MId)elements.get(0).data;
	}
	public MId new_serviceChangeMgcId(){
		return new MId();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_serviceChangeMgcId(); }});
	}
	public MIdWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
