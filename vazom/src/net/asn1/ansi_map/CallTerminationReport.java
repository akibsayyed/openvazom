package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CallTerminationReport extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(1).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(2).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public NetworkTMSI get_networkTMSI(){
		return (NetworkTMSI)elements.get(3).data;
	}
	public NetworkTMSI new_networkTMSI(){
		return new NetworkTMSI();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(233, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkTMSI(); }});
	}
	public CallTerminationReport(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
