package net.asn1.types;

import java.util.ArrayList;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagClass;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ElementDescriptor;
//import net.asn1.compiler.TagDescriptor;

public class EXTERNAL extends SEQUENCE {
/*
	@TagDescriptor(tagValue = 6, tagClass = ASNTagClass.UNIVERSAL, optional = true)
	public OBJECT_IDENTIFIER direct_reference;
	
	@TagDescriptor(tagValue = 2, tagClass = ASNTagClass.UNIVERSAL, optional = true)
	public INTEGER indirect_reference;

	@TagDescriptor(tagValue = 7, tagClass = ASNTagClass.UNIVERSAL, optional = true)
	public OBJECT_DESCRIPTOR data_value_descriptor;

	@TagDescriptor(tagValue = -3, tagClass = ASNTagClass.UNIVERSAL)
	public EXTERNAL_ENCODING encoding;
*/
	
	public OBJECT_IDENTIFIER get_direct_reference(){
		return (OBJECT_IDENTIFIER)elements.get(0).data;
	}
	public OBJECT_IDENTIFIER new_direct_reference(){
		return new OBJECT_IDENTIFIER();
	}
	
	public INTEGER get_indirect_reference(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_indirect_reference(){
		return new INTEGER();
	}
	
	public OBJECT_DESCRIPTOR get_data_value_descriptor(){
		return (OBJECT_DESCRIPTOR)elements.get(2).data;
	}
	public OBJECT_DESCRIPTOR new_data_value_descriptor(){
		return new OBJECT_DESCRIPTOR();
	}
	
	public EXTERNAL_ENCODING get_encoding(){
		return (EXTERNAL_ENCODING)elements.get(3).data;
	}
	public EXTERNAL_ENCODING new_encoding(){
		return new EXTERNAL_ENCODING();
	}
	
	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_direct_reference(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_indirect_reference(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_data_value_descriptor(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_encoding(); }});
	}
	
	public EXTERNAL(){
		super();
		universalTag = ASNTag.EXTERNAL;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Constructed;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		
	}
}
