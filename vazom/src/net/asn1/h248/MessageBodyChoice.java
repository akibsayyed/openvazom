package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MessageBodyChoice extends CHOICE{

	public ErrorDescriptor get_messageError(){
		return (ErrorDescriptor)elements.get(0).data;
	}
	public ErrorDescriptor new_messageError(){
		return new ErrorDescriptor();
	}

	public TransactionsSequenceOf get_transactions(){
		return (TransactionsSequenceOf)elements.get(1).data;
	}
	public TransactionsSequenceOf new_transactions(){
		return new TransactionsSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_messageError(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactions(); }});
	}
	public MessageBodyChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
