package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class D_CSI extends SEQUENCE{

	public DP_AnalysedInfoCriteriaList get_dp_AnalysedInfoCriteriaList(){
		return (DP_AnalysedInfoCriteriaList)elements.get(0).data;
	}
	public DP_AnalysedInfoCriteriaList new_dp_AnalysedInfoCriteriaList(){
		return new DP_AnalysedInfoCriteriaList();
	}

	public CamelCapabilityHandling get_camelCapabilityHandling(){
		return (CamelCapabilityHandling)elements.get(1).data;
	}
	public CamelCapabilityHandling new_camelCapabilityHandling(){
		return new CamelCapabilityHandling();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NULL get_notificationToCSE(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_notificationToCSE(){
		return new NULL();
	}

	public NULL get_csi_Active(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_csi_Active(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dp_AnalysedInfoCriteriaList(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_camelCapabilityHandling(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_notificationToCSE(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_csi_Active(); }});
	}
	public D_CSI(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
