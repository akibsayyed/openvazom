package net.smstpdu.tpdcs.general;

import java.util.HashMap;


public enum MessageClass {
	CLASS0(0x00),
	CLASS1(0x01),
	CLASS2(0x02),
	CLASS3(0x03);

	// BITS 1 and 0  = 000000XX

	
	private int id;
	private static final HashMap<Integer, MessageClass> lookup = new HashMap<Integer, MessageClass>();
	static{
		for(MessageClass td : MessageClass.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static MessageClass get(int id){ return lookup.get(id); }
	private MessageClass(int _id){ id = _id; }
	
}
