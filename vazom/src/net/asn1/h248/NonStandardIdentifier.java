package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NonStandardIdentifier extends CHOICE{

	public OBJECT_IDENTIFIER get_object(){
		return (OBJECT_IDENTIFIER)elements.get(0).data;
	}
	public OBJECT_IDENTIFIER new_object(){
		return new OBJECT_IDENTIFIER();
	}

	public H221NonStandard get_h221NonStandard(){
		return (H221NonStandard)elements.get(1).data;
	}
	public H221NonStandard new_h221NonStandard(){
		return new H221NonStandard();
	}

	public IA5STRING get_experimental(){
		return (IA5STRING)elements.get(2).data;
	}
	public IA5STRING new_experimental(){
		return new IA5STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_object(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_h221NonStandard(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_experimental(); }});
	}
	public NonStandardIdentifier(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
