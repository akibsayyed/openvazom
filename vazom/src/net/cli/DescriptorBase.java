package net.cli;

public abstract class DescriptorBase {
	public String name;
	public String description;
	public String parent;
	public MethodType type;
	public String[] params;
	
	public abstract String execute(Object[] params);
	
	
	
	public DescriptorBase(String _name, String _description, String _parent, String _params){
		name = _name;
		description = _description;
		parent = _parent;
		
		if(_params.length() > 0){
			String[] tmp = _params.split(":");
			params = new String[tmp.length];
			for(int i = 0; i<tmp.length; i++) params[i] = tmp[i];
			
		}
	}
	
	
}
