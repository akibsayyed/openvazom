package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RegistrationNotification extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MEID get_meid(){
		return (MEID)elements.get(2).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(3).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public QualificationInformationCode get_qualificationInformationCode(){
		return (QualificationInformationCode)elements.get(4).data;
	}
	public QualificationInformationCode new_qualificationInformationCode(){
		return new QualificationInformationCode();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(5).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public AvailabilityType get_availabilityType(){
		return (AvailabilityType)elements.get(6).data;
	}
	public AvailabilityType new_availabilityType(){
		return new AvailabilityType();
	}

	public BorderCellAccess get_borderCellAccess(){
		return (BorderCellAccess)elements.get(7).data;
	}
	public BorderCellAccess new_borderCellAccess(){
		return new BorderCellAccess();
	}

	public CDMANetworkIdentification get_cdmaNetworkIdentification(){
		return (CDMANetworkIdentification)elements.get(8).data;
	}
	public CDMANetworkIdentification new_cdmaNetworkIdentification(){
		return new CDMANetworkIdentification();
	}

	public ControlChannelData get_controlChannelData(){
		return (ControlChannelData)elements.get(9).data;
	}
	public ControlChannelData new_controlChannelData(){
		return new ControlChannelData();
	}

	public ControlChannelMode get_controlChannelMode(){
		return (ControlChannelMode)elements.get(10).data;
	}
	public ControlChannelMode new_controlChannelMode(){
		return new ControlChannelMode();
	}

	public ExtendedMSCID get_extendedMSCID(){
		return (ExtendedMSCID)elements.get(11).data;
	}
	public ExtendedMSCID new_extendedMSCID(){
		return new ExtendedMSCID();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(12).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MSC_Address get_msc_Address(){
		return (MSC_Address)elements.get(13).data;
	}
	public MSC_Address new_msc_Address(){
		return new MSC_Address();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(14).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(15).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public ReceivedSignalQuality get_receivedSignalQuality(){
		return (ReceivedSignalQuality)elements.get(16).data;
	}
	public ReceivedSignalQuality new_receivedSignalQuality(){
		return new ReceivedSignalQuality();
	}

	public ReportType get_reportType(){
		return (ReportType)elements.get(17).data;
	}
	public ReportType new_reportType(){
		return new ReportType();
	}

	public ServiceRedirectionCause get_serviceRedirectionCause(){
		return (ServiceRedirectionCause)elements.get(18).data;
	}
	public ServiceRedirectionCause new_serviceRedirectionCause(){
		return new ServiceRedirectionCause();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(19).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SMS_Address get_sms_Address(){
		return (SMS_Address)elements.get(20).data;
	}
	public SMS_Address new_sms_Address(){
		return new SMS_Address();
	}

	public SMS_MessageWaitingIndicator get_sms_MessageWaitingIndicator(){
		return (SMS_MessageWaitingIndicator)elements.get(21).data;
	}
	public SMS_MessageWaitingIndicator new_sms_MessageWaitingIndicator(){
		return new SMS_MessageWaitingIndicator();
	}

	public SystemAccessData get_systemAccessData(){
		return (SystemAccessData)elements.get(22).data;
	}
	public SystemAccessData new_systemAccessData(){
		return new SystemAccessData();
	}

	public SystemAccessType get_systemAccessType(){
		return (SystemAccessType)elements.get(23).data;
	}
	public SystemAccessType new_systemAccessType(){
		return new SystemAccessType();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(24).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(25).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(26).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(27).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public MPCAddress get_mpcAddress(){
		return (MPCAddress)elements.get(28).data;
	}
	public MPCAddress new_mpcAddress(){
		return new MPCAddress();
	}

	public MPCAddressList get_mpcAddressList(){
		return (MPCAddressList)elements.get(29).data;
	}
	public MPCAddressList new_mpcAddressList(){
		return new MPCAddressList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(17, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_qualificationInformationCode(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(90, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_availabilityType(); }});
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_borderCellAccess(); }});
		elements.add(new ElementDescriptor(232, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaNetworkIdentification(); }});
		elements.add(new ElementDescriptor(55, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelData(); }});
		elements.add(new ElementDescriptor(199, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelMode(); }});
		elements.add(new ElementDescriptor(53, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedMSCID(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(284, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msc_Address(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(72, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_receivedSignalQuality(); }});
		elements.add(new ElementDescriptor(44, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reportType(); }});
		elements.add(new ElementDescriptor(237, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceRedirectionCause(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(104, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_Address(); }});
		elements.add(new ElementDescriptor(118, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_MessageWaitingIndicator(); }});
		elements.add(new ElementDescriptor(56, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemAccessData(); }});
		elements.add(new ElementDescriptor(34, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemAccessType(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemCapabilities(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(370, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_mpcAddress(); }});
		elements.add(new ElementDescriptor(381, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_mpcAddressList(); }});
	}
	public RegistrationNotification(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
