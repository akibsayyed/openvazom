package net.m3ua_conn;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;
import net.config.SGNConfigData;
import net.logging.LoggingManager;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.ASPAC;
import net.m3ua.messages.ASPACACK;
import net.m3ua.messages.ASPDN;
import net.m3ua.messages.ASPIA;
import net.m3ua.messages.ASPIAACK;
import net.m3ua.messages.ASPUPACK;
import net.m3ua.messages.DAVA;
import net.m3ua.messages.DUNA;
import net.m3ua.messages.NTFY;
import net.m3ua.parameters.AffetedPointCode;
import net.m3ua.parameters.RoutingContext;
import net.m3ua.parameters.Status;
import net.m3ua.parameters.TrafficModeType;
import net.m3ua.parameters.status.StatusInfoType;
import net.m3ua.parameters.status.StatusType;
import net.m3ua.parameters.tmt.TMTType;
import net.routing.RoutingConnectionDirectionType;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.sctp.SSctp.Timeout;

import org.apache.log4j.Logger;

public class M3UAConnManager {
	public static Logger logger=Logger.getLogger(M3UAConnManager.class);
	public static ConcurrentHashMap<String, M3UAConnection> conn_map;
	public static String default_conn = null;
	public static boolean stopping;
	private static M3UAReconnect m3uar_r;
	private static Thread m3uar_t;

	
	private static class M3UAReconnect implements Runnable{
		public void run() {
			M3UAConnection conn = null;
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Starting...");
			while(!stopping){
				try{ Thread.sleep(SGNConfigData.m3ua_reconnect_interval * 1000); }catch(Exception e){ e.printStackTrace(); }
				for(String s: conn_map.keySet()){
					conn = conn_map.get(s);
					if(!conn.active && !conn.down){
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Reactivating M3UA Connection [" + conn.label + "], address = [" + conn.remote_ip + "], port = [" + conn.remote_port + "], local bind = [" + conn.local_ip + ":" + conn.local_port + "]!");
						reconnect(conn.label);
						notify_all_DAVA(conn.label);
					}
				}
				

			}
			
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Ending...");
			
		}
	}
	
	
	public static void start_M3UA_monitor(){
		if(SGNConfigData.m3ua_reconnect_status){
			m3uar_r = new M3UAReconnect();
			m3uar_t = new Thread(m3uar_r, "M3UA_RECONNECT_MONITOR");
			m3uar_t.start();
			
		}
	}
	public static void shut_rc(int rctx){
		M3UAConnection conn = null;
		
		for(String s : conn_map.keySet()){
			conn = conn_map.get(s);
			if(conn.routing_ctx == rctx && conn.active){
				conn.active = false;
				conn.stopping = true;
			}
		}		
	
	}

	public static void noshut_rc(int rctx){
		M3UAConnection conn = null;
		
		for(String s : conn_map.keySet()){
			conn = conn_map.get(s);
			if(conn.routing_ctx == rctx && !conn.down){
				conn.start();
			}
		}		
	
	}
	
	
	public static void process_DUNA(int pc, int rctx){
		M3UAConnection conn = null;
		
		for(String s : conn_map.keySet()){
			conn = conn_map.get(s);
			if(conn.dpc == pc && conn.routing_ctx == rctx && conn.active){
				conn.active = false;
				conn.stopping = true;
			}
		}		
	}

	public static void process_DAVA(int pc, int rctx){
		M3UAConnection conn = null;
		
		for(String s : conn_map.keySet()){
			conn = conn_map.get(s);
			if(conn.dpc == pc && conn.routing_ctx == rctx && !conn.active && !conn.down){
				conn.start();
			}
		}		
	}

	public static void notify_all_DAVA(String conn_id){
		M3UAConnection conn = getConnection(conn_id);
		SSCTPDescriptor sd = null;
		M3UAPayload mpl = null;
		if(conn != null){
			M3UAPacket mp = M3UA.prepareNew(M3UAMessageType.DAVA);
			DAVA dava = (DAVA)mp.message;
			dava.affectedPointCode = new AffetedPointCode();
			dava.affectedPointCode.setAffetedPointCode(conn.dpc);
			dava.routingContext = new RoutingContext();
			dava.routingContext.setRoutingContext(conn.routing_ctx);
			sd = new SSCTPDescriptor(0, mp.encode());
			mpl = new M3UAPayload();
			mpl.sd = sd;
			mpl.m3ua_packet = mp;
			for(String s : conn_map.keySet()){
				if(!s.equals(conn_id)){
					conn = conn_map.get(s);
					conn.out_offer(mpl);
				}
			}

		}

	}
	
	public static void notify_all_DUNA(String conn_id){
		M3UAConnection conn = getConnection(conn_id);
		SSCTPDescriptor sd = null;
		M3UAPayload mpl = null;
		if(conn != null){
			M3UAPacket mp = M3UA.prepareNew(M3UAMessageType.DUNA);
			DUNA duna = (DUNA)mp.message;
			duna.affectedPointCode = new AffetedPointCode();
			duna.affectedPointCode.setAffetedPointCode(conn.dpc);
			duna.routingContext = new RoutingContext();
			duna.routingContext.setRoutingContext(conn.routing_ctx);
			sd = new SSCTPDescriptor(0, mp.encode());
			mpl = new M3UAPayload();
			mpl.sd = sd;
			mpl.m3ua_packet = mp;
			for(String s : conn_map.keySet()){
				if(!s.equals(conn_id)){
					conn = conn_map.get(s);
					conn.out_offer(mpl);
				}
			}

		}
	}
	public static boolean init_m3ua_server(int sctp_conn_id, int rc){
		M3UAPacket mp;
		ASPACACK aspacack;
		NTFY ntfy;
		byte[] data;
		SSCTPDescriptor sd = null;
		boolean ssctp_ready = false;
		Timeout t;
		
		LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: M3UA Init, ID: [ " + sctp_conn_id + " ]!");

		
		while(!ssctp_ready){
			// timeout
			t = SSctp.runTimeout(5000, sctp_conn_id);
			// receive
			sd = SSctp.receiveM3ua(sctp_conn_id);
			t.stop();
			
			if(sd != null){
				if(sd.payload != null){
					mp = M3UA.decode(sd.payload);
					LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Received " + mp.message.type);
					if(mp.message.type == M3UAMessageType.ASPUP){
						// ASPUP ACK
						mp = M3UA.prepareNew(M3UAMessageType.ASPUP_ACK);
						data = mp.encode();
						SSctp.sendM3ua(sctp_conn_id, data, sd.sid);
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Sent " + mp.message.type);
						
						// NTFY
						mp = M3UA.prepareNew(M3UAMessageType.NTFY);
						ntfy = (NTFY)mp.message;
						ntfy.status = new Status();
						ntfy.status.setStatus(StatusType.AS_STATE_CHANGE, StatusInfoType.AS_INACTIVE);
						data = mp.encode();
						SSctp.sendM3ua(sctp_conn_id, data, sd.sid);
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Sent " + mp.message.type);
						
					}else if(mp.message.type == M3UAMessageType.ASPAC){
						mp = M3UA.prepareNew(M3UAMessageType.ASPAC_ACK);
						aspacack = (ASPACACK)mp.message;
						aspacack.trafficModeType = new TrafficModeType(TMTType.LOADSHARE);
						aspacack.routingContext = new RoutingContext();
						aspacack.routingContext.setRoutingContext(rc); 
						data = mp.encode();
						SSctp.sendM3ua(sctp_conn_id, data, sd.sid);
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Sent " + mp.message.type);

						// NTFY
						mp = M3UA.prepareNew(M3UAMessageType.NTFY);
						ntfy = (NTFY)mp.message;
						ntfy.status = new Status();
						ntfy.status.setStatus(StatusType.AS_STATE_CHANGE, StatusInfoType.AS_ACTIVE);
						data = mp.encode();
						SSctp.sendM3ua(sctp_conn_id, data, sd.sid);
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Sent " + mp.message.type);
						
						
						ssctp_ready = true;
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: SCTP M3UA Connection Ready: [ " + sctp_conn_id + " ]!");
						return true;
					}else if(mp.message.type == M3UAMessageType.ERR){
						return false;
					}

				}else{
					return false;
				}
			}else{
				return false;
				
			}			
			
		}
		return false;	}	
	
	public static boolean init_m3ua_client(int sctp_conn_id, int rc){
		M3UAPacket mp;
		ASPAC aspac;
		NTFY ntfy;
		byte[] data;
		SSCTPDescriptor sd = null;
		boolean ssctp_ready = false;
		long ts = 0;
		Timeout t;
		boolean aspac_ack_received = false;
		
		LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: M3UA Init, ID: [ " + sctp_conn_id + " ]!");

		mp = M3UA.prepareNew(M3UAMessageType.ASPUP);
		data = mp.encode();
		SSctp.sendM3ua(sctp_conn_id, data, 0);
		LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Sent " + mp.message.type);

		
		
		while(!ssctp_ready){
			// timeout
			t = SSctp.runTimeout(5000, sctp_conn_id);
			// receive
			sd = SSctp.receiveM3ua(sctp_conn_id);
			t.stop();
			
			if(sd != null){
				if(sd.payload != null){
					mp = M3UA.decode(sd.payload);
					LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Received " + mp.message.type);
					if(mp.message.type == M3UAMessageType.ASPUP_ACK){
						mp = M3UA.prepareNew(M3UAMessageType.ASPAC);
						aspac = (ASPAC)mp.message;
						aspac.trafficModeType = new TrafficModeType(TMTType.LOADSHARE);
						aspac.routingContext = new RoutingContext();
						aspac.routingContext.setRoutingContext(rc); 
						data = mp.encode();
						SSctp.sendM3ua(sctp_conn_id, data, sd.sid);
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: Sent " + mp.message.type);
					}else if(mp.message.type == M3UAMessageType.NTFY && aspac_ack_received){
						ntfy = (NTFY)mp.message;
						if(ntfy.status != null){
							if(ntfy.status.statusType == StatusType.AS_STATE_CHANGE && ntfy.status.statusInfo == StatusInfoType.AS_INACTIVE){
								return false;
							}else{
								ssctp_ready = true;
								LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "SSCTP: SCTP M3UA Connection Ready: [ " + sctp_conn_id + " ]!");
								return true;
								
							}
						}else return false;
					}else if(mp.message.type == M3UAMessageType.ASPAC_ACK){
						aspac_ack_received = true;
					}else if(mp.message.type == M3UAMessageType.ERR){
						return false;
					}

				}else{
					return false;
				}
			}else{
				return false;
				
			}			
			
		}
		return false;
	}
	public static boolean connectionExists(String conn_id){
		return (conn_map.get(conn_id) != null);
	}
	public static void reconnect(String conn_id){
		M3UAConnection conn = getConnection(conn_id);
		int cid;
		if(conn != null){
			/*
			if(!conn.sctp_active){
				// close previous
				if(conn.sctp_id > -1) SSctp.shutdownClient(conn.sctp_id);
				// wait 1 sec
				try{ Thread.sleep(1000); }catch(Exception e){}
				
			}
			*/
			
			switch(conn.direction){
				case CLIENT:
					if(!conn.sctp_active){
						// close previous if necessary
						if(conn.sctp_id > -1) SSctp.shutdownClient(conn.sctp_id);
						// init SCTP
						if(conn.local_ip.equals("*")) cid = SSctp.initClient(conn.remote_ip, conn.remote_port, conn.stream_count, null, 0);
						else cid = SSctp.initClient(conn.remote_ip, conn.remote_port, conn.stream_count, conn.local_ip, conn.local_port);
					}else cid = conn.sctp_id;

					if(cid > -1){
						conn.sctp_id = cid;
						conn.sctp_active = true;
						// init M3UA
						if(init_m3ua_client(cid, conn.routing_ctx)){
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection [" + conn.label + "] reactivated!");
							conn.start();
						}else{
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Error while negotiating M3UA connection: [" + conn_id + "]!");
						}
					}else{
						LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Error while initializing SCTP connection for M3UA [" + conn_id + "]!");
					}
					break;
				case SERVER:
					if(!conn.sctp_active){
						// close previous if necessary
						if(conn.server_sctp_id > -1) SSctp.shutdownServer(conn.server_sctp_id);
						// init SCTP
						if(conn.local_ip.equals("*")) cid = SSctp.initServer("0.0.0.0", conn.local_port);
						else cid = SSctp.initServer(conn.local_ip, conn.local_port);
					}else cid = conn.server_sctp_id;

					conn.server_sctp_id = cid;
					conn.active = true;
					conn.sctp_active = true;
					// start sctp client listener
					conn.start_server_listener();
					break;
			}
		}
	}
	
	public static void addConnection(String conn_id, 
									String label, 
									String local_ip,
									int local_port,
									String remote_ip, 
									int remote_port,
									int dpc,
									int opc,
									int network_app,
									int routing_ctx,
									int stream_count,
									int beat_interval,
									int max_beat_miss_count,
									RoutingConnectionDirectionType direction){
		int cid = -1;
		if(!connectionExists(conn_id)){
			// init M3UA
			// connection descriptor
			M3UAConnection conn = new M3UAConnection(-1);
			conn.id = label;
			conn.label = label;
			conn.local_ip = local_ip;
			conn.local_port = local_port;
			conn.remote_ip = remote_ip;
			conn.remote_port = remote_port;
			conn.dpc = dpc;
			conn.opc = opc;
			conn.network_app = network_app;
			conn.routing_ctx = routing_ctx;
			conn.stream_count = stream_count;
			conn.beat_interval = beat_interval;
			conn.max_beat_miss_count = max_beat_miss_count;
			// add to map
			conn_map.put(label, conn);

			switch(direction){
				case CLIENT:
					// init SCTP
					if(local_ip.equals("*")) cid = SSctp.initClient(remote_ip, remote_port, stream_count, null, 0);
					else cid = SSctp.initClient(remote_ip, remote_port, stream_count, local_ip, local_port);
					if(cid > -1){
						conn.sctp_id = cid;
						conn.sctp_active = true;
						if(init_m3ua_client(cid, routing_ctx)){
							// start connection threds
							conn.start();
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [ID = " + conn_id + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [LABEL = " + conn_id + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [BIND ADDR = " + local_ip + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [BIND PORT = " + local_port + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [REMOTE ADDR = " + remote_ip + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [REMOTE PORT = " + remote_port + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [DPC = " + dpc + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [OPC = " + opc + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [NETWORK APP = " + network_app + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [ROUTING CTX = " + routing_ctx + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [SCTP STREAM COUNT = " + stream_count + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [M3UA BEAT INTERVAL = " + beat_interval + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [MAX BEAT MISS COUNT = " + max_beat_miss_count + "]");
							LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection: [DIRECTION = " + direction.toString() + "]");
						}else LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Error while negotiating M3UA connection: [" + conn_id + "]!");
						
					}else LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Error while initializing SCTP connection for M3UA");
					
					break;
				case SERVER:
					// init SCTP
					if(local_ip.equals("*")) cid = SSctp.initServer("0.0.0.0", local_port);
					else cid = SSctp.initServer(local_ip, local_port);
					conn.server_sctp_id = cid;
					conn.sctp_active = true;
					conn.active = true;
					conn.direction = RoutingConnectionDirectionType.SERVER;
					// start sctp client listener
					conn.start_server_listener();
					break;
			}
			
		}else{
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "Connection [" + conn_id + "] already exists!");
		}
		
	}
	public static void send_fail_check(M3UAPayload mp, String conn_lst){
		String[] tmp = conn_lst.split(":");
		mp.connections = new ArrayList<String>();
		for(int i = 0; i<tmp.length; i++) mp.connections.add(tmp[i]);
		M3UAConnection conn = getConnection(mp.connections.get(0));
		if(conn != null){
			conn.out_offer(mp);
		}else{
			LoggingManager.debug(SGNConfigData.m3ua_debug, logger, "M3UA Connection [" + mp.connections.get(0) + "] does not exist!");
		}
		
		
	}
	public static void removeConnection(String conn_id){
		if(conn_id != null){
			conn_map.remove(conn_id);
			
		}
	}
	
	public static M3UAConnection shutConnection(String conn_id, boolean shut_sctp){
		if(conn_id != null){
			M3UAConnection conn = conn_map.get(conn_id);
			if(conn != null){
				if(conn.active){
					conn.down = true;
					//conn.stop();
					if(shut_sctp){
						// send ASPDOWN
						M3UAPacket mp_new = M3UA.prepareNew(M3UAMessageType.ASPDN);
						ASPDN aspdn = (ASPDN)mp_new.message;
						SSCTPDescriptor sd = new SSCTPDescriptor(0, mp_new.encode());
						SSctp.sendM3ua(conn.sctp_id, sd.payload, sd.sid);

						//SSctp.shutdownClient(conn.sctp_id);
						//conn.sctp_active = false;
					}else{
						// send ASPIA
						M3UAPacket mp_new = M3UA.prepareNew(M3UAMessageType.ASPIA);
						ASPIA aspia = (ASPIA)mp_new.message;
						aspia.routingContext = new RoutingContext();
						aspia.routingContext.setRoutingContext(conn.routing_ctx);
						SSCTPDescriptor sd = new SSCTPDescriptor(0, mp_new.encode());
						SSctp.sendM3ua(conn.sctp_id, sd.payload, sd.sid);
						
					}
					
				}
				return conn;
			}
			
		}
		return null;
	}
	public static M3UAConnection noshutConnection(String conn_id){
		if(conn_id != null){
			M3UAConnection conn = conn_map.get(conn_id);
			if(conn != null){
				if(!conn.active){
					conn.down = false;
					reconnect(conn_id);
					return conn;
					
				}
			}
			//return null;
		}
		return null;
	}
	
	public static M3UAConnection getConnection(String conn_id){
		if(conn_id != null){
			return conn_map.get(conn_id);
			
		}
		return null;
	}
	public static void init(){
		LoggingManager.info(logger, "Starting M3UA Connection Manager...");
		conn_map = new ConcurrentHashMap<String, M3UAConnection>();
		LoggingManager.info(logger, "Starting M3UA Connection Monitor...");
		start_M3UA_monitor();
	}
}
