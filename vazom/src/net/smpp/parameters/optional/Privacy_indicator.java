package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Privacy_indicator extends OptionalParameterBase {
	public enum ValueType{
		PRIVACY_LEVEL_0(0x00),
		PRIVACY_LEVEL_1(0x01),
		PRIVACY_LEVEL_2(0x02),
		PRIVACY_LEVEL_3(0x03);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public Privacy_indicator(){
		type = OptionalParameterType.PRIVACY_INDICATOR;
	}
	public ValueType value;
	
	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
