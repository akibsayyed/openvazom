package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ChangeServiceRes extends SET{

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(0).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(1).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(2).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public ChangeServiceAttributes get_changeServiceAttributes(){
		return (ChangeServiceAttributes)elements.get(3).data;
	}
	public ChangeServiceAttributes new_changeServiceAttributes(){
		return new ChangeServiceAttributes();
	}

	public DataKey get_dataKey(){
		return (DataKey)elements.get(4).data;
	}
	public DataKey new_dataKey(){
		return new DataKey();
	}

	public DataPrivacyParameters get_dataPrivacyParameters(){
		return (DataPrivacyParameters)elements.get(5).data;
	}
	public DataPrivacyParameters new_dataPrivacyParameters(){
		return new DataPrivacyParameters();
	}

	public RandomVariable get_randomVariable(){
		return (RandomVariable)elements.get(6).data;
	}
	public RandomVariable new_randomVariable(){
		return new RandomVariable();
	}

	public ReasonList get_reasonList(){
		return (ReasonList)elements.get(7).data;
	}
	public ReasonList new_reasonList(){
		return new ReasonList();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(8).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(9).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(214, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_changeServiceAttributes(); }});
		elements.add(new ElementDescriptor(215, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataKey(); }});
		elements.add(new ElementDescriptor(216, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataPrivacyParameters(); }});
		elements.add(new ElementDescriptor(40, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariable(); }});
		elements.add(new ElementDescriptor(218, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reasonList(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
	}
	public ChangeServiceRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
