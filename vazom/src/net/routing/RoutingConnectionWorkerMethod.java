package net.routing;

public abstract class RoutingConnectionWorkerMethod {
	public RoutingConnection conn;
	
	
	public abstract void execute(Object o);
	
	public RoutingConnectionWorkerMethod(RoutingConnection _conn){
		conn = _conn;
	}

}
