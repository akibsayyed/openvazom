package net.smsfs;

import java.io.BufferedReader;
import java.io.ByteArrayInputStream;
import java.io.File;
import java.io.FileReader;
import java.io.PrintWriter;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;
import java.util.concurrent.CopyOnWriteArrayList;

import net.config.FNConfigData;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.ANTLRInputStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.Tree;
import org.apache.log4j.Logger;

public class SmsfsManager {
	private static Logger logger=Logger.getLogger(SmsfsManager.class);
	public static ConcurrentLinkedQueue<SmsfsPacket> queue;
	private static ArrayList<SmsfsWorker> workerLst;
	public static FilterDescriptor currentFilterMO;
	public static FilterDescriptor currentFilterMT;
	public static FilterDescriptor currentFilterSMPP_MO;
	public static FilterDescriptor currentFilterSMPP_MT;
	public static FilterDescriptor currentFilterHLR;
	public static FilterDescriptor currentFilterISUP;

	public static FilterDescriptor simulationFilterMO;
	public static FilterDescriptor simulationFilterMT;
	public static FilterDescriptor simulationFilterSMPP_MO;
	public static FilterDescriptor simulationFilterSMPP_MT;
	public static FilterDescriptor simulationFilterHLR;
	public static FilterDescriptor simulationFilterISUP;
	public static String current_filter_data = "";
	public static String simulation_filter_data = "";
	//public static CopyOnWriteArrayList<String> sccp_gt_called_wl;
	//public static CopyOnWriteArrayList<String> sccp_gt_calling_wl;
	//public static CopyOnWriteArrayList<String> map_scoa_wl;
	//public static CopyOnWriteArrayList<String> map_scda_wl;

	public static void init(){
		SmsfsWorker fw;
		//smscAllowed = new ArrayList<String>();
		//smscExceptions = new ArrayList<String>();
		//sccp_gt_called_wl = new CopyOnWriteArrayList<String>();
		//sccp_gt_calling_wl = new CopyOnWriteArrayList<String>();
		//map_scda_wl = new CopyOnWriteArrayList<String>();
		//map_scoa_wl = new CopyOnWriteArrayList<String>();
		queue = new ConcurrentLinkedQueue<SmsfsPacket>();
		workerLst = new ArrayList<SmsfsWorker>();
		//int count = Integer.parseInt(ConfigManager.get("filter.threads"));
		logger.info("Creating SMSFS Workers: [" + FNConfigData.smsfs_workers + "]");
		for(int i = 0; i<FNConfigData.smsfs_workers; i++){
			fw = new SmsfsWorker(i);
			workerLst.add(fw);
		}
		
	}
	public static String currentFilter2xml(){
		String res = "<XML>";
		RuleDescriptor rd = null;
		ModifierDescriptor md = null;
		try{
			res += "<MO>";
			if(currentFilterMO != null){
				for(int i = 0; i<currentFilterMO.rule_mod_lst.size(); i++) if(currentFilterMO.rule_mod_lst.get(i).type == FilterDescriptorType.RULE){
					//System.out.println("I: " + i);
					res += "<RULE>";
					rd = (RuleDescriptor)currentFilterMO.rule_mod_lst.get(i);
					res += "<ACTIVE>" + (rd.active ? 1 : 0) + "</ACTIVE>";
					res += "<POINTS>" + rd.points + "</POINTS>";
					res += "<EVAL_ITEM_1>" + rd.evalItem1 + "</EVAL_ITEM_1>";
					res += "<EVAL_ITEM_2>" + rd.evalItem2 + "</EVAL_ITEM_2>";
					res += "<OPERATOR>" + rd.evalOperator + "</OPERATOR>";
					res += "<EVAL_TRUE>" + rd.ruleEvalTrue + "</EVAL_TRUE>";
					res += "<EVAL_FALSE>" + rd.ruleEvalFalse + "</EVAL_FALSE>";
					res += "</RULE>";
					
				}else if(currentFilterMO.rule_mod_lst.get(i).type == FilterDescriptorType.MODIFIER){
					res += "<MODIFIER>";
					md = (ModifierDescriptor)currentFilterMO.rule_mod_lst.get(i);
					res += "<ACTIVE>" + (md.active ? 1 : 0) + "</ACTIVE>";
					res += "<NAME>" + md.name + "</NAME>";
					res += "<NEW_VALUE>" + md.newValue + "</NEW_VALUE>";
					res += "</MODIFIER>";
					
				}
				
			}
			res += "</MO>";
			res += "<MT>";
			if(currentFilterMT != null){
				for(int i = 0; i<currentFilterMT.rule_mod_lst.size(); i++) if(currentFilterMT.rule_mod_lst.get(i).type == FilterDescriptorType.RULE){
					res += "<RULE>";
					rd = (RuleDescriptor)currentFilterMT.rule_mod_lst.get(i);
					res += "<ACTIVE>" + (rd.active ? 1 : 0) + "</ACTIVE>";
					res += "<POINTS>" + rd.points + "</POINTS>";
					res += "<EVAL_ITEM_1>" + rd.evalItem1 + "</EVAL_ITEM_1>";
					res += "<EVAL_ITEM_2>" + rd.evalItem2 + "</EVAL_ITEM_2>";
					res += "<OPERATOR>" + rd.evalOperator + "</OPERATOR>";
					res += "<EVAL_TRUE>" + rd.ruleEvalTrue + "</EVAL_TRUE>";
					res += "<EVAL_FALSE>" + rd.ruleEvalFalse + "</EVAL_FALSE>";
					res += "</RULE>";
					
				}else if(currentFilterMT.rule_mod_lst.get(i).type == FilterDescriptorType.MODIFIER){
					res += "<MODIFIER>";
					md = (ModifierDescriptor)currentFilterMT.rule_mod_lst.get(i);
					res += "<ACTIVE>" + (md.active ? 1 : 0) + "</ACTIVE>";
					res += "<NAME>" + md.name + "</NAME>";
					res += "<NEW_VALUE>" + md.newValue + "</NEW_VALUE>";
					res += "</MODIFIER>";
					
				}
				
			}
			res += "</MT>";
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		res += "</XML>";
		return res;
	}
	
	
	public static synchronized void filterActivate(String fname){
		if(Compiler.compile(fname)){
			try{
				File f = new File(fname);
				String line;
				BufferedReader in = new BufferedReader(new FileReader(f));
				while((line = in.readLine()) != null) current_filter_data += line + "\n";
				in.close();
				
	        	SmsFilterScriptLexer lex = new SmsFilterScriptLexer(new ANTLRFileStream(fname, "UTF8"));
	            CommonTokenStream tokens = new CommonTokenStream(lex);
	            SmsFilterScriptParser g = new SmsFilterScriptParser(tokens);
	            SmsFilterScriptParser.input_return ir = g.input();
	            Tree tree = (CommonTree)ir.getTree();

	            currentFilterMO = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.MO), FilterType.MO);
	            currentFilterMT = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.MT), FilterType.MT);
	            currentFilterSMPP_MO = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.SMPP_MO), FilterType.SMPP_MO);
	            currentFilterSMPP_MT = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.SMPP_MT), FilterType.SMPP_MT);
	            currentFilterHLR = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.HLR), FilterType.HLR);
	            
	            
	            logger.info("Filter [" + fname + "] activated!");
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		
	}
	public static String filterSave(String data){
		long ts = System.currentTimeMillis();
		if(Compiler.compile_str(data)){
			try{
				File f = new File("conf/filter." + ts + ".smsfs");
				PrintWriter out = new PrintWriter(f);
				out.println(data);
				out.close();
	            logger.info("Filter [ filter." + ts + ".smsfs ] saved!");
	            return "filter." + ts + ".smsfs";
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		return null;
		
	}
	public static synchronized void setSimulationFilter(String data){
		if(Compiler.compile_str(data)){
			try{
				simulation_filter_data = data;
		    	SmsFilterScriptLexer lex = new SmsFilterScriptLexer(new ANTLRInputStream(new ByteArrayInputStream(data.getBytes()), "UTF8"));
	            CommonTokenStream tokens = new CommonTokenStream(lex);
	            SmsFilterScriptParser g = new SmsFilterScriptParser(tokens);
	            SmsFilterScriptParser.input_return ir = g.input();
	            Tree tree = (CommonTree)ir.getTree();

	            simulationFilterMO = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.MO), FilterType.MO);
	            simulationFilterMT = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.MT), FilterType.MT);
	            simulationFilterHLR = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.HLR), FilterType.HLR);
	            simulationFilterSMPP_MO = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.SMPP_MO), FilterType.SMPP_MO);
	            simulationFilterSMPP_MT = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.SMPP_MT), FilterType.SMPP_MT);
	            
	            
	            logger.info("New simulation filter set!");
				
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
	}
	public static synchronized void filterActivate_str(String data){
		long ts = System.currentTimeMillis();
		if(Compiler.compile_str(data)){
			try{
				current_filter_data = data;
		    	SmsFilterScriptLexer lex = new SmsFilterScriptLexer(new ANTLRInputStream(new ByteArrayInputStream(data.getBytes()), "UTF8"));
	            CommonTokenStream tokens = new CommonTokenStream(lex);
	            SmsFilterScriptParser g = new SmsFilterScriptParser(tokens);
	            SmsFilterScriptParser.input_return ir = g.input();
	            Tree tree = (CommonTree)ir.getTree();

	            currentFilterMO = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.MO), FilterType.MO);
	            currentFilterMT = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.MT), FilterType.MT);
	            currentFilterHLR = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.HLR), FilterType.HLR);
	            
	            currentFilterSMPP_MO = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.SMPP_MO), FilterType.SMPP_MO);
	            currentFilterSMPP_MT = Smsfs.processFilter(Smsfs.getRules(tree, FilterType.SMPP_MT), FilterType.SMPP_MT);
	            
	            
	            logger.info("Filter [ filter." + ts + ".smsfs ] activated!");
			}catch(Exception e){
				e.printStackTrace();
				logger.error(e.getMessage());
			}
		}
		
	}
	
	public static void stopWorker(int id){
		logger.info("Stopping FilterWorker thread: [" + id + "]");
		workerLst.get(id).stop();
		
	}
}
