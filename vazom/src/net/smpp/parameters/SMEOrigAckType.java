package net.smpp.parameters;

import java.util.HashMap;

public enum SMEOrigAckType {
	NO_SME_ACK(0x00),
	SME_ACK(0x04),
	SME_MANUAL_USER_ACK(0x08),
	SME_BOTH(0x0c);
	
	private int id;
	private static final HashMap<Integer, SMEOrigAckType> lookup = new HashMap<Integer, SMEOrigAckType>();
	static{
		for(SMEOrigAckType td : SMEOrigAckType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static SMEOrigAckType get(int id){ return lookup.get(id); }
	private SMEOrigAckType(int _id){ id = _id; }	
}
