package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TargetMeasurementInformationType extends TargetMeasurementInformation{

	public TargetMeasurementInformationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 157;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
