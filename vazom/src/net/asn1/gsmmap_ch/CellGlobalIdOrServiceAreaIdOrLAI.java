package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CellGlobalIdOrServiceAreaIdOrLAI extends CHOICE{

	public CellGlobalIdOrServiceAreaIdFixedLength get_cellGlobalIdOrServiceAreaIdFixedLength(){
		return (CellGlobalIdOrServiceAreaIdFixedLength)elements.get(0).data;
	}
	public CellGlobalIdOrServiceAreaIdFixedLength new_cellGlobalIdOrServiceAreaIdFixedLength(){
		return new CellGlobalIdOrServiceAreaIdFixedLength();
	}

	public LAIFixedLength get_laiFixedLength(){
		return (LAIFixedLength)elements.get(1).data;
	}
	public LAIFixedLength new_laiFixedLength(){
		return new LAIFixedLength();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cellGlobalIdOrServiceAreaIdFixedLength(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_laiFixedLength(); }});
	}
	public CellGlobalIdOrServiceAreaIdOrLAI(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
