package net.isup.parameters;

import net.isup.ParameterType;

public class UserTeleserviceInformation extends ParameterBase {
	public UserTeleserviceInformation(){
		type = ParameterType.USER_TELESERVICE_INFORMATION;
	}
}
