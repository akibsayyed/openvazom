package net.isup.parameters;

import net.isup.ParameterType;

public class TransmissionMediumRequirement extends ParameterBase {
	public TransmissionMediumRequirement(){
		type = ParameterType.TRANSMISSION_MEDIUM_REQUIREMENT;
	}
}
