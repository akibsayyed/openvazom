package net.sgn;


import java.util.concurrent.ConcurrentLinkedQueue;

import net.config.ConfigManagerV2;
import net.config.SGNConfigData;
import net.ds.DSManager;
import net.ds.DataSourceType;
import net.hlr.HLRManager;
import net.hplmnr.HPLMNRManager;
import net.logging.LoggingManager;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.m3ua_conn.M3UAConnManager;
import net.mtp3_conn.MTP3ConnManager;
import net.routing.RoutingConnectionDirectionType;
import net.routing.RoutingManager;
import net.sccp.SubsystemNumber;
import net.sctp.SSctp;
import net.security.SecurityManager;
import net.sgc.SGCManager;
import net.sgc.SGC_SGDescriptor;
import net.sgn.cli.CLIService;
import net.sgn.cli.VSTP_CLIService;
import net.smpp2ss7.SMPP2SS7Manager;
import net.smpp_conn.SMPPConnManager;
import net.smpp_conn.SMPPService;
import net.smsfs.bindings.Bindings;
import net.spcap.SPcap;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.MessageDescriptor;
import net.vstp.VSTP_SGF_CorrelationManager;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class SGNode {
	private static Logger logger;
	private static Listener listener;
	private static Thread listener_t;
	private static boolean stopping;
	//private static ConcurrentHashMap<Integer, FNDescriptor> fn_conn_map;
	private static ConcurrentLinkedQueue<MessageDescriptor> out_queue;
	private static ConcurrentLinkedQueue<MessageDescriptor> in_queue;

	private static class Listener implements Runnable{
		FNDescriptor fnd = null;
		int s_sctp_id = -1;
		int c_sctp_id = -1;
		public void run() {
			logger.info("Starting...");
			s_sctp_id = SSctp.initServer(SGNConfigData.sgn_bind, SGNConfigData.sgn_port);
			if(s_sctp_id > -1){
				while(!stopping){
					c_sctp_id = SSctp.clientAccept(s_sctp_id);
					if(c_sctp_id > -1){
						fnd = new FNDescriptor(c_sctp_id);
						FNManager.addNode(c_sctp_id, fnd);
					}
				}
				
			}else logger.error("Error while initializing SCTP server connection for F nodes!");
			logger.info("Ending...");
			
		}
		
	}

	public static MessageDescriptor out_poll(){
		return out_queue.poll();
	}
	public static void out_offer(MessageDescriptor  md){
		if(FNManager.fn_connected){
			if(out_queue.size() < SGNConfigData.sgn_global_max_queue_size) out_queue.offer(md);
			else{
				StatsManager.SYSTEM_STATS.SGN_OUT_QUEUE_MAX++;
				//logger.warn("SGNode.out_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
			}
		}
	}
	public static void in_offer(MessageDescriptor  md){
		if(FNManager.fn_connected){
			if(in_queue.size() < SGNConfigData.sgn_global_max_queue_size) in_queue.offer(md);
			else{
				StatsManager.SYSTEM_STATS.SGN_IN_QUEUE_MAX++;
				//logger.warn("SGNode.in_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
			}
		}
	}
	public static MessageDescriptor in_poll(){
		return in_queue.poll();
	}
	
	
	private void initConfig(){
		try{
			// h248
			SGNConfigData.h248_status = (ConfigManagerV2.get("h248.status").equalsIgnoreCase("ON") ? true : false);
			
			// isup
			SGNConfigData.isup_status = (ConfigManagerV2.get("isup.status").equalsIgnoreCase("ON") ? true : false);
			
			// config
			SGNConfigData.sgn_id = ConfigManagerV2.get("sgn.id");
			SGNConfigData.sgn_debug = (ConfigManagerV2.get("sgn.debug").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.sgn_bind = ConfigManagerV2.get("sgn.bind");
			SGNConfigData.sgn_port = Integer.parseInt(ConfigManagerV2.get("sgn.port"));
			SGNConfigData.sgn_global_max_queue_size = Integer.parseInt(ConfigManagerV2.get("sgn.global.max.queue.size"));
			SGNConfigData.f_workers = Integer.parseInt(ConfigManagerV2.get("f.workers"));
			// spcap
			SGNConfigData.spcap_snaplen = Integer.parseInt(ConfigManagerV2.get("spcap.snaplen"));
			SGNConfigData.spcap_queue_max = Integer.parseInt(ConfigManagerV2.get("spcap.queue.max"));

			// smpp -> ss7
			SGNConfigData.smpp2ss7_error_code_sync_interval = Integer.parseInt(ConfigManagerV2.get("smpp2ss7.error_code.sync.interval"))*1000;
			SGNConfigData.smpp2ss7_status = (ConfigManagerV2.get("smpp2ss7.status").equalsIgnoreCase("ON") ? true : false);
			
			// smpp
			SGNConfigData.smpp_debug = (ConfigManagerV2.get("smpp.debug").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.smpp_check_inerval = Integer.parseInt(ConfigManagerV2.get("smpp.check.inerval"))*1000;
			SGNConfigData.smpp_timeout = Integer.parseInt(ConfigManagerV2.get("smpp.timeout"))*1000;
			SGNConfigData.smpp_proxy_client_port = Integer.parseInt(ConfigManagerV2.get("smpp.proxy.client.port"));
			SGNConfigData.smpp_proxy_status = (ConfigManagerV2.get("smpp.proxy.status").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.smpp_proxy_end_point = ConfigManagerV2.get("smpp.proxy.end_point");
			SGNConfigData.smpp_users_sync_status = (ConfigManagerV2.get("smpp.users.sync.status").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.smpp_users_sync_interval = Integer.parseInt(ConfigManagerV2.get("smpp.users.sync.interval"))*1000;
			
			
			// CMDI
			SGNConfigData.cmdi_port = Integer.parseInt(ConfigManagerV2.get("cmdi.port"));
			SGNConfigData.vstp_cmdi_port = Integer.parseInt(ConfigManagerV2.get("vstp.cmdi.port"));
			// m3ua
			SGNConfigData.m3ua_debug = (ConfigManagerV2.get("m3ua.debug").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.m3ua_reconnect_interval = Integer.parseInt(ConfigManagerV2.get("m3ua.reconnect.interval"));
			SGNConfigData.m3ua_daud_interval = Integer.parseInt(ConfigManagerV2.get("m3ua.daud.interval"))*1000;
			SGNConfigData.m3ua_reconnect_status = (ConfigManagerV2.get("m3ua.reconnect.status").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.m3ua_smsfs_hlr_callback_check_interval = Integer.parseInt(ConfigManagerV2.get("m3ua.smsfs.hlr.callback.check.interval"))*1000;
			SGNConfigData.m3ua_smsfs_hlr_callback_timeout = Integer.parseInt(ConfigManagerV2.get("m3ua.smsfs.hlr.callback.timeout"))*1000;

			// hlr
			SGNConfigData.hlr_timeout_check_interval = Integer.parseInt(ConfigManagerV2.get("hlr.timeout.check.interval"))*1000;
			SGNConfigData.hlr_timeout = Integer.parseInt(ConfigManagerV2.get("hlr.timeout"))*1000;
			
			// sgc
			SGNConfigData.sgc_bind = ConfigManagerV2.get("sgc.bind");
			SGNConfigData.sgc_port = Integer.parseInt(ConfigManagerV2.get("sgc.port"));
			SGNConfigData.sgc_reconnect_interval = Integer.parseInt(ConfigManagerV2.get("sgc.reconnect.interval")) * 1000;
			SGNConfigData.sgc_reconnect_status = (ConfigManagerV2.get("sgc.reconnect.status").equalsIgnoreCase("ON") ? true : false);
			// vstp
			SGNConfigData.vstp_sgf_crl_check_interval = Integer.parseInt(ConfigManagerV2.get("vstp.sgf.crl.check.interval")) * 1000;
			SGNConfigData.vstp_sgf_crl_timeout = Integer.parseInt(ConfigManagerV2.get("vstp.sgf.crl.timeout")) * 1000;
			
			
			// pds
			SGNConfigData.pds_status = (ConfigManagerV2.get("pds.status").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.pds_ip_frag_timeout = Integer.parseInt(ConfigManagerV2.get("pds.ip.frag.timeout")) * 1000;
			// HPLMNR
			SGNConfigData.hplmnr_timeout_check_interval = Integer.parseInt(ConfigManagerV2.get("hplmnr.timeout.check.interval"))*1000;
			SGNConfigData.hplmnr_timeout = Integer.parseInt(ConfigManagerV2.get("hplmnr.timeout"))*1000;
			SGNConfigData.hplmnr_status = (ConfigManagerV2.get("hplmnr.status").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.hplmnr_debug = (ConfigManagerV2.get("hplmnr.debug").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.hplmnr_hlr_gt_override_status = (ConfigManagerV2.get("hplmnr.hlr.gt.override.status").equalsIgnoreCase("ON") ? true : false);
			SGNConfigData.hplmnr_hlr_gt_override = ConfigManagerV2.get("hplmnr.hlr.gt.override");
			SGNConfigData.hplmnr_nnn_gt = ConfigManagerV2.get("hplmnr.nnn.gt");
			SGNConfigData.hplmnr_nnn_routable_gt = ConfigManagerV2.get("hplmnr.nnn.routable.gt");
			SGNConfigData.hplmnr_msc_gt = ConfigManagerV2.get("hplmnr.msc.gt");
			SGNConfigData.hplmnr_sgsn_gt = ConfigManagerV2.get("hplmnr.sgsn.gt");
			SGNConfigData.hplmnr_smsrouter_gt = ConfigManagerV2.get("hplmnr.smsrouter.gt");
			//SGNConfigData.hplmnr_m3ua_data_opc = Integer.parseInt(ConfigManagerV2.get("hplmnr.m3ua.data.opc"));
			//SGNConfigData.hplmnr_mss_dpc_m3ua_data_dpc = Integer.parseInt(ConfigManagerV2.get("hplmnr.mss_dpc.m3ua.data.dpc"));
			//SGNConfigData.hplmnr_hlr_dpc_m3ua_data_dpc = Integer.parseInt(ConfigManagerV2.get("hplmnr.hlr_dpc.m3ua.data.dpc"));
			//SGNConfigData.hplmnr_m3ua_data_si = ServiceIndicatorType.valueOf(ConfigManagerV2.get("hplmnr.m3ua.data.si"));
			SGNConfigData.hplmnr_m3ua_data_ni = Integer.parseInt(ConfigManagerV2.get("hplmnr.m3ua.data.ni"));
			SGNConfigData.hplmnr_m3ua_data_mp = Integer.parseInt(ConfigManagerV2.get("hplmnr.m3ua.data.mp"));
			SGNConfigData.hplmnr_m3ua_data_sls = Integer.parseInt(ConfigManagerV2.get("hplmnr.m3ua.data.sls"));
			SGNConfigData.hplmnr_m3ua_data_nap = Integer.parseInt(ConfigManagerV2.get("hplmnr.m3ua.data.nap"));
			SGNConfigData.hplmnr_sccp_called_gt_translationtype = Integer.parseInt(ConfigManagerV2.get("hplmnr.sccp.called.gt.translationtype"));
			SGNConfigData.hplmnr_sccp_calling_gt_translationtype = Integer.parseInt(ConfigManagerV2.get("hplmnr.sccp.calling.gt.translationtype"));
			SGNConfigData.hplmnr_sri_gt = ConfigManagerV2.get("hplmnr.sri.gt");
			SGNConfigData.hplmnr_mo_gt = ConfigManagerV2.get("hplmnr.mo.gt");
			SGNConfigData.hplmnr_mt_gt = ConfigManagerV2.get("hplmnr.mt.gt");
			SGNConfigData.hplmnr_sccp_called_ssn = SubsystemNumber.valueOf(ConfigManagerV2.get("hplmnr.sccp.called.ssn"));
			SGNConfigData.hplmnr_sccp_calling_ssn = SubsystemNumber.valueOf(ConfigManagerV2.get("hplmnr.sccp.calling.ssn"));
			SGNConfigData.hplmnr_mo_sccp_called_ssn = SubsystemNumber.valueOf(ConfigManagerV2.get("hplmnr.mo.sccp.called.ssn"));
			SGNConfigData.hplmnr_mo_sccp_calling_ssn = SubsystemNumber.valueOf(ConfigManagerV2.get("hplmnr.mo.sccp.calling.ssn"));
			SGNConfigData.hplmnr_mo_smsc = ConfigManagerV2.get("hplmnr.mo.smsc");
			SGNConfigData.hplmnr_mo_smsrouter_gt = ConfigManagerV2.get("hplmnr.mo.smsrouter.gt");
			SGNConfigData.hplmnr_imsi_correlation_mcc = Integer.parseInt(ConfigManagerV2.get("hplmnr.imsi.correlation.mcc"));
			SGNConfigData.hplmnr_imsi_correlation_mnc = Integer.parseInt(ConfigManagerV2.get("hplmnr.imsi.correlation.mnc"));
			//SGNConfigData.hplmnr_tcap_dialogue_appctx_oid = ConfigManagerV2.get("hplmnr.tcap.dialogue.appctx.oid");

		}catch(Exception e){
			e.printStackTrace();
			logger.error("Invalid configuration file...");
			System.exit(0);
		}

		
	}
	public SGNode(){
		String tmp;
		String[] tmp_lst;
		String[] tmp_lst2;
		String tmp_name;
		SGC_SGDescriptor sgcd = null;
		
		logger.info("Starting SG Node...");
		// logging
		LoggingManager.init();

		// security
		/*
		SecurityManager.init();
		SecurityManager.set_critical_feature(SecurityManager.FEATURE_SGN);
		if(!SecurityManager.check_feature(SecurityManager.FEATURE_SGN)){
			LoggingManager.error(logger, "Invalid license, shutting down!");
			System.exit(0);
			
		}
		*/
		
		ConfigManagerV2.init("conf/sgn.properties");
		logger.info("Loadig config file...");
		
		initConfig();
		
		
		//fn_conn_map = new ConcurrentHashMap<Integer, FNDescriptor>();
		in_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		
		
		// connection listener thread
		listener = new Listener();
		listener_t = new Thread(listener, "FN_LISTENER");
		listener_t.start();
		
		// FN Manager
		FNManager.init();
		
		// F workers
		logger.info("Number of F workers = [" + SGNConfigData.f_workers + "]!");
		for(int i = 0; i<SGNConfigData.f_workers; i++) new FWorker(i);
		// Stats manager
		StatsManager.init();
		
		// VSTP-SRI correlation
		VSTP_SGF_CorrelationManager.init();
		
		// CLI
		CLIService.init(SGNConfigData.cmdi_port);
		// VSTP CLI
		VSTP_CLIService.init(SGNConfigData.vstp_cmdi_port);
		
		
		// SPCAP
		SPcap.init();

		// DS
		DSManager.init();
		
		
		// SGC
		SGCManager.init();
		// SGC nodes
		tmp = ConfigManagerV2.get("sgc.nodes");
		logger.info("SGC Nodes: [" + tmp + "]!");
		tmp_lst = tmp.split(",");
		for(int i = 0; i<tmp_lst.length; i++) if(tmp_lst[i].trim().length() > 0){
			tmp_name = tmp_lst[i].trim();
			sgcd = new SGC_SGDescriptor(tmp_name, 
										ConfigManagerV2.get("sgc.nodes." + tmp_name + ".address"), 
										Integer.parseInt(ConfigManagerV2.get("sgc.nodes." + tmp_name + ".port")));
			SGCManager.addNode_client(tmp_name, sgcd);
			
			
		}	
		// Feture check for FEATURE_M3UA_CONN
		//if(SecurityManager.check_feature(SecurityManager.FEATURE_M3UA_CONN)){
			// M3UA Conn Manager
			M3UAConnManager.init();
			// m3ua connections
			tmp = ConfigManagerV2.get("m3ua.connections");
			logger.info("M3UA Connections: [" + tmp + "]!");
			tmp_lst = tmp.split(",");
			for(int i = 0; i<tmp_lst.length; i++) if(tmp_lst[i].trim().length() > 0){
				tmp_name = tmp_lst[i].trim();
				// default connection
				if(tmp_name.endsWith("*")){
					tmp_name = tmp_name.substring(0, tmp_name.length()-1);
					M3UAConnManager.default_conn = tmp_name;
					logger.info("M3UA Default Connection: [" + tmp_name + "]!");
				}
				logger.info("M3UA Connection [" + tmp_name + "] parameters: [" + ConfigManagerV2.get("m3ua." + tmp_name) + "]!");
				tmp_lst2 = ConfigManagerV2.get("m3ua." + tmp_name).split(":");
				// init m3ua connection
				if(tmp_lst2.length == 12){
					M3UAConnManager.addConnection(	tmp_name, 
													tmp_name, 
													tmp_lst2[0].trim(),
													Integer.parseInt(tmp_lst2[1].trim()), 
													tmp_lst2[2].trim(),
													Integer.parseInt(tmp_lst2[3].trim()), 
													Integer.parseInt(tmp_lst2[4].trim()),
													Integer.parseInt(tmp_lst2[5].trim()), 
													Integer.parseInt(tmp_lst2[6].trim()), 
													Integer.parseInt(tmp_lst2[7].trim()), 
													Integer.parseInt(tmp_lst2[8].trim()),
													Integer.parseInt(tmp_lst2[9].trim())*1000,
													Integer.parseInt(tmp_lst2[10].trim()),
													RoutingConnectionDirectionType.valueOf(tmp_lst2[11].toUpperCase().trim()));
				}else LoggingManager.error(logger, "M3UAConnManager.addConnection: missing parameters!");
			}
			// check if default set
			if(M3UAConnManager.default_conn == null){
				if(tmp_lst.length > 0){
					// if default not set, make fist connection defaut one
					M3UAConnManager.default_conn = tmp_lst[0];
					
				}
			}
			
		//}

		// Feture check for FEATURE_MTP3_CONN
		//if(SecurityManager.check_feature(SecurityManager.FEATURE_MTP3_CONN)){
			// E1 Manager
			MTP3ConnManager.init();
		
		//}

		// Feture check for FEATURE_SMPP_CONN
		//if(SecurityManager.check_feature(SecurityManager.FEATURE_SMPP_CONN)){
			// SMPP
			SMPPConnManager.init();
			if(SGNConfigData.smpp_users_sync_status) SMPPConnManager.init_smpp_user_sync_thread();

			// SMPP end point connections
			tmp = ConfigManagerV2.get("smpp.connections");
			logger.info("SMPP Connections: [" + tmp + "]!");
			tmp_lst = tmp.split(",");
			for(int i = 0; i<tmp_lst.length; i++) if(tmp_lst[i].trim().length() > 0){
				tmp_name = tmp_lst[i].trim();
				logger.info("SMPP Connection [" + tmp_name + "] parameters: [" + ConfigManagerV2.get("smpp." + tmp_name) + "]!");
				tmp_lst2 = ConfigManagerV2.get("smpp." + tmp_name).split(":");
				RoutingConnectionDirectionType smpp_conn_type = RoutingConnectionDirectionType.valueOf(tmp_lst2[0].toUpperCase().trim());
				if(smpp_conn_type != null){
					switch(smpp_conn_type){
						case CLIENT:
							if(tmp_lst2.length == 6){
								SMPPConnManager.addConnection(	
										tmp_name, 
										tmp_lst2[1], 
										Utils.safeStr2int(tmp_lst2[2]), 
										tmp_lst2[3], 
										tmp_lst2[4],
										Utils.safeStr2int(tmp_lst2[5])*1000,
										smpp_conn_type);
								
							}else logger.warn("Missing parameters for SMPP Connection [" + tmp_name + "], type = [" + smpp_conn_type + "]!");

							break;
						case SERVER:
							if(tmp_lst2.length == 3){
								SMPPConnManager.addConnection(	
										tmp_name, 
										tmp_lst2[1], 
										Utils.safeStr2int(tmp_lst2[2]), 
										null, 
										null,
										0,
										smpp_conn_type);
								
							}else logger.warn("Missing parameters for SMPP Connection [" + tmp_name + "], type = [" + smpp_conn_type + "]!");						
							break;
					}
				}
				
				
			}
		
		//}	

		// Feture check for FEATURE_SMPP_PROXY
		//if(SecurityManager.check_feature(SecurityManager.FEATURE_SMPP_PROXY)){
			// SMPP proxy
			if(SGNConfigData.smpp_proxy_status){
				SMPPService.init(SGNConfigData.smpp_proxy_client_port);
			}

		//}else SGNConfigData.smpp_proxy_status = false;
		
		
		
		// Routing Manager
		// M3UA
		RoutingManager.init();
		if(M3UAConnManager.conn_map != null){
			for(String s : M3UAConnManager.conn_map.keySet()){
				logger.info("RoutingManager: adding connection: [" + s + "], type: [" +  M3UAConnManager.conn_map.get(s).type + "]!");
				RoutingManager.add(s, M3UAConnManager.conn_map.get(s));
			}
			
		}
		// E1
		if(MTP3ConnManager.conn_map != null){
			for(String s : MTP3ConnManager.conn_map.keySet()){
				logger.info("RoutingManager: adding connection: [" + s + "], type: [" +  MTP3ConnManager.conn_map.get(s).type + "]!");
				RoutingManager.add(s, MTP3ConnManager.conn_map.get(s));
				
			}
			
		}
		// SMPP
		if(SMPPConnManager.conn_map != null){
			for(String s : SMPPConnManager.conn_map.keySet()){
				logger.info("RoutingManager: adding connection: [" + s + "], type: [" +  SMPPConnManager.conn_map.get(s).type + "]!");
				RoutingManager.add(s, SMPPConnManager.conn_map.get(s));
			}
		}
		
		// Feture check for FEATURE_SRI
		//if(SecurityManager.check_feature(SecurityManager.FEATURE_SRI)){
			// SRI-for-SM
			HPLMNRManager.init();
		
		//}		
		
		// HLR manager
		HLRManager.init(SGNConfigData.hlr_timeout_check_interval, SGNConfigData.hlr_timeout);

		// SMPP->SS7 manager
		SMPP2SS7Manager.init();
		if(SGNConfigData.smpp2ss7_status) SMPP2SS7Manager.init_ec_sync_thread();
		
		// SMSFS Bindigs - needed for conversions
		Bindings.init();
	}
	public static void main(String[] args) {
		PropertyConfigurator.configure(SGNode.class.getResource("/log4j.sgn.properties"));
		logger = Logger.getLogger(SGNode.class);
		new SGNode();
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				try{ Thread.sleep(5000); }catch(Exception e){e.printStackTrace();}
				logger.info("Shutdown complete...");
			}
		});
	}

}
