package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class H221NonStandard extends SEQUENCE{

	public INTEGER get_t35CountryCode1(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_t35CountryCode1(){
		return new INTEGER();
	}

	public INTEGER get_t35CountryCode2(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_t35CountryCode2(){
		return new INTEGER();
	}

	public INTEGER get_t35Extension(){
		return (INTEGER)elements.get(2).data;
	}
	public INTEGER new_t35Extension(){
		return new INTEGER();
	}

	public INTEGER get_manufacturerCode(){
		return (INTEGER)elements.get(3).data;
	}
	public INTEGER new_manufacturerCode(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_t35CountryCode1(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_t35CountryCode2(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_t35Extension(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_manufacturerCode(); }});
	}
	public H221NonStandard(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
