package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UnreliableCallData extends SET{

	public ControlNetworkID get_controlNetworkID(){
		return (ControlNetworkID)elements.get(0).data;
	}
	public ControlNetworkID new_controlNetworkID(){
		return new ControlNetworkID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(1).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(307, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_controlNetworkID(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
	}
	public UnreliableCallData(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
