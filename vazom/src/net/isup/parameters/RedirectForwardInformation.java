package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectForwardInformation extends ParameterBase {
	public RedirectForwardInformation(){
		type = ParameterType.REDIRECT_FORWARD_INFORMATION;
	}
}
