package net.asn1.ansi_tcap;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ComponentSequence extends SEQUENCE{

	public ComponentPDU new_child(){
		return new ComponentPDU();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ComponentPDU getChild(int index){
		return (ComponentPDU)of_children.get(index);
	}
	public ComponentSequence(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 8;
		of_children = new ArrayList<ASNType>();
		asn_class = ASNTagClass.PRIVATE;
	}
}
