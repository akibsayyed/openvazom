package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class OriginalComponentIdentifier extends CHOICE{

	public OperationCode get_operationCode(){
		return (OperationCode)elements.get(0).data;
	}
	public OperationCode new_operationCode(){
		return new OperationCode();
	}

	public ErrorCode get_errorCode(){
		return (ErrorCode)elements.get(1).data;
	}
	public ErrorCode new_errorCode(){
		return new ErrorCode();
	}

	public NULL get_userInfo(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_userInfo(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_operationCode(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_errorCode(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_userInfo(); }});
	}
	public OriginalComponentIdentifier(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
