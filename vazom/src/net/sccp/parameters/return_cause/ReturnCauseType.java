package net.sccp.parameters.return_cause;

import java.util.HashMap;

public enum ReturnCauseType {
	NO_TRANSLATION_FOR_ADDRESS_OF_SUCH_NATURE(0x00),
	NO_TRANSLATION_FOR_THIS_SPECIFIC_ADDRESS(0x01),
	SUBSYSTEM_CONGESTION(0x02),
	SUBSYSTEM_FAILURE(0x03),
	UNEQUIPPED_USER(0x04),
	MTP_FAILURE(0x05),
	NETWORK_CONGESTION(0x06),
	UNQUALIFIED(0x07),
	ERROR_IN_MESSAGE_TRANSPORT(0x08),
	ERROR_IN_LOCAL_PROCESSING(0x09),
	DESTINATION_CANNOT_PERFORM_REASSEMBLY(0x0A),
	SCCP_FAILURE(0x0B),
	HOP_COUNTER_VIOLATION(0x0C),
	SEGMENTATION_NOT_SUPPORTED(0x0D),
	SEGMENTATION_FAILURE(0x0E);
	
	
	
	
	// 1 byte
	private int id;
	private static final HashMap<Integer, ReturnCauseType> lookup = new HashMap<Integer, ReturnCauseType>();
	static{
		for(ReturnCauseType td : ReturnCauseType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static ReturnCauseType get(int id){ return lookup.get(id); }
	private ReturnCauseType(int _id){ id = _id; }
	
}
