package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemPositionRequestRes extends SET{

	public PositionResult get_positionResult(){
		return (PositionResult)elements.get(0).data;
	}
	public PositionResult new_positionResult(){
		return new PositionResult();
	}

	public LCSBillingID get_lcsBillingID(){
		return (LCSBillingID)elements.get(1).data;
	}
	public LCSBillingID new_lcsBillingID(){
		return new LCSBillingID();
	}

	public MobilePositionCapability get_mobilePositionCapability(){
		return (MobilePositionCapability)elements.get(2).data;
	}
	public MobilePositionCapability new_mobilePositionCapability(){
		return new MobilePositionCapability();
	}

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(3).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public DTXIndication get_dtxIndication(){
		return (DTXIndication)elements.get(4).data;
	}
	public DTXIndication new_dtxIndication(){
		return new DTXIndication();
	}

	public ReceivedSignalQuality get_receivedSignalQuality(){
		return (ReceivedSignalQuality)elements.get(5).data;
	}
	public ReceivedSignalQuality new_receivedSignalQuality(){
		return new ReceivedSignalQuality();
	}

	public CDMAChannelData get_cdmaChannelData(){
		return (CDMAChannelData)elements.get(6).data;
	}
	public CDMAChannelData new_cdmaChannelData(){
		return new CDMAChannelData();
	}

	public CDMACodeChannel get_cdmaCodeChannel(){
		return (CDMACodeChannel)elements.get(7).data;
	}
	public CDMACodeChannel new_cdmaCodeChannel(){
		return new CDMACodeChannel();
	}

	public CDMAMobileCapabilities get_cdmaMobileCapabilities(){
		return (CDMAMobileCapabilities)elements.get(8).data;
	}
	public CDMAMobileCapabilities new_cdmaMobileCapabilities(){
		return new CDMAMobileCapabilities();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(9).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServingOneWayDelay2 get_cdmaServingOneWayDelay2(){
		return (CDMAServingOneWayDelay2)elements.get(10).data;
	}
	public CDMAServingOneWayDelay2 new_cdmaServingOneWayDelay2(){
		return new CDMAServingOneWayDelay2();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(11).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public CDMATargetMAHOList get_cdmaTargetMAHOList(){
		return (CDMATargetMAHOList)elements.get(12).data;
	}
	public CDMATargetMAHOList new_cdmaTargetMAHOList(){
		return new CDMATargetMAHOList();
	}

	public CDMAPSMMList get_cdmaPSMMList(){
		return (CDMAPSMMList)elements.get(13).data;
	}
	public CDMAPSMMList new_cdmaPSMMList(){
		return new CDMAPSMMList();
	}

	public NAMPSChannelData get_nampsChannelData(){
		return (NAMPSChannelData)elements.get(14).data;
	}
	public NAMPSChannelData new_nampsChannelData(){
		return new NAMPSChannelData();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(15).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TargetMeasurementList get_targetMeasurementList(){
		return (TargetMeasurementList)elements.get(16).data;
	}
	public TargetMeasurementList new_targetMeasurementList(){
		return new TargetMeasurementList();
	}

	public TDMA_MAHO_CELLID get_tdma_MAHO_CELLID(){
		return (TDMA_MAHO_CELLID)elements.get(17).data;
	}
	public TDMA_MAHO_CELLID new_tdma_MAHO_CELLID(){
		return new TDMA_MAHO_CELLID();
	}

	public TDMA_MAHO_CHANNEL get_tdma_MAHO_CHANNEL(){
		return (TDMA_MAHO_CHANNEL)elements.get(18).data;
	}
	public TDMA_MAHO_CHANNEL new_tdma_MAHO_CHANNEL(){
		return new TDMA_MAHO_CHANNEL();
	}

	public TDMA_TimeAlignment get_tdma_TimeAlignment(){
		return (TDMA_TimeAlignment)elements.get(19).data;
	}
	public TDMA_TimeAlignment new_tdma_TimeAlignment(){
		return new TDMA_TimeAlignment();
	}

	public TDMAVoiceMode get_tdmaVoiceMode(){
		return (TDMAVoiceMode)elements.get(20).data;
	}
	public TDMAVoiceMode new_tdmaVoiceMode(){
		return new TDMAVoiceMode();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(21).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(22).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public PositionInformation get_positionInformation(){
		return (PositionInformation)elements.get(23).data;
	}
	public PositionInformation new_positionInformation(){
		return new PositionInformation();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(24).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(338, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_positionResult(); }});
		elements.add(new ElementDescriptor(367, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcsBillingID(); }});
		elements.add(new ElementDescriptor(335, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobilePositionCapability(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(329, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dtxIndication(); }});
		elements.add(new ElementDescriptor(72, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_receivedSignalQuality(); }});
		elements.add(new ElementDescriptor(63, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaChannelData(); }});
		elements.add(new ElementDescriptor(68, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaCodeChannel(); }});
		elements.add(new ElementDescriptor(330, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileCapabilities(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(347, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServingOneWayDelay2(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(136, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMAHOList(); }});
		elements.add(new ElementDescriptor(346, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPSMMList(); }});
		elements.add(new ElementDescriptor(76, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsChannelData(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(157, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetMeasurementList(); }});
		elements.add(new ElementDescriptor(359, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_MAHO_CELLID(); }});
		elements.add(new ElementDescriptor(360, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_MAHO_CHANNEL(); }});
		elements.add(new ElementDescriptor(362, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdma_TimeAlignment(); }});
		elements.add(new ElementDescriptor(223, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaVoiceMode(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(336, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_positionInformation(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
	}
	public InterSystemPositionRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
