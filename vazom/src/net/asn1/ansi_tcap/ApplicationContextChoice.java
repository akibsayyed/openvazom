package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ApplicationContextChoice extends CHOICE{

	public IntegerApplicationContext get_integerApplicationId(){
		return (IntegerApplicationContext)elements.get(0).data;
	}
	public IntegerApplicationContext new_integerApplicationId(){
		return new IntegerApplicationContext();
	}

	public ObjectIDApplicationContext get_objectApplicationId(){
		return (ObjectIDApplicationContext)elements.get(1).data;
	}
	public ObjectIDApplicationContext new_objectApplicationId(){
		return new ObjectIDApplicationContext();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(27, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_integerApplicationId(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_objectApplicationId(); }});
	}
	public ApplicationContextChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
