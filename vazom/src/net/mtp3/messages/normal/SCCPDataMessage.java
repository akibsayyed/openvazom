package net.mtp3.messages.normal;

public class SCCPDataMessage extends NormalMessageBase {
	public byte[] data;
	
	public SCCPDataMessage(){
		super();
	}
	
	public byte[] encode(){
		return data;
	
	}
	
	public void init(byte[] _data) {
		super.init(_data);
		data = _data;
		
	}

}
