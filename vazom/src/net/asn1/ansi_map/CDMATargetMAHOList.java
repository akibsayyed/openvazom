package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMATargetMAHOList extends SEQUENCE{

	public CDMATargetMAHOInformationType new_child(){
		return new CDMATargetMAHOInformationType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMATargetMAHOInformationType getChild(int index){
		return (CDMATargetMAHOInformationType)of_children.get(index);
	}
	public CDMATargetMAHOList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
