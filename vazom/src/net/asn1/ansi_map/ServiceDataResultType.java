package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceDataResultType extends ServiceDataResult{

	public ServiceDataResultType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 272;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
