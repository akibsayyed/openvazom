package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionResultChoice extends CHOICE{

	public ErrorDescriptor get_transactionError(){
		return (ErrorDescriptor)elements.get(0).data;
	}
	public ErrorDescriptor new_transactionError(){
		return new ErrorDescriptor();
	}

	public ActionRepliesSequenceOf get_actionReplies(){
		return (ActionRepliesSequenceOf)elements.get(1).data;
	}
	public ActionRepliesSequenceOf new_actionReplies(){
		return new ActionRepliesSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionError(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_actionReplies(); }});
	}
	public TransactionResultChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
