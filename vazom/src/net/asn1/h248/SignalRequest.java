package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SignalRequest extends CHOICE{

	public Signal get_signal(){
		return (Signal)elements.get(0).data;
	}
	public Signal new_signal(){
		return new Signal();
	}

	public SeqSigList get_seqSigList(){
		return (SeqSigList)elements.get(1).data;
	}
	public SeqSigList new_seqSigList(){
		return new SeqSigList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signal(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_seqSigList(); }});
	}
	public SignalRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
