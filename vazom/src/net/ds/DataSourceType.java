package net.ds;

public enum DataSourceType {
	E1,
	SCTP,
	SPCAP_SCTP,
	SPCAP_SMPP,
	SPCAP_E1,
	SMPP,
	NA; // not available
}
