package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CommandReplySequenceOf extends SEQUENCE{

	public CommandReply new_child(){
		return new CommandReply();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CommandReply getChild(int index){
		return (CommandReply)of_children.get(index);
	}
	public CommandReplySequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
