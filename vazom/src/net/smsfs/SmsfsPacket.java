package net.smsfs;

import java.util.ArrayList;

public class SmsfsPacket {
	public FilterMode filterMode;
	public ModeOfOperation packetMode;
	public ArrayList<Object> bindings;
	public byte[] packet;
	public int totalScore = 0;
	public FilterType filterType;
	public FilterDescriptor filterDescriptor;
	public ArrayList<Object> extra_data;
	public boolean hlr_request_executed;
	public String last_hlr_req_id;

	
	public SmsfsPacket(){
		bindings = new ArrayList<Object>();
		extra_data = new ArrayList<Object>();
		// default filter mode
		filterMode = FilterMode.NORMAL;
	}
	
}
