package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Relation extends ENUMERATED{

	public static final int _greaterThan = 0;
	public static final int _smallerThan = 1;
	public static final int _unequalTo = 2;
	public Relation(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
