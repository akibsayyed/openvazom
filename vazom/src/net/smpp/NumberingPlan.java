package net.smpp;

import java.util.HashMap;

public enum NumberingPlan {
	UNKNOWN(0x00),
	ISDN_TELEPHONE(0x01),
	DATA_X121(0x03),
	TELEX(0x04),
	LAND_MOBILE(0x06),
	NATIONAL(0x08),
	PRIVATE(0x09),
	ERMES(0x0A),
	INTERNET_IP(0x0E),
	WAP_CLIENT_ID(0x12);
	
	private int id;
	private static final HashMap<Integer, NumberingPlan> lookup = new HashMap<Integer, NumberingPlan>();
	static{
		for(NumberingPlan td : NumberingPlan.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static NumberingPlan get(int id){
		NumberingPlan res = lookup.get(id);
		if(res == null) res = UNKNOWN;
		return res; 
	}
	private NumberingPlan(int _id){ id = _id; }

}
