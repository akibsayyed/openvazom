package net.isup.parameters.fci;

import java.util.HashMap;

public enum ISDNUserParRefIndicatorType {
	PREFFERED_ALL_THE_WAY(0x00),
	NOT_REQUIRED_ALL_THE_WAY(0x40),
	REQUIRED_ALL_THE_WAY(0x80);
	
	
	private int id;
	private static final HashMap<Integer, ISDNUserParRefIndicatorType> lookup = new HashMap<Integer, ISDNUserParRefIndicatorType>();
	static{
		for(ISDNUserParRefIndicatorType td :ISDNUserParRefIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ISDNUserParRefIndicatorType get(int id){ return lookup.get(id); }
	private ISDNUserParRefIndicatorType(int _id){ id = _id; }		
}
