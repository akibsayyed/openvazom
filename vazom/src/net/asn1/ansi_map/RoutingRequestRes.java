package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RoutingRequestRes extends SET{

	public MSCID get_mscid(){
		return (MSCID)elements.get(0).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(1).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(2).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(3).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public ConditionallyDeniedReason get_conditionallyDeniedReason(){
		return (ConditionallyDeniedReason)elements.get(4).data;
	}
	public ConditionallyDeniedReason new_conditionallyDeniedReason(){
		return new ConditionallyDeniedReason();
	}

	public Digits get_digits_Destination(){
		return (Digits)elements.get(5).data;
	}
	public Digits new_digits_Destination(){
		return new Digits();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(6).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(7).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(8).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(162, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_conditionallyDeniedReason(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits_Destination(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
	}
	public RoutingRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
