package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationRequest extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public SystemAccessType get_systemAccessType(){
		return (SystemAccessType)elements.get(3).data;
	}
	public SystemAccessType new_systemAccessType(){
		return new SystemAccessType();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(4).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public AuthenticationData get_authenticationData(){
		return (AuthenticationData)elements.get(5).data;
	}
	public AuthenticationData new_authenticationData(){
		return new AuthenticationData();
	}

	public AuthenticationResponse get_authenticationResponse(){
		return (AuthenticationResponse)elements.get(6).data;
	}
	public AuthenticationResponse new_authenticationResponse(){
		return new AuthenticationResponse();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(7).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CDMANetworkIdentification get_cdmaNetworkIdentification(){
		return (CDMANetworkIdentification)elements.get(8).data;
	}
	public CDMANetworkIdentification new_cdmaNetworkIdentification(){
		return new CDMANetworkIdentification();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(9).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public ControlChannelMode get_controlChannelMode(){
		return (ControlChannelMode)elements.get(10).data;
	}
	public ControlChannelMode new_controlChannelMode(){
		return new ControlChannelMode();
	}

	public Digits get_digits(){
		return (Digits)elements.get(11).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(12).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public RandomVariable get_randomVariable(){
		return (RandomVariable)elements.get(13).data;
	}
	public RandomVariable new_randomVariable(){
		return new RandomVariable();
	}

	public ServiceRedirectionCause get_serviceRedirectionCause(){
		return (ServiceRedirectionCause)elements.get(14).data;
	}
	public ServiceRedirectionCause new_serviceRedirectionCause(){
		return new ServiceRedirectionCause();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(15).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SuspiciousAccess get_suspiciousAccess(){
		return (SuspiciousAccess)elements.get(16).data;
	}
	public SuspiciousAccess new_suspiciousAccess(){
		return new SuspiciousAccess();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(17).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(18).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public MEID get_meid(){
		return (MEID)elements.get(19).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(34, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemAccessType(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemCapabilities(); }});
		elements.add(new ElementDescriptor(161, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationData(); }});
		elements.add(new ElementDescriptor(35, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_authenticationResponse(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(232, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaNetworkIdentification(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(199, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelMode(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(40, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariable(); }});
		elements.add(new ElementDescriptor(237, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceRedirectionCause(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(285, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_suspiciousAccess(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public AuthenticationRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
