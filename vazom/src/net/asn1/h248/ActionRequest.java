package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ActionRequest extends SEQUENCE{

	public ContextID get_contextId(){
		return (ContextID)elements.get(0).data;
	}
	public ContextID new_contextId(){
		return new ContextID();
	}

	public ContextRequest get_contextRequest(){
		return (ContextRequest)elements.get(1).data;
	}
	public ContextRequest new_contextRequest(){
		return new ContextRequest();
	}

	public ContextAttrAuditRequest get_contextAttrAuditReq(){
		return (ContextAttrAuditRequest)elements.get(2).data;
	}
	public ContextAttrAuditRequest new_contextAttrAuditReq(){
		return new ContextAttrAuditRequest();
	}

	public CommandRequestSequenceOf get_commandRequests(){
		return (CommandRequestSequenceOf)elements.get(3).data;
	}
	public CommandRequestSequenceOf new_commandRequests(){
		return new CommandRequestSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_contextId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_contextRequest(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_contextAttrAuditReq(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_commandRequests(); }});
	}
	public ActionRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
