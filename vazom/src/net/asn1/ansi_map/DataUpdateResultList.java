package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DataUpdateResultList extends SEQUENCE{

	public DataUpdateResult new_child(){
		return new DataUpdateResult();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public DataUpdateResult getChild(int index){
		return (DataUpdateResult)of_children.get(index);
	}
	public DataUpdateResultList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
