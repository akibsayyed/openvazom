package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RegulatedEmbeddedDescriptor extends SEQUENCE{

	public SecondEventsDescriptor get_secondEvent(){
		return (SecondEventsDescriptor)elements.get(0).data;
	}
	public SecondEventsDescriptor new_secondEvent(){
		return new SecondEventsDescriptor();
	}

	public SignalsDescriptor get_signalsDescriptor(){
		return (SignalsDescriptor)elements.get(1).data;
	}
	public SignalsDescriptor new_signalsDescriptor(){
		return new SignalsDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_secondEvent(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalsDescriptor(); }});
	}
	public RegulatedEmbeddedDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
