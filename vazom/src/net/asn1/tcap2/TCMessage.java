package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TCMessage extends CHOICE{

	public Unidirectional get_unidirectional(){
		return (Unidirectional)elements.get(0).data;
	}
	public Unidirectional new_unidirectional(){
		return new Unidirectional();
	}

	public Begin get_begin(){
		return (Begin)elements.get(1).data;
	}
	public Begin new_begin(){
		return new Begin();
	}

	public End get_end(){
		return (End)elements.get(2).data;
	}
	public End new_end(){
		return new End();
	}

	public Continue get_continue(){
		return (Continue)elements.get(3).data;
	}
	public Continue new_continue(){
		return new Continue();
	}

	public Abort get_abort(){
		return (Abort)elements.get(4).data;
	}
	public Abort new_abort(){
		return new Abort();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_unidirectional(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_begin(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_end(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_continue(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_abort(); }});
	}
	public TCMessage(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
