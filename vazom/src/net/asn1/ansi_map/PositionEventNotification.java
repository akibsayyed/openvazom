package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PositionEventNotification extends SET{

	public PositionResult get_positionResult(){
		return (PositionResult)elements.get(0).data;
	}
	public PositionResult new_positionResult(){
		return new PositionResult();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(1).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LCSBillingID get_lcsBillingID(){
		return (LCSBillingID)elements.get(2).data;
	}
	public LCSBillingID new_lcsBillingID(){
		return new LCSBillingID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(3).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(338, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_positionResult(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(367, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_lcsBillingID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
	}
	public PositionEventNotification(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
