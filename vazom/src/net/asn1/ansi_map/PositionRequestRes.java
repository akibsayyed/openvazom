package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PositionRequestRes extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public ExtendedMSCID get_extendedMSCID(){
		return (ExtendedMSCID)elements.get(1).data;
	}
	public ExtendedMSCID new_extendedMSCID(){
		return new ExtendedMSCID();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(2).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(3).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSID get_msid(){
		return (MSID)elements.get(4).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MSStatus get_mSStatus(){
		return (MSStatus)elements.get(5).data;
	}
	public MSStatus new_mSStatus(){
		return new MSStatus();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(6).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public PSID_RSIDInformation get_pSID_RSIDInformation(){
		return (PSID_RSIDInformation)elements.get(7).data;
	}
	public PSID_RSIDInformation new_pSID_RSIDInformation(){
		return new PSID_RSIDInformation();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(8).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(9).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(53, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedMSCID(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(313, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSStatus(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(202, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pSID_RSIDInformation(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
	}
	public PositionRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
