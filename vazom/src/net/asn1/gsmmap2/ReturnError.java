package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReturnError extends SEQUENCE{

	public InvokeIdType get_invokeID(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_invokeID(){
		return new InvokeIdType();
	}

	public MAP_ERROR get_errorCode(){
		return (MAP_ERROR)elements.get(1).data;
	}
	public MAP_ERROR new_errorCode(){
		return new MAP_ERROR();
	}

	public ReturnErrorParameter get_parameter(){
		return (ReturnErrorParameter)elements.get(2).data;
	}
	public ReturnErrorParameter new_parameter(){
		return new ReturnErrorParameter();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeID(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_errorCode(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_parameter(); }});
	}
	public ReturnError(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
