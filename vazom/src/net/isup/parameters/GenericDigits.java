package net.isup.parameters;

import java.util.Arrays;

import net.isup.ParameterType;
import net.isup.parameters.gd.EncodingSchemeType;
import net.isup.parameters.gd.TypeOfDigitsType;

public class GenericDigits extends ParameterBase {
	public EncodingSchemeType encoding_scheme;
	public TypeOfDigitsType type_of_digits;
	public byte[] digits;
	
	
	public GenericDigits(){
		type = ParameterType.GENERIC_DIGITS;
	}

	public void init(byte[] data) {
		if(data != null){
			encoding_scheme = EncodingSchemeType.get(data[byte_position] & 0xe0);
			type_of_digits = TypeOfDigitsType.get(data[byte_position] & 0x1f);
			byte_position++;
			digits = Arrays.copyOfRange(data, byte_position, data.length);
		}
	}

}
