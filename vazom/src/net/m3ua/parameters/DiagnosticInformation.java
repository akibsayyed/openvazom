package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class DiagnosticInformation extends M3UAParameter {

	public DiagnosticInformation(){
		type = M3UAParameterType.DIAGNOSTIC_INFO;
	}

	public void init(byte[] data) {
		value = data;

	}

}
