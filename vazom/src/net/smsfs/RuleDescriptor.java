package net.smsfs;

import java.util.ArrayList;

public class RuleDescriptor extends FilterDescriptorBase{
	public int points;
	public boolean active;
	public boolean isDummy;
	public String evalItem1 = "";
	public String evalItem1Regex;
	public boolean item1_is_regex;
	
	public boolean item1_is_method;
	public ArrayList<String> item_1_method_params;
	public boolean item2_is_method;
	public ArrayList<String> item_2_method_params;
	
	
	
	public String evalOperator;
	public String evalItem2 = "";
	public String evalItem2Regex;
	public boolean item2_is_regex;
	
	public RuleAction ruleEvalFalse;
	public String ruleEvalFalseGotoLabel = "";
	public ArrayList<String> ruleEvalFalse_connection;
	
	public RuleAction ruleEvalTrue;
	public String ruleEvalTrueGotoLabel = "";
	public ArrayList<String> ruleEvalTrue_connection;
	
	public String label = "";
	public RuleDescriptor(){
		type = FilterDescriptorType.RULE;
		ruleEvalFalse_connection = new ArrayList<String>();
		ruleEvalTrue_connection = new ArrayList<String>();
	}
}
