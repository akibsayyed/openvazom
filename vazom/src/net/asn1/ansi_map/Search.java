package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Search extends SET{

	public DatabaseKey get_databaseKey(){
		return (DatabaseKey)elements.get(0).data;
	}
	public DatabaseKey new_databaseKey(){
		return new DatabaseKey();
	}

	public ServiceDataAccessElementList get_serviceDataAccessElementList(){
		return (ServiceDataAccessElementList)elements.get(1).data;
	}
	public ServiceDataAccessElementList new_serviceDataAccessElementList(){
		return new ServiceDataAccessElementList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(252, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_databaseKey(); }});
		elements.add(new ElementDescriptor(271, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceDataAccessElementList(); }});
	}
	public Search(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
