package net.hlr;

import net.m3ua.M3UAPacket;

public class HLRPacket {
	public String gt_called;
	public String gt_calling;
	public long tcap_id;
	public String imsi;
	public String msisdn;
	public String sca;
	public String nnn;
	public String annn;
	public byte[] packet;
	public M3UAPacket m3ua_packet;
	
	public Object mutex;
	public long ts;
}
