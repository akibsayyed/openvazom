package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuditReply extends CHOICE{

	public TerminationIDList get_contextAuditResult(){
		return (TerminationIDList)elements.get(0).data;
	}
	public TerminationIDList new_contextAuditResult(){
		return new TerminationIDList();
	}

	public ErrorDescriptor get_error(){
		return (ErrorDescriptor)elements.get(1).data;
	}
	public ErrorDescriptor new_error(){
		return new ErrorDescriptor();
	}

	public AuditResult get_auditResult(){
		return (AuditResult)elements.get(2).data;
	}
	public AuditResult new_auditResult(){
		return new AuditResult();
	}

	public TermListAuditResult get_auditResultTermList(){
		return (TermListAuditResult)elements.get(3).data;
	}
	public TermListAuditResult new_auditResultTermList(){
		return new TermListAuditResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_contextAuditResult(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_error(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditResult(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, true){public void set(){ data = new_auditResultTermList(); }});
	}
	public AuditReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
