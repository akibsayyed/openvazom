package net.ds;

public class DSDescriptor {
	public String id;
	public DataSourceType type;
	public DSBase data_source;
	public String[] bpf;
	public int workers;
	public String[] if_lst;
	public int[] spcap_id;
}
