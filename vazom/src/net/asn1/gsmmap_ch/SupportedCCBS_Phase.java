package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SupportedCCBS_Phase extends INTEGER{

	public SupportedCCBS_Phase(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
