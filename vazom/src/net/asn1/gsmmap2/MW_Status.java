package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MW_Status extends BIT_STRING{

	public static final int _sc_AddressNotIncluded = 0;
	public static final int _mnrf_Set = 1;
	public static final int _mcef_Set = 2;
	public static final int _mnrg_Set = 3;
	public MW_Status(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
