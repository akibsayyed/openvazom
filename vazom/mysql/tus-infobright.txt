import
=======
load data infile '/tmp/sms.sql' into table sms fields terminated by ',' escaped by '\\' enclosed by '"'

export
=======
select 
  s.id, 
  s.direction_id, 
  s.type_id, 
  s.mode_id, 
  s.pdu_id,
  s.filter_action_id, 
  s.sms_status_id,
  s.filter_total_score,
  s.ip_source,
  s.ip_destination,
  s.tcp_source,
  s.tcp_destination,
  s.system_id, 
  s.gt_called, 
  conv(substr(md5(s.gt_called), 1, 14), 16, 10),
  s.gt_calling, 
  conv(substr(md5(s.gt_calling), 1, 14), 16, 10),
  s.scda, 
  conv(substr(md5(s.scda), 1, 14), 16, 10),
  s.scoa, 
  conv(substr(md5(s.scoa), 1, 14), 16, 10),
  s.imsi, 
  conv(substr(md5(s.imsi), 1, 14), 16, 10),
  s.msisdn, 
  conv(substr(md5(s.msisdn), 1, 14), 16, 10),
  s.sms_destination, 
  conv(substr(md5(s.sms_destination), 1, 14), 16, 10),
  s.sms_originating, 
  conv(substr(md5(s.sms_originating), 1, 14), 16, 10),
  s.sms_text, 
  conv(substr(md5(s.sms_text), 1, 14), 16, 10),
  s.timestamp, 
  s.sms_text_enc_id, 
  s.sms_destination_enc_id, 
  s.sms_originating_enc_id, 
  s.m3ua_data_dpc, 
  s.m3ua_data_opc, 
  s.tcap_sid, 
  s.tcap_did,
  s.sms_conc_partnum,
  s.sms_conc_parts,
  s.sms_conc_msgid,
  s.error_code  
into outfile '/tmp/sms.sql' fields terminated by ',' escaped by '\\' enclosed by '"' 
from sms s 




If doing select into outfile in IB, do this first:
set @bh_dataformat = 'txt_variable';
