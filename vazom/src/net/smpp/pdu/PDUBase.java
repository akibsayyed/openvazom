package net.smpp.pdu;

import java.util.ArrayList;
import java.util.Arrays;

import net.smpp.ErrorCodeType;
import net.smpp.OptionalParameterType;
import net.smpp.PDUType;
import net.smpp.parameters.optional.Additional_status_info_text;
import net.smpp.parameters.optional.Alert_on_message_delivery;
import net.smpp.parameters.optional.Callback_num;
import net.smpp.parameters.optional.Callback_num_atag;
import net.smpp.parameters.optional.Callback_num_pres_ind;
import net.smpp.parameters.optional.Delivery_failure_reason;
import net.smpp.parameters.optional.Dest_addr_subunit;
import net.smpp.parameters.optional.Dest_bearer_type;
import net.smpp.parameters.optional.Dest_network_type;
import net.smpp.parameters.optional.Dest_subaddress;
import net.smpp.parameters.optional.Dest_telematics_id;
import net.smpp.parameters.optional.Destination_port;
import net.smpp.parameters.optional.Display_time;
import net.smpp.parameters.optional.Dpf_result;
import net.smpp.parameters.optional.Its_reply_type;
import net.smpp.parameters.optional.Its_session_info;
import net.smpp.parameters.optional.Language_indicator;
import net.smpp.parameters.optional.Message_payload;
import net.smpp.parameters.optional.Message_state;
import net.smpp.parameters.optional.More_messages_to_send;
import net.smpp.parameters.optional.Ms_availability_status;
import net.smpp.parameters.optional.Ms_msg_wait_facilities;
import net.smpp.parameters.optional.Ms_validity;
import net.smpp.parameters.optional.Network_error_code;
import net.smpp.parameters.optional.Number_of_messages;
import net.smpp.parameters.optional.OptionalParameterBase;
import net.smpp.parameters.optional.Payload_type;
import net.smpp.parameters.optional.Privacy_indicator;
import net.smpp.parameters.optional.Qos_time_to_live;
import net.smpp.parameters.optional.Receipted_message_id;
import net.smpp.parameters.optional.Sar_msg_ref_num;
import net.smpp.parameters.optional.Sar_segment_seqnum;
import net.smpp.parameters.optional.Sar_total_segments;
import net.smpp.parameters.optional.Sc_interface_version;
import net.smpp.parameters.optional.Set_dpf;
import net.smpp.parameters.optional.Sms_signal;
import net.smpp.parameters.optional.Source_addr_subunit;
import net.smpp.parameters.optional.Source_bearer_type;
import net.smpp.parameters.optional.Source_network_type;
import net.smpp.parameters.optional.Source_port;
import net.smpp.parameters.optional.Source_subaddress;
import net.smpp.parameters.optional.Source_telematics_id;
import net.smpp.parameters.optional.User_message_reference;
import net.smpp.parameters.optional.User_response_code;
import net.smpp.parameters.optional.Ussd_service_op;
import net.utils.Utils;

public abstract class PDUBase {
	public long command_length;
	public long command_id;
	public ErrorCodeType command_status;
	public long sequence_number;
	public PDUType type;
	protected int byte_pos = 0;
	
	// optional parameters
	public ArrayList<OptionalParameterBase> optional_params;
	
	public OptionalParameterBase getParameter(OptionalParameterType type){
		if(optional_params != null){
			for(int i = 0; i<optional_params.size(); i++) if(optional_params.get(i).type == type) return optional_params.get(i);
		}
		return null;
		
	}
	protected void initOptional(byte[] data){
		optional_params = new ArrayList<OptionalParameterBase>();
		OptionalParameterBase param = null;
		OptionalParameterType param_type = null;
		int tag;
		int l;
		while(byte_pos < data.length){
			tag = ((data[byte_pos] & 0xff) << 8) + (data[byte_pos + 1] & 0xff);
			byte_pos += 2;
			l = ((data[byte_pos] & 0xff) << 8) + (data[byte_pos + 1] & 0xff);
			byte_pos += 2;
			param_type = OptionalParameterType.get(tag);
			if(param_type != null){
				switch(param_type){
					case ADDITIONAL_STATUS_INFO_TEXT: param = new Additional_status_info_text(); break;
					case ALERT_ON_MESSAGE_DELIVERY: param = new Alert_on_message_delivery(); break;
					case CALLBACK_NUM: param = new Callback_num(); break;
					case CALLBACK_NUM_ATAG: param = new Callback_num_atag(); break;
					case CALLBACK_NUM_PRES_IND: param = new Callback_num_pres_ind(); break;
					case DELIVERY_FAILURE_REASON: param = new Delivery_failure_reason(); break;
					case DEST_ADDR_SUBUNIT: param = new Dest_addr_subunit(); break;
					case DEST_BEARER_TYPE: param = new Dest_bearer_type(); break;
					case DEST_NETWORK_TYPE: param = new Dest_network_type(); break;
					case DEST_SUBADDRESS: param = new Dest_subaddress(); break;
					case DEST_TELEMATICS_ID: param = new Dest_telematics_id(); break;
					case DESTINATION_PORT: param = new Destination_port(); break;
					case DISPLAY_TIME: param = new Display_time(); break;
					case DPF_RESULT: param = new Dpf_result(); break;
					case ITS_REPLY_TYPE: param = new Its_reply_type(); break;
					case ITS_SESSION_INFO: param = new Its_session_info(); break;
					case LANGUAGE_INDICATOR: param = new Language_indicator(); break;
					case MESSAGE_PAYLOAD: param = new Message_payload(); break;
					case MESSAGE_STATE: param = new Message_state(); break;
					case MORE_MESSAGES_TO_SEND: param = new More_messages_to_send(); break;
					case MS_AVAILABILITY_STATUS: param = new Ms_availability_status(); break;
					case MS_MSG_WAIT_FACILITIES: param = new Ms_msg_wait_facilities(); break;
					case MS_VALIDITY: param = new Ms_validity(); break;
					case NETWORK_ERROR_CODE: param = new Network_error_code(); break;
					case NUMBER_OF_MESSAGES: param = new Number_of_messages(); break;
					case PAYLOAD_TYPE: param = new Payload_type(); break;
					case PRIVACY_INDICATOR: param = new Privacy_indicator(); break;
					case QOS_TIME_TO_LIVE: param = new Qos_time_to_live(); break;
					case RECEIPTED_MESSAGE_ID: param = new Receipted_message_id(); break;
					case SAR_MSG_REF_NUM: param = new Sar_msg_ref_num(); break;
					case SAR_SEGMENT_SEQNUM: param = new Sar_segment_seqnum(); break;
					case SAR_TOTAL_SEGMENTS: param = new Sar_total_segments(); break;
					case SC_INTERFACE_VERSION: param = new Sc_interface_version(); break;
					case SET_DPF: param = new Set_dpf(); break;
					case SMS_SIGNAL: param = new Sms_signal(); break;
					case SOURCE_ADDR_SUBUNIT: param = new Source_addr_subunit(); break;
					case SOURCE_BEARER_TYPE: param = new Source_bearer_type(); break;
					case SOURCE_NETWORK_TYPE: param = new Source_network_type(); break;
					case SOURCE_PORT: param = new Source_port(); break;
					case SOURCE_SUBADDRESS: param = new Source_subaddress(); break;
					case SOURCE_TELEMATICS_ID: param = new Source_telematics_id(); break;
					case USER_MESSAGE_REFERENCE: param = new User_message_reference(); break;
					case USER_RESPONSE_CODE: param = new User_response_code(); break;
					case USSD_SERVICE_OP: param = new Ussd_service_op(); break;
				}
				if(param != null){
					param.init(Arrays.copyOfRange(data, byte_pos, byte_pos + l));
					optional_params.add(param);
				}
			}
			byte_pos += l;
		}
		
	}
	public byte[] encode(){
		command_length = 16;
		ArrayList<Byte> buff = new ArrayList<Byte>();
		// command length calculated later
		buff.add((byte)(0));
		buff.add((byte)(0));
		buff.add((byte)(0));
		buff.add((byte)(0));
		// type
		buff.add((byte)(type.getId() >> 24));
		buff.add((byte)(type.getId() >> 16));
		buff.add((byte)(type.getId() >> 8));
		buff.add((byte)(type.getId()));
		// status
		if(command_status != null){
			buff.add((byte)(command_status.getId() >> 24));
			buff.add((byte)(command_status.getId() >> 16));
			buff.add((byte)(command_status.getId() >> 8));
			buff.add((byte)(command_status.getId()));
		}else{
			buff.add((byte)(0));
			buff.add((byte)(0));
			buff.add((byte)(0));
			buff.add((byte)(0));
			
		}
		// sequence
		buff.add((byte)(sequence_number >> 24));
		buff.add((byte)(sequence_number >> 16));
		buff.add((byte)(sequence_number >> 8));
		buff.add((byte)(sequence_number));
		// system id
		return Utils.list2array(buff);
	}
	public abstract void init(byte[] _data);


}
