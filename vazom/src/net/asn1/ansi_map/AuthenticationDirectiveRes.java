package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationDirectiveRes extends SET{

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(0).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
	}
	public AuthenticationDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
