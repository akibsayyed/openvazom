package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Sar_segment_seqnum extends OptionalParameterBase {
	public Sar_segment_seqnum(){
		type = OptionalParameterType.SAR_SEGMENT_SEQNUM;
	}
	public int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
