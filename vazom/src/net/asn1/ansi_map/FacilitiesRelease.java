package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FacilitiesRelease extends SET{

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(0).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public ReleaseReason get_releaseReason(){
		return (ReleaseReason)elements.get(1).data;
	}
	public ReleaseReason new_releaseReason(){
		return new ReleaseReason();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(2).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(3).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_releaseReason(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
	}
	public FacilitiesRelease(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
