package net.isup.parameters.eci;

import java.util.HashMap;

public enum OutgoingECDeviceRequestIndicatorType {
	
	NO_INFORMATION(0x00),
	ACTIVATION_REQUEST(0x10),
	DEACTIVATION_REQUEST(0x20);
	
	
	private int id;
	private static final HashMap<Integer, OutgoingECDeviceRequestIndicatorType> lookup = new HashMap<Integer, OutgoingECDeviceRequestIndicatorType>();
	static{
		for(OutgoingECDeviceRequestIndicatorType td :OutgoingECDeviceRequestIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static OutgoingECDeviceRequestIndicatorType get(int id){ return lookup.get(id); }
	private OutgoingECDeviceRequestIndicatorType(int _id){ id = _id; }		
}
