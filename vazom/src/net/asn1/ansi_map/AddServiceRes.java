package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AddServiceRes extends SET{

	public CDMAConnectionReferenceList get_cdmaConnectionReferenceList(){
		return (CDMAConnectionReferenceList)elements.get(0).data;
	}
	public CDMAConnectionReferenceList new_cdmaConnectionReferenceList(){
		return new CDMAConnectionReferenceList();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(1).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public QoSPriority get_qosPriority(){
		return (QoSPriority)elements.get(2).data;
	}
	public QoSPriority new_qosPriority(){
		return new QoSPriority();
	}

	public ReasonList get_reasonList(){
		return (ReasonList)elements.get(3).data;
	}
	public ReasonList new_reasonList(){
		return new ReasonList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(212, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceList(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(348, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_qosPriority(); }});
		elements.add(new ElementDescriptor(218, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reasonList(); }});
	}
	public AddServiceRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
