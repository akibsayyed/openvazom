package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.ResetCause;
import net.sccp.parameters.Slr;

public class RSR_ResetRequest extends MessageBase {
	public Dlr destinationLocalReference;
	public Slr sourceLocalReference;
	public ResetCause resetCause;
	
	public RSR_ResetRequest(){
		super();
		type = MessageType.RSR_RESET_REQUEST;
		pointers = new Pointer[1];
	}
	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		sourceLocalReference = new Slr();
		sourceLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		resetCause = new ResetCause();
		resetCause.init(new byte[]{_data[byte_pos++]});
		
		// pointer, reserved for optional parameters in the future
		pointers[0] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		

	}

}
