package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudStreamDescriptor extends SEQUENCE{

	public StreamID get_streamID(){
		return (StreamID)elements.get(0).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public IndAudStreamParms get_streamParms(){
		return (IndAudStreamParms)elements.get(1).data;
	}
	public IndAudStreamParms new_streamParms(){
		return new IndAudStreamParms();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_streamID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_streamParms(); }});
	}
	public IndAudStreamDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
