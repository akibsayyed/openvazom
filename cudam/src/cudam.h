// CUDA manager v1.0
#include <pthread.h>
#include <map>
#include <deque>
#include <cuda_runtime_api.h>
using namespace std;

// list types, used with LDPacket and DataPacket
#define LD_REPETITION		0
#define LD_SPAM 		1
#define MD5_QUARANTINE_LST	2
#define MD5_NORMAL_LST	 	3
#define ALL_LISTS		9999
// action types
#define ACT_DELETE	10
#define ACT_NEW		11
#define ACT_COMPARE	12
#define ACT_RESET	13
// callback types
#define CB_LD		100
#define CB_MD5		200

// LD Packet sent to CUDA
// JNI callback executed when computation
// is complete
struct LDPacket{
	int lst_type;
	char *data;
	int length;
	// callback method, called on LD complete
	void *callback_args;
	void (*callback_method)(void *);
	// buffer on host for results returned by GPU
	char *result;
	int result_size;

};
// Data packet
// used for sending new message data
// to cuda device
struct DataPacket{
	int lst_type;
	char *data;
	int length;
	// action
	int action;
	int action_param;
};
// MD5 packet
// SMS message white list
// - contains md5 hash from sms parameters
struct MD5Packet{
	char *hash;
	//action
	int action;
	int action_param;
	int result_size;
	int lst_type;
	char *result;
	// callback method, called on MD5 kernel complete
        void *callback_args;
        void (*callback_method)(void *);
};


// Kernel statistics
struct KernelStats{
	float last_execution_time;
	float max_execution_time;
	unsigned long long execution_count;
	unsigned long long update_q_size;
	unsigned long long ld_q_size;
	unsigned long long md5_q_size;

};
// CUDA Grid Descriptor
struct GridDescriptor{
	dim3 blocks;
	dim3 threads;
};
//
// GPU Descriptor for each CUDA device present
// - queue for LD requests
// - each one in its own pthread
// - unique id
struct GPUDescriptor{
	// GPU id
	int id;
	// pthread used for current GPU context
	pthread_t pth;
	pthread_attr_t pth_attr;
	// LD queue
	deque<LDPacket*> *ld_queue;
	// data_lst update queue
	deque<DataPacket*> *update_queue;
	// md5 queue
	deque<MD5Packet*> *md5_queue;
	// locks
	pthread_mutex_t ld_lock;
	pthread_mutex_t update_lock;
	pthread_mutex_t md5_lock;
	// actual data on cuda device
	// max data list item size
	int *device_max_i_size;
	// max item size variation
	int *device_max_i_variation;
	// repetition list
	char *rep_device_data_lst;
	int *rep_device_current_size;
	int rep_current_size;
	int rep_current_pos;
	// known spam list
	char *spam_device_data_lst;
	int *spam_device_current_size;
	int spam_current_size;
	int spam_current_pos;
	// md5 quarantine list
	char *md5_quarantine_device_data_lst;
	int *md5_quarantine_device_current_size;
	int md5_quarantine_current_size;
	int md5_quarantine_current_pos;
	// md5 normal list
        char *md5_normal_device_data_lst;
        int *md5_normal_device_current_size;
        int md5_normal_current_size;
        int md5_normal_current_pos;	
	// buffer for kernel result on cuda device
	char *device_rep_result;
	char *device_spam_result;
	char *device_md5_q_result;
	char *device_md5_result;
	// buffer for current message on cuda device
	char *device_current_msg;
	int *device_current_msg_length;
	char *device_current_md5;
	// counters on host
	cudaEvent_t timer_start, timer_stop;
	pthread_mutex_t stats_lock;
	unsigned long long execution_count;
	float last_execution_time;
	float max_execution_time;
	// needle search
	char *device_needle;
	int *device_needle_size;
	int *device_max_needle_size;
	int *device_needle_length_size;


};

class CUDAM{
private:
	// private vars
	static bool stopping;
	//static pthread_mutex_t gpu_map_lock;
	static map<int, GPUDescriptor*> gpu_map;
	static map<int, GridDescriptor*> grid_map;
	static pthread_mutex_t gpu_map_lock;
	//static int last_used_device;
	static map<int, GPUDescriptor*>::iterator last_used_device_it;
	static int device_count;
	static void doShutdown(int device);	
	static void shutdownComplete();
	// private methods
	static int getActiveDevice();
	static void *gpu_ld_thread(void *args);
	static void push_update_device(int device, DataPacket *dp);
	static void push_ld_device(int device, LDPacket *ldp);
	//static void push_md5_device(int device, MD5Packet *md5p);
	static void push_md5_compare_device(int device, MD5Packet *md5p);
	//static void removeFromLst(int device, int lst_id, int item_id);
	static void remove_from_lst(int device, DataPacket *dp);
	static void reset(int device, int lst_id);
	static GridDescriptor* getGrid(int lst_id);
	static int get_device_lst_item(int device, int lst_id, int item_id, char *result, int *result_size);

public:
	//vars
        static int max_rep_lst_size;
        static int max_spam_lst_size;
	static int max_md5_quarantine_lst_size;
	static int max_md5_normal_lst_size;
	static int max_q_size;
        static int max_i_size;
	static int max_i_variation;
	// methods
	static int getDeviceCount();
	static void init(       int _max_rep_lst_size,
        	                int _max_spam_lst_size,
                	        int _max_md5_quarantine_lst_size,
                        	int _max_md5_normal_lst_size,          
                        	int _max_i_size,
				int _max_i_variation,
                       		int _max_q_size);

	static void initGrid(int lst_id, int blocks_w, int blocks_h, int threads);
	static void initAllDevices();
	static void init_device(int device);
	static void shutdown();
	static int getDeviceInfo(int dev, cudaDeviceProp *res);
	static void push_update_host(int device, DataPacket *dp);
	static void push_ld_host(int device, LDPacket *ldp);
	static void push_md5_host(int device, MD5Packet *md5p); 
	static int getFreeDevice();
	static void getKernelStats(int device, KernelStats *stats);
};
