package net.mtp2.messages;

public class MSU extends MessageBase {
	public byte[] data;
	
	public MSU(){
		type = MessageType.MSU;
	}

	public void init(byte[] _data) {
		data = _data;
	}
}
