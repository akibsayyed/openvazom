package net.vstp;

import java.util.ArrayList;

import net.ds.DataSourceType;
import net.utils.Utils;

public class HeaderDescriptor {
	public LocationType source;
	public String source_id = "";
	public LocationType destination;
	public String destination_id = "";
	public DataSourceType ds;
	public MessageType msg_type;
	public String msg_id;
	
	public byte[] encode(){
		byte[] res = null;
		try{
			ArrayList<Byte> tmp = new ArrayList<Byte>();
			Utils.bytesToLst(tmp, source.toString().getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)':'});
			Utils.bytesToLst(tmp, source_id.getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)':'});
			Utils.bytesToLst(tmp, ds.toString().getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)':'});
			Utils.bytesToLst(tmp, destination.toString().getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)':'});
			Utils.bytesToLst(tmp, destination_id.getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)':'});
			Utils.bytesToLst(tmp, msg_type.toString().getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)':'});
			Utils.bytesToLst(tmp, msg_id.getBytes());
			Utils.bytesToLst(tmp, new byte[]{(byte)'\n'});
			// result
			res = new byte[tmp.size()];
			for(int i = 0; i<tmp.size(); i++) res[i] = tmp.get(i);
		}catch(Exception e){
			e.printStackTrace();
		}
		return res;
	}
}
