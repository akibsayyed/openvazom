package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class IntegerApplicationContext extends INTEGER{

	public IntegerApplicationContext(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 27;
		asn_class = ASNTagClass.PRIVATE;
	}
}
