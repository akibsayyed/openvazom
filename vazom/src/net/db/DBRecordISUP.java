package net.db;

public class DBRecordISUP extends DBRecordBase {

	public long ringing_duration;
	public long call_duration;
	//public long ts;
	public String called_party;
	public Integer called_party_nai;
	public String calling_party;
	public Integer calling_party_nai;
	public int cic;
	public int dpc;
	public int opc;
	public int release_cause_class;
	public int release_cause;
	
	public DBRecordISUP(){
		recordType = DBRecordType.ISUP;
	}
}
