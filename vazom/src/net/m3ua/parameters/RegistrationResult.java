package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class RegistrationResult extends M3UAParameter {
	
	public RegistrationResult(){
		type = M3UAParameterType.REGISTRATION_RESULT;
	}

	public void init(byte[] data) {
		value = data;
	}



}
