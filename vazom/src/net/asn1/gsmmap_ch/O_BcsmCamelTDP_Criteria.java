package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class O_BcsmCamelTDP_Criteria extends SEQUENCE{

	public O_BcsmTriggerDetectionPoint get_o_BcsmTriggerDetectionPoint(){
		return (O_BcsmTriggerDetectionPoint)elements.get(0).data;
	}
	public O_BcsmTriggerDetectionPoint new_o_BcsmTriggerDetectionPoint(){
		return new O_BcsmTriggerDetectionPoint();
	}

	public DestinationNumberCriteria get_destinationNumberCriteria(){
		return (DestinationNumberCriteria)elements.get(1).data;
	}
	public DestinationNumberCriteria new_destinationNumberCriteria(){
		return new DestinationNumberCriteria();
	}

	public BasicServiceCriteria get_basicServiceCriteria(){
		return (BasicServiceCriteria)elements.get(2).data;
	}
	public BasicServiceCriteria new_basicServiceCriteria(){
		return new BasicServiceCriteria();
	}

	public CallTypeCriteria get_callTypeCriteria(){
		return (CallTypeCriteria)elements.get(3).data;
	}
	public CallTypeCriteria new_callTypeCriteria(){
		return new CallTypeCriteria();
	}

	public O_CauseValueCriteria get_o_CauseValueCriteria(){
		return (O_CauseValueCriteria)elements.get(4).data;
	}
	public O_CauseValueCriteria new_o_CauseValueCriteria(){
		return new O_CauseValueCriteria();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(5).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_o_BcsmTriggerDetectionPoint(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationNumberCriteria(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_basicServiceCriteria(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callTypeCriteria(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_o_CauseValueCriteria(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_extensionContainer(); }});
	}
	public O_BcsmCamelTDP_Criteria(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
