package net.smsfs;
import java.util.ArrayList;


public class FilterDescriptor {
	public FilterType filterType;
	//public ArrayList<RuleDescriptor> ruleLst;
	//public ArrayList<ModifierDescriptor> modifierLst;
	public ArrayList<FilterDescriptorBase> rule_mod_lst;
	
	public FilterDescriptor(){
		//ruleLst = new ArrayList<RuleDescriptor>();
		//modifierLst = new ArrayList<ModifierDescriptor>();
		rule_mod_lst = new ArrayList<FilterDescriptorBase>();
	}
}
