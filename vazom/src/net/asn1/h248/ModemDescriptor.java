package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ModemDescriptor extends SEQUENCE{

	public ModemTypeSequenceOf get_mtl(){
		return (ModemTypeSequenceOf)elements.get(0).data;
	}
	public ModemTypeSequenceOf new_mtl(){
		return new ModemTypeSequenceOf();
	}

	public PropertyParmSequenceOf get_mpl(){
		return (PropertyParmSequenceOf)elements.get(1).data;
	}
	public PropertyParmSequenceOf new_mpl(){
		return new PropertyParmSequenceOf();
	}

	public NonStandardData get_nonStandardData(){
		return (NonStandardData)elements.get(2).data;
	}
	public NonStandardData new_nonStandardData(){
		return new NonStandardData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mtl(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mpl(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nonStandardData(); }});
	}
	public ModemDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
