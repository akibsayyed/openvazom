package net.test;

import java.io.File;
import java.io.FileInputStream;

import net.mtp3.MTP3;
import net.mtp3.MTP3Packet;
import net.utils.Utils;

public class test11 {
	public static void main(String[] args) {
		File f = new File("/tmp/mtp3.raw");
		byte[] data = new byte[(int)f.length()];
		int i = 0;
		try{
			FileInputStream fi = new FileInputStream(f);
			int a;
			while( (a = fi.read()) != -1){
				data[i++] = (byte)a;
			}
			MTP3Packet mtp3 = MTP3.decode(data);
			//System.out.println(mtp3.serviceInformation.serviceIndicator);
			//System.out.println(mtp3.message.type);
			
			byte[] enc = mtp3.encode();
			for(int j = 0; j<enc.length; j++){
				System.out.println(String.format("%02x", enc[j]));
			}
			
			//System.out.println("SLS: " + mtp3.routingLabel.signallingLinkSelector);
			//System.out.println("OPC: " + mtp3.routingLabel.originatingPointCode);
			//System.out.println("DPC: " + mtp3.routingLabel.destinationPointCode);
		}catch(Exception e){
			e.printStackTrace();
		}

		
	}

}
