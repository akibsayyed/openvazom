package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class GSN_Address extends OCTET_STRING{

	public GSN_Address(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
