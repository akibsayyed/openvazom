package net.ds.pds;

import net.config.SGNConfigData;
import net.ds.DSBase;
import net.ds.DSPacket;
import net.ds.DataSourceType;
import net.ds.smpp.SMPPCorrelationManager;
import net.ds.smpp.SMPPCorrelationPacket;
import net.ds.smpp.SMPPSequenceDescriptor;
import net.ds.smpp.SMPPSmsInfo;
import net.sgn.SGNode;
import net.smpp.OptionalParameterType;
import net.smpp.SMPP_PDU;
import net.smpp.parameters.optional.OptionalParameterBase;
import net.smpp.parameters.optional.Sar_msg_ref_num;
import net.smpp.parameters.optional.Sar_segment_seqnum;
import net.smpp.parameters.optional.Sar_total_segments;
import net.smpp.pdu.Bind_receiver;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Bind_transmitter;
import net.smpp.pdu.Deliver_sm;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_multi;
import net.smpp.pdu.Submit_sm;
import net.smstpdu.SmsType;
import net.smstpdu.tpdcs.general.Alphabet;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class PDS_DecoderWorker_SMPP extends PDSWorkerBase{
	private static Logger logger=Logger.getLogger(PDS_DecoderWorker_SMPP.class);

	
	
	private SMPPCorrelationPacket initCorrelation(DSPacket dsp){
		SMPPCorrelationPacket smppp = new SMPPCorrelationPacket();
		smppp.ip_destination = dsp.ip_destination;
		smppp.ip_source = dsp.ip_source;
		smppp.tcp_destination = dsp.tcp_destination;
		smppp.tcp_source = dsp.tcp_source;
		SMPPCorrelationManager.add(smppp);
		return smppp;

		
	}
	
	private void processSMS_RESP(SMPPSmsInfo smsInfo, PDUBase pdu){
		MessageDescriptor vstp_md = new MessageDescriptor();
		try{
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = MessageType.SMPP_RESP;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			
			// ip
			vstp_md.values.put(VSTPDataItemType.IP_SOURCE.getId(), smsInfo.ip_destination);
			vstp_md.values.put(VSTPDataItemType.IP_DESTINATION.getId(), smsInfo.ip_source);
			// tcp
			vstp_md.values.put(VSTPDataItemType.TCP_SOURCE.getId(), Integer.toString(smsInfo.tcp_destination));
			vstp_md.values.put(VSTPDataItemType.TCP_DESTINATION.getId(), Integer.toString(smsInfo.tcp_source));
			// other
			vstp_md.values.put(VSTPDataItemType.SMPP_SEQ_NUM.getId(), Long.toString(pdu.sequence_number));
			vstp_md.values.put(VSTPDataItemType.SMPP_COMMAND_STATUS.getId(), Integer.toString(pdu.command_status.getId()));

			// out queue
			SGNode.out_offer(vstp_md);
			

		}catch(Exception e){
			e.printStackTrace();
		}		
		
	}
	
	private void processSMS(SMPPSmsInfo smsInfo, MessageType msg_type){
		MessageDescriptor vstp_md = new MessageDescriptor();
		int tmp_dcs;
		try{
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = msg_type;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;

			// seq num
			vstp_md.values.put(VSTPDataItemType.SMPP_SEQ_NUM.getId(), Long.toString(smsInfo.sequence_number));
			
			// ip
			vstp_md.values.put(VSTPDataItemType.IP_SOURCE.getId(), smsInfo.ip_source);
			vstp_md.values.put(VSTPDataItemType.IP_DESTINATION.getId(), smsInfo.ip_destination);
			// tcp
			vstp_md.values.put(VSTPDataItemType.TCP_SOURCE.getId(), Integer.toString(smsInfo.tcp_source));
			vstp_md.values.put(VSTPDataItemType.TCP_DESTINATION.getId(), Integer.toString(smsInfo.tcp_destination));
			// user
			vstp_md.values.put(VSTPDataItemType.SMPP_SYSTEM_ID.getId(), smsInfo.system_id);
			vstp_md.values.put(VSTPDataItemType.SMPP_PASSWORD.getId(), smsInfo.password);
			// other
			vstp_md.values.put(VSTPDataItemType.SMPP_SERVICE_TYPE.getId(), smsInfo.service_type.toString());
			vstp_md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_MODE.getId(), Integer.toString(smsInfo.esm_message_mode.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_ESM_MESSAGE_TYPE.getId(), Integer.toString(smsInfo.esm_message_type.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_ESM_GSM_FEATURES.getId(), Integer.toString(smsInfo.esm_gsm_features.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_PROTOCOL_ID.getId(), Integer.toString(smsInfo.protocol_id));
			vstp_md.values.put(VSTPDataItemType.SMPP_PRIORITY_FLAG.getId(), Integer.toString(smsInfo.priority_flag));
			vstp_md.values.put(VSTPDataItemType.SMPP_DELIVERY_TIME.getId(), smsInfo.delivery_time);
			vstp_md.values.put(VSTPDataItemType.SMPP_VALIDITY_PERIOD.getId(), smsInfo.validity_period);
			vstp_md.values.put(VSTPDataItemType.SMPP_RD_SMSC_RECEIPT.getId(), Integer.toString(smsInfo.rd_smsc_receipt.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_RD_SME_ACK.getId(), Integer.toString(smsInfo.rd_sme_ack.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_RD_INTERMEDIATE_NOTIFICATION.getId(), Integer.toString(smsInfo.rd_intermediate_notification.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_REPLACE_IF_PRESENT.getId(), Integer.toString(smsInfo.replace_if_present_flag));
			vstp_md.values.put(VSTPDataItemType.SMPP_SM_DEFAULT_MSG_ID.getId(), Integer.toString(smsInfo.sm_default_msg_id));

			
			// udh
			if(smsInfo.udh_present){
				vstp_md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
				// extra info for concatenated message
				vstp_md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(smsInfo.sar_segment_seqnum));
				vstp_md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(smsInfo.sar_total_segments));
				vstp_md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(smsInfo.sar_msg_ref_num));
			}else{
				vstp_md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
			}

			// SMS destination
			vstp_md.values.put(VSTPDataItemType.SMPP_RECIPIENT_ADDRESS.getId(), smsInfo.destination_addr);
			vstp_md.values.put(VSTPDataItemType.SMPP_RECIPIENT_TON.getId(), Integer.toString(smsInfo.destination_addr_ton.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_RECIPIENT_NP.getId(), Integer.toString(smsInfo.destination_addr_np.getId()));
			// SMS source
			vstp_md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_ADDRESS.getId(), smsInfo.source_addr);
			vstp_md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_TON.getId(), Integer.toString(smsInfo.source_addr_ton.getId()));
			vstp_md.values.put(VSTPDataItemType.SMPP_ORIGINATOR_NP.getId(), Integer.toString(smsInfo.source_addr_np.getId()));
			
			// SMS DCS
			switch(smsInfo.data_coding){
				case _8BIT_BINARY_1: tmp_dcs = Alphabet._8BIT.getId(); break;
				case _8BIT_BINARY_2: tmp_dcs = Alphabet._8BIT.getId(); break;
				case UCS2: tmp_dcs = Alphabet.UCS2.getId(); break;
				default:
					tmp_dcs = Alphabet.DEFAULT.getId();
					break;
			}
			vstp_md.values.put(VSTPDataItemType.SMPP_DATA_CODING.getId(), Integer.toString(tmp_dcs));
			// SMS status
			vstp_md.values.put(VSTPDataItemType.SMS_STATUS.getId(),  Integer.toString(smsInfo.status_code.getId()));
			// SMS text
			vstp_md.values.put(VSTPDataItemType.SMPP_SM_LENGTH.getId(), Integer.toString(smsInfo.message_length));
			vstp_md.values.put(VSTPDataItemType.SMPP_SM.getId(), Utils.bytes2hexstr(smsInfo.short_message.getBytes(), ""));
			// out queue
			SGNode.out_offer(vstp_md);


		}catch(Exception e){
			e.printStackTrace();
		}

	}
	
	
	private class Dw_r implements Runnable{
		DSPacket packet;
		SMPPCorrelationPacket smppp = null;
		SMPPSequenceDescriptor sd = null;
		SMPPSmsInfo smsInfo = null;
		Submit_sm submit = null;
		Submit_multi submit_multi = null;
		Deliver_sm deliver = null;
		OptionalParameterBase opp = null;
		PDUBase pdu = null;
		PDUBase[] pdu_lst = null;

		public void run() {
			logger.info(worker_name + " STARTING!");
			while(!stopping){
				packet = data_source.queue.poll();
				if(packet != null){
					try{
						// check data source type
						if(data_source.type == DataSourceType.SPCAP_SMPP || data_source.type == DataSourceType.SMPP){
							pdu_lst = SMPP_PDU.decode(packet.data);
							if(pdu_lst != null){
								for(int i = 0; i<pdu_lst.length; i++){
									pdu = pdu_lst[i];
									if(pdu != null){
										switch(pdu.type){
											// submit multi
											case SUBMIT_MULTI:
												StatsManager.DECODER_STATS.SMPP_SUBMIT_MULTI++;
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
													submit_multi = (Submit_multi)pdu;
													smsInfo = new SMPPSmsInfo();
													smsInfo.sequence_number = submit_multi.sequence_number;
													smsInfo.system_id = smppp.system_id;
													smsInfo.password = smppp.password;
													smsInfo.data_coding = submit_multi.data_coding;
													smsInfo.dataSource = data_source.type;
													smsInfo.source_addr = submit_multi.source_addr;
													smsInfo.source_addr_ton = submit_multi.source_addr_ton;
													smsInfo.source_addr_np = submit_multi.source_addr_npi;
													smsInfo.ip_destination = smppp.ip_destination;
													smsInfo.ip_source = smppp.ip_source;
													smsInfo.short_message = new String(submit_multi.short_message);
													smsInfo.status_code = pdu.command_status;
													smsInfo.tcp_destination = smppp.tcp_destination;
													smsInfo.tcp_source = smppp.tcp_source;
													smsInfo.message_length = submit_multi.sm_length;														
													smsInfo.delivery_time = submit_multi.schedule_delivery_time;
													smsInfo.esm_gsm_features = submit_multi.ns_features;
													smsInfo.esm_message_mode = submit_multi.message_mode;
													smsInfo.esm_message_type = submit_multi.message_type;
													smsInfo.priority_flag = submit_multi.priority_flag;
													smsInfo.protocol_id = submit_multi.protocol_id;
													smsInfo.rd_intermediate_notification = submit_multi.rd_intermediate_notification;
													smsInfo.rd_sme_ack = submit_multi.rd_sme_orig_ack;
													smsInfo.rd_smsc_receipt = submit_multi.rd_smsc_delivery_receipt;
													smsInfo.replace_if_present_flag = submit_multi.replace_if_present_flag;
													smsInfo.service_type = submit_multi.service_type;
													smsInfo.sm_default_msg_id = submit_multi.sm_default_msg_id;
													// UDH
													opp = submit_multi.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
													if(opp != null){
														smsInfo.udh_present = true;
														smsInfo.sar_msg_ref_num = ((Sar_msg_ref_num)opp).value;
													}
													opp = submit_multi.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
													if(opp != null) smsInfo.sar_segment_seqnum = ((Sar_segment_seqnum)opp).value;
													opp = submit_multi.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
													if(opp != null) smsInfo.sar_total_segments = ((Sar_total_segments)opp).value;

													// one entry for each destination
													for(int j = 0; j<submit_multi.dest_address_lst.length; j++){
														smsInfo.destination_addr = submit_multi.dest_address_lst[j].destination_addr;
														smsInfo.destination_addr_ton = submit_multi.dest_address_lst[j].dest_addr_ton;
														smsInfo.destination_addr_np = submit_multi.dest_address_lst[j].dest_addr_npi;
														
														
														// process
														processSMS(smsInfo, MessageType.SMPP_MO);
													}
												
												}
												break;
											case SUBMIT_MULTI_RESP:
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													smsInfo = new SMPPSmsInfo();
													smsInfo.ip_destination = smppp.ip_destination;
													smsInfo.ip_source = smppp.ip_source;
													smsInfo.tcp_destination = smppp.tcp_destination;
													smsInfo.tcp_source = smppp.tcp_source;
													processSMS_RESP(smsInfo, pdu);
												
												}												
												break;
											// submit
											case SUBMT_SM:
												StatsManager.DECODER_STATS.SMPP_SUBMIT_SM++;
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
													submit = (Submit_sm)pdu;
													smsInfo = new SMPPSmsInfo();
													smsInfo.sequence_number = submit.sequence_number;
													smsInfo.system_id = smppp.system_id;
													smsInfo.password = smppp.password;
													smsInfo.data_coding = submit.data_coding;
													smsInfo.dataSource = data_source.type;
													smsInfo.source_addr = submit.source_addr;
													smsInfo.source_addr_ton = submit.source_addr_ton;
													smsInfo.source_addr_np = submit.source_addr_npi;
													smsInfo.destination_addr = submit.destination_addr;
													smsInfo.destination_addr_ton = submit.dest_addr_ton;
													smsInfo.destination_addr_np = submit.dest_addr_npi;
													smsInfo.ip_destination = smppp.ip_destination;
													smsInfo.ip_source = smppp.ip_source;
													smsInfo.short_message = new String(submit.short_message);
													smsInfo.status_code = pdu.command_status;
													smsInfo.tcp_destination = smppp.tcp_destination;
													smsInfo.tcp_source = smppp.tcp_source;
													smsInfo.message_length = submit.sm_length;														
													smsInfo.delivery_time = submit.schedule_delivery_time;
													smsInfo.esm_gsm_features = submit.ns_features;
													smsInfo.esm_message_mode = submit.message_mode;
													smsInfo.esm_message_type = submit.message_type;
													smsInfo.priority_flag = submit.priority_flag;
													smsInfo.protocol_id = submit.protocol_id;
													smsInfo.rd_intermediate_notification = submit.rd_intermediate_notification;
													smsInfo.rd_sme_ack = submit.rd_sme_orig_ack;
													smsInfo.rd_smsc_receipt = submit.rd_smsc_delivery_receipt;
													smsInfo.replace_if_present_flag = submit.replace_if_present_flag;
													smsInfo.service_type = submit.service_type;
													smsInfo.sm_default_msg_id = submit.sm_default_msg_id;
													
													// UDH
													opp = submit.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
													if(opp != null){
														smsInfo.udh_present = true;
														smsInfo.sar_msg_ref_num = ((Sar_msg_ref_num)opp).value;
													}
													opp = submit.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
													if(opp != null) smsInfo.sar_segment_seqnum = ((Sar_segment_seqnum)opp).value;
													opp = submit.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
													if(opp != null) smsInfo.sar_total_segments = ((Sar_total_segments)opp).value;
													
													// process
													processSMS(smsInfo, MessageType.SMPP_MO);												
												}


												break;
											case SUBMIT_SM_RESP:
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													smsInfo = new SMPPSmsInfo();
													smsInfo.ip_destination = smppp.ip_destination;
													smsInfo.ip_source = smppp.ip_source;
													smsInfo.tcp_destination = smppp.tcp_destination;
													smsInfo.tcp_source = smppp.tcp_source;
													processSMS_RESP(smsInfo, pdu);
												
												}												
												break;
												
											// deliver
											case DELIVER_SM:
												StatsManager.DECODER_STATS.SMPP_DELIVER_SM++;
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
													deliver = (Deliver_sm)pdu;
													smsInfo = new SMPPSmsInfo();
													smsInfo.sequence_number = deliver.sequence_number;
													smsInfo.system_id = smppp.system_id;
													smsInfo.password = smppp.password;
													smsInfo.data_coding = deliver.data_coding;
													smsInfo.dataSource = data_source.type;
													smsInfo.source_addr = deliver.source_addr;
													smsInfo.source_addr_ton = deliver.source_addr_ton;
													smsInfo.source_addr_np = deliver.source_addr_npi;
													smsInfo.destination_addr = deliver.destination_addr;
													smsInfo.destination_addr_ton = deliver.dest_addr_ton;
													smsInfo.destination_addr_np = deliver.dest_addr_npi;
													smsInfo.ip_destination = smppp.ip_destination;
													smsInfo.ip_source = smppp.ip_source;
													smsInfo.short_message = new String(deliver.short_message);
													smsInfo.status_code = pdu.command_status;
													smsInfo.tcp_destination = smppp.tcp_destination;
													smsInfo.tcp_source = smppp.tcp_source;
													smsInfo.message_length = deliver.sm_length;														
													smsInfo.delivery_time = deliver.schedule_delivery_time;
													smsInfo.esm_gsm_features = deliver.ns_features;
													smsInfo.esm_message_mode = deliver.message_mode;
													smsInfo.esm_message_type = deliver.message_type;
													smsInfo.priority_flag = deliver.priority_flag;
													smsInfo.protocol_id = deliver.protocol_id;
													smsInfo.rd_intermediate_notification = deliver.rd_intermediate_notification;
													smsInfo.rd_sme_ack = deliver.rd_sme_orig_ack;
													smsInfo.rd_smsc_receipt = deliver.rd_smsc_delivery_receipt;
													smsInfo.replace_if_present_flag = deliver.replace_if_present_flag;
													smsInfo.service_type = deliver.service_type;
													smsInfo.sm_default_msg_id = deliver.sm_default_msg_id;
													// UDH
													opp = deliver.getParameter(OptionalParameterType.SAR_MSG_REF_NUM);
													if(opp != null){
														smsInfo.udh_present = true;
														smsInfo.sar_msg_ref_num = ((Sar_msg_ref_num)opp).value;
													}
													opp = deliver.getParameter(OptionalParameterType.SAR_SEGMENT_SEQNUM);
													if(opp != null) smsInfo.sar_segment_seqnum = ((Sar_segment_seqnum)opp).value;
													opp = deliver.getParameter(OptionalParameterType.SAR_TOTAL_SEGMENTS);
													if(opp != null) smsInfo.sar_total_segments = ((Sar_total_segments)opp).value;
													
													// process
													processSMS(smsInfo, MessageType.SMPP_MT);													
												}


												break;
											case DELIVER_SM_RESP:
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													sd = smppp.sequence_map.get(pdu.sequence_number);
													if(sd != null) sd.response_code = pdu.command_status;
													smsInfo = new SMPPSmsInfo();
													smsInfo.ip_destination = smppp.ip_destination;
													smsInfo.ip_source = smppp.ip_source;
													smsInfo.tcp_destination = smppp.tcp_destination;
													smsInfo.tcp_source = smppp.tcp_source;
													processSMS_RESP(smsInfo, pdu);

												}
												break;
											
											// receiver
											case BIND_RECEIVER: 
												StatsManager.DECODER_STATS.SMPP_BIND_RECEIVER++;
												smppp = initCorrelation(packet);
												smppp.system_id = ((Bind_receiver)pdu).system_id;
												smppp.password = ((Bind_receiver)pdu).password;
												SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
												break;
											case BIND_RECEIVER_RESP: 
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													sd = smppp.sequence_map.get(pdu.sequence_number);
													if(sd != null) sd.response_code = pdu.command_status;
												}
												break;
											
											// transmitter
											case BIND_TRANSMITTER: 
												StatsManager.DECODER_STATS.SMPP_BIND_TRANSMITTER++;
												smppp = initCorrelation(packet);
												smppp.system_id = ((Bind_transmitter)pdu).system_id;
												smppp.password = ((Bind_transmitter)pdu).password;
												SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
												break;
											case BIND_TRANSMITTER_RESP: 
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													sd = smppp.sequence_map.get(pdu.sequence_number);
													if(sd != null) sd.response_code = pdu.command_status;
												}
												break;
											
											// transceiver
											case BIND_TRANSCEIVER: 
												StatsManager.DECODER_STATS.SMPP_BIND_TRANSCEIVER++;
												smppp = initCorrelation(packet);
												smppp.system_id = ((Bind_transceiver)pdu).system_id;
												smppp.password = ((Bind_transceiver)pdu).password;
												SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
												break;
											case BIND_TRANSCEIVER_RESP: 
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													sd = smppp.sequence_map.get(pdu.sequence_number);
													if(sd != null) sd.response_code = pdu.command_status;
												}
												break;
											
											// unbind
											case UNBIND:
												StatsManager.DECODER_STATS.SMPP_UNBIND++;
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												SMPPCorrelationManager.newSequence(smppp, pdu.sequence_number, pdu.type, null, null);
												break;
											case UNBIND_RESP: 
												smppp = SMPPCorrelationManager.get(packet.ip_source, packet.ip_destination, packet.tcp_source, packet.tcp_destination);
												if(smppp != null){
													sd = smppp.sequence_map.get(pdu.sequence_number);
													if(sd != null) sd.response_code = pdu.command_status;
												}
												// remove correlation
												SMPPCorrelationManager.remove(smppp);
												break;
												
										}
									}	
								}
							}
						}
						
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Error while decoding packet!");
						Utils.bytes2file(packet.data, "/mnt/vazom_rt/" + System.currentTimeMillis() + ".raw");
					}
					
				}else{
					//logger.info(worker_name + " sleeping for 1000 msec!");
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
			}
		}
	}

	public PDS_DecoderWorker_SMPP(int id, DSBase _data_source){
		data_source = _data_source;
		dw_r = new Dw_r();
		worker_name = "PDS_DECODER_WORKER_SMPP_" + _data_source.dsd.id.toUpperCase() + "_" + id;
		dw_t = new Thread(dw_r, worker_name);
		dw_t.start();
		
	}
}
