package net.isup.parameters.gn;

import java.util.HashMap;


public enum NatureOfAddressIndicatorType {
	SUBSCRIBER_NUMBER(0x01),
	UNKNOWN(0x02),
	NATIONAL(0x03),
	INTERNATIONAL(0x04);
	

	
	
	private int id;
	private static final HashMap<Integer, NatureOfAddressIndicatorType> lookup = new HashMap<Integer, NatureOfAddressIndicatorType>();
	static{
		for(NatureOfAddressIndicatorType td :NatureOfAddressIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NatureOfAddressIndicatorType get(int id){ return lookup.get(id); }
	private NatureOfAddressIndicatorType(int _id){ id = _id; }	
}
