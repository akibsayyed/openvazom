package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Source_addr_subunit extends OptionalParameterBase {
	public enum ValueType{
		UNKNOWN(0x00),
		MS_DISPLAY(0x01),
		MOBILE_EQUIPMENT(0x02),
		SMART_CARD_1(0x03),
		EXTERNAL_UNIT_1(0x04);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}

	public ValueType value;

	public Source_addr_subunit(){
		type = OptionalParameterType.SOURCE_ADDR_SUBUNIT;
	}
	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);

		
	}

}
