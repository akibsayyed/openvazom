package net.sccp.parameters.global_title;

import java.util.ArrayList;
import java.util.Arrays;

import net.sccp.NatureOfAddress;

public class GlobalTitle_NOA extends GlobalTitleBase {
	public NatureOfAddress natureOfAddress;
	public boolean oddNumOfAddrSignals;
	public byte[] addressSignals;
	
	public GlobalTitle_NOA(){
		super();
		type = GlobalTitleIndicator.NATURE_OF_ADDRESS_INDICATOR_ONLY;
	}
	public byte[] encode(){
		byte[] res = null;
		int tmp = 0;
		ArrayList<Byte> packet = new ArrayList<Byte>();
		if(addressSignals.length % 2 != 0) tmp = 0x80;
		tmp += natureOfAddress.getId();
		packet.add((byte)tmp);
		for(int i = 0; i<addressSignals.length; i++) packet.add(addressSignals[i]);
		res = new byte[packet.size()];
		for(int i = 0; i<packet.size(); i++) res[i] = packet.get(i);
		return res;
	}	

	public void init(byte[] data) {
		natureOfAddress = NatureOfAddress.get(data[0] & 0x7F);
		oddNumOfAddrSignals = (data[0] & 0x80) == 0x80;
		addressSignals = Arrays.copyOfRange(data, 1, data.length);

	}

}
