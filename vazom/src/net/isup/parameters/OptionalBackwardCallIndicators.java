package net.isup.parameters;

import net.isup.ParameterType;

public class OptionalBackwardCallIndicators extends ParameterBase {
	public OptionalBackwardCallIndicators(){
		type = ParameterType.OPTIONAL_BACKWARD_CALL_INDICATORS;
	}
}
