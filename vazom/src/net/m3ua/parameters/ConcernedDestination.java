package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class ConcernedDestination extends M3UAParameter {
	public ConcernedDestination(){
		type = M3UAParameterType.CONCERNED_DESTINATION;
	}
	
	public void init(byte[] data) {
		value = data;
	}



}
