package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CallRecoveryID extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public TimeDateOffset get_timeDateOffset(){
		return (TimeDateOffset)elements.get(1).data;
	}
	public TimeDateOffset new_timeDateOffset(){
		return new TimeDateOffset();
	}

	public TimeOfDay get_timeOfDay(){
		return (TimeOfDay)elements.get(2).data;
	}
	public TimeOfDay new_timeOfDay(){
		return new TimeOfDay();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(275, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_timeDateOffset(); }});
		elements.add(new ElementDescriptor(309, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_timeOfDay(); }});
	}
	public CallRecoveryID(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
