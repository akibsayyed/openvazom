package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class RedirectionReason extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _busy = 1;
	public static final int _no_Answer = 2;
	public static final int _unconditional = 3;
	public static final int _no_Page_Response = 4;
	public static final int _unavailable = 5;
	public static final int _unroutable = 6;
	public static final int _call_accepted = 7;
	public static final int _call_refused = 8;
	public static final int _uSCFvm_Divert_to_voice_mail = 9;
	public static final int _uSCFms_Divert_to_an_MS_provided_DN = 10;
	public static final int _uSCFnr_Divert_to_a_network_registered_DN = 11;
	public RedirectionReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
