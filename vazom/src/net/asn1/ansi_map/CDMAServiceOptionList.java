package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAServiceOptionList extends SEQUENCE{

	public CDMAServiceOptionType new_child(){
		return new CDMAServiceOptionType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMAServiceOptionType getChild(int index){
		return (CDMAServiceOptionType)of_children.get(index);
	}
	public CDMAServiceOptionList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
