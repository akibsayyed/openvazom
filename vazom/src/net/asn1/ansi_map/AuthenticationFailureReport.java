package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationFailureReport extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public ReportType get_reportType(){
		return (ReportType)elements.get(2).data;
	}
	public ReportType new_reportType(){
		return new ReportType();
	}

	public SystemAccessType get_systemAccessType(){
		return (SystemAccessType)elements.get(3).data;
	}
	public SystemAccessType new_systemAccessType(){
		return new SystemAccessType();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(4).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(5).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CallHistoryCountExpected get_callHistoryCountExpected(){
		return (CallHistoryCountExpected)elements.get(6).data;
	}
	public CallHistoryCountExpected new_callHistoryCountExpected(){
		return new CallHistoryCountExpected();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(7).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public ReportType get_reportType2(){
		return (ReportType)elements.get(8).data;
	}
	public ReportType new_reportType2(){
		return new ReportType();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(9).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(10).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public MEID get_meid(){
		return (MEID)elements.get(11).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(44, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_reportType(); }});
		elements.add(new ElementDescriptor(34, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemAccessType(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemCapabilities(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(79, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCountExpected(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(44, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reportType2(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public AuthenticationFailureReport(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
