package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TargetMeasurementInformation extends SEQUENCE{

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(0).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public SignalQuality get_signalQuality(){
		return (SignalQuality)elements.get(1).data;
	}
	public SignalQuality new_signalQuality(){
		return new SignalQuality();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalQuality(); }});
	}
	public TargetMeasurementInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
