package net.isup.parameters;

import net.isup.ParameterType;

public class CalledDirectoryNumber extends ParameterBase {
	public CalledDirectoryNumber(){
		type = ParameterType.CALLED_DIRECTORY_NUMBER;
	}
}
