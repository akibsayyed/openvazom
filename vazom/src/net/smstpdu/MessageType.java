package net.smstpdu;


public enum MessageType {
	SMS_DELIVER,
	SMS_DELIVER_REPORT,
	SMS_STATUS_REPORT,
	SMS_COMMAND,
	SMS_SUBMIT,
	SMS_SUBMIT_REPORT,
	RESERVED;
	
	// bits 0 and 1 = 000000XX
	public synchronized static MessageType fromId(int id, MessageDirection md){
		MessageType res = null;
		switch(id){
			case 0x00: res = (md == MessageDirection.SC_MS ? SMS_DELIVER : SMS_DELIVER_REPORT); break; 
			case 0x02: res = (md == MessageDirection.SC_MS ? SMS_STATUS_REPORT : SMS_COMMAND); break; 
			case 0x01: res = (md == MessageDirection.SC_MS ? SMS_STATUS_REPORT : SMS_SUBMIT); break;
			case 0x03: res = RESERVED;
		}
		return res;
	}
}
