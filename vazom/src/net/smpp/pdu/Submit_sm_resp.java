package net.smpp.pdu;

import java.util.ArrayList;

import net.smpp.ErrorCodeType;
import net.smpp.PDUType;
import net.utils.Utils;

public class Submit_sm_resp extends PDUBase {
	public String message_id;
	
	public Submit_sm_resp(){
		type = PDUType.SUBMIT_SM_RESP;
	}

	public byte[] encode(){
		//command_length += system_id.length() + 1 + (sc_interface_version != null ? sc_interface_version.length : 0);
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		Utils.bytesToLst(buff, Utils.Cstr_encode(message_id));
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		
		return Utils.list2array(buff);

	}
	
	
	
	public void init(byte[] data) {
		if(command_status == ErrorCodeType.ESME_ROK){
			// System ID
			message_id = Utils.Cstr_decode(data, byte_pos);
			byte_pos += message_id.length() + 1;
			
		}

	}

}
