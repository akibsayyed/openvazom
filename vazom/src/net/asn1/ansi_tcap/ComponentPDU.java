package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ComponentPDU extends CHOICE{

	public Invoke get_invokeLast(){
		return (Invoke)elements.get(0).data;
	}
	public Invoke new_invokeLast(){
		return new Invoke();
	}

	public ReturnResult get_returnResultLast(){
		return (ReturnResult)elements.get(1).data;
	}
	public ReturnResult new_returnResultLast(){
		return new ReturnResult();
	}

	public ReturnError get_returnError(){
		return (ReturnError)elements.get(2).data;
	}
	public ReturnError new_returnError(){
		return new ReturnError();
	}

	public Reject get_reject(){
		return (Reject)elements.get(3).data;
	}
	public Reject new_reject(){
		return new Reject();
	}

	public Invoke get_invokeNotLast(){
		return (Invoke)elements.get(4).data;
	}
	public Invoke new_invokeNotLast(){
		return new Invoke();
	}

	public ReturnResult get_returnResultNotLast(){
		return (ReturnResult)elements.get(5).data;
	}
	public ReturnResult new_returnResultNotLast(){
		return new ReturnResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_invokeLast(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_returnResultLast(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_returnError(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_reject(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_invokeNotLast(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_returnResultNotLast(); }});
	}
	public ComponentPDU(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
