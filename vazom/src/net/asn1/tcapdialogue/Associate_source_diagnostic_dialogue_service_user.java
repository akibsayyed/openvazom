package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Associate_source_diagnostic_dialogue_service_user extends SEQUENCE{

	public Dialogue_service_userInteger get_dialogue_service_user(){
		return (Dialogue_service_userInteger)elements.get(0).data;
	}
	public Dialogue_service_userInteger new_dialogue_service_user(){
		return new Dialogue_service_userInteger();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_dialogue_service_user(); }});
	}
	public Associate_source_diagnostic_dialogue_service_user(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 1;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
