package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ChangeFacilities extends SET{

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(0).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(1).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public DataKey get_dataKey(){
		return (DataKey)elements.get(2).data;
	}
	public DataKey new_dataKey(){
		return new DataKey();
	}

	public DataPrivacyParameters get_dataPrivacyParameters(){
		return (DataPrivacyParameters)elements.get(3).data;
	}
	public DataPrivacyParameters new_dataPrivacyParameters(){
		return new DataPrivacyParameters();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(4).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(5).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public ISLPInformation get_ilspInformation(){
		return (ISLPInformation)elements.get(6).data;
	}
	public ISLPInformation new_ilspInformation(){
		return new ISLPInformation();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(7).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public RandomVariable get_randomVariable(){
		return (RandomVariable)elements.get(8).data;
	}
	public RandomVariable new_randomVariable(){
		return new RandomVariable();
	}

	public TDMABandwidth get_tdmaBandwidth(){
		return (TDMABandwidth)elements.get(9).data;
	}
	public TDMABandwidth new_tdmaBandwidth(){
		return new TDMABandwidth();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(10).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(11).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(215, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataKey(); }});
		elements.add(new ElementDescriptor(216, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataPrivacyParameters(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(217, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ilspInformation(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(40, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariable(); }});
		elements.add(new ElementDescriptor(220, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBandwidth(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
	}
	public ChangeFacilities(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
