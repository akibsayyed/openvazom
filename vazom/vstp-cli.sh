cmd=$3
port=$2
ip=$1
if [ -n "$cmd" -a -n "$port" -a -n "$ip" ]; then
        vstp="NA:-:NA:NA:-:CLI:1\nVSTP.CLI.CMD=$cmd\nEND"
        data=`echo -e $vstp |nc -w 2 -u $ip $port|sed -n '2p'|sed -e 's/.*=//g'`
        printf "%b\n" "`echo $data | sed -e 's/\(..\)/\\\x\1/g'`"
else
        echo "Missing parameters, usage => ./vstp.sh IP PORT CMD!"
fi