package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ACGDirective extends SET{

	public ControlType get_controlType(){
		return (ControlType)elements.get(0).data;
	}
	public ControlType new_controlType(){
		return new ControlType();
	}

	public DestinationAddress get_destinationAddress(){
		return (DestinationAddress)elements.get(1).data;
	}
	public DestinationAddress new_destinationAddress(){
		return new DestinationAddress();
	}

	public GapDuration get_gapDuration(){
		return (GapDuration)elements.get(2).data;
	}
	public GapDuration new_gapDuration(){
		return new GapDuration();
	}

	public GapInterval get_gapInterval(){
		return (GapInterval)elements.get(3).data;
	}
	public GapInterval new_gapInterval(){
		return new GapInterval();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(341, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_controlType(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_destinationAddress(); }});
		elements.add(new ElementDescriptor(342, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_gapDuration(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_gapInterval(); }});
	}
	public ACGDirective(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
