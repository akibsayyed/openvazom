package net.smpp_conn;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Inet4Address;
import java.net.InetAddress;
import java.net.ServerSocket;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import org.apache.log4j.Logger;
import net.config.SGNConfigData;
import net.logging.LoggingManager;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionDirectionType;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionType;
import net.routing.RoutingConnectionWorkerMethod;
import net.security.SecurityManager;
import net.smpp.ErrorCodeType;
import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.SMPP_PDU;
import net.smpp.TypeOfNumber;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Enquire_link;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Unbind;

public class SMPPConnection extends RoutingConnection {
	public static Logger logger=Logger.getLogger(SMPPConnection.class);
	public String ip;
	public int port;
	public String id;
	public String system_id;
	public String password;
	public int el_interval;
	public long sequence_number = 1;
	
	public Socket socket;
	public ServerSocket server_socket;
	private OutputStream out;
	private InputStream in;
	private ConcurrentLinkedQueue<SMPP_In_Packet> in_queue;
	private ConcurrentLinkedQueue<RoutingConnectionPayload> out_queue;
	public boolean stopping;
	public boolean active;
	public boolean down;
	public boolean smpp_ready;
	public boolean is_proxy;
	private Data_r datar_r;
	private Thread datar_t;
	private Data_w dataw_r;
	private Thread dataw_t;
	private Worker worker_r;
	private Thread worker_t;
	private El el_r;
	private Thread el_t;
	private ServerListener sl_r;
	private Thread sl_t;
	private ConcurrentHashMap<String, SMPPClientConnection> client_connection_lst;

	
	private class ServerListener implements Runnable{
		private Socket client_socket;
		private SMPPClientConnection con;
		public void run() {
			while(!stopping){
				try{
					client_socket = server_socket.accept();
					if(client_socket != null){
						//LoggingManager.debug(logger, "SMPP CLient connected = [" + client_socket.getInetAddress().toString() + ":" + client_socket.getPort() + "]");
						con = new SMPPClientConnection(client_socket, client_connection_lst, in_queue);
						client_connection_lst.put(client_socket.getInetAddress().toString() + ":" + client_socket.getPort(), con);
					}
					
				}catch(Exception e){
					LoggingManager.error(logger, "Error while accepting new connection to SMPP Connection! [ " + id + " ]");
					e.printStackTrace();
					stopping = true;
				}
			}			
		}
	
	}	
	
	private class El implements Runnable{
		public void run() {
			Enquire_link el = null;
			SMPPPayload spl = null;
			while(!stopping){
				try{ Thread.sleep(el_interval); }catch(Exception e){ e.printStackTrace(); }
				el = new Enquire_link();
				el.sequence_number = sequence_number++;
				spl = new SMPPPayload();
				spl.setParams(new Object[]{el, el.encode()});
				out_offer(spl);


			}
		}
	}
	
	private class Data_r implements Runnable{
		byte[] buffer = new byte[8192];
		int bc;
		SMPP_In_Packet in_packet = null;
		public void run() {
			//logger.info("Starting...");
			while(!stopping){
				try{
					bc = in.read(buffer);
					if(bc > 0){
						// statistics
						stats.IN++;
						stats.BYTES += bc;
						
						if(in_queue.size() < SGNConfigData.sgn_global_max_queue_size){
							in_packet = new SMPP_In_Packet();
							in_packet.data = Arrays.copyOfRange(buffer, 0, bc);
							in_queue.offer(in_packet);
						}else{
							IN_QUEUE_MAX++;
							//LoggingManager.warn(logger, "SMPPConnection.in_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
						}
						
					}else{
						//stop();
						//try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

					}
				}catch(Exception e){
					stop();
				}
			}
			//logger.info("Ending...");

		}
	
	}	
	private class Data_w implements Runnable{
		RoutingConnectionPayload rpl = null;
		SMPPPayload mp = null;
		public void run() {
			//logger.info("Starting...");
			while(!stopping){
				// get payload, convert if necessary
				rpl = convert(out_queue.poll());
				// check if payload is valid
				if(rpl != null) mp = (SMPPPayload)rpl; else mp = null;
				// process if payload is valid
				if(mp != null){
					// statistics
					stats.OUT++;
					stats.BYTES += mp.payload_data.length;
					// send
					try{
						out.write(mp.payload_data);
					}catch(Exception e){
						stop();
					}
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					
				}
			}	
			//logger.info("Ending...");
		}

		
	}	
	private class Worker implements Runnable{
		SMPP_In_Packet data = null;
		public void run() {
			//logger.info("Starting...");
			while(!stopping){
				data = in_queue.poll();
				if(data != null){
					// custom method
					if(worker_method != null) worker_method.execute(data);
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					
				}
			}			
			//logger.info("Ending...");
		}
		
	}	
	
	
	public void stop(){
		if(is_proxy){
			stopping = true;
			active = false;
			try{
				socket.close();
			}catch(Exception e){}
			
		}else{
			closeSMPP();
		}
		
	}
	
	
	public void start(){
		in_queue.clear();
		out_queue.clear();
		
		active = true;
		stopping = false;
		//logger.info("Creating SMPP Connection Data Receiver thread: [ " + ip + ":" + port + " ]!");
		if(direction == RoutingConnectionDirectionType.CLIENT){
			datar_r = new Data_r();
			datar_t = new Thread(datar_r, "SMPP_DATA_RECEIVER_" + ip + ":" + port);
			datar_t.start();

			//logger.info("Creating SMPP Connection Data Sender thread: [ " + ip + ":" + port +" ]!");
			dataw_r = new Data_w();
			dataw_t = new Thread(dataw_r, "SMPP_DATA_SENDER_" + ip + ":" + port);
			dataw_t.start();
			
		}

		
		//logger.info("Creating SMPP Connection Data Worker thread: [ " + ip + ":" + port + " ]!");
		worker_r = new Worker();
		worker_t = new Thread(worker_r, "SMPP_DATA_WORKER_" + ip + ":" + port);
		worker_t.start();
		
		// Enquire link
		if(!is_proxy){
			switch(direction){
				case CLIENT:
					if(worker_method == null){
						SMPPClientWorkerMethod swm = new SMPPClientWorkerMethod(this);
						attachWorkerMethod(swm);
					}
					el_r = new El();
					el_t = new Thread(el_r, "SMPP_EL_" + id);
					el_t.start();
					break;
				case SERVER:
					if(worker_method == null){
						SMPPServerWorkerMethod swm = new SMPPServerWorkerMethod(this);
						attachWorkerMethod(swm);
					}
					break;
			}
			
		}
		
		
		// stats
		stats.reset();

		
	}	
	
	public boolean closeSMPP(){
		try{
			Unbind unb = new Unbind();
			unb.sequence_number = sequence_number++;
			SMPPPayload spl = new SMPPPayload();
			spl.setParams(new Object[]{unb, unb.encode()});
			out_offer(spl);
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return false;
	
	}
	
	
	public void initSMPP(){
		byte[] buffer = new byte[8192];
		int bc;

		sequence_number = 1;
		
		Bind_transceiver bt = new Bind_transceiver();
		bt.system_id = system_id;
		bt.password = password;
		bt.system_type = "";
		bt.interface_version = 0x34;
		bt.addr_ton = TypeOfNumber.UNKNOWN;
		bt.addr_npi = NumberingPlan.UNKNOWN;
		bt.address_range = "";
		bt.sequence_number = sequence_number++;
		
		try{
			out.write(bt.encode());
			out.flush();
			bc = in.read(buffer);
			if(bc > 0){
				PDUBase[] pdu = SMPP_PDU.decode(Arrays.copyOfRange(buffer, 0, bc));
				if(pdu != null){
					for(int i = 0; i<pdu.length; i++){
						if(pdu[i].type == PDUType.BIND_TRANSCEIVER_RESP){
							if(pdu[i].command_status != null){
								if(pdu[i].command_status == ErrorCodeType.ESME_ROK){
									smpp_ready = true;
									start();
									LoggingManager.debug(SGNConfigData.smpp_debug, logger, "SMPP connection [" + id + "] initialized!");
									
								}else{
									LoggingManager.error(logger, "SMPP BIND error [" + pdu[i].command_status.toString() + "]!");
								}
								
							}
						}
					}
				}else{
					smpp_ready = false;
					stop();
					
				}
			}else{
				smpp_ready = false;
				stop();
			}
		}catch(Exception e){
			e.printStackTrace();
			smpp_ready = false;
			stop();
			LoggingManager.error(logger, "Error while initializing SMPP connection [" + id + "]!");
		}
	}

	public void init_server(String local_ip, int local_port){
		try{
			InetAddress addr = Inet4Address.getByName(local_ip);
			client_connection_lst = new ConcurrentHashMap<String, SMPPClientConnection>();
			server_socket = new ServerSocket(local_port, 1000, addr);
			sl_r = new ServerListener();
			sl_t = new Thread(sl_r, "SMPP_CLIENT_LISTENER_" + id.toUpperCase());
			sl_t.start();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	public SMPPConnection(String _id, String _ip, int _port, String _system_id, String _password, int _el_interval){
		type = RoutingConnectionType.SMPP;
		direction = RoutingConnectionDirectionType.CLIENT;
		id = _id;
		system_id = _system_id;
		password = _password;
		ip = _ip;
		port = _port;
		el_interval = _el_interval;
		in_queue = new ConcurrentLinkedQueue<SMPP_In_Packet>();
		out_queue = new ConcurrentLinkedQueue<RoutingConnectionPayload>();

	}
	public SMPPConnection(String _id, String _ip, int _port, String _system_id, String _password, int _el_interval, RoutingConnectionDirectionType _direction){
		type = RoutingConnectionType.SMPP;
		direction = _direction;
		id = _id;
		system_id = _system_id;
		password = _password;
		ip = _ip;
		port = _port;
		el_interval = _el_interval;
		in_queue = new ConcurrentLinkedQueue<SMPP_In_Packet>();
		out_queue = new ConcurrentLinkedQueue<RoutingConnectionPayload>();

	}
	

	
	public boolean initSocket(){
		try{
			socket = new Socket(ip, port);
			in = socket.getInputStream();
			out = socket.getOutputStream();
			return true;
			
		}catch(Exception e){
			LoggingManager.error(logger, "SMPPConnection: Error while initializing TCP socket [" + ip + ":" + port + "]!");
			return false;
			
		}
	}
	
	public SMPPConnection(Socket _socket, boolean proxy){
		type = RoutingConnectionType.SMPP;
		direction = RoutingConnectionDirectionType.CLIENT;
		socket = _socket;
		is_proxy = proxy;
		if(is_proxy) smpp_ready = true;
		in_queue = new ConcurrentLinkedQueue<SMPP_In_Packet>();
		out_queue = new ConcurrentLinkedQueue<RoutingConnectionPayload>();
		try{
			in = socket.getInputStream();
			out = socket.getOutputStream();
		}catch(Exception e){
			e.printStackTrace();
			LoggingManager.error(logger, "SMPPConnection: Error while initializing IN/OUT streams!");
		}
	}
	
	public void attachWorkerMethod(RoutingConnectionWorkerMethod method) {
		worker_method = method;

	}

	public RoutingConnectionPayload convert(RoutingConnectionPayload input) {
		RoutingConnectionPayload res = null;
		if(input != null){
			switch(input.type){
				case SMPP:
					res = input;
					break;
				case M3UA:
					// Feature check for FEATURE_PDU_CONVERT
					//if(SecurityManager.check_feature(SecurityManager.FEATURE_PDU_CONVERT)){
					
					//}					
					//TODO
					break;
				case MTP3:
					// Feature check for FEATURE_PDU_CONVERT
					//if(SecurityManager.check_feature(SecurityManager.FEATURE_PDU_CONVERT)){
					
					//}					
					//TODO
					break;
					
			}
			
		}
		return res;
	}

	public void detachWorkerMethod() {
		worker_method = null;

	}

	public void out_offer(RoutingConnectionPayload data) {
		if(data != null){
			if(out_queue.size() < SGNConfigData.sgn_global_max_queue_size) out_queue.offer(data);
			else{
				OUT_QUEUE_MAX++;
			}
		}

	}


	@Override
	public void start_server_listener() {
		// TODO Auto-generated method stub
		
	}

}
