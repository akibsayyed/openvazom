package net.m3ua;

import java.util.HashMap;

public enum M3UAMessageType {
	ERR(M3UAMessageClass.MGMT.getId(), 0x00),
	NTFY(M3UAMessageClass.MGMT.getId(), 0x01),
	
	DATA(M3UAMessageClass.TRANSFER.getId(), 0x01),
	
	DUNA(M3UAMessageClass.SSNM.getId(), 0x01),
	DAVA(M3UAMessageClass.SSNM.getId(), 0x02),
	DAUD(M3UAMessageClass.SSNM.getId(), 0x03),
	SCON(M3UAMessageClass.SSNM.getId(), 0x04),
	DUPU(M3UAMessageClass.SSNM.getId(), 0x05),
	DRST(M3UAMessageClass.SSNM.getId(), 0x06),
	
	ASPUP(M3UAMessageClass.ASPSM.getId(), 0x01),
	ASPDN(M3UAMessageClass.ASPSM.getId(), 0x02),
	BEAT(M3UAMessageClass.ASPSM.getId(), 0x03),
	ASPUP_ACK(M3UAMessageClass.ASPSM.getId(), 0x04),
	ASPDN_ACK(M3UAMessageClass.ASPSM.getId(), 0x05),
	BEAT_ACK(M3UAMessageClass.ASPSM.getId(), 0x06),
	
	ASPAC(M3UAMessageClass.ASPTM.getId(), 0x01),
	ASPIA(M3UAMessageClass.ASPTM.getId(), 0x02),
	ASPAC_ACK(M3UAMessageClass.ASPTM.getId(), 0x03),
	ASPIA_ACK(M3UAMessageClass.ASPTM.getId(), 0x04),
	
	REG_REQ(M3UAMessageClass.RKM.getId(), 0x01),
	REG_RSP(M3UAMessageClass.RKM.getId(), 0x02),
	DEREG_REQ(M3UAMessageClass.RKM.getId(), 0x03),
	DEREG_RSP(M3UAMessageClass.RKM.getId(), 0x04);
	
	
	
	private int id;
	private int class_id;
	private static final HashMap<String, M3UAMessageType> lookup = new HashMap<String, M3UAMessageType>();
	static{
		for(M3UAMessageType td : M3UAMessageType.values()){
			lookup.put(td.class_id + ":" + td.id, td);
		}
	}
	public int getId(){ return id; }
	public int getClassId(){ return class_id; }
	public static M3UAMessageType get(int id, int _class_id){ return lookup.get(_class_id + ":" + id); }
	private M3UAMessageType(int _class_id, int _id){ id = _id; class_id = _class_id; }	
}
