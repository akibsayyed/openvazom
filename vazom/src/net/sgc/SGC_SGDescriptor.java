package net.sgc;


import java.util.concurrent.ConcurrentLinkedQueue;

import net.config.SGNConfigData;
import net.sctp.SSCTPDescriptor;
import net.sctp.SSctp;
import net.stats.StatsManager;
import net.vstp.MessageDescriptor;
import net.vstp.VSTP;

import org.apache.log4j.Logger;

public class SGC_SGDescriptor {
	public static Logger logger=Logger.getLogger(SGC_SGDescriptor.class);
	public String id;
	public SGCType type;
	public String address;
	public int port;
	private boolean stopping;
	public int sctp_id = -1;
	private Reader reader_r;
	private Writer writer_r;
	private Thread reader_t;
	private Thread writer_t;
	public boolean active;
	public ConcurrentLinkedQueue<MessageDescriptor> private_out_queue;

	
	private class Reader implements Runnable{
		MessageDescriptor md = null;
		SSCTPDescriptor sd = null;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				sd = SSctp.receive(sctp_id);
				if(sd != null){
					if(sd.payload != null){
						//System.out.println(new String(sd.payload));
						md = VSTP.decode(sd.payload);
						SGCManager.in_offer(md);
						
					}
				}else{
					stopping = true;
					switch(type){
						case CLIENT: 
							logger.info("Removing SGC Client Connection [" + id + "]!");
							SGCManager.deactivateNode_client(id); 
							break;
						case HOST: 
							logger.info("Removing SGC Host Connection [" + sctp_id + "]!");
							SGCManager.removeNode_host(sctp_id); 
							break;
					}
				}

			}	
			logger.info("Ending...");
		}
	}
	
	private class Writer implements Runnable{
		MessageDescriptor md = null;
		MessageDescriptor md_p = null;
		int res;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				// mutual queue
				md = SGCManager.out_poll();
				if(md != null){
					// set destination id
					md.header.destination_id = id;
					md.header.source_id = SGNConfigData.sgn_id;
					// encode and send
					res = SSctp.send(sctp_id, md.encode(), 0);
					if(res == -1){
						// send back to queue
						SGCManager.out_offer(md);
						// remove connection
						stopping = true;
						switch(type){
							case CLIENT: 
								logger.info("Removing SGC Client Connection [" + id + "]!");
								SGCManager.deactivateNode_client(id); 
								break;
							case HOST: 
								logger.info("Removing SGC Host Connection [" + sctp_id + "]!");
								SGCManager.removeNode_host(sctp_id); 
								break;
						}
						// stats
						StatsManager.VSTP_STATS.VSTP_SGC_CONNECTION_LOST++;

					}
				}
				// private queue
				md_p = private_out_queue.poll();
				if(md_p != null){
					// set destination id
					md_p.header.destination_id = id;
					md_p.header.source_id = SGNConfigData.sgn_id;
					// encode and send
					res = SSctp.send(sctp_id, md_p.encode(), 0);
					if(res == -1){
						stopping = true;
						switch(type){
							case CLIENT: 
								logger.info("Removing SGC Client Connection [" + id + "]!");
								SGCManager.deactivateNode_client(id); 
								break;
							case HOST: 
								logger.info("Removing SGC Host Connection [" + sctp_id + "]!");
								SGCManager.removeNode_host(sctp_id); 
								break;
						}
						// stats
						StatsManager.VSTP_STATS.VSTP_SGC_CONNECTION_LOST++;
						
					}	
				}
				
				if(md == null && md_p == null){
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
				
			}	
			logger.info("Ending...");
			
		}
	}
	
	public void start(){
		active = true;
		stopping = false;
		logger.info("Starting SGC Connection id = [" + id + "]!");
		reader_r = new Reader();
		reader_t = new Thread(reader_r, "SGC_SG_READER_" + (type == SGCType.HOST ? "HOST_" : "CLIENT_") + id);
		
		writer_r = new Writer();
		writer_t = new Thread(writer_r, "SGC_SG_WRITER_" + (type == SGCType.HOST ? "HOST_" : "CLIENT_") + id);
		
		reader_t.start();
		writer_t.start();
		
		
	}
	public void stop(){
		stopping = true;
	}

	public SGC_SGDescriptor(int _sctp_id){
		private_out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		sctp_id = _sctp_id;
		type = SGCType.HOST;
	}
	public SGC_SGDescriptor(String _id, String _address, int _port){
		private_out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		id = _id;
		address = _address;
		port = _port;
		type = SGCType.CLIENT;

	}
	
}
