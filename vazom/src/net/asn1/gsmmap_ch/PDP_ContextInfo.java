package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PDP_ContextInfo extends SEQUENCE{

	public ContextId get_pdp_ContextIdentifier(){
		return (ContextId)elements.get(0).data;
	}
	public ContextId new_pdp_ContextIdentifier(){
		return new ContextId();
	}

	public NULL get_pdp_ContextActive(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_pdp_ContextActive(){
		return new NULL();
	}

	public PDP_Type get_pdp_Type(){
		return (PDP_Type)elements.get(2).data;
	}
	public PDP_Type new_pdp_Type(){
		return new PDP_Type();
	}

	public PDP_Address get_pdp_Address(){
		return (PDP_Address)elements.get(3).data;
	}
	public PDP_Address new_pdp_Address(){
		return new PDP_Address();
	}

	public APN get_apn_Subscribed(){
		return (APN)elements.get(4).data;
	}
	public APN new_apn_Subscribed(){
		return new APN();
	}

	public APN get_apn_InUse(){
		return (APN)elements.get(5).data;
	}
	public APN new_apn_InUse(){
		return new APN();
	}

	public NSAPI get_nsapi(){
		return (NSAPI)elements.get(6).data;
	}
	public NSAPI new_nsapi(){
		return new NSAPI();
	}

	public TransactionId get_transactionId(){
		return (TransactionId)elements.get(7).data;
	}
	public TransactionId new_transactionId(){
		return new TransactionId();
	}

	public TEID get_teid_ForGnAndGp(){
		return (TEID)elements.get(8).data;
	}
	public TEID new_teid_ForGnAndGp(){
		return new TEID();
	}

	public TEID get_teid_ForIu(){
		return (TEID)elements.get(9).data;
	}
	public TEID new_teid_ForIu(){
		return new TEID();
	}

	public GSN_Address get_ggsn_Address(){
		return (GSN_Address)elements.get(10).data;
	}
	public GSN_Address new_ggsn_Address(){
		return new GSN_Address();
	}

	public Ext_QoS_Subscribed get_qos_Subscribed(){
		return (Ext_QoS_Subscribed)elements.get(11).data;
	}
	public Ext_QoS_Subscribed new_qos_Subscribed(){
		return new Ext_QoS_Subscribed();
	}

	public Ext_QoS_Subscribed get_qos_Requested(){
		return (Ext_QoS_Subscribed)elements.get(12).data;
	}
	public Ext_QoS_Subscribed new_qos_Requested(){
		return new Ext_QoS_Subscribed();
	}

	public Ext_QoS_Subscribed get_qos_Negotiated(){
		return (Ext_QoS_Subscribed)elements.get(13).data;
	}
	public Ext_QoS_Subscribed new_qos_Negotiated(){
		return new Ext_QoS_Subscribed();
	}

	public GPRSChargingID get_chargingId(){
		return (GPRSChargingID)elements.get(14).data;
	}
	public GPRSChargingID new_chargingId(){
		return new GPRSChargingID();
	}

	public ChargingCharacteristics get_chargingCharacteristics(){
		return (ChargingCharacteristics)elements.get(15).data;
	}
	public ChargingCharacteristics new_chargingCharacteristics(){
		return new ChargingCharacteristics();
	}

	public GSN_Address get_rnc_Address(){
		return (GSN_Address)elements.get(16).data;
	}
	public GSN_Address new_rnc_Address(){
		return new GSN_Address();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(17).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public Ext2_QoS_Subscribed get_qos2_Subscribed(){
		return (Ext2_QoS_Subscribed)elements.get(18).data;
	}
	public Ext2_QoS_Subscribed new_qos2_Subscribed(){
		return new Ext2_QoS_Subscribed();
	}

	public Ext2_QoS_Subscribed get_qos2_Requested(){
		return (Ext2_QoS_Subscribed)elements.get(19).data;
	}
	public Ext2_QoS_Subscribed new_qos2_Requested(){
		return new Ext2_QoS_Subscribed();
	}

	public Ext2_QoS_Subscribed get_qos2_Negotiated(){
		return (Ext2_QoS_Subscribed)elements.get(20).data;
	}
	public Ext2_QoS_Subscribed new_qos2_Negotiated(){
		return new Ext2_QoS_Subscribed();
	}

	public Ext3_QoS_Subscribed get_qos3_Subscribed(){
		return (Ext3_QoS_Subscribed)elements.get(21).data;
	}
	public Ext3_QoS_Subscribed new_qos3_Subscribed(){
		return new Ext3_QoS_Subscribed();
	}

	public Ext3_QoS_Subscribed get_qos3_Requested(){
		return (Ext3_QoS_Subscribed)elements.get(22).data;
	}
	public Ext3_QoS_Subscribed new_qos3_Requested(){
		return new Ext3_QoS_Subscribed();
	}

	public Ext3_QoS_Subscribed get_qos3_Negotiated(){
		return (Ext3_QoS_Subscribed)elements.get(23).data;
	}
	public Ext3_QoS_Subscribed new_qos3_Negotiated(){
		return new Ext3_QoS_Subscribed();
	}

	public Ext4_QoS_Subscribed get_qos4_Subscribed(){
		return (Ext4_QoS_Subscribed)elements.get(24).data;
	}
	public Ext4_QoS_Subscribed new_qos4_Subscribed(){
		return new Ext4_QoS_Subscribed();
	}

	public Ext4_QoS_Subscribed get_qos4_Requested(){
		return (Ext4_QoS_Subscribed)elements.get(25).data;
	}
	public Ext4_QoS_Subscribed new_qos4_Requested(){
		return new Ext4_QoS_Subscribed();
	}

	public Ext4_QoS_Subscribed get_qos4_Negotiated(){
		return (Ext4_QoS_Subscribed)elements.get(26).data;
	}
	public Ext4_QoS_Subscribed new_qos4_Negotiated(){
		return new Ext4_QoS_Subscribed();
	}

	public Ext_PDP_Type get_ext_pdp_Type(){
		return (Ext_PDP_Type)elements.get(27).data;
	}
	public Ext_PDP_Type new_ext_pdp_Type(){
		return new Ext_PDP_Type();
	}

	public PDP_Address get_ext_pdp_Address(){
		return (PDP_Address)elements.get(28).data;
	}
	public PDP_Address new_ext_pdp_Address(){
		return new PDP_Address();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_pdp_ContextIdentifier(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pdp_ContextActive(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_pdp_Type(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pdp_Address(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_apn_Subscribed(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_apn_InUse(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nsapi(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionId(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_teid_ForGnAndGp(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_teid_ForIu(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ggsn_Address(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_qos_Subscribed(); }});
		elements.add(new ElementDescriptor(12, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_qos_Requested(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_qos_Negotiated(); }});
		elements.add(new ElementDescriptor(14, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_chargingId(); }});
		elements.add(new ElementDescriptor(15, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_chargingCharacteristics(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_rnc_Address(); }});
		elements.add(new ElementDescriptor(17, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos2_Subscribed(); }});
		elements.add(new ElementDescriptor(19, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos2_Requested(); }});
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos2_Negotiated(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos3_Subscribed(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos3_Requested(); }});
		elements.add(new ElementDescriptor(23, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos3_Negotiated(); }});
		elements.add(new ElementDescriptor(25, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos4_Subscribed(); }});
		elements.add(new ElementDescriptor(26, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos4_Requested(); }});
		elements.add(new ElementDescriptor(27, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_qos4_Negotiated(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ext_pdp_Type(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ext_pdp_Address(); }});
	}
	public PDP_ContextInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
