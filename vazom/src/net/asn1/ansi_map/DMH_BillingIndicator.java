package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DMH_BillingIndicator extends ENUMERATED{

	public static final int _unspecified = 0;
	public DMH_BillingIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
