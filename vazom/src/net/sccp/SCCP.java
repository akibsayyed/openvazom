package net.sccp;

import net.sccp.messages.AK_DataAcknowledgement;
import net.sccp.messages.CC_ConnectionConfirm;
import net.sccp.messages.CREF_ConnectionRefused;
import net.sccp.messages.CR_ConnectionRequest;
import net.sccp.messages.DT1_DataForm1;
import net.sccp.messages.DT2_DataForm2;
import net.sccp.messages.EA_ExpeditedDataAcknowledgment;
import net.sccp.messages.ED_ExpeditedData;
import net.sccp.messages.ERR_PduError;
import net.sccp.messages.IT_InactivityTest;
import net.sccp.messages.LUDTS_LongUnitdataService;
import net.sccp.messages.LUDT_LongUnitdata;
import net.sccp.messages.MessageBase;
import net.sccp.messages.RLC_ReleaseComplete;
import net.sccp.messages.RLSD_Released;
import net.sccp.messages.RSC_ResetConfirmation;
import net.sccp.messages.RSR_ResetRequest;
import net.sccp.messages.UDTS_UnitDataService;
import net.sccp.messages.UDT_UnitData;
import net.sccp.messages.XUDTS_ExtendedUnitdataService;
import net.sccp.messages.XUDT_ExtendedUnitdata;

public abstract class SCCP {
	
	
	public static MessageType decodeType(byte[] data){
		return MessageType.get(data[0] & 0xFF);
		
	}
	
	public static MessageBase prepareNew(MessageType type){
		MessageBase res = null;
		switch(type){
			case UDT_UNITDATA: res = new UDT_UnitData(); break;
			case UDTS_UNITDATA_SERVICE: res = new UDTS_UnitDataService(); break;
			// TODO 
			//other types
		}
		if(res != null) res.initNew();
		return res;
	}
	
	public static MessageBase decode(byte[] data){
		MessageBase res = null;
		MessageType mt = MessageType.get(data[0] & 0xFF);
		switch(mt){
			case UDT_UNITDATA: res = new UDT_UnitData(); break;
			case AK_DATA_ACKNOWLEDGEMENT: res = new AK_DataAcknowledgement(); break;
			case CC_CONNECTION_CONFIRM: res = new CC_ConnectionConfirm(); break;
			case CR_CONNECTION_REQUEST: res = new CR_ConnectionRequest(); break;
			case CREF_CONNECTION_REFUSED: res = new CREF_ConnectionRefused(); break;
			case DT1_DATA_FORM_1: res = new DT1_DataForm1(); break;
			case DT2_DATA_FORM_2: res = new DT2_DataForm2(); break;
			case EA_EXPEDITED_DATA_ACKNOWLEDGMENT: res = new EA_ExpeditedDataAcknowledgment(); break;
			case ED_EXPEDITED_DATA: res = new ED_ExpeditedData(); break;
			case ERR_PROTOCOL_DATA_UNIT_ERROR: res = new ERR_PduError(); break;
			case IT_INACTIVITY_TEST: res = new IT_InactivityTest(); break;
			case LUDT_LONG_UNIDATA: res = new LUDT_LongUnitdata(); break;
			case LUDTS_LONG_UNIDATA_SERVICE: res = new LUDTS_LongUnitdataService(); break;
			case RLC_RELEASE_COMPLETE: res = new RLC_ReleaseComplete(); break;
			case RLSD_RELEASED: res = new RLSD_Released(); break;
			case RSC_RESET_CONFIRM: res = new RSC_ResetConfirmation(); break;
			case RSR_RESET_REQUEST: res = new RSR_ResetRequest(); break;
			case UDTS_UNITDATA_SERVICE: res = new UDTS_UnitDataService(); break;
			case XUDT_EXTENDED_UNITDATA: res = new XUDT_ExtendedUnitdata(); break;
			case XUDTS_EXTENDED_UNIDATA_SERVICE: res = new XUDTS_ExtendedUnitdataService(); break;
			
		
		}
		if(res != null) res.init(data);
		
		return res;
		
	}

	
	

}
