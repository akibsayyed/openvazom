package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SeizeResource extends SET{

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(0).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public PrivateSpecializedResource get_privateSpecializedResource(){
		return (PrivateSpecializedResource)elements.get(1).data;
	}
	public PrivateSpecializedResource new_privateSpecializedResource(){
		return new PrivateSpecializedResource();
	}

	public SpecializedResource get_specializedResource(){
		return (SpecializedResource)elements.get(2).data;
	}
	public SpecializedResource new_specializedResource(){
		return new SpecializedResource();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(265, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_privateSpecializedResource(); }});
		elements.add(new ElementDescriptor(274, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_specializedResource(); }});
	}
	public SeizeResource(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
