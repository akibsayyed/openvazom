package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReportSM_DeliveryStatusArg extends SEQUENCE{

	public ISDN_AddressString get_msisdn(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_msisdn(){
		return new ISDN_AddressString();
	}

	public AddressString get_serviceCentreAddress(){
		return (AddressString)elements.get(1).data;
	}
	public AddressString new_serviceCentreAddress(){
		return new AddressString();
	}

	public SM_DeliveryOutcome get_sm_DeliveryOutcome(){
		return (SM_DeliveryOutcome)elements.get(2).data;
	}
	public SM_DeliveryOutcome new_sm_DeliveryOutcome(){
		return new SM_DeliveryOutcome();
	}

	public AbsentSubscriberDiagnosticSM get_absentSubscriberDiagnosticSM(){
		return (AbsentSubscriberDiagnosticSM)elements.get(3).data;
	}
	public AbsentSubscriberDiagnosticSM new_absentSubscriberDiagnosticSM(){
		return new AbsentSubscriberDiagnosticSM();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(4).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NULL get_gprsSupportIndicator(){
		return (NULL)elements.get(5).data;
	}
	public NULL new_gprsSupportIndicator(){
		return new NULL();
	}

	public NULL get_deliveryOutcomeIndicator(){
		return (NULL)elements.get(6).data;
	}
	public NULL new_deliveryOutcomeIndicator(){
		return new NULL();
	}

	public SM_DeliveryOutcome get_additionalSM_DeliveryOutcome(){
		return (SM_DeliveryOutcome)elements.get(7).data;
	}
	public SM_DeliveryOutcome new_additionalSM_DeliveryOutcome(){
		return new SM_DeliveryOutcome();
	}

	public AbsentSubscriberDiagnosticSM get_additionalAbsentSubscriberDiagnosticSM(){
		return (AbsentSubscriberDiagnosticSM)elements.get(8).data;
	}
	public AbsentSubscriberDiagnosticSM new_additionalAbsentSubscriberDiagnosticSM(){
		return new AbsentSubscriberDiagnosticSM();
	}

	public NULL get_ip_sm_gw_Indicator(){
		return (NULL)elements.get(9).data;
	}
	public NULL new_ip_sm_gw_Indicator(){
		return new NULL();
	}

	public SM_DeliveryOutcome get_ip_sm_gw_sm_deliveryOutcome(){
		return (SM_DeliveryOutcome)elements.get(10).data;
	}
	public SM_DeliveryOutcome new_ip_sm_gw_sm_deliveryOutcome(){
		return new SM_DeliveryOutcome();
	}

	public AbsentSubscriberDiagnosticSM get_ip_sm_gw_absentSubscriberDiagnosticSM(){
		return (AbsentSubscriberDiagnosticSM)elements.get(11).data;
	}
	public AbsentSubscriberDiagnosticSM new_ip_sm_gw_absentSubscriberDiagnosticSM(){
		return new AbsentSubscriberDiagnosticSM();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_msisdn(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_serviceCentreAddress(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sm_DeliveryOutcome(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_absentSubscriberDiagnosticSM(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_gprsSupportIndicator(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_deliveryOutcomeIndicator(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additionalSM_DeliveryOutcome(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additionalAbsentSubscriberDiagnosticSM(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ip_sm_gw_Indicator(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ip_sm_gw_sm_deliveryOutcome(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_ip_sm_gw_absentSubscriberDiagnosticSM(); }});
	}
	public ReportSM_DeliveryStatusArg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
