package net.vstp;

import java.util.ArrayList;

import net.hplmnr.HPLMNRPacket;
import net.routing.CallbackBase;
import net.routing.RoutingConnectionType;
import net.sctp.SSCTPDescriptor;
import net.smpp_conn.SMPPPacket;

public class VSTP_SGF_CorrelationPacket {
	public RoutingConnectionType type;
	public long timestamp;
	// M3UA via SCTP
	public HPLMNRPacket sri_packet;
	public SSCTPDescriptor sd;
	// SMPP
	public SMPPPacket smpp_packet;
	public byte[] smpp_packet_data;
	public ArrayList<Object> extra_params;
	public boolean callback_required;
	public CallbackBase callback;
	
	// MTP3 via E1
	// TODO
	

	public VSTP_SGF_CorrelationPacket(RoutingConnectionType _type){
		type = _type;
	}

	
	public VSTP_SGF_CorrelationPacket(){
		// default
		type = RoutingConnectionType.M3UA;
		extra_params = new ArrayList<Object>();
	}
}
