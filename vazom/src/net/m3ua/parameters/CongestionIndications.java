package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class CongestionIndications extends M3UAParameter {
	public int level;
	public CongestionIndications(){
		type = M3UAParameterType.CONGESTION_INDICATIONS;
	}
	
	public void init(byte[] data) {
		value = data;
		level = data[3] & 0xFF;
	}



}
