package net.smpp;

import java.util.Calendar;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

public class TimeFormat {

	public static SMPPTimeFormat toTimestamp(String tfield){
		try{
			SMPPTimeFormat res = new SMPPTimeFormat();
			res.formatted = tfield;
			//YYMMDDhhmmsstnnp
			Pattern pt = Pattern.compile("(..)(..)(..)(..)(..)(..)(.)?(..)?(.)?.*", Pattern.CASE_INSENSITIVE);
			//11 09 01 10 00 00 0 00 +
			Matcher mt = pt.matcher(tfield);
			if(mt.find()){
				//for(int i = 0; i<=mt.groupCount(); i++) System.out.println(mt.group(i));
				Calendar cal = Calendar.getInstance();
				
				cal.set(Calendar.YEAR, 2000 + Integer.parseInt(mt.group(1)));
				cal.set(Calendar.MONTH, Integer.parseInt(mt.group(2)) - 1);
				cal.set(Calendar.DAY_OF_MONTH, Integer.parseInt(mt.group(3)));
				cal.set(Calendar.HOUR_OF_DAY, Integer.parseInt(mt.group(4)));
				cal.set(Calendar.MINUTE, Integer.parseInt(mt.group(5)));
				cal.set(Calendar.SECOND, Integer.parseInt(mt.group(6)));
				if(mt.group(7) != null){
					cal.set(Calendar.MILLISECOND, Integer.parseInt(mt.group(7)) * 100);
				}else cal.set(Calendar.MILLISECOND, 0);
				// nn missing
				// +/-/R missing
				
				if(mt.group(9) != null){
					if(mt.group(9).equalsIgnoreCase("R")){
						res.is_relative = true;
						res.ts = cal.getTimeInMillis() + System.currentTimeMillis();
					}else{
						res.ts = cal.getTimeInMillis();
					}
				}else{
					res.ts = cal.getTimeInMillis();
				}
				
				res.ts = res.ts / 1000;
				
			}
			return res;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}
}
