package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ModificationRequestType extends ModificationRequest{

	public ModificationRequestType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 262;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
