package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.CalledPA;
import net.sccp.parameters.CallingPA;
import net.sccp.parameters.Eoop;
import net.sccp.parameters.HopCounter;
import net.sccp.parameters.Importance;
import net.sccp.parameters.ParamType;
import net.sccp.parameters.ProtocolClass;
import net.sccp.parameters.Segmentation;

public class XUDT_ExtendedUnitdata extends MessageBase {
	public ProtocolClass protocolClass;
	public HopCounter hopCounter;
	public CalledPA calledPartyAddress;
	public CallingPA callingPartyAddress;
	public byte[] data;
	// optional
	public Segmentation segmentation;
	public Importance importance;
	public Eoop eoop;
	
	public XUDT_ExtendedUnitdata(){
		super();
		type = MessageType.XUDT_EXTENDED_UNITDATA;
		pointers = new Pointer[4];
		
	}
	
	public void init(byte[] _data) {
		boolean opt_done = false;
		ParamType pt = null;
		int l = 0;
		
		byte_pos++;

		// Fixed mandatory
		protocolClass = new ProtocolClass();
		protocolClass.init(new byte[]{_data[byte_pos++]});

		// Fixed mandatory
		hopCounter = new HopCounter();
		hopCounter.init(new byte[]{_data[byte_pos++]});
		
		// pointers to mandatory variable parts
		pointers[0] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		pointers[1] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		pointers[2] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		pointers[3] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		
		// First mandatory variable part
		calledPartyAddress = new CalledPA();
		calledPartyAddress.init(Arrays.copyOfRange(_data, pointers[0].position + 1, pointers[0].position + 1 + (_data[pointers[0].position] & 0xFF)));
		
		// Second mandatory variable part
		callingPartyAddress = new CallingPA();
		callingPartyAddress.init(Arrays.copyOfRange(_data, pointers[1].position + 1, pointers[1].position + 1 + (_data[pointers[1].position] & 0xFF)));
		
		// Third mandatory variable part
		data = Arrays.copyOfRange(_data, pointers[2].position + 1, pointers[2].position + 1 + (_data[pointers[2].position] & 0xFF));

		// Optional part
		// if equals 0(zero) no optional parameters present
		if(pointers[3].value != 0x00){
			// position = first optional parameter
			byte_pos = pointers[1].position;
			while(!opt_done){
				pt = ParamType.get(_data[byte_pos++] & 0xFF);
				// End of optional parameters has no length, only one byte which is 0x00
				if(pt != ParamType.END_OF_OPTIONAL_PARAMETERS) l = _data[byte_pos++] & 0xFF;
				else l = 0;
				switch(pt){
					case SEGMENTATION:
						segmentation = new Segmentation();
						segmentation.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case IMPORTANCE:
						importance = new Importance();
						importance.init(Arrays.copyOfRange(_data, byte_pos, l + byte_pos));
						break;
					case END_OF_OPTIONAL_PARAMETERS:
						eoop = new Eoop();
						opt_done = true;
						break;
					
				}
				byte_pos += l;
			}
		
		}
	}

}
