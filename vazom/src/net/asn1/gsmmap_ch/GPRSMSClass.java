package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class GPRSMSClass extends SEQUENCE{

	public MSNetworkCapability get_mSNetworkCapability(){
		return (MSNetworkCapability)elements.get(0).data;
	}
	public MSNetworkCapability new_mSNetworkCapability(){
		return new MSNetworkCapability();
	}

	public MSRadioAccessCapability get_mSRadioAccessCapability(){
		return (MSRadioAccessCapability)elements.get(1).data;
	}
	public MSRadioAccessCapability new_mSRadioAccessCapability(){
		return new MSRadioAccessCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mSNetworkCapability(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSRadioAccessCapability(); }});
	}
	public GPRSMSClass(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
