package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationFailureReportRes extends SET{

	public AuthenticationAlgorithmVersion get_authenticationAlgorithmVersion(){
		return (AuthenticationAlgorithmVersion)elements.get(0).data;
	}
	public AuthenticationAlgorithmVersion new_authenticationAlgorithmVersion(){
		return new AuthenticationAlgorithmVersion();
	}

	public AuthenticationResponseUniqueChallenge get_authenticationResponseUniqueChallenge(){
		return (AuthenticationResponseUniqueChallenge)elements.get(1).data;
	}
	public AuthenticationResponseUniqueChallenge new_authenticationResponseUniqueChallenge(){
		return new AuthenticationResponseUniqueChallenge();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(2).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(3).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public DenyAccess get_denyAccess(){
		return (DenyAccess)elements.get(4).data;
	}
	public DenyAccess new_denyAccess(){
		return new DenyAccess();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(5).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public RandomVariableSSD get_randomVariableSSD(){
		return (RandomVariableSSD)elements.get(6).data;
	}
	public RandomVariableSSD new_randomVariableSSD(){
		return new RandomVariableSSD();
	}

	public RandomVariableUniqueChallenge get_randomVariableUniqueChallenge(){
		return (RandomVariableUniqueChallenge)elements.get(7).data;
	}
	public RandomVariableUniqueChallenge new_randomVariableUniqueChallenge(){
		return new RandomVariableUniqueChallenge();
	}

	public SharedSecretData get_sharedSecretData(){
		return (SharedSecretData)elements.get(8).data;
	}
	public SharedSecretData new_sharedSecretData(){
		return new SharedSecretData();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(9).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public SSDNotShared get_ssdnotShared(){
		return (SSDNotShared)elements.get(10).data;
	}
	public SSDNotShared new_ssdnotShared(){
		return new SSDNotShared();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(11).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public UpdateCount get_updateCount(){
		return (UpdateCount)elements.get(12).data;
	}
	public UpdateCount new_updateCount(){
		return new UpdateCount();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(77, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationAlgorithmVersion(); }});
		elements.add(new ElementDescriptor(37, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponseUniqueChallenge(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_denyAccess(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(42, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableSSD(); }});
		elements.add(new ElementDescriptor(43, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableUniqueChallenge(); }});
		elements.add(new ElementDescriptor(46, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sharedSecretData(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(52, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ssdnotShared(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(51, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_updateCount(); }});
	}
	public AuthenticationFailureReportRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
