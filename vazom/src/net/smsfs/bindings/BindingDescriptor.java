package net.smsfs.bindings;

import net.hplmnr.HPLMNRPacket;
import net.smsfs.SmsfsPacket;


public abstract class BindingDescriptor {
	public int index;
	public int value;
	
	public BindingDescriptor(int _index){
		index = _index;
		
	}
	public void modify(HPLMNRPacket sp, String new_value){
	
	}

	public abstract void modify(SmsfsPacket fp, String new_value);
}
