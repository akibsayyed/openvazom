package net.isup.parameters;

import net.isup.ParameterType;

public class OptionalForwardCallIndicators extends ParameterBase {
	public OptionalForwardCallIndicators(){
		type = ParameterType.OPTIONAL_FORWARD_CALL_INDICATORS;
	}
}
