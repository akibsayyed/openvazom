package net.isup.parameters.ei;

import java.util.HashMap;


public enum EventInformationType {
	ALERTING(0x01),
	PROGRESS(0x02),
	IN_BAND_INFO_OR_APPROPRIATE_PATTERN_AVAILABLE(0x03),
	CALL_FORWARDED_ON_BUSY(0x04),
	CALL_FORWARDED_ON_NO_REPLY(0x05),
	CALL_FORWARDED_UNCONDITIONAL(0x06); 



	
	
	
	
	private int id;
	private static final HashMap<Integer, EventInformationType> lookup = new HashMap<Integer, EventInformationType>();
	static{
		for(EventInformationType td :EventInformationType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static EventInformationType get(int id){ return lookup.get(id); }
	private EventInformationType(int _id){ id = _id; }			
}
