// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/dfranusic/cli.g 2011-04-30 20:02:18

package net.cli;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;

public class cliLexer extends Lexer {
    public static final int NEQUAL=14;
    public static final int LT=19;
    public static final int PERCENT=17;
    public static final int CLI_METHOD=32;
    public static final int CLI_PARENT=34;
    public static final int HEX_P=18;
    public static final int REGEX_BLOCK=27;
    public static final int GTE=22;
    public static final int CLI_PARAM=36;
    public static final int L_CR_B=8;
    public static final int SQUOTE=26;
    public static final int MINUS=24;
    public static final int AND=25;
    public static final int EOF=-1;
    public static final int R_CR_B=9;
    public static final int LTE=21;
    public static final int ANNT=10;
    public static final int R_PAREN=5;
    public static final int ASTERISK=28;
    public static final int CLI_PARAMS=35;
    public static final int COLON=15;
    public static final int WORD=38;
    public static final int WS=39;
    public static final int L_SQ_B=7;
    public static final int COMMA=29;
    public static final int R_SQ_B=6;
    public static final int EQUAL=11;
    public static final int OR=13;
    public static final int L_PAREN=4;
    public static final int SL_COMMENT=40;
    public static final int ASSIGN=12;
    public static final int CLI_FINAL=30;
    public static final int GT=20;
    public static final int PLUS=23;
    public static final int CLI_DESC=37;
    public static final int STMTSEP=16;
    public static final int CLI_DEPTH=33;
    public static final int CLI_COMPLEX=31;

    // delegates
    // delegators

    public cliLexer() {;} 
    public cliLexer(CharStream input) {
        this(input, new RecognizerSharedState());
    }
    public cliLexer(CharStream input, RecognizerSharedState state) {
        super(input,state);

    }
    public String getGrammarFileName() { return "/home/dfranusic/cli.g"; }

    // $ANTLR start "L_PAREN"
    public final void mL_PAREN() throws RecognitionException {
        try {
            int _type = L_PAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:11:9: ( '(' )
            // /home/dfranusic/cli.g:11:11: '('
            {
            match('('); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "L_PAREN"

    // $ANTLR start "R_PAREN"
    public final void mR_PAREN() throws RecognitionException {
        try {
            int _type = R_PAREN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:12:9: ( ')' )
            // /home/dfranusic/cli.g:12:11: ')'
            {
            match(')'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "R_PAREN"

    // $ANTLR start "R_SQ_B"
    public final void mR_SQ_B() throws RecognitionException {
        try {
            int _type = R_SQ_B;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:13:8: ( ']' )
            // /home/dfranusic/cli.g:13:10: ']'
            {
            match(']'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "R_SQ_B"

    // $ANTLR start "L_SQ_B"
    public final void mL_SQ_B() throws RecognitionException {
        try {
            int _type = L_SQ_B;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:14:8: ( '[' )
            // /home/dfranusic/cli.g:14:10: '['
            {
            match('['); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "L_SQ_B"

    // $ANTLR start "L_CR_B"
    public final void mL_CR_B() throws RecognitionException {
        try {
            int _type = L_CR_B;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:15:8: ( '{' )
            // /home/dfranusic/cli.g:15:10: '{'
            {
            match('{'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "L_CR_B"

    // $ANTLR start "R_CR_B"
    public final void mR_CR_B() throws RecognitionException {
        try {
            int _type = R_CR_B;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:16:8: ( '}' )
            // /home/dfranusic/cli.g:16:10: '}'
            {
            match('}'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "R_CR_B"

    // $ANTLR start "ANNT"
    public final void mANNT() throws RecognitionException {
        try {
            int _type = ANNT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:17:6: ( '@' )
            // /home/dfranusic/cli.g:17:8: '@'
            {
            match('@'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ANNT"

    // $ANTLR start "EQUAL"
    public final void mEQUAL() throws RecognitionException {
        try {
            int _type = EQUAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:18:7: ( '==' )
            // /home/dfranusic/cli.g:18:9: '=='
            {
            match("=="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "EQUAL"

    // $ANTLR start "ASSIGN"
    public final void mASSIGN() throws RecognitionException {
        try {
            int _type = ASSIGN;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:19:8: ( '=' )
            // /home/dfranusic/cli.g:19:10: '='
            {
            match('='); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ASSIGN"

    // $ANTLR start "OR"
    public final void mOR() throws RecognitionException {
        try {
            int _type = OR;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:20:4: ( '||' )
            // /home/dfranusic/cli.g:20:6: '||'
            {
            match("||"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "OR"

    // $ANTLR start "NEQUAL"
    public final void mNEQUAL() throws RecognitionException {
        try {
            int _type = NEQUAL;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:21:8: ( '!=' )
            // /home/dfranusic/cli.g:21:10: '!='
            {
            match("!="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "NEQUAL"

    // $ANTLR start "COLON"
    public final void mCOLON() throws RecognitionException {
        try {
            int _type = COLON;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:22:7: ( ':' )
            // /home/dfranusic/cli.g:22:9: ':'
            {
            match(':'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COLON"

    // $ANTLR start "STMTSEP"
    public final void mSTMTSEP() throws RecognitionException {
        try {
            int _type = STMTSEP;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:23:9: ( ';' )
            // /home/dfranusic/cli.g:23:11: ';'
            {
            match(';'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "STMTSEP"

    // $ANTLR start "PERCENT"
    public final void mPERCENT() throws RecognitionException {
        try {
            int _type = PERCENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:24:9: ( '%' )
            // /home/dfranusic/cli.g:24:11: '%'
            {
            match('%'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PERCENT"

    // $ANTLR start "HEX_P"
    public final void mHEX_P() throws RecognitionException {
        try {
            int _type = HEX_P;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:25:7: ( '0x' )
            // /home/dfranusic/cli.g:25:9: '0x'
            {
            match("0x"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "HEX_P"

    // $ANTLR start "LT"
    public final void mLT() throws RecognitionException {
        try {
            int _type = LT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:26:4: ( '<' )
            // /home/dfranusic/cli.g:26:6: '<'
            {
            match('<'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LT"

    // $ANTLR start "GT"
    public final void mGT() throws RecognitionException {
        try {
            int _type = GT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:27:4: ( '>' )
            // /home/dfranusic/cli.g:27:6: '>'
            {
            match('>'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GT"

    // $ANTLR start "LTE"
    public final void mLTE() throws RecognitionException {
        try {
            int _type = LTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:28:5: ( '<=' )
            // /home/dfranusic/cli.g:28:7: '<='
            {
            match("<="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "LTE"

    // $ANTLR start "GTE"
    public final void mGTE() throws RecognitionException {
        try {
            int _type = GTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:29:5: ( '>=' )
            // /home/dfranusic/cli.g:29:7: '>='
            {
            match(">="); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "GTE"

    // $ANTLR start "PLUS"
    public final void mPLUS() throws RecognitionException {
        try {
            int _type = PLUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:30:6: ( '+' )
            // /home/dfranusic/cli.g:30:8: '+'
            {
            match('+'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "PLUS"

    // $ANTLR start "MINUS"
    public final void mMINUS() throws RecognitionException {
        try {
            int _type = MINUS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:31:7: ( '-' )
            // /home/dfranusic/cli.g:31:9: '-'
            {
            match('-'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "MINUS"

    // $ANTLR start "AND"
    public final void mAND() throws RecognitionException {
        try {
            int _type = AND;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:32:5: ( '&&' )
            // /home/dfranusic/cli.g:32:7: '&&'
            {
            match("&&"); 


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "AND"

    // $ANTLR start "SQUOTE"
    public final void mSQUOTE() throws RecognitionException {
        try {
            int _type = SQUOTE;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:33:8: ( '\\'' )
            // /home/dfranusic/cli.g:33:10: '\\''
            {
            match('\''); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SQUOTE"

    // $ANTLR start "REGEX_BLOCK"
    public final void mREGEX_BLOCK() throws RecognitionException {
        try {
            int _type = REGEX_BLOCK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:34:13: ( '/' )
            // /home/dfranusic/cli.g:34:15: '/'
            {
            match('/'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "REGEX_BLOCK"

    // $ANTLR start "ASTERISK"
    public final void mASTERISK() throws RecognitionException {
        try {
            int _type = ASTERISK;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:35:10: ( '*' )
            // /home/dfranusic/cli.g:35:12: '*'
            {
            match('*'); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "ASTERISK"

    // $ANTLR start "COMMA"
    public final void mCOMMA() throws RecognitionException {
        try {
            int _type = COMMA;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:36:7: ( ',' )
            // /home/dfranusic/cli.g:36:9: ','
            {
            match(','); 

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "COMMA"

    // $ANTLR start "SL_COMMENT"
    public final void mSL_COMMENT() throws RecognitionException {
        try {
            int _type = SL_COMMENT;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:100:3: ( '//' ( options {greedy=false; } : . )* '\\n' )
            // /home/dfranusic/cli.g:100:5: '//' ( options {greedy=false; } : . )* '\\n'
            {
            match("//"); 

            // /home/dfranusic/cli.g:100:10: ( options {greedy=false; } : . )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0=='\n') ) {
                    alt1=2;
                }
                else if ( ((LA1_0>='\u0000' && LA1_0<='\t')||(LA1_0>='\u000B' && LA1_0<='\uFFFF')) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/dfranusic/cli.g:100:37: .
            	    {
            	    matchAny(); 

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            match('\n'); 
            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "SL_COMMENT"

    // $ANTLR start "WORD"
    public final void mWORD() throws RecognitionException {
        try {
            int _type = WORD;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:108:3: ( ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '.' | '_' )* )
            // /home/dfranusic/cli.g:108:5: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '.' | '_' )*
            {
            // /home/dfranusic/cli.g:108:5: ( 'a' .. 'z' | 'A' .. 'Z' | '0' .. '9' | '.' | '_' )*
            loop2:
            do {
                int alt2=2;
                int LA2_0 = input.LA(1);

                if ( (LA2_0=='.'||(LA2_0>='0' && LA2_0<='9')||(LA2_0>='A' && LA2_0<='Z')||LA2_0=='_'||(LA2_0>='a' && LA2_0<='z')) ) {
                    alt2=1;
                }


                switch (alt2) {
            	case 1 :
            	    // /home/dfranusic/cli.g:
            	    {
            	    if ( input.LA(1)=='.'||(input.LA(1)>='0' && input.LA(1)<='9')||(input.LA(1)>='A' && input.LA(1)<='Z')||input.LA(1)=='_'||(input.LA(1)>='a' && input.LA(1)<='z') ) {
            	        input.consume();

            	    }
            	    else {
            	        MismatchedSetException mse = new MismatchedSetException(null,input);
            	        recover(mse);
            	        throw mse;}


            	    }
            	    break;

            	default :
            	    break loop2;
                }
            } while (true);


            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WORD"

    // $ANTLR start "WS"
    public final void mWS() throws RecognitionException {
        try {
            int _type = WS;
            int _channel = DEFAULT_TOKEN_CHANNEL;
            // /home/dfranusic/cli.g:113:3: ( ( ' ' | '\\t' | '\\r' | '\\n' ) )
            // /home/dfranusic/cli.g:113:5: ( ' ' | '\\t' | '\\r' | '\\n' )
            {
            if ( (input.LA(1)>='\t' && input.LA(1)<='\n')||input.LA(1)=='\r'||input.LA(1)==' ' ) {
                input.consume();

            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                recover(mse);
                throw mse;}

            _channel=HIDDEN;

            }

            state.type = _type;
            state.channel = _channel;
        }
        finally {
        }
    }
    // $ANTLR end "WS"

    public void mTokens() throws RecognitionException {
        // /home/dfranusic/cli.g:1:8: ( L_PAREN | R_PAREN | R_SQ_B | L_SQ_B | L_CR_B | R_CR_B | ANNT | EQUAL | ASSIGN | OR | NEQUAL | COLON | STMTSEP | PERCENT | HEX_P | LT | GT | LTE | GTE | PLUS | MINUS | AND | SQUOTE | REGEX_BLOCK | ASTERISK | COMMA | SL_COMMENT | WORD | WS )
        int alt3=29;
        alt3 = dfa3.predict(input);
        switch (alt3) {
            case 1 :
                // /home/dfranusic/cli.g:1:10: L_PAREN
                {
                mL_PAREN(); 

                }
                break;
            case 2 :
                // /home/dfranusic/cli.g:1:18: R_PAREN
                {
                mR_PAREN(); 

                }
                break;
            case 3 :
                // /home/dfranusic/cli.g:1:26: R_SQ_B
                {
                mR_SQ_B(); 

                }
                break;
            case 4 :
                // /home/dfranusic/cli.g:1:33: L_SQ_B
                {
                mL_SQ_B(); 

                }
                break;
            case 5 :
                // /home/dfranusic/cli.g:1:40: L_CR_B
                {
                mL_CR_B(); 

                }
                break;
            case 6 :
                // /home/dfranusic/cli.g:1:47: R_CR_B
                {
                mR_CR_B(); 

                }
                break;
            case 7 :
                // /home/dfranusic/cli.g:1:54: ANNT
                {
                mANNT(); 

                }
                break;
            case 8 :
                // /home/dfranusic/cli.g:1:59: EQUAL
                {
                mEQUAL(); 

                }
                break;
            case 9 :
                // /home/dfranusic/cli.g:1:65: ASSIGN
                {
                mASSIGN(); 

                }
                break;
            case 10 :
                // /home/dfranusic/cli.g:1:72: OR
                {
                mOR(); 

                }
                break;
            case 11 :
                // /home/dfranusic/cli.g:1:75: NEQUAL
                {
                mNEQUAL(); 

                }
                break;
            case 12 :
                // /home/dfranusic/cli.g:1:82: COLON
                {
                mCOLON(); 

                }
                break;
            case 13 :
                // /home/dfranusic/cli.g:1:88: STMTSEP
                {
                mSTMTSEP(); 

                }
                break;
            case 14 :
                // /home/dfranusic/cli.g:1:96: PERCENT
                {
                mPERCENT(); 

                }
                break;
            case 15 :
                // /home/dfranusic/cli.g:1:104: HEX_P
                {
                mHEX_P(); 

                }
                break;
            case 16 :
                // /home/dfranusic/cli.g:1:110: LT
                {
                mLT(); 

                }
                break;
            case 17 :
                // /home/dfranusic/cli.g:1:113: GT
                {
                mGT(); 

                }
                break;
            case 18 :
                // /home/dfranusic/cli.g:1:116: LTE
                {
                mLTE(); 

                }
                break;
            case 19 :
                // /home/dfranusic/cli.g:1:120: GTE
                {
                mGTE(); 

                }
                break;
            case 20 :
                // /home/dfranusic/cli.g:1:124: PLUS
                {
                mPLUS(); 

                }
                break;
            case 21 :
                // /home/dfranusic/cli.g:1:129: MINUS
                {
                mMINUS(); 

                }
                break;
            case 22 :
                // /home/dfranusic/cli.g:1:135: AND
                {
                mAND(); 

                }
                break;
            case 23 :
                // /home/dfranusic/cli.g:1:139: SQUOTE
                {
                mSQUOTE(); 

                }
                break;
            case 24 :
                // /home/dfranusic/cli.g:1:146: REGEX_BLOCK
                {
                mREGEX_BLOCK(); 

                }
                break;
            case 25 :
                // /home/dfranusic/cli.g:1:158: ASTERISK
                {
                mASTERISK(); 

                }
                break;
            case 26 :
                // /home/dfranusic/cli.g:1:167: COMMA
                {
                mCOMMA(); 

                }
                break;
            case 27 :
                // /home/dfranusic/cli.g:1:173: SL_COMMENT
                {
                mSL_COMMENT(); 

                }
                break;
            case 28 :
                // /home/dfranusic/cli.g:1:184: WORD
                {
                mWORD(); 

                }
                break;
            case 29 :
                // /home/dfranusic/cli.g:1:189: WS
                {
                mWS(); 

                }
                break;

        }

    }


    protected DFA3 dfa3 = new DFA3(this);
    static final String DFA3_eotS =
        "\1\30\7\uffff\1\33\5\uffff\1\30\1\36\1\40\4\uffff\1\42\6\uffff\1"+
        "\43\7\uffff";
    static final String DFA3_eofS =
        "\44\uffff";
    static final String DFA3_minS =
        "\1\11\7\uffff\1\75\5\uffff\1\170\2\75\4\uffff\1\57\6\uffff\1\56"+
        "\7\uffff";
    static final String DFA3_maxS =
        "\1\175\7\uffff\1\75\5\uffff\1\170\2\75\4\uffff\1\57\6\uffff\1\172"+
        "\7\uffff";
    static final String DFA3_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\uffff\1\12\1\13\1\14\1\15"+
        "\1\16\3\uffff\1\24\1\25\1\26\1\27\1\uffff\1\31\1\32\1\34\1\35\1"+
        "\10\1\11\1\uffff\1\22\1\20\1\23\1\21\1\33\1\30\1\17";
    static final String DFA3_specialS =
        "\44\uffff}>";
    static final String[] DFA3_transitionS = {
            "\2\31\2\uffff\1\31\22\uffff\1\31\1\12\3\uffff\1\15\1\23\1\24"+
            "\1\1\1\2\1\26\1\21\1\27\1\22\1\uffff\1\25\1\16\11\uffff\1\13"+
            "\1\14\1\17\1\10\1\20\1\uffff\1\7\32\uffff\1\4\1\uffff\1\3\35"+
            "\uffff\1\5\1\11\1\6",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\32",
            "",
            "",
            "",
            "",
            "",
            "\1\34",
            "\1\35",
            "\1\37",
            "",
            "",
            "",
            "",
            "\1\41",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\30\1\uffff\12\30\7\uffff\32\30\4\uffff\1\30\1\uffff\32\30",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA3_eot = DFA.unpackEncodedString(DFA3_eotS);
    static final short[] DFA3_eof = DFA.unpackEncodedString(DFA3_eofS);
    static final char[] DFA3_min = DFA.unpackEncodedStringToUnsignedChars(DFA3_minS);
    static final char[] DFA3_max = DFA.unpackEncodedStringToUnsignedChars(DFA3_maxS);
    static final short[] DFA3_accept = DFA.unpackEncodedString(DFA3_acceptS);
    static final short[] DFA3_special = DFA.unpackEncodedString(DFA3_specialS);
    static final short[][] DFA3_transition;

    static {
        int numStates = DFA3_transitionS.length;
        DFA3_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA3_transition[i] = DFA.unpackEncodedString(DFA3_transitionS[i]);
        }
    }

    class DFA3 extends DFA {

        public DFA3(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 3;
            this.eot = DFA3_eot;
            this.eof = DFA3_eof;
            this.min = DFA3_min;
            this.max = DFA3_max;
            this.accept = DFA3_accept;
            this.special = DFA3_special;
            this.transition = DFA3_transition;
        }
        public String getDescription() {
            return "1:1: Tokens : ( L_PAREN | R_PAREN | R_SQ_B | L_SQ_B | L_CR_B | R_CR_B | ANNT | EQUAL | ASSIGN | OR | NEQUAL | COLON | STMTSEP | PERCENT | HEX_P | LT | GT | LTE | GTE | PLUS | MINUS | AND | SQUOTE | REGEX_BLOCK | ASTERISK | COMMA | SL_COMMENT | WORD | WS );";
        }
    }
 

}