package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class BMPSTRING extends ASNType {

	public BMPSTRING(){
		super();
		universalTag = ASNTag.BMPSTRING;
		tag = ASNTag.name2Tag(universalTag.toString());
		
	}
}
