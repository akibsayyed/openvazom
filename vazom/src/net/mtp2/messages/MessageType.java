package net.mtp2.messages;

public enum MessageType {
	MSU, // Message Signal Unit
	LSSU, // Link Status Signal Unit
	FISU; // Fill-in Signal Unit
}
