package net.ds.pds;

import java.util.HashMap;

import net.asn1.compiler.ASNType;
import net.asn1.gsmmap2.LocationInfoWithLMSI;
import net.asn1.gsmmap2.MO_ForwardSM_Arg;
import net.asn1.gsmmap2.MT_ForwardSM_Arg;
import net.asn1.gsmmap2.RoutingInfoForSM_Arg;
import net.asn1.gsmmap2.RoutingInfoForSM_Res;
import net.asn1.gsmmap_ch.RoutingInfo;
import net.asn1.gsmmap_ch.SendRoutingInfoArg;
import net.asn1.gsmmap_ch.SendRoutingInfoRes;
import net.asn1.tcap2.ReturnError;
import net.asn1.tcap2.TCMessage;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
import net.ds.DSBase;
import net.ds.DSPacket;
import net.ds.DataSourceType;
import net.isup.ISUP;
import net.isup.ISUPPacket;
import net.isup.ParameterType;
import net.isup.messages.AddressComplete;
import net.isup.messages.Answer;
import net.isup.messages.InitialAddress;
import net.isup.messages.Release;
import net.isup.parameters.CallingPartyNumber;
import net.isup.parameters.ParameterBase;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.mtp2.MTP2;
import net.mtp2.MTP2Packet;
import net.mtp2.messages.MSU;
import net.mtp3.MTP3;
import net.mtp3.MTP3Packet;
import net.mtp3.messages.normal.SCCPDataMessage;
import net.mtp3.types.ServiceIndicatorType;
import net.sccp.MessageType;
import net.sccp.NatureOfAddress;
import net.sccp.SCCP;
import net.sccp.messages.MessageBase;
import net.sccp.messages.UDT_UnitData;
import net.sccp.parameters.global_title.NumberingPlan;
import net.sgn.SGNode;
import net.smstpdu.AddressString;
import net.smstpdu.GSMAlphabet;
import net.smstpdu.MessageDirection;
import net.smstpdu.SmsType;
import net.smstpdu.TBCD;
import net.smstpdu.tpdu.SmsDeliver;
import net.smstpdu.tpdu.SmsSubmit;
import net.smstpdu.tpdu.TPDU;
import net.smstpdu.tpdu.udh.IE_ConcatenatedReference;
import net.stats.StatsManager;
import net.utils.Utils;
import net.vstp.HeaderDescriptor;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class PDS_DecoderWorker_MTP3_M3UA extends PDSWorkerBase{
	private static Logger logger=Logger.getLogger(PDS_DecoderWorker_MTP3_M3UA.class);


	private void process_SMS_SUBMIT(SmsSubmit sms, SmsInfo sms_info){
		String sms_text_decoded = "";
		String destination_decoded = "";
		//DBRecordSMS dbr;
		MessageDescriptor vstp_md;
		try{
			// VSTP packet
			// header
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			/*
			vstp_md.header.msg_id = Utils.isNull(sms_info.gt_called, "") + "." + 
									Utils.isNull(sms_info.gt_calling, "") + "." + 
									(sms_info.tcap_sid == null ? "0" : sms_info.tcap_sid) + "." + 
									(sms_info.tcap_did == null ? "0" : sms_info.tcap_did); 
			*/
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.SMS_MO;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			// body
			if(sms.udh != null){
				IE_ConcatenatedReference ie_ref = sms.udh.findIERef();
				if(ie_ref != null){
					StatsManager.DECODER_STATS.MO_CONCATENATED_COUNT++;
					vstp_md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
					// extra info for concatenated message
					vstp_md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(ie_ref.partNumber));
					vstp_md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(ie_ref.parts));
					vstp_md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(ie_ref.messageIdentifier));
				}
			}else{
				vstp_md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
				
			}
			// sccp part
			// gt address
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), sms_info.gt_called);
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), sms_info.gt_calling);
			// gt gti
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(sms_info.gt_called_gti.getId()));
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(sms_info.gt_calling_gti.getId()));
			// gt tt
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(sms_info.gt_called_tt));
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(sms_info.gt_calling_tt));
			// gt np
			if(sms_info.gt_called_np != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), Integer.toString(sms_info.gt_called_np.getId()));
			if(sms_info.gt_calling_np != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), Integer.toString(sms_info.gt_calling_np.getId()));
			// gt nai
			if(sms_info.gt_called_nai != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), Integer.toString(sms_info.gt_called_nai.getId()));
			if(sms_info.gt_calling_nai != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), Integer.toString(sms_info.gt_calling_nai.getId()));
			
			// m3ua, tcap, sms-tpdu
			vstp_md.values.put(VSTPDataItemType.MAP_SCOA.getId(), sms_info.scoa);
			vstp_md.values.put(VSTPDataItemType.MAP_SCDA.getId(), sms_info.scda);
			vstp_md.values.put(VSTPDataItemType.MAP_IMSI.getId(), sms_info.imsi);
			vstp_md.values.put(VSTPDataItemType.MAP_MSISDN.getId(), sms_info.msisdn);
			vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(sms_info.m3ua_data_dpc));
			vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(sms_info.m3ua_data_opc));
			vstp_md.values.put(VSTPDataItemType.TCAP_SID.getId(), (sms_info.tcap_sid == null ? null : Long.toString(sms_info.tcap_sid)));
			vstp_md.values.put(VSTPDataItemType.TCAP_DID.getId(), (sms_info.tcap_did == null ? null : Long.toString(sms_info.tcap_did)));
			vstp_md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), (sms_info.invoke_id == null ? null : Long.toString(sms_info.invoke_id)));
			vstp_md.values.put(VSTPDataItemType.SMS_TPDU_DCS.getId(), Integer.toString(sms.TP_DCS.encoding.getId()));
			
			switch(sms.TP_DCS.encoding){
				case DEFAULT: 
					if(sms.udh == null) sms_text_decoded = GSMAlphabet.decode(sms.TP_UD, sms.TP_UDL);
					else sms_text_decoded = GSMAlphabet.decodePadded(sms.TP_UD, sms.udh.length + 1);
					break;
				case _8BIT:
					//sms_text_decoded = Utils.getPrintable(sms.TP_UD);
					sms_text_decoded = Utils.bytes2hexstr(sms.TP_UD, "");
					break;
				case UCS2:
					sms_text_decoded = new String(sms.TP_UD, "UTF-16");
					break;
			}
			vstp_md.values.put(VSTPDataItemType.SMS_TPDU_UD.getId(), Utils.bytes2hexstr(sms_text_decoded.getBytes(), ""));
			sms_text_decoded = "";

			// Mandatory field
			if(sms.TP_DA.typeOfNumber != null){
				vstp_md.values.put(VSTPDataItemType.SMS_TPDU_DESTINATION_ENC.getId(), Integer.toString(sms.TP_DA.typeOfNumber.getId()));
				switch(sms.TP_DA.typeOfNumber){
					case ALPHANUMERIC:
						destination_decoded = GSMAlphabet.decode(sms.TP_DA.digits, sms.TP_DA.length);
						break;
					default:
						destination_decoded = TBCD.decode(sms.TP_DA.digits);
						break;
				}
				
			}
			vstp_md.values.put(VSTPDataItemType.SMS_TPDU_DESTINATION.getId(), destination_decoded);
			destination_decoded = "";
			// out queue
			SGNode.out_offer(vstp_md);
		
				
			
		}catch(Exception e){
			e.printStackTrace();
		
		}			

	}
	private void process_SMS_DELIVER(SmsDeliver sms, SmsInfo sms_info){
		String sms_text_decoded = "";
		String originating_decoded = "";
		//DBRecordSMS dbr;
		MessageDescriptor vstp_md;
		try{
			// VSTP packet
			// header
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			/*
			vstp_md.header.msg_id = Utils.isNull(sms_info.gt_called, "") + "." + 
									Utils.isNull(sms_info.gt_calling, "") + "." + 
									(sms_info.tcap_sid == null ? "0" : sms_info.tcap_sid) + "." + 
									(sms_info.tcap_did == null ? "0" : sms_info.tcap_did); 
			*/
			vstp_md.header.msg_id = "-";

			vstp_md.header.msg_type = net.vstp.MessageType.SMS_MT;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			// body
			if(sms.udh != null){
				IE_ConcatenatedReference ie_ref = sms.udh.findIERef();
				if(ie_ref != null){
					StatsManager.DECODER_STATS.MT_CONCATENATED_COUNT++;
					vstp_md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.CONCATENATED.getId()));
					// extra info for concatenated message
					vstp_md.values.put(VSTPDataItemType.SMS_CONC_PART_NUMBER.getId(), Integer.toString(ie_ref.partNumber));
					vstp_md.values.put(VSTPDataItemType.SMS_CONC_PARTS.getId(), Integer.toString(ie_ref.parts));
					vstp_md.values.put(VSTPDataItemType.SMS_CONC_MSG_IDENTIFIER.getId(), Integer.toString(ie_ref.messageIdentifier));
				}
			}else{
				vstp_md.values.put(VSTPDataItemType.SMS_MSG_TYPE.getId(), Integer.toString(SmsType.SINGLE.getId()));
				
			}
			// sccp part
			// gt address
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), sms_info.gt_called);
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), sms_info.gt_calling);
			// gt gti
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(sms_info.gt_called_gti.getId()));
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(sms_info.gt_calling_gti.getId()));
			// gt tt
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(sms_info.gt_called_tt));
			vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(sms_info.gt_calling_tt));
			// gt np
			if(sms_info.gt_called_np != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), Integer.toString(sms_info.gt_called_np.getId()));
			if(sms_info.gt_calling_np != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), Integer.toString(sms_info.gt_calling_np.getId()));
			// gt nai
			if(sms_info.gt_called_nai != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), Integer.toString(sms_info.gt_called_nai.getId()));
			if(sms_info.gt_calling_nai != null) vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), Integer.toString(sms_info.gt_calling_nai.getId()));
			
			// m3ua, tcap, sms-tpdu
			vstp_md.values.put(VSTPDataItemType.MAP_SCOA.getId(), sms_info.scoa);
			vstp_md.values.put(VSTPDataItemType.MAP_SCDA.getId(), sms_info.scda);
			vstp_md.values.put(VSTPDataItemType.MAP_IMSI.getId(), sms_info.imsi);
			vstp_md.values.put(VSTPDataItemType.MAP_MSISDN.getId(), sms_info.msisdn);
			vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(sms_info.m3ua_data_dpc));
			vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(sms_info.m3ua_data_opc));
			vstp_md.values.put(VSTPDataItemType.TCAP_SID.getId(), (sms_info.tcap_sid == null ? null : Long.toString(sms_info.tcap_sid)));
			vstp_md.values.put(VSTPDataItemType.TCAP_DID.getId(), (sms_info.tcap_did == null ? null : Long.toString(sms_info.tcap_did)));
			vstp_md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), (sms_info.invoke_id == null ? null : Long.toString(sms_info.invoke_id)));
			vstp_md.values.put(VSTPDataItemType.SMS_TPDU_DCS.getId(), Integer.toString(sms.TP_DCS.encoding.getId()));
			
			switch(sms.TP_DCS.encoding){
				case DEFAULT: 
					if(sms.udh == null) sms_text_decoded = GSMAlphabet.decode(sms.TP_UD, sms.TP_UDL);
					else sms_text_decoded = GSMAlphabet.decodePadded(sms.TP_UD, sms.udh.length + 1);
					break;
				case _8BIT:
					//sms_text_decoded = Utils.getPrintable(sms.TP_UD);
					sms_text_decoded = Utils.bytes2hexstr(sms.TP_UD, "");
					break;
				case UCS2:
					sms_text_decoded = new String(sms.TP_UD, "UTF-16");
					break;
			}
			vstp_md.values.put(VSTPDataItemType.SMS_TPDU_UD.getId(), Utils.bytes2hexstr(sms_text_decoded.getBytes(), ""));
			sms_text_decoded = "";

			// Mandatory field
			if(sms.TP_OA.typeOfNumber != null){
				vstp_md.values.put(VSTPDataItemType.SMS_TPDU_ORIGINATING_ENC.getId(), Integer.toString(sms.TP_OA.typeOfNumber.getId()));
				switch(sms.TP_OA.typeOfNumber){
					case ALPHANUMERIC:
						originating_decoded = GSMAlphabet.decode(sms.TP_OA.digits, sms.TP_OA.length);
						break;
					default:
						originating_decoded = TBCD.decode(sms.TP_OA.digits);
						break;
				}
				
			}
			vstp_md.values.put(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId(), originating_decoded);
			originating_decoded = "";
			// out queue
			//System.out.println(new String(vstp_md.encode()));
			SGNode.out_offer(vstp_md);
		
				
			
		}catch(Exception e){
			e.printStackTrace();
		
		}			
	}
	
	private void process_MAP_RETURN_ERROR(TCMessage tcm, int error_code, HashMap<String, String> extra){
		MessageDescriptor vstp_md;
		int did;
		int invoke_id;
		try{
			// VSTP packet
			// header
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.MAP_RETURN_ERROR;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			
			// body
			did = (int)Utils.getTcap_did(tcm);
			invoke_id = (int)Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
			vstp_md.values.put(VSTPDataItemType.TCAP_DID.getId(), Integer.toString(did));
			vstp_md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), Integer.toString(invoke_id));
			vstp_md.values.put(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId(), Integer.toString(error_code));
			
			if(extra != null){
				for(String s : extra.keySet()){
					vstp_md.values.put(s, extra.get(s));
					
				}
				
			}
			
			
			// send
			SGNode.out_offer(vstp_md);
		
				
			
		}catch(Exception e){
			e.printStackTrace();
		
		}			
	
	
	}
	
	private void process_MAP_RETURN_RESULT(TCMessage tcm, HashMap<String, String> extra){
		MessageDescriptor vstp_md;
		int did;
		int invoke_id;
		try{
			// VSTP packet
			// header
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.MAP_RETURN_RESULT;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			
			// body
			did = (int)Utils.getTcap_did(tcm);
			invoke_id = (int)Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
			vstp_md.values.put(VSTPDataItemType.TCAP_DID.getId(), Integer.toString(did));
			vstp_md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), Integer.toString(invoke_id));
			if(extra != null){
				for(String s : extra.keySet()){
					vstp_md.values.put(s, extra.get(s));
					
				}
				
			}
			
			// send
			SGNode.out_offer(vstp_md);
		
				
			
		}catch(Exception e){
			e.printStackTrace();
		
		}			
	
	}
	
	private void process_TCAP_ABORT(TCMessage tcm, HashMap<String, String> extra){
		MessageDescriptor vstp_md;
		int error_code;
		int did;
		try{
			// VSTP packet
			// header
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.TCAP_ABORT;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			
			// body
			did = (int)Utils.getTcap_did(tcm);
			vstp_md.values.put(VSTPDataItemType.TCAP_DID.getId(), Integer.toString(did));
			
			if(tcm.get_abort().get_reason() != null){
				if(tcm.get_abort().get_reason().get_p_abortCause() != null){
					error_code = Utils.getTcapAbortErrorCode(tcm);
					vstp_md.values.put(VSTPDataItemType.TCAP_ERROR_CODE.getId(), Integer.toString(error_code));
					
				}else if(tcm.get_abort().get_reason().get_u_abortCause() != null){
					error_code = Utils.getTcapAbortDialogueError(tcm);
					vstp_md.values.put(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId(), Integer.toString(error_code));
				
				}			
				
								
				
			}else vstp_md.values.put(VSTPDataItemType.TCAP_ERROR_CODE.getId(), "9999");
			
			if(extra != null){
				for(String s : extra.keySet()){
					vstp_md.values.put(s, extra.get(s));
					
				}
				
			}
			
			// send
			SGNode.out_offer(vstp_md);
		
				
			
		}catch(Exception e){
			e.printStackTrace();
		
		}			
		
	}
	private void processSMS(TPDU sms, SmsInfo sms_info){
		SmsSubmit sms_s = null;
		SmsDeliver sms_d = null;
		if(sms != null){
			// SMS-DELIVER
			if(sms.type == net.smstpdu.MessageType.SMS_DELIVER){
				sms_d = (SmsDeliver)sms;
				if(sms_d.TP_UD != null){
					StatsManager.DECODER_STATS.SMS_DELIVER_COUNT++;
					process_SMS_DELIVER(sms_d, sms_info);
					
				}
				
			// SMS-SUBMIT
			}else if(sms.type == net.smstpdu.MessageType.SMS_SUBMIT){
				sms_s = (SmsSubmit)sms;
				if(sms_s.TP_UD != null){
					StatsManager.DECODER_STATS.SMS_SUBMIT_COUNT++;
					process_SMS_SUBMIT(sms_s, sms_info);
					
				}
			}
		}
	}
	private void process_ISUP_ANSWER(net.isup.messages.MessageBase _anm, int cic, int dpc, int opc){
		//Answer anm = null;
		MessageDescriptor vstp_md = null;
		if(_anm != null){
			//anm = (Answer)_anm;
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.ISUP_ANM;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			// body
			vstp_md.values.put(VSTPDataItemType.ISUP_CIC.getId(), Integer.toString(cic));
			vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
			vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));

			// send
			SGNode.out_offer(vstp_md);
		}
		
	}

	private void process_ISUP_RELEASE(net.isup.messages.MessageBase _rel, int cic, int dpc, int opc){
		Release rel = null;
		MessageDescriptor vstp_md = null;
		if(_rel != null){
			rel = (Release)_rel;
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.ISUP_REL;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			// body
			vstp_md.values.put(VSTPDataItemType.ISUP_CIC.getId(), Integer.toString(cic));
			vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
			vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));
			if(rel.cause_indicators.cause_value != null && rel.cause_indicators.cause_class != null){
				vstp_md.values.put(VSTPDataItemType.ISUP_RELEASE_CLASS.getId(), Integer.toString(rel.cause_indicators.cause_value.getClassId()));
				vstp_md.values.put(VSTPDataItemType.ISUP_RELEASE_CAUSE.getId(), Integer.toString(rel.cause_indicators.cause_value.getId()));
				
			}else{
				vstp_md.values.put(VSTPDataItemType.ISUP_RELEASE_CLASS.getId(), Integer.toString(rel.cause_indicators.class_code));
				vstp_md.values.put(VSTPDataItemType.ISUP_RELEASE_CAUSE.getId(), Integer.toString(rel.cause_indicators.cause_code));
				
			}
			
			
			// send
			SGNode.out_offer(vstp_md);
		}
		
	
	}
	private void process_ISUP_INITIAL_ADDRESS(net.isup.messages.MessageBase _iam, int cic, int dpc, int opc){
		InitialAddress iam = null;
		MessageDescriptor vstp_md = null;
		ParameterBase param = null;
		CallingPartyNumber cpn = null;
		String tmp = null;
		if(_iam != null){
			iam = (InitialAddress)_iam;
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.ISUP_IAM;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			// body
			vstp_md.values.put(VSTPDataItemType.ISUP_CIC.getId(), Integer.toString(cic));
			vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
			vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));
			// called party
			if(iam.called_party_number.odd_number_of_address_signals) tmp = TBCD.decodeOdd(iam.called_party_number.address_signal);
			else tmp = TBCD.decode(iam.called_party_number.address_signal);
			vstp_md.values.put(VSTPDataItemType.ISUP_CALLED_PARTY.getId(), tmp);
			if(iam.called_party_number.nature_of_address_indicator != null)	vstp_md.values.put(VSTPDataItemType.ISUP_CALLED_PARTY_NAI.getId(), Integer.toString(iam.called_party_number.nature_of_address_indicator.getId()));

			// calling party
			param = iam.getParameter(ParameterType.CALLING_PARTY_NUMBER);
			if(param != null){
				cpn = (CallingPartyNumber)param;

				if(cpn.odd_number_of_address_signals) tmp = TBCD.decodeOdd(cpn.address_signal);
				else tmp = TBCD.decode(cpn.address_signal);

				vstp_md.values.put(VSTPDataItemType.ISUP_CALLING_PARTY.getId(), tmp);
				if(cpn.nature_of_address_indicator != null)	vstp_md.values.put(VSTPDataItemType.ISUP_CALLING_PARTY_NAI.getId(), Integer.toString(cpn.nature_of_address_indicator.getId()));
				
			}
			
			// send
			SGNode.out_offer(vstp_md);
		}
		
		
	}
	private void process_ISUP_ADDRESS_COMPLETE(net.isup.messages.MessageBase _acm, int cic, int dpc, int opc){
		MessageDescriptor vstp_md = null;
		if(_acm != null){
			vstp_md = new MessageDescriptor();
			vstp_md.header = new HeaderDescriptor();
			vstp_md.header.destination = LocationType.FN;
			vstp_md.header.ds = data_source.type;
			vstp_md.header.msg_id = "-";
			vstp_md.header.msg_type = net.vstp.MessageType.ISUP_ACM;
			vstp_md.header.source = LocationType.SGN;
			vstp_md.header.source_id = SGNConfigData.sgn_id;
			// body
			vstp_md.values.put(VSTPDataItemType.ISUP_CIC.getId(), Integer.toString(cic));
			vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
			vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));
			// send
			SGNode.out_offer(vstp_md);
		}
	}	
	
	
	private void process_ISUP(ISUPPacket isupp, int cic, int dpc, int opc){
		if(isupp != null){
			if(isupp.message != null){
				if(isupp.message.type != null){
					switch(isupp.message.type){
						case INITIAL_ADDRESS:
							StatsManager.DECODER_STATS.ISUP_IAM++;
							process_ISUP_INITIAL_ADDRESS(isupp.message, cic, dpc, opc); 
							break;
						case ADDRESS_COMPLETE: 
							StatsManager.DECODER_STATS.ISUP_ACM++;
							process_ISUP_ADDRESS_COMPLETE(isupp.message, cic, dpc, opc); 
							break;
						case RELEASE: 
							StatsManager.DECODER_STATS.ISUP_REL++;
							process_ISUP_RELEASE(isupp.message, cic, dpc, opc); 
							break;
						case RELEASE_COMPLETE: 
							StatsManager.DECODER_STATS.ISUP_RLC++;
							break;
						case ANSWER: 
							StatsManager.DECODER_STATS.ISUP_ANM++;
							process_ISUP_ANSWER(isupp.message, cic, dpc, opc); 
							break;
					}
				}
				
			}
			
		}
		
		
	}
	
	private class Dw_r implements Runnable{
		DSPacket packet;
		MessageDescriptor vstp_md = null;
		UDT_UnitData udt;
		MessageBase sccp_packet;
		TCMessage tcm;
		BerTranscoderv2 ber = new BerTranscoderv2();
		MO_ForwardSM_Arg mo_f_sm;
		MT_ForwardSM_Arg mt_f_sm;
		String gt_called_s;
		String gt_calling_s;
		AddressString as;
		ASNType invoke_param = null;
		//DBRecordSRI dbr_sri;
		//DBRecordSRI tmp_dbr_sri;
		ASNType invoke = null;
		ASNType returnResultLast = null;
		int invoke_id;
		//long tcap_id;
		ReturnError returnErrorTcap;
		net.asn1.gsmmap2.ReturnError returnErrorGSMMAP;
		String ctx_oid = null;
		ISUPPacket isup_packet = null;
		
		
		
		public void run() {
			boolean sccp_ready = false;
			boolean isup_ready = false;
			boolean h248_ready = false;
			M3UAPacket m3ua_packet;
			MTP2Packet mtp2_packet;
			MTP3Packet mtp3_packet;
			MSU msu;
			DATA m3ua_data_packet = null;
			SCCPDataMessage mtp3_data_packet = null;
			int dpc = 0;
			int opc = 0;
			SmsInfo sms_info = null;
			TPDU tpdu = null;
			Long tcap_sid = null;
			Long tcap_did = null;
			NumberingPlan tmp_np = null;
			NatureOfAddress tmp_nai = null;
			String annn = null;
			String nnn = null;
			HashMap<String, String> tmp_map = null;
			logger.info(worker_name + " STARTING!");
			while(!stopping){
				packet = data_source.queue.poll();
				if(packet != null){
					try{
						// check data source type
						// SCTP M3UA
						if(data_source.type == DataSourceType.SPCAP_SCTP || data_source.type == DataSourceType.SCTP){
							m3ua_packet = M3UA.decode(packet.data);
							if(m3ua_packet.messageType == M3UAMessageType.DATA){
								m3ua_data_packet = (DATA)m3ua_packet.message;
								dpc = m3ua_data_packet.protocolData.destinationPointCode;
								opc = m3ua_data_packet.protocolData.originatingPointCode;
								if(m3ua_data_packet.protocolData != null){
									if(m3ua_data_packet.protocolData.serviceIndicator == net.m3ua.parameters.protocol_data.ServiceIndicatorType.SCCP){
										packet.data = m3ua_data_packet.protocolData.userProtocolData;
										sccp_ready = true;
										StatsManager.DECODER_STATS.SCCP++;
										
									}else if(m3ua_data_packet.protocolData.serviceIndicator == net.m3ua.parameters.protocol_data.ServiceIndicatorType.ISUP){
										packet.data = m3ua_data_packet.protocolData.userProtocolData;
										isup_ready = true;
										StatsManager.DECODER_STATS.ISUP++;
									}else if(m3ua_data_packet.protocolData.serviceIndicator == net.m3ua.parameters.protocol_data.ServiceIndicatorType.GCP){
										packet.data = m3ua_data_packet.protocolData.userProtocolData;
										h248_ready = true;
										StatsManager.DECODER_STATS.GCP++;
										
									}
								}
							}
						// E1 - MTP2
						}else if(data_source.type == DataSourceType.SPCAP_E1 || data_source.type == DataSourceType.E1){
							mtp2_packet = MTP2.decode(packet.data);
							if(mtp2_packet.message != null){
								if(mtp2_packet.message.type == net.mtp2.messages.MessageType.MSU){
									msu = (MSU)mtp2_packet.message;
									mtp3_packet = MTP3.decode(msu.data);
									dpc = mtp3_packet.routingLabel.destinationPointCode;
									opc = mtp3_packet.routingLabel.originatingPointCode;
									if(mtp3_packet.message != null){
										if(mtp3_packet.serviceInformation.serviceIndicator == ServiceIndicatorType.SCCP){
											mtp3_data_packet = (SCCPDataMessage)mtp3_packet.message;
											packet.data = mtp3_data_packet.data;
											sccp_ready = true;
											StatsManager.DECODER_STATS.SCCP++;
										}else if(mtp3_packet.serviceInformation.serviceIndicator == ServiceIndicatorType.ISDN_USER_PART){
											mtp3_data_packet = (SCCPDataMessage)mtp3_packet.message;
											packet.data = mtp3_data_packet.data;
											isup_ready = true;
											StatsManager.DECODER_STATS.ISUP++;
											
										}
									}
								}
							}
						}
						
						
						// SCCP 
						if(sccp_ready && SCCP.decodeType(packet.data) == MessageType.UDT_UNITDATA){
							sccp_packet = SCCP.decode(packet.data);
							StatsManager.DECODER_STATS.UDT_COUNT++;
							
							udt = (UDT_UnitData)sccp_packet;
							
							// decode GT info
							gt_called_s = Utils.getGT(udt.calledPartyAddress.globalTitle);
							gt_calling_s = Utils.getGT(udt.callingPartyAddress.globalTitle);
							
							
							
							// check if TCAP exists
							if(Utils.isTCAP(udt.data)){
								//System.out.println("DECODING TCAP");
								StatsManager.DECODER_STATS.TCAP_COUNT++;
								tcm = (TCMessage)ber.decode(new TCMessage(), udt.data);
								// common
								tmp_map = new HashMap<String, String>();
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), gt_called_s);
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), gt_calling_s);
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(udt.calledPartyAddress.globalTitleIndicator.getId()));
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(udt.callingPartyAddress.globalTitleIndicator.getId()));
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(Utils.get_called_gt_tt(udt)));
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(Utils.get_calling_gt_tt(udt)));
								tmp_np = Utils.get_called_gt_np(udt);
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), (tmp_np == null ? null : Integer.toString(tmp_np.getId())));
								tmp_np = Utils.get_calling_gt_np(udt);
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), (tmp_np == null ? null : Integer.toString(tmp_np.getId())));
								tmp_nai = Utils.get_called_gt_nai(udt);
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), (tmp_nai == null ? null : Integer.toString(tmp_nai.getId())));
								tmp_nai = Utils.get_calling_gt_nai(udt);
								tmp_map.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), (tmp_nai == null ? null : Integer.toString(tmp_nai.getId())));
								tmp_map.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
								tmp_map.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));
								
								// MO
								if(Utils.isMO(tcm)){
									StatsManager.DECODER_STATS.MO_COUNT++;
									// Begin or Continue
									if(Utils.isBegin(tcm)){
										StatsManager.DECODER_STATS.TCAP_BEGIN++;
										//invoke_param = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
										invoke_param = Utils.get_INVOKE_PARAMETER(tcm);
										tcap_sid = Utils.bytes2num(tcm.get_begin().get_otid().value);
									}else if(Utils.isContinue(tcm)){
										StatsManager.DECODER_STATS.TCAP_CONTINUE++;
										//invoke_param = tcm.get_continue().get_components().getChild(0).get_invoke().get_parameter();
										invoke_param = Utils.get_INVOKE_PARAMETER(tcm);
										tcap_sid = Utils.bytes2num(tcm.get_continue().get_otid().value);
										tcap_did = Utils.bytes2num(tcm.get_continue().get_dtid().value);
									}else if(Utils.isEnd(tcm)){
										StatsManager.DECODER_STATS.TCAP_END++;
										//invoke_param = tcm.get_end().get_components().getChild(0).get_invoke().get_parameter();
										invoke_param = Utils.get_INVOKE_PARAMETER(tcm);
										tcap_did = Utils.bytes2num(tcm.get_end().get_dtid().value);
									}
									if(invoke_param != null){
										mo_f_sm = (MO_ForwardSM_Arg)ber.decode(new MO_ForwardSM_Arg(), invoke_param.getDataBytes());
										// SMS TPDU
										if(mo_f_sm.get_sm_RP_UI() != null){
											
											sms_info = new SmsInfo();
											// gt address
											sms_info.gt_called = gt_called_s;
											sms_info.gt_calling = gt_calling_s;
											// gt type
											sms_info.gt_called_gti = udt.calledPartyAddress.globalTitleIndicator;
											sms_info.gt_calling_gti = udt.callingPartyAddress.globalTitleIndicator;
											// gt translation type
											sms_info.gt_called_tt = Utils.get_called_gt_tt(udt);
											sms_info.gt_calling_tt = Utils.get_calling_gt_tt(udt);
											// gt numbering plan
											sms_info.gt_called_np = Utils.get_called_gt_np(udt);
											sms_info.gt_calling_np = Utils.get_calling_gt_np(udt);
											// gt nai
											sms_info.gt_called_nai = Utils.get_called_gt_nai(udt);
											sms_info.gt_calling_nai = Utils.get_calling_gt_nai(udt);
											// m3ua & tcap
											sms_info.m3ua_data_dpc = dpc;
											sms_info.m3ua_data_opc = opc;
											sms_info.tcap_sid = tcap_sid;
											sms_info.tcap_did = tcap_did;
											sms_info.invoke_id = Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
											sms_info.dataSource = packet.dataSource;

											// serviceCentreAddressDA/OA
											if(mo_f_sm.get_sm_RP_DA().get_serviceCentreAddressDA() != null){
												as = AddressString.decode(mo_f_sm.get_sm_RP_DA().get_serviceCentreAddressDA().getValueBytes());
												sms_info.scda = TBCD.decode(as.digits);
												tpdu = TPDU.decode(mo_f_sm.get_sm_RP_UI().getValueBytes(), MessageDirection.MS_SC);
												
											}else if(mo_f_sm.get_sm_RP_OA().get_serviceCentreAddressOA() != null){
												as = AddressString.decode(mo_f_sm.get_sm_RP_OA().get_serviceCentreAddressOA().getValueBytes());
												sms_info.scoa = TBCD.decode(as.digits);
												tpdu = TPDU.decode(mo_f_sm.get_sm_RP_UI().getValueBytes(), MessageDirection.SC_MS);
											}
											// IMSI
											if(mo_f_sm.get_imsi() != null)
												sms_info.imsi = TBCD.decode(mo_f_sm.get_imsi().getValueBytes());
											else if(mo_f_sm.get_sm_RP_DA().get_imsi() != null)
												sms_info.imsi = TBCD.decode(mo_f_sm.get_sm_RP_DA().get_imsi().getValueBytes());
											// MSISDN
											if(mo_f_sm.get_sm_RP_OA().get_msisdn() != null){
												as = AddressString.decode(mo_f_sm.get_sm_RP_OA().get_msisdn().getValueBytes());
												sms_info.msisdn = TBCD.decode(as.digits);
												
											}
											// Process SMS
											processSMS(tpdu, sms_info);
											tpdu = null;

										}									
									}

	
								// MT
								}else if(Utils.isMT(tcm)){
									StatsManager.DECODER_STATS.MT_COUNT++;
									
									// Begin or Continue
									if(Utils.isBegin(tcm)){
										StatsManager.DECODER_STATS.TCAP_BEGIN++;
										//invoke_param = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
										invoke_param = Utils.get_INVOKE_PARAMETER(tcm);
										tcap_sid = Utils.bytes2num(tcm.get_begin().get_otid().value);
									}else if(Utils.isContinue(tcm)){
										StatsManager.DECODER_STATS.TCAP_CONTINUE++;
										//invoke_param = tcm.get_continue().get_components().getChild(0).get_invoke().get_parameter();
										invoke_param = Utils.get_INVOKE_PARAMETER(tcm);
										tcap_sid = Utils.bytes2num(tcm.get_continue().get_otid().value);
										tcap_did = Utils.bytes2num(tcm.get_continue().get_dtid().value);
									}else if(Utils.isEnd(tcm)){
										StatsManager.DECODER_STATS.TCAP_END++;
										//invoke_param = tcm.get_end().get_components().getChild(0).get_invoke().get_parameter();
										invoke_param = Utils.get_INVOKE_PARAMETER(tcm);
										tcap_did = Utils.bytes2num(tcm.get_end().get_dtid().value);
									}
									if(invoke_param != null){
										mt_f_sm = (MT_ForwardSM_Arg)ber.decode(new MT_ForwardSM_Arg(), invoke_param.getDataBytes());
										if(mt_f_sm.get_sm_RP_UI() != null){
											sms_info = new SmsInfo();
											// gt address
											sms_info.gt_called = gt_called_s;
											sms_info.gt_calling = gt_calling_s;
											// gt type
											sms_info.gt_called_gti = udt.calledPartyAddress.globalTitleIndicator;
											sms_info.gt_calling_gti = udt.callingPartyAddress.globalTitleIndicator;
											// gt translation type
											sms_info.gt_called_tt = Utils.get_called_gt_tt(udt);
											sms_info.gt_calling_tt = Utils.get_calling_gt_tt(udt);
											// gt numbering plan
											sms_info.gt_called_np = Utils.get_called_gt_np(udt);
											sms_info.gt_calling_np = Utils.get_calling_gt_np(udt);
											// gt nai
											sms_info.gt_called_nai = Utils.get_called_gt_nai(udt);
											sms_info.gt_calling_nai = Utils.get_calling_gt_nai(udt);
											// m3ua & tcap
											sms_info.m3ua_data_dpc = dpc;
											sms_info.m3ua_data_opc = opc;
											sms_info.tcap_sid = tcap_sid;
											sms_info.tcap_did = tcap_did;
											sms_info.invoke_id = Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
											sms_info.dataSource = packet.dataSource;
											
											// serviceCentreAddressDA/OA
											if(mt_f_sm.get_sm_RP_DA().get_serviceCentreAddressDA() != null){
												as = AddressString.decode(mt_f_sm.get_sm_RP_DA().get_serviceCentreAddressDA().getValueBytes());
												sms_info.scda = TBCD.decode(as.digits);
												tpdu = TPDU.decode(mt_f_sm.get_sm_RP_UI().getValueBytes(), MessageDirection.MS_SC);
											}else if(mt_f_sm.get_sm_RP_OA().get_serviceCentreAddressOA() != null){
												as = AddressString.decode(mt_f_sm.get_sm_RP_OA().get_serviceCentreAddressOA().getValueBytes());
												sms_info.scoa = TBCD.decode(as.digits);
												tpdu = TPDU.decode(mt_f_sm.get_sm_RP_UI().getValueBytes(), MessageDirection.SC_MS);
											}
											// IMSI
											if(mt_f_sm.get_sm_RP_DA().get_imsi() != null)
												sms_info.imsi = TBCD.decode(mt_f_sm.get_sm_RP_DA().get_imsi().getValueBytes());
											// MSISDN
											if(mt_f_sm.get_sm_RP_OA().get_msisdn() != null){
												as = AddressString.decode(mt_f_sm.get_sm_RP_OA().get_msisdn().getValueBytes());
												sms_info.msisdn = TBCD.decode(as.digits);
												
											}
											// Process SMS
											processSMS(tpdu, sms_info);
											tpdu = null;

										}									
									}									
									


								// TCAP Abort	
								}else if(Utils.isAbort(tcm)){
									StatsManager.DECODER_STATS.TCAP_ABORT++;
									process_TCAP_ABORT(tcm, tmp_map);
									
								// returnResultLast
								}else if(Utils.isReturnResultLast(tcm)){
									//StatsManager.DECODER_STATS.SRI_END++;
									//StatsManager.DECODER_STATS.SRI_RETURN_RESULT++;
									StatsManager.DECODER_STATS.TCAP_END++;
									StatsManager.DECODER_STATS.TCAP_RETURN_RESULT++;
									
									// Dialogue 
									if(tcm.get_end().get_dialoguePortion() != null){
										byte[] tmp_oid = Utils.getTcapDialogueAppCtx(tcm.get_end().get_dialoguePortion());
										if(tmp_oid != null){
											ctx_oid = Utils.decodeOID(tmp_oid);
										}
											
									}
									// aditional fields for SRI
									if(Utils.isEndSRI(tcm)){
										StatsManager.DECODER_STATS.SRI_END++;
										//StatsManager.DECODER_STATS.TCAP_END++;
										//StatsManager.DECODER_STATS.SRI_RETURN_RESULT++;
										
										//returnResultLast = tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_parameter();
										returnResultLast = Utils.get_RETURN_RESULT_LAST_RETRES_PARAMETER(tcm);
										RoutingInfoForSM_Res sm_res = (RoutingInfoForSM_Res)ber.decode(new RoutingInfoForSM_Res(), returnResultLast.getDataBytes());
										LocationInfoWithLMSI li_wlmsi = sm_res.get_locationInfoWithLMSI();
										if(li_wlmsi.get_additional_Number() != null){
											if(li_wlmsi.get_additional_Number().get_msc_Number() != null){
												annn = TBCD.decode(AddressString.decode(li_wlmsi.get_additional_Number().get_msc_Number().getValueBytes()).digits);
											}else if(li_wlmsi.get_additional_Number().get_sgsn_Number() != null){
												annn = TBCD.decode(AddressString.decode(li_wlmsi.get_additional_Number().get_sgsn_Number().getValueBytes()).digits);
											}
											
										}
										if(li_wlmsi.get_networkNode_Number() != null){
											nnn = TBCD.decode(AddressString.decode(li_wlmsi.get_networkNode_Number().getValueBytes()).digits);
										}else nnn = "";
										
										tmp_map.put(VSTPDataItemType.TCAP_DIALOGUE_OID.getId(), ctx_oid);
										tmp_map.put(VSTPDataItemType.HLR_IMSI.getId(), TBCD.decode(sm_res.get_imsi().value));
										tmp_map.put(VSTPDataItemType.HLR_NNN.getId(), nnn);
										tmp_map.put(VSTPDataItemType.HLR_ANNN.getId(), annn);
										
										
									// CH END Sri aditional fields
									}else if(Utils.isEnd_CH_SRI(tcm)){
										StatsManager.DECODER_STATS.SRI_END++;
										returnResultLast = Utils.get_RETURN_RESULT_LAST_RETRES_PARAMETER(tcm);
										SendRoutingInfoRes ch_res = (SendRoutingInfoRes)ber.decode(new SendRoutingInfoRes(), returnResultLast.getDataBytes());
										
										//IMSI
										if(ch_res.get_imsi() != null){
											tmp_map.put(VSTPDataItemType.HLR_IMSI.getId(), TBCD.decode(ch_res.get_imsi().value));
											
										}
										// extended routing info
										if(ch_res.get_extendedRoutingInfo() != null){
											if(ch_res.get_extendedRoutingInfo().get_routingInfo() != null){
												// ROAMING NUMBER
												if(ch_res.get_extendedRoutingInfo().get_routingInfo().get_roamingNumber() != null){
													as = AddressString.decode(ch_res.get_extendedRoutingInfo().get_routingInfo().get_roamingNumber().value);
													tmp_map.put(VSTPDataItemType.HLR_ROAMING_NUMBER.getId(), TBCD.decode(as.digits));
													tmp_map.put(VSTPDataItemType.HLR_ROAMING_NUMBER_NAI.getId(), Integer.toString(as.typeOfNumber.getId()));
													
												}
												// FORWARDED-TO NUMBER
												if(ch_res.get_extendedRoutingInfo().get_routingInfo().get_forwardingData()!= null){
													if(ch_res.get_extendedRoutingInfo().get_routingInfo().get_forwardingData().get_forwardedToNumber() != null){
														as = AddressString.decode(ch_res.get_extendedRoutingInfo().get_routingInfo().get_forwardingData().get_forwardedToNumber().value);
														tmp_map.put(VSTPDataItemType.HLR_FWD_TO_NUMBER.getId(), TBCD.decode(as.digits));
														tmp_map.put(VSTPDataItemType.HLR_FWD_TO_NUMBER_NAI.getId(), Integer.toString(as.typeOfNumber.getId()));
													
													}														
												}
												
												
											}
										}
										// VMSC
										if(ch_res.get_vmsc_Address() != null){
											as = AddressString.decode(ch_res.get_vmsc_Address().value);
											tmp_map.put(VSTPDataItemType.HLR_VMSC.getId(), TBCD.decode(as.digits));
											tmp_map.put(VSTPDataItemType.HLR_VMSC_NAI.getId(), Integer.toString(as.typeOfNumber.getId()));
											
										}
										
									}
									process_MAP_RETURN_RESULT(tcm, tmp_map);

									
									
								// returnError
								}else if(Utils.isReturnError(tcm)){
									StatsManager.DECODER_STATS.TCAP_END++;
									StatsManager.DECODER_STATS.TCAP_RETURN_ERROR++;
									
									//returnErrorTcap = tcm.get_end().get_components().getChild(0).get_returnError();
									returnErrorTcap = Utils.get_RETURN_ERROR(tcm);
									returnErrorGSMMAP = (net.asn1.gsmmap2.ReturnError)ber.decode(new net.asn1.gsmmap2.ReturnError(), returnErrorTcap.getDataBytes());
									
									// Check if ReturnError is from GSM MAP
									if(returnErrorGSMMAP.get_errorCode() != null){
										if(returnErrorGSMMAP.get_errorCode().get_localValue() != null){
											process_MAP_RETURN_ERROR(tcm, (int)Utils.bytes2num(returnErrorGSMMAP.get_errorCode().get_localValue().value), tmp_map);
										// Error, but unknown error code	
										}else{
											process_MAP_RETURN_ERROR(tcm, 9999, tmp_map);
										}
									}

									
									
								// HLR REQ packets
								}else{
									if(m3ua_data_packet != null || mtp3_data_packet != null){
										// TCAP Begin SRI
										// TCAP End SRI (Result)
										// TCAP Continue SRI (Result)
										if(Utils.isBeginSRI(tcm)){
											
											// VSTP packet
											// header
											vstp_md = new MessageDescriptor();
											vstp_md.header = new HeaderDescriptor();
											vstp_md.header.destination = LocationType.FN;
											vstp_md.header.ds = data_source.type;
											//vstp_md.header.msg_id = sms_info.gt_called + ":" + sms_info.gt_calling + ":" + (sms_info.tcap_sid == null ? 0 : sms_info.tcap_sid) + ":" + (sms_info.tcap_did == null ? 0 : sms_info.tcap_did); 
											vstp_md.header.msg_type = net.vstp.MessageType.HLR;
											vstp_md.header.source = LocationType.SGN;
											// body
											vstp_md.values.put(VSTPDataItemType.HLR_ROUTING_TYPE.getId(), "1");
											// sccp part
											// gt address
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), gt_called_s);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), gt_calling_s);
											// gt gti
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(udt.calledPartyAddress.globalTitleIndicator.getId()));
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(udt.callingPartyAddress.globalTitleIndicator.getId()));
											// gt tt
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(Utils.get_called_gt_tt(udt)));
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(Utils.get_calling_gt_tt(udt)));
											// gt np
											tmp_np = Utils.get_called_gt_np(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), (tmp_np == null ? null : Integer.toString(tmp_np.getId())));
											tmp_np = Utils.get_calling_gt_np(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), (tmp_np == null ? null : Integer.toString(tmp_np.getId())));
											// gt nai
											tmp_nai = Utils.get_called_gt_nai(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), (tmp_nai == null ? null : Integer.toString(tmp_nai.getId())));
											tmp_nai = Utils.get_calling_gt_nai(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), (tmp_nai == null ? null : Integer.toString(tmp_nai.getId())));
											
											// m3ua & tcap
											vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
											vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));

											StatsManager.DECODER_STATS.SRI_BEGIN++;
											StatsManager.DECODER_STATS.TCAP_BEGIN++;
											// msg id
											vstp_md.header.msg_id = "-";
											vstp_md.values.put(VSTPDataItemType.TCAP_SID.getId(), Long.toString(Utils.bytes2num(tcm.get_begin().get_otid().value)));

											// Dialogue 
											if(tcm.get_begin().get_dialoguePortion() != null){
												byte[] tmp_oid = Utils.getTcapDialogueAppCtx(tcm.get_begin().get_dialoguePortion());
												if(tmp_oid != null){
													vstp_md.values.put(VSTPDataItemType.TCAP_DIALOGUE_OID.getId(),  Utils.decodeOID(tmp_oid));
												}
												
											}
											if(tcm.get_begin().get_components() != null){
												//invoke = tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter();
												invoke = Utils.get_INVOKE_PARAMETER(tcm);
												RoutingInfoForSM_Arg sm_arg = (RoutingInfoForSM_Arg)ber.decode(new RoutingInfoForSM_Arg(), invoke.getDataBytes());
												// MSISDN
												if(sm_arg.get_msisdn() != null){
													as = AddressString.decode(sm_arg.get_msisdn().value);
													vstp_md.values.put(VSTPDataItemType.HLR_MSISDN.getId(), TBCD.decode(as.digits));
													vstp_md.values.put(VSTPDataItemType.HLR_MSISDN_NAI.getId(), Integer.toString(as.typeOfNumber.getId()));
												}else{
													vstp_md.values.put(VSTPDataItemType.HLR_MSISDN.getId(), "");
													vstp_md.values.put(VSTPDataItemType.HLR_MSISDN_NAI.getId(), "0");
												}

												// SCA
												if(sm_arg.get_serviceCentreAddress() != null){
													vstp_md.values.put(VSTPDataItemType.HLR_SCA.getId(), TBCD.decode(AddressString.decode(sm_arg.get_serviceCentreAddress().value).digits));
												}else vstp_md.values.put(VSTPDataItemType.HLR_SCA.getId(), "");

												// invoke id
												//invoke_id = (int)Utils.bytes2num(tcm.get_begin().get_components().getChild(0).get_invoke().get_invokeID().value);
												invoke_id = (int)Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
												vstp_md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), Integer.toString(invoke_id));
											}
											// out queue
											SGNode.out_offer(vstp_md);
										// CH Begin SRI	
										}else if(Utils.isBegin_CH_SRI(tcm)){
											// VSTP packet
											// header
											vstp_md = new MessageDescriptor();
											vstp_md.header = new HeaderDescriptor();
											vstp_md.header.destination = LocationType.FN;
											vstp_md.header.ds = data_source.type;
											//vstp_md.header.msg_id = sms_info.gt_called + ":" + sms_info.gt_calling + ":" + (sms_info.tcap_sid == null ? 0 : sms_info.tcap_sid) + ":" + (sms_info.tcap_did == null ? 0 : sms_info.tcap_did); 
											vstp_md.header.msg_type = net.vstp.MessageType.HLR;
											vstp_md.header.source = LocationType.SGN;
											// body
											vstp_md.values.put(VSTPDataItemType.HLR_ROUTING_TYPE.getId(), "2");
											// sccp part
											// gt address
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId(), gt_called_s);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId(), gt_calling_s);
											// gt gti
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId(), Integer.toString(udt.calledPartyAddress.globalTitleIndicator.getId()));
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId(), Integer.toString(udt.callingPartyAddress.globalTitleIndicator.getId()));
											// gt tt
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_TT.getId(), Integer.toString(Utils.get_called_gt_tt(udt)));
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_TT.getId(), Integer.toString(Utils.get_calling_gt_tt(udt)));
											// gt np
											tmp_np = Utils.get_called_gt_np(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NP.getId(), (tmp_np == null ? null : Integer.toString(tmp_np.getId())));
											tmp_np = Utils.get_calling_gt_np(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NP.getId(), (tmp_np == null ? null : Integer.toString(tmp_np.getId())));
											// gt nai
											tmp_nai = Utils.get_called_gt_nai(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId(), (tmp_nai == null ? null : Integer.toString(tmp_nai.getId())));
											tmp_nai = Utils.get_calling_gt_nai(udt);
											vstp_md.values.put(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId(), (tmp_nai == null ? null : Integer.toString(tmp_nai.getId())));
											
											// m3ua & tcap
											vstp_md.values.put(VSTPDataItemType.M3UA_DPC.getId(), Integer.toString(dpc));
											vstp_md.values.put(VSTPDataItemType.M3UA_OPC.getId(), Integer.toString(opc));

											StatsManager.DECODER_STATS.SRI_BEGIN++;
											StatsManager.DECODER_STATS.TCAP_BEGIN++;
											// msg id
											vstp_md.header.msg_id = "-";
											vstp_md.values.put(VSTPDataItemType.TCAP_SID.getId(), Long.toString(Utils.bytes2num(tcm.get_begin().get_otid().value)));

											// Dialogue 
											if(tcm.get_begin().get_dialoguePortion() != null){
												byte[] tmp_oid = Utils.getTcapDialogueAppCtx(tcm.get_begin().get_dialoguePortion());
												if(tmp_oid != null){
													vstp_md.values.put(VSTPDataItemType.TCAP_DIALOGUE_OID.getId(),  Utils.decodeOID(tmp_oid));
												}
												
											}
											if(tcm.get_begin().get_components() != null){
												invoke = Utils.get_INVOKE_PARAMETER(tcm);
												if(invoke != null){
													SendRoutingInfoArg ch_arg = (SendRoutingInfoArg)ber.decode(new SendRoutingInfoArg(), invoke.getDataBytes());
													// MSISDN
													if(ch_arg.get_msisdn() != null){
														as = AddressString.decode(ch_arg.get_msisdn().value);
														vstp_md.values.put(VSTPDataItemType.HLR_MSISDN.getId(), TBCD.decode(as.digits));
														vstp_md.values.put(VSTPDataItemType.HLR_MSISDN_NAI.getId(), Integer.toString(as.typeOfNumber.getId()));
													}else{
														vstp_md.values.put(VSTPDataItemType.HLR_MSISDN.getId(), "");
														vstp_md.values.put(VSTPDataItemType.HLR_MSISDN_NAI.getId(), "0");
													}
													// GSMC
													if(ch_arg.get_gmsc_OrGsmSCF_Address() != null){
														as = AddressString.decode(ch_arg.get_gmsc_OrGsmSCF_Address().value);
														vstp_md.values.put(VSTPDataItemType.HLR_GSMC.getId(), TBCD.decode(as.digits));
														vstp_md.values.put(VSTPDataItemType.HLR_GSMC_NAI.getId(), Integer.toString(as.typeOfNumber.getId()));
													}else{
														vstp_md.values.put(VSTPDataItemType.HLR_GSMC.getId(), "");
														vstp_md.values.put(VSTPDataItemType.HLR_GSMC_NAI.getId(), "0");
													}
													
													
												}
												// invoke id
												//invoke_id = (int)Utils.bytes2num(tcm.get_begin().get_components().getChild(0).get_invoke().get_invokeID().value);
												invoke_id = (int)Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
												vstp_md.values.put(VSTPDataItemType.TCAP_INVOKE_ID.getId(), Integer.toString(invoke_id));
												
											}
											// out queue
											SGNode.out_offer(vstp_md);
											
										}
									}
								}
								tcap_did = null;
								tcap_sid = null;
							}							
							
						}else if(isup_ready && SGNConfigData.isup_status){
							isup_packet = ISUP.decode(packet.data);
							if(isup_packet != null){
								process_ISUP(isup_packet, isup_packet.cic, dpc, opc);
								
							}
						}else if(h248_ready && SGNConfigData.h248_status){
							StatsManager.DECODER_STATS.H248++;
							// TODO
						}else StatsManager.DECODER_STATS.SKIPPED_COUNT++;
						
					}catch(Exception e){
						e.printStackTrace();
						logger.error("Error while decoding packet!");
						Utils.bytes2file(packet.data, "/mnt/vazom_rt/" + System.currentTimeMillis() + ".raw");
					}
					// reset
					m3ua_data_packet = null;
					mtp3_data_packet = null;
					sccp_ready = false;
					isup_ready = false;
					h248_ready = false;
					
				}else{
					//logger.info(worker_name + " sleeping for 1000 msec!");
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
				
			}
			logger.info(worker_name + " ENDING!");
			
		}
	
	
	}
	public void stop(){
		stopping = true;
		
	}
	
	public PDS_DecoderWorker_MTP3_M3UA(int id, DSBase _data_source){
		data_source = _data_source;
		dw_r = new Dw_r();
		worker_name = "PDS_DECODER_WORKER_MTP3_M3UA_" + _data_source.dsd.id.toUpperCase() + "_" + id;
		dw_t = new Thread(dw_r, worker_name);
		dw_t.start();
		
		
	}
}
