package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IMSI_WithLMSI extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LMSI get_lmsi(){
		return (LMSI)elements.get(1).data;
	}
	public LMSI new_lmsi(){
		return new LMSI();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_lmsi(); }});
	}
	public IMSI_WithLMSI(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
