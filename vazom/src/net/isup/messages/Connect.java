package net.isup.messages;

import java.util.Arrays;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.BackwardCallIndicators;

public class Connect extends MessageBase {
	public BackwardCallIndicators backward_call_indicators;
	public Connect(){
		super();
		type = MessageType.CONNECT;
		pointers = new Pointer[1];
	}

	
	public void init(byte[] data) {
		if(data != null){
			backward_call_indicators = new BackwardCallIndicators();
			backward_call_indicators.init(Arrays.copyOfRange(data, 0, 2));
			byte_pos += 2;
			// pointers to optional
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			// Optional part
			// optional
			processOptional(data, 0);

			
		}
	}

}
