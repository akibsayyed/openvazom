package net.isup.messages;

import net.isup.MessageType;

public class UnblockingAck extends MessageBase {
	public UnblockingAck(){
		super();
		type = MessageType.UNBLOCKING_ACKNOWLEDGEMENT;
	}

	public void init(byte[] data) {
		// no data
	}

}
