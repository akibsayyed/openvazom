package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Invoke extends SEQUENCE{

	public InvokeIdType get_invokeID(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_invokeID(){
		return new InvokeIdType();
	}

	public InvokeIdType get_linkedID(){
		return (InvokeIdType)elements.get(1).data;
	}
	public InvokeIdType new_linkedID(){
		return new InvokeIdType();
	}

	public MAP_OPERATION get_opCode(){
		return (MAP_OPERATION)elements.get(2).data;
	}
	public MAP_OPERATION new_opCode(){
		return new MAP_OPERATION();
	}

	public InvokeParameter get_invokeparameter(){
		return (InvokeParameter)elements.get(3).data;
	}
	public InvokeParameter new_invokeparameter(){
		return new InvokeParameter();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeID(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_linkedID(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_opCode(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_invokeparameter(); }});
	}
	public Invoke(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
