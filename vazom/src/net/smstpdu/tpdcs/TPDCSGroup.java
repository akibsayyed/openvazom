package net.smstpdu.tpdcs;

import java.util.HashMap;

public enum TPDCSGroup {
	GENERAL(0x00),
	AUTOMATIC_DELETION_GROUP(0x40),
	MESSAGE_WAITING_DISCARD(0xC0),
	MESSAGE_WAITING_STORE(0xD0),
	MESSAGE_WAITING_STORE_UCS2(0xE0),
	DATA_ENCODING_MESSAGE(0xF0);
	
	// BITS 7 to 4  = XXXX0000
	
	private int id;
	private static final HashMap<Integer, TPDCSGroup> lookup = new HashMap<Integer, TPDCSGroup>();
	static{
		for(TPDCSGroup td : TPDCSGroup.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static TPDCSGroup get(int id){ return lookup.get(id); }
	private TPDCSGroup(int _id){ id = _id; }
	
}
