package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AllOrNone extends ENUMERATED{

	public static final int _notUsed =  0 ;
	public static final int _allChangesMustSucceedOrNoneShouldBeApplied = 1;
	public static final int _treatEachChangeIndependently = 2;
	public AllOrNone(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
