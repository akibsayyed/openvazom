package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RemoteUserInteractionDirective extends SET{

	public AnnouncementList get_announcementList(){
		return (AnnouncementList)elements.get(0).data;
	}
	public AnnouncementList new_announcementList(){
		return new AnnouncementList();
	}

	public DigitCollectionControl get_digitCollectionControl(){
		return (DigitCollectionControl)elements.get(1).data;
	}
	public DigitCollectionControl new_digitCollectionControl(){
		return new DigitCollectionControl();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(130, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_announcementList(); }});
		elements.add(new ElementDescriptor(139, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digitCollectionControl(); }});
	}
	public RemoteUserInteractionDirective(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
