package net.smpp;

import java.util.ArrayList;
import java.util.Arrays;

import net.smpp.pdu.Alert_notification;
import net.smpp.pdu.Bind_receiver;
import net.smpp.pdu.Bind_receiver_resp;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Bind_transceiver_resp;
import net.smpp.pdu.Bind_transmitter;
import net.smpp.pdu.Bind_transmitter_resp;
import net.smpp.pdu.Cancel_sm;
import net.smpp.pdu.Cancel_sm_resp;
import net.smpp.pdu.Deliver_sm;
import net.smpp.pdu.Deliver_sm_resp;
import net.smpp.pdu.Enquire_link;
import net.smpp.pdu.Enquire_link_resp;
import net.smpp.pdu.Generic_nack;
import net.smpp.pdu.Outbind;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Query_sm;
import net.smpp.pdu.Query_sm_resp;
import net.smpp.pdu.Submit_multi;
import net.smpp.pdu.Submit_multi_resp;
import net.smpp.pdu.Submit_sm;
import net.smpp.pdu.Submit_sm_resp;
import net.smpp.pdu.Unbind;
import net.smpp.pdu.Unbind_resp;
import net.utils.Utils;

public class SMPP_PDU {
	
	public static PDUBase[] decode(byte[] data){
		int byte_pos = 0;
		PDUBase res = null;
		PDUBase[] res_lst = null;
		ArrayList<PDUBase> res_buf = new ArrayList<PDUBase>();
		int tmp;
		PDUType pdut = null;
		while(byte_pos < data.length){
			tmp = ((data[byte_pos + 4] & 0xff) << 24) + ((data[byte_pos + 5] & 0xff) << 16) + ((data[byte_pos + 6] & 0xff) << 8) + (data[byte_pos + 7] & 0xff);
			pdut = PDUType.get(tmp);
			if(pdut != null){
				switch(pdut){
					case BIND_TRANSMITTER: res = new Bind_transmitter(); break;
					case BIND_TRANSMITTER_RESP: res = new Bind_transmitter_resp(); break;
					case BIND_RECEIVER: res = new Bind_receiver(); break;
					case BIND_RECEIVER_RESP: res = new Bind_receiver_resp(); break;
					case BIND_TRANSCEIVER: res = new Bind_transceiver(); break;
					case BIND_TRANSCEIVER_RESP: res = new Bind_transceiver_resp(); break;
					case OUTBIND: res = new Outbind(); break;
					case UNBIND: res = new Unbind(); break;
					case UNBIND_RESP: res = new Unbind_resp(); break;
					case GENERIC_NACK: res = new Generic_nack(); break;
					case SUBMT_SM: res = new Submit_sm(); break;
					case SUBMIT_SM_RESP: res = new Submit_sm_resp(); break;
					case SUBMIT_MULTI: res = new Submit_multi(); break;
					case SUBMIT_MULTI_RESP: res = new Submit_multi_resp(); break;
					case DELIVER_SM: res = new Deliver_sm(); break;
					case DELIVER_SM_RESP: res = new Deliver_sm_resp(); break;
					case ENQUIRE_LINK: res = new Enquire_link(); break;
					case ENQUIRE_LINK_RESP: res = new Enquire_link_resp(); break;
					case QUERY_SM: res = new Query_sm(); break;
					case QUERY_SM_RESP: res = new Query_sm_resp(); break;
					case CANCEL_SM: res = new Cancel_sm(); break;
					case CANCEL_SM_RESP: res = new Cancel_sm_resp(); break;
					case ALERT_NOTIFICATION: res = new Alert_notification(); break;
					
				}
				if(res != null){
					// init header
					tmp = ((data[byte_pos + 0] & 0xff) << 24) + ((data[byte_pos + 1] & 0xff) << 16) + ((data[byte_pos + 2] & 0xff) << 8) + (data[byte_pos + 3] & 0xff);
					res.command_length = tmp;
					res.command_id = res.type.getId();
					tmp = ((data[byte_pos + 8] & 0xff) << 24) + ((data[byte_pos + 9] & 0xff) << 16) + ((data[byte_pos + 10] & 0xff) << 8) + (data[byte_pos + 11] & 0xff);
					res.command_status = ErrorCodeType.get(tmp);
					tmp = ((data[byte_pos + 12] & 0xff) << 24) + ((data[byte_pos + 13] & 0xff) << 16) + ((data[byte_pos + 14] & 0xff) << 8) + (data[byte_pos + 15] & 0xff);
					res.sequence_number = tmp;
					res.init(Arrays.copyOfRange(data, byte_pos + 16, byte_pos + 16 + (int)res.command_length));
					res_buf.add(res);
					byte_pos += res.command_length;

				}
			// unknown PDU, stop
			}else{
				byte_pos = data.length;
			}
		}
		if(res_buf.size() > 0){
			res_lst = new PDUBase[res_buf.size()];
			for(int i = 0; i<res_buf.size(); i++) res_lst[i] = res_buf.get(i);
			return res_lst;
			
		}else return null;
	}

}
