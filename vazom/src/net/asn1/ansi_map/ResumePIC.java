package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ResumePIC extends ENUMERATED{

	public static final int _continue_Call_Processing = 1;
	public static final int _collect_Information_PIC = 2;
	public static final int _analyze_Information_PIC = 3;
	public static final int _select_Route_PIC = 4;
	public static final int _authorize_Origination_Attempt_PIC = 5;
	public static final int _authorize_Call_Setup_PIC = 6;
	public static final int _send_Call_PIC = 7;
	public static final int _o_Alerting_PIC = 8;
	public static final int _o_Active_PIC = 9;
	public static final int _o_Suspended_PIC = 10;
	public static final int _o_Null_PIC = 11;
	public static final int _select_Facility_PIC = 32;
	public static final int _present_Call_PIC = 33;
	public static final int _authorize_Termination_Attempt_PIC = 34;
	public static final int _t_Alerting_PIC = 35;
	public static final int _t_Active_PIC = 36;
	public static final int _t_Suspended_PIC = 37;
	public static final int _t_Null_PIC = 38;
	public ResumePIC(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
