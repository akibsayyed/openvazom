package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Credit;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.ProtocolClass;
import net.sccp.parameters.SeqSegm;
import net.sccp.parameters.Slr;

public class IT_InactivityTest extends MessageBase {
	public Dlr destinationLocalReference;
	public Slr sourceLocalReference;
	public ProtocolClass protocolClass;
	public SeqSegm sequencingSegmenting;
	public Credit credit;
	
	public IT_InactivityTest(){
		super();
		type = MessageType.IT_INACTIVITY_TEST;
	}
	

	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
		// Fixed mandatory
		sourceLocalReference = new Slr();
		sourceLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;

		// Fixed mandatory
		protocolClass = new ProtocolClass();
		protocolClass.init(new byte[]{_data[byte_pos++]});
		
		// Fixed mandatory
		sequencingSegmenting = new SeqSegm();
		sequencingSegmenting.init(new byte[]{_data[byte_pos++], _data[byte_pos++]});
		
		// Fixed mandatory
		credit = new Credit();
		credit.init(new byte[]{_data[byte_pos++]});
		
	}

}
