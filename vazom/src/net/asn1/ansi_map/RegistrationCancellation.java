package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RegistrationCancellation extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(1).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public CancellationType get_cancellationType(){
		return (CancellationType)elements.get(2).data;
	}
	public CancellationType new_cancellationType(){
		return new CancellationType();
	}

	public ControlChannelData get_controlChannelData(){
		return (ControlChannelData)elements.get(3).data;
	}
	public ControlChannelData new_controlChannelData(){
		return new ControlChannelData();
	}

	public ReceivedSignalQuality get_receivedSignalQuality(){
		return (ReceivedSignalQuality)elements.get(4).data;
	}
	public ReceivedSignalQuality new_receivedSignalQuality(){
		return new ReceivedSignalQuality();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(5).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SystemAccessData get_systemAccessData(){
		return (SystemAccessData)elements.get(6).data;
	}
	public SystemAccessData new_systemAccessData(){
		return new SystemAccessData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(85, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cancellationType(); }});
		elements.add(new ElementDescriptor(55, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelData(); }});
		elements.add(new ElementDescriptor(72, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_receivedSignalQuality(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(56, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemAccessData(); }});
	}
	public RegistrationCancellation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
