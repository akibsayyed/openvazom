package net.mtp3.messages.signalling;

import java.util.Arrays;

import net.mtp3.types.UnavailabilityCauseType;
import net.mtp3.types.UserPartIdentityType;
import net.utils.Utils;

public class UserPartUnavailable extends SignallingMessageBase {
	public int destination;
	public UserPartIdentityType userPartIdentity;
	public UnavailabilityCauseType unavailabilityCause;

	public UserPartUnavailable(){
		super();
	}
	public void init(byte[] data) {
		super.init(data);
		boolean[] tmp_bits = Utils.bytes2bits(Utils.reverse(Arrays.copyOfRange(data, byte_pos, byte_pos + (data.length - byte_pos))));;
		destination = Utils.bits2int(tmp_bits, 0, 14);
		userPartIdentity = UserPartIdentityType.get(Utils.bits2int(tmp_bits, 16, 4));
		unavailabilityCause = UnavailabilityCauseType.get(Utils.bits2int(tmp_bits, 20, 4));

	}
}
