package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class GENERALSTRING extends ASNType {

	public GENERALSTRING(){
		super();
		universalTag = ASNTag.BOOLEAN;
		tag = ASNTag.name2Tag(universalTag.toString());
		
	}
}
