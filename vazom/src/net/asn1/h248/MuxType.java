package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MuxType extends ENUMERATED{

	public static final int _h221 = 0;
	public static final int _h223 = 1;
	public static final int _h226 = 2;
	public static final int _v76 = 3;
	public static final int _nx64k = 4;
	public MuxType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
