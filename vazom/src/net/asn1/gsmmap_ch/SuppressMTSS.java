package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SuppressMTSS extends BIT_STRING{

	public static final int _suppressCUG = 0;
	public static final int _suppressCCBS = 1;
	public SuppressMTSS(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
