package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ForwardSM_Arg extends SEQUENCE{

	public SM_RP_DAold get_sm_RP_DA(){
		return (SM_RP_DAold)elements.get(0).data;
	}
	public SM_RP_DAold new_sm_RP_DA(){
		return new SM_RP_DAold();
	}

	public SM_RP_OAold get_sm_RP_OA(){
		return (SM_RP_OAold)elements.get(1).data;
	}
	public SM_RP_OAold new_sm_RP_OA(){
		return new SM_RP_OAold();
	}

	public SignalInfo get_sm_RP_UI(){
		return (SignalInfo)elements.get(2).data;
	}
	public SignalInfo new_sm_RP_UI(){
		return new SignalInfo();
	}

	public NULL get_moreMessagesToSend(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_moreMessagesToSend(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sm_RP_DA(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sm_RP_OA(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_sm_RP_UI(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_moreMessagesToSend(); }});
	}
	public ForwardSM_Arg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
