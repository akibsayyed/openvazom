package net.ds.smpp;

import net.ds.DataSourceType;
import net.smpp.ErrorCodeType;
import net.smpp.NumberingPlan;
import net.smpp.TypeOfNumber;
import net.smpp.parameters.Data_codingType;
import net.smpp.parameters.GSMNetworkSpecific;
import net.smpp.parameters.IntermediateNotificationType;
import net.smpp.parameters.MessageMode;
import net.smpp.parameters.MessageType;
import net.smpp.parameters.SMEOrigAckType;
import net.smpp.parameters.SMSCDeliveryReceiptType;
import net.smpp.parameters.ServiceType;
import net.vstp.VSTPDataItemType;

public class SMPPSmsInfo {
	public String ip_source;
	public String ip_destination;
	public int tcp_source;
	public int tcp_destination;
	public String system_id;
	public String password;
	public ServiceType service_type;
	public String source_addr;
	public TypeOfNumber source_addr_ton;
	public NumberingPlan source_addr_np;
	public String destination_addr;
	public TypeOfNumber destination_addr_ton;
	public NumberingPlan destination_addr_np;
	public MessageMode esm_message_mode;
	public MessageType esm_message_type;
	public GSMNetworkSpecific esm_gsm_features;
	public int protocol_id;
	public int priority_flag;
	public String delivery_time;
	public String validity_period;
	public SMSCDeliveryReceiptType rd_smsc_receipt;
	public SMEOrigAckType rd_sme_ack;
	public IntermediateNotificationType rd_intermediate_notification;
	public int replace_if_present_flag;
	public int sm_default_msg_id;
	public long sequence_number;
	
	public Data_codingType data_coding;
	public String short_message;
	public int message_length;
	public DataSourceType dataSource;
	// UDH
	public boolean udh_present;
	public int sar_msg_ref_num;
	public int sar_total_segments;
	public int sar_segment_seqnum;
	
	public ErrorCodeType status_code;

}
