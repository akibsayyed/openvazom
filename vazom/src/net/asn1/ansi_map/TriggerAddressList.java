package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TriggerAddressList extends SET{

	public TriggerList get_triggerList(){
		return (TriggerList)elements.get(0).data;
	}
	public TriggerList new_triggerList(){
		return new TriggerList();
	}

	public TriggerList get_triggerListOpt(){
		return (TriggerList)elements.get(1).data;
	}
	public TriggerList new_triggerListOpt(){
		return new TriggerList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(278, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_triggerList(); }});
		elements.add(new ElementDescriptor(278, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerListOpt(); }});
	}
	public TriggerAddressList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
