package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class LIRMode extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _unconditionally_Restricted = 1;
	public static final int _pre_Authorized_LCS_Clients_Only = 2;
	public static final int _pre_Authorized_LCS_Clients_and_User_Authorized_LCS_Clients = 3;
	public static final int _unrestricted = 4;
	public LIRMode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
