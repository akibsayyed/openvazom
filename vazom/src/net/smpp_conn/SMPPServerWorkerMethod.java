package net.smpp_conn;

import net.routing.RoutingConnectionWorkerMethod;
import net.sgn.FNManager;
import net.smpp.ErrorCodeType;
import net.smpp.SMPP_PDU;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Bind_transceiver_resp;
import net.smpp.pdu.PDUBase;
import net.utils.Utils;
import net.vstp.VSTP_SGF_CorrelationManager;
import net.vstp.VSTP_SGF_CorrelationPacket;

public class SMPPServerWorkerMethod extends RoutingConnectionWorkerMethod {
	SMPPConnection smpp_conn = null;

	public SMPPServerWorkerMethod(SMPPConnection conn){
		super(conn);
		smpp_conn = (SMPPConnection)conn;
		
	}
	public void execute(Object o) {
		byte[] data = null;
		SMPPPayload spl = null;
		SMPP_In_Packet in_packet = null;
		SMPPPacket smppp = null;
		Bind_transceiver bind_transceiver = null;
		VSTP_SGF_CorrelationPacket vsp = null;
		ErrorCodeType tmp_ec = null;
		if(o != null){
			in_packet = (SMPP_In_Packet)o;
			data = in_packet.data;
			PDUBase[] pdu = SMPP_PDU.decode(data);
			for(int i = 0; i<pdu.length; i++){
				// save last sequence
				in_packet.client_conn.last_sequence = pdu[i].sequence_number;
				// process smpp pdu
				switch(pdu[i].type){
					case BIND_TRANSCEIVER:
						bind_transceiver = (Bind_transceiver)pdu[i];
						in_packet.client_conn.smpp_system_id = bind_transceiver.system_id;
						in_packet.client_conn.smpp_password = bind_transceiver.password;
						// send direct reply
						// user auth
						tmp_ec = SMPPConnManager.auth_smpp_user(bind_transceiver.system_id, bind_transceiver.password);
						in_packet.client_conn.user_auth = (tmp_ec == ErrorCodeType.ESME_ROK);
						// send reply
						Bind_transceiver_resp bt_resp = new Bind_transceiver_resp();
						bt_resp.command_status = tmp_ec;
						bt_resp.sequence_number = bind_transceiver.sequence_number + 1;
						bt_resp.system_id = bind_transceiver.system_id;
						in_packet.client_conn.out_queue.offer(bt_resp.encode());
						break;
						
					case SUBMT_SM:
						// check if user authenticated
						if(in_packet.client_conn.user_auth){
							smppp = new SMPPPacket();
							smppp.client_ip = in_packet.client_conn.con_ip;
							smppp.client_port = in_packet.client_conn.con_port;
							smppp.host_ip = in_packet.client_conn.host_ip;
							smppp.host_port = in_packet.client_conn.host_port;
							smppp.system_id = in_packet.client_conn.smpp_system_id;
							smppp.password = in_packet.client_conn.smpp_password;
							smppp.smpp_pdu = pdu[i];
							smppp.smpp_data = data;
							if(FNManager.fn_connected){
								SMPPConnManager.processFilter_sms(smppp);
								vsp = VSTP_SGF_CorrelationManager.get(smppp.client_ip + "." + smppp.client_port + "." + smppp.smpp_pdu.sequence_number);
								if(vsp != null){
									vsp.callback_required = true;
									vsp.callback = new SMPPCallback(in_packet.client_conn);
									//vsp.extra_params.add(in_packet.client_conn);
								}
							// no filter, send error
							}else{
								PDUBase smpp_spam_reply = Utils.genereate_SMPP_Spam_reply(smppp.smpp_pdu);
								in_packet.client_conn.out_queue.offer(smpp_spam_reply.encode());

							}
							
						}
						break;
						
				}
			}
		}
	}

}
