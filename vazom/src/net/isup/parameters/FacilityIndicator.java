package net.isup.parameters;

import net.isup.ParameterType;

public class FacilityIndicator extends ParameterBase {
	public boolean user_to_user_service;
	public FacilityIndicator(){
		type = ParameterType.FACILITY_INDICATOR;
	}

	public void init(byte[] data) {
		if(data != null){
			user_to_user_service = data[byte_position]  == 0x02;
		}
	}

}
