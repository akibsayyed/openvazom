package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReadyForSM_Arg extends SEQUENCE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public AlertReason get_alertReason(){
		return (AlertReason)elements.get(1).data;
	}
	public AlertReason new_alertReason(){
		return new AlertReason();
	}

	public NULL get_alertReasonIndicator(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_alertReasonIndicator(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(3).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public NULL get_additionalAlertReasonIndicator(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_additionalAlertReasonIndicator(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_alertReason(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_alertReasonIndicator(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_additionalAlertReasonIndicator(); }});
	}
	public ReadyForSM_Arg(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
