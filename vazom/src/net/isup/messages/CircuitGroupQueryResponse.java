package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.CircuitStateIndicator;
import net.isup.parameters.RangeAndStatus;

public class CircuitGroupQueryResponse extends MessageBase {
	public RangeAndStatus range_and_status;
	public CircuitStateIndicator circuit_state_indicator;
	
	public CircuitGroupQueryResponse(){
		super();
		type = MessageType.CIRCUIT_GROUP_QUERY_RESPONSE;
		pointers = new Pointer[2];
	}

	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			pointers[1] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			range_and_status = new RangeAndStatus();
			range_and_status.init(getPointerData(data, 0));
			
			circuit_state_indicator = new CircuitStateIndicator();
			circuit_state_indicator.init(getPointerData(data, 1));

		}
		
	}

}
