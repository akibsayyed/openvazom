package net.smsfs.list;

import java.util.Arrays;
import java.util.List;

import net.smsfs.FilterMode;

public abstract class ListBase {
	public ListType list_type;
	public String list_type_id;
	public ListDescriptor list_desc;
	public ListDescriptor tmp_list_desc;
	
	public void fill_tmp(){
		if(tmp_list_desc != null){
			for(ListItemBase li : list_desc.list){
				tmp_list_desc.add(li.data, false);
			}
			
		}
	}
	
	public void on_add(byte[] data){}
	public void on_delete(byte[] data){}
	
	private void _add(byte[] value, ListDescriptor lst, boolean explicit){
		if(lst != null){
			lst.add(value, explicit);
		}
	}
	
	public void add(byte[] value, boolean temp, boolean explicit){
		if(temp) _add(value, tmp_list_desc, explicit);
		else _add(value, list_desc, explicit);
		
	}
	public void add(byte[] value, FilterMode fm, boolean explicit){
		if(fm == FilterMode.REFILTER) _add(value, tmp_list_desc, explicit);
		else _add(value, list_desc, explicit);
		
	}
	
	
	private void _remove(byte[] value, List<ListItemBase> lst){
		if(lst != null){
			for(ListItemBase li : lst){
				if(Arrays.equals(li.data, value)){
					li.deleted = true;
					//lst.remove(li);
				} 
				
			}
		}
	}
	
	public void remove(byte[] value, boolean temp){
		if(temp) _remove(value, tmp_list_desc.list);
		else _remove(value, list_desc.list);
		
	}
	public void remove(byte[] value, FilterMode fm){
		if(fm == FilterMode.REFILTER) _remove(value, tmp_list_desc.list);
		else _remove(value, list_desc.list);
		
	}
	
	private ListItemBase _exists(byte[] value, List<ListItemBase> lst){
		if(lst != null){
			for(ListItemBase li : lst) if(!li.deleted){
				if(Arrays.equals(li.data, value)) return li;
			
			}			
			
		}
		return null;
	}

	public ListItemBase exists(byte[] value, boolean temp){
		if(temp) return _exists(value, tmp_list_desc.list);
		else return _exists(value, list_desc.list);
	}
	public ListItemBase exists(byte[] value, FilterMode fm){
		if(fm == FilterMode.REFILTER) return _exists(value, tmp_list_desc.list);
		else return _exists(value, list_desc.list);
	}
	
	public List<ListItemBase> get_list(FilterMode fm){
		if(fm == FilterMode.REFILTER) return tmp_list_desc.list; else return list_desc.list;
	}

	public List<ListItemBase> get_list(boolean temp){
		if(temp) return tmp_list_desc.list; else return list_desc.list;
	}
	
	public ListDescriptor get_list_desc(boolean temp){
		if(temp) return tmp_list_desc; else return list_desc;
	}
	public ListDescriptor get_list_desc(FilterMode fm){
		if(fm == FilterMode.REFILTER) return tmp_list_desc; else return list_desc;
	}

	
	private void _clear(ListDescriptor lst){
		if(lst != null){
			lst.clear();
			
		}
	}
	public void clear(boolean temp){
		if(temp) _clear(tmp_list_desc); else _clear(list_desc);
	}
	
	public ListBase(boolean has_tmp_lst, int _max_size){
		list_desc = new ListDescriptor(_max_size);
		if(has_tmp_lst) tmp_list_desc = new ListDescriptor(_max_size);
		
	}
	
}
