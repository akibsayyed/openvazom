package net.sccp.parameters.global_title;

import java.util.ArrayList;
import java.util.Arrays;

public class GlobalTitle_TT extends GlobalTitleBase {
	public int translationType;
	public byte[] addressInformation;
	
	public GlobalTitle_TT(){
		super();
		type = GlobalTitleIndicator.TRANSLATION_TYPE_ONLY;
	}
	public byte[] encode(){
		byte[] res = null;
		ArrayList<Byte> packet = new ArrayList<Byte>();
		packet.add((byte)translationType);
		for(int i = 0; i<addressInformation.length; i++) packet.add(addressInformation[i]);
		res = new byte[packet.size()];
		for(int i = 0; i<packet.size(); i++) res[i] = packet.get(i);
		return res;
	}	

	public void init(byte[] data) {
		translationType = data[0] & 0xFF;
		addressInformation = Arrays.copyOfRange(data, 1, data.length);
		
	}

}
