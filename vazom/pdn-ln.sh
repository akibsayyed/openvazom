version=$1
dist=$2
cd $dist/pdn/lib
ln -s ../../lib/utils-${version}.jar
ln -s ../../lib/asn1-${version}.jar
ln -s ../../lib/m3ua-${version}.jar
ln -s ../../lib/sccp-${version}.jar
ln -s ../../lib/sctp-${version}.jar
ln -s ../../lib/smstpdu-${version}.jar
ln -s ../../lib/mtp3-${version}.jar
ln -s ../../lib/stats-${version}.jar
ln -s ../../lib/smpp-${version}.jar
ln -s ../../lib/gpu-${version}.jar
ln -s ../../lib/gpu2-${version}.jar
ln -s ../../lib/vstp-${version}.jar
ln -s ../../lib/ds-${version}.jar
ln -s ../../lib/config-${version}.jar
ln -s ../../lib/log4j-1.2.16.jar
ln -s ../../lib/cli-${version}.jar
ln -s ../../lib/log4jconfig-${version}.jar
ln -s ../../lib/security-${version}.jar
ln -s ../../lib/hasp-srm-api.jar
ln -s ../../lib/logging-${version}.jar