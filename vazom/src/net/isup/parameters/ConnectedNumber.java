package net.isup.parameters;

import java.util.Arrays;

import net.isup.ParameterType;
import net.isup.parameters.cldpn.NatureOfAddressIndicatorType;
import net.isup.parameters.cldpn.NumberingPlanIndicatorType;
import net.isup.parameters.clgpn.AddressPresentationRestrictedIndicatorType;
import net.isup.parameters.clgpn.ScreeningIndicatorType;

public class ConnectedNumber extends ParameterBase {
	public boolean odd_number_of_address_signals;
	public NatureOfAddressIndicatorType nature_of_address_indicator;
	public NumberingPlanIndicatorType numbering_plan_indicator;
	public AddressPresentationRestrictedIndicatorType address_presentation_restricted_indicator;
	public ScreeningIndicatorType screening_indicator;
	public byte[] address_signal;
	
	public ConnectedNumber(){
		type = ParameterType.CONNECTED_NUMBER;
	}

	public void init(byte[] data) {
		if(data != null){
			odd_number_of_address_signals = (data[byte_position] & 0x80) == 0x80;
			nature_of_address_indicator = NatureOfAddressIndicatorType.get(data[byte_position] & 0x70);
			address_presentation_restricted_indicator = AddressPresentationRestrictedIndicatorType.get(data[byte_position] & 0x0c);
			screening_indicator = ScreeningIndicatorType.get(data[byte_position] & 0x03);
			byte_position++;
			address_signal = Arrays.copyOfRange(data, byte_position, data.length);
			
		}
	}

}
