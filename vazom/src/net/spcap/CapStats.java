package net.spcap;

public class CapStats {
	public long ps_recv;		/* number of packets received */
	public long ps_drop;		/* number of packets dropped */
	public long ps_ifdrop;		/* drops by interface -- only supported on some platforms */
	public long q_full;
	
	
	public CapStats(long recv, long drop, long ifdrop, long _q_full){
		ps_recv = recv;
		ps_drop = drop;
		ps_ifdrop = ifdrop;
		q_full = _q_full;
	}
}
