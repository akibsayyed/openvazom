package net.smstpdu.tpdcs;

import net.smstpdu.tpdcs.general.Alphabet;

public abstract class TPDCS {
	public TPDCSGroup group;
	public Alphabet encoding;
	
	public byte encode(){
		return 0;
	}
	public static TPDCS decode(byte data){
		TPDCS res = null;
		// first check bits 7..4
		TPDCSGroup g = TPDCSGroup.get(data & 0xF0);
		//System.out.println(g);
		// if not found, check bits 7 and 6(GENERAL group uses only bits 7 and 6 for selection) 
		if(g == null) g = TPDCSGroup.get(data & 0xC0);
		//System.out.println(String.format("%02x", data));
		//System.out.println("!!!!!!!!!!!!!!! " + (data & 0xC0));
		if(g != null){
			switch(g){
				case GENERAL:
					res = new TPDCS_General();
					break;
				case AUTOMATIC_DELETION_GROUP:
					res = new TPDCS_General();
					break;
				case DATA_ENCODING_MESSAGE:
					res = new TPDCS_DataEncoding();
					break;
				case MESSAGE_WAITING_DISCARD:
					res = new TPDCS_Discard();
					break;
				case MESSAGE_WAITING_STORE:
					res = new TPDCS_Store();
					break;
				case MESSAGE_WAITING_STORE_UCS2:
					res = new TPDCS_StoreUCS2();
					break;
			
			}
		}
		if(res != null) res.init(data);
		
		
		return res;
	}
	public abstract void init(byte data);

}
