package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class ASPIdentifier extends M3UAParameter {
	public int aspIdentifier;
	
	public ASPIdentifier(){
		type = M3UAParameterType.ASP_IDENTIFIER;
	}
	

	public void init(byte[] data) {
		value = data;
		aspIdentifier = (data[0] << 24) + (data[1] << 16) + (data[2] << 8) + (data[3] & 0xFF);
	}




}
