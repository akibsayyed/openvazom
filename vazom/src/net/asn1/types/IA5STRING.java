package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class IA5STRING extends ASNType {

	public IA5STRING(){
		super();
		universalTag = ASNTag.IA5STRING;
		tag = ASNTag.name2Tag(universalTag.toString());
		
	}
}
