drop table packet;
drop table filter;
alter table sms add column filter_total_score int null after mode_id;
alter table sms add column filter_action_id int null after mode_id;
alter table sms add index fk_sms_filter_action (filter_action_id);
alter table sms add constraint fk_sms_filter_action foreign key (filter_action_id) references filter_action (id) on delete no action on update no action;
