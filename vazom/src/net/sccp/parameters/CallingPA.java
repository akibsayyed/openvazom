package net.sccp.parameters;

import java.util.ArrayList;
import java.util.Arrays;

import net.sccp.RoutingIndicator;
import net.sccp.SignallingPointCode;
import net.sccp.SubsystemNumber;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitleIndicator;

public class CallingPA extends ParamBase {
	public RoutingIndicator routingIndicator;
	public GlobalTitleIndicator globalTitleIndicator;
	public boolean SSNIndicator;
	public boolean pointCodeIndicator;
	public SignallingPointCode pointCode;
	public SubsystemNumber subsystemNumber;
	public GlobalTitleBase globalTitle;

	public CallingPA(){
		super();
		type = ParamType.CALLING_PARTY_ADDRESS;
	}
	public byte[] encode(){
		byte[] res = null;
		byte[] tmp_buff = null;
		int tmp;
		ArrayList<Byte> packet = new ArrayList<Byte>();
		tmp = routingIndicator.getId() + globalTitleIndicator.getId() + (SSNIndicator ? 0x02 : 0x00) + (pointCodeIndicator ? 0x01 : 0x00);
		packet.add((byte)0x00); // length, calculated later
		packet.add((byte)tmp);
		packet.add((byte)subsystemNumber.getId());
		
		tmp_buff = globalTitle.encode();
		// set length
		packet.set(0, (byte)(tmp_buff.length + 2));
		// add GT part
		for(int i = 0; i<tmp_buff.length; i++) packet.add(tmp_buff[i]);
		
		
		res = new byte[packet.size()];
		for(int i = 0; i<packet.size(); i++) res[i] = packet.get(i);
		return res;
	}
	
	public void init(byte[] data){
		int byte_pos = 0;
		routingIndicator = RoutingIndicator.get(data[byte_pos] & 0x40);
		globalTitleIndicator = GlobalTitleIndicator.get(data[byte_pos] & 0x3C);
		SSNIndicator = (data[byte_pos] & 0x02) == 0x02;
		pointCodeIndicator = (data[byte_pos] & 0x01) == 0x01;
		byte_pos++;
		
		// Point Code
		if(pointCodeIndicator){
			pointCode = new SignallingPointCode(new byte[]{data[byte_pos++], data[byte_pos++]});
		}
		// Subsystem Number
		if(SSNIndicator) subsystemNumber = SubsystemNumber.get(data[byte_pos++] & 0xFF);
		
		// Global Title
		globalTitle = GlobalTitleBase.decode(Arrays.copyOfRange(data, byte_pos, data.length), globalTitleIndicator);
		
		
		
	}
	
}
