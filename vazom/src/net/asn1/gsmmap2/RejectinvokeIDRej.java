package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RejectinvokeIDRej extends CHOICE{

	public InvokeIdType get_erivable(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_erivable(){
		return new InvokeIdType();
	}

	public NULL get_not_derivable(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_not_derivable(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_erivable(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_not_derivable(); }});
	}
	public RejectinvokeIDRej(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
