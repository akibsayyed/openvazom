package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PDP_Type extends OCTET_STRING{

	public PDP_Type(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
