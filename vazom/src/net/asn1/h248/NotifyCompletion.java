package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NotifyCompletion extends BIT_STRING{

	public static final int _onTimeOut = 0;
	public static final int _onInterruptByEvent = 1;
	public static final int _onInterruptByNewSignalDescr = 2;
	public static final int _otherReason = 3;
	public static final int _onIteration = 4;
	public NotifyCompletion(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
