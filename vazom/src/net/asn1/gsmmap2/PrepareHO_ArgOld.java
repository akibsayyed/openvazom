package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PrepareHO_ArgOld extends SEQUENCE{

	public GlobalCellId get_targetCellId(){
		return (GlobalCellId)elements.get(0).data;
	}
	public GlobalCellId new_targetCellId(){
		return new GlobalCellId();
	}

	public NULL get_ho_NumberNotRequired(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_ho_NumberNotRequired(){
		return new NULL();
	}

	public Bss_APDU get_bss_APDU(){
		return (Bss_APDU)elements.get(2).data;
	}
	public Bss_APDU new_bss_APDU(){
		return new Bss_APDU();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_targetCellId(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_ho_NumberNotRequired(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_bss_APDU(); }});
	}
	public PrepareHO_ArgOld(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
