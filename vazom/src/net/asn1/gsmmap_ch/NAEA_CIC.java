package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NAEA_CIC extends OCTET_STRING{

	public NAEA_CIC(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
