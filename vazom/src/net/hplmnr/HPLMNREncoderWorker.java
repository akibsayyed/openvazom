package net.hplmnr;

import java.util.ArrayList;

import net.asn1.compiler.ElementDescriptor;
import net.ber.BerTag;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
//import net.db.DBRecordType;
//import net.dr.DRManager;
//import net.ds.DataSourceType;
import net.logging.LoggingManager;
import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.m3ua.parameters.NetworkAppearance;
import net.m3ua.parameters.protocol_data.ServiceIndicatorType;
import net.routing.RoutingConnectionType;
import net.sccp.MessageType;
import net.sccp.NatureOfAddress;
import net.sccp.RoutingIndicator;
import net.sccp.SCCP;
import net.sccp.SubsystemNumber;
import net.sccp.messages.UDT_UnitData;
import net.sccp.parameters.global_title.EncodingScheme;
import net.sccp.parameters.global_title.GlobalTitleBase;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.GlobalTitle_TTNPENOA;
import net.sccp.parameters.global_title.NumberingPlan;
import net.sccp.parameters.protocol_class.MessageHandling;
import net.sccp.parameters.protocol_class.ProtocolClassType;
import net.sctp.SSCTPDescriptor;
import net.smstpdu.TBCD;
import net.smstpdu.TypeOfNumber;
import net.stats.StatsManager;
import net.utils.Utils;

import org.apache.log4j.Logger;

public class HPLMNREncoderWorker {
	public static Logger logger=Logger.getLogger(HPLMNREncoderWorker.class);
	public int worker_id;
	private Enc_r enc_r;
	private Thread enc_t;
	private boolean stopping;
	private BerTranscoderv2 ber = new BerTranscoderv2();
	
	private class Enc_r implements Runnable{
		HPLMNRPacket sri_packet;
		SSCTPDescriptor sd = null;
		//byte[] data = null;
		public void run() {
			LoggingManager.info(logger, "Starting SRI Encoder thread!");
			while(!stopping){
				sri_packet = HPLMNRManager.sri_queue.poll();
				if(sri_packet != null){
					sd = new SSCTPDescriptor(sri_packet.ssctp_sid, null);
					try{
						switch(sri_packet.replyType){
							// SRI packets REQUEST/ACK/ERR
							case SRI_PHASE_1: 
								sd.payload = encodePhase_1_sri(sri_packet); 
								StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_REQUEST++;
								// DR
								//DRManager.offerDRP(DataSourceType.SCTP, DBRecordType.SRI, sd.payload);
								break;
							case SRI_PHASE_1_REPORT_SM:
								sd.payload = encodeReport_sm_sri(sri_packet);
								StatsManager.HPLMNR_STATS.HPLMNR_HLR_OUT_REQUEST++;
								break;
							case SRI_PHASE_2_ACK: 
								sd.payload = encodePhase_2_sri(sri_packet); 
								StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ACK++;
								// DR
								//DRManager.offerDRP(DataSourceType.SCTP, DBRecordType.SRI, sd.payload);
								break;
							case SRI_PHASE_2_REPORT_SM_ACK: 
								sd.payload = encodePhase_2_report_sm(sri_packet); 
								StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ACK++;
								// DR
								//DRManager.offerDRP(DataSourceType.SCTP, DBRecordType.SRI, sd.payload);
								break;
							case SRI_PHASE_2_ERR: 
								sd.payload = encodePhase_2_sri_err(sri_packet);
								StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ERR++;
								// DR
								//DRManager.offerDRP(DataSourceType.SCTP, DBRecordType.SRI, sd.payload);
								break;
							case SRI_PHASE_2_ABORT: 
								sd.payload = encodePhase_2_sri_abort(sri_packet);
								StatsManager.HPLMNR_STATS.HPLMNR_HLR_IN_REPLY_ABORT++;
								// DR
								//DRManager.offerDRP(DataSourceType.SCTP, DBRecordType.SRI, sd.payload);
								break;
								
							// MT packets BEGIN/CONTINUE
							case MT_PHASE_1_BEGIN: 
								StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_BEGIN++; 
								sd.payload = encodePhase_1_mt(sri_packet, sri_packet.replyType);
								break;
							case MT_PHASE_1_CONTINUE:
								StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_CONTINUE++; 
								sd.payload = encodePhase_1_mt(sri_packet, sri_packet.replyType);
								break;
							case MT_PHASE_1_END:
								StatsManager.HPLMNR_STATS.HPLMNR_MT_OUT_END++; 
								sd.payload = encodePhase_1_mt(sri_packet, sri_packet.replyType); 
								break;
							// TCAP packets BEGIN/CONTINUE/END
							case MT_PHASE_1_TCAP_BEGIN: 
								StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_BEGIN++; 
								sd.payload = encodePhase_1_tcap(sri_packet, sri_packet.replyType); 
								break;
							case MT_PHASE_1_TCAP_CONTINUE:
								StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_CONTINUE++; 
								sd.payload = encodePhase_1_tcap(sri_packet, sri_packet.replyType); 
								break;
							case MT_PHASE_1_TCAP_END: 
								StatsManager.HPLMNR_STATS.HPLMNR_MT_TCAP_OUT_END++; 
								sd.payload = encodePhase_1_tcap(sri_packet, sri_packet.replyType); 
								break;
								
							// MT ACK/ERR
							case MT_PHASE_2_ACK:
								StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_ACK++; 
								sd.payload = encodePhase_2_mt(sri_packet); 
								break;
							case MT_PHASE_2_ERR:
								StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_ERR++; 
								sd.payload = encodePhase_2_mt(sri_packet); 
								break;
							case MT_PHASE_2_TCAP: 
								StatsManager.HPLMNR_STATS.HPLMNR_MT_IN_TCAP_REPLY++; 
								sd.payload = encodePhase_2_mt(sri_packet); 
								break;
								
							// MO
							case MO_PHASE_1: 
								StatsManager.HPLMNR_STATS.HPLMNR_MO_OUT++;
								sd.payload = encodePhase_1_mo(sri_packet); 
								break;
							case MO_PHASE_2: 
								StatsManager.HPLMNR_STATS.HPLMNR_MO_IN_REPLY++;
								sd.payload = encodePhase_2_mo(sri_packet); 
								break;
								
						}
						// send to SCTP out queue
						if(sd.payload != null){
							LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Sending packet type: [" + sri_packet.replyType + "]!");
							//System.out.println("----- PACKET START -----");
							//System.out.println(Utils.getHexStr(sd.payload));
							//System.out.println("----- PACKET END -----");
							//SRIManager.outOffer(sd);
							
							// current m3ua connection
							sd.extra_params = new ArrayList<Object>();
							sd.extra_params.add(sri_packet.extra_params.get(0));
							// process queue
							HPLMNRManager.processFilter(sd, sri_packet);
						}

					}catch(Exception e){
						e.printStackTrace();
						LoggingManager.error(logger, "HPLMNREncoderWorker: Error while encoding packet!");
					}

					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					
					
				}
				
			}
			LoggingManager.info(logger, "Ending SRI Encoder thread!");
		}
	}
	
	
	private byte[] encodePhase_2_mo(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		
		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_mo_sccp_called_ssn;// SubsystemNumber.MSC;//ConfigData.sri_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt.encodingScheme =  (sri.calledPartyGT.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt.addressInformation = TBCD.encode(sri.calledPartyGT);

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_mo_sccp_calling_ssn;//SubsystemNumber.MSC;// ConfigData.sri_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_mo_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_mo_smsrouter_gt);
		ns.data = sri.tcap_packet;
		sccp_packet = ns.encode();
		sri.sccp_udt = ns;
		
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		ndd.protocolData.setUserProtocolData(sccp_packet);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		m3ua_packet = ndata.encode();
		sri.m3ua_packet = ndata;

		// MTP3
		if(sri.routingType == RoutingConnectionType.MTP3){
			sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
			res = sri.mtp3_packet.encode();
		// M3UA
		}else{
			res = m3ua_packet;
			
		}
		
		return res;
	
	}
	
	
	private byte[] encodePhase_1_mo(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;

		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_mo_sccp_called_ssn;// SubsystemNumber.MSC;//SGNConfigData.sri_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt.encodingScheme =  (sri.calledPartyGT.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt.addressInformation = TBCD.encode(sri.calledPartyGT);// TBCD.encode(TBCD.decode(sri.msisdn.digits));

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_mo_sccp_calling_ssn;//SubsystemNumber.MSC;// SGNConfigData.sri_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_mo_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_mo_smsrouter_gt);
		ns.data = sri.tcap_packet;
		sccp_packet = ns.encode();
		sri.sccp_udt = ns;
		
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		ndd.protocolData.setUserProtocolData(sccp_packet);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		m3ua_packet = ndata.encode();
		sri.m3ua_packet = ndata;
	
		// MTP3
		if(sri.routingType == RoutingConnectionType.MTP3){
			sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
			res = sri.mtp3_packet.encode();
		// M3UA
		}else{
			res = m3ua_packet;
			
		}

		return res;
	}
	
	private byte[] encodePhase_2_sri_abort(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		byte[] tcap_packet = null;
		BerTag b_dportion = null;
		HPLMNRCorrelationPacket sri_corr = HPLMNRManager.SRICorrelationGet(Utils.bytes2num(sri.tcap_did));
		
		if(sri_corr != null){
			sri.sri_tcm.get_abort().get_dtid().value = sri_corr.tcap_sid;
			// encode TCAP
			ber.clearLengths(sri.sri_tcm.berTag);
			ber.prepareLenghts(sri.sri_tcm.berTag);
			tcap_packet = ber.encode(sri.sri_tcm.berTag);
			
			// encode SCCP
			UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
			ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
			ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
			// Called party
			ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.calledPartyAddress.SSNIndicator = true;
			ns.calledPartyAddress.pointCodeIndicator = false;
			ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
			ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Called GT
			GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
			ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
			ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt.encodingScheme = (sri_corr.sms_gms_c.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(sri_corr.sms_gms_c);

			// Calling party
			ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.callingPartyAddress.SSNIndicator = true;
			ns.callingPartyAddress.pointCodeIndicator = false;
			ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
			ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Calling GT
			GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
			ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
			ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt2.encodingScheme = (sri_corr.msisdn.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
			
			// SCCP data
			ns.data = tcap_packet;
			sccp_packet = ns.encode();
			sri.sccp_udt = ns;
			
			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			DATA ndd = (DATA)ndata.message;
			ndd.protocolData.setOPC(0);
			ndd.protocolData.setDPC(0);
			ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
			ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
			ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
			ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
			ndd.protocolData.setUserProtocolData(sccp_packet);
			ndd.networkAppearance = new NetworkAppearance();
			ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
			m3ua_packet = ndata.encode();
			sri.m3ua_packet = ndata;
		
			// MTP3
			if(sri.routingType == RoutingConnectionType.MTP3){
				sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
				res = sri.mtp3_packet.encode();
			// M3UA
			}else{
				res = m3ua_packet;
				
			}
			
		
			// Remove SRI correlation
			HPLMNRManager.SRICorrelationRemove(Utils.bytes2num(sri.tcap_did));
		}else{
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI Correlation ID(TCAP_ID) not found!: [" + Utils.bytes2num(sri.tcap_did) + "]");
		}
		return res;
	}

	private byte[] encodePhase_2_sri_err(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		byte[] tcap_packet = null;
		BerTag b_dportion = null;
		HPLMNRCorrelationPacket sri_corr = HPLMNRManager.SRICorrelationGet(Utils.bytes2num(sri.tcap_did));
		
		if(sri_corr != null){
			sri.sri_tcm.get_end().get_dtid().value = sri_corr.tcap_sid;
			sri.sri_gsmmap_err.get_invokeID().value = sri_corr.invoke_id;
/*
			if(sri_corr.hasDialogue){
				//DialoguePortion
				DialoguePortion dportion = new DialoguePortion();
				
				// remove old DialoguePortion if it exists
				if(sri.sri_tcm.get_end().get_dialoguePortion() != null){
					b_dportion = BerTag.createNew(dportion, null, null);
					sri.sri_tcm.get_end().get_dialoguePortion().berTag.setBerTag(b_dportion);
				// create new one at index 1(between dTid and Components
				}else b_dportion = BerTag.createNew(dportion, sri.sri_tcm.get_end().berTag, 1, null);
				
				
					// EXTERNAL
					EXTERNAL external = new EXTERNAL();
					BerTag b_external = BerTag.createNew(external, b_dportion, null);
						
						// OID
						OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
						BerTag b_oid = BerTag.createNew(oid, b_external, null);
						oid.value = new byte[]{0x00, 0x11, (byte)0x86, 0x05, 0x01, 0x01, 0x01};
						
						// External Encoding
						EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
						BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
						
							// single asn1 type
							ASNType single_type = new ASNType();
							BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
							
								// DialoguePDU
								DialoguePDU dpdu = new DialoguePDU();
								BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
							
									// DialogueResponse
									AARE_apdu dresponse = new AARE_apdu(); 
									BerTag b_dresponse = BerTag.createNew(dresponse, b_dpdu, dpdu.elements.get(1));
									
										// Protocol Version
										BIT_STRING pversion = new BIT_STRING();
										BerTag b_pversion = BerTag.createNew(pversion, b_dresponse, dresponse.elements.get(0));
										pversion.value = new byte[]{0x07, (byte)0x80};
										
										
										// Application Context Name EXPLICIT
										ASNType appctx_ex = new ASNType();
										BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_dresponse, dresponse.elements.get(1));
										
											// Application Context Name
											OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
											BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
											appctx.value = sri_corr.app_ctx_oid;// new byte[]{0x04, 0x00, 0x00, 0x01, 0x00, 0x14, 0x02};
										
										
										// Result EXPLICIT
										ASNType asres_ex = new ASNType();
										asres_ex.asn_pc = ASNTagComplexity.Constructed;
										BerTag b_asres_ex = BerTag.createNew(asres_ex, b_dresponse, dresponse.elements.get(2));
										
										
											// Result
											Associate_result asres = new Associate_result();
											BerTag b_asres = BerTag.createNew(asres, b_asres_ex, null);
											asres.value = Utils.num2bytes(Associate_result._accepted);

											
										// Source Diagnostic EXPLICIT
										ASNType asd_ex = new ASNType();
										asd_ex.asn_pc = ASNTagComplexity.Constructed;
										BerTag b_asd_ex = BerTag.createNew(asd_ex, b_dresponse, dresponse.elements.get(3));
											
											
											// Source Diagnostic
											Associate_source_diagnostic asd = new Associate_source_diagnostic();
											BerTag b_asd = BerTag.createNew(asd, b_asd_ex, null);
										
												// Dialogue service user EXLICIT
												ASNType dsui_ex = new ASNType();
												dsui_ex.asn_pc = ASNTagComplexity.Constructed;
												BerTag b_dsui_ex = BerTag.createNew(dsui_ex, b_asd, asd.elements.get(0));
												
											
													// Dialogue service user
													Dialogue_service_userInteger dsui = new Dialogue_service_userInteger();
													BerTag b_dsui = BerTag.createNew(dsui, b_dsui_ex, null);
													dsui.value = Utils.num2bytes(Dialogue_service_userInteger._null);
					
				
			}
			*/
			// Parent defined tag values
			// TCAP ReturnError and MAP ReturnError are differnet
			// Replacing TCAP ReturnError with MAP ReturnError
			ElementDescriptor ed = sri.sri_tcm.get_end().get_components().getChild(0).elements.get(2);
			sri.sri_tcm.get_end().get_components().getChild(0).get_returnError().berTag.setBerTag(sri.sri_gsmmap_err.berTag, ed);
			
			// encode TCAP
			ber.clearLengths(sri.sri_tcm.berTag);
			ber.prepareLenghts(sri.sri_tcm.berTag);
			tcap_packet = ber.encode(sri.sri_tcm.berTag);
			
			// encode SCCP
			UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
			ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
			ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
			// Called party
			ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.calledPartyAddress.SSNIndicator = true;
			ns.calledPartyAddress.pointCodeIndicator = false;
			ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
			ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Called GT
			GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
			ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
			ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt.encodingScheme = (sri_corr.sms_gms_c.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(sri_corr.sms_gms_c);

			// Calling party
			ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.callingPartyAddress.SSNIndicator = true;
			ns.callingPartyAddress.pointCodeIndicator = false;
			ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
			ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Calling GT
			GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
			ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
			ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt2.encodingScheme = (sri_corr.msisdn.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
			
			// SCCP data
			ns.data = tcap_packet;
			sccp_packet = ns.encode();
			sri.sccp_udt = ns;
			
			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			DATA ndd = (DATA)ndata.message;
			ndd.protocolData.setOPC(0);
			ndd.protocolData.setDPC(0);
			ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
			ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
			ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
			ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
			ndd.protocolData.setUserProtocolData(sccp_packet);
			ndd.networkAppearance = new NetworkAppearance();
			ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
			m3ua_packet = ndata.encode();
			sri.m3ua_packet = ndata;

			// MTP3
			if(sri.routingType == RoutingConnectionType.MTP3){
				sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
				res = sri.mtp3_packet.encode();
			// M3UA
			}else{
				res = m3ua_packet;
				
			}
		
			// Remove SRI correlation
			HPLMNRManager.SRICorrelationRemove(Utils.bytes2num(sri.tcap_did));
		}else{
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger,"SRI Correlation ID(TCAP_ID) not found!: [" + Utils.bytes2num(sri.tcap_did) + "]");
		}
		return res;
	}
	
	
	private byte[] encodePhase_2_mt(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber =  SubsystemNumber.MSC;//SGNConfigData.sri_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt.encodingScheme =  (sri.callingPartyGT.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt.addressInformation = TBCD.encode(sri.callingPartyGT);

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SubsystemNumber.MSC;// SGNConfigData.sri_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_nnn_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_nnn_gt);
		ns.data = sri.tcap_packet;
		sccp_packet = ns.encode();
		sri.sccp_udt = ns;
		
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		ndd.protocolData.setUserProtocolData(sccp_packet);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		m3ua_packet = ndata.encode();
		sri.m3ua_packet = ndata;

		// MTP3
		if(sri.routingType == RoutingConnectionType.MTP3){
			sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
			res = sri.mtp3_packet.encode();
		// M3UA
		}else{
			res = m3ua_packet;
			
		}
		
		return res;
	}	
	private byte[] encodePhase_1_tcap(HPLMNRPacket sri, HPLMNRReplyType type){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;

		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber =  SubsystemNumber.MSC;//SGNConfigData.sri_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt.encodingScheme =  (sri.calledPartyGT.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt.addressInformation = TBCD.encode(sri.calledPartyGT);// TBCD.encode(TBCD.decode(sri.msisdn.digits));

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SubsystemNumber.MSC;// SGNConfigData.sri_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_nnn_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_nnn_gt);
		ns.data = sri.tcap_packet;
		sccp_packet = ns.encode();
		sri.sccp_udt = ns;
		
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		ndd.protocolData.setUserProtocolData(sccp_packet);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		m3ua_packet = ndata.encode();
		sri.m3ua_packet = ndata;

		// MTP3
		if(sri.routingType == RoutingConnectionType.MTP3){
			sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
			res = sri.mtp3_packet.encode();
		// M3UA
		}else{
			res = m3ua_packet;
			
		}

		return res;
	}
	private byte[] encodePhase_1_mt(HPLMNRPacket sri, HPLMNRReplyType type){
		byte[] res = null;
		byte[] tcap_packet = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		//String mts_id = "";
		
		MTCorrelationPacket mt_corr = HPLMNRManager.MTCorrelationGet(Long.parseLong(sri.imsi));

		if(mt_corr != null){
			// MO
			if(sri.mo_arg != null){
				// BEGIN
				if(type == HPLMNRReplyType.MT_PHASE_1_BEGIN)
					Utils.get_INVOKE_PARAMETER(sri.mt_tcm).berTag.setBerTag(sri.mo_arg.berTag);
					//sri.mt_tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter().berTag.setBerTag(sri.mo_arg.berTag);
				// CONTINUE
				else if(type == HPLMNRReplyType.MT_PHASE_1_CONTINUE)
					//sri.mt_tcm.get_continue().get_components().getChild(0).get_invoke().get_parameter().berTag.setBerTag(sri.mo_arg.berTag);
					Utils.get_INVOKE_PARAMETER(sri.mt_tcm).berTag.setBerTag(sri.mo_arg.berTag);
				// END
				else if(type == HPLMNRReplyType.MT_PHASE_1_END)
					//sri.mt_tcm.get_end().get_components().getChild(0).get_invoke().get_parameter().berTag.setBerTag(sri.mo_arg.berTag);
					Utils.get_INVOKE_PARAMETER(sri.mt_tcm).berTag.setBerTag(sri.mo_arg.berTag);

				
				// IMSI
				sri.mo_arg.get_sm_RP_DA().get_imsi().value = TBCD.encode(mt_corr.imsi, 0x0f);

				
			// MT
			}else if(sri.mt_arg != null){
				// BEGIN
				if(type == HPLMNRReplyType.MT_PHASE_1_BEGIN)
					//sri.mt_tcm.get_begin().get_components().getChild(0).get_invoke().get_parameter().berTag.setBerTag(sri.mt_arg.berTag);
					Utils.get_INVOKE_PARAMETER(sri.mt_tcm).berTag.setBerTag(sri.mt_arg.berTag);
				// CONTINUE
				else if(type == HPLMNRReplyType.MT_PHASE_1_CONTINUE)
					//sri.mt_tcm.get_continue().get_components().getChild(0).get_invoke().get_parameter().berTag.setBerTag(sri.mt_arg.berTag);
					Utils.get_INVOKE_PARAMETER(sri.mt_tcm).berTag.setBerTag(sri.mt_arg.berTag);
				// END
				else if(type == HPLMNRReplyType.MT_PHASE_1_END)
					//sri.mt_tcm.get_end().get_components().getChild(0).get_invoke().get_parameter().berTag.setBerTag(sri.mt_arg.berTag);
					Utils.get_INVOKE_PARAMETER(sri.mt_tcm).berTag.setBerTag(sri.mt_arg.berTag);

					
				// IMSI
				sri.mt_arg.get_sm_RP_DA().get_imsi().value = TBCD.encode(mt_corr.imsi, 0x0f);
			}

			// encode TCAP
			ber.clearLengths(sri.mt_tcm.berTag);
			ber.prepareLenghts(sri.mt_tcm.berTag);
			tcap_packet = ber.encode(sri.mt_tcm.berTag);
			
			// encode SCCP
			UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
			ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
			ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
			// Called party
			ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.calledPartyAddress.SSNIndicator = true;
			ns.calledPartyAddress.pointCodeIndicator = false;
			ns.calledPartyAddress.subsystemNumber =  SubsystemNumber.MSC;//SGNConfigData.sri_sccp_called_ssn;
			ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Called GT
			GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
			ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
			ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt.encodingScheme =  (TBCD.decode(mt_corr.nnn.digits).length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(TBCD.decode(mt_corr.nnn.digits));// TBCD.encode(TBCD.decode(sri.msisdn.digits));

			// Calling party
			ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.callingPartyAddress.SSNIndicator = true;
			ns.callingPartyAddress.pointCodeIndicator = false;
			ns.callingPartyAddress.subsystemNumber = SubsystemNumber.MSC;// SGNConfigData.sri_sccp_calling_ssn;
			ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Calling GT
			GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
			ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
			ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt2.encodingScheme = (SGNConfigData.hplmnr_nnn_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_nnn_gt);
			ns.data = tcap_packet;
			sccp_packet = ns.encode();
			sri.sccp_udt = ns;
	
			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			DATA ndd = (DATA)ndata.message;
			ndd.protocolData.setOPC(0);
			ndd.protocolData.setDPC(0);
			ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
			ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
			ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
			ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
			ndd.protocolData.setUserProtocolData(sccp_packet);
			ndd.networkAppearance = new NetworkAppearance();
			ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
			m3ua_packet = ndata.encode();
			sri.m3ua_packet = ndata;
			// MTP3
			if(sri.routingType == RoutingConnectionType.MTP3){
				sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
				res = sri.mtp3_packet.encode();
			// M3UA
			}else{
				res = m3ua_packet;
				
			}
			// Remove MT correlation
			HPLMNRManager.MTCorrelationRemove(Long.parseLong(sri.imsi));
			
		}else LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "MT Correlation IMSI not found!: [" + sri.imsi + "]");
		
		return res;
	}


	private byte[] encodeReport_sm_sri(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		HPLMNRCorrelationPacket sri_corr = new HPLMNRCorrelationPacket();
		
		sri_corr.tcap_sid = sri.tcap_sid.clone();
		sri_corr.sms_gms_c = sri.callingPartyGT;
		sri_corr.msisdn = TBCD.decode(sri.msisdn.digits);
		sri_corr.invoke_id = sri.invoke_id;
		//sri_corr.hasDialogue = sri.hasDialogue;
		//sri_corr.app_ctx_oid = sri.app_ctx_oid;
		HPLMNRManager.SRICorrelationPut(Utils.bytes2num(sri.tcap_sid), sri_corr);
	

		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		// check for HLR GT Override flag
		if(SGNConfigData.hplmnr_hlr_gt_override_status){
			ngt.encodingScheme = (SGNConfigData.hplmnr_hlr_gt_override.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(SGNConfigData.hplmnr_hlr_gt_override);
			
		}else{
			ngt.encodingScheme =  (TBCD.decode(sri.msisdn.digits).length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(TBCD.decode(sri.msisdn.digits));
			
		}

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
		ns.data = sri.tcap_packet;
		sccp_packet = ns.encode();
		sri.sccp_udt = ns;
	
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		ndd.protocolData.setUserProtocolData(sccp_packet);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		m3ua_packet = ndata.encode();
		sri.m3ua_packet = ndata;
		// MTP3
		if(sri.routingType == RoutingConnectionType.MTP3){
			sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
			res = sri.mtp3_packet.encode();
		// M3UA
		}else{
			res = m3ua_packet;
			
		}
		return res;
	}
	
	
	
	private byte[] encodePhase_1_sri(HPLMNRPacket sri){
		byte[] res = null;
		byte[] tcap_packet = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		HPLMNRCorrelationPacket sri_corr = new HPLMNRCorrelationPacket();
		
		sri_corr.tcap_sid = sri.tcap_sid.clone();
		sri_corr.sms_gms_c = sri.callingPartyGT;
		sri_corr.msisdn = TBCD.decode(sri.msisdn.digits);
		sri_corr.invoke_id = sri.invoke_id;
		//sri_corr.hasDialogue = sri.hasDialogue;
		//sri_corr.app_ctx_oid = sri.app_ctx_oid;
		
		/*
		// TCM
		TCMessage tcm = new TCMessage();
		BerTag b_root = BerTag.createNew(tcm, null, null);
		
			// BEGIN
			Begin begin = new Begin();
			BerTag b_begin = BerTag.createNew(begin, b_root, tcm.elements.get(1));
			//Continue cont = new Continue();
			//BerTag b_cont = BerTag.createNew(cont, b_root, tcm.elements.get(3));
		
				// Source Transaction ID
				OrigTransactionID sid = new OrigTransactionID();
				BerTag b_sid = BerTag.createNew(sid, b_begin, null);
				//sid.value = new byte[]{(byte)(SRIManager.NEXT_TCAP_ID >> 24), (byte)(SRIManager.NEXT_TCAP_ID >> 16), (byte)(SRIManager.NEXT_TCAP_ID >> 8), (byte)(SRIManager.NEXT_TCAP_ID)};
				sid.value = Utils.num2bytes(SRIManager.getNext_TCAP_ID()); //new byte[]{(byte)(SRIManager.getNext_TCAP_ID())};
				//sid.value = new byte[]{0x11, 0x00, (byte)0xcc, (byte)0xab};
				
				// add to correlation list
				SRIManager.SRICorrelationPut(SRIManager.getNext_TCAP_ID(), sri_corr);
				//SRIManager.correlation_map.put(SRIManager.getNext_TCAP_ID(), sri_corr);
				SRIManager.consume_TCAP_ID();
				
				// Dest Transaction ID
				//DestTransactionID did = new DestTransactionID();
				//BerTag b_did = BerTag.createNew(did, b_cont, null);
				//sid.value = new byte[]{(byte)(SRIManager.NEXT_TCAP_ID >> 24), (byte)(SRIManager.NEXT_TCAP_ID >> 16), (byte)(SRIManager.NEXT_TCAP_ID >> 8), (byte)(SRIManager.NEXT_TCAP_ID)};
				//did.value = sri.tcap_sid;
				//SRIManager.NEXT_TCAP_ID++;
	
				// Components
				ComponentPortion components = new ComponentPortion();
				BerTag b_components = BerTag.createNew(components, b_begin, null);
	
					// Compoent
					Component component = new Component();
					BerTag b_component = BerTag.createNew(component, b_components, null);
	
						// Invoke
						Invoke invoke = new Invoke();
						BerTag b_invoke = BerTag.createNew(invoke, b_component, component.elements.get(0));
	
							// Invoke ID
							InvokeIdType invoke_id = new InvokeIdType();
							BerTag b_invoke_id = BerTag.createNew(invoke_id, b_invoke, null);
							invoke_id.value = new byte[]{1};
	
							// opCode
							MAP_OPERATION opCode = new MAP_OPERATION();
							BerTag b_opCode = BerTag.createNew(opCode, b_invoke, null);
							
	
								// localValue
								OperationLocalvalue localValue = new OperationLocalvalue();
								BerTag b_localValue = BerTag.createNew(localValue, b_opCode, null);
								localValue.value = new byte[]{GSMMAPOperationLocalvalue._sendRoutingInfoForSM};
	
							// RoutingInfoForSM_Arg
							RoutingInfoForSM_Arg sm_arg = new RoutingInfoForSM_Arg();
							BerTag b_sm_arg = BerTag.createNew(sm_arg, b_invoke, null);
	
								// MSISDN
								ISDN_AddressString msisdn = new ISDN_AddressString();
								BerTag b_msisdn = BerTag.createNew(msisdn, b_sm_arg, sm_arg.elements.get(0));
								msisdn.value = net.smstpdu.AddressString.encode(sri.msisdn);
								
								// SM-RP-PRI
								BOOLEAN sm_rp_pri = new BOOLEAN();
								BerTag b_sm_rp_pri = BerTag.createNew(sm_rp_pri, b_sm_arg, sm_arg.elements.get(1));
								sm_rp_pri.value = new byte[]{1};
								
								// Service Centre Address
								AddressString sca = new AddressString();
								BerTag b_sca = BerTag.createNew(sca, b_sm_arg, sm_arg.elements.get(2));
								sca.value = net.smstpdu.AddressString.encode(sri.smsc);
		
		*/
		
		sri.sri_tcm.get_begin().get_otid().value = sri.tcap_sid; //Utils.num2bytes(SRIManager.getNext_TCAP_ID());
		// add to correlation list
		//SRIManager.SRICorrelationPut(SRIManager.getNext_TCAP_ID(), sri_corr);
		HPLMNRManager.SRICorrelationPut(Utils.bytes2num(sri.tcap_sid), sri_corr);
		
		//SRIManager.consume_TCAP_ID();
		
		
		// encode TCAP
		ber.clearLengths(sri.sri_tcm.berTag);
		ber.prepareLenghts(sri.sri_tcm.berTag);
		tcap_packet = ber.encode(sri.sri_tcm.berTag);

		// encode SCCP
		UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
		ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
		ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
		// Called party
		ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.calledPartyAddress.SSNIndicator = true;
		ns.calledPartyAddress.pointCodeIndicator = false;
		ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
		ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Called GT
		GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
		ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
		ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		// check for HLR GT Override flag
		if(SGNConfigData.hplmnr_hlr_gt_override_status){
			ngt.encodingScheme = (SGNConfigData.hplmnr_hlr_gt_override.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(SGNConfigData.hplmnr_hlr_gt_override);
			
		}else{
			ngt.encodingScheme =  (TBCD.decode(sri.msisdn.digits).length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(TBCD.decode(sri.msisdn.digits));
			
		}

		// Calling party
		ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
		ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
		ns.callingPartyAddress.SSNIndicator = true;
		ns.callingPartyAddress.pointCodeIndicator = false;
		ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
		ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
		// Calling GT
		GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
		ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
		ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
		ngt2.encodingScheme = (SGNConfigData.hplmnr_smsrouter_gt.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
		ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
		ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
		ns.data = tcap_packet;
		sccp_packet = ns.encode();
		sri.sccp_udt = ns;
		
		// M3UA
		M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
		DATA ndd = (DATA)ndata.message;
		ndd.protocolData.setOPC(0);
		ndd.protocolData.setDPC(0);
		ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
		ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
		ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
		ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
		//ndd.protocolData.setSLS(((DATA)sri.m3ua_packet.message).protocolData.signallingLinkSelectionCode);
		ndd.protocolData.setUserProtocolData(sccp_packet);
		ndd.networkAppearance = new NetworkAppearance();
		ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
		m3ua_packet = ndata.encode();
		sri.m3ua_packet = ndata;
		// MTP3
		if(sri.routingType == RoutingConnectionType.MTP3){
			sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
			res = sri.mtp3_packet.encode();
		// M3UA
		}else{
			res = m3ua_packet;
			
		}
		return res;
	}

	private byte[] encodePhase_2_report_sm(HPLMNRPacket sri){
		byte[] res = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		HPLMNRCorrelationPacket sri_corr = HPLMNRManager.SRICorrelationGet(Utils.bytes2num(sri.tcap_did));
		if(sri_corr != null){
			// encode SCCP
			UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
			ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
			ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
			// Called party
			ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.calledPartyAddress.SSNIndicator = true;
			ns.calledPartyAddress.pointCodeIndicator = false;
			ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
			ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Called GT
			GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
			ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
			ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt.encodingScheme = (sri_corr.sms_gms_c.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(sri_corr.sms_gms_c);

			// Calling party
			ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.callingPartyAddress.SSNIndicator = true;
			ns.callingPartyAddress.pointCodeIndicator = false;
			ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
			ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Calling GT
			GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
			ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
			ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt2.encodingScheme = (sri_corr.msisdn.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
			
			// SCCP data
			ns.data = sri.tcap_packet;
			sccp_packet = ns.encode();
			sri.sccp_udt = ns;

			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			DATA ndd = (DATA)ndata.message;
			ndd.protocolData.setOPC(0);
			ndd.protocolData.setDPC(0);
			ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
			ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
			ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
			ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
			ndd.protocolData.setUserProtocolData(sccp_packet);
			ndd.networkAppearance = new NetworkAppearance();
			ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
			m3ua_packet = ndata.encode();
			sri.m3ua_packet = ndata;
	
			// MTP3
			if(sri.routingType == RoutingConnectionType.MTP3){
				sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
				res = sri.mtp3_packet.encode();
			// M3UA
			}else{
				res = m3ua_packet;
				
			}
			// Remove SRI correlation
			HPLMNRManager.SRICorrelationRemove(Utils.bytes2num(sri.tcap_did));
		}else{
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI Correlation ID(TCAP_ID) not found!: [" + Utils.bytes2num(sri.tcap_did) + "]");

		}
		
		
		return res;
	}
	
	private byte[] encodePhase_2_sri(HPLMNRPacket sri){
		byte[] res = null;
		byte[] tcap_packet = null;
		byte[] sccp_packet = null;
		byte[] m3ua_packet = null;
		ElementDescriptor ed = null;
		MTCorrelationPacket mt_corr = null;
		HPLMNRCorrelationPacket sri_corr = HPLMNRManager.SRICorrelationGet(Utils.bytes2num(sri.tcap_did));// correlation_map.get(Utils.bytes2num(sri.tcap_did));
		if(sri_corr != null){
			// save imsi
			sri_corr.imsi = sri.imsi;
			
	/*		

			// TCM
			TCMessage tcm = new TCMessage();
			BerTag b_root = BerTag.createNew(tcm, null, null);
			
				// End
				End end = new End();
				BerTag b_end = BerTag.createNew(end, b_root, tcm.elements.get(2));
			
					// Dest Transaction ID
					DestTransactionID did = new DestTransactionID();
					BerTag b_did = BerTag.createNew(did, b_end, null);
					//did.value = new byte[]{0x11, 0x00, (byte)0xcc, (byte)0xab};
					did.value = sri_corr.tcap_sid; 
					if(sri_corr.hasDialogue){
						//DialoguePortion
						DialoguePortion dportion = new DialoguePortion();
						BerTag b_dportion = BerTag.createNew(dportion, b_end, null);
						
							// EXTERNAL
							EXTERNAL external = new EXTERNAL();
							BerTag b_external = BerTag.createNew(external, b_dportion, null);
								
								// OID
								OBJECT_IDENTIFIER oid = new OBJECT_IDENTIFIER();
								BerTag b_oid = BerTag.createNew(oid, b_external, null);
								oid.value = new byte[]{0x00, 0x11, (byte)0x86, 0x05, 0x01, 0x01, 0x01};
								
								// External Encoding
								EXTERNAL_ENCODING eenc = new EXTERNAL_ENCODING();
								BerTag b_eenc = BerTag.createNew(eenc, b_external, null);
								
									// single asn1 type
									ASNType single_type = new ASNType();
									BerTag b_single_type = BerTag.createNew(single_type, b_eenc, eenc.elements.get(0));
									
										// DialoguePDU
										DialoguePDU dpdu = new DialoguePDU();
										BerTag b_dpdu = BerTag.createNew(dpdu, b_single_type, null);
									
											// DialogueResponse
											AARE_apdu dresponse = new AARE_apdu(); 
											BerTag b_dresponse = BerTag.createNew(dresponse, b_dpdu, dpdu.elements.get(1));
											
												// Protocol Version
												BIT_STRING pversion = new BIT_STRING();
												BerTag b_pversion = BerTag.createNew(pversion, b_dresponse, dresponse.elements.get(0));
												pversion.value = new byte[]{0x07, (byte)0x80};
												
												
												// Application Context Name EXPLICIT
												ASNType appctx_ex = new ASNType();
												BerTag b_appctx_ex = BerTag.createNew(appctx_ex, b_dresponse, dresponse.elements.get(1));
												
													// Application Context Name
													OBJECT_IDENTIFIER appctx = new OBJECT_IDENTIFIER();
													BerTag b_appctx = BerTag.createNew(appctx, b_appctx_ex, null);
													appctx.value = sri_corr.app_ctx_oid;// new byte[]{0x04, 0x00, 0x00, 0x01, 0x00, 0x14, 0x02};
												
												
												// Result EXPLICIT
												ASNType asres_ex = new ASNType();
												asres_ex.asn_pc = ASNTagComplexity.Constructed;
												BerTag b_asres_ex = BerTag.createNew(asres_ex, b_dresponse, dresponse.elements.get(2));
												
												
													// Result
													Associate_result asres = new Associate_result();
													BerTag b_asres = BerTag.createNew(asres, b_asres_ex, null);
													asres.value = Utils.num2bytes(Associate_result._accepted);

													
												// Source Diagnostic EXPLICIT
												ASNType asd_ex = new ASNType();
												asd_ex.asn_pc = ASNTagComplexity.Constructed;
												BerTag b_asd_ex = BerTag.createNew(asd_ex, b_dresponse, dresponse.elements.get(3));
													
													
													// Source Diagnostic
													Associate_source_diagnostic asd = new Associate_source_diagnostic();
													BerTag b_asd = BerTag.createNew(asd, b_asd_ex, null);
												
														// Dialogue service user EXLICIT
														ASNType dsui_ex = new ASNType();
														dsui_ex.asn_pc = ASNTagComplexity.Constructed;
														BerTag b_dsui_ex = BerTag.createNew(dsui_ex, b_asd, asd.elements.get(0));
														
													
															// Dialogue service user
															Dialogue_service_userInteger dsui = new Dialogue_service_userInteger();
															BerTag b_dsui = BerTag.createNew(dsui, b_dsui_ex, null);
															dsui.value = Utils.num2bytes(Dialogue_service_userInteger._null);
						
					}
						
					
		
					// Components
					ComponentPortion components = new ComponentPortion();
					BerTag b_components = BerTag.createNew(components, b_end, null);
		
						// Component
						Component component = new Component();
						BerTag b_component = BerTag.createNew(component, b_components, null);
		
							// ReturnResultLast
							ReturnResult rrlast = new ReturnResult();
							BerTag b_rrlast = BerTag.createNew(rrlast, b_component, component.elements.get(1));
		
								// Invoke ID
								InvokeIdType invoke_id = new InvokeIdType();
								BerTag b_invoke_id = BerTag.createNew(invoke_id, b_rrlast, null);
								//invoke_id.value = new byte[]{1};
								invoke_id.value = sri_corr.invoke_id;
								
								ReturnResultResult rrr = new ReturnResultResult();
								BerTag b_rrr = BerTag.createNew(rrr, b_rrlast, null);
		
									// opCode
									MAP_OPERATION opCode = new MAP_OPERATION();
									BerTag b_opCode = BerTag.createNew(opCode, b_rrr, null);
			
										// localValue
										OperationLocalvalue localValue = new OperationLocalvalue();
										BerTag b_localValue = BerTag.createNew(localValue, b_opCode, null);
										localValue.value = new byte[]{GSMMAPOperationLocalvalue._sendRoutingInfoForSM};
		
									// RoutingInfoForSM_Res
									RoutingInfoForSM_Res sm_res = new RoutingInfoForSM_Res();
									BerTag b_sm_res = BerTag.createNew(sm_res, b_rrr, null);
									
										// imsi
										IMSI imsi = new IMSI();
										BerTag b_imsi = BerTag.createNew(imsi, b_sm_res, null);
										String tmp_imsi_id = Integer.toString(SGNConfigData.sri_imsi_correlation_mcc) + 
															Integer.toString(SGNConfigData.sri_imsi_correlation_mnc) + 
															Long.toString(SRIManager.getNext_IMSI_CORRELATION_ID());
										imsi.value = TBCD.encode(tmp_imsi_id, 0x0f);//TBCD.encode(sri_corr.imsi, 0x0f);
										sri_corr.imsi_correlation_id = Long.parseLong(tmp_imsi_id);
										// MT correlation
										mt_corr.imsi = sri_corr.imsi;
										mt_corr.nnn = sri.networkNodeNumber;
										mt_corr.annn = sri.additonalNumber;
										SRIManager.MTCorrelationPut(Long.parseLong(tmp_imsi_id), mt_corr);
										SRIManager.consume_IMSI_CORRELATION_ID();
										
										
										// LocationInfoWithLMSI
										LocationInfoWithLMSI lwlmsi = new LocationInfoWithLMSI();
										BerTag b_lwlmsi = BerTag.createNew(lwlmsi, b_sm_res, sm_res.elements.get(1));
										
											// Network Node Number
											AddressString nnn = new AddressString();
											BerTag b_nnn = BerTag.createNew(nnn, b_lwlmsi, lwlmsi.elements.get(0));
											net.smstpdu.AddressString as = new net.smstpdu.AddressString();
											as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
											as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
											as.digits = TBCD.encode(SGNConfigData.sri_nnn_gt, 0x0f);
											nnn.value = net.smstpdu.AddressString.encode(as);
											
											// HACK START
											//INTEGER hack = new INTEGER();
											//BerTag b_hack = BerTag.createNew(hack, b_lwlmsi, lwlmsi.elements.get(5));
											//hack.value = new byte[]{0};
											// HACK END
											
										
											
							/*
						// Component
						Component component2 = new Component();
						BerTag b_component2 = BerTag.createNew(component2, b_components, null);

							Invoke invoke = new Invoke();
							BerTag b_invoke = BerTag.createNew(invoke, b_component2, component2.elements.get(0));
										
								// Invoke ID
								InvokeIdType invoke_id2 = new InvokeIdType();
								BerTag b_invoke_id2 = BerTag.createNew(invoke_id2, b_invoke, null);
								invoke_id2.value = new byte[]{2};
								
								// opCode
								MAP_OPERATION opCode2 = new MAP_OPERATION();
								BerTag b_opCode2 = BerTag.createNew(opCode2, b_invoke, null);
		
									// localValue
									OperationLocalvalue localValue2 = new OperationLocalvalue();
									BerTag b_localValue2 = BerTag.createNew(localValue2, b_opCode2, null);
									localValue2.value = new byte[]{GSMMAPOperationLocalvalue._informServiceCentre};
								
								// InformServiceCentreArg
								InformServiceCentreArg isca = new InformServiceCentreArg();
								BerTag b_isca = BerTag.createNew(isca, b_invoke, null);
										
									// MW status
									MW_Status mws = new MW_Status();
									BerTag b_mws = BerTag.createNew(mws, b_isca, null);
									mws.value = new byte[]{0x02, (byte)0x80};
		
									
							*/		
			if(TBCD.decode(sri.networkNodeNumber.digits).trim().equals(SGNConfigData.hplmnr_nnn_routable_gt)){
				mt_corr = new MTCorrelationPacket();
				
				// IMSI
				String tmp_imsi_id = Integer.toString(SGNConfigData.hplmnr_imsi_correlation_mcc) + Integer.toString(SGNConfigData.hplmnr_imsi_correlation_mnc) + Long.toString(HPLMNRManager.getNext_IMSI_CORRELATION_ID());
				sri.sri_res.get_imsi().value = TBCD.encode(tmp_imsi_id, 0x0f);
				
				// Network Node Number
				net.smstpdu.AddressString as = new net.smstpdu.AddressString();
				as.numberingPlan = net.smstpdu.NumberingPlan.ISDN_TELEPHONE;
				as.typeOfNumber = TypeOfNumber.INTERNATIONAL;
				as.digits = TBCD.encode(SGNConfigData.hplmnr_nnn_gt, 0x0f);
				sri.sri_res.get_locationInfoWithLMSI().get_networkNode_Number().value = net.smstpdu.AddressString.encode(as);
				
				// MT correlation
				mt_corr.imsi = sri_corr.imsi;
				mt_corr.nnn = sri.networkNodeNumber;
				mt_corr.annn = sri.additonalNumber;
				HPLMNRManager.MTCorrelationPut(Long.parseLong(tmp_imsi_id), mt_corr);
				HPLMNRManager.consume_IMSI_CORRELATION_ID();
				
			}else{
				StatsManager.HPLMNR_STATS.HPLMNR_NOT_ROUTABLE++;
				LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "Unsupported non-routable NNN found, Roaming assumed: [" + TBCD.decode(sri.networkNodeNumber.digits) + "]!");
			}
			
			// set TCAP Parameters to RoutingInfoForSM_Res
			//ed = new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false);
			//sri.sri_res.get_locationInfoWithLMSI().berTag.node.asn_class = ed.tagClass;
			//sri.sri_res.get_locationInfoWithLMSI().berTag.node.tag = ed.tagValue;
			//sri.sri_tcm.get_end().get_components().getChild(0).get_returnResultLast().get_resultretres().get_parameter().berTag.setBerTag(sri.sri_res.berTag);
			Utils.get_RETURN_RESULT_LAST_RETRES_PARAMETER(sri.sri_tcm).berTag.setBerTag(sri.sri_res.berTag);
			//sri.sri_res.get_locationInfoWithLMSI().get_networkNode_Number().berTag.node.asn_class = sri.sri_res.get_locationInfoWithLMSI().elements.get(0).tagClass;
			//sri.sri_res.get_locationInfoWithLMSI().get_networkNode_Number().berTag.node.tag = sri.sri_res.get_locationInfoWithLMSI().elements.get(0).tagValue;

			
			// encode TCAP
			ber.clearLengths(sri.sri_tcm.berTag);
			ber.prepareLenghts(sri.sri_tcm.berTag);
			tcap_packet = ber.encode(sri.sri_tcm.berTag);		

			
			// encode SCCP
			UDT_UnitData ns = (UDT_UnitData)SCCP.prepareNew(MessageType.UDT_UNITDATA);
			ns.protocolClass.protocolClass = ProtocolClassType.CLASS0;
			ns.protocolClass.messageHandling = MessageHandling.RETURN_MESSAGE_ON_ERROR;
			// Called party
			ns.calledPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.calledPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.calledPartyAddress.SSNIndicator = true;
			ns.calledPartyAddress.pointCodeIndicator = false;
			ns.calledPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_calling_ssn;
			ns.calledPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Called GT
			GlobalTitle_TTNPENOA ngt = (GlobalTitle_TTNPENOA)ns.calledPartyAddress.globalTitle; 
			ngt.translationType = SGNConfigData.hplmnr_sccp_called_gt_translationtype;
			ngt.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt.encodingScheme = (sri_corr.sms_gms_c.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt.addressInformation = TBCD.encode(sri_corr.sms_gms_c);

			// Calling party
			ns.callingPartyAddress.routingIndicator = RoutingIndicator.ROUTE_ON_GT;
			ns.callingPartyAddress.globalTitleIndicator = GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS;
			ns.callingPartyAddress.SSNIndicator = true;
			ns.callingPartyAddress.pointCodeIndicator = false;
			ns.callingPartyAddress.subsystemNumber = SGNConfigData.hplmnr_sccp_called_ssn;
			ns.callingPartyAddress.globalTitle = GlobalTitleBase.prepareNew(GlobalTitleIndicator.TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS);
			// Calling GT
			GlobalTitle_TTNPENOA ngt2 = (GlobalTitle_TTNPENOA)ns.callingPartyAddress.globalTitle;
			ngt2.translationType = SGNConfigData.hplmnr_sccp_calling_gt_translationtype;
			ngt2.numberingPlan = NumberingPlan.ISDN_TELEPHONE;
			ngt2.encodingScheme = (sri_corr.msisdn.length() % 2 == 0 ? EncodingScheme.BCD_EVEN : EncodingScheme.BCD_ODD);
			ngt2.natureOfAddress = NatureOfAddress.INTERNATIONAL;
			ngt2.addressInformation = TBCD.encode(SGNConfigData.hplmnr_smsrouter_gt);
			
			// SCCP data
			ns.data = tcap_packet;
			sccp_packet = ns.encode();
			sri.sccp_udt = ns;
			
			
			// M3UA
			M3UAPacket ndata = M3UA.prepareNew(M3UAMessageType.DATA);
			DATA ndd = (DATA)ndata.message;
			ndd.protocolData.setOPC(0);
			ndd.protocolData.setDPC(0);
			ndd.protocolData.setSI(ServiceIndicatorType.SCCP);
			ndd.protocolData.setNI(SGNConfigData.hplmnr_m3ua_data_ni);
			ndd.protocolData.setMP(SGNConfigData.hplmnr_m3ua_data_mp);
			ndd.protocolData.setSLS(SGNConfigData.hplmnr_m3ua_data_sls);
			ndd.protocolData.setUserProtocolData(sccp_packet);
			ndd.networkAppearance = new NetworkAppearance();
			ndd.networkAppearance.setNetworkAppearance(SGNConfigData.hplmnr_m3ua_data_nap);
			m3ua_packet = ndata.encode();
			sri.m3ua_packet = ndata;
			// MTP3
			if(sri.routingType == RoutingConnectionType.MTP3){
				sri.mtp3_packet = Utils.convert_M3UA_MTP3(ndata);
				res = sri.mtp3_packet.encode();
			// M3UA
			}else{
				res = m3ua_packet;
				
			}
			// Remove SRI correlation
			HPLMNRManager.SRICorrelationRemove(Utils.bytes2num(sri.tcap_did));
		}else{
			LoggingManager.debug(SGNConfigData.hplmnr_debug, logger, "SRI Correlation ID(TCAP_ID) not found!: [" + Utils.bytes2num(sri.tcap_did) + "]");
		}
		
		return res;
		
	}
	
	
	public HPLMNREncoderWorker(int id){
		worker_id = id;
		LoggingManager.info(logger, "Creating HPLMNR ENCODER worker thread: [ " + id + " ]!");
		enc_r = new Enc_r();
		enc_t = new Thread(enc_r, "HPLMNR_ENCODER_WORKER_" + id);
		enc_t.start();
	}

}
