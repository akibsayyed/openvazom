package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TopologyDirectionEnum extends ENUMERATED{

	public static final int _bothway = 0;
	public static final int _isolate = 1;
	public static final int _oneway = 2;
	public TopologyDirectionEnum(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
