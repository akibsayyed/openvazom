package net.db;

import java.sql.Connection;
import java.sql.DriverManager;
import java.sql.PreparedStatement;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.Vector;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.config.FNConfigData;
import net.gpu.cuda.CudaManager;
import net.gpu2.NodeType;
import net.logging.LoggingManager;
import net.smsfs.FilterMode;
import net.smsfs.SmsfsManager;
import net.smsfs.SmsfsPacket;
import net.smsfs.FilterResult;
import net.smsfs.FilterType;
import net.smsfs.Smsfs;
import net.smsfs.list.ListId;
import net.smsfs.list.ListManagerV2;
import net.smsfs.list.ListType;
import net.smstpdu.SmsDirection;
import net.smstpdu.SmsType;
import net.stats.StatsManager;

import org.apache.log4j.Logger;

import com.mysql.jdbc.CommunicationsException;

public abstract class DBManager {
	public static Logger logger=Logger.getLogger(DBManager.class);
	public static DBType current_db_type;
	public static DBManager current_db_impl;
	protected static Connection con;
	//private static Connection con_sim;
	//private static String db_driver_mysql = "com.mysql.jdbc.Driver";
	//private static String db_driver_monetdb = "nl.cwi.monetdb.jdbc.MonetDriver";
	protected static String current_database_driver;
	protected static String connectionString;
	//private static String connectionString_sim;
	private static ConcurrentLinkedQueue<DBRecordBase> queue;
	private static boolean stopping;
	private static Db_worker_r db_rorker_r;
	private static Thread db_worker_t;
	//private static Db_refilter_r refilter_worker_r;
	//private static Thread refilter_worker_t;
	public static boolean refiltering_active;
	public static boolean refiltering_stopping;
	private static int active_refilter_workers = 0;
	public static Vector<DBRecordSMS> dbq_sms;
	public static Vector<DBRecordSRI> dbq_sri;
	public static Vector<DBRecordISUP> dbq_isup;

	private static ConcurrentLinkedQueue<Connection> conn_pool;
	
	//public static long TEST = 0;

	public static HashMap<String, String> get_smpp_users_map(){
		HashMap<String, String> res = null;
		Connection db_conn = null;
		Statement st = null;
		ResultSet db_res = null;
		try{
			// get connection
			db_conn = DBManager.get_connection();
			if(db_conn != null){
				st = db_conn.createStatement();
				if(st != null){
					db_res = st.executeQuery("select smpp_system_id, smpp_password from smpp_users");
					if(db_res != null){
						res = new HashMap<String, String>();
						while(db_res.next()){
							res.put(db_res.getString(1), db_res.getString(2));
						
						}						
					}
					
				}
			}

		
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return res;
	}
	
	public static HashMap<Integer, Integer> get_ss7_smpp_ec_conversion_map(){
		HashMap<Integer, Integer> res = null;
		Connection db_conn = null;
		Statement st = null;
		ResultSet db_res = null;
		try{
			// get connection
			db_conn = DBManager.get_connection();
			if(db_conn != null){
				st = db_conn.createStatement();
				if(st != null){
					db_res = st.executeQuery("select error_code_ss7, error_code_smpp from ss7_smpp");
					if(db_res != null){
						res = new HashMap<Integer, Integer>();
						while(db_res.next()){
							res.put(db_res.getInt(1), db_res.getInt(2));
						
						}						
					}
					
				}
			}

		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		return res;
	}
	
	private static int refilterConfigure(){
		String sql;
		java.sql.Statement stat;
		PreparedStatement p_stat;
		ResultSet res = null;
		StatsManager.REFILTER_STATS.START_TIME = System.currentTimeMillis();
		int last_id = 0;
		long min_id;
		long max_id;
		Connection con_sim;

		try{
			// create db connection
			con_sim = create_refilter_db_con();
			// filter smsfs data
			p_stat = con_sim.prepareStatement("insert into filter_simulation(smsfs_data) values(?)");
			p_stat.setString(1, SmsfsManager.current_filter_data);
			p_stat.execute();
			// get last insert id
			res = p_stat.executeQuery("select last_insert_id()");
			res.next();
			last_id = res.getInt(1);
			p_stat.close();
			
			
			// clear current cimulation data
			stat = con_sim.createStatement();
			//stat.executeUpdate("delete from filter_simulation_data");
			
			// Min ID
			if(StatsManager.REFILTER_STATS.MIN_TS != null && StatsManager.REFILTER_STATS.MAX_TS != null)
				sql = "select min(id) from sms where timestamp between '" + StatsManager.REFILTER_STATS.MIN_TS + "' and '" + StatsManager.REFILTER_STATS.MAX_TS + "'";
			else sql = "select min(id) from sms";
			res = stat.executeQuery(sql);
			res.next();
			min_id = res.getLong(1);
			StatsManager.REFILTER_STATS.MIN_ID = min_id; 

			// Max ID
			if(StatsManager.REFILTER_STATS.MIN_TS != null && StatsManager.REFILTER_STATS.MAX_TS != null)
				sql = "select max(id) from sms where timestamp between '" + StatsManager.REFILTER_STATS.MIN_TS + "' and '" + StatsManager.REFILTER_STATS.MAX_TS + "'";
			else sql = "select max(id) from sms";
			res = stat.executeQuery(sql);
			res.next();
			max_id = res.getLong(1);
			StatsManager.REFILTER_STATS.MAX_ID = max_id; 
			stat.close();
			p_stat.close();
			con_sim.close();
			// Process
			StatsManager.REFILTER_STATS.POSITION = min_id;
			
		}catch(Exception e){
			e.printStackTrace();
		}		
		return last_id;
	}
	private static synchronized void refilterWorkerDone(int id){
		active_refilter_workers--;
		if(active_refilter_workers == 0){
			refiltering_active = false;
			refiltering_stopping = false;
			StatsManager.REFILTER_STATS.POSITION = 0;
			StatsManager.REFILTER_STATS.MAX_ID = 0;
			StatsManager.REFILTER_STATS.MIN_ID = 0;
			StatsManager.REFILTER_STATS.MIN_TS = null;
			StatsManager.REFILTER_STATS.MAX_TS = null;
			StatsManager.REFILTER_STATS.ELAPSED_TIME = System.currentTimeMillis() - StatsManager.REFILTER_STATS.START_TIME;
			
			logger.info("All refilter worked finished...");
			
		}
		
	}
	public static void quarantine(long sms_id, Connection _con){
		try{
			PreparedStatement stat = _con.prepareStatement("insert into quarantine(sms_id, timestamp) values (?, ?)");
			stat.setLong(1, sms_id);
			stat.setTimestamp(2, new Timestamp(System.currentTimeMillis()));
			stat.execute();
			stat.close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	public static int get_queue_size(){
		return queue.size();
	}
	private static class Db_refilter_r implements Runnable{
		String sql;
		java.sql.Statement stat;
		DBRecordSMS dbr = null;
		ArrayList<DBRecordSMS> dbr_lst = null;
		FilterResult fres = null;
		SmsfsPacket fp = null;
		Connection db_con;
		int worker_id;
		long id_from;
		long id_to;
		int filter_id;
		long from;
		long to;
		
		public Db_refilter_r(int _worker_id, int _filter_id, long _from, long _to, Connection _db_con){
			worker_id = _worker_id;
			id_from = _from;
			id_to = _to;
			filter_id = _filter_id;
			db_con = _db_con;
		}
		
		public void run() {
			logger.info("Starting Refilter Worker: [" + worker_id + "], [" + id_from + " - " + id_to + "]...");
			try{
				from = id_from;
				to = id_from + FNConfigData.refilter_batch_size; 
				stat = db_con.createStatement();
				while(from <= id_to && !refiltering_stopping){
					dbr_lst = getSMSbyRange(stat, from, to);
					for(int i = 0; i<dbr_lst.size(); i++){
						if(refiltering_stopping) break;
						dbr = dbr_lst.get(i);
						if(dbr != null){
							fp = new SmsfsPacket();
							fp.filterMode = FilterMode.REFILTER;
							fp.bindings.add(dbr.gt_called);
							fp.bindings.add(dbr.gt_calling);
							fp.bindings.add(dbr.scoa);
							fp.bindings.add(dbr.scda);
							fp.bindings.add(dbr.imsi);
							fp.bindings.add(dbr.msisdn);
							fp.bindings.add(Integer.toString(dbr.m3ua_data_dpc));
							fp.bindings.add(Integer.toString(dbr.m3ua_data_opc));
							fp.bindings.add(dbr.sms_originating);
							fp.bindings.add(Integer.toString(dbr.sms_originating_enc));
							fp.bindings.add(dbr.sms_destination);
							fp.bindings.add(Integer.toString(dbr.sms_destination_enc));
							fp.bindings.add(dbr.sms_text);
							fp.bindings.add(Integer.toString(dbr.sms_text_enc));
							fp.bindings.add(Integer.toString(dbr.type.getId()));
							switch(dbr.direction){
								case MO: 
									fp.filterType = FilterType.MO;
									fres = Smsfs.executeFilter(SmsfsManager.simulationFilterMO, fp); 
									break;
								case MT:
									fp.filterType = FilterType.MT;
									fres = Smsfs.executeFilter(SmsfsManager.simulationFilterMT, fp); 
									break;
							}
							if(fres != null){
								sql = "insert into filter_simulation_data(sms_id, filter_simulation_id, filter_action_id, filter_total_score) values(" + dbr.id + ", " + 
									filter_id + ", " + (FilterResult.evaluate(fres, FNConfigData.max_total_score) ? 1 : 2) + ", " + fres.total_score + ")";
								stat.execute(sql);
								// if quarantine flag is set
								if(fres.quarantine) quarantine(dbr.id, db_con);
								
							}
							
						}
						StatsManager.REFILTER_STATS.incPos();						
						
					}
					dbr_lst.clear();
					if(to == id_to){
						from = id_to + 1;
						
					}else{
						from = to;
						to = (from + FNConfigData.refilter_batch_size <= id_to ? from + FNConfigData.refilter_batch_size : id_to);
					}
					//c++;
				}
		
				stat.close();
				db_con.close();
		
			}catch(Exception e){
				e.printStackTrace();
			}
			logger.info("Ending Refilter Worker: [" + worker_id + "]...");
			refilterWorkerDone(worker_id);
		}
	}
	public static synchronized void buffer_flush(){
		if(dbq_sms.size() > 0){
			//logger.info("DB Flush: [SMS], [" + dbq_sms.size() + "]!");
			if(do_db_sms_insert(dbq_sms)) dbq_sms.clear();
		}

		if(dbq_sri.size() > 0){
			//logger.info("DB Flush: [SRI], [" + dbq_sri.size() + "]!");
			if(do_db_sri_insert(dbq_sri)) dbq_sri.clear();
			
		}

		if(dbq_isup.size() > 0){
			//logger.info("DB Flush: [SRI], [" + dbq_sri.size() + "]!");
			if(do_db_isup_insert(dbq_isup)) dbq_isup.clear();
			
		}

		
	}
	private static class Db_worker_r implements Runnable{
		private DBRecordBase dbr;
		//private int packet_size = Integer.parseInt(ConfigManager.get("db.packet.size"));
		public void run() {
			logger.info("Starting...");
			dbq_sms = new Vector<DBRecordSMS>();
			dbq_sri = new Vector<DBRecordSRI>();
			dbq_isup = new Vector<DBRecordISUP>();
			while(!stopping){
				dbr = queue.poll();
				if(dbr != null){
					if(dbr.recordType == DBRecordType.SMS){
						// buffer * 2 max limit, in case of previous failed inserts
						if(dbq_sms.size() < FNConfigData.db_packet_size * 2){
							dbq_sms.add((DBRecordSMS)dbr);
						}else{
							logger.warn("DB SMS Buffer full: [" + (FNConfigData.db_packet_size * 2) + "]!");
						}
						
					}else if(dbr.recordType == DBRecordType.SRI){
						// buffer * 2 max limit, in case of previous failed inserts
						if(dbq_sri.size() < FNConfigData.db_packet_sri_size * 2){
							dbq_sri.add((DBRecordSRI)dbr);
							
						}else{
							logger.warn("DB SRI Buffer full: [" + (FNConfigData.db_packet_size * 2) + "]!");
						}
					}else if(dbr.recordType == DBRecordType.ISUP){
						// buffer * 2 max limit, in case of previous failed inserts
						if(dbq_isup.size() < FNConfigData.db_packet_isup_size * 2){
							dbq_isup.add((DBRecordISUP)dbr);
							
						}else{
							logger.warn("DB ISUP Buffer full: [" + (FNConfigData.db_packet_isup_size * 2) + "]!");
						}
						
					}
					// check if conn is null
					if(con == null) {
						connect();
					}else{
						// insert db sms packet
						if(dbq_sms.size() >= FNConfigData.db_packet_size){
							if(do_db_sms_insert(dbq_sms)){
								//logger.debug("DB [" + dbr.recordType + "] Packet inserted: [" + FNConfigData.db_packet_size + "]!");
								// clear
								dbq_sms.clear();
							}
						}
						// insert db sri packet
						if(dbq_sri.size() >= FNConfigData.db_packet_sri_size){
							if(do_db_sri_insert(dbq_sri)){
								//logger.debug("DB [" + dbr.recordType + "] Packet inserted: [" + FNConfigData.db_packet_sri_size + "]!");
								// clear
								dbq_sri.clear();
							}
						}
						// insert db isup packet
						if(dbq_isup.size() >= FNConfigData.db_packet_isup_size){
							if(do_db_isup_insert(dbq_isup)){
								//logger.debug("DB [" + dbr.recordType + "] Packet inserted: [" + FNConfigData.db_packet_sri_size + "]!");
								// clear
								dbq_isup.clear();
							}
						}
						
					}
					

				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					
				}
				
				
				
			}
			logger.info("Ending...");
		}
		
		
	}
	
	public static void offer(DBRecordBase dbr){
		if(FNConfigData.dr_status){
			if(dbr != null){
				if(queue.size() < FNConfigData.fn_global_max_queue_size){
					queue.offer(dbr);
					//DBManager.TEST++;
				}else{
					StatsManager.DB_STATS.DB_QUEUE_FULL_COUNT++;
					//LoggingManager.warn(logger, "DBManager.queue: maximum queue size reached: [" + FNConfigData.fn_global_max_queue_size + "]!");
				}

			}
		}
		
	}
	
	public static void stopRefiltering(){
		refiltering_stopping = true;
	
	}
	public static void refilterRetainedSMS(String ts_from, String ts_to){
		StatsManager.REFILTER_STATS.MIN_TS = ts_from;
		StatsManager.REFILTER_STATS.MAX_TS = ts_to;
		Db_refilter_r refilter_worker_r = null;
		Thread refilter_worker_t = null;
		long from = 0;
		long to = 0;
		long total;
		Connection tmp_con;
		long tpt = 0;
		refiltering_active = true;
		refiltering_stopping = false;
		int filter_id;
		//connect_sim();
		filter_id = refilterConfigure();
		total = StatsManager.REFILTER_STATS.MAX_ID - StatsManager.REFILTER_STATS.MIN_ID + 1;
		tpt = (int)Math.ceil((double)total / FNConfigData.refilter_workers);
		logger.debug("Refilter MIN ID: " + StatsManager.REFILTER_STATS.MIN_ID);
		logger.debug("Refilter MAX ID: " + StatsManager.REFILTER_STATS.MAX_ID);
		logger.debug("Refilter TPT: " + tpt);
		/*
		Retained SMS Refiltering position (CURRENT ID / MAX ID): [ 668801 / 80006233 ]!
		1 - 100
		tpt = 25
		-------
		0   25
		25  50
		50  75
		75 100
		 */

		// cuda reset 
		net.gpu2.GPUBalancer.reset(CudaManager.ALL_LISTS, NodeType.REFILTER);
		// lists reset
		// LD_SPAM
		ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.LD_SPAM.toString()).clear(true);
		ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.LD_SPAM.toString()).fill_tmp();;
		// MD5_NORMAL
		ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_NORMAL.toString()).clear(true);
		ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_NORMAL.toString()).fill_tmp();;
		// MD5_QUARANTINE
		ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_QUARANTINE.toString()).clear(true);
		ListManagerV2.get_list(ListType.SMSFS_LIST, ListId.MD5_QUARANTINE.toString()).fill_tmp();;
		
		
		for(int i = 0; i<FNConfigData.refilter_workers; i++){
			//db connection
			tmp_con = create_refilter_db_con();
			// worker setup
			from = i*tpt + StatsManager.REFILTER_STATS.MIN_ID;
			to = from + tpt;
			refilter_worker_r = new Db_refilter_r(i, filter_id, from , to, tmp_con);
			refilter_worker_t = new Thread(refilter_worker_r, "REFILTER_WORKER_" + i);
			refilter_worker_t.start();
			active_refilter_workers++;
		}
	}
	
	public static ArrayList<DBRecordSMS> getSMSbyRange(Statement _stat, long id_from, long id_to){
		ArrayList<DBRecordSMS> dbr = new ArrayList<DBRecordSMS>();
		DBRecordSMS tmp_db = null;
		String sql;
		java.sql.Statement stat;
		ResultSet res = null;
		try{
			sql = "select id, direction_id, type_id, gt_called, gt_calling, imsi, m3ua_data_dpc, m3ua_data_opc, msisdn, scda, scoa, sms_destination, sms_destination_enc_id, " +
				"sms_originating, sms_originating_enc_id, sms_text, sms_text_enc_id, tcap_did, tcap_sid from sms where id > " + id_from + " and id <= " + id_to;
			logger.debug("Fetching data: [" + id_from + " - " + id_to + "]");
			// reuse statement
			if(_stat == null) stat = con.createStatement();
			else stat = _stat;
			
			res = stat.executeQuery(sql);
			while(res.next()){
				tmp_db = new DBRecordSMS();
				tmp_db.id = res.getLong(1);
				tmp_db.direction = (res.getInt(2) == 1 ? SmsDirection.MO : SmsDirection.MT);
				tmp_db.type = (res.getInt(3) == 1 ? SmsType.SINGLE : SmsType.CONCATENATED);
				tmp_db.gt_called = res.getString(4);
				tmp_db.gt_calling = res.getString(5);
				tmp_db.imsi = res.getString(6);
				tmp_db.m3ua_data_dpc = res.getInt(7);
				tmp_db.m3ua_data_opc = res.getInt(8);
				tmp_db.msisdn = res.getString(9);
				tmp_db.scda = res.getString(10);
				tmp_db.scoa = res.getString(11);
				tmp_db.sms_destination = res.getString(12);
				tmp_db.sms_destination_enc = res.getInt(13);
				tmp_db.sms_originating = res.getString(14);
				tmp_db.sms_originating_enc = res.getInt(15);
				tmp_db.sms_text = res.getString(16);
				tmp_db.sms_text_enc = res.getInt(17);
				tmp_db.tcap_did = res.getLong(18);
				tmp_db.tcap_sid = res.getLong(19);		
				
				dbr.add(tmp_db);
			}
			// only close local statement
			if(_stat == null) stat.close();
		}catch(CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			connect();
		}catch(com.mysql.jdbc.exceptions.jdbc4.CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			connect();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return dbr;
	}
	public static DBRecordSMS getSMSByID(long id, Statement _stat){
		DBRecordSMS dbr = null;
		String sql;
		java.sql.Statement stat = null;
		ResultSet res = null;
		try{
			sql = "select id, direction_id, type_id, gt_called, gt_calling, imsi, m3ua_data_dpc, m3ua_data_opc, msisdn, scda, scoa, sms_destination, sms_destination_enc_id, " +
				"sms_originating, sms_originating_enc_id, sms_text, sms_text_enc_id, tcap_did, tcap_sid from sms where id = " + id;
			
			// reuse statement
			if(_stat == null){
				if(con != null) stat = con.createStatement();
			}else stat = _stat;
			
			if(stat != null){
				res = stat.executeQuery(sql);
				if(res.next()){
					dbr = new DBRecordSMS();
					dbr.id = res.getLong(1);
					dbr.direction = (res.getInt(2) == 1 ? SmsDirection.MO : SmsDirection.MT);
					dbr.type = (res.getInt(3) == 1 ? SmsType.SINGLE : SmsType.CONCATENATED);
					dbr.gt_called = res.getString(4);
					dbr.gt_calling = res.getString(5);
					dbr.imsi = res.getString(6);
					dbr.m3ua_data_dpc = res.getInt(7);
					dbr.m3ua_data_opc = res.getInt(8);
					dbr.msisdn = res.getString(9);
					dbr.scda = res.getString(10);
					dbr.scoa = res.getString(11);
					dbr.sms_destination = res.getString(12);
					dbr.sms_destination_enc = res.getInt(13);
					dbr.sms_originating = res.getString(14);
					dbr.sms_originating_enc = res.getInt(15);
					dbr.sms_text = res.getString(16);
					dbr.sms_text_enc = res.getInt(17);
					dbr.tcap_did = res.getLong(18);
					dbr.tcap_sid = res.getLong(19);			
				}
				// only close local statement
				if(_stat == null && stat != null) stat.close();
				
			}
		}catch(CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			connect();
		}catch(com.mysql.jdbc.exceptions.jdbc4.CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			connect();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		return dbr;
	}
	
	public abstract boolean _do_db_isup_insert(Vector<DBRecordISUP> lst);
	
	public static synchronized boolean do_db_isup_insert(Vector<DBRecordISUP> lst){
		return current_db_impl._do_db_isup_insert(lst);
	
	}	
	public abstract boolean _do_db_sri_insert(Vector<DBRecordSRI> lst);
	
	public static synchronized boolean do_db_sri_insert(Vector<DBRecordSRI> lst){
		return current_db_impl._do_db_sri_insert(lst);
	}
	public abstract boolean _do_db_sms_insert(Vector<DBRecordSMS> lst);

	public static synchronized boolean do_db_sms_insert(Vector<DBRecordSMS> lst){
		return current_db_impl._do_db_sms_insert(lst);
	}
	
	protected abstract void _connect();
	
	private static void connect(){
		current_db_impl._connect();
	}

	private static Connection create_refilter_db_con(){
		Connection tmp_con = null;
		try{
			//tmp_con = DriverManager.getConnection(connectionString_sim, FNConfigData.db_username, FNConfigData.db_password);
			tmp_con = DriverManager.getConnection(connectionString, FNConfigData.db_username, FNConfigData.db_password);
			tmp_con.setAutoCommit(true);
			return tmp_con;
		}catch(Exception e){
			e.printStackTrace();
		}
		return null;
	}

	public static void offer_connection(Connection con){
		conn_pool.offer(con);
	
	}
	
	public static Connection get_connection(){
		return conn_pool.poll();
	}
	
	public static void add_connection(){
		try{
			Connection con = DriverManager.getConnection(connectionString, FNConfigData.db_username, FNConfigData.db_password);
			if(con != null){
				con.setAutoCommit(true);
				conn_pool.offer(con);
				
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}
	
	public static void init_connections(int conn_count){
		Connection tmp_con = null;
		try{
			LoggingManager.info(logger, "Initializing DB connection pool...");
			for(int i = 0; i<conn_count; i++){
				LoggingManager.info(logger, "Initializing DB connection id = [" + i + "]...");
				tmp_con = DriverManager.getConnection(connectionString, FNConfigData.db_username, FNConfigData.db_password);
				if(tmp_con != null){
					tmp_con.setAutoCommit(true);
					conn_pool.offer(tmp_con);
					
				}
				
			}
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	public static DBManager get_db_manager(DBType type){
		if(type != null){
			switch(type){
				case MYSQL: return new DBManager_mysql();
				case MONETDB: return new DBManager_monetdb();
			}
		}
		return null;
	}
	protected abstract void init_db_params();
	
	public static void init(DBType db_type){
		queue = new ConcurrentLinkedQueue<DBRecordBase>();
		conn_pool = new ConcurrentLinkedQueue<Connection>();
		try{
			// init db driver
			current_db_impl = get_db_manager(db_type);
			current_db_impl.init_db_params();
			
			Class.forName(current_database_driver);
			LoggingManager.info(logger, "Initializing DB connection type = [" + db_type + "]...");
			
			// main db connect
			connect();
			
			// pool
			init_connections(FNConfigData.db_connection_pool_size);
			

			db_rorker_r = new Db_worker_r();
			db_worker_t = new Thread(db_rorker_r, "DB_WORKER_THREAD");
			db_worker_t.start();
			
			
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
}
