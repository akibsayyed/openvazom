package net.gpu.cuda;

public class KernelStats {
    public float last_execution_time;
    public float max_execution_time;
    public long execution_count;
    public long update_q_size;
    public long ld_q_size;
    public long md5_q_size;
    
    public KernelStats(float _last_execution_time, float _max_execution_time, long _execution_count, long _update_q_size,
    		long _ld_q_size, long _md5_q_size){
    	last_execution_time = _last_execution_time;
    	max_execution_time = _max_execution_time;
    	execution_count = _execution_count;
    	update_q_size = _update_q_size;
    	ld_q_size = _ld_q_size;
    	md5_q_size = _md5_q_size;
    	
    }
}
