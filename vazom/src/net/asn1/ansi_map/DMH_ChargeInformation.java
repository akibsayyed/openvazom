package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DMH_ChargeInformation extends OCTET_STRING{

	public DMH_ChargeInformation(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
