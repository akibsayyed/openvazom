#include <string>
#include <map>
#include <deque>
#include <iostream>
#include <pthread.h>

#ifdef __PFRING__
#include <pfring.h>
#else
#include <pcap.h>
#endif

using namespace std;

#ifndef SPCAP_H_
#define SPCAP_H_
#endif


#ifdef __PFRING__
#define PCAP_ERRBUF_SIZE 256
#endif

struct packet_descriptor{
	u_char *packet;
	u_int caplen;
};

struct stream_descriptor{
	#ifdef __PFRING__
	pfring *handle;
	#else
	pcap_t *handle;
	#endif
	int id;
	u_int MAX_Q_SIZE;
	u_int MAX_Q_LIMIT_REACHED;
	deque<packet_descriptor*> *q;
	deque<packet_descriptor*> *tmp_q;

	pthread_mutex_t q_lock;
	pthread_t thread;
	bool stopping;
};

class SPcap {
public:
	static map<int, stream_descriptor*> stream_lst;
	SPcap();
	static int getDevices(string *buffer);
	static void init();
	static stream_descriptor *initCapture(string device, int snaplen, int max_q_size);
	static void startCapture(int id);
	static void stopCapture(int id);

	#ifdef __PFRING__
	static void setBPF(pfring *handle, string filter);
	static pfring_stat* getStats(int id);
	#else
	static void setBPF(pcap_t *handle, string filter);
	static pcap_stat* getStats(int id);
	#endif

	static stream_descriptor* getStream(int id);
	static packet_descriptor* popPacket(int id);
	virtual ~SPcap();
private:
	static char errbuf[PCAP_ERRBUF_SIZE];
	//static u_int MAX_Q_LENGTH;
	static int NEXT_STREAM_ID;
	static void addToStreamList(stream_descriptor *sd);

	#ifdef __PFRING__
	static void packet_received(const struct pfring_pkthdr *hdr, const u_char *packet, const u_char *id);
	#else
	static void packet_received(u_char *id, const struct pcap_pkthdr *hdr, const u_char *packet);
	#endif

	static void *capture_thread(void *args);
	static pthread_mutex_t stram_lst_mt;
};

