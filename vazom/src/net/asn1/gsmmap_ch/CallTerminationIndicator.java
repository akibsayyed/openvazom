package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallTerminationIndicator extends ENUMERATED{

	public static final int _terminateCallActivityReferred = 0;
	public static final int _terminateAllCallActivities = 1;
	public CallTerminationIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
