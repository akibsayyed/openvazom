package net.mtp3.messages.signalling_mtn;

import net.mtp3.messages.MessageBase;
import net.mtp3.messages.MessageType;
import net.mtp3.messages.SignallingMessageMTNGroupType;
import net.mtp3.messages.SignallingMessageMTNType;

public abstract class SignallingMessageBaseMTN extends MessageBase {
	public SignallingMessageMTNGroupType signallingGroupType;
	public SignallingMessageMTNType signallingType;
	
	public SignallingMessageBaseMTN(){
		type = MessageType.SIGNALLING_MTN;
	}
	
	public static SignallingMessageMTNType decodeType(byte first_octet){
		SignallingMessageMTNGroupType tmp_group = SignallingMessageMTNGroupType.get(first_octet & 0x0f);
		SignallingMessageMTNType tmp_type = SignallingMessageMTNType.get((first_octet & 0xf0) >> 4, tmp_group.getId());
		return tmp_type;
	}
	public static SignallingMessageBaseMTN decode(byte[] data){
		SignallingMessageBaseMTN res = null;
		SignallingMessageMTNType type = decodeType(data[0]);
		switch(type){
			case SLTM: res = new SLTM(); break;
			case SLTA: res = new SLTA(); break;
		}
		
		if(res != null) res.init(data);
		
		return res;
	}
	
	public void init(byte[] data) {
		// H0
		signallingGroupType = SignallingMessageMTNGroupType.get(data[byte_pos] & 0x0f);
		// H1
		signallingType = SignallingMessageMTNType.get((data[byte_pos] & 0xf0) >> 4, signallingGroupType.getId());
		byte_pos++;
	}

}
