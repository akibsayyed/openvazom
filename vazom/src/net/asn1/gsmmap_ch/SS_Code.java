package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SS_Code extends OCTET_STRING{

	public SS_Code(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
