package net.vstp;

import java.util.concurrent.ConcurrentHashMap;


import net.config.SGNConfigData;
import net.ds.DataSourceType;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionType;
import net.routing.RoutingManager;
import net.sgc.SGCManager;
import net.stats.StatsManager;

import org.apache.log4j.Logger;

public class VSTP_SGF_CorrelationManager {
	public static Logger logger=Logger.getLogger(VSTP_SGF_CorrelationManager.class);
	private static ConcurrentHashMap<String, VSTP_SGF_CorrelationPacket> map;
	private static Timeout timeout_r;
	private static Thread timeout_t;
	public static boolean stopping;
	
	private static class Timeout implements Runnable{
		public void run() {
			VSTP_SGF_CorrelationPacket vsd = null;
			//M3UAConnection m3ua_conn = null;
			RoutingConnection r_conn = null;
			RoutingConnectionPayload rpl = null;
			//M3UAPayload mp = null;
			long ts;
			while(!stopping){
				logger.info("Starting...");
				while(!stopping){
					try{ Thread.sleep(SGNConfigData.vstp_sgf_crl_check_interval); }catch(Exception e){ e.printStackTrace(); }
					ts = System.currentTimeMillis();
					for(String s : map.keySet()){
						vsd = map.get(s);
						if(ts - vsd.timestamp > SGNConfigData.vstp_sgf_crl_timeout){
							logger.warn("Removing VSTP-SGF Correlation packet [" + s + "], timeout reached!");
							//m3ua_conn = M3UAConnManager.getConnection(M3UAConnManager.default_conn);
							r_conn = RoutingManager.getDefault(vsd.type, vsd);
							if(r_conn != null){
								rpl = RoutingManager.createPayload(vsd.type);
								if(rpl != null){
									rpl.setParamsFromVSTP_SGF(vsd);
									r_conn.out_offer(rpl);
									logger.debug("No available F Nodes, bypassing filters, message sent to default [" + vsd.type + "] Connection!");
								}else logger.warn("RoutingManager: unknown payload type [" + vsd.type + "]!");

							}else logger.warn("Default Routing Connection for type: [" +  vsd.type + "] does not exist!");
							
							VSTP_SGF_CorrelationManager.remove(s);
							// distribute remove
							switch(vsd.type){
								case SMPP: SGCManager.distribute_vstp_crl_remove(s, DataSourceType.SMPP); break;
								case M3UA: SGCManager.distribute_vstp_crl_remove(s, DataSourceType.SCTP); break;
								case MTP3: SGCManager.distribute_vstp_crl_remove(s, DataSourceType.E1); break;
								
							}
							
							// stats
							StatsManager.VSTP_STATS.VSTP_SGF_TIMEOUT++;
							StatsManager.VSTP_STATS.VSTP_CRL_REMOVE++;
							
						}
						
					}
				}
				logger.info("Ending...");
			}
		}
		
	}
	
	public static void add(String id, VSTP_SGF_CorrelationPacket vsd){
		vsd.timestamp = System.currentTimeMillis();
		map.put(id, vsd);
	}
	public static VSTP_SGF_CorrelationPacket remove(String id){
		return map.remove(id);
	}
	public static boolean exists(String id){
		return map.get(id) != null;
	}

	public static VSTP_SGF_CorrelationPacket get(String id){
		return map.get(id);
	}
	
	public static void init(){
		logger.info("Starting VTSP-SG-F Correlation Manager...");
		map = new ConcurrentHashMap<String, VSTP_SGF_CorrelationPacket>();
		logger.info("Starting VTSP-SG-F Timeout monitor...");
		timeout_r = new Timeout();
		timeout_t = new Thread(timeout_r, "VSTP_SGF_TIMEOUT");
		timeout_t.start();
	}
}
