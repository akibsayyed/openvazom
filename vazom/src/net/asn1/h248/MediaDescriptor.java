package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MediaDescriptor extends SEQUENCE{

	public TerminationStateDescriptor get_termStateDescr(){
		return (TerminationStateDescriptor)elements.get(0).data;
	}
	public TerminationStateDescriptor new_termStateDescr(){
		return new TerminationStateDescriptor();
	}

	public MediaDescriptorStreamsChoiceWrapper get_streams(){
		return (MediaDescriptorStreamsChoiceWrapper)elements.get(1).data;
	}
	public MediaDescriptorStreamsChoiceWrapper new_streams(){
		return new MediaDescriptorStreamsChoiceWrapper();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_termStateDescr(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streams(); }});
	}
	public MediaDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
