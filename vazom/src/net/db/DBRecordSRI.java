package net.db;

import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.NumberingPlan;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;

public class DBRecordSRI extends DBRecordBase {
	public int m3ua_data_opc;
	public int m3ua_data_dpc;
	
	public String gt_called;
	public String gt_calling;
	public int gt_called_tt;
	public int gt_calling_tt;
	public NatureOfAddress gt_called_nai;
	public NatureOfAddress gt_calling_nai;
	public NumberingPlan gt_called_np;
	public NumberingPlan gt_calling_np;
	public GlobalTitleIndicator gt_called_gti;
	public GlobalTitleIndicator gt_calling_gti;
	
	public String imsi;
	public String msisdn;
	public int msisdn_nai;
	public String nnn;
	public String annn;
	public String sca;
	
	public String gsmc;
	public int gsmc_nai;
	public String vmsc;
	public int vmsc_nai;
	public String roaming_number;
	public int roaming_number_nai;
	public String fwd_to_number;
	public int fwd_to_number_nai;

	// 1 - SM
	// 2 - CH
	// default = 1
	public int routing_type = 1;
	
	public static DBRecordSRI VSTP_to_DBR(MessageDescriptor md){
		if(md.header.msg_type == MessageType.HLR){
			DBRecordSRI dbr_sri = new DBRecordSRI();
			dbr_sri.timestamp = System.currentTimeMillis();
		
			dbr_sri.gt_called = md.values.get(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId());
			dbr_sri.gt_calling = md.values.get(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId());
			dbr_sri.gt_called_gti = GlobalTitleIndicator.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_GTI.getId())));
			dbr_sri.gt_calling_gti = GlobalTitleIndicator.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_GTI.getId())));
			dbr_sri.gt_called_tt = Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_TT.getId()));
			dbr_sri.gt_calling_tt = Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_TT.getId()));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId()) != null) dbr_sri.gt_called_np = NumberingPlan.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NP.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId()) != null) dbr_sri.gt_calling_np = NumberingPlan.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NP.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId()) != null) dbr_sri.gt_called_nai = NatureOfAddress.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_NAI.getId())));
			if(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId()) != null) dbr_sri.gt_calling_nai = NatureOfAddress.get(Integer.parseInt(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_NAI.getId())));
			
			dbr_sri.m3ua_data_dpc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_DPC.getId()));
			dbr_sri.m3ua_data_opc = Integer.parseInt(md.values.get(VSTPDataItemType.M3UA_OPC.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_SID.getId()) != null) dbr_sri.tcap_sid = Long.parseLong(md.values.get(VSTPDataItemType.TCAP_SID.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_DID.getId()) != null) dbr_sri.tcap_did = Long.parseLong(md.values.get(VSTPDataItemType.TCAP_DID.getId()));
			dbr_sri.msisdn = md.values.get(VSTPDataItemType.HLR_MSISDN.getId());
			dbr_sri.imsi = md.values.get(VSTPDataItemType.HLR_IMSI.getId());
			dbr_sri.nnn = md.values.get(VSTPDataItemType.HLR_NNN.getId());
			dbr_sri.annn = md.values.get(VSTPDataItemType.HLR_ANNN.getId());
			dbr_sri.sca = md.values.get(VSTPDataItemType.HLR_SCA.getId());
			dbr_sri.app_ctx_oid = md.values.get(VSTPDataItemType.TCAP_DIALOGUE_OID.getId());
			// error codes
			if(md.values.get(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId()) != null) dbr_sri.gsm_map_error_code = Integer.parseInt(md.values.get(VSTPDataItemType.GSM_MAP_ERROR_CODE.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_ERROR_CODE.getId()) != null) dbr_sri.tcap_abort_error_code = Integer.parseInt(md.values.get(VSTPDataItemType.TCAP_ERROR_CODE.getId()));
			if(md.values.get(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId()) != null) dbr_sri.tcap_dialogue_error_code = Integer.parseInt(md.values.get(VSTPDataItemType.TCAP_DIALOGUE_ERROR_CODE.getId()));
			
			return dbr_sri;
		}
		return null;
	}
	
	
	public DBRecordSRI(){
		recordType = DBRecordType.SRI;
	}
}
