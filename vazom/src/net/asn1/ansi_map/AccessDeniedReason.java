package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AccessDeniedReason extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _unassigned_directory_number =  1 ;
	public static final int _inactive =  2 ;
	public static final int _busy =  3 ;
	public static final int _termination_denied =  4 ;
	public static final int _no_page_response =  5 ;
	public static final int _unavailable =  6 ;
	public static final int _service_Rejected_by_MS =  7 ;
	public static final int _services_Rejected_by_the_System =  8 ;
	public static final int _service_Type_Mismatch =  9 ;
	public static final int _service_Denied =  10 ;
	public static final int _position_Determination_Not_Supported = 12;
	public AccessDeniedReason(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
