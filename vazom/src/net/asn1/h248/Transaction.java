package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Transaction extends CHOICE{

	public TransactionRequest get_transactionRequest(){
		return (TransactionRequest)elements.get(0).data;
	}
	public TransactionRequest new_transactionRequest(){
		return new TransactionRequest();
	}

	public TransactionPending get_transactionPending(){
		return (TransactionPending)elements.get(1).data;
	}
	public TransactionPending new_transactionPending(){
		return new TransactionPending();
	}

	public TransactionReply get_transactionReply(){
		return (TransactionReply)elements.get(2).data;
	}
	public TransactionReply new_transactionReply(){
		return new TransactionReply();
	}

	public TransactionResponseAck get_transactionResponseAck(){
		return (TransactionResponseAck)elements.get(3).data;
	}
	public TransactionResponseAck new_transactionResponseAck(){
		return new TransactionResponseAck();
	}

	public SegmentReply get_segmentReply(){
		return (SegmentReply)elements.get(4).data;
	}
	public SegmentReply new_segmentReply(){
		return new SegmentReply();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionRequest(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionPending(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionReply(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionResponseAck(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, true){public void set(){ data = new_segmentReply(); }});
	}
	public Transaction(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
