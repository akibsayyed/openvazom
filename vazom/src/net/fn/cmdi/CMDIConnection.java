package net.fn.cmdi;

import java.io.BufferedReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.io.OutputStream;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;
import java.util.concurrent.ConcurrentHashMap;

import net.config.FNConfigData;
import net.smsfs.Compiler;
import net.smsfs.SmsfsManager;

import org.apache.log4j.Logger;

public class CMDIConnection{
	public Logger logger=Logger.getLogger(CMDIConnection.class);
	private Socket tcp;
	private ConcurrentHashMap<String, CMDIConnection> conn_list;
	private Reader_r reader_r;
	private Thread reader_t;
	private boolean stopping;
	private BufferedReader in;
	private PrintWriter out;
	private OutputStream outb;
	private InputStream inb;
	private String con_ip;
	private int con_port;
	//private String uname;
	private boolean userFriendly = true;
	private boolean filterMode = false;
	private String tmp_smsfs_filter = "";
	
	private class Reader_r implements Runnable{
		private String cmd = "";
		private int last_cmd_index = 0;
		private ArrayList<String> last_cmd_lst = new ArrayList<String>();
		private String res;
		boolean history_found;
		public void run() {
			byte[] buff = new byte[512];
			byte[] buff_special = new byte[2];
			boolean special = false;
			int c = 0;
			int bc;
			logger.info("VAZOM CMDI READER STARTED: [ " + con_ip + ":" + con_port + " ]");
			
			//out.print(new char[]{(char)0xff, (char)0xfc, (char)0x22});
			try{
				// IAC WILL ECHO IAC WILL SUPPRESS_GO_AHEAD IAC WONT LINEMODE
				outb.write(new byte[]{(byte)255, (byte)251, 1, (byte)255, (byte)251, 3, (byte)255, (byte)252, 34});
				out.flush();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				inb.read();
				
			}catch(Exception e){}
			//out.flush();
			out.print("\u001B[1;34m");
			out.println("Welcome to VAZOM F CMDI!" + (char)13);
			out.print("\u001B[0m");
			out.println("\u001B[1;37mType '\u001B[1;32mHELP\u001B[1;37m' for help!" + (char)13);
			out.print("\u001B[1;33m");
			out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
			out.print("\u001B[0m");
			out.print("\u001B7"); // save cursor pos
			out.flush();
			while(!stopping){
				try{
					//cmd = in.readLine();
					bc = inb.read(buff);
					//c = in.read();
					//System.out.println(bc);
					for(int i = 0; i<bc; i++){
						c = buff[i];
						if(c == 13){
							// echo
							out.println();
							out.print((char)13);
							out.flush();

							for(int j = 0; j<last_cmd_lst.size(); j++) if(last_cmd_lst.get(j).equals(cmd)){
								history_found = true;
								last_cmd_index = j;
							}
							if(!history_found){
								last_cmd_lst.add(cmd);
								last_cmd_index = last_cmd_lst.size() - 1;
							}
							history_found = false;
							//System.out.println("CMD: " + cmd);
							if(cmd.equalsIgnoreCase("FILTER START")){
								tmp_smsfs_filter = "";
								filterMode = true;
								out.println("\u001B[1;32mStart sending SMSFS Data!\u001B[0m");
								out.println((char)13);
								out.flush();
								
							}else if(cmd.equalsIgnoreCase("FILTER END")){
								if(filterMode){
									filterMode = false;
									out.println("\u001B[1;32mSMSFS Received!" + (char)13);
									out.println("\u001B[1;33m----FILTER DATA START-----" + (char)13);
									out.println("\u001B[1;34m" + tmp_smsfs_filter.replaceAll("\n", "\n" + (char)13));
									out.println("\u001B[1;33m----FILTER DATA END-----" + (char)13);
									boolean valid = Compiler.compile_str(tmp_smsfs_filter);
									out.println("\u001B[1;37mSMSFS Filter valid: [ \u001B[1;32m" + valid + "\u001B[1;37m ]!" + (char)13);
									if(!valid) tmp_smsfs_filter = "";
									// set new simulation filter
									else{
										SmsfsManager.setSimulationFilter(tmp_smsfs_filter);
										out.println("\u001B[1;37mNew simulation SMSFS Filter set!" + (char)13);
										out.flush();
									}
									
								}else out.println("\u001B[1;31mFilter mode not started!" + (char)13);
								out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
								out.print("\u001B[0m");
								out.print("\u001B7"); // save cursor pos
								out.flush();
								
							}else if(cmd.equalsIgnoreCase("FILTER SAVE")){
								if(tmp_smsfs_filter != ""){
									String fname = SmsfsManager.filterSave(tmp_smsfs_filter);
									if(fname != null){
										out.println("\u001B[1;37mFilter [ \u001B[1;32m" + fname + "\u001B[1;37m ] saved!" + (char)13);
										
									}else out.println("\u001B[1;31mError while saving filter!" + (char)13);
									out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
									out.print("\u001B[0m");
									out.print("\u001B7"); // save cursor pos
									out.flush();
									
								}else{
									out.println("\u001B[1;31mFilter not available!" + (char)13);
									out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
									out.print("\u001B[0m");
									out.print("\u001B7"); // save cursor pos
									out.flush();
								}

							}else if(cmd.equalsIgnoreCase("FILTER ACTIVATE")){
								if(tmp_smsfs_filter != ""){
									SmsfsManager.filterActivate_str(tmp_smsfs_filter);
									out.println("\u001B[1;32mFilter activated!" + (char)13);
									out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
									out.print("\u001B[0m");
									out.print("\u001B7"); // save cursor pos
									out.flush();
									
								}else{
									out.println("\u001B[1;31mFilter not available!" + (char)13);
									out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
									out.print("\u001B[0m");
									out.print("\u001B7"); // save cursor pos
									out.flush();
								}
							}else if(filterMode){
								tmp_smsfs_filter += cmd + "\n";
								
							}else if(cmd.equalsIgnoreCase("MODE")){
								userFriendly = !userFriendly;
							}else if(cmd.equalsIgnoreCase("BYE")){
								stopping = true;
							}else if(cmd.equalsIgnoreCase("HALT")){
								System.exit(0);
							}else{
								if(userFriendly){
									res = CMDIListener.process_cmd(cmd);
									out.println(res.replaceAll("\n", "\n" + (char)13) + (char)13);
									out.print("\u001B[1;33m");
									out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
									out.print("\u001B[0m");
									out.print("\u001B7"); // save cursor pos
								}else out.println(CMDIListener.process_cmd_raw(cmd));
								out.flush();
							}
							cmd = "";
						}else{
							// start of special key
							if(c == 0x1b){
								special = true;
								
							// tab
							}else if(c == 9){
								
							// bye
							}else if(c == 4){
								stopping = true;
							// backspace
							}else if(c == 127){
								if(cmd.length() > 0){
									//System.out.println("AAA");
									cmd = cmd.substring(0, cmd.length() - 1);
									out.print("\u001B[1D");
									out.print("\u001B[K");
									out.flush();
								}
							// clear
							}else if(c == 12){
								out.print("\u001B[2J");
								out.print("\u001B[0;0H");
								out.print("\u001B[1;33m");
								out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
								out.print("\u001B[0m");
								out.print(cmd);
								out.print("\u001B7"); // save cursor pos
								out.print("\u001B[0m");
								out.flush();
							// normal key
							}else if(c != 0){
								// special started
								if(special){
									if(buff[i] == 0x5b) buff_special[0] = 0x5b;
									else if(buff[i] == 0x41) buff_special[1] = 0x41;
									else if(buff[i] == 0x42) buff_special[1] = 0x42;
									else special = false;
									
									
									// up key
									if(buff_special[0] == 0x5b && buff_special[1] == 0x41 && last_cmd_lst.size() > 0){
										if(last_cmd_index < 0 || last_cmd_index >= last_cmd_lst.size()) last_cmd_index = last_cmd_lst.size() - 1;
										//System.out.println("LAST: " + last_cmd_lst.get(last_cmd_index));
										
										out.print("\u001B8"); // get cursor pos
										out.print("\u001B[K"); //clear line from cursor right
										
										out.print(last_cmd_lst.get(last_cmd_index));
										out.flush();
										//System.out.println("AAA");
										cmd = last_cmd_lst.get(last_cmd_index--);
										c = 0;
										special = false;
										buff_special[0] = -1;
										buff_special[1] = -1;
											
									// down key
									}else if(buff_special[0] == 0x5b && buff_special[1] == 0x42 && last_cmd_lst.size() > 0){
										if(last_cmd_index >= last_cmd_lst.size() || last_cmd_index < 0) last_cmd_index = 0;
										//System.out.println(last_cmd_index);
										
										out.print("\u001B8"); // get cursor pos
										out.print("\u001B[K"); //clear line from cursor right
										
										out.print(last_cmd_lst.get(last_cmd_index));
										out.flush();
										//System.out.println("AAA");
										cmd = last_cmd_lst.get(last_cmd_index++);
										c = 0;
										special = false;
										buff_special[0] = -1;
										buff_special[1] = -1;
	
									}
								// normal keuy
								}else{
									special = false;
									cmd += (char)c;
									// echo
									out.print((char)c);
									out.flush();
									
								}
							}
						}
					}
					
				}catch(SocketTimeoutException e){
					logger.error("CONNECTION TIMEOUT! [ " + con_ip + ":" + con_port + " ]");
					//e.printStackTrace();
					stopping = true;
					
				}catch(Exception e){
					logger.error("ERROR WHILE READING INPUT STREAM FOR CMDI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
					e.printStackTrace();
					stopping = true;
					
				}
				
			}
			logger.info("CMDI READER STOPPED: [ " + con_ip + ":" + con_port + " ]");
			logger.info("CLOSING CMDI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
			conn_list.remove(con_ip + ":" + con_port); 
			try{ tcp.close(); }catch(Exception e){}
			
		}
		
		
	}
	private class Writer_r implements Runnable{

		public void run() {
			while(!stopping){
				
				
			}
			
		}
		
		
	}
	
	private void init_threads(){
		reader_r = new Reader_r();
		//writer_r = new Writer_r();
		
		reader_t = new Thread(reader_r, "CMDI_CONN_R_" + con_ip + ":" + con_port);
		//writer_t = new Thread(writer_r, "CMDI_CONN_W_" + tcp.getInetAddress().toString() + ":" + tcp.getPort());
		
		reader_t.start();
		//writer_t.start();
		
		
	}
	
	
	
	public CMDIConnection(Socket socket, ConcurrentHashMap<String, CMDIConnection> _conn_list){
		tcp = socket;
		try{
			tcp.setSoTimeout(300000);
			in = new BufferedReader(new InputStreamReader(tcp.getInputStream()));
			inb = tcp.getInputStream();
			out = new PrintWriter(tcp.getOutputStream());
			outb = tcp.getOutputStream();
			con_ip = tcp.getInetAddress().toString();
			conn_list = _conn_list;
			con_port = tcp.getPort();
			init_threads();
		}catch(Exception e){
			logger.error("ERROR WHILE INITIALIZING INPUT/OUTPUT STREAMS FOR CMDI CONNECTION! [ " + con_ip + ":" + con_port + " ]");
			e.printStackTrace();
		}
		
		
	}
	
	
	
	
}
