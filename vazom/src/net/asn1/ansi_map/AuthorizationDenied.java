package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AuthorizationDenied extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _delinquent_account =  1 ;
	public static final int _invalid_serial_number =  2 ;
	public static final int _stolen_unit =  3 ;
	public static final int _duplicate_unit =  4 ;
	public static final int _unassigned_directory_number =  5 ;
	public static final int _unspecified =  6 ;
	public static final int _multiple_access =  7 ;
	public static final int _not_Authorized_for_the_MSC =  8 ;
	public static final int _missing_authentication_parameters =  9 ;
	public static final int _terminalType_mismatch =  10 ;
	public static final int _requested_Service_Code_Not_Supported =  11 ;
	public AuthorizationDenied(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
