group cli;



method_class(name, method_name, method_desc, method_parent, method_params) ::=
<<
public class CLI_<name> extends MethodDescriptor {

	public CLI_<name>() {
		super("<method_name>", "<method_desc>", "<method_parent>", "<method_params; separator=":">");
	}

	public String execute(Object[] params) {
		// TODO
		return null;
	}

}
public CLI_<name> _cli_<name> = new CLI_<name>();

>>

class(methods, instances, groups) ::=
<<
package net.cli;

import java.util.ArrayList;
import net.cli.CLIBase;
import net.cli.GroupDescriptor;
import net.cli.MethodDescriptor;

public class CLIManager extends CLIBase{
	<methods; separator="\n">

    
	public CLIManager(){
		super();
		// methods
		<instances; separator="\n">
		// groups
		<groups; separator="\n">
	}
}
>>

method_block(methods) ::= 
<<
<methods;separator="\n">
>>

method_block_complex(methods, grp_class, grp_desc, grp_parent, grp_params) ::= 
<<
public class GRP_<grp_class> extends GroupDescriptor {

	public GRP_<grp_class>() {
		super("<grp_class>", "<grp_desc>", "<grp_parent>", "<grp_params; separator=":">");
	}

	public String execute(Object[] params) {
		// TODO
		return null;
	}

}
public GRP_<grp_class> _grp_<grp_class> = new GRP_<grp_class>();


<methods;separator="\n">
>>


method(name, comment) ::=  <<
// <comment>
public String <name>(Object[] params){
	return null;
}
>>