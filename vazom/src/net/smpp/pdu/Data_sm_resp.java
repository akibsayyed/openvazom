package net.smpp.pdu;

import net.smpp.PDUType;
import net.utils.Utils;

public class Data_sm_resp extends PDUBase {
	public String message_id;

	public Data_sm_resp(){
		type = PDUType.DATA_SM_RESP;
	}

	public void init(byte[] data) {
		// Message ID
		message_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += message_id.length() + 1;
		// optional
		initOptional(data);

	}

}
