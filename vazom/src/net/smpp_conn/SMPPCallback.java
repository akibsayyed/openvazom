package net.smpp_conn;

import net.asn1.tcap2.TCMessage;
import net.routing.CallbackBase;
import net.smpp.pdu.PDUBase;
import net.smpp2ss7.SMPP2SS7Manager;

public class SMPPCallback extends CallbackBase {
	public long tcap_id;
	public SMPPClientConnection client_connection;
	public TCMessage tcm;

	public SMPPCallback(long _tcap_id, SMPPClientConnection _client_connection){
		super();
		tcap_id = _tcap_id;
		client_connection = _client_connection;
	}
	public SMPPCallback(SMPPClientConnection _client_connection){
		super();
		client_connection = _client_connection;
	}
	
	public long get_callback_id() {
		return tcap_id;
	}
	public void set_callback_id(long _id) {
		tcap_id = _id;
	}

	public void run() {
		PDUBase pdu = SMPP2SS7Manager.convert(tcm, client_connection.last_sequence);
		if(pdu != null){
			client_connection.out_offer(pdu.encode());
		}
		
	}

	public void set_params(Object[] params) {
		try{
			if(params != null){
				if(params.length > 0){
					tcm = (TCMessage)params[0];
				}
			}
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	
}
