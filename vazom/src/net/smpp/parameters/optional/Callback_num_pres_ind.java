package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Callback_num_pres_ind extends OptionalParameterBase {
	public enum ScreeningIndicator{
		NOT_SCREENED(0),
		VERIFIED_PASSED(1),
		VERIFIED_FAILED(2),
		NETWORK_PROVIDED(3);
		private int id;
		private static final HashMap<Integer, ScreeningIndicator> lookup = new HashMap<Integer, ScreeningIndicator>();
		static{
			for(ScreeningIndicator td : ScreeningIndicator.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ScreeningIndicator get(int id){ return lookup.get(id); }
		private ScreeningIndicator(int _id){ id = _id; }			
	}	
	public enum PresentationIndicator{
		ALLOWED(0),
		RESTRICTED(1),
		NOT_AVAILABLE(2),
		RESERVED(3);
		private int id;
		private static final HashMap<Integer, PresentationIndicator> lookup = new HashMap<Integer, PresentationIndicator>();
		static{
			for(PresentationIndicator td : PresentationIndicator.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static PresentationIndicator get(int id){ return lookup.get(id); }
		private PresentationIndicator(int _id){ id = _id; }			
	}
	public Callback_num_pres_ind(){
		type = OptionalParameterType.CALLBACK_NUM_PRES_IND;
	}
	public PresentationIndicator presentationIndicator;
	public ScreeningIndicator screeningIndicator;
	
	public void init(byte[] data) {
		presentationIndicator = PresentationIndicator.get(data[0] & 0x0c);
		screeningIndicator = ScreeningIndicator.get(data[0] & 0x03);
	}

}
