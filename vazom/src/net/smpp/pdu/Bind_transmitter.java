package net.smpp.pdu;

import java.util.ArrayList;

import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.utils.Utils;

public class Bind_transmitter extends PDUBase {
	public String system_id;
	public String password;
	public String system_type;
	public int interface_version;
	public TypeOfNumber addr_ton;
	public NumberingPlan addr_npi;
	public String address_range;
	
	public Bind_transmitter(){
		type = PDUType.BIND_TRANSMITTER;
	}

	
	public byte[] encode(){
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		Utils.bytesToLst(buff, Utils.Cstr_encode(system_id));
		Utils.bytesToLst(buff, Utils.Cstr_encode(password));
		Utils.bytesToLst(buff, Utils.Cstr_encode(system_type));
		buff.add((byte)interface_version);
		buff.add((byte)addr_ton.getId());
		buff.add((byte)addr_npi.getId());
		Utils.bytesToLst(buff, Utils.Cstr_encode(address_range));
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		//System.out.println(buff.size());
		return Utils.list2array(buff);
	}
	public void init(byte[] data) {
		// System ID
		system_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += system_id.length() + 1;
		// Password
		password = Utils.Cstr_decode(data, byte_pos);
		byte_pos += password.length() + 1;
		// System type
		system_type = Utils.Cstr_decode(data, byte_pos);
		byte_pos += system_type.length() + 1;
		// Interface version
		interface_version = data[byte_pos] & 0xff;
		byte_pos++;
		// type of number
		addr_ton = TypeOfNumber.get(data[byte_pos] & 0xff);
		byte_pos++;
		// numbering plan
		addr_npi = NumberingPlan.get(data[byte_pos] & 0xff);
		byte_pos++;
		// address range
		address_range = Utils.Cstr_decode(data, byte_pos);
		
		
		
	}
}
