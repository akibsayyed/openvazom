package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Ms_msg_wait_facilities extends OptionalParameterBase {
	public enum ValueType{
		VOICEMAIL_MESSAGE_WAITING(0x00),
		FAX_MESAGE_WAITING(0x01),
		ELECTRONIC_MAIL_MESSAGE_WAITING(0x02),
		OTHER_MESSAGE_WAITING(0x03);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public Ms_msg_wait_facilities(){
		type = OptionalParameterType.MS_MSG_WAIT_FACILITIES;
	}
	public ValueType value;
	public boolean indication;
	
	public void init(byte[] data) {
		indication = (data[0] & 0x80) == 0x80;
		value = ValueType.get(data[0] & 0x03);
		
	}

}
