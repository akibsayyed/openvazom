package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class BIT_STRING extends ASNType {

	public BIT_STRING(){
		super();
		universalTag = ASNTag.BIT_STRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
