package net.smpp.pdu;

import net.smpp.PDUType;
import net.utils.Utils;

public class Outbind extends PDUBase {
	public String system_id;
	public String password;

	public Outbind(){
		type = PDUType.OUTBIND;
	}

	public void init(byte[] data) {
		// System ID
		system_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += system_id.length() + 1;
		// Password
		password = Utils.Cstr_decode(data, byte_pos);

	}

}
