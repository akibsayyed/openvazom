package net.sccp.parameters;

import net.sccp.parameters.release_cause.ReleaseCauseType;

public class ReleaseCause extends ParamBase {
	public ReleaseCauseType releaseCause;
	public ReleaseCause(){
		super();
		type = ParamType.RELEASE_CAUSE;
	}

	public void init(byte[] data) {
		releaseCause = ReleaseCauseType.get(data[0] & 0xFF);
	}

}
