package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CallTypeCriteria extends ENUMERATED{

	public static final int _forwarded = 0;
	public static final int _notForwarded = 1;
	public CallTypeCriteria(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
