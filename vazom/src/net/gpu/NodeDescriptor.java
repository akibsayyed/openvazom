package net.gpu;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.net.SocketTimeoutException;
import java.util.ArrayList;

import net.gpu.cuda.CudaManager;
import net.stats.StatsManager;
import net.utils.Utils;

import org.apache.log4j.Logger;

public class NodeDescriptor {
	private static Logger logger=Logger.getLogger(NodeDescriptor.class);
	public String id;
	public String host;
	public int port;
	public int conn_count;
	public ArrayList<NodeType> type_lst;
	//public boolean available = false;
	//public boolean busy = false;
	public ArrayList<ConnectionDescriptor> connections;

	public synchronized void closeConnections(){
		for(int i = 0; i<connections.size(); i++){
			try{ connections.get(i).socket.close(); }catch(Exception e){}
		}
	}
	public int checkMD5(byte[] hash, ConnectionDescriptor cd, int lst_type){
		int res = 0;
		String reply = null;
		long ts;
		try{
			StatsManager.GPUB_STATS.MD5_REQUEST_COUNT++;
			
			ts = System.currentTimeMillis();
			cd.out.println("MD5." + lst_type + "." + Utils.bytes2hexstr(hash, ""));
			cd.out.flush();
			reply = cd.in.readLine();
			//reply = "R.0";
			if(reply != null){
				if(!reply.equalsIgnoreCase("ERROR") && !reply.equalsIgnoreCase("CERR") && !reply.equalsIgnoreCase("ERR110")){
					StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
					StatsManager.GPUB_STATS.md5_setMaxTS(StatsManager.GPUB_STATS.MD5_LAST_REPLY_TIME);
					
					res = Integer.parseInt(reply.substring(2));
					StatsManager.GPUB_STATS.MD5_REPLY_OK_COUNT++;
					return res;
				// check for errors
				}else{
					StatsManager.GPUB_STATS.MD5_REPLY_ERROR_COUNT++;
					if(reply.equalsIgnoreCase("CERR")){
						logger.error("MD5 Node [" + id + "] CUDA Error!");
					}else if(reply.equalsIgnoreCase("ERR110")){
						logger.error("MD5 Node [" + id + "] Unknown List ID Error!");
						
					}else{
						logger.error("MD5 Node [" + id + "] Unknown Error!");
						
					}
				}
			}else{
				StatsManager.GPUB_STATS.MD5_CONNECTION_DOWN_COUNT++;
				reconnect(cd);
			}
		}catch(SocketTimeoutException e){
			logger.error("Connection timeout while waiting for MD5 results...");
			//logger.error(data);
			reconnect(cd);
		}catch(IOException e){
			e.printStackTrace();
			reconnect(cd);
			//available = false;
			return 0;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
		
	}

	
	public int checkLD(int lst_id, String data, ConnectionDescriptor cd){
		int res = 0;
		String reply = null;
		long ts;
		try{
			StatsManager.GPUB_STATS.LD_REQUEST_COUNT++;
			
			ts = System.currentTimeMillis();
			cd.out.println("LD." + lst_id + "." + Utils.bytes2hexstr(data.getBytes(), ""));
			cd.out.flush();
			reply = cd.in.readLine();
			//reply = "R.0";
			if(reply != null){
				if(!reply.equalsIgnoreCase("ERROR") && !reply.equalsIgnoreCase("CERR") && !reply.equalsIgnoreCase("ERR110")){
					StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
					StatsManager.GPUB_STATS.ld_setMaxTS(StatsManager.GPUB_STATS.LD_LAST_REPLY_TIME);
					
					res = Integer.parseInt(reply.substring(2));
					StatsManager.GPUB_STATS.LD_REPLY_OK_COUNT++;
					return res;
				// check for errors
				}else{
					StatsManager.GPUB_STATS.LD_REPLY_ERROR_COUNT++;
					if(reply.equalsIgnoreCase("CERR")){
						logger.error("GPU Node [" + id + "] CUDA Error!");
					}else if(reply.equalsIgnoreCase("ERR110")){
						logger.error("GPU Node [" + id + "] Unknown List ID Error!");
						
					}else{
						logger.error("GPU Node [" + id + "] Unknown Error!");
						
					}
				}
			}else{
				StatsManager.GPUB_STATS.LD_CONNECTION_DOWN_COUNT++;
				reconnect(cd);
			}
		}catch(SocketTimeoutException e){
			logger.error("Connection timeout while waiting for LD results...");
			logger.error(data);
			reconnect(cd);
		}catch(IOException e){
			e.printStackTrace();
			reconnect(cd);
			//available = false;
			return 0;
			
		}catch(Exception e){
			e.printStackTrace();
		}
		return 0;
		
	}
	public void reconnect(ConnectionDescriptor cd){
		try{
			logger.info("GPU Node [" + id + "], [" + cd.type + "] connection down, trying to reconnect!");
			cd.socket = new Socket(host, port);
			cd.socket.setSoTimeout(3000);
			cd.in = new BufferedReader(new InputStreamReader(cd.socket.getInputStream()));
			cd.out = new PrintWriter(cd.socket.getOutputStream());
			cd.available = true;
			logger.info("GPU Node [" + id + "], connection ready!");
		}catch(Exception e){
			logger.error("GPU Node [" + id + "], connection down, error while reconnecting!");
			//e.printStackTrace();
		}
		
	}
	private void update_ld(int lst_id, String data){
		String reply = null;
		long ts;
		try{
			StatsManager.GPUB_STATS.UPDATE_REQUEST_COUNT++;
			ts = System.currentTimeMillis();
			connections.get(0).out.println("U." + lst_id + "." + Utils.bytes2hexstr(data.getBytes(), ""));
			connections.get(0).out.flush();
			reply = connections.get(0).in.readLine();
			if(reply == null){
				StatsManager.GPUB_STATS.UPDATE_CONNECTION_DOWN_COUNT++;
				logger.error("Error while updating GPU Node [" + id + "]!");
				reconnect(connections.get(0));
			}else{
				StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
				StatsManager.GPUB_STATS.update_setMaxTS(StatsManager.GPUB_STATS.UPDATE_LAST_REPLY_TIME);
				StatsManager.GPUB_STATS.UPDATE_REPLY_OK_COUNT++;
			}
			//logger.debug("Node [" + id + "], connection [" + i + "], updated!");
		}catch(SocketTimeoutException e){
			logger.error("Connection timeout while updating GPU Node [" + id + "]!");
			reconnect(connections.get(0));
			
		}catch(IOException e){
			logger.error("Connection IO error while updating GPU Node [" + id + "]!");
			reconnect(connections.get(0));
			
		}catch(Exception e){
			logger.error("Error while updating GPU Node [" + id + "]!");
			StatsManager.GPUB_STATS.UPDATE_CONNECTION_DOWN_COUNT++;
			e.printStackTrace();
		}
		
		
	}
	private void update_md5(byte[] hash, int lst_id){
		String reply = null;
		long ts;
		try{
			StatsManager.GPUB_STATS.MD5_UPDATE_REQUEST_COUNT++;
			ts = System.currentTimeMillis();
			connections.get(0).out.println("U." + lst_id + "." + Utils.bytes2hexstr(hash, ""));
			connections.get(0).out.flush();
			reply = connections.get(0).in.readLine();
			if(reply == null){
				StatsManager.GPUB_STATS.MD5_UPDATE_CONNECTION_DOWN_COUNT++;
				logger.error("Error while updating MD5 Node [" + id + "]!");
				reconnect(connections.get(0));
			}else{
				StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME = System.currentTimeMillis() - ts;
				StatsManager.GPUB_STATS.md5_update_setMaxTS(StatsManager.GPUB_STATS.MD5_UPDATE_LAST_REPLY_TIME);
				StatsManager.GPUB_STATS.MD5_UPDATE_REPLY_OK_COUNT++;
			}
			//logger.debug("Node [" + id + "], connection [" + i + "], updated!");
		}catch(SocketTimeoutException e){
			logger.error("Connection timeout while updating MD5 Node [" + id + "]!");
			reconnect(connections.get(0));
			
		}catch(IOException e){
			logger.error("Connection IO error while updating MD5 Node [" + id + "]!");
			reconnect(connections.get(0));
			
		}catch(Exception e){
			logger.error("Error while updating MD5 Node [" + id + "]!");
			StatsManager.GPUB_STATS.MD5_UPDATE_CONNECTION_DOWN_COUNT++;
			e.printStackTrace();
		}
		
		
		
	}
	public void remove(int lst_id, int item_id){
		//int res = 0;
		String reply = null;
		try{
			connections.get(0).out.println("R." + lst_id + "." + item_id);
			connections.get(0).out.flush();
			reply = connections.get(0).in.readLine();
			//reply = "R.0";
			if(reply != null){
				if(!reply.equalsIgnoreCase("ERROR") && !reply.equalsIgnoreCase("CERR") && !reply.equalsIgnoreCase("ERR110")){
					//res = Integer.parseInt(reply.substring(2));
					StatsManager.GPUB_STATS.LST_REMOVE_COUNT++;
				// check for errors
				}else{
					StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
					if(reply.equalsIgnoreCase("CERR")){
						logger.error("Node [" + id + "] CUDA Error!");
					}else if(reply.equalsIgnoreCase("ERR110")){
						logger.error("Node [" + id + "] Unknown List ID Error!");
						
					}else{
						logger.error("Node [" + id + "] Unknown Error!");
						
					}
				}
			}else{
				StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
				reconnect(connections.get(0));
			}
		}catch(SocketTimeoutException e){
			logger.error("Connection timeout while removing item from CUDA node...");
			//logger.error(data);
			reconnect(connections.get(0));
		}catch(IOException e){
			e.printStackTrace();
			reconnect(connections.get(0));
			//available = false;
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}
	public boolean typeExists(NodeType type){
		for(int i = 0; i<type_lst.size(); i++) if(type_lst.get(i) == type) return true;
		return false;
	}
	public void update(int lst_id, GPUDataPacket data){
		if(lst_id == CudaManager.MD5_QUARANTINE_LST || lst_id == CudaManager.MD5_NORMAL_LST) update_md5(data.hash_data, lst_id);
		else update_ld(lst_id, data.data);
	}
/*
	public void reconnect(){
		connections.clear();
		ConnectionDescriptor cd = null;
		try{
			logger.info("Repetition Node [" + id + "], reconnecting!");
			// UPDATER connections
			for(int i = 0; i<1; i++){
				cd = new ConnectionDescriptor(ConnectionType.UPDATER);
				cd.socket = new Socket(host, port);
				cd.socket.setSoTimeout(10000);
				cd.in = new BufferedReader(new InputStreamReader(cd.socket.getInputStream()));
				cd.out = new PrintWriter(cd.socket.getOutputStream());
				//logger.info("Repetition Node [" + id + "], [" + cd.type + "] connection [" + i + "] ready!");
				// add to list
				connections.add(cd);
			
			}
			// LD connections
			for(int i = 0; i<conn_count; i++){
				cd = new ConnectionDescriptor(ConnectionType.LD);
				cd.socket = new Socket(host, port);
				cd.socket.setSoTimeout(10000);
				cd.in = new BufferedReader(new InputStreamReader(cd.socket.getInputStream()));
				cd.out = new PrintWriter(cd.socket.getOutputStream());
				//logger.info("Repetition Node [" + id + "], [" + cd.type + "] connection [" + i + "] ready!");
				// add to list
				connections.add(cd);
			
			}
			
			available = true;
			//busy = false;
			//busy_count = 0;
			logger.info("Repetition Node [" + id + "], ready!");
			
		}catch(Exception e){
			logger.error("Repetition Node [" + id + "] not available!");
			
		}
		
	}
	*/
	public void addType(NodeType type){
		type_lst.add(type);
	}
	public NodeDescriptor(String _id, String _host, int _port, int _conn_count){
		ConnectionDescriptor cd = null;
		try{
			type_lst = new ArrayList<NodeType>();
			id = _id;
			host = _host;
			port = _port;
			conn_count = _conn_count;
			connections = new ArrayList<ConnectionDescriptor>();
			logger.info("GPU Node [" + id + "], connecting!");
			// UPDATER connection
			cd = new ConnectionDescriptor(ConnectionType.UPDATER);
			cd.socket = new Socket(host, port);
			cd.socket.setSoTimeout(10000);
			cd.in = new BufferedReader(new InputStreamReader(cd.socket.getInputStream()));
			cd.out = new PrintWriter(cd.socket.getOutputStream());
			logger.info("GPU Node [" + id + "], [" + cd.type + "] connection ready!");
			// add to list
			connections.add(cd);

			// KERNEL connections
			for(int i = 0; i<conn_count; i++){
				cd = new ConnectionDescriptor(ConnectionType.KERNEL);
				cd.socket = new Socket(host, port);
				cd.socket.setSoTimeout(10000);
				//cd.socket.setSoTimeout(10000);
				cd.in = new BufferedReader(new InputStreamReader(cd.socket.getInputStream()));
				cd.out = new PrintWriter(cd.socket.getOutputStream());
				logger.info("GPU Node [" + id + "], [" + cd.type + "] connection [" + i + "] ready!");
				// add to list
				cd.node = this;
				connections.add(cd);
			
			}
			
			//available = true;
			logger.info("GPU Node [" + id + "], ready!");
		}catch(Exception e){
			//available = false;
			logger.error("GPU Node [" + id + "] not available!");
			//e.printStackTrace();
		}
		
	}
}
