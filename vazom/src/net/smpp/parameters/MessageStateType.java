package net.smpp.parameters;

import java.util.HashMap;


public enum MessageStateType {
	ENROUTE(1),
	DELIVERED(2),
	EXPIRED(3),
	DELETED(4),
	UNDELIVERABLE(5),
	ACCEPTED(6),
	UNKNOWN(7),
	REJECTED(8);
	private int id;
	private static final HashMap<Integer, MessageStateType> lookup = new HashMap<Integer, MessageStateType>();
	static{
		for(MessageStateType td : MessageStateType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static MessageStateType get(int id){ return lookup.get(id); }
	private MessageStateType(int _id){ id = _id; }	
}
