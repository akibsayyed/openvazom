package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InformationDirective extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(2).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public AnnouncementList get_announcementList(){
		return (AnnouncementList)elements.get(3).data;
	}
	public AnnouncementList new_announcementList(){
		return new AnnouncementList();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(4).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(5).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(6).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(7).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DisplayText2 get_displayText2(){
		return (DisplayText2)elements.get(8).data;
	}
	public DisplayText2 new_displayText2(){
		return new DisplayText2();
	}

	public RedirectingNumberString get_redirectingNumberString(){
		return (RedirectingNumberString)elements.get(9).data;
	}
	public RedirectingNumberString new_redirectingNumberString(){
		return new RedirectingNumberString();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(10).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(11).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(130, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_announcementList(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(299, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText2(); }});
		elements.add(new ElementDescriptor(101, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberString(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
	}
	public InformationDirective(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
