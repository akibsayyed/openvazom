package net.ds.pds;

import net.ds.DataSourceType;
import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.NumberingPlan;


public class SmsInfo {
	public String gt_called;
	public String gt_calling;
	public int gt_called_tt;
	public int gt_calling_tt;
	public NatureOfAddress gt_called_nai;
	public NatureOfAddress gt_calling_nai;
	public NumberingPlan gt_called_np;
	public NumberingPlan gt_calling_np;
	public GlobalTitleIndicator gt_called_gti;
	public GlobalTitleIndicator gt_calling_gti;
	public String scda;
	public String scoa;
	public String imsi;
	public String msisdn;
	public int m3ua_data_dpc;
	public int m3ua_data_opc;
	public Long tcap_sid;
	public Long tcap_did;
	public Long invoke_id;
	public DataSourceType dataSource;
}
