package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudMediaDescriptorStreamsChoiceWrapper extends SEQUENCE{

	public IndAudMediaDescriptorStreamsChoice get_indAudMediaDescriptorStreamsChoice(){
		return (IndAudMediaDescriptorStreamsChoice)elements.get(0).data;
	}
	public IndAudMediaDescriptorStreamsChoice new_indAudMediaDescriptorStreamsChoice(){
		return new IndAudMediaDescriptorStreamsChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_indAudMediaDescriptorStreamsChoice(); }});
	}
	public IndAudMediaDescriptorStreamsChoiceWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
