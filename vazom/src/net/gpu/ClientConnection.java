package net.gpu;

import java.io.BufferedReader;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;

import net.config.GPUNConfigData;
import net.gpu.cuda.CudaCallback;
import net.gpu.cuda.CudaManager;
import net.gpu.cuda.KernelStats;
import net.utils.Utils;

public class ClientConnection {
	public class MD5Callback extends CudaCallback{
		public void execute(byte[] data) {
			try{
				// check for error
				// if CUDA error encountered, data will be of length 1, and item 0 will be the error code
				int rep_c = 0;
				if(data.length == 1 && (data[0] == -100 || data[0] == -110)){
					if(data[0] == -100){
						out.println("CERR");
						out.flush();
					}else if(data[0] == -110){
						out.println("ERR110");
						out.flush();
						
					}
				}else{
					for(int i = 0; i<data.length; i++) if(data[i] == 1) rep_c++;
					out.println("R." + rep_c);
					out.flush();
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
	}
	
	
	
	public class LDCallback extends CudaCallback{
		public void execute(byte[] data) {
			try{
				// check for error
				// if CUDA error encountered, data will be of length 1, and item 0 will be the error code
				int rep_c = 0;
				if(data.length == 1 && (data[0] == -100 || data[0] == -110)){
					if(data[0] == -100){
						out.println("CERR");
						out.flush();
					}else if(data[0] == -110){
						out.println("ERR110");
						out.flush();
						
					}
				}else{
					for(int i = 0; i<data.length; i++) if(data[i] >= GPUNConfigData.ld_threshold) rep_c++;
					out.println("R." + rep_c);
					out.flush();
					
				}
			}catch(Exception e){
				e.printStackTrace();
			}
		}
		
		
	}
	
	public class Reader_r implements Runnable{
		public void run() {
			String line;
			String[] tmp_lst;
			int lst_id;
			int item_id;
			while(!stopping){
				try{
					line = in.readLine();
					if(line.equalsIgnoreCase("BYE")){
						stopping = true;
					// S.GPU_ID
					}else if(line.startsWith("S.")){
						tmp_lst = line.split("\\.");
						KernelStats ks = GPUNode.getStats(Integer.parseInt(tmp_lst[1]));
						out.println(ks.execution_count + ":" + ks.last_execution_time + ":" + ks.max_execution_time + ":" + ks.update_q_size + ":" + ks.ld_q_size + ":" + ks.md5_q_size);
						out.flush();
					// R.LST_TYPE.ITEM_ID
					}else if(line.startsWith("R.")){
						tmp_lst = line.split("\\.");
						lst_id = Integer.parseInt(tmp_lst[1]);
						item_id = Integer.parseInt(tmp_lst[2]);
						GPUNode.remove(lst_id, item_id);
						out.println("OK");
						out.flush();
					// U.LST_TYPE.DATA
					}else if(line.startsWith("U.")){
						tmp_lst = line.split("\\.");
						lst_id = Integer.parseInt(tmp_lst[1]);
						GPUNode.update(lst_id, Utils.strhex2bytes(tmp_lst[2]));
						out.println("OK");
						out.flush();
					// MD5.LST_TYPE.DATA
					}else if(line.startsWith("MD5.")){
						tmp_lst = line.split("\\.");
						lst_id = Integer.parseInt(tmp_lst[1]);
						GPUNode.check(lst_id, Utils.strhex2bytes(tmp_lst[2]), md5_callback);
					// LD.LST_TYPE.DATA
					}else if(line.startsWith("LD.")){
						tmp_lst = line.split("\\.");
						lst_id = Integer.parseInt(tmp_lst[1]);
						GPUNode.check(lst_id,Utils.strhex2bytes(tmp_lst[2]), callback);
					}else{
						out.println("ERROR");
						out.flush();
						
					}
				}catch(Exception e){
					stopping = true;
					con_lst.remove(con_ip + ":" + con_port); 
					try{
						socket.close();
					}catch(Exception e2){
						e2.printStackTrace();
					}
				}
			}
			con_lst.remove(con_ip + ":" + con_port); 
			try{
				socket.close();
			}catch(Exception e){
				e.printStackTrace();
			}
			
		}
		
	}
	public LDCallback callback;
	public MD5Callback md5_callback;
	private ConcurrentHashMap<String, ClientConnection> con_lst;
	private Socket socket;
	private Reader_r reader_r;
	private Thread reader_t;
	public boolean stopping;
	private String con_ip;
	private int con_port;
	private BufferedReader in;
	private PrintWriter out;
	
	public ClientConnection(Socket _socket, ConcurrentHashMap<String, ClientConnection> _lst){
		try{
			con_lst = _lst;
			socket = _socket;
			con_ip = socket.getInetAddress().toString();
			con_port = socket.getPort();
			in = new BufferedReader(new InputStreamReader(socket.getInputStream()));
			out = new PrintWriter(socket.getOutputStream());
			callback = new LDCallback();
			md5_callback = new MD5Callback();
			reader_r = new Reader_r();
			reader_t = new Thread(reader_r, "RN_READER");
			reader_t.start();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
