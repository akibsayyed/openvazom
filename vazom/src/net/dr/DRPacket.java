package net.dr;

import net.ds.DataSourceType;
import net.vstp.MessageDescriptor;

public class DRPacket {
	public DataSourceType dataSource;
	public byte[] data;
	public MessageDescriptor vstp_data;
	
	public DRPacket(DataSourceType ds, byte[] _data){
		dataSource = ds;
		data = _data;
	}
	public DRPacket(DataSourceType ds, MessageDescriptor _data){
		dataSource = ds;
		vstp_data = _data;
	}
}
