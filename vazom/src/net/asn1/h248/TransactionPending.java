package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionPending extends SEQUENCE{

	public TransactionId get_transactionId(){
		return (TransactionId)elements.get(0).data;
	}
	public TransactionId new_transactionId(){
		return new TransactionId();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionId(); }});
	}
	public TransactionPending(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
