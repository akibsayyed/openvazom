package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ISDN_SubaddressString extends OCTET_STRING{

	public ISDN_SubaddressString(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
