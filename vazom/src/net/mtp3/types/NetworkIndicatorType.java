package net.mtp3.types;

import java.util.HashMap;
//bits xx...... (mask 0xc0)
public enum NetworkIndicatorType {
	INTERNATIONAL_NETWORK(0x00),
	SPARE_INTERNATIONAL(0x40),
	NATIONAL_NETWORK(0x80),
	RESERVED_NATIONAL(0xc0);
	
	private int id;
	private static final HashMap<Integer, NetworkIndicatorType> lookup = new HashMap<Integer, NetworkIndicatorType>();
	static{
		for(NetworkIndicatorType td : NetworkIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NetworkIndicatorType get(int id){ return lookup.get(id); }
	private NetworkIndicatorType(int _id){ id = _id; }	
}
