package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class CHOICE extends ASNType {
	public CHOICE(){
		super();
		universalTag = ASNTag.CHOICE;
		tag = -3;
		asn_pc = ASNTagComplexity.Constructed;
		
		
	}

}
