package net.fn;



import java.util.HashMap;

import net.config.FNConfigData;
import net.db.DBManager;
import net.db.DBRecordType;
import net.dr.DRManager;
import net.ds.DataSourceType;
import net.flood.FloodManager;
import net.hlr.HLRManager;
import net.hlr.HLRPacket;
import net.smsfs.SmsfsManager;
import net.smsfs.SmsfsPacket;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class SGWorker {
	public static Logger logger=Logger.getLogger(SGWorker.class);
	public int id;
	private Worker_r worker_r;
	private Thread worker_t;
	private boolean stopping;
	
	

	private class Worker_r implements Runnable{
		MessageDescriptor md = null;
		MessageDescriptor md_new = null;
		DBRecordType dbr_type = null;
		SmsfsPacket fp = null;
		SGDescriptor dest_sgn = null;
		boolean smsfs_active = false;
		int smsfs_action;
		int smsfs_module;
		String tmp = null;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				md = FNode.in_poll();
				if(md != null){
					try{
						// SGC
						if(md.header.msg_type == MessageType.SGC && md.exists(VSTPDataItemType.VSTP_SMSFS_ACTION.getId()) && md.exists(VSTPDataItemType.VSTP_SMSFS_MODULE.getId())){
							smsfs_action = Integer.parseInt(md.values.get(VSTPDataItemType.VSTP_SMSFS_ACTION.getId()));
							smsfs_module = Integer.parseInt(md.values.get(VSTPDataItemType.VSTP_SMSFS_MODULE.getId()));
							switch(smsfs_module){
								// MD5
								case 0: 
									// ADD
									if(smsfs_action == 1){
										//MD5Manager.addItem(new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.VSTP_SMSFS_DATA.getId()))), FilterMode.NORMAL);
									// REMOVE
									}else if(smsfs_action == 0){
										//MD5Manager.removeItem(Utils.strhex2bytes(md.values.get(VSTPDataItemType.VSTP_SMSFS_DATA.getId())), FilterMode.NORMAL);
									}
									break;
								// QUARANTINE
								case 1: 
									// ADD
									if(smsfs_action == 1){
										//QuarantineManager.addItem(new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.VSTP_SMSFS_DATA.getId()))), FilterMode.NORMAL);
									// REMOVE
									}else if(smsfs_action == 0){
										//QuarantineManager.removeItem(Utils.strhex2bytes(md.values.get(VSTPDataItemType.VSTP_SMSFS_DATA.getId())), FilterMode.NORMAL);
										
									}
									break;
								// KNOWN SPAM
								case 2: 
									// ADD
									if(smsfs_action == 1){
										//SpamManager.addItem(new String(Utils.strhex2bytes(md.values.get(VSTPDataItemType.VSTP_SMSFS_DATA.getId()))), FilterMode.NORMAL);
										
									// REMOVE
									}else if(smsfs_action == 0){
										//SpamManager.removeItem(Utils.strhex2bytes(md.values.get(VSTPDataItemType.VSTP_SMSFS_DATA.getId())), FilterMode.NORMAL);
										
									}
									break;
							}
						// DB REQUEST
						}else if(md.header.msg_type == MessageType.DB_REQUEST){
							if(md.values.get(VSTPDataItemType.DB_REQUEST_TYPE.getId()) != null){
								tmp = md.values.get(VSTPDataItemType.DB_REQUEST_TYPE.getId());
								// ss7->smpp error conversion
								if(tmp.equals(VSTPDataItemType.DB_SS7_SMPP_EC_CONVERSION.getId())){
									HashMap<Integer, Integer> tmp_ec_map = DBManager.get_ss7_smpp_ec_conversion_map();
									if(tmp_ec_map != null){
										for(Integer ss7_ec_item : tmp_ec_map.keySet()){
											md_new = new MessageDescriptor();
											// header
											md_new.header.msg_type = MessageType.DB_REPLY;
											md_new.header.destination = LocationType.SGN;
											md_new.header.ds = DataSourceType.NA;
											md_new.header.msg_id = md.header.msg_id;
											md_new.header.source = LocationType.FN;
											// body
											md_new.values.put(VSTPDataItemType.DB_REPLY_TYPE.getId(), VSTPDataItemType.DB_SS7_SMPP_EC_CONVERSION.getId());
											md_new.values.put(VSTPDataItemType.SS7_ERROR_CODE.getId(), ss7_ec_item.toString());
											md_new.values.put(VSTPDataItemType.SMPP_ERROR_CODE.getId(), tmp_ec_map.get(ss7_ec_item).toString());

											// send back to SG node
											// get originating SG node
											dest_sgn = SGNManager.getNode(md.header.source_id);
											// if node found
											if(dest_sgn != null){
												// send back to originating SG node
												dest_sgn.private_out_queue.offer(md_new);
											}
											
										}
									}
								// smpp users sync
								}else if(tmp.equals(VSTPDataItemType.DB_SMPP_USER_SYNC.getId())){
									HashMap<String, String> tmp_smpp_users_map = DBManager.get_smpp_users_map();
									if(tmp_smpp_users_map != null){
										for(String smpp_user_item : tmp_smpp_users_map.keySet()){
											md_new = new MessageDescriptor();
											// header
											md_new.header.msg_type = MessageType.DB_REPLY;
											md_new.header.destination = LocationType.SGN;
											md_new.header.ds = DataSourceType.NA;
											md_new.header.msg_id = md.header.msg_id;
											md_new.header.source = LocationType.FN;
											// body
											md_new.values.put(VSTPDataItemType.DB_REPLY_TYPE.getId(), VSTPDataItemType.DB_SMPP_USER_SYNC.getId());
											md_new.values.put(VSTPDataItemType.SMPP_SYSTEM_ID.getId(), smpp_user_item);
											md_new.values.put(VSTPDataItemType.SMPP_PASSWORD.getId(), tmp_smpp_users_map.get(smpp_user_item).toString());
											
											// send back to SG node
											// get originating SG node
											dest_sgn = SGNManager.getNode(md.header.source_id);
											// if node found
											if(dest_sgn != null){
												// send back to originating SG node
												dest_sgn.private_out_queue.offer(md_new);
											}
											
										}
										
									}
									
								}
							}
						// SMSFS HLR REPLY
						}else if(md.header.msg_type == MessageType.HLRRPL){
							HLRPacket hlrp = HLRManager.getHLRP(md.header.msg_id);
							if(hlrp != null){
								//System.out.println(new String(md.encode()));
								// fill data
								
								hlrp.imsi = md.values.get(VSTPDataItemType.HLR_IMSI.getId());
								hlrp.nnn = md.values.get(VSTPDataItemType.HLR_NNN.getId());
								hlrp.annn = md.values.get(VSTPDataItemType.HLR_ANNN.getId());
								//System.out.println("IMSI: " + hlrp.imsi);
								//System.out.println("NNN: " + hlrp.nnn);

								// unlock mutex
								synchronized(hlrp.mutex){
									hlrp.mutex.notify();
								}
							}
						// HLR/SMS data
						}else{
							//md.header.ds
							switch(md.header.msg_type){
								case HLR:
									dbr_type = DBRecordType.SRI;
									smsfs_active = FNConfigData.smsfs_hlr_intrusive_status;
									break;
								case SMS_MO:
								case SMS_MT:
								case SMPP_MO:
								case SMPP_MT:
									dbr_type = DBRecordType.SMS; 
									smsfs_active = FNConfigData.smsfs_sms_intrusive_status;
									break;
								case MAP_RETURN_ERROR:
								case MAP_RETURN_RESULT:
								case TCAP_ABORT:
									dbr_type = DBRecordType.ACK_ERR;
									smsfs_active = false;
									break;
								// ISUP
								case ISUP_IAM:
								case ISUP_ACM:
								case ISUP_ANM:
								case ISUP_REL:
								case ISUP_RLC:
									dbr_type = DBRecordType.ISUP;
									smsfs_active = FNConfigData.smsfs_isup_intrusive_status;
									break;
									
							}
							
							// flood
							if(FNConfigData.flood_status){
								switch(md.header.msg_type){
									case SMS_MO:
										FloodManager.calcInc(md.values.get(VSTPDataItemType.SCCP_GT_CALLED_ADDRESS.getId()), MessageType.SMS_MO, true);
										FloodManager.calcInc(md.values.get(VSTPDataItemType.MAP_SCDA.getId()), md.header.msg_type, false);
										FloodManager.calcInc(md.values.get(VSTPDataItemType.SMS_TPDU_DESTINATION.getId()), md.header.msg_type, false);
										break;
									case SMS_MT:
										FloodManager.calcInc(md.values.get(VSTPDataItemType.SCCP_GT_CALLING_ADDRESS.getId()), MessageType.SMS_MT, true);
										FloodManager.calcInc(md.values.get(VSTPDataItemType.MAP_SCOA.getId()), md.header.msg_type, false);
										FloodManager.calcInc(md.values.get(VSTPDataItemType.SMS_TPDU_ORIGINATING.getId()), md.header.msg_type, false);
										break;
									// TODO
									// smpp
								}
							}
							
							
							// DR
							// no reply needed
							// NON INTRUSIVE
							if(md.header.msg_id.equals("-")){
								DRManager.offerDRP(md.header.ds, dbr_type, md);
							// filter reply needed
							// INTRUSIVE
							}else{
								/*
								System.out.println("----- DEBUG START --------");
								System.out.println(new String(md.encode()));
								System.out.println("----- DEBUG END --------");
								*/
								// check if filter module active
								if(FNConfigData.smsfs_status && smsfs_active){
									fp = SGNManager.prepareFilter(md);
									SmsfsManager.queue.offer(fp);
								// filter is OFF
								}else{
									// DB
									//System.out.println("-------------- SGWorker----------");
									//System.out.println(new String(md.encode()));
									DRManager.offerDRP(md.header.ds, dbr_type, md);

									// send reply to SG
									md_new = new MessageDescriptor();
									md_new.header = md.header;
									md_new.values.put(VSTPDataItemType.FILTER_RESULT.getId(), "1");
									
									// send back to SG node
									// get originating SG node
									dest_sgn = SGNManager.getNode(md.header.source_id);
									// if node found
									if(dest_sgn != null){
										// send back to originating SG node
										dest_sgn.private_out_queue.offer(md_new);
									// else sent to SG node pool
									}else{
										// send to out queue
										FNode.out_offer(md_new);
										
									}

								}
							}
							
						}
						
						//System.out.println("---------VSTP PACKET START----------");
						//System.out.println(new String(md.encode()));
						//System.out.println("---------VSTP PACKET END----------");

					}catch(Exception e){
						e.printStackTrace();
					}
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
				
			}
			
			logger.info("Ending...");
			
		}
	}	
	

	public SGWorker(int _id){
		id = _id;
		logger.info("Strting SGWorker: [" + _id + "]!");
		// worker thread
		worker_r = new Worker_r();
		worker_t = new Thread(worker_r, "SG_WORKER_" + id);
		worker_t.start();

	}
}
