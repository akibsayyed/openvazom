package net.isup.parameters;

import net.isup.ParameterType;

public class NatureOfConnIndicators extends ParameterBase {
	public NatureOfConnIndicators(){
		type = ParameterType.NATURE_OF_CONNECTION_INDICATORS;
	}
}
