package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MAP_ERROR extends CHOICE{

	public LocalErrorcode get_localValue(){
		return (LocalErrorcode)elements.get(0).data;
	}
	public LocalErrorcode new_localValue(){
		return new LocalErrorcode();
	}

	public OBJECT_IDENTIFIER get_globalValue(){
		return (OBJECT_IDENTIFIER)elements.get(1).data;
	}
	public OBJECT_IDENTIFIER new_globalValue(){
		return new OBJECT_IDENTIFIER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_localValue(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_globalValue(); }});
	}
	public MAP_ERROR(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
