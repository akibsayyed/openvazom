package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class RELATIVE_OID extends ASNType {

	public RELATIVE_OID(){
		super();
		universalTag = ASNTag.RELATIVE_OID;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Primitive;
	}
}
