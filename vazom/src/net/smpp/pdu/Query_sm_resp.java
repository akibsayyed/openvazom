package net.smpp.pdu;

import net.smpp.ErrorCodeType;
import net.smpp.PDUType;
import net.utils.Utils;

public class Query_sm_resp extends PDUBase {
	public String message_id;
	public String final_date;
	public int message_state;
	public ErrorCodeType error_code;
	
	public Query_sm_resp(){
		type = PDUType.QUERY_SM_RESP;
	}

	public void init(byte[] data) {
		// message id
		message_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += message_id.length() + 1;
		// final date
		final_date = Utils.Cstr_decode(data, byte_pos);
		byte_pos += final_date.length() + 1;
		// message state
		message_state = data[byte_pos++] & 0xff;
		// error code
		error_code = ErrorCodeType.get(data[byte_pos++] & 0xff);
		
	}

}
