package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Dlr;

public class ED_ExpeditedData extends MessageBase {
	public Dlr destinationLocalReference;
	public byte[] data;
	
	public ED_ExpeditedData(){
		super();
		type = MessageType.ED_EXPEDITED_DATA;
		pointers = new Pointer[1];
	}
	
	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;

		// Second mandatory variable part
		data = Arrays.copyOfRange(_data, pointers[0].position + 1, pointers[0].position + 1 + (_data[pointers[0].position] & 0xFF));
		
	}

}
