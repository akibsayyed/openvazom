package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ChannelData extends OCTET_STRING{

	public ChannelData(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
