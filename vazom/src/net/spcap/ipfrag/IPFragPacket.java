package net.spcap.ipfrag;
import java.util.HashMap;

import net.spcap.IPHeader;

public class IPFragPacket {
	public HashMap<Integer, IPHeader> ip_fragment_map;
	public long ts;
	
	
	
	public IPFragPacket(){
		ip_fragment_map = new HashMap<Integer, IPHeader>();
		ts = System.currentTimeMillis();
	}
	
	
	public void new_fragment(IPHeader iph){
		if(ip_fragment_map.get(iph.fragment_offset) == null) ip_fragment_map.put(iph.fragment_offset, iph);
	}
	
	public int get_total_length(){
		IPHeader iph = null;
		for(Integer id : ip_fragment_map.keySet()){
			iph = ip_fragment_map.get(id);
			if(iph != null){
				if(!iph.more_fragments && iph.fragment_offset > 0){
					return iph.fragment_offset + (iph.totalLength - iph.headerLength);
				}
				
			}
		}
		return 0;
	}
	
}
