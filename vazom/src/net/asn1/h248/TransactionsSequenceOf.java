package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionsSequenceOf extends SEQUENCE{

	public Transaction new_child(){
		return new Transaction();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public Transaction getChild(int index){
		return (Transaction)of_children.get(index);
	}
	public TransactionsSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
