__global__ void _SEARCH_KERNEL(char *data_lst, int *data_lst_size, char *needle, int *needle_size, int *max_needle_size, int *needle_length_size, char *result){
        volatile unsigned int thread_id = (blockIdx.y * gridDim.x*blockDim.x) + (blockIdx.x*blockDim.x) + threadIdx.x;
        volatile unsigned int lst_msg_id = thread_id * (*max_needle_size + *needle_length_size);
	int msg_length;
		
        // check if current thread id is >= data_lst_size
        if(thread_id >= *data_lst_size) return;
	// get message length
	if(*needle_length_size > 0) msg_length = data_lst[lst_msg_id] & 0xff;
	else msg_length = 16;

	// if lengths are equal, try to match	
	if(msg_length == *needle_size){
		for(int i = 0; i<msg_length; i++){
			if(data_lst[lst_msg_id + i + *needle_length_size] != needle[i]){
				// not equal, return
				result[thread_id] = 0;
				return;
			}	
		}
		// matched
		result[thread_id] = 1;
		return;
	} 


	// default result	
	result[thread_id] = 0;

}


void SEARCH_KERNEL(char *data_lst, int *data_lst_size, char *needle, int *needle_size, int *max_needle_size, int *needle_length_size, char *result, dim3 blocks, dim3 threads){
	_SEARCH_KERNEL<<<blocks, threads>>>(data_lst, data_lst_size, needle, needle_size, max_needle_size, needle_length_size, result);
}
