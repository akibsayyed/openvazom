package net.smstpdu;

import java.util.HashMap;


public enum TypeOfNumber {
	UNKNOWN(0x00),
	INTERNATIONAL(0x10),
	NATIONAL(0x20),
	NETWORK_SPECIFIC(0x30),
	SUBSCRIBER_NUMBER(0x40),
	ALPHANUMERIC(0x50),
	ABBREVIATED(0x60),
	RESERVED(0x70);

	
	// BITS 6, 5, 4 = 0XXX0000
	private int id;
	private static final HashMap<Integer, TypeOfNumber> lookup = new HashMap<Integer, TypeOfNumber>();
	static{
		for(TypeOfNumber td : TypeOfNumber.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static TypeOfNumber get(int id){ return lookup.get(id); }
	private TypeOfNumber(int _id){ id = _id; }


}
