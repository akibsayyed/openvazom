package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Dest_network_type extends OptionalParameterBase {
	public enum ValueType{
		UNKNOWN(0x00),
		GSM(0x01),
		ANSI_136(0x02),
		IS_95(0x03),
		PDC(0x04),
		PHS(0x05),
		IDEN(0x06),
		AMPS(0x07),
		PAGING_NETWORK(0x08);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public ValueType value;

	public Dest_network_type(){
		type = OptionalParameterType.DEST_NETWORK_TYPE;
		
	}

	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
