package net.gpu2;

import java.util.ArrayList;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.config.ConfigManagerV2;
import net.config.FNConfigData;
import net.gpu2.ConnectionDescriptor;
import net.gpu2.ConnectionType;
import net.gpu2.GPUDataPacket;
import net.gpu2.NodeDescriptor;
import net.gpu2.NodeType;
import net.gpu.cuda.CudaManager;
import net.logging.LoggingManager;
import net.stats.StatsManager;

import org.apache.log4j.Logger;

public class GPUBalancer {
	private static Logger logger=Logger.getLogger(GPUBalancer.class);
	public static ArrayList<NodeDescriptor> node_lst;
	public static ConcurrentLinkedQueue<GPUDataPacket> update_queue;
	public static ConcurrentLinkedQueue<ConnectionDescriptor> device_connection_pool;
	private static Updater_r updater_r;
	private static Thread updater_t;
	private static boolean stopping;
	private static int total_connection_count = 0;

	
	public static class Updater_r implements Runnable{
		GPUDataPacket packet = null;
		NodeDescriptor nd = null;
		public void run() {
			LoggingManager.debug(FNConfigData.gpub_debug, logger, "STARTING!");
			while(!stopping){
				packet = update_queue.poll();
				if(packet != null){
					// add new item
					if(packet.action_type == CudaManager.ACT_NEW){
						for(int i = 0; i<node_lst.size(); i++){
							// check if node supports current packet type
							if(node_lst.get(i).typeExists(packet.type)) node_lst.get(i).update(packet.lst_id, packet);
						}
					// delete item 
					}else if(packet.action_type == CudaManager.ACT_DELETE){
						for(int i = 0; i<node_lst.size(); i++){
							// check if node supports current packet type
							if(node_lst.get(i).typeExists(packet.type)) node_lst.get(i).remove(packet.lst_id, packet.msg_bytes);
						}
					// reset
					}else if(packet.action_type == CudaManager.ACT_RESET){
						for(int i = 0; i<node_lst.size(); i++){
							// check if node supports current packet type
							if(node_lst.get(i).typeExists(packet.type)) node_lst.get(i).reset(packet.lst_id);
						}
						
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
				
				
			}
			LoggingManager.debug(FNConfigData.gpub_debug, logger, "ENDING!");
		}
		
	}
	private static ConnectionDescriptor getConnection(NodeType type){
		ConnectionDescriptor cd = null;
		long l = System.currentTimeMillis();
		boolean done = false;
		while(!done){
			cd = device_connection_pool.poll();
			if(cd != null){
				if(cd.available){
					// check if node supports current type
					for(int j = 0; j<cd.node.type_lst.size(); j++) if(cd.node.type_lst.get(j) == type){
						return cd;
					}
					// if not found push back
					device_connection_pool.offer(cd);
				// push back
				}else{
					// reconnect
					cd.node.reconnect(cd);
					// push back
					device_connection_pool.offer(cd);
					
				}
				
			}
			if(System.currentTimeMillis() - l > 10000) done = true;
			//i++;
		}
		LoggingManager.debug(FNConfigData.gpub_debug, logger, "GPUBalancer getConnection: no available connection for type [" + type + "]!");
		
		return null;
		
	}
	public static void remove(int lst_id, int item_id, NodeType ntype){
		GPUDataPacket rp = null;
		try{
			rp = new GPUDataPacket();
			rp.lst_id = lst_id;
			rp.type = ntype;
			rp.action_type = CudaManager.ACT_DELETE;
			rp.action_param = item_id;
			update_queue.offer(rp);
		}catch(Exception e){
			StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
			e.printStackTrace();
		}
	}
	
	public static void reset(int lst_id, NodeType ntype){
		GPUDataPacket rp = null;
		try{
			rp = new GPUDataPacket();
			rp.lst_id = lst_id;
			rp.type = ntype;
			rp.action_type = CudaManager.ACT_RESET;
			update_queue.offer(rp);
		}catch(Exception e){
			StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
			e.printStackTrace();
		}
	}
	public static void remove(int lst_id, byte[] data, NodeType ntype){
		if(lst_id != CudaManager.LD_REPETITION){
			GPUDataPacket rp = null;
			try{
				rp = new GPUDataPacket();
				rp.lst_id = lst_id;
				switch(lst_id){
					case CudaManager.MD5_QUARANTINE_LST:
						rp.msg_bytes = data;
						rp.type = ntype; 
						break;
					case CudaManager.MD5_NORMAL_LST:
						rp.type = ntype; 
						rp.msg_bytes = data;
						break;
					case CudaManager.LD_SPAM: 
						rp.type = ntype; 
						rp.msg_bytes = data;
						break;
				}
				rp.action_type = CudaManager.ACT_DELETE;
				//rp.action_param = item_id;
				update_queue.offer(rp);
			}catch(Exception e){
				StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
				e.printStackTrace();
			}
			
		}else LoggingManager.error(logger, "Remove action not available on [REPETITION] lst!");
	}

	
	public static void remove(int lst_id, byte[] data){
		if(lst_id != CudaManager.LD_REPETITION){
			GPUDataPacket rp = null;
			try{
				rp = new GPUDataPacket();
				rp.lst_id = lst_id;
				switch(lst_id){
					case CudaManager.MD5_QUARANTINE_LST:
						rp.msg_bytes = data;
						rp.type = NodeType.QUARANTINE; 
						break;
					case CudaManager.MD5_NORMAL_LST:
						rp.type = NodeType.NORMAL; 
						rp.msg_bytes = data;
						break;
					case CudaManager.LD_SPAM: 
						rp.type = NodeType.NORMAL; 
						rp.msg_bytes = data;
						break;
				}
				rp.action_type = CudaManager.ACT_DELETE;
				//rp.action_param = item_id;
				update_queue.offer(rp);
			}catch(Exception e){
				StatsManager.GPUB_STATS.LST_REMOVE_ERR_COUNT++;
				e.printStackTrace();
			}
			
		}else LoggingManager.error(logger, "Remove action not available on [REPETITION] lst!");
	}
	public static void update_md5(byte[] hash, int lst_type, NodeType ntype){
		if(hash != null){
			if(hash.length == 16){
				GPUDataPacket rp = new GPUDataPacket();
				rp.lst_id = lst_type;
				rp.type = ntype;
				rp.action_type = CudaManager.ACT_NEW;
				rp.hash_data = hash;
				update_queue.offer(rp);
			}//else logger.warn("UPDATER: messasge length [" + data.getBytes().length + "] exceeds max available length [" + ConfigData.rp_ld_max_length + "]!");
			
		}
	}

	public static void update_md5(byte[] hash, int lst_type){
		if(hash != null){
			if(hash.length == 16){
				GPUDataPacket rp = new GPUDataPacket();
				rp.lst_id = lst_type;
				switch(lst_type){
					case CudaManager.MD5_QUARANTINE_LST: rp.type = NodeType.QUARANTINE; break;
					case CudaManager.MD5_NORMAL_LST: rp.type = NodeType.NORMAL; break;
				}
				rp.action_type = CudaManager.ACT_NEW;
				rp.hash_data = hash;
				update_queue.offer(rp);
			}//else logger.warn("UPDATER: messasge length [" + data.getBytes().length + "] exceeds max available length [" + ConfigData.rp_ld_max_length + "]!");
			
		}
	}

	
	public static void update(int lst_id, String data){
		if(data != null){
			if(data.getBytes().length >= FNConfigData.gpub_ld_min_length && data.getBytes().length <= FNConfigData.gpub_ld_max_length){
				GPUDataPacket rp = new GPUDataPacket();
				rp.lst_id = lst_id;
				rp.type = NodeType.NORMAL;
				rp.action_type = CudaManager.ACT_NEW;
				rp.data = data;
				update_queue.offer(rp);
			}//else logger.warn("UPDATER: messasge length [" + data.getBytes().length + "] exceeds max available length [" + ConfigData.rp_ld_max_length + "]!");
			
		}
	}
	public static void update(int lst_id, String data, NodeType ntype){
		if(data != null){
			if(data.getBytes().length >= FNConfigData.gpub_ld_min_length && data.getBytes().length <= FNConfigData.gpub_ld_max_length){
				GPUDataPacket rp = new GPUDataPacket();
				rp.lst_id = lst_id;
				rp.type = ntype;
				rp.action_type = CudaManager.ACT_NEW;
				rp.data = data;
				update_queue.offer(rp);
			}//else logger.warn("UPDATER: messasge length [" + data.getBytes().length + "] exceeds max available length [" + ConfigData.rp_ld_max_length + "]!");
			
		}
	}
	public static int checkMD5(byte[] hash, int lst_type, NodeType ntype){
		if(hash != null){
			if(hash.length == 16){
				ConnectionDescriptor cd = null;
				int res = 0;
				try{
					cd = getConnection(ntype);
					// fetch data
					if(cd != null){
						res = cd.node.checkMD5(hash, cd, lst_type);
						// release connection
						device_connection_pool.offer(cd);
						return res;
						
					}else{
						StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT++;
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
		}
		return 0;
		
		
	}
	public static int checkMD5(byte[] hash, int lst_type){
		if(hash != null){
			if(hash.length == 16){
				ConnectionDescriptor cd = null;
				int res = 0;
				try{
					cd = getConnection((lst_type == CudaManager.MD5_QUARANTINE_LST ? NodeType.QUARANTINE : NodeType.NORMAL));
					//cd = device_connection_pool.poll();
					// fetch data
					if(cd != null){
						res = cd.node.checkMD5(hash, cd, lst_type);
						// release connection
						device_connection_pool.offer(cd);
						return res;
						
					}else{
						StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT++;
					}
					
				}catch(Exception e){
					e.printStackTrace();
				}
				
			}
			
		}
		return 0;
	
	}
	public static int checkLD(int lst_id, String data, NodeType ntype){
		if(data != null){
			if(data.getBytes().length >= FNConfigData.gpub_ld_min_length && data.getBytes().length <= FNConfigData.gpub_ld_max_length){
				ConnectionDescriptor cd = null;
				int res = 0;
				try{
					//cd = device_connection_pool.poll();
					cd = getConnection(ntype);
					// fetch data
					if(cd != null){
						res = cd.node.checkLD(lst_id, data, cd);
						// release connection
						device_connection_pool.offer(cd);
						return res;
						
					}else{
						StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT++;
					}
				}catch(Exception e){
					e.printStackTrace();
					
				}
				LoggingManager.warn(logger, "Cannot acquire free KERNEL connetion to GPU Node [" + ntype + "]!");
			}//else logger.warn("LD: messasge length [" + data.getBytes().length + "] exceeds max available length [" + ConfigData.rp_ld_max_length + "]!");
			
		}
		return 0;
	}

	public static int checkLD(int lst_id, String data){
		if(data != null){
			if(data.getBytes().length >= FNConfigData.gpub_ld_min_length && data.getBytes().length <= FNConfigData.gpub_ld_max_length){
				ConnectionDescriptor cd = null;
				int res = 0;
				try{
					//cd = device_connection_pool.poll();
					cd = getConnection(NodeType.NORMAL);
					// fetch data
					if(cd != null){
						res = cd.node.checkLD(lst_id, data, cd);
						// release connection
						device_connection_pool.offer(cd);
						return res;
						
					}else{
						StatsManager.GPUB_STATS.LD_NODE_BUSY_COUNT++;
					}
				}catch(Exception e){
					e.printStackTrace();
					
				}
				LoggingManager.warn(logger, "Cannot acquire free KERNEL connetion to GPU Node [NORMAL]!");
			}//else logger.warn("LD: messasge length [" + data.getBytes().length + "] exceeds max available length [" + ConfigData.rp_ld_max_length + "]!");
			
		}
		return 0;
	}
	public static void stop(){
		if(node_lst != null){
			for(int i = 0; i<node_lst.size(); i++) node_lst.get(i).closeConnections();
			node_lst.clear();
			stopping = true;
			update_queue.clear();
		}
	}
	public static NodeDescriptor getNodeDescriptor(String node_id){
		for(int i = 0; i<node_lst.size(); i++) if(node_lst.get(i).id.equalsIgnoreCase(node_id)) return node_lst.get(i);
		return null;
		
	}
	public static void init(){
		String tmp = null;
		String[] tmp_lst = null;
		NodeDescriptor nd  = null;
		stopping = false;
		int connections;// = Integer.parseInt(ConfigManager.get("rb.node.connections"));
		node_lst = new ArrayList<NodeDescriptor>();
		device_connection_pool = new ConcurrentLinkedQueue<ConnectionDescriptor>();
		update_queue = new ConcurrentLinkedQueue<GPUDataPacket>();
		LoggingManager.info(logger, "Starting GPU Balancer module...");
		tmp = ConfigManagerV2.get("gpub.nodes");
		LoggingManager.info(logger, "GPU nodes: [" + tmp + "]!");
		tmp_lst = tmp.split(",");
		for(int i = 0; i<tmp_lst.length; i++){
			LoggingManager.info(logger, "GPU [" + tmp_lst[i].trim() + "] host: [" + ConfigManagerV2.get("gpub.nodes." + tmp_lst[i].trim() + ".host") + "], port: [" + ConfigManagerV2.get("gpub.nodes." + tmp_lst[i].trim() + ".port") + "]!");
			connections = Integer.parseInt(ConfigManagerV2.get("gpub.nodes." + tmp_lst[i].trim() + ".gpus"));
			nd = new NodeDescriptor(tmp_lst[i].trim(), 
									ConfigManagerV2.get("gpub.nodes." + tmp_lst[i].trim() + ".host"), 
									Integer.parseInt(ConfigManagerV2.get("gpub.nodes." + tmp_lst[i].trim() + ".port")),
									connections);
			// add ld connections to pool
			for(int j = 0; j<nd.connections.size(); j++) if(nd.connections.get(j).type == ConnectionType.KERNEL) device_connection_pool.offer(nd.connections.get(j));
			// add to node list
			node_lst.add(nd);
			// total nu,ber of connections, -1 for updater connection
			total_connection_count += nd.connections.size() - 1;

			// node dedication configuration
			//gpub.nodes.refilter
			tmp_lst = ConfigManagerV2.get("gpub.nodes.refilter").split(",");
			for(int j = 0; j<tmp_lst.length; j++){
				if(node_lst.get(i).id.equalsIgnoreCase(tmp_lst[j].trim())){
					node_lst.get(i).addType(NodeType.REFILTER);
					LoggingManager.info(logger, "GPU Node [" + node_lst.get(i).id + "], set type: [REFILTER]!");
				}
			}
			//gpub.nodes.quarantine
			tmp_lst = ConfigManagerV2.get("gpub.nodes.quarantine").split(",");
			for(int j = 0; j<tmp_lst.length; j++){
				if(node_lst.get(i).id.equalsIgnoreCase(tmp_lst[j].trim())){
					node_lst.get(i).addType(NodeType.QUARANTINE);
					LoggingManager.info(logger, "GPU Node [" + node_lst.get(i).id + "], set type: [QUARANTINE]!");
				}
				
			}
			//gpub.nodes.normal
			tmp_lst = ConfigManagerV2.get("gpub.nodes.normal").split(",");
			for(int j = 0; j<tmp_lst.length; j++){
				if(node_lst.get(i).id.equalsIgnoreCase(tmp_lst[j].trim())){
					node_lst.get(i).addType(NodeType.NORMAL);
					LoggingManager.info(logger, "GPU Node [" + node_lst.get(i).id + "], set type: [NORMAL]!");
				}
				
			}
			
		}
		LoggingManager.info(logger, "Total GPU node connection count: [" + total_connection_count + "]!");

		updater_r = new Updater_r();
		updater_t = new Thread(updater_r, "GPU_UPDATER");
		updater_t.start();
		
	}

}
