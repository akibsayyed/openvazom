package net.m3ua.parameters.user_cause;

import java.util.HashMap;

public enum UserType {
	SCCP(0x03),
	TUP(0x04),
	ISUP(0x05),
	BROADBAND_ISUP(0x09),
	SATELLITE_ISUP(0x0A),
	AAL_TYPE2_SIGNALLING(0x0C),
	BICC(0x0D),
	GATEWAY_CONTROL_PROTOCOL(0x0E);
	
	private int id;
	private static final HashMap<Integer, UserType> lookup = new HashMap<Integer, UserType>();
	static{
		for(UserType td : UserType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static UserType get(int id){ return lookup.get(id); }
	private UserType(int _id){ id = _id; }	
}
