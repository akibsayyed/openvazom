package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class OfferedCamel4CSIs extends BIT_STRING{

	public static final int _o_csi = 0;
	public static final int _d_csi = 1;
	public static final int _vt_csi = 2;
	public static final int _t_csi = 3;
	public static final int _mt_sms_csi = 4;
	public static final int _mg_csi = 5;
	public static final int _psi_enhancements = 6;
	public OfferedCamel4CSIs(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
