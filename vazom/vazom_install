#!/bin/bash
# Vazom installer
bold=`tput bold`
normal=`tput sgr0`
green=`tput setf 2`
red=`tput setf 4`
yellow=`tput setf 6`

function install_sgn {
  echo "${normal}Starting [${yellow}SGN${normal}] installer..."
  #echo -n "${normal}Type the destination directory where VAZOM [${yellow}SGN${normal}] will be installed, followed by [ENTER]: "
  #read dest
  #echo "${normal}VAZOM [${yellow}SGN${normal}] will be installed in the following directory: ${green}$dest${normal}"
  #if [ ! -d "$dest" ]; then
  #  echo "${red}Directory [${yellow}$dest${red}] does not exist!"
  #  exit 2
  #fi
  dest=/vazom
  mkdir -p $dest
  echo -n "${normal}Are you sure? [${green}yes${normal}/${red}no${normal}]: "
  read ok
  if [ "$ok" == "yes" ]; then
    mkdir -p $dest/sgn
    echo "${normal}Copying VAZOM [${yellow}SGN${normal}] files..."
    cp -aL vazom/sgn/* $dest/sgn
    echo "${green}Finished copying VAZOM [${yellow}SGN${green}] files!"
    echo "${normal}Copying required system libraries..."
    cp hasp/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}HASP${green}] library!"
    cp spcap/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}SPCAP${green}] library!"
    cp ssctp/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}SSCTP${green}] library!"
    cp install_scripts/hasp/* /etc/local.d
    cp install_scripts/vazom/sgn/*.start /etc/local.d
    cp install_scripts/vazom/sgn/*.stop /etc/local.d
    cp install_scripts/vazom/sgn/vazom.init /etc/init.d/vazom.sgn
    cp install_scripts/vazom/sgn/java.profile /etc/profile.d/java.sh
    cp install_scripts/vazom/sgn/vazom.conf.d /etc/conf.d/vazom
    env-update
    source /etc/profile
    echo "${green}Finished copying startup scripts!"
    echo "${green}Finished copying requires system libraries!"
    echo "${green}VAZOM [${yellow}SGN${green}] successfully installed to \"${yellow}$dest${green}\"!"
  else
    exit 1
  fi

}

function install_fgn {
  echo "${normal}Starting [${yellow}FGN${normal}] installer..."
  #echo -n "${normal}Type the destination directory where VAZOM [${yellow}FGN${normal}] will be installed, followed by [ENTER]: "
  #read dest
  #echo "${normal}VAZOM [${yellow}FGN${normal}] will be installed in the following directory: ${green}$dest${normal}"
  #if [ ! -d "$dest" ]; then
  #  echo "${red}Directory [${yellow}$dest${red}] does not exist!"
  #  exit 2
  #fi
  dest=/vazom
  mkdir -p $dest
  echo -n "${normal}Are you sure? [${green}yes${normal}/${red}no${normal}]: "
  read ok
  if [ "$ok" == "yes" ]; then
    mkdir -p $dest/fgn
    echo "${normal}Copying VAZOM [${yellow}FGN${normal}] files..."
    cp -aL vazom/fgn/* $dest/fgn
    echo "${green}Finished copying VAZOM [${yellow}FGN${green}] files!"
    echo "${normal}Copying required system libraries..."
    cp hasp/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}HASP${green}] library!"
    cp ssctp/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}SSCTP${green}] library!"
    cp install_scripts/hasp/* /etc/local.d
    cp install_scripts/vazom/fgn/*.start /etc/local.d
    cp install_scripts/vazom/fgn/*.stop /etc/local.d
    cp install_scripts/vazom/fgn/vazom.init /etc/init.d/vazom.fgn
    cp install_scripts/vazom/fgn/java.profile /etc/profile.d/java.sh
    cp install_scripts/vazom/fgn/vazom.conf.d /etc/conf.d/vazom
    env-update
    source /etc/profile
    echo "${green}Finished copying startup scripts!"
    echo "${green}Finished copying requires system libraries!"
    echo "${green}VAZOM [${yellow}FGN${green}] successfully installed to \"${yellow}$dest${green}\"!"
  else
    exit 1
  fi


}


function install_pdn {
  echo "${normal}Starting [${yellow}PDN${normal}] installer..."
  #echo -n "Type the destination directory where VAZOM [PDN] will be installed, followed by [ENTER]: "
  #read dest
  #echo "VAZOM [PDN] will be installed in the following directory: $dest"
  dest=/vazom
  mkdir -p $dest
  echo -n "${normal}Are you sure? [${green}yes${normal}/${red}no${normal}]: "
  read ok
  if [ "$ok" == "yes" ]; then
    mkdir -p $dest/pdn
    echo "${normal}Copying VAZOM [${yellow}PDN${normal}] files..."
    cp -aL vazom/pdn/* $dest/pdn
    echo "${green}Finished copying VAZOM [${yellow}PDN${green}] files!"
    echo "${normal}Copying required system libraries..."
    cp hasp/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}HASP${green}] library!"
    cp ssctp/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}SSCTP${green}] library!"
    cp cudam/*.so /usr/lib64
    echo "${green}Finished copying [${yellow}CUDAM${green}] library!"
    cp install_scripts/cuda/* /etc/local.d
    cp install_scripts/hasp/* /etc/local.d
    cp install_scripts/vazom/pdn/*.start /etc/local.d
    cp install_scripts/vazom/pdn/*.stop /etc/local.d
    cp install_scripts/vazom/pdn/vazom.init /etc/init.d/vazom.pdn
    cp install_scripts/vazom/pdn/java.profile /etc/profile.d/java.sh
    cp install_scripts/vazom/pdn/vazom.conf.d /etc/conf.d/vazom
    env-update
    source /etc/profile
    echo "${green}Finished copying startup scripts!"
    echo "${green}Finished copying requires system libraries!"
    echo "${green}VAZOM [${yellow}PDN${green}] successfully installed to \"${yellow}$dest${green}\"!"
  else
    exit 1
  fi

}

# check for root user
if [ `id -u` -ne 0 ]; then
    echo "${red}Installer must be run as root" 1>&2
    echo "Aborting..." 1>&2
    exit 1
fi

echo "+=================================+"
echo "| Welcome to VAZOM node installer |"
echo "+=================================+"
echo ""
echo "${normal}Please select VAZOM node type to install:${normal}"
echo ""
echo "  1) ${yellow}SGN${normal} - Signalling node"
echo "  2) ${yellow}FGN${normal} - Filtering node"
echo "  3) ${yellow}PDN${normal} - Pattern detection node"
echo ""
echo -n "${normal}Type the node type number you wish to install, followed by [ENTER]: ${normal}"


read nt



case $nt in
  1)
    echo "${normal}[${yellow}SGN${normal}] node type selected, installing..."
    install_sgn
    ;;
  2)
    echo "${normal}[${yellow}FGN${normal}] node type selected, installing..."
    install_fgn
    ;;
  3)
    echo "${normal}[${yellow}PDN${normal}] node type selected, installing..."
    install_pdn
    ;;
  *)
    echo "${red}Unknown Node type!"
    ;;
esac   

