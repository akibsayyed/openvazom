package net.isup.parameters.cdi;

import java.util.HashMap;



public enum NotificationSubscriptionOptionsType {
	UNKNOWN(0x00),
	PRESENTATION_NOT_ALLOWED(0x01),
	PRESENTATION_ALLOWED_WITH_REDIRECTION_NUMBER(0x02),
	PRESENTATION_ALLOWED_WITHOUT_REDIRECTION_NUMBER(0x03);
	
	
	
	
	private int id;
	private static final HashMap<Integer, NotificationSubscriptionOptionsType> lookup = new HashMap<Integer, NotificationSubscriptionOptionsType>();
	static{
		for(NotificationSubscriptionOptionsType td : NotificationSubscriptionOptionsType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NotificationSubscriptionOptionsType get(int id){ return lookup.get(id); }
	private NotificationSubscriptionOptionsType(int _id){ id = _id; }
}
