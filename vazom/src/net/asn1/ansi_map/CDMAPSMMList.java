package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAPSMMList extends SET{

	public CDMAPSMMListSet new_child(){
		return new CDMAPSMMListSet();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMAPSMMListSet getChild(int index){
		return (CDMAPSMMListSet)of_children.get(index);
	}
	public CDMAPSMMList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		of_children = new ArrayList<ASNType>();
	}
}
