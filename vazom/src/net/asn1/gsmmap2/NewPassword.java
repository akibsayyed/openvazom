package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NewPassword extends NUMERICSTRING{

	public NewPassword(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 18;
	}
}
