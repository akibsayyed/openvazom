package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MatchType extends ENUMERATED{

	public static final int _inhibiting = 0;
	public static final int _enabling = 1;
	public MatchType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
