package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class NumberOfForwarding extends INTEGER{

	public NumberOfForwarding(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
