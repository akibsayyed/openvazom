package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PDP_ContextInfoList extends SEQUENCE{

	public PDP_ContextInfo new_child(){
		return new PDP_ContextInfo();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public PDP_ContextInfo getChild(int index){
		return (PDP_ContextInfo)of_children.get(index);
	}
	public PDP_ContextInfoList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
