package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransferToNumberRequest extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(2).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public RedirectionReason get_redirectionReason(){
		return (RedirectionReason)elements.get(3).data;
	}
	public RedirectionReason new_redirectionReason(){
		return new RedirectionReason();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(4).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public ACGEncountered get_acgencountered(){
		return (ACGEncountered)elements.get(5).data;
	}
	public ACGEncountered new_acgencountered(){
		return new ACGEncountered();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(6).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(7).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(8).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(9).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(10).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(11).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public GroupInformation get_groupInformation(){
		return (GroupInformation)elements.get(12).data;
	}
	public GroupInformation new_groupInformation(){
		return new GroupInformation();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(13).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(14).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(15).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public PilotBillingID get_pilotBillingID(){
		return (PilotBillingID)elements.get(16).data;
	}
	public PilotBillingID new_pilotBillingID(){
		return new PilotBillingID();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(17).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(18).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(19).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(20).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(21).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public MEID get_meid(){
		return (MEID)elements.get(22).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(19, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_redirectionReason(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(340, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_acgencountered(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(163, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_groupInformation(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(169, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotBillingID(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public TransferToNumberRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
