package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class IST_AlertTimerValue extends INTEGER{

	public IST_AlertTimerValue(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
