package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class UnavailabilityCause extends ENUMERATED{

	public static final int _bearerServiceNotProvisioned = 1;
	public static final int _teleserviceNotProvisioned = 2;
	public static final int _absentSubscriber = 3;
	public static final int _busySubscriber = 4;
	public static final int _callBarred = 5;
	public static final int _cug_Reject = 6;
	public UnavailabilityCause(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
