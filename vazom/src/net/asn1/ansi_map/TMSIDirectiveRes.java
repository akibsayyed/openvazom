package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TMSIDirectiveRes extends SET{

	public DenyAccess get_denyAccess(){
		return (DenyAccess)elements.get(0).data;
	}
	public DenyAccess new_denyAccess(){
		return new DenyAccess();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(2).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public ReasonList get_reasonList(){
		return (ReasonList)elements.get(3).data;
	}
	public ReasonList new_reasonList(){
		return new ReasonList();
	}

	public MEID get_meid(){
		return (MEID)elements.get(4).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_denyAccess(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(218, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reasonList(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public TMSIDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
