package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMATargetMAHOInformation extends SEQUENCE{

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(0).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public CDMAPilotStrength get_cdmaPilotStrength(){
		return (CDMAPilotStrength)elements.get(1).data;
	}
	public CDMAPilotStrength new_cdmaPilotStrength(){
		return new CDMAPilotStrength();
	}

	public CDMATargetOneWayDelay get_cdmaTargetOneWayDelay(){
		return (CDMATargetOneWayDelay)elements.get(2).data;
	}
	public CDMATargetOneWayDelay new_cdmaTargetOneWayDelay(){
		return new CDMATargetOneWayDelay();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(65, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaPilotStrength(); }});
		elements.add(new ElementDescriptor(61, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaTargetOneWayDelay(); }});
	}
	public CDMATargetMAHOInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
