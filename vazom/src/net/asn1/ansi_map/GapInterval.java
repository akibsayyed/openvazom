package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class GapInterval extends CHOICE{

	public SCFOverloadGapInterval get_sCFOverloadGapInterval(){
		return (SCFOverloadGapInterval)elements.get(0).data;
	}
	public SCFOverloadGapInterval new_sCFOverloadGapInterval(){
		return new SCFOverloadGapInterval();
	}

	public ServiceManagementSystemGapInterval get_serviceManagementSystemGapInterval(){
		return (ServiceManagementSystemGapInterval)elements.get(1).data;
	}
	public ServiceManagementSystemGapInterval new_serviceManagementSystemGapInterval(){
		return new ServiceManagementSystemGapInterval();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(343, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_sCFOverloadGapInterval(); }});
		elements.add(new ElementDescriptor(344, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceManagementSystemGapInterval(); }});
	}
	public GapInterval(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
