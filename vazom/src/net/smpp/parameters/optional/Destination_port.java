package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Destination_port extends OptionalParameterBase {

	public Destination_port(){
		type = OptionalParameterType.DESTINATION_PORT;
	}
	public int value;
	public void init(byte[] data) {
		value = ((data[0] << 8) & 0xff) + (data[1] & 0xff);

	}

}
