package net.security;

import java.util.Arrays;
import java.util.HashMap;
import java.util.concurrent.ConcurrentHashMap;

import net.logging.LoggingManager;

import org.apache.log4j.Logger;

//import Aladdin.Hasp;
//import Aladdin.HaspStatus;

public class SecurityManager {
	/*
	public static Logger logger=Logger.getLogger(SecurityManager.class);
	private static String vendor_code = 
		"/bxjvQ+st/1clltOYVgqKnIhJHOSIDbodaZE69k9ItgPLeK92Q83bDyuSFr7kZeiNaIntRBtUoQt1h5t" +
		"pnaTaQu1379rL3cL6c2Lkg8u6LAvnU6E5+V45fb4jVyCL9JaO4ZkxEduMlIPKBVT8EaAUe8pJP+Ltpl9" +
		"xQUFdGhglHnzBVmvKf5LntVbi5AnalPuhI/sK60IeowWJA+lVa3jRCIwCL0DMkk6MQ43wA12W+dKtuN2" +
		"NUXwUVCTmfpIqny/wYH8mRl8rv7B+NvIx6YsmTl27qma/UronLnOb9rc/zPJC7k3U/bOuSQx2rdtnhEa" +
		"hG311v+oOrorF86AgXXOI7IFacEYxTGx9JwE8P3fLV5Gl82OqIK6gdnu6JHjr1XuPFF2fMaPSWUP9JxJ" +
		"XXQej9lY0M/hKTvT45SaiQ/Wqa7GEE8DRezHrZp08dZt9phn5l3izLm9UB+zbmeUqlWGqseudy7hxlHg" +
		"aQWM634nmOJzuN05eoee1cT0r30XGesfZFDEemlWFUyXrf78ej7i/BTUwtmG33B02zq4I2KyEcMOd6g1" +
		"8wiETPRH71O5nNndK/goVtL0UZUFfqcoQiyjEDMHTxNQb4EBGw0ZNF4RcHgpdgcmHMszfiSugp6XFnOg" +
		"jDdlI2XuqACA8eDU3URsHqgjoOhgytz0KKq0Q3cqtg0RDU8E8b9qKVDqTPDsiweC54d2fDFsGE/WykzH" +
		"i3j66/G2LpkHhQIoT4GOkUXOahZ6mhu74rQN/NtA7euxFbdpw+uNiLbfma4cdpIXKy61wM0GLPhDZrUj" +
		"EFE/OWB+DxKsSrMijPUuLZSYXkEFK7EUBwaJEzBwM1aMED8J5aODjcZvlmncolNNUv9sOnGNsRVo9Fhr" +
		"ppmFFjfD96Xwstlra+eYYoXgMGwKRFNwo1lkRd8V+hsRJRPbrNhLPA5Vpng=";

	private static HashMap<Integer, Hasp> map;
	public static ConcurrentHashMap<Integer, FeatureDescriptor> feature_map;
	private static Monitor r_monitor;
	private static Thread t_monitor;
	private static boolean stopping;
	
	private static class Monitor implements Runnable{
		public void run() {
			while(!stopping){
				try{ Thread.sleep(60000); }catch(Exception e){}
				check_all_features();
			}
			System.exit(0);
			
		}
		
	}
	/*
	10. SRI
	11. SMPP Proxy
	12. SMPP Connection Manager
	13. M3UA Connection Manager (SIGTRAN)
	14. MTP3 Connection Manager (E1)
	15. SS7 PROXY
	16. STP (ROUTING CONNECTIONS)
	17. PDU CONVERTER (ROUTING)
	18. GPU BALANCER
	50. PDN
	51. SGN
	52. FGN
	*/
	/*
	public static final int FEATURE_SRI 			= 10;
	public static final int FEATURE_SMPP_PROXY 		= 11;
	public static final int FEATURE_SMPP_CONN 		= 12;
	public static final int FEATURE_M3UA_CONN 		= 13;
	public static final int FEATURE_MTP3_CONN 		= 14;
	public static final int FEATURE_SS7_PROXY 		= 15;
	public static final int FEATURE_STP 			= 16;
	public static final int FEATURE_PDU_CONVERT 	= 17;
	public static final int FEATURE_GPU_BALANCER 	= 18;
	public static final int FEATURE_PDN 			= 50;
	public static final int FEATURE_SGN 			= 51;
	public static final int FEATURE_FGN 			= 52;
	/*
	06 EC 66 50 7D 9C 9B A9  1D B2 5C 99 34 79 B2 B4   
    CE FA 2F D6 5C D1 DE 9A  06 19 6F F3 48 6B 70
    */
	/*
	public static final byte[] hash_test = new byte[]{
		0x06, (byte)0xEC, 0x66, 0x50, 0x7D, (byte)0x9C, (byte)0x9B, (byte)0xA9, 0x1D, (byte)0xB2, 0x5C, 
		(byte)0x99, 0x34, 0x79, (byte)0xB2, (byte)0xB4,   
		(byte)0xCE, (byte)0xFA, 0x2F, (byte)0xD6, 0x5C, (byte)0xD1, (byte)0xDE, 
		(byte)0x9A,  0x06, 0x19, 0x6F, (byte)0xF3, 0x48, 0x6B, 0x70
	};
	
	public static void check_all_features(){
		Hasp hasp = null;
		int status;
		boolean enc_dec_check;
		boolean login_check;
		boolean valid;
		FeatureDescriptor fd = null;
		for(Integer l : map.keySet()){
			hasp = map.get(l);
			hasp.getSessionInfo(Hasp.HASP_KEYINFO);
			status = hasp.getLastError();
			if(status != HaspStatus.HASP_STATUS_OK){
				fd = feature_map.get(l);
				if(fd != null){
					fd.active = false;
					fd.err_count++;
					if(fd.err_count > 3){
						if(fd.critical){
							LoggingManager.error(logger, "Invalid license for critical feature id = [" + l + "], trial number = [" + fd.err_count + "], shutting down!");
							System.exit(0);
						}
						
					}else{
						LoggingManager.error(logger, "Invalid license for feature id = [" + l + "], trial number = [" + fd.err_count + "]");
						
					}
				}
				feature_login(l);
			}else{
				hasp.logout();
				login_check = feature_login(l);
				enc_dec_check = enc_dec_check(l);
				valid = login_check && enc_dec_check;
				fd = feature_map.get(l);
				if(fd != null){
					fd.active = valid;
					if(!valid){
						fd.err_count++;
						if(fd.err_count > 3){
							if(fd.critical){
								LoggingManager.error(logger, "Invalid license for critical feature id = [" + l + "], trial number = [" + fd.err_count + "], shutting down!");
								System.exit(0);
							}
							
						}else{
							LoggingManager.error(logger, "Invalid license for feature id = [" + l + "], trial number = [" + fd.err_count + "]");

						}
						
					}else fd.err_count = 0;
					
				}
				
			}
		}
	}
	
	private static void init_feature_map(){
		feature_map.put(0, new FeatureDescriptor(0, feature_login(0)));
		feature_map.put(FEATURE_SMPP_PROXY,  new FeatureDescriptor(FEATURE_SMPP_PROXY, feature_login(FEATURE_SMPP_PROXY)));
		feature_map.put(FEATURE_SMPP_CONN, new FeatureDescriptor(FEATURE_SMPP_CONN, feature_login(FEATURE_SMPP_CONN)));
		feature_map.put(FEATURE_M3UA_CONN, new FeatureDescriptor(FEATURE_M3UA_CONN, feature_login(FEATURE_M3UA_CONN)));
		feature_map.put(FEATURE_MTP3_CONN, new FeatureDescriptor(FEATURE_MTP3_CONN, feature_login(FEATURE_MTP3_CONN)));
		feature_map.put(FEATURE_SS7_PROXY, new FeatureDescriptor(FEATURE_SS7_PROXY, feature_login(FEATURE_SS7_PROXY)));
		feature_map.put(FEATURE_STP, new FeatureDescriptor(FEATURE_STP, feature_login(FEATURE_STP)));
		feature_map.put(FEATURE_SRI, new FeatureDescriptor(FEATURE_SRI, feature_login(FEATURE_SRI)));
		feature_map.put(FEATURE_PDU_CONVERT, new FeatureDescriptor(FEATURE_PDU_CONVERT, feature_login(FEATURE_PDU_CONVERT)));
		feature_map.put(FEATURE_GPU_BALANCER, new FeatureDescriptor(FEATURE_GPU_BALANCER, feature_login(FEATURE_GPU_BALANCER)));
		feature_map.put(FEATURE_PDN, new FeatureDescriptor(FEATURE_PDN, feature_login(FEATURE_PDN)));
		feature_map.put(FEATURE_SGN, new FeatureDescriptor(FEATURE_SGN, feature_login(FEATURE_SGN)));
		feature_map.put(FEATURE_FGN, new FeatureDescriptor(FEATURE_FGN, feature_login(FEATURE_FGN)));
		
		for(Integer i : feature_map.keySet()){
			LoggingManager.info(logger, "Feature [" + i + "] active = [" + feature_map.get(i).active + "]");
		}
		
	} 
	
	private static boolean enc_dec_check(int fid){
		Hasp hasp = map.get(fid);
		if(hasp != null){
			try{
				byte[] data = new byte[31];
				byte[] data2 = Arrays.copyOfRange(hash_test, 0, hash_test.length);
				hasp.read(Hasp.HASP_FILEID_RO, 0, data);
				int status = hasp.getLastError();
				if (HaspStatus.HASP_STATUS_OK != status){
					return false;
				}else{
					hasp.decrypt(data);
					status = hasp.getLastError();
					if(status != HaspStatus.HASP_STATUS_OK) return false;
					
					hasp.decrypt(data2);
					status = hasp.getLastError();
					if(status != HaspStatus.HASP_STATUS_OK) return false;

					return Arrays.equals(data, data2);
				}
				
			}catch(Exception e){
				return false;
			}
			
			
		}
		return false;
	}
	
	public static boolean check_feature(int fid){
		FeatureDescriptor fd = feature_map.get(fid);
		if(fd != null){
			if(!fd.active){
				LoggingManager.error(logger, "License not valid for feature id = [" + fid + "]!");
				
			}
			return fd.active;
		}
		LoggingManager.error(logger, "License not valid for feature id = [" + fid + "]!");
		return false;
		/*
		Hasp hasp = map.get(fid);
		int status;
		if(hasp != null){
			hasp.getSessionInfo(Hasp.HASP_KEYINFO);
			status = hasp.getLastError();
			if(status == HaspStatus.HASP_STATUS_OK) return true;
		}
		//System.out.println("License not valid for feature id = [" + fid + "], hasp error = [" + status + "]!");
		//LoggingManager.error(logger, "License not valid for feature id = [" + fid + "], hasp error = [" + status + "]!");
		return false;
		*/
	/*
	}
	
	private static boolean feature_login(int fid){
		Hasp hasp = new Hasp(fid);
		hasp.login(vendor_code);
		int status = hasp.getLastError();
		map.put(fid, hasp);
		if(status == HaspStatus.HASP_STATUS_OK){
			return true;
		}
		//System.out.println("Error while logging to feature id = [" + fid + "], hasp error = [" + status + "]!");
		//LoggingManager.error(logger, "Error while logging to feature id = [" + fid + "], hasp error = [" + status + "]!");
		return false;
		
	}

	private static boolean feature_logout(int fid){
		Hasp hasp = map.get(fid);
		if(hasp != null){
			return hasp.logout();
			
		}
		return false;
		
	}

	
	public static void set_critical_feature(int fid){
		FeatureDescriptor fd = feature_map.get(fid);
		if(fd != null){
			fd.critical = true;
		}
		
	}
	public static void init(){
		LoggingManager.info(logger, "Starting Security Manager...");
		LoggingManager.info(logger, "Initializing Features...");
		map = new HashMap<Integer, Hasp>();
		feature_map = new ConcurrentHashMap<Integer, FeatureDescriptor>();
		init_feature_map();
		LoggingManager.info(logger, "Starting Security Monitor...");
		r_monitor = new Monitor();
		t_monitor = new Thread(r_monitor, "SECURITY_MONITOR");
		t_monitor.start();
		
		//logger.info("Starting Security Manager...");
		
	}
	*/
}
