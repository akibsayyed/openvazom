package net.ber;

public enum BerLengthType {
	DEFINITE_SHORT,
	DEFINITE_LONG,
	INDEFINITE;
}
