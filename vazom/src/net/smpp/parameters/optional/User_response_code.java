package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class User_response_code extends OptionalParameterBase {

	public User_response_code(){
		type = OptionalParameterType.USER_RESPONSE_CODE;
	}
	int value;
	public void init(byte[] data) {
		value = data[0] & 0xff;
	}

}
