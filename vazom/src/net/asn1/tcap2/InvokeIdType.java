package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class InvokeIdType extends INTEGER{

	public InvokeIdType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
