package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Ext_PDP_Type extends OCTET_STRING{

	public Ext_PDP_Type(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
