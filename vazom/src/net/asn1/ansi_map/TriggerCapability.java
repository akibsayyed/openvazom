package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TriggerCapability extends OCTET_STRING{

	public TriggerCapability(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
