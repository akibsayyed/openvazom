package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class InterSystemPage extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(2).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(3).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(4).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(5).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(6).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(7).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(8).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CDMABandClass get_cdmaBandClass(){
		return (CDMABandClass)elements.get(9).data;
	}
	public CDMABandClass new_cdmaBandClass(){
		return new CDMABandClass();
	}

	public CDMAMobileProtocolRevision get_cdmaMobileProtocolRevision(){
		return (CDMAMobileProtocolRevision)elements.get(10).data;
	}
	public CDMAMobileProtocolRevision new_cdmaMobileProtocolRevision(){
		return new CDMAMobileProtocolRevision();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(11).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(12).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public CDMASlotCycleIndex get_cdmaSlotCycleIndex(){
		return (CDMASlotCycleIndex)elements.get(13).data;
	}
	public CDMASlotCycleIndex new_cdmaSlotCycleIndex(){
		return new CDMASlotCycleIndex();
	}

	public CDMAStationClassMark get_cdmaStationClassMark(){
		return (CDMAStationClassMark)elements.get(14).data;
	}
	public CDMAStationClassMark new_cdmaStationClassMark(){
		return new CDMAStationClassMark();
	}

	public CDMAStationClassMark2 get_cdmaStationClassMark2(){
		return (CDMAStationClassMark2)elements.get(15).data;
	}
	public CDMAStationClassMark2 new_cdmaStationClassMark2(){
		return new CDMAStationClassMark2();
	}

	public ControlChannelMode get_controlChannelMode(){
		return (ControlChannelMode)elements.get(16).data;
	}
	public ControlChannelMode new_controlChannelMode(){
		return new ControlChannelMode();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(17).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DisplayText2 get_displayText2(){
		return (DisplayText2)elements.get(18).data;
	}
	public DisplayText2 new_displayText2(){
		return new DisplayText2();
	}

	public DMH_AccountCodeDigits get_dmh_AccountCodeDigits(){
		return (DMH_AccountCodeDigits)elements.get(19).data;
	}
	public DMH_AccountCodeDigits new_dmh_AccountCodeDigits(){
		return new DMH_AccountCodeDigits();
	}

	public DMH_AlternateBillingDigits get_dmh_AlternateBillingDigits(){
		return (DMH_AlternateBillingDigits)elements.get(20).data;
	}
	public DMH_AlternateBillingDigits new_dmh_AlternateBillingDigits(){
		return new DMH_AlternateBillingDigits();
	}

	public DMH_BillingDigits get_dmh_BillingDigits(){
		return (DMH_BillingDigits)elements.get(21).data;
	}
	public DMH_BillingDigits new_dmh_BillingDigits(){
		return new DMH_BillingDigits();
	}

	public ExtendedMSCID get_extendedMSCID(){
		return (ExtendedMSCID)elements.get(22).data;
	}
	public ExtendedMSCID new_extendedMSCID(){
		return new ExtendedMSCID();
	}

	public ExtendedSystemMyTypeCode get_extendedSystemMyTypeCode(){
		return (ExtendedSystemMyTypeCode)elements.get(23).data;
	}
	public ExtendedSystemMyTypeCode new_extendedSystemMyTypeCode(){
		return new ExtendedSystemMyTypeCode();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(24).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(25).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(26).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(27).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(28).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(29).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(30).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public MSIDUsage get_mSIDUsage(){
		return (MSIDUsage)elements.get(31).data;
	}
	public MSIDUsage new_mSIDUsage(){
		return new MSIDUsage();
	}

	public NetworkTMSI get_networkTMSI(){
		return (NetworkTMSI)elements.get(32).data;
	}
	public NetworkTMSI new_networkTMSI(){
		return new NetworkTMSI();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(33).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PageCount get_pageCount(){
		return (PageCount)elements.get(34).data;
	}
	public PageCount new_pageCount(){
		return new PageCount();
	}

	public PageIndicator get_pageIndicator(){
		return (PageIndicator)elements.get(35).data;
	}
	public PageIndicator new_pageIndicator(){
		return new PageIndicator();
	}

	public PageResponseTime get_pageResponseTime(){
		return (PageResponseTime)elements.get(36).data;
	}
	public PageResponseTime new_pageResponseTime(){
		return new PageResponseTime();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(37).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public PilotBillingID get_pilotBillingID(){
		return (PilotBillingID)elements.get(38).data;
	}
	public PilotBillingID new_pilotBillingID(){
		return new PilotBillingID();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(39).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(40).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(41).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingNumberString get_redirectingNumberString(){
		return (RedirectingNumberString)elements.get(42).data;
	}
	public RedirectingNumberString new_redirectingNumberString(){
		return new RedirectingNumberString();
	}

	public RedirectingPartyName get_redirectingPartyName(){
		return (RedirectingPartyName)elements.get(43).data;
	}
	public RedirectingPartyName new_redirectingPartyName(){
		return new RedirectingPartyName();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(44).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(45).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(46).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public TDMADataFeaturesIndicator get_tdmaDataFeaturesIndicator(){
		return (TDMADataFeaturesIndicator)elements.get(47).data;
	}
	public TDMADataFeaturesIndicator new_tdmaDataFeaturesIndicator(){
		return new TDMADataFeaturesIndicator();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(48).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(49).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public TerminationTreatment get_terminationTreatment(){
		return (TerminationTreatment)elements.get(50).data;
	}
	public TerminationTreatment new_terminationTreatment(){
		return new TerminationTreatment();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(51).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public TriggerAddressList get_triggerAddressList(){
		return (TriggerAddressList)elements.get(52).data;
	}
	public TriggerAddressList new_triggerAddressList(){
		return new TriggerAddressList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(170, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaBandClass(); }});
		elements.add(new ElementDescriptor(66, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileProtocolRevision(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(166, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaSlotCycleIndex(); }});
		elements.add(new ElementDescriptor(59, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark(); }});
		elements.add(new ElementDescriptor(177, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark2(); }});
		elements.add(new ElementDescriptor(199, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelMode(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(299, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText2(); }});
		elements.add(new ElementDescriptor(140, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AccountCodeDigits(); }});
		elements.add(new ElementDescriptor(141, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AlternateBillingDigits(); }});
		elements.add(new ElementDescriptor(142, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_BillingDigits(); }});
		elements.add(new ElementDescriptor(53, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedMSCID(); }});
		elements.add(new ElementDescriptor(54, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedSystemMyTypeCode(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(327, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSIDUsage(); }});
		elements.add(new ElementDescriptor(233, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_networkTMSI(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(300, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pageCount(); }});
		elements.add(new ElementDescriptor(71, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pageIndicator(); }});
		elements.add(new ElementDescriptor(301, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pageResponseTime(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(169, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotBillingID(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(101, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberString(); }});
		elements.add(new ElementDescriptor(245, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingPartyName(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(221, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaDataFeaturesIndicator(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(121, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTreatment(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(276, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerAddressList(); }});
	}
	public InterSystemPage(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
