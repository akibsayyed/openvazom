package net.isup.parameters.csi;

public class CSIOctet {
	public MaintenanceBlockingStateType maintenance_blocking_state;
	public CallProcessingStateType call_processing_state;
	public HardwareBlockingStateType hardware_blocking_state;
}
