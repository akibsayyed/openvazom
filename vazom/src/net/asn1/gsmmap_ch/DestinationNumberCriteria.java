package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DestinationNumberCriteria extends SEQUENCE{

	public MatchType get_matchType(){
		return (MatchType)elements.get(0).data;
	}
	public MatchType new_matchType(){
		return new MatchType();
	}

	public DestinationNumberList get_destinationNumberList(){
		return (DestinationNumberList)elements.get(1).data;
	}
	public DestinationNumberList new_destinationNumberList(){
		return new DestinationNumberList();
	}

	public DestinationNumberLengthList get_destinationNumberLengthList(){
		return (DestinationNumberLengthList)elements.get(2).data;
	}
	public DestinationNumberLengthList new_destinationNumberLengthList(){
		return new DestinationNumberLengthList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_matchType(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationNumberList(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationNumberLengthList(); }});
	}
	public DestinationNumberCriteria(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
