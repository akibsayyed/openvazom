package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventDMWrapper extends SEQUENCE{

	public EventDM get_eventDM(){
		return (EventDM)elements.get(0).data;
	}
	public EventDM new_eventDM(){
		return new EventDM();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_eventDM(); }});
	}
	public EventDMWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
