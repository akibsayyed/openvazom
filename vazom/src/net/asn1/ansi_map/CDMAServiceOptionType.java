package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMAServiceOptionType extends CDMAServiceOption{

	public CDMAServiceOptionType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 175;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
