package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DestinationNumberList extends SEQUENCE{

	public ISDN_AddressString new_child(){
		return new ISDN_AddressString();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ISDN_AddressString getChild(int index){
		return (ISDN_AddressString)of_children.get(index);
	}
	public DestinationNumberList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
