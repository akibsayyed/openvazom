package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class FacilitySelectedAndAvailable extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(1).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(2).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public TriggerType get_triggerType(){
		return (TriggerType)elements.get(3).data;
	}
	public TriggerType new_triggerType(){
		return new TriggerType();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(4).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public ACGEncountered get_acgencountered(){
		return (ACGEncountered)elements.get(5).data;
	}
	public ACGEncountered new_acgencountered(){
		return new ACGEncountered();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(6).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(7).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(8).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(9).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(10).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public GroupInformation get_groupInformation(){
		return (GroupInformation)elements.get(11).data;
	}
	public GroupInformation new_groupInformation(){
		return new GroupInformation();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(12).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(13).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(14).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(15).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(16).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(17).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PilotBillingID get_pilotBillingID(){
		return (PilotBillingID)elements.get(18).data;
	}
	public PilotBillingID new_pilotBillingID(){
		return new PilotBillingID();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(19).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(20).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(21).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingPartyName get_redirectingPartyName(){
		return (RedirectingPartyName)elements.get(22).data;
	}
	public RedirectingPartyName new_redirectingPartyName(){
		return new RedirectingPartyName();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(23).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(24).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(25).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public TerminationAccessType get_terminationAccessType(){
		return (TerminationAccessType)elements.get(26).data;
	}
	public TerminationAccessType new_terminationAccessType(){
		return new TerminationAccessType();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(279, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_triggerType(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(340, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_acgencountered(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(163, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_groupInformation(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(169, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotBillingID(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(245, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingPartyName(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(119, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationAccessType(); }});
	}
	public FacilitySelectedAndAvailable(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
