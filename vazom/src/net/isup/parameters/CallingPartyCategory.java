package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.clgpc.CallingPartyCategoryType;

public class CallingPartyCategory extends ParameterBase {
	public CallingPartyCategoryType category;
	public CallingPartyCategory(){
		type = ParameterType.CALLING_PARTY_CATEGORY;
	}

	public void init(byte[] data) {
		if(data != null){
			category = CallingPartyCategoryType.get(data[0] & 0xff);
		}
	}

}
