package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SecondEventsDescriptor extends SEQUENCE{

	public RequestID get_requestID(){
		return (RequestID)elements.get(0).data;
	}
	public RequestID new_requestID(){
		return new RequestID();
	}

	public SecondRequestedEventSequenceOf get_eventList(){
		return (SecondRequestedEventSequenceOf)elements.get(1).data;
	}
	public SecondRequestedEventSequenceOf new_eventList(){
		return new SecondRequestedEventSequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_requestID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventList(); }});
	}
	public SecondEventsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
