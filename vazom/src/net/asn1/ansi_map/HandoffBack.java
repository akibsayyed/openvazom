package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffBack extends SET{

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(0).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(1).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(2).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(3).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(4).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(5).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(6).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public HandoffReason get_handoffReason(){
		return (HandoffReason)elements.get(7).data;
	}
	public HandoffReason new_handoffReason(){
		return new HandoffReason();
	}

	public HandoffState get_handoffState(){
		return (HandoffState)elements.get(8).data;
	}
	public HandoffState new_handoffState(){
		return new HandoffState();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(9).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public TDMABurstIndicator get_tdmaBurstIndicator(){
		return (TDMABurstIndicator)elements.get(10).data;
	}
	public TDMABurstIndicator new_tdmaBurstIndicator(){
		return new TDMABurstIndicator();
	}

	public TDMACallMode get_tdmaCallMode(){
		return (TDMACallMode)elements.get(11).data;
	}
	public TDMACallMode new_tdmaCallMode(){
		return new TDMACallMode();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(12).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(13).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_handoffReason(); }});
		elements.add(new ElementDescriptor(164, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_handoffState(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(31, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBurstIndicator(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaCallMode(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
	}
	public HandoffBack(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
