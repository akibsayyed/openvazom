package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class T_BcsmCamelTDPDataList extends SEQUENCE{

	public T_BcsmCamelTDPData new_child(){
		return new T_BcsmCamelTDPData();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public T_BcsmCamelTDPData getChild(int index){
		return (T_BcsmCamelTDPData)of_children.get(index);
	}
	public T_BcsmCamelTDPDataList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
