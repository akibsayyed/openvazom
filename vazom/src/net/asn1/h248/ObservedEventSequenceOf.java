package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ObservedEventSequenceOf extends SEQUENCE{

	public ObservedEvent new_child(){
		return new ObservedEvent();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ObservedEvent getChild(int index){
		return (ObservedEvent)of_children.get(index);
	}
	public ObservedEventSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
