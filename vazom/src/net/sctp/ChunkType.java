package net.sctp;

import java.util.HashMap;


public enum ChunkType {
	DATA(0x00),
	INIT(0x01),
	INIT_ACK(0x02),
	SACK(0x03),
	HEARTBEAT(0x04),
	HEARTBEAT_ACK(0x05),
	ABORT(0x06),
	SHUTDOWN(0x07),
	SHUTDOWN_ACK(0x08),
	ERROR(0x09),
	COOKIE_ECHO(0x0a),
	COOKIE_ACK(0x0b),
	ECNE(0x0c),
	CWR(0x0d),
	SHUTDOWN_COMPLETE(0x0e);
	
	
	
	private int id;
	private static final HashMap<Integer, ChunkType> lookup = new HashMap<Integer, ChunkType>();
	static{
		for(ChunkType td : ChunkType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static ChunkType get(int id){ return lookup.get(id); }
	private ChunkType(int _id){ id = _id; }		
}
