package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PropertyParmExtraInfoChoiceWrapper extends SEQUENCE{

	public PropertyParmExtraInfoChoice get_propertyParmExtraInfoChoice(){
		return (PropertyParmExtraInfoChoice)elements.get(0).data;
	}
	public PropertyParmExtraInfoChoice new_propertyParmExtraInfoChoice(){
		return new PropertyParmExtraInfoChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_propertyParmExtraInfoChoice(); }});
	}
	public PropertyParmExtraInfoChoiceWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
