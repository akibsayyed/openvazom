package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SCreasonValue extends SEQUENCE{

	public SCreasonValueOctetStr new_child(){
		return new SCreasonValueOctetStr();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SCreasonValueOctetStr getChild(int index){
		return (SCreasonValueOctetStr)of_children.get(index);
	}
	public SCreasonValue(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
