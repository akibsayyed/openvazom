package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DomainName extends SEQUENCE{

	public IA5STRING get_name(){
		return (IA5STRING)elements.get(0).data;
	}
	public IA5STRING new_name(){
		return new IA5STRING();
	}

	public INTEGER get_portNumber(){
		return (INTEGER)elements.get(1).data;
	}
	public INTEGER new_portNumber(){
		return new INTEGER();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_name(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_portNumber(); }});
	}
	public DomainName(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
