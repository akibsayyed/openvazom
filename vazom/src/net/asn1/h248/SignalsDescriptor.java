package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SignalsDescriptor extends SEQUENCE{

	public SignalRequest new_child(){
		return new SignalRequest();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SignalRequest getChild(int index){
		return (SignalRequest)of_children.get(index);
	}
	public SignalsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
