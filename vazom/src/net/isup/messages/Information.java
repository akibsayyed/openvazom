package net.isup.messages;

import java.util.Arrays;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.InformationIndicators;

public class Information extends MessageBase {
	public InformationIndicators information_indicators;
	public Information(){
		super();
		type = MessageType.INFORMATION;
		pointers = new Pointer[1];
		
	}

	public void init(byte[] data) {
		if(data != null){
			information_indicators = new InformationIndicators();
			information_indicators.init(Arrays.copyOfRange(data, byte_pos, byte_pos + 2));
			byte_pos += 2;
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			// optional
			processOptional(data, 0);
			
		}
	}

}
