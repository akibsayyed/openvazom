package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudStreamParms extends SEQUENCE{

	public IndAudLocalControlDescriptor get_localControlDescriptor(){
		return (IndAudLocalControlDescriptor)elements.get(0).data;
	}
	public IndAudLocalControlDescriptor new_localControlDescriptor(){
		return new IndAudLocalControlDescriptor();
	}

	public IndAudLocalRemoteDescriptor get_localDescriptor(){
		return (IndAudLocalRemoteDescriptor)elements.get(1).data;
	}
	public IndAudLocalRemoteDescriptor new_localDescriptor(){
		return new IndAudLocalRemoteDescriptor();
	}

	public IndAudLocalRemoteDescriptor get_remoteDescriptor(){
		return (IndAudLocalRemoteDescriptor)elements.get(2).data;
	}
	public IndAudLocalRemoteDescriptor new_remoteDescriptor(){
		return new IndAudLocalRemoteDescriptor();
	}

	public IndAudStatisticsDescriptor get_statisticsDescriptor(){
		return (IndAudStatisticsDescriptor)elements.get(3).data;
	}
	public IndAudStatisticsDescriptor new_statisticsDescriptor(){
		return new IndAudStatisticsDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_localControlDescriptor(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_localDescriptor(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_remoteDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_statisticsDescriptor(); }});
	}
	public IndAudStreamParms(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
