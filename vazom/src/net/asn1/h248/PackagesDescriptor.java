package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PackagesDescriptor extends SEQUENCE{

	public PackagesItem new_child(){
		return new PackagesItem();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public PackagesItem getChild(int index){
		return (PackagesItem)of_children.get(index);
	}
	public PackagesDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
