package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TerminationIDList extends SEQUENCE{

	public TerminationID new_child(){
		return new TerminationID();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public TerminationID getChild(int index){
		return (TerminationID)of_children.get(index);
	}
	public TerminationIDList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
