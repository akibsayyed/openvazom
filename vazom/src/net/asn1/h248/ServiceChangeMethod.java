package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceChangeMethod extends ENUMERATED{

	public static final int _failover = 0;
	public static final int _forced = 1;
	public static final int _graceful = 2;
	public static final int _restart = 3;
	public static final int _disconnected = 4;
	public static final int _handOff = 5;
	public ServiceChangeMethod(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
