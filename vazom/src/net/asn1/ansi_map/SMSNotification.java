package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSNotification extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(2).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public SMS_AccessDeniedReason get_sms_AccessDeniedReason(){
		return (SMS_AccessDeniedReason)elements.get(3).data;
	}
	public SMS_AccessDeniedReason new_sms_AccessDeniedReason(){
		return new SMS_AccessDeniedReason();
	}

	public SMS_Address get_sms_Address(){
		return (SMS_Address)elements.get(4).data;
	}
	public SMS_Address new_sms_Address(){
		return new SMS_Address();
	}

	public SMS_TeleserviceIdentifier get_sms_TeleserviceIdentifier(){
		return (SMS_TeleserviceIdentifier)elements.get(5).data;
	}
	public SMS_TeleserviceIdentifier new_sms_TeleserviceIdentifier(){
		return new SMS_TeleserviceIdentifier();
	}

	public MEID get_meid(){
		return (MEID)elements.get(6).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(152, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_AccessDeniedReason(); }});
		elements.add(new ElementDescriptor(104, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_Address(); }});
		elements.add(new ElementDescriptor(116, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_TeleserviceIdentifier(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public SMSNotification(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
