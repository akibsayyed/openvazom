package net.stats;

public class IPFragStats {
	public long IP_FRAG_ASSEMBLED;
	public long IP_FRAG_MISSING;
	public long IP_FRAG_MAX_FRAGMENTS;
	public long IP_FRAG_MIN_FRAGMENTS;
	
	
	public void setMaxFragCount(long c){
		if(c > IP_FRAG_MAX_FRAGMENTS) IP_FRAG_MAX_FRAGMENTS = c;
	}

	public void setMinFragCount(long c){
		if(IP_FRAG_MIN_FRAGMENTS == 0) IP_FRAG_MIN_FRAGMENTS = c;
		else{
			if(c < IP_FRAG_MIN_FRAGMENTS) IP_FRAG_MIN_FRAGMENTS = c;
		}
	}
	
	
}
