package net.smsfs;

public enum ModeOfOperation {
	INTRUSIVE,
	NON_INTRUSIVE;
}
