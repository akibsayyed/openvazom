package net.smsfs;


import net.config.FNConfigData;
import net.db.DBManager;
import net.db.DBRecordBase;
import net.db.DBRecordSMS;
import net.db.DBRecordSRI;
import net.fn.FNode;
import net.fn.SGDescriptor;
import net.fn.SGNManager;
import net.hlr.HLRManager;
import net.hplmnr.HPLMNRManager;
import net.hplmnr.HPLMNRPacket;
import net.sctp.SSCTPDescriptor;
import net.stats.StatsManager;
import net.vstp.HeaderDescriptor;
import net.vstp.MessageDescriptor;
import net.vstp.VSTPDataItemType;

import org.apache.log4j.Logger;

public class SmsfsWorker {
	private static Logger logger=Logger.getLogger(SmsfsWorker.class);
	private Fw_r fw_r;
	private Thread fw_t;
	private boolean stopping;
	private String worker_name;
	
	private class Fw_r implements Runnable{
		SmsfsPacket fp = null;
		FilterResult fres = null;
		public void run() {
			logger.info(worker_name + " STARTING!");
			while(!stopping){
				fp = SmsfsManager.queue.poll();
				if(fp != null){
					fres = Smsfs.executeFilter(fp.filterDescriptor, fp);
					switch(fp.packetMode){
						case INTRUSIVE: 
							process_INTRUSIVE(fp, fres);
							break;
						case NON_INTRUSIVE: 
							process_NON_INTRUSIVE(fp, fres); 
							break;
						
					}
					// remove HLR requests if present
					if(fp.hlr_request_executed){
						if(fp.extra_data.size() > 0){
							HLRManager.removeMutex("HLRREQ." + ((MessageDescriptor)fp.extra_data.get(0)).header.msg_id);
							
						}
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
				
			}
			logger.info(worker_name + " ENDING!");
		}
	}
	public void process_INTRUSIVE(SmsfsPacket sp, FilterResult fres){
		DBRecordSMS dbr_sms = null;
		DBRecordSRI dbr_sri = null;
		boolean filter_result = FilterResult.evaluate(fres, FNConfigData.max_total_score);
		
		MessageDescriptor md = (MessageDescriptor)sp.extra_data.get(0);
		// prepare VSTP result
		MessageDescriptor vstp_res = SGNManager.prepareFilter_result(fres, md.header);
		
		// send back to SG node
		// get originating SG node
		SGDescriptor dest_sgn = SGNManager.getNode(md.header.source_id);
		// if node found
		if(dest_sgn != null){
			// send back to originating SG node
			dest_sgn.private_out_queue.offer(vstp_res);
		// else sent to SG node pool
		}else{
			// send to out queue
			FNode.out_offer(vstp_res);
			
		}
		
		// send to out queue
		//FNode.out_offer(vstp_res);

		// send to DB
		switch(sp.filterType){
			// HLR
			case HLR: 
				if(FNConfigData.dr_intrusive_hlr_status){
					dbr_sri = DBRecordSRI.VSTP_to_DBR(md);
					dbr_sri.filter_total_score = (fres == null ? 0 : fres.total_score);
					dbr_sri.filter_action_id = (filter_result ? 1 : 2);
					if(!fres.no_dr) DBManager.offer(dbr_sri);
				}
				break;
			// SMS
			case MO:
			case MT: 
			case SMPP_MO:
			case SMPP_MT:
				if(FNConfigData.dr_intrusive_sms_status){
					dbr_sms = DBRecordSMS.VSTP_to_DBR(md); 
					dbr_sms.filter_total_score = (fres == null ? 0 : fres.total_score);
					dbr_sms.filter_action_id = (filter_result ? 1 : 2);
					dbr_sms.filter_exit_point = (fres == null ? "" : fres.filter_exit_point);
					dbr_sms.quarantine = (fres == null ? false : fres.quarantine);
					if(fres != null){
						if(!fres.no_dr) DBManager.offer(dbr_sms);
					}else DBManager.offer(dbr_sms);
				}
				break;
		}
		// stats
		if(filter_result){
			StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
		}else{
			StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED++;
		}
		
		/*
		if(fres != null){
			// if UNCONDITIONAL, total_socre is bypassed
			// ACCEPTED
			if(fres.result){
				if(fres.isUnconditional) StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
				else{
					// check total_score
					// ACCEPTED
					if(fres.total_score <= FNConfigData.max_total_score){
						StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
					// REJECTED
					}else{
						StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED++;
					}
				}
			}else StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED++;
		}else{
			StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
			
		}
		*/
		/*
		
		if(fres != null){
			// Stats
			if(fres.result) StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
			else StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED++;
			
			
			
			// ACCEPTED
			if(fres.result){
				// if UNCONDITIONAL, total_socre is bypassed
				// ACCEPTED
				if(fres.isUnconditional){
					StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
					logger.info("Filter status: ACCEPTED, UNCONDITIONAL!");
					SRIManager.outOffer(sd);
				}else{
					// check total_score
					// ACCEPTED
					if(fres.total_score <= FNConfigData.max_total_score){
						StatsManager.SMSFS_STATS.INTRUSIVE_ACCEPTED++;
						logger.info("Filter status: ACCEPTED!");
						SRIManager.outOffer(sd);
					// REJECTED
					}else{
						StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED++;
						logger.info("Filter status: REJECTED!");
					}
				}
				
			// REJECTED
			}else{
				StatsManager.SMSFS_STATS.INTRUSIVE_REJECTED++;
				logger.info("Filter status: REJECTED!");
			}			
		}else{
			logger.info("Filter bypassed for packet type: [" + sri_packet.replyType + "]");
			SRIManager.outOffer(sd);
			// move DB part here from SRIDecoder to avoid double filtering
		}

		*/
		
	}
	public void process_NON_INTRUSIVE(SmsfsPacket sp, FilterResult fres){
		DBRecordBase dbr = (DBRecordBase)sp.extra_data.get(0);
		DBRecordSMS dbr_sms = null;
		DBRecordSRI dbr_sri = null;
		boolean filter_result = FilterResult.evaluate(fres, FNConfigData.max_total_score);
		
		
		if(filter_result) StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED++;
		else StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED++;
		
		switch(dbr.recordType){
			case SMS: 
				dbr_sms = (DBRecordSMS)dbr; 
				dbr_sms.filter_total_score = (fres == null ? 0 : fres.total_score);
				dbr_sms.filter_action_id = (filter_result ? 1 : 2);
				dbr_sms.filter_exit_point = (fres == null ? "" : fres.filter_exit_point);
				dbr_sms.quarantine = (fres == null ? false : fres.quarantine);
				break;
			case SRI: 
				dbr_sri = (DBRecordSRI)dbr; 
				dbr_sri.filter_total_score = (fres == null ? 0 : fres.total_score);
				dbr_sri.filter_action_id = (filter_result ? 1 : 2);
				break;
		}

		
		/*
		if(fres != null){
			// stats
			if(fres != null){
				// if UNCONDITIONAL, total_socre is bypassed
				// ACCEPTED
				if(fres.result){
					if(fres.isUnconditional) StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED++;
					else{
						// check total_score
						// ACCEPTED
						if(fres.total_score <= FNConfigData.max_total_score){
							StatsManager.SMSFS_STATS.NON_INTRUSIVE_ACCEPTED++;
						// REJECTED
						}else{
							StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED++;
						}
					}
				}else StatsManager.SMSFS_STATS.NON_INTRUSIVE_REJECTED++;
			}

			switch(dbr.recordType){
				case SMS: 
					dbr_sms = (DBRecordSMS)dbr; 
					dbr_sms.filter_total_score = fres.total_score;
					dbr_sms.filter_action_id = (fres.result && fres.total_score <= FNConfigData.max_total_score ? 1 : (fres.result && fres.isUnconditional ? 1 : 2));
					break;
				case SRI: 
					dbr_sri = (DBRecordSRI)dbr; 
					dbr_sri.filter_total_score = fres.total_score;
					dbr_sri.filter_action_id = (fres.result && fres.total_score <= FNConfigData.max_total_score ? 1 : (fres.result && fres.isUnconditional ? 1 : 2));
					break;
			}
		}
		*/
		// DB
		if(fres != null){
			if(!fres.no_dr) DBManager.offer(dbr);
		}else DBManager.offer(dbr);
	}

	public void stop(){
		stopping = true;
		
	}
	public SmsfsWorker(int id){
		fw_r = new Fw_r();
		worker_name = "SMSFS_WORKER_" + id;
		fw_t = new Thread(fw_r, worker_name);
		fw_t.start();
		
	}

}
