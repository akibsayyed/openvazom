package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ContextId extends INTEGER{

	public ContextId(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
