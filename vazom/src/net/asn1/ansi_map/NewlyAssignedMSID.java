package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NewlyAssignedMSID extends CHOICE{

	public NewlyAssignedMIN get_newlyAssignedMIN(){
		return (NewlyAssignedMIN)elements.get(0).data;
	}
	public NewlyAssignedMIN new_newlyAssignedMIN(){
		return new NewlyAssignedMIN();
	}

	public NewlyAssignedIMSI get_newlyAssignedIMSI(){
		return (NewlyAssignedIMSI)elements.get(1).data;
	}
	public NewlyAssignedIMSI new_newlyAssignedIMSI(){
		return new NewlyAssignedIMSI();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(187, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_newlyAssignedMIN(); }});
		elements.add(new ElementDescriptor(287, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_newlyAssignedIMSI(); }});
	}
	public NewlyAssignedMSID(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
