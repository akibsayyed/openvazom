package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StatusRequest extends SET{

	public MSID get_msid(){
		return (MSID)elements.get(0).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public Record_Type get_record_Type(){
		return (Record_Type)elements.get(1).data;
	}
	public Record_Type new_record_Type(){
		return new Record_Type();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(392, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_record_Type(); }});
	}
	public StatusRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
