package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMABandClassInformation extends SEQUENCE{

	public CDMABandClass get_cdmaBandClass(){
		return (CDMABandClass)elements.get(0).data;
	}
	public CDMABandClass new_cdmaBandClass(){
		return new CDMABandClass();
	}

	public CDMAMobileProtocolRevision get_cdmaMobileProtocolRevision(){
		return (CDMAMobileProtocolRevision)elements.get(1).data;
	}
	public CDMAMobileProtocolRevision new_cdmaMobileProtocolRevision(){
		return new CDMAMobileProtocolRevision();
	}

	public CDMAStationClassMark2 get_cdmaStationClassMark2(){
		return (CDMAStationClassMark2)elements.get(2).data;
	}
	public CDMAStationClassMark2 new_cdmaStationClassMark2(){
		return new CDMAStationClassMark2();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(170, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaBandClass(); }});
		elements.add(new ElementDescriptor(66, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileProtocolRevision(); }});
		elements.add(new ElementDescriptor(177, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark2(); }});
	}
	public CDMABandClassInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
