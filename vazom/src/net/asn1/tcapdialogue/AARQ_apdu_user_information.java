package net.asn1.tcapdialogue;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AARQ_apdu_user_information extends SEQUENCE{

	public User_information get_user_information(){
		return (User_information)elements.get(0).data;
	}
	public User_information new_user_information(){
		return new User_information();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_user_information(); }});
	}
	public AARQ_apdu_user_information(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 30;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
