package net.mtp2;

import java.util.Arrays;

import net.mtp2.messages.MSU;
import net.mtp2.messages.MessageBase;

public class MTP2Packet {
	public int backwardSequenceNumber;
	public boolean backwardIndicator;
	public int forwardSequenceNumber;
	public boolean forwardIndicator;
	public int lengthIndicator;
	public MessageBase message;
	
	
	public void init(byte[] data){
		backwardSequenceNumber = data[0] & 0x7f;
		backwardIndicator = (data[0] & 0x80) == 0x80;
		forwardSequenceNumber = data[1] & 0x7f;
		forwardIndicator = (data[1] & 0x80) == 0x80;
		lengthIndicator = data[2] & 0x3f;
		// MSU message
		if(lengthIndicator > 2){
			message = new MSU();
			message.init(Arrays.copyOfRange(data, 3, data.length));
		}
	}

}
