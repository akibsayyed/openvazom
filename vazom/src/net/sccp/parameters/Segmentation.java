package net.sccp.parameters;

import java.util.Arrays;

import net.sccp.parameters.segmentation.ClassType;

public class Segmentation extends ParamBase {
	public boolean isFirstSegment;
	public ClassType classType;
	public int numOfRemainingSegments;
	public byte[] segmentationLocalRef;
	
	public Segmentation(){
		super();
		type = ParamType.SEGMENTATION;
	}
	
	public void init(byte[] data) {
		isFirstSegment = (data[0] & 0x80) == 0x80;
		if((data[0] & 0x40) == 0x40) classType = ClassType.CLASS1; else classType = ClassType.CLASS0;
		numOfRemainingSegments = data[0] & 0x0F;
		segmentationLocalRef = Arrays.copyOfRange(data, 1, data.length);
	}

}
