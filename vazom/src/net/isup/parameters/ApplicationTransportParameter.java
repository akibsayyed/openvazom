package net.isup.parameters;

import net.isup.ParameterType;

public class ApplicationTransportParameter extends ParameterBase {
	public ApplicationTransportParameter(){
		type = ParameterType.APPLICATION_TRANSPORT_PARAMETER;
	}
}
