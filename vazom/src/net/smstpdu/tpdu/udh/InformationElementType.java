package net.smstpdu.tpdu.udh;

import java.util.HashMap;

public enum InformationElementType {
	CONCATENATED_8bit(0x00),
	CONCATENATED_16bit(0x08),
	IEDL(0x03);
	
	
	private int id;
	private static final HashMap<Integer, InformationElementType> lookup = new HashMap<Integer, InformationElementType>();
	static{
		for(InformationElementType td : InformationElementType.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static InformationElementType get(int id){ return lookup.get(id); }
	private InformationElementType(int _id){ id = _id; }	
	
}
