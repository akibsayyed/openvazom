package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAChannelNumberList extends SEQUENCE{

	public CDMAChannelNumberListInner new_child(){
		return new CDMAChannelNumberListInner();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMAChannelNumberListInner getChild(int index){
		return (CDMAChannelNumberListInner)of_children.get(index);
	}
	public CDMAChannelNumberList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
