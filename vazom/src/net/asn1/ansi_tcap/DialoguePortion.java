package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DialoguePortion extends SEQUENCE{

	public ProtocolVersion get_version(){
		return (ProtocolVersion)elements.get(0).data;
	}
	public ProtocolVersion new_version(){
		return new ProtocolVersion();
	}

	public ApplicationContextChoice get_applicationContext(){
		return (ApplicationContextChoice)elements.get(1).data;
	}
	public ApplicationContextChoice new_applicationContext(){
		return new ApplicationContextChoice();
	}

	public SecurityContextChoice get_securityContext(){
		return (SecurityContextChoice)elements.get(2).data;
	}
	public SecurityContextChoice new_securityContext(){
		return new SecurityContextChoice();
	}

	public Confidentiality get_confidentiality(){
		return (Confidentiality)elements.get(3).data;
	}
	public Confidentiality new_confidentiality(){
		return new Confidentiality();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(26, ASNTagClass.PRIVATE, true, false){public void set(){ data = new_version(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_applicationContext(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_securityContext(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentiality(); }});
	}
	public DialoguePortion(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 25;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
