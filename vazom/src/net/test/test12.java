package net.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.Arrays;

import net.m3ua.M3UA;
import net.m3ua.M3UAMessageType;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DUNA;
import net.m3ua.parameters.AffetedPointCode;
import net.m3ua.parameters.RoutingContext;
import net.mtp3.MTP3;
import net.mtp3.MTP3Packet;
import net.sctp.SSCTPDescriptor;
import net.smpp.ErrorCodeType;
import net.smpp.SMPP_PDU;
import net.smpp.parameters.optional.OptionalParameterBase;
import net.smpp.pdu.Bind_receiver_resp;
import net.smpp.pdu.Bind_transmitter;
import net.smpp.pdu.Bind_transmitter_resp;
import net.smpp.pdu.Deliver_sm;
import net.smpp.pdu.Enquire_link;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_sm;
import net.smpp.pdu.Submit_sm_resp;
import net.spcap.ETHHeader;
import net.spcap.IPHeader;
import net.spcap.SPcap;
import net.spcap.TCPHeader;
import net.utils.Utils;

public class test12 {

	public static void main(String[] args) {
		
		M3UAPacket mp = M3UA.prepareNew(M3UAMessageType.DUNA);
		DUNA duna = (DUNA)mp.message;
		duna.affectedPointCode = new AffetedPointCode();
		duna.affectedPointCode.setAffetedPointCode(13);
		duna.routingContext = new RoutingContext();
		duna.routingContext.setRoutingContext(12);
		mp.encode();

		
		System.exit(0);
		File f = new File("/tmp/test.raw");
		byte[] data = new byte[(int)f.length()];
		int i = 0;
		try{
			FileInputStream fi = new FileInputStream(f);
			int a;
			while( (a = fi.read()) != -1){
				data[i++] = (byte)a;
			}
			ETHHeader eth = SPcap.getETHHeader(data);
			IPHeader iph = SPcap.getIPHeader(eth.payload);
			TCPHeader tcp = SPcap.getTCPHeader(iph.payload);

			PDUBase[] pdu = SMPP_PDU.decode(tcp.payload);
			Deliver_sm dl = (Deliver_sm)pdu[0];
			for(int j = 0; j<dl.optional_params.size(); j++){
				OptionalParameterBase op = dl.optional_params.get(j);
				System.out.println(op.type);
				
			}
			
			
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}
