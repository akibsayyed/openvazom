package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Abort extends SEQUENCE{

	public DestTransactionID get_dtid(){
		return (DestTransactionID)elements.get(0).data;
	}
	public DestTransactionID new_dtid(){
		return new DestTransactionID();
	}

	public Reason get_reason(){
		return (Reason)elements.get(1).data;
	}
	public Reason new_reason(){
		return new Reason();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.APPLICATION, false, false){public void set(){ data = new_dtid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_reason(); }});
	}
	public Abort(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
