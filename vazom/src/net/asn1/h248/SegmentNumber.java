package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SegmentNumber extends INTEGER{

	public SegmentNumber(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
