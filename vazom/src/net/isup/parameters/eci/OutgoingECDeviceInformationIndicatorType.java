package net.isup.parameters.eci;

import java.util.HashMap;



public enum OutgoingECDeviceInformationIndicatorType {
	NO_INFORMATION(0x00),
	NOT_INCLUDED_NOT_AVAILABLE(0x01),
	INCLUDED(0x02),
	NOT_INCLUDED_AVAILABLE(0x03);
	
	
	
	private int id;
	private static final HashMap<Integer, OutgoingECDeviceInformationIndicatorType> lookup = new HashMap<Integer, OutgoingECDeviceInformationIndicatorType>();
	static{
		for(OutgoingECDeviceInformationIndicatorType td :OutgoingECDeviceInformationIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static OutgoingECDeviceInformationIndicatorType get(int id){ return lookup.get(id); }
	private OutgoingECDeviceInformationIndicatorType(int _id){ id = _id; }		
}
