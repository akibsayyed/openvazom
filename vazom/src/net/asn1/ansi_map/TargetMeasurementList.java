package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TargetMeasurementList extends SEQUENCE{

	public TargetMeasurementInformationType new_child(){
		return new TargetMeasurementInformationType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public TargetMeasurementInformationType getChild(int index){
		return (TargetMeasurementInformationType)of_children.get(index);
	}
	public TargetMeasurementList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
