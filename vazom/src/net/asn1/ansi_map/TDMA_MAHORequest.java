package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TDMA_MAHORequest extends OCTET_STRING{

	public TDMA_MAHORequest(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
