package net.smstpdu.tpdcs;

import net.smstpdu.tpdcs.data_encoding.MessageEncoding;
import net.smstpdu.tpdcs.general.Alphabet;
import net.smstpdu.tpdcs.general.MessageClass;

public class TPDCS_DataEncoding extends TPDCS {

	public MessageEncoding messageEncoding;
	public MessageClass messageClass;
	
	public TPDCS_DataEncoding(){
		super();
		group = TPDCSGroup.DATA_ENCODING_MESSAGE;
		
	}
	public byte encode(){
		byte tmp_res = (byte)group.getId();
		tmp_res += messageEncoding.getId() + messageClass.getId();
		return tmp_res;
	}
	public void init(byte data) {
		// bit 3 reserved
		// bit 2
		messageEncoding = MessageEncoding.get(data & 0x04);
		// bits 1, 0
		messageClass = MessageClass.get(data & 0x03);
		
		if(messageEncoding == MessageEncoding.DEFAULT) encoding = Alphabet.DEFAULT;
		else encoding = Alphabet._8BIT;
		
	}
}
