package net.cli;

public abstract class GroupDescriptor extends DescriptorBase {

	public GroupDescriptor(String _name, String _description, String _parent, String _params){
		super(_name, _description, _parent, _params);
		type = MethodType.CONSTRUCTED;
	}


}
