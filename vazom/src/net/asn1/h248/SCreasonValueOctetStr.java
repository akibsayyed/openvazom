package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SCreasonValueOctetStr extends OCTET_STRING{

	public SCreasonValueOctetStr(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
