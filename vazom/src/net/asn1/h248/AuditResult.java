package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuditResult extends SEQUENCE{

	public TerminationID get_terminationID(){
		return (TerminationID)elements.get(0).data;
	}
	public TerminationID new_terminationID(){
		return new TerminationID();
	}

	public TerminationAudit get_terminationAuditResult(){
		return (TerminationAudit)elements.get(1).data;
	}
	public TerminationAudit new_terminationAuditResult(){
		return new TerminationAudit();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationAuditResult(); }});
	}
	public AuditResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
