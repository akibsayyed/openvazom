package net.smpp_conn;

import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;
import net.config.SGNConfigData;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingManager;

import org.apache.log4j.Logger;

public class SMPPClientConnection {
	
	public Logger logger=Logger.getLogger(SMPPClientConnection.class);
	private Socket socket;
	private OutputStream out;
	private InputStream in;
	private ConcurrentLinkedQueue<byte[]> in_queue;
	public ConcurrentLinkedQueue<byte[]> out_queue;
	private ConcurrentLinkedQueue<SMPP_In_Packet> server_in_queue;
	public long IN_QUEUE_MAX;
	public long OUT_QUEUE_MAX;

	public String con_ip;
	public int con_port;
	public String host_ip;
	public int host_port;
	public String smpp_system_id;
	public String smpp_password;
	public boolean user_auth;
	public boolean proxy;
	
	private ConcurrentHashMap<String, SMPPClientConnection> conn_list;
	public boolean stopping;
	private Data_r datar_r;
	private Thread datar_t;
	private Data_w dataw_r;
	private Thread dataw_t;
	private Worker worker_r;
	private Thread worker_t;
	private SMPPClientConnection client_conn;
	public long last_sequence;


	public void out_offer(byte[] data) {
		if(data != null){
			if(out_queue.size() < SGNConfigData.sgn_global_max_queue_size) out_queue.offer(data);
			else{
				OUT_QUEUE_MAX++;
			}
		}
	}
	
	
	private class Worker implements Runnable{
		byte[] data = null;
		SMPPPacket smppp = null;
		SMPP_In_Packet in_packet = null;
		public void run() {
			while(!stopping){
				data = in_queue.poll();
				if(data != null){
					if(proxy){
						smppp = new SMPPPacket();
						smppp.client_ip = con_ip;
						smppp.client_port = con_port;
						smppp.host_ip = host_ip;
						smppp.host_port = host_port;
						smppp.system_id = smpp_system_id;
						smppp.password = smpp_password;
						//data = SMPPConnManager.process(data, smppp, client_conn);
						data = SMPPConnManager.process(data, smppp, client_conn);
					}else{
						in_packet = new SMPP_In_Packet();
						in_packet.client_conn = client_conn;
						in_packet.data = data;
						server_in_queue.offer(in_packet);
					}
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					
				}
			}			
		}
	}	

	
	private class Data_r implements Runnable{
		byte[] buffer = new byte[8192];
		int bc;
		
		public void run() {
			//logger.info("Starting...");
			while(!stopping){
				try{
					bc = in.read(buffer);
					if(bc > 0){
						if(in_queue.size() < SGNConfigData.sgn_global_max_queue_size) in_queue.offer(Arrays.copyOfRange(buffer, 0, bc));
						else{
							IN_QUEUE_MAX++;
							//logger.warn("SMPPConnection.in_queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
						}
					}else{
						stop();

					}
					
				}catch(Exception e){
					stop();
				}
			
			}			
			//logger.info("Ending...");
		}
	
	}	
	
	private class Data_w implements Runnable{
		byte[] data = null;
		public void run() {
			//logger.info("Starting...");
			while(!stopping){
				data = out_queue.poll();
				if(data != null){
					try{
						out.write(data);
						out.flush();
					}catch(Exception e){
						stop();
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
				
			}			
			//logger.info("Ending...");
		}
	
	}	
	
	
	public void stop(){
		//logger.info("SMPP Client connection lost [" + con_ip + ":" + con_port + "]!");
		stopping = true;
		conn_list.remove(con_ip + ":" + con_port);
		if(proxy){
			// remove routing connetion (end point connection)
			RoutingManager.remove(con_ip + ":" + con_port);
			// remove (end point - client) correlation, close both client and end point connections
			ProxyCorrelation pc = SMPPConnManager.correlation_get(con_ip + ":" + con_port);
			if(pc != null){
				// close end point connection
				pc.end_point_conn.detachWorkerMethod();
				pc.end_point_conn.stop();
				SMPPConnManager.correlation_remove(pc.client_conn);
			}
			
		}
		// close client connection
		try{
			socket.close();
		}catch(Exception e){}
		
	}
	
	
	public void start(){
		stopping = false;
		//logger.info("Creating SMPP Connection Data Receiver thread: [ " + socket.getInetAddress().toString() + ":" + socket.getPort() + " ]!");
		
		datar_r = new Data_r();
		datar_t = new Thread(datar_r, "SMPP_DATA_RECEIVER_" + socket.getInetAddress().toString() + ":" + socket.getPort());
		datar_t.start();

		//logger.info("Creating SMPP Connection Data Sender thread: [ " + socket.getInetAddress().toString() + ":" + socket.getPort() + " ]!");
		dataw_r = new Data_w();
		dataw_t = new Thread(dataw_r, "SMPP_DATA_SENDER_" + socket.getInetAddress().toString() + ":" + socket.getPort());
		dataw_t.start();
		
		//logger.info("Creating SMPP Connection Data Worker thread: [ " + socket.getInetAddress().toString() + ":" + socket.getPort() + " ]!");
		worker_r = new Worker();
		worker_t = new Thread(worker_r, "SMPP_DATA_WORKER_" + socket.getInetAddress().toString() + ":" + socket.getPort());
		worker_t.start();
		
		
		
	}		
	
	public SMPPClientConnection(Socket _socket, ConcurrentHashMap<String, SMPPClientConnection> _conn_list, ConcurrentLinkedQueue<SMPP_In_Packet> _server_in_queue){
		client_conn = this;
		socket = _socket;
		in_queue = new ConcurrentLinkedQueue<byte[]>();
		out_queue = new ConcurrentLinkedQueue<byte[]>();
		server_in_queue = _server_in_queue;
		con_ip = socket.getInetAddress().getHostAddress();
		host_port = socket.getLocalPort();
		host_ip = socket.getLocalAddress().getHostAddress();
		con_port = socket.getPort();
		conn_list = _conn_list;
		proxy = false;
		try{
			in = socket.getInputStream();
			out = socket.getOutputStream();
			start();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("SMPPConnection: Error while initializing IN/OUT streams!");
		}
	}
	

	
	public SMPPClientConnection(Socket _socket, ConcurrentHashMap<String, SMPPClientConnection> _conn_list, boolean _proxy){
		client_conn = this;
		socket = _socket;
		in_queue = new ConcurrentLinkedQueue<byte[]>();
		out_queue = new ConcurrentLinkedQueue<byte[]>();
		con_ip = socket.getInetAddress().getHostAddress();
		host_port = socket.getLocalPort();
		host_ip = socket.getLocalAddress().getHostAddress();
		con_port = socket.getPort();
		conn_list = _conn_list;
		proxy = _proxy;
		try{
			in = socket.getInputStream();
			out = socket.getOutputStream();
			if(proxy) SMPPConnManager.correlation_add(this);
			start();
		}catch(Exception e){
			e.printStackTrace();
			logger.error("SMPPConnection: Error while initializing IN/OUT streams!");
		}
	}
}
