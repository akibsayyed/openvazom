package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DenyAccess extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _unspecified =  1 ;
	public static final int _ssd_Update_failure =  2 ;
	public static final int _cOUNT_Update_failure =  3 ;
	public static final int _unique_Challenge_failure =  4 ;
	public static final int _aUTHR_mismatch =  5 ;
	public static final int _cOUNT_mismatch =  6 ;
	public static final int _process_collision =  7 ;
	public static final int _missing_authentication_parameters =  8 ;
	public static final int _terminalType_mismatch =  9 ;
	public static final int _mIN_IMSI_or_ESN_authorization_failure =  10 ;
	public DenyAccess(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
