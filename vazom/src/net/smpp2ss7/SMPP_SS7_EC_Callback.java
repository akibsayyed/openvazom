package net.smpp2ss7;

import net.vstp.MessageDescriptor;
import net.vstp.VSTPCallback;
import net.vstp.VSTPDataItemType;

public class SMPP_SS7_EC_Callback extends VSTPCallback {
	public String msg_id;
	private MessageDescriptor md_reply;
	
	public SMPP_SS7_EC_Callback(String _msg_id){
		super();
		msg_id = _msg_id;
		
	}
	public void set_error_code_reply(MessageDescriptor md){
		md_reply = md;
	}
	public void run() {
		//String tmp = null;
		int ss7_ec;
		int smpp_ec;
		try{
			if(md_reply != null){
				// error code conversion
				if(md_reply.values.get(VSTPDataItemType.SS7_ERROR_CODE.getId()) != null && md_reply.values.get(VSTPDataItemType.SMPP_ERROR_CODE.getId()) != null){
					ss7_ec = Integer.parseInt(md_reply.values.get(VSTPDataItemType.SS7_ERROR_CODE.getId()));
					smpp_ec = Integer.parseInt(md_reply.values.get(VSTPDataItemType.SMPP_ERROR_CODE.getId()));
					// add conversion
					SMPP2SS7Manager.add_conversion(ss7_ec, smpp_ec);
					
				}

				// end
				/*
				if(md_reply.values.get(VSTPDataItemType.MORE_DATA.getId()) != null){
					tmp = md_reply.values.get(VSTPDataItemType.MORE_DATA.getId());
					if(tmp.equals("0")){
						SMPP2SS7Manager.remove_error_correlation(md_reply.header.msg_id);
					}
				}
				*/
			}			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
}
