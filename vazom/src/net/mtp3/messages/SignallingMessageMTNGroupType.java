package net.mtp3.messages;

import java.util.HashMap;

public enum SignallingMessageMTNGroupType {
	TEST_MESSAGE(0x01);
	
	
	private int id;
	private static final HashMap<Integer, SignallingMessageMTNGroupType> lookup = new HashMap<Integer, SignallingMessageMTNGroupType>();
	static{
		for(SignallingMessageMTNGroupType td : SignallingMessageMTNGroupType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static SignallingMessageMTNGroupType get(int id){ return lookup.get(id); }
	private SignallingMessageMTNGroupType(int _id){ id = _id; }
}
