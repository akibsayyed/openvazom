package net.smpp.parameters;

import java.util.HashMap;

public enum MessageMode {
	DEFAULT_SMSC_MODE(0x00),
	DATAGRAM_MODE(0x01),
	FORWARD_MODE(0x02),
	STORE_FORWARD_MODE(0x03);
	
	private int id;
	private static final HashMap<Integer, MessageMode> lookup = new HashMap<Integer, MessageMode>();
	static{
		for(MessageMode td : MessageMode.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static MessageMode get(int id){ return lookup.get(id); }
	private MessageMode(int _id){ id = _id; }
}
