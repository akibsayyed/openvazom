package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DataUpdateResult extends SEQUENCE{

	public DataID get_dataID(){
		return (DataID)elements.get(0).data;
	}
	public DataID new_dataID(){
		return new DataID();
	}

	public DataResult get_dataResult(){
		return (DataResult)elements.get(1).data;
	}
	public DataResult new_dataResult(){
		return new DataResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(251, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataID(); }});
		elements.add(new ElementDescriptor(253, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataResult(); }});
	}
	public DataUpdateResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
