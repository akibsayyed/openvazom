package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NonStandardData extends SEQUENCE{

	public NonStandardIdentifierWrapper get_nonStandardIdentifier(){
		return (NonStandardIdentifierWrapper)elements.get(0).data;
	}
	public NonStandardIdentifierWrapper new_nonStandardIdentifier(){
		return new NonStandardIdentifierWrapper();
	}

	public OCTET_STRING get_data(){
		return (OCTET_STRING)elements.get(1).data;
	}
	public OCTET_STRING new_data(){
		return new OCTET_STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_nonStandardIdentifier(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_data(); }});
	}
	public NonStandardData(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
