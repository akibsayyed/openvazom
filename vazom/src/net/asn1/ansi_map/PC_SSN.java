package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PC_SSN extends OCTET_STRING{

	public PC_SSN(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
