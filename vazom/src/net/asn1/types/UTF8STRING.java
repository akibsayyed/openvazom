package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class UTF8STRING extends ASNType {
	public UTF8STRING(){
		super();
		universalTag = ASNTag.UTF8STRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
