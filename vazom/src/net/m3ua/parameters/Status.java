package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.status.StatusInfoType;
import net.m3ua.parameters.status.StatusType;

public class Status extends M3UAParameter {
	public StatusType statusType;
	public StatusInfoType statusInfo;
	
	public Status(){
		type = M3UAParameterType.STATUS;
	}

	public void setStatus(StatusType type, StatusInfoType info){
		value = new byte[4];
		statusType = type;
		statusInfo = info;
		value[0] = (byte)(type.getId() >> 8);
		value[1] = (byte)(type.getId());
		value[2] = (byte)(info.getId() >> 8);
		value[3] = (byte)(info.getId());
		
		
	}
	
	public void init(byte[] data) {
		value = data;
		statusType = StatusType.get((data[0] << 8) + (data[1] & 0xFF));
		statusInfo = StatusInfoType.get((data[2] << 8) + (data[3] & 0xFF));
	}



}
