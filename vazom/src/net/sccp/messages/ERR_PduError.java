package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Dlr;
import net.sccp.parameters.ErrorCause;

public class ERR_PduError extends MessageBase {
	public Dlr destinationLocalReference;
	public ErrorCause errorCause;
	
	public ERR_PduError(){
		super();
		type = MessageType.ERR_PROTOCOL_DATA_UNIT_ERROR;
		pointers = new Pointer[1];
		
	}
	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;

		// Fixed mandatory
		errorCause = new ErrorCause();
		errorCause.init(new byte[]{_data[byte_pos++]});

		// pointer, reserved for optional parameters in the future
		pointers[0] = new Pointer(_data[byte_pos] & 0xFF, (_data[byte_pos] & 0xFF) + (byte_pos++));
		
	}

}
