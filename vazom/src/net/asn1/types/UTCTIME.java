package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class UTCTIME extends ASNType {
	public UTCTIME(){
		super();
		universalTag = ASNTag.UTCTIME;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
