package net.isup.messages;

import net.isup.MessageType;
import net.isup.parameters.ContinuityIndicators;

public class Continuity extends MessageBase {
	public ContinuityIndicators continuity_indicators;
	public Continuity(){
		super();
		type = MessageType.CONTINUITY;
	}

	public void init(byte[] data) {
		if(data != null){
			continuity_indicators = new ContinuityIndicators();
			continuity_indicators.init(new byte[]{data[byte_pos]});
			
		}
	}

}
