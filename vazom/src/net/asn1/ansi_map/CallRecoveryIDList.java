package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CallRecoveryIDList extends SET{

	public CallRecoveryIDType new_child(){
		return new CallRecoveryIDType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CallRecoveryIDType getChild(int index){
		return (CallRecoveryIDType)of_children.get(index);
	}
	public CallRecoveryIDList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		of_children = new ArrayList<ASNType>();
	}
}
