package net.test;


import java.io.InputStream;
import java.io.OutputStream;
import java.net.Socket;
import java.util.Arrays;
import java.util.concurrent.ConcurrentLinkedQueue;


import net.smpp.ErrorCodeType;
import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.SMPP_PDU;
import net.smpp.TypeOfNumber;
import net.smpp.parameters.Data_codingType;
import net.smpp.parameters.GSMNetworkSpecific;
import net.smpp.parameters.MessageMode;
import net.smpp.parameters.MessageType;
import net.smpp.parameters.ServiceType;
import net.smpp.pdu.Bind_receiver;
import net.smpp.pdu.Bind_transceiver;
import net.smpp.pdu.Bind_transmitter;
import net.smpp.pdu.Enquire_link;
import net.smpp.pdu.PDUBase;
import net.smpp.pdu.Submit_sm;

public class smpptest2 {
	public static InputStream in;
	public static OutputStream out;
	public static ConcurrentLinkedQueue<byte[]> out_queue;
	public static Reader reader_r;
	public static Thread reader_t;
	public static Writer writer_r;
	public static Thread writer_t;

	
	public static class Reader implements Runnable{
		private InputStream in;
		private ConcurrentLinkedQueue<byte[]> out_queue;
		
		public Reader(InputStream _in, ConcurrentLinkedQueue<byte[]> _out_queue){
			in = _in;
			out_queue = _out_queue;
		}
		public void run() {
			byte[] data = new byte[8192];
			int br;
			System.out.println("Starting...");
			while(true){
				try{
					br = in.read(data);
					if(br > 0){
						System.out.println("----- PACKET RECEIVED START -----");
						PDUBase[] pdu = SMPP_PDU.decode(Arrays.copyOfRange(data, 0, br));
						for(int i = 0; i<pdu.length; i++){
							System.out.println(i + ":" + pdu[i].type);
							if(pdu[i].type == PDUType.BIND_RECEIVER_RESP){
								System.out.println("STATUS: " + pdu[i].command_status);
								if(pdu[i].command_status == ErrorCodeType.ESME_ROK){
									Enquire_link el = new Enquire_link();
									el.sequence_number = 2;
									out_queue.offer(el.encode());
									
								}
							}else if(pdu[i].type == PDUType.ENQUIRE_LINK_RESP){
								System.out.println("STATUS: " + pdu[i].command_status);
							}
							
						}
						System.out.println("----- PACKET RECEIVED END -----");
					}
				}catch(Exception e){
					e.printStackTrace();
				}

			}
		}
		
	}
	public static class Writer implements Runnable{
		private OutputStream out;
		private ConcurrentLinkedQueue<byte[]> out_queue;
		public Writer(OutputStream _out, ConcurrentLinkedQueue<byte[]> _out_queue){
			out = _out;
			out_queue = _out_queue;
		}
		public void run() {
			byte[] data;
			System.out.println("Starting...");
			while(true){
				data = out_queue.poll();
				if(data != null){
					try{
						out.write(data);
						out.flush();
						
					}catch(Exception e){
						e.printStackTrace();
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
				}
			}
			
		}
		
	}

	public static void main(String[] args) {
		try{

			out_queue = new ConcurrentLinkedQueue<byte[]>();
			Socket socket = new Socket("10.4.6.7", 5566);
			in = socket.getInputStream();
			out = socket.getOutputStream();

			reader_r = new Reader(in, out_queue);
			writer_r = new Writer(out, out_queue);
			reader_t = new Thread(reader_r, "READER");
			reader_t.start();
			writer_t = new Thread(writer_r, "WRITER");
			writer_t.start();
			
			Bind_receiver bt = new Bind_receiver();
			bt.system_id = "spam2";
			bt.password = "spam2";
			bt.system_type = "";
			bt.interface_version = 0x34;
			bt.addr_ton = TypeOfNumber.UNKNOWN;
			bt.addr_npi = NumberingPlan.UNKNOWN;
			bt.address_range = "";
			bt.sequence_number = 1;
			out_queue.offer(bt.encode());
		}catch(Exception e){
			e.printStackTrace();
		}
		
	}

}
