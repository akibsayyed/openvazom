package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AnalogRedirectRecord extends SEQUENCE{

	public AnalogRedirectInfo get_analogRedirectInfo(){
		return (AnalogRedirectInfo)elements.get(0).data;
	}
	public AnalogRedirectInfo new_analogRedirectInfo(){
		return new AnalogRedirectInfo();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(1).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(224, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_analogRedirectInfo(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
	}
	public AnalogRedirectRecord(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
