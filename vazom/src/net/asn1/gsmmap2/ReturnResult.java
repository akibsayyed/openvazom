package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReturnResult extends SEQUENCE{

	public InvokeIdType get_invokeID(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_invokeID(){
		return new InvokeIdType();
	}

	public ReturnResultResult get_resultretres(){
		return (ReturnResultResult)elements.get(1).data;
	}
	public ReturnResultResult new_resultretres(){
		return new ReturnResultResult();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeID(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_resultretres(); }});
	}
	public ReturnResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
