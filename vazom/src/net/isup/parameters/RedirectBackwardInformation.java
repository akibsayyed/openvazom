package net.isup.parameters;

import net.isup.ParameterType;

public class RedirectBackwardInformation extends ParameterBase {
	public RedirectBackwardInformation(){
		type = ParameterType.REDIRECT_BACKWARD_INFORMATION;
	}
}
