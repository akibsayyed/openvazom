package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAuditParameterSequenceOf extends SEQUENCE{

	public IndAuditParameter new_child(){
		return new IndAuditParameter();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public IndAuditParameter getChild(int index){
		return (IndAuditParameter)of_children.get(index);
	}
	public IndAuditParameterSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
