package net.smpp_conn;

import net.vstp.MessageDescriptor;
import net.vstp.VSTPCallback;
import net.vstp.VSTPDataItemType;

public class SMPP_USER_SYNC_Callback extends VSTPCallback {
	public String msg_id;
	private MessageDescriptor md_reply;

	public SMPP_USER_SYNC_Callback(String _msg_id){
		super();
		msg_id = _msg_id;
		
	}

	public void set_smpp_user_sync_reply(MessageDescriptor md){
		md_reply = md;
	}
	
	
	public void run() {
		try{
			if(md_reply != null){
				if(	md_reply.values.get(VSTPDataItemType.SMPP_SYSTEM_ID.getId()) != null && 
					md_reply.values.get(VSTPDataItemType.SMPP_PASSWORD.getId()) != null){
					
					SMPPConnManager.add_smpp_user(	md_reply.values.get(VSTPDataItemType.SMPP_SYSTEM_ID.getId()), 
													md_reply.values.get(VSTPDataItemType.SMPP_PASSWORD.getId()));
					
				}			
			}
		}catch(Exception e){
			e.printStackTrace();
		}

	}

}
