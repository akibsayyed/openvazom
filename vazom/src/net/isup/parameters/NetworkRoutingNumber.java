package net.isup.parameters;

import net.isup.ParameterType;

public class NetworkRoutingNumber extends ParameterBase {
	public NetworkRoutingNumber(){
		type = ParameterType.NETWORK_ROUTING_NUMBER;
	}
}
