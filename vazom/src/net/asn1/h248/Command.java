package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Command extends CHOICE{

	public AmmRequest get_addReq(){
		return (AmmRequest)elements.get(0).data;
	}
	public AmmRequest new_addReq(){
		return new AmmRequest();
	}

	public AmmRequest get_moveReq(){
		return (AmmRequest)elements.get(1).data;
	}
	public AmmRequest new_moveReq(){
		return new AmmRequest();
	}

	public AmmRequest get_modReq(){
		return (AmmRequest)elements.get(2).data;
	}
	public AmmRequest new_modReq(){
		return new AmmRequest();
	}

	public SubtractRequest get_subtractReq(){
		return (SubtractRequest)elements.get(3).data;
	}
	public SubtractRequest new_subtractReq(){
		return new SubtractRequest();
	}

	public AuditRequest get_auditCapRequest(){
		return (AuditRequest)elements.get(4).data;
	}
	public AuditRequest new_auditCapRequest(){
		return new AuditRequest();
	}

	public AuditRequest get_auditValueRequest(){
		return (AuditRequest)elements.get(5).data;
	}
	public AuditRequest new_auditValueRequest(){
		return new AuditRequest();
	}

	public NotifyRequest get_notifyReq(){
		return (NotifyRequest)elements.get(6).data;
	}
	public NotifyRequest new_notifyReq(){
		return new NotifyRequest();
	}

	public ServiceChangeRequest get_serviceChangeReq(){
		return (ServiceChangeRequest)elements.get(7).data;
	}
	public ServiceChangeRequest new_serviceChangeReq(){
		return new ServiceChangeRequest();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_addReq(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_moveReq(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_modReq(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_subtractReq(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditCapRequest(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditValueRequest(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_notifyReq(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceChangeReq(); }});
	}
	public Command(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
