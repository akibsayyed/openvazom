package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SMS_MessageWaitingIndicator extends NULL{

	public SMS_MessageWaitingIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 5;
	}
}
