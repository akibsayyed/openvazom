package net.isup.parameters;

import net.isup.ParameterType;

public class CallDiversionTreatmentIndicators extends ParameterBase {
	public CallDiversionTreatmentIndicators(){
		type = ParameterType.CALL_DIVERSION_TREATMENT_INDICATORS;
	}
}
