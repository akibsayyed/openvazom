package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UnsolicitedResponseRes extends SET{

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(0).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public AlertCode get_alertCode(){
		return (AlertCode)elements.get(1).data;
	}
	public AlertCode new_alertCode(){
		return new AlertCode();
	}

	public BillingID get_billingID(){
		return (BillingID)elements.get(2).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(3).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(4).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(5).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(6).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(7).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(8).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(9).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DisplayText2 get_displayText2(){
		return (DisplayText2)elements.get(10).data;
	}
	public DisplayText2 new_displayText2(){
		return new DisplayText2();
	}

	public DMH_AccountCodeDigits get_dmh_AccountCodeDigits(){
		return (DMH_AccountCodeDigits)elements.get(11).data;
	}
	public DMH_AccountCodeDigits new_dmh_AccountCodeDigits(){
		return new DMH_AccountCodeDigits();
	}

	public DMH_AlternateBillingDigits get_dmh_AlternateBillingDigits(){
		return (DMH_AlternateBillingDigits)elements.get(12).data;
	}
	public DMH_AlternateBillingDigits new_dmh_AlternateBillingDigits(){
		return new DMH_AlternateBillingDigits();
	}

	public DMH_BillingDigits get_dmh_BillingDigits(){
		return (DMH_BillingDigits)elements.get(13).data;
	}
	public DMH_BillingDigits new_dmh_BillingDigits(){
		return new DMH_BillingDigits();
	}

	public ExtendedMSCID get_extendedMSCID(){
		return (ExtendedMSCID)elements.get(14).data;
	}
	public ExtendedMSCID new_extendedMSCID(){
		return new ExtendedMSCID();
	}

	public ExtendedSystemMyTypeCode get_extendedSystemMyTypeCode(){
		return (ExtendedSystemMyTypeCode)elements.get(15).data;
	}
	public ExtendedSystemMyTypeCode new_extendedSystemMyTypeCode(){
		return new ExtendedSystemMyTypeCode();
	}

	public LegInformation get_legInformation(){
		return (LegInformation)elements.get(16).data;
	}
	public LegInformation new_legInformation(){
		return new LegInformation();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(17).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(18).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(19).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(20).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public PilotBillingID get_pilotBillingID(){
		return (PilotBillingID)elements.get(21).data;
	}
	public PilotBillingID new_pilotBillingID(){
		return new PilotBillingID();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(22).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(23).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(24).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public RedirectingNumberString get_redirectingNumberString(){
		return (RedirectingNumberString)elements.get(25).data;
	}
	public RedirectingNumberString new_redirectingNumberString(){
		return new RedirectingNumberString();
	}

	public RedirectingPartyName get_redirectingPartyName(){
		return (RedirectingPartyName)elements.get(26).data;
	}
	public RedirectingPartyName new_redirectingPartyName(){
		return new RedirectingPartyName();
	}

	public RedirectingSubaddress get_redirectingSubaddress(){
		return (RedirectingSubaddress)elements.get(27).data;
	}
	public RedirectingSubaddress new_redirectingSubaddress(){
		return new RedirectingSubaddress();
	}

	public TerminationTreatment get_terminationTreatment(){
		return (TerminationTreatment)elements.get(28).data;
	}
	public TerminationTreatment new_terminationTreatment(){
		return new TerminationTreatment();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(29).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public TriggerAddressList get_triggerAddressList(){
		return (TriggerAddressList)elements.get(30).data;
	}
	public TriggerAddressList new_triggerAddressList(){
		return new TriggerAddressList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(75, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_alertCode(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(299, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText2(); }});
		elements.add(new ElementDescriptor(140, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AccountCodeDigits(); }});
		elements.add(new ElementDescriptor(141, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AlternateBillingDigits(); }});
		elements.add(new ElementDescriptor(142, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_BillingDigits(); }});
		elements.add(new ElementDescriptor(53, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedMSCID(); }});
		elements.add(new ElementDescriptor(54, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extendedSystemMyTypeCode(); }});
		elements.add(new ElementDescriptor(144, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_legInformation(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(169, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotBillingID(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(101, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberString(); }});
		elements.add(new ElementDescriptor(245, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingPartyName(); }});
		elements.add(new ElementDescriptor(102, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingSubaddress(); }});
		elements.add(new ElementDescriptor(121, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTreatment(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(276, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerAddressList(); }});
	}
	public UnsolicitedResponseRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
