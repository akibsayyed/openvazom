package net.asn1.compiler;

import java.io.BufferedReader;
import java.io.BufferedWriter;
import java.io.File;
import java.io.FileReader;
import java.io.FileWriter;
import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.asn1.tcap2.Component;
import net.asn1.tcap2.OrigTransactionID;

public class Asn2Javav2 {
	private String asn1contents;
	private ArrayList<ASNType> def_single;
	private ArrayList<ASNType> def_multi; 
	private String java_package = "net.asn1.tcap";
	private String out_dir = "out";
	
	
	private String preProcess(String input){
		String res = input;
		res = res.replaceAll("(?i)OBJECT IDENTIFIER", "OBJECT_IDENTIFIER");
		res = res.replaceAll("(?i)CHARACTER STRING", "CHARACTER_STRING");
		res = res.replaceAll("(?i)OCTET STRING", "OCTET_STRING");
		res = res.replaceAll("(?i)BIT STRING", "BIT_STRING");
		res = res.replaceAll("(?i)OBJECT DESCRIPTOR", "OBJECT_DESCRIPTOR");
		res = res.replaceAll("(?i)EMBEDDED PDV", "EMBEDDED_PDV");
		return res;
		
	}
	
	public void setOutputOptions(String _package, String _out_dir){
		java_package = _package;
		out_dir = _out_dir;
		
	}
	private ASNType defTokenize(String def){
		/*
		 0:dialog [0] IMPLICIT Dialog1
			1:dialog
			2:0
			3:IMPLICIT
			4:Dialog1
			5:
		 */
		ASNType res = new ASNType();
		Pattern pt = Pattern.compile("^(\\S+)\\s*(?:\\[(.*?)\\]\\s*(IMPLICIT|EXPLICIT)?\\s*)?(?:(.*?))\\s*((?: OF|SIZE|\\().*?)?((OPTIONAL)|$)", Pattern.CASE_INSENSITIVE);
		Matcher mt = pt.matcher(def);
		mt.find();
		System.out.println("Def: " + def);
		for(int i = 0; i<mt.groupCount(); i++) System.out.println(i + ":" + mt.group(i));
	
		// Type parameters
		
		res.name = mt.group(1).replaceAll("-", "_");
		res.type = mt.group(4);
		res.implicit = (mt.group(3) != null ? mt.group(3).equalsIgnoreCase("IMPLICIT") : false);
		res.explicit = (mt.group(3) != null ? mt.group(3).equalsIgnoreCase("EXPLICIT") : false);
		res.optional = (mt.group(6) != null ? mt.group(6).equalsIgnoreCase("OPTIONAL") : false);
		res.constraint = mt.group(5);
		
		// Non UNIVERSAL tag number
		if(mt.group(2) != null){
			String tmp[] = mt.group(2).split(" +");
			// APPLICATION/PRIVATE/UNIVERSAL
			if(tmp.length > 1){
				res.asn_class = ASNTagClass.valueOf(tmp[0].toUpperCase());
				res.tag = Integer.parseInt(tmp[1]);
			// CONTEXT SPECIFIC
			}else{
				res.asn_class = ASNTagClass.CONTEXT_SPECIFIC;
				res.tag = Integer.parseInt(tmp[0]);
			}
		// UNIVERSAL tag number
		}else{
			res.asn_class = ASNTagClass.UNIVERSAL;
			res.tag = ASNTag.name2Tag(mt.group(4).toUpperCase().replace(" ", "_").replaceAll("_\\(.*", ""));
			
		}
		System.out.println("Tag: " + res.tag + " = > " + mt.group(4).toUpperCase().replace(" ", "_").replaceAll("_\\(.*", ""));
		System.out.println("Tag type: " + res.asn_class);
		System.out.println("-------------");

		
		return res;
		
		
	}
	private ASNType tagFromName(String name){
		ASNType res = null;
		for(int i = 0; i<def_single.size(); i++){
			res = def_single.get(i);
			if(res.name.equals(name)) return res;
		}
		for(int i = 0; i<def_multi.size(); i++){
			res = def_multi.get(i);
			if(res.name.equals(name)) return res;
		}
		res = new ASNType();
		res.tag = ASNTag.name2Tag(name);
		
		return res;
	}
	public String processConstrucedTypes(ASNType tag){
		ASNType ast;
		ASNType ast2;
		String[] tmp;
		String res = "";
		String elems = "";
		String tmp_type;
		int c = 0;
		boolean ext = false;
		//if(tag.universalTag == ASNTag.CHOICE) res += "\t" + "public ASNType selectedChoice;\n\n";
		if(tag.of_type){
			//Pattern pt = Pattern.compile("OF (\\S*)", Pattern.CASE_INSENSITIVE);
			//Matcher mt = pt.matcher(tag.constraint);
			//mt.find();
			
			//res += "\t" + "public ArrayList<" + mt.group(1).replaceAll("-", "_") + "> children;\n\n"; 
		}else{
			if(tag.multi_line){
				// check for comments, remove if found
				tag.def = Pattern.compile("--.*?$", Pattern.MULTILINE).matcher(tag.def).replaceAll("");
				tmp = tag.def.split(",");
				//res += "\tpublic void initElements(){\n";

				for(int i = 0; i<tmp.length; i++){
					// extension addition marker
					if(tmp[i].trim().equals("...")) ext = true; 
					else{
						// tokenize
						ast = defTokenize(tmp[i].trim().replace("\n", ""));
						// if user tag, look for universal definition
						tmp_type = ast.type;
						// if not universal type, look for reference to original universal type
						while(ast.tag == -1){
							ast2 = tagFromName(tmp_type.replaceAll("-", "_"));
							if(ast2.tag == -1){
								tmp_type = ast2.type;
							}else{
								ast.tag = ast2.tag;
								// if not IMPLICIT, override tag value
								if(!ast.implicit){
									ast.asn_class = ast2.asn_class;
								}
							}
							
						}

						//if(ast.tag != -1) res += "\t@TagDescriptor(tagValue = " + ast.tag+ ", tagClass = ASNTagClass." + ast.asn_class + (ast.optional ? ", optional = true" : "") + (ext ? ", extAddition = true" : "") + ")\n";
						//res += "\tpublic " + ast.type.replaceAll("[ -]", "_") + " _" + ast.name + ";\n";
						res += "\tpublic " + ast.type.replaceAll("[ -]", "_") + " get_" + ast.name + "(){\n";
						res += "\t\treturn (" + ast.type.replaceAll("[ -]", "_") + ")elements.get(" + c + ").data;\n";
						res += "\t}\n";
						

						res += "\tpublic " + ast.type.replaceAll("[ -]", "_") + " new_" + ast.name + "(){\n";
						res += "\t\treturn new " + ast.type.replaceAll("[ -]", "_") + "();\n";
						res += "\t}\n";

						elems += "\t\telements.add(new ElementDescriptor(" + ast.tag + ", ASNTagClass." + ast.asn_class + ", " + (ast.optional ? "true" : "false") + ", " + (ext ? "true" : "false") + ")";
						elems += "{public void set(){ data = new_" + ast.name + "(); }});\n";
						if(ast.tag != -1) res += "\n";
						c++;
					}
				}
			}
		}
		if(!tag.of_type) res += "\tpublic void initElements(){\n" + elems + "\t}\n";
		return res;
	}

	public ASNType decodeType(String name, String def, String def_full, String marker){
		ASNType ast = null;
		Pattern pt;
		Matcher mt;
		// MULTI LINE definition = CONSTRUCTED type
		// exception = INTEGER which is PRIMITIVE, Named number list is
		// not important for decoding process
		if(marker.equals("M")){
			// SEQUENCE / CHOICE
			ast = defTokenize(name + " " + def);
			ast.multi_line = true;
			// ASN.1 Universal tag type
			ast.universalTag = ASNTag.valueOf(ast.type.toUpperCase().replace(" ", "_").replaceAll("_\\(.*", ""));
			// INTEGER, ENUMERATED types are always PRIMITIVE
			ast.asn_pc = (ast.universalTag == ASNTag.INTEGER  || ast.universalTag == ASNTag.ENUMERATED || ast.universalTag == ASNTag.BIT_STRING ? ASNTagComplexity.Primitive : ASNTagComplexity.Constructed);
			// save definition for further processing
			ast.def = def_full;
			// add to list of mult line types
			def_multi.add(ast);
				
		// SINGLE LINE definition = CONSTRUCTED or PRIMITIVE
		// ANY(Open type) = UNKNOWN
		}else{
			//System.out.println("Primitive: " + def);
			ast = defTokenize(name + " " + def.replace("\n", "").replace("::=", ""));
			// ASN.1 Universal tag type
			try{
				ast.universalTag = ASNTag.valueOf(ast.type.toUpperCase().replace(" ", "_").replaceAll("_\\(.*", ""));
			// type is not standard type, user defined type
			}catch(Exception e) {
				ast.universalTag = null;
			}
			// determine complexity
			// SEQUENCE OF, SET OF, EXTERNAL => Constructed
			// ANY => Unknown
			pt = Pattern.compile("(ANY)?(?:.*(EXTERNAL))?(?:.*?\\s(OF))?(.*)", Pattern.CASE_INSENSITIVE);
			// TODO
			// check
			//pt = Pattern.compile("(?:(ANY)|(EXTERNAL)| (OF) )(.*)", Pattern.CASE_INSENSITIVE);
			mt = pt.matcher(def);
			mt.find();
			for(int i = 0; i<mt.groupCount(); i++) System.out.println(i + ": " + mt.group(i));
			// EXTERNAL or SEQUENCE/SET OF
			if(mt.group(3) != null || mt.group(2) != null){ 
				ast.asn_pc = ASNTagComplexity.Constructed;
				// OF type = array type(SEQUENCE OF)
				if(mt.group(3) != null) ast.of_type = true;
			// ANY
			}else if(mt.group(1) != null) ast.asn_pc = ASNTagComplexity.Unknown;
			else ast.asn_pc = ASNTagComplexity.Primitive;
			// save definition for further processing
			ast.def = def;
			// add to list
			def_single.add(ast);
			
		}
		
		//System.out.println("Name: " + name + ", Type: " + def + ", Marker: " + marker);
		return ast;
		
	}

	public void getDataTypes(){
		
		String res = null;
		String tmp;
		String def_full = "";
		Pattern pt = Pattern.compile("^([^-|\\}|\n][\\S]+)\\s*::=\\s*(.+?)\\s*([\\{|\n])", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher mt = pt.matcher(asn1contents);
		Pattern pt2 = Pattern.compile("(.*?)\\}", Pattern.DOTALL | Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher mt2 = null;
		
		while(mt.find()){
			res = mt.group(1);
			// if { found, Constructed type found, set 'M'(multi line) marker
			// else set 'S'(single line) primitive marker
			
			tmp = mt.group(3).replace("\n", "S");
			tmp = tmp.replace("{", "M");
			
			// multiline type
			if(tmp.equals("M")){
				mt2 = pt2.matcher(asn1contents.substring(mt.end()));
				mt2.find();
				def_full = mt2.group(1);
			}

			decodeType(mt.group(1), mt.group(2), def_full, tmp);
			def_full = "";
			
			
		}
		
	}

	private void createJavaClass(String name, String def){
		File f = new File(out_dir + "/" + name);
		try{
			BufferedWriter bw = new BufferedWriter(new FileWriter(f));
			bw.write(def);
			bw.close();
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	private String createExplicitWrapper(ASNType ast){
		String res = "";
		ASNType ast2 = tagFromName(ast.type.replaceAll("-", "_"));
		//res += "\tprivate class " + ast.name + "_INNER extends " + ast.universalTag + "{\n";
		//System.out.println("AAAAAAAAAAAAAAAAAAA: " + ast.type);
		//res += "\t}\n";
		//res += "\t@TagDescriptor(tagValue = " + ast2.tag+ ", tagClass = ASNTagClass." + ast2.asn_class + (ast.optional ? ", optional = true" : "") + ")\n";
		//res += "\tpublic " + ast.type + " _" + ast.name + ";\n\n";

		res += "\tpublic " + ast.type.replaceAll("[ -]", "_") + " get_" + ast.name + "(){\n";
		res += "\t\treturn (" + ast.type.replaceAll("[ -]", "_") + ")elements.get(0).data;\n";
		res += "\t}\n";

		res += "\tpublic " + ast.type.replaceAll("[ -]", "_") + " new_" + ast.name + "(){\n";
		res += "\t\treturn new " + ast.type.replaceAll("[ -]", "_") + "();\n";
		res += "\t}\n";
		

		
		
		res += "\tpublic void initElements(){\n"; 
		//res += "\t\telements.add(new ElementDescriptor(" + ast2.tag + ", ASNTagClass." + ast2.asn_class + ", false, false));\n";
		res += "\t\telements.add(new ElementDescriptor(" + ast.tag + ", ASNTagClass." + ast.asn_class + ", " + (ast.optional ? "true" : "false") + ", false" + ")";
		res += "{public void set(){ data = new_" + ast.name + "(); }});\n";

		
		res += "\t}\n";
		
		//elems += "\t\telements.add(new ElementDescriptor(" + ast.tag + ", ASNTagClass." + ast.asn_class + ", " + (ast.optional ? "true" : "false") + ", " + (ext ? "true" : "false") + "));\n";
		
		return res;
		
	}
	private String generate_int_consts(String def){
		String res = "";
		String[] tmp;
		Pattern pt = Pattern.compile("(\\S*?)\\s*\\((.*)\\)(.*)", Pattern.CASE_INSENSITIVE);
		Matcher mt;
		tmp = Pattern.compile("--.*?$", Pattern.MULTILINE).matcher(def).replaceAll("").split(",");
		//System.out.println("!!!!!!!!!!!!!!!!!!!!!!!" + def);
		for(int i = 0; i<tmp.length; i++){
			// check for extension addition marker
			if(!tmp[i].trim().startsWith("...")){
				mt = pt.matcher(tmp[i]);
				mt.find();
				res += "\tpublic static final int _" + mt.group(1).replaceAll("-", "_") + " = " + mt.group(2) + ";\n"; 
				//for(int j = 0; j<mt.groupCount(); j++) System.out.println(j+":"+mt.group(j));
				
			}
			
			
		}
		return res;
		
	}
	private String generate_consts(String type){
		String res = "";
		Pattern pt = Pattern.compile("(\\S+?)[\\t| ]+" + type + "[\t| ]*::=[\t| ]*(.*)(.*)$", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher mt = pt.matcher(asn1contents);
		while(mt.find()) res += "\tpublic static final String " + mt.group(1).replaceAll("-", "_") + " = \"" + mt.group(2) + "\";\n";
		return res;
		
	}

	private void generate_primitive(){
		String res = "";
		ASNType ast;
		ArrayList<ASNType> lst = new ArrayList<ASNType>();
		// chek for Primitives in single line definitions
		for(int i = 0; i<def_single.size(); i++){
			ast = def_single.get(i);
			if(ast.asn_pc == ASNTagComplexity.Primitive) lst.add(ast);
			else if(ast.asn_pc == ASNTagComplexity.Unknown) lst.add(ast);
		}
		// chek for Primitives in multi line definitions
		for(int i = 0; i<def_multi.size(); i++){
			ast = def_multi.get(i);
			if(ast.asn_pc == ASNTagComplexity.Primitive) lst.add(ast);
		}

		for(int i = 0; i<lst.size(); i++){
			ast = lst.get(i);
			 
			res = "package " + java_package + ";\n\n";
			if(ast.of_type) res += "import java.util.ArrayList;\n";
			res += "import net.asn1.compiler.*;\n";
			res += "import net.asn1.types.*;\n";
			
			res += "public class " + ast.name + " extends " + (ast.explicit ? "ASNType" : (ast.universalTag == null ? ast.type.replaceAll("-", "_") :ast.universalTag)) + "{\n\n";

			// constants for INTEGER, ENUMERATED, BIT STRING types
			if(ast.multi_line && (ast.universalTag == ASNTag.INTEGER || ast.universalTag == ASNTag.ENUMERATED || ast.universalTag == ASNTag.BIT_STRING)){
				res += generate_int_consts(ast.def);
				
			}
			// check for constants if any
			if(!ast.multi_line) res += generate_consts(ast.name);
			
			
			if(ast.explicit) res += createExplicitWrapper(ast);
			res += "\tpublic " + ast.name + "(){\n";
			// tag definitions
			//res += "\t\tuniversalTag = ASNTag." + ast.universalTag + ";\n";
			res += "\t\tsuper();\n";
			if(ast.tag != -1) res += "\t\tasn_pc = ASNTagComplexity." + ast.asn_pc + ";\n";
			//res += "\t\tname = \"" + ast.name + "\";\n";
			//res += "\t\ttype = \"" + ast.type + "\";\n";
			if(ast.tag != -1) res += "\t\ttag = " + ast.tag + ";\n";
			if(ast.asn_class != ASNTagClass.UNIVERSAL) res += "\t\tasn_class = ASNTagClass." + ast.asn_class + ";\n";
			//if(ast.asn_pc == ASNTagComplexity.Primitive) res += "\t\tchildren = null;\n";
			//else res += "\t\tchildren = new ArrayList<ASNType>();\n";
			
			res += "\t}\n";
			res += "}\n";
			System.out.println(res);
			createJavaClass(ast.name + ".java", res);
			res = "";
			
		}
	}
	public void generate_constructed(){
		String res = "";
		ASNType ast;
		ArrayList<ASNType> lst = new ArrayList<ASNType>();
		// chek for Primitives in single line definitions
		for(int i = 0; i<def_single.size(); i++){
			ast = def_single.get(i);
			if(ast.asn_pc == ASNTagComplexity.Constructed) lst.add(ast);
		}
		// chek for Primitives in multi line definitions
		for(int i = 0; i<def_multi.size(); i++){
			ast = def_multi.get(i);
			if(ast.asn_pc == ASNTagComplexity.Constructed) lst.add(ast);
		}
		
		for(int i = 0; i<lst.size(); i++){
			ast = lst.get(i);
			res = "package " + java_package + ";\n\n";
			if(ast.of_type) res += "import java.util.ArrayList;\n";
			res += "import net.asn1.compiler.*;\n";
			res += "import net.asn1.types.*;\n";
			res += "import java.util.ArrayList;\n";
			res += "public class " + ast.name + " extends " + (ast.explicit ? "ASNType" : ast.universalTag) + "{\n\n";
			//if(!ast.of_type) res += "\tpublic ArrayList<ElementDescriptor> elements;\n\n";
			if(ast.of_type){
				Pattern pt = Pattern.compile("OF (\\S*)", Pattern.CASE_INSENSITIVE);
				Matcher mt = pt.matcher(ast.constraint);
				mt.find();
			//	ASNType tmp_t = tagFromName(mt.group(1).replaceAll("-", "_"));
				
				res += "\tpublic " + mt.group(1).replaceAll("-", "_") + " new_child(){\n";
				res += "\t\treturn new " + mt.group(1).replaceAll("-", "_") + "();\n";
				res += "\t}\n";
				
				res += "\tpublic void addChild(){\n";
				res += "\t\tof_children.add(new_child());\n";
				res += "\t}\n";
				
				res += "\tpublic " + mt.group(1).replaceAll("-", "_") + " getChild(int index){\n";
				res += "\t\treturn (" + mt.group(1).replaceAll("-", "_") + ")of_children.get(index);\n";
				res += "\t}\n";
				
				
			}
			
			if(ast.explicit) res += createExplicitWrapper(ast); else res += processConstrucedTypes(ast);
			res += "\tpublic " + ast.name + "(){\n";
			// tag definitions
			//res += "\t\tuniversalTag = ASNTag." + ast.universalTag + ";\n";
			res += "\t\tsuper();\n";
			res += "\t\tasn_pc = ASNTagComplexity." + ast.asn_pc + ";\n";
			//res += "\t\tname = \"" + ast.name + "\";\n";
			//res += "\t\ttype = \"" + ast.type + "\";\n";
			res += "\t\ttag = " + ast.tag + ";\n";
			if(!ast.of_type) res += "\t\telements = new ArrayList<ElementDescriptor>();\n";
			if(ast.of_type){
				res += "\t\tof_children = new ArrayList<ASNType>();\n";
				/*
				Pattern pt = Pattern.compile("OF (\\S*)", Pattern.CASE_INSENSITIVE);
				Matcher mt = pt.matcher(ast.constraint);
				mt.find();
				ASNType tmp_t = tagFromName(mt.group(1).replaceAll("-", "_"));
				res += "\t\tof_tag = " + tmp_t.tag + ";\n";
				res += "\t\tof_class = ASNTagClass." + tmp_t.asn_class + ";\n";
				res += "\t\tof_pc = ASNTagComplexity." + tmp_t.asn_pc + ";\n";
				*/
			}
			if(!ast.of_type) res += "\t\tinitElements();\n";
			if(ast.explicit) res += "\t\texplicit = true;\n";
			if(ast.asn_class != ASNTagClass.UNIVERSAL) res += "\t\tasn_class = ASNTagClass." + ast.asn_class + ";\n";

			res += "\t}\n";
			res += "}\n";
			System.out.println(res);
			
			
			createJavaClass(ast.name + ".java", res);
			res = "";
			
		}
		
		
		
	}
	public void generate(){
		generate_primitive();
		generate_constructed();
		
	}
	public String getDefinitionName(){
		String res = null;
		Pattern pt = Pattern.compile("^sasn\\.1:(.*)$", Pattern.CASE_INSENSITIVE | Pattern.MULTILINE);
		Matcher mt = pt.matcher(asn1contents);
		mt.find();
		res = mt.group(1);

		return res;
		
	}
	public void compile(String fname){
		readFile(fname);
		//System.out.println(asn1contents);
		
	}
	
	private void readFile(String fname){
		String line;
		asn1contents = "";
		try{
			BufferedReader reader = new BufferedReader(new FileReader(fname));
			while( (line = reader.readLine()) != null){
				asn1contents += line + "\n";
			}
			reader.close();
			asn1contents = preProcess(asn1contents);
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
	}
	
	public Asn2Javav2(){
		def_single = new ArrayList<ASNType>();
		def_multi = new ArrayList<ASNType>();
		
	}
	
}
