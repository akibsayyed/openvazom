package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ReturnErrorProblem extends INTEGER{

	public static final int _unrecognizedInvokeID = 0;
	public static final int _returnErrorUnexpected = 1;
	public static final int _unrecognizedError = 2;
	public static final int _unexpectedError = 3;
	public static final int _mistypedParameter = 4;
	public ReturnErrorProblem(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
