package net.smsfs;

import java.util.ArrayList;
import java.util.HashMap;
import net.vstp.VSTPDataItemType;

public class FilterResult {
	public boolean isUnconditional;
	public boolean result;
	public int total_score = 0;
	public boolean quarantine;
	public boolean no_dr;
	public String filter_exit_point = "";
	
	// routing connections
	public ArrayList<String> routing_connections;
	
	// mdified on signaling node
	public ArrayList<VSTPModifier> vstp_modifiers;
	//public HashMap<VSTPDataItemType, String> vstp_modifiers;
	
	public FilterResult(){
		vstp_modifiers = new ArrayList<VSTPModifier>();
		routing_connections = new ArrayList<String>();
	}
	
	
	
	public static boolean evaluate(FilterResult fres, int max_total_score){
		if(fres != null){
			// if UNCONDITIONAL, total_socre is bypassed
			// ACCEPTED
			if(fres.result){
				if(fres.isUnconditional) return true;
				else{
					// check total_score
					// ACCEPTED
					if(fres.total_score <= max_total_score){
						return true;
					// REJECTED
					}else{
						return false;
					}
				}
			}else return false;
		}else{
			return true;
			
		}
	}
}
