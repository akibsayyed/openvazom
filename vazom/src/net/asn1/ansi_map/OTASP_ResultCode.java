package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class OTASP_ResultCode extends OCTET_STRING{

	public OTASP_ResultCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
