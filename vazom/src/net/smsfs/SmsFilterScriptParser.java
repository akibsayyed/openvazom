// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/dfranusic/SmsFilterScript.g 2011-10-18 12:35:31
package net.smsfs;
import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;


import org.antlr.runtime.tree.*;

public class SmsFilterScriptParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "SCCP_GT_CALLED_WL", "SCCP_GT_CALLING_WL", "MAP_SCOA_WL", "MAP_SCDA_WL", "SMPP_IP_SOURCE", "SMPP_IP_DESTINATION", "SMPP_TCP_SOURCE", "SMPP_TCP_DESTINATION", "SMPP_SYSTEM_ID", "SMPP_PASSWORD", "SMPP_SERVICE_TYPE", "SMPP_ORIGINATOR_TON", "SMPP_ORIGINATOR_NP", "SMPP_ORIGINATOR_ADDRESS", "SMPP_RECIPIENT_TON", "SMPP_RECIPIENT_NP", "SMPP_RECIPIENT_ADDRESS", "SMPP_ESM_MESSAGE_MODE", "SMPP_ESM_MESSAGE_TYPE", "SMPP_ESM_GSM_FEATURES", "SMPP_PROTOCOL_ID", "SMPP_PRIORITY_FLAG", "SMPP_DELIVERY_TIME", "SMPP_VALIDITY_PERIOD", "SMPP_RD_SMSC_RECEIPT", "SMPP_RD_SME_ACK", "SMPP_RD_INTERMEDIATE_NOTIFICATION", "SMPP_REPLACE_IF_PRESENT", "SMPP_DATA_CODING", "SMPP_SM_DEFAULT_MSG_ID", "SMPP_SM_LENGTH", "SMPP_SM", "SMPP_ESM_MM_DEFAULT", "SMPP_ESM_MM_DATAGRAM", "SMPP_ESM_MM_FORWARD", "SMPP_ESM_MM_STORE_FORWARD", "SMPP_ESM_MT_DEFAULT", "SMPP_ESM_MT_DELIVERY_ACK", "SMPP_ESM_MT_MANUAL_USER_ACK", "SMPP_ESM_GF_NO", "SMPP_ESM_GF_UDHI_INDICATOR", "SMPP_ESM_GF_SET_REPLY_PATH", "SMPP_ESM_GF_SET_BOTH", "SMPP_TON_UNKNOWN", "SMPP_TON_INTERNATIONAL", "SMPP_TON_NATIONAL", "SMPP_TON_NETWORK_SPECIFIC", "SMPP_TON_SUBSCRIBER_NUMBER", "SMPP_TON_ALPHANUMERIC", "SMPP_TON_ABBREVIATED", "SMPP_NP_UNKNOWN", "SMPP_NP_ISDN_TELEPHONE", "SMPP_NP_DATA_X121", "SMPP_NP_TELEX", "SMPP_NP_LAND_MOBILE", "SMPP_NP_NATIONAL", "SMPP_NP_PRIVATE", "SMPP_NP_ERMES", "SMPP_NP_INTERNET_IP", "SMPP_NP_WAP_CLIENT_ID", "SMPP_RD_SMSCDR_NO", "SMPP_RD_SMSCDR_SUCCESS_FAILURE", "SMPP_RD_SMSCDR_FAILURE", "SMPP_RD_SMEOA_NO", "SMPP_RD_SMEOA_ACK", "SMPP_RD_SMEOA_MANUAL_USER_ACK", "SMPP_RD_SMEOA_BOTH", "SMPP_RD_IN_NO", "SMPP_RD_IN_YES", "SMPP_DC_DEFAULT", "SMPP_DC_IA5_ASCII", "SMPP_DC_8BIT_BINARY_1", "SMPP_DC_ISO_8859_1", "SMPP_DC_8BIT_BINARY_2", "SMPP_DC_JIS", "SMPP_DC_ISO_8859_5", "SMPP_DC_ISO_8859_8", "SMPP_DC_UCS2", "SMPP_DC_PICTOGRAM", "SMPP_DC_ISO_2011_JP", "SMPP_DC_EXTENDED_KANJI", "SMPP_DC_KS_C_5601", "SCCP_GT_CALLED_ADDRESS", "SCCP_GT_CALLING_ADDRESS", "SCCP_GT_CALLED_TT", "SCCP_GT_CALLING_TT", "SCCP_GT_CALLED_NAI", "SCCP_GT_CALLING_NAI", "SCCP_GT_CALLED_NP", "SCCP_GT_CALLING_NP", "SCCP_GT_CALLED_GTI", "SCCP_GT_CALLING_GTI", "MAP_SCOA", "MAP_SCDA", "MAP_IMSI", "MAP_MSISDN", "M3UA_DPC", "M3UA_OPC", "SMS_TPDU_ORIGINATING", "SMS_TPDU_ORIGINATING_ENC", "SMS_TPDU_DESTINATION", "SMS_TPDU_DESTINATION_ENC", "SMS_TPDU_UD", "DICT_SMS_TPDU_UD", "HLR_IMSI", "HLR_MSISDN", "HLR_NNN", "HLR_ANNN", "HLR_SCA", "HLR_RESULT_IMSI", "HLR_RESULT_NNN", "HLR_RESULT_ANNN", "SPAM_SMS_TPDU_UD", "RP_SMS_TPDU_UD", "QUARANTINE_SMS_TPDU_UD", "MD5_SMS_TPDU_UD", "SMS_TPDU_DCS", "SMS_MSG_TYPE", "TON_UNKNOWN", "TON_INTERNATIONAL", "TON_NATIONAL", "TON_NETWORK_SPECIFIC", "TON_SUBSCRIBER_NUMBER", "TON_ALPHANUMERIC", "TON_ABBREVIATED", "NP_UNKNOWN", "NP_ISDN_TELEPHONE", "NP_GENERIC", "NP_DATA_X121", "NP_TELEX", "NP_MARITIME", "NP_LAND_MOBILE", "NP_ISDN_MOBILE", "NP_PRIVATE", "NAI_UNKNOWN", "NAI_SUBSCRIBER_NUMBER", "NAI_RESERVED_FOR_NATIONAL_USE", "NAI_NATIONAL_SIGNIFICANT_NUMBER", "NAI_INTERNATIONAL", "GTI_NONE", "GTI_NAI", "GTI_TT", "GTI_TTNPE", "GTI_TTNPENOA", "DCS_DEFAULT", "DCS_8BIT", "DCS_UCS2", "MSG_TYPE_SINGLE", "MSG_TYPE_CONCATENATED", "SPAM_UPDATE_LST", "SPAM_REMOVE_LST", "QUARANTINE_UPDATE_LST", "QUARANTINE_REMOVE_LST", "MD5_UPDATE_LST", "MD5_REMOVE_LST", "HLR_REQUEST", "NO_DR", "CONVERT_SS7", "CONVERT_SMPP", "FLOOD", "FLOOD_MAX", "FLOOD_GLOBAL", "FLOOD_GLOBAL_MAX", "FLOOD_ALL_MAX", "FLOOD_HOUR", "FLOOD_MINUTE", "FLOOD_DAY", "FLOOD_ALL", "LIST", "RULE", "IN", "NOT_IN", "CONT", "CONT_Q", "ALW", "ALW_Q", "ALWU", "ALWU_Q", "DNY", "DNY_Q", "L_PAREN", "R_PAREN", "R_SQ_B", "L_SQ_B", "L_CR_B", "R_CR_B", "F_MO", "F_SMPP_MO", "F_MT", "F_SMPP_MT", "F_HLR", "F_M3UA", "ON", "OFF", "ANNT", "EQUAL", "ASSIGN", "OR", "NEQUAL", "COLON", "STMTSEP", "PERCENT", "HEX_P", "LT", "GT", "LTE", "GTE", "PLUS", "MINUS", "AND", "MODIFY", "SQUOTE", "DQUOTE", "GOTO", "REGEX_BLOCK", "ASTERISK", "COMMA", "FILTER_NODE", "RULE_EVAL", "RULE_STATUS", "RULE_EVAL_ATOM", "RULE_REGEX", "RULE_REGEX_EXPR", "RULE_EVAL_TRUE", "RULE_EVAL_FALSE", "RULE_EVAL_POINTS", "RULE_DEF", "DUMMY_RULE_DEF", "RULE_LABEL", "MODIFIER", "MODIFIER_STATUS", "MODIFIER_METHOD", "MODIFIER_LABEL", "M3UA_CONNECTION", "CONNECTION_LABEL", "LOCAL_IP", "LOCAL_PORT", "REMOTE_IP", "REMOTE_PORT", "CONN_DPC", "CONN_OPC", "CONN_N_APP", "CONN_RC", "CONN_SC", "WS", "STRINGLITERAL", "DIGITS", "WORD", "IP", "ML_COMMENT", "SL_COMMENT", "DECIMAL", "ESCAPESEQUENCE", "'^'", "'$'", "'.'", "'?'", "'\\\\'"
    };
    public static final int SMPP_RD_IN_YES=72;
    public static final int HEX_P=206;
    public static final int T__259=259;
    public static final int REMOTE_IP=241;
    public static final int T__258=258;
    public static final int T__257=257;
    public static final int MAP_MSISDN=99;
    public static final int SMS_TPDU_ORIGINATING_ENC=103;
    public static final int DQUOTE=216;
    public static final int T__260=260;
    public static final int T__261=261;
    public static final int ALW_Q=179;
    public static final int SMPP_DELIVERY_TIME=26;
    public static final int R_CR_B=189;
    public static final int EOF=-1;
    public static final int GTI_NONE=143;
    public static final int HLR_RESULT_NNN=114;
    public static final int SMPP_RD_SMEOA_NO=67;
    public static final int F_SMPP_MO=191;
    public static final int SPAM_REMOVE_LST=154;
    public static final int SMPP_NP_PRIVATE=60;
    public static final int OFF=197;
    public static final int FLOOD_ALL_MAX=167;
    public static final int SCCP_GT_CALLED_GTI=94;
    public static final int SCCP_GT_CALLED_NP=92;
    public static final int NP_MARITIME=134;
    public static final int FLOOD_GLOBAL=165;
    public static final int NP_ISDN_TELEPHONE=130;
    public static final int GOTO=217;
    public static final int F_SMPP_MT=193;
    public static final int TON_NETWORK_SPECIFIC=125;
    public static final int SMPP_DATA_CODING=32;
    public static final int LOCAL_PORT=240;
    public static final int SMPP_TCP_DESTINATION=11;
    public static final int M3UA_OPC=101;
    public static final int NP_GENERIC=131;
    public static final int SMPP_ESM_MESSAGE_TYPE=22;
    public static final int SMPP_RD_INTERMEDIATE_NOTIFICATION=30;
    public static final int SMPP_NP_WAP_CLIENT_ID=63;
    public static final int GTI_NAI=144;
    public static final int MD5_REMOVE_LST=158;
    public static final int MD5_SMS_TPDU_UD=119;
    public static final int WS=248;
    public static final int M3UA_CONNECTION=237;
    public static final int DCS_UCS2=150;
    public static final int ESCAPESEQUENCE=256;
    public static final int CONN_N_APP=245;
    public static final int FLOOD_ALL=171;
    public static final int RULE_EVAL=222;
    public static final int SMPP_DC_IA5_ASCII=74;
    public static final int MSG_TYPE_SINGLE=151;
    public static final int SL_COMMENT=254;
    public static final int DCS_8BIT=149;
    public static final int QUARANTINE_SMS_TPDU_UD=118;
    public static final int GT=208;
    public static final int SMPP_RD_IN_NO=71;
    public static final int SMPP_TON_ALPHANUMERIC=52;
    public static final int DNY_Q=183;
    public static final int SMPP_ESM_GF_UDHI_INDICATOR=44;
    public static final int HLR_REQUEST=159;
    public static final int STMTSEP=204;
    public static final int NP_ISDN_MOBILE=136;
    public static final int MD5_UPDATE_LST=157;
    public static final int MODIFY=214;
    public static final int DUMMY_RULE_DEF=231;
    public static final int SMPP_SERVICE_TYPE=14;
    public static final int SMPP_ESM_MT_MANUAL_USER_ACK=42;
    public static final int HLR_IMSI=108;
    public static final int SMPP_DC_KS_C_5601=85;
    public static final int SMPP_ESM_GF_NO=43;
    public static final int NP_TELEX=133;
    public static final int F_MO=190;
    public static final int NAI_INTERNATIONAL=142;
    public static final int NAI_NATIONAL_SIGNIFICANT_NUMBER=141;
    public static final int MODIFIER_STATUS=234;
    public static final int SMPP_ESM_MESSAGE_MODE=21;
    public static final int SMPP_ESM_GSM_FEATURES=23;
    public static final int SMPP_DC_JIS=78;
    public static final int TON_UNKNOWN=122;
    public static final int RULE_REGEX=225;
    public static final int ASTERISK=219;
    public static final int HLR_RESULT_ANNN=115;
    public static final int RULE_STATUS=223;
    public static final int SMS_TPDU_ORIGINATING=102;
    public static final int TON_SUBSCRIBER_NUMBER=126;
    public static final int NP_PRIVATE=137;
    public static final int SMPP_ORIGINATOR_NP=16;
    public static final int L_SQ_B=187;
    public static final int SMPP_ESM_MT_DEFAULT=40;
    public static final int SMPP_TON_SUBSCRIBER_NUMBER=51;
    public static final int RP_SMS_TPDU_UD=117;
    public static final int NAI_SUBSCRIBER_NUMBER=139;
    public static final int NAI_RESERVED_FOR_NATIONAL_USE=140;
    public static final int SMPP_RD_SMEOA_MANUAL_USER_ACK=69;
    public static final int RULE_EVAL_ATOM=224;
    public static final int PLUS=211;
    public static final int SMPP_TON_NATIONAL=49;
    public static final int SMPP_NP_NATIONAL=59;
    public static final int SCCP_GT_CALLED_ADDRESS=86;
    public static final int GTI_TTNPENOA=147;
    public static final int SMPP_DC_DEFAULT=73;
    public static final int SMPP_IP_DESTINATION=9;
    public static final int SMPP_ESM_MT_DELIVERY_ACK=41;
    public static final int SMPP_DC_8BIT_BINARY_1=75;
    public static final int F_HLR=194;
    public static final int SMPP_DC_8BIT_BINARY_2=77;
    public static final int FLOOD_GLOBAL_MAX=166;
    public static final int SQUOTE=215;
    public static final int MINUS=212;
    public static final int SCCP_GT_CALLING_ADDRESS=87;
    public static final int NP_LAND_MOBILE=135;
    public static final int CONNECTION_LABEL=238;
    public static final int SMS_TPDU_UD=106;
    public static final int COLON=203;
    public static final int MAP_SCOA_WL=6;
    public static final int SCCP_GT_CALLED_TT=88;
    public static final int DECIMAL=255;
    public static final int RULE_EVAL_TRUE=227;
    public static final int QUARANTINE_UPDATE_LST=155;
    public static final int STRINGLITERAL=249;
    public static final int SMPP_IP_SOURCE=8;
    public static final int SCCP_GT_CALLING_GTI=95;
    public static final int TON_ABBREVIATED=128;
    public static final int RULE_REGEX_EXPR=226;
    public static final int SPAM_SMS_TPDU_UD=116;
    public static final int CONN_DPC=243;
    public static final int RULE_DEF=230;
    public static final int F_MT=192;
    public static final int QUARANTINE_REMOVE_LST=156;
    public static final int MAP_IMSI=98;
    public static final int SMPP_DC_ISO_8859_5=79;
    public static final int LT=207;
    public static final int SMPP_RD_SMSCDR_SUCCESS_FAILURE=65;
    public static final int FLOOD_DAY=170;
    public static final int SMPP_DC_ISO_8859_1=76;
    public static final int CONT_Q=177;
    public static final int SMS_TPDU_DESTINATION_ENC=105;
    public static final int MODIFIER_METHOD=235;
    public static final int SMPP_SYSTEM_ID=12;
    public static final int SMPP_ESM_MM_FORWARD=38;
    public static final int DICT_SMS_TPDU_UD=107;
    public static final int SMPP_RD_SMSCDR_NO=64;
    public static final int MAP_SCDA=97;
    public static final int HLR_NNN=110;
    public static final int MODIFIER=233;
    public static final int SMPP_DC_ISO_8859_8=80;
    public static final int SMPP_RD_SMEOA_BOTH=70;
    public static final int SMPP_VALIDITY_PERIOD=27;
    public static final int DCS_DEFAULT=148;
    public static final int WORD=251;
    public static final int SMPP_RD_SMEOA_ACK=68;
    public static final int HLR_ANNN=111;
    public static final int SMPP_ESM_GF_SET_REPLY_PATH=45;
    public static final int SCCP_GT_CALLED_WL=4;
    public static final int SMPP_TON_ABBREVIATED=53;
    public static final int ALWU_Q=181;
    public static final int SMPP_DC_EXTENDED_KANJI=84;
    public static final int SMPP_PROTOCOL_ID=24;
    public static final int L_PAREN=184;
    public static final int NP_UNKNOWN=129;
    public static final int NP_DATA_X121=132;
    public static final int HLR_SCA=112;
    public static final int SMPP_NP_TELEX=57;
    public static final int SMPP_ESM_MM_DEFAULT=36;
    public static final int SMPP_NP_UNKNOWN=54;
    public static final int NEQUAL=202;
    public static final int SMPP_REPLACE_IF_PRESENT=31;
    public static final int RULE=173;
    public static final int SCCP_GT_CALLING_NAI=91;
    public static final int ON=196;
    public static final int SMPP_TON_INTERNATIONAL=48;
    public static final int SMPP_RD_SME_ACK=29;
    public static final int LOCAL_IP=239;
    public static final int TON_INTERNATIONAL=123;
    public static final int SMS_TPDU_DCS=120;
    public static final int SMPP_RECIPIENT_TON=18;
    public static final int SMPP_SM_DEFAULT_MSG_ID=33;
    public static final int LIST=172;
    public static final int RULE_EVAL_FALSE=228;
    public static final int NAI_UNKNOWN=138;
    public static final int SMPP_NP_ERMES=61;
    public static final int SMPP_RECIPIENT_ADDRESS=20;
    public static final int SMS_TPDU_DESTINATION=104;
    public static final int FLOOD=163;
    public static final int NOT_IN=175;
    public static final int R_SQ_B=186;
    public static final int FILTER_NODE=221;
    public static final int OR=201;
    public static final int M3UA_DPC=100;
    public static final int SMPP_NP_LAND_MOBILE=58;
    public static final int NO_DR=160;
    public static final int MSG_TYPE_CONCATENATED=152;
    public static final int SMPP_NP_DATA_X121=56;
    public static final int SCCP_GT_CALLING_NP=93;
    public static final int HLR_MSISDN=109;
    public static final int CONVERT_SMPP=162;
    public static final int DIGITS=250;
    public static final int REMOTE_PORT=242;
    public static final int REGEX_BLOCK=218;
    public static final int GTE=210;
    public static final int ALWU=180;
    public static final int L_CR_B=188;
    public static final int SCCP_GT_CALLED_NAI=90;
    public static final int SMPP_DC_PICTOGRAM=82;
    public static final int SMPP_PASSWORD=13;
    public static final int F_M3UA=195;
    public static final int AND=213;
    public static final int SMPP_DC_ISO_2011_JP=83;
    public static final int SMPP_SM=35;
    public static final int LTE=209;
    public static final int ANNT=198;
    public static final int DNY=182;
    public static final int SCCP_GT_CALLING_WL=5;
    public static final int ALW=178;
    public static final int ML_COMMENT=253;
    public static final int FLOOD_MINUTE=169;
    public static final int GTI_TT=145;
    public static final int IN=174;
    public static final int IP=252;
    public static final int SMPP_TON_UNKNOWN=47;
    public static final int SMPP_ORIGINATOR_TON=15;
    public static final int COMMA=220;
    public static final int SMPP_ESM_MM_STORE_FORWARD=39;
    public static final int EQUAL=199;
    public static final int SMPP_NP_INTERNET_IP=62;
    public static final int HLR_RESULT_IMSI=113;
    public static final int SMS_MSG_TYPE=121;
    public static final int CONN_SC=247;
    public static final int SMPP_NP_ISDN_TELEPHONE=55;
    public static final int SMPP_PRIORITY_FLAG=25;
    public static final int TON_ALPHANUMERIC=127;
    public static final int GTI_TTNPE=146;
    public static final int PERCENT=205;
    public static final int SMPP_ORIGINATOR_ADDRESS=17;
    public static final int MAP_SCOA=96;
    public static final int CONVERT_SS7=161;
    public static final int SCCP_GT_CALLING_TT=89;
    public static final int SMPP_RECIPIENT_NP=19;
    public static final int CONN_RC=246;
    public static final int TON_NATIONAL=124;
    public static final int SMPP_TCP_SOURCE=10;
    public static final int FLOOD_MAX=164;
    public static final int R_PAREN=185;
    public static final int SMPP_ESM_MM_DATAGRAM=37;
    public static final int MAP_SCDA_WL=7;
    public static final int SPAM_UPDATE_LST=153;
    public static final int RULE_EVAL_POINTS=229;
    public static final int CONT=176;
    public static final int SMPP_RD_SMSCDR_FAILURE=66;
    public static final int SMPP_SM_LENGTH=34;
    public static final int SMPP_DC_UCS2=81;
    public static final int ASSIGN=200;
    public static final int SMPP_RD_SMSC_RECEIPT=28;
    public static final int FLOOD_HOUR=168;
    public static final int CONN_OPC=244;
    public static final int MODIFIER_LABEL=236;
    public static final int RULE_LABEL=232;
    public static final int SMPP_ESM_GF_SET_BOTH=46;
    public static final int SMPP_TON_NETWORK_SPECIFIC=50;

    // delegates
    // delegators


        public SmsFilterScriptParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public SmsFilterScriptParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return SmsFilterScriptParser.tokenNames; }
    public String getGrammarFileName() { return "/home/dfranusic/SmsFilterScript.g"; }


    public static class input_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "input"
    // /home/dfranusic/SmsFilterScript.g:330:1: input : ( filter )* EOF ;
    public final SmsFilterScriptParser.input_return input() throws RecognitionException {
        SmsFilterScriptParser.input_return retval = new SmsFilterScriptParser.input_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token EOF2=null;
        SmsFilterScriptParser.filter_return filter1 = null;


        Object EOF2_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:331:3: ( ( filter )* EOF )
            // /home/dfranusic/SmsFilterScript.g:331:5: ( filter )* EOF
            {
            root_0 = (Object)adaptor.nil();

            // /home/dfranusic/SmsFilterScript.g:331:5: ( filter )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( ((LA1_0>=F_MO && LA1_0<=F_M3UA)) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:331:6: filter
            	    {
            	    pushFollow(FOLLOW_filter_in_input3127);
            	    filter1=filter();

            	    state._fsp--;

            	    adaptor.addChild(root_0, filter1.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_input3131); 

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "input"

    public static class eval_nai_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_nai"
    // /home/dfranusic/SmsFilterScript.g:334:1: eval_nai : ( SCCP_GT_CALLED_NAI | SCCP_GT_CALLING_NAI );
    public final SmsFilterScriptParser.eval_nai_return eval_nai() throws RecognitionException {
        SmsFilterScriptParser.eval_nai_return retval = new SmsFilterScriptParser.eval_nai_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set3=null;

        Object set3_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:335:3: ( SCCP_GT_CALLED_NAI | SCCP_GT_CALLING_NAI )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set3=(Token)input.LT(1);
            if ( (input.LA(1)>=SCCP_GT_CALLED_NAI && input.LA(1)<=SCCP_GT_CALLING_NAI) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set3));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_nai"

    public static class smpp_eval_np_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_np"
    // /home/dfranusic/SmsFilterScript.g:339:1: smpp_eval_np : ( SMPP_ORIGINATOR_NP | SMPP_RECIPIENT_NP );
    public final SmsFilterScriptParser.smpp_eval_np_return smpp_eval_np() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_np_return retval = new SmsFilterScriptParser.smpp_eval_np_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set4=null;

        Object set4_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:340:3: ( SMPP_ORIGINATOR_NP | SMPP_RECIPIENT_NP )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set4=(Token)input.LT(1);
            if ( input.LA(1)==SMPP_ORIGINATOR_NP||input.LA(1)==SMPP_RECIPIENT_NP ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set4));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_np"

    public static class eval_np_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_np"
    // /home/dfranusic/SmsFilterScript.g:345:1: eval_np : ( SCCP_GT_CALLED_NP | SCCP_GT_CALLING_NP );
    public final SmsFilterScriptParser.eval_np_return eval_np() throws RecognitionException {
        SmsFilterScriptParser.eval_np_return retval = new SmsFilterScriptParser.eval_np_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set5=null;

        Object set5_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:346:3: ( SCCP_GT_CALLED_NP | SCCP_GT_CALLING_NP )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set5=(Token)input.LT(1);
            if ( (input.LA(1)>=SCCP_GT_CALLED_NP && input.LA(1)<=SCCP_GT_CALLING_NP) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set5));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_np"

    public static class eval_gti_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_gti"
    // /home/dfranusic/SmsFilterScript.g:350:1: eval_gti : ( SCCP_GT_CALLED_GTI | SCCP_GT_CALLING_GTI );
    public final SmsFilterScriptParser.eval_gti_return eval_gti() throws RecognitionException {
        SmsFilterScriptParser.eval_gti_return retval = new SmsFilterScriptParser.eval_gti_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set6=null;

        Object set6_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:351:3: ( SCCP_GT_CALLED_GTI | SCCP_GT_CALLING_GTI )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set6=(Token)input.LT(1);
            if ( (input.LA(1)>=SCCP_GT_CALLED_GTI && input.LA(1)<=SCCP_GT_CALLING_GTI) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set6));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_gti"

    public static class eval_dcs_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_dcs"
    // /home/dfranusic/SmsFilterScript.g:355:1: eval_dcs : SMS_TPDU_DCS ;
    public final SmsFilterScriptParser.eval_dcs_return eval_dcs() throws RecognitionException {
        SmsFilterScriptParser.eval_dcs_return retval = new SmsFilterScriptParser.eval_dcs_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMS_TPDU_DCS7=null;

        Object SMS_TPDU_DCS7_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:356:3: ( SMS_TPDU_DCS )
            // /home/dfranusic/SmsFilterScript.g:356:5: SMS_TPDU_DCS
            {
            root_0 = (Object)adaptor.nil();

            SMS_TPDU_DCS7=(Token)match(input,SMS_TPDU_DCS,FOLLOW_SMS_TPDU_DCS_in_eval_dcs3222); 
            SMS_TPDU_DCS7_tree = (Object)adaptor.create(SMS_TPDU_DCS7);
            adaptor.addChild(root_0, SMS_TPDU_DCS7_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_dcs"

    public static class smpp_eval_dcs_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_dcs"
    // /home/dfranusic/SmsFilterScript.g:359:1: smpp_eval_dcs : SMPP_DATA_CODING ;
    public final SmsFilterScriptParser.smpp_eval_dcs_return smpp_eval_dcs() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_dcs_return retval = new SmsFilterScriptParser.smpp_eval_dcs_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_DATA_CODING8=null;

        Object SMPP_DATA_CODING8_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:360:3: ( SMPP_DATA_CODING )
            // /home/dfranusic/SmsFilterScript.g:360:5: SMPP_DATA_CODING
            {
            root_0 = (Object)adaptor.nil();

            SMPP_DATA_CODING8=(Token)match(input,SMPP_DATA_CODING,FOLLOW_SMPP_DATA_CODING_in_smpp_eval_dcs3235); 
            SMPP_DATA_CODING8_tree = (Object)adaptor.create(SMPP_DATA_CODING8);
            adaptor.addChild(root_0, SMPP_DATA_CODING8_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_dcs"

    public static class smpp_eval_esm_mm_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_esm_mm"
    // /home/dfranusic/SmsFilterScript.g:363:1: smpp_eval_esm_mm : SMPP_ESM_MESSAGE_MODE ;
    public final SmsFilterScriptParser.smpp_eval_esm_mm_return smpp_eval_esm_mm() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_esm_mm_return retval = new SmsFilterScriptParser.smpp_eval_esm_mm_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_ESM_MESSAGE_MODE9=null;

        Object SMPP_ESM_MESSAGE_MODE9_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:364:3: ( SMPP_ESM_MESSAGE_MODE )
            // /home/dfranusic/SmsFilterScript.g:364:5: SMPP_ESM_MESSAGE_MODE
            {
            root_0 = (Object)adaptor.nil();

            SMPP_ESM_MESSAGE_MODE9=(Token)match(input,SMPP_ESM_MESSAGE_MODE,FOLLOW_SMPP_ESM_MESSAGE_MODE_in_smpp_eval_esm_mm3248); 
            SMPP_ESM_MESSAGE_MODE9_tree = (Object)adaptor.create(SMPP_ESM_MESSAGE_MODE9);
            adaptor.addChild(root_0, SMPP_ESM_MESSAGE_MODE9_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_esm_mm"

    public static class smpp_eval_esm_mt_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_esm_mt"
    // /home/dfranusic/SmsFilterScript.g:367:1: smpp_eval_esm_mt : SMPP_ESM_MESSAGE_TYPE ;
    public final SmsFilterScriptParser.smpp_eval_esm_mt_return smpp_eval_esm_mt() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_esm_mt_return retval = new SmsFilterScriptParser.smpp_eval_esm_mt_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_ESM_MESSAGE_TYPE10=null;

        Object SMPP_ESM_MESSAGE_TYPE10_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:368:3: ( SMPP_ESM_MESSAGE_TYPE )
            // /home/dfranusic/SmsFilterScript.g:368:5: SMPP_ESM_MESSAGE_TYPE
            {
            root_0 = (Object)adaptor.nil();

            SMPP_ESM_MESSAGE_TYPE10=(Token)match(input,SMPP_ESM_MESSAGE_TYPE,FOLLOW_SMPP_ESM_MESSAGE_TYPE_in_smpp_eval_esm_mt3261); 
            SMPP_ESM_MESSAGE_TYPE10_tree = (Object)adaptor.create(SMPP_ESM_MESSAGE_TYPE10);
            adaptor.addChild(root_0, SMPP_ESM_MESSAGE_TYPE10_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_esm_mt"

    public static class smpp_eval_esm_gf_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_esm_gf"
    // /home/dfranusic/SmsFilterScript.g:371:1: smpp_eval_esm_gf : SMPP_ESM_GSM_FEATURES ;
    public final SmsFilterScriptParser.smpp_eval_esm_gf_return smpp_eval_esm_gf() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_esm_gf_return retval = new SmsFilterScriptParser.smpp_eval_esm_gf_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_ESM_GSM_FEATURES11=null;

        Object SMPP_ESM_GSM_FEATURES11_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:372:3: ( SMPP_ESM_GSM_FEATURES )
            // /home/dfranusic/SmsFilterScript.g:372:5: SMPP_ESM_GSM_FEATURES
            {
            root_0 = (Object)adaptor.nil();

            SMPP_ESM_GSM_FEATURES11=(Token)match(input,SMPP_ESM_GSM_FEATURES,FOLLOW_SMPP_ESM_GSM_FEATURES_in_smpp_eval_esm_gf3274); 
            SMPP_ESM_GSM_FEATURES11_tree = (Object)adaptor.create(SMPP_ESM_GSM_FEATURES11);
            adaptor.addChild(root_0, SMPP_ESM_GSM_FEATURES11_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_esm_gf"

    public static class smpp_eval_rd_smscdr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_rd_smscdr"
    // /home/dfranusic/SmsFilterScript.g:375:1: smpp_eval_rd_smscdr : SMPP_RD_SMSC_RECEIPT ;
    public final SmsFilterScriptParser.smpp_eval_rd_smscdr_return smpp_eval_rd_smscdr() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_rd_smscdr_return retval = new SmsFilterScriptParser.smpp_eval_rd_smscdr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_RD_SMSC_RECEIPT12=null;

        Object SMPP_RD_SMSC_RECEIPT12_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:376:3: ( SMPP_RD_SMSC_RECEIPT )
            // /home/dfranusic/SmsFilterScript.g:376:5: SMPP_RD_SMSC_RECEIPT
            {
            root_0 = (Object)adaptor.nil();

            SMPP_RD_SMSC_RECEIPT12=(Token)match(input,SMPP_RD_SMSC_RECEIPT,FOLLOW_SMPP_RD_SMSC_RECEIPT_in_smpp_eval_rd_smscdr3287); 
            SMPP_RD_SMSC_RECEIPT12_tree = (Object)adaptor.create(SMPP_RD_SMSC_RECEIPT12);
            adaptor.addChild(root_0, SMPP_RD_SMSC_RECEIPT12_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_rd_smscdr"

    public static class smpp_eval_rd_smeoa_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_rd_smeoa"
    // /home/dfranusic/SmsFilterScript.g:379:1: smpp_eval_rd_smeoa : SMPP_RD_SME_ACK ;
    public final SmsFilterScriptParser.smpp_eval_rd_smeoa_return smpp_eval_rd_smeoa() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_rd_smeoa_return retval = new SmsFilterScriptParser.smpp_eval_rd_smeoa_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_RD_SME_ACK13=null;

        Object SMPP_RD_SME_ACK13_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:380:3: ( SMPP_RD_SME_ACK )
            // /home/dfranusic/SmsFilterScript.g:380:5: SMPP_RD_SME_ACK
            {
            root_0 = (Object)adaptor.nil();

            SMPP_RD_SME_ACK13=(Token)match(input,SMPP_RD_SME_ACK,FOLLOW_SMPP_RD_SME_ACK_in_smpp_eval_rd_smeoa3300); 
            SMPP_RD_SME_ACK13_tree = (Object)adaptor.create(SMPP_RD_SME_ACK13);
            adaptor.addChild(root_0, SMPP_RD_SME_ACK13_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_rd_smeoa"

    public static class smpp_eval_rd_in_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_rd_in"
    // /home/dfranusic/SmsFilterScript.g:383:1: smpp_eval_rd_in : SMPP_RD_INTERMEDIATE_NOTIFICATION ;
    public final SmsFilterScriptParser.smpp_eval_rd_in_return smpp_eval_rd_in() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_rd_in_return retval = new SmsFilterScriptParser.smpp_eval_rd_in_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMPP_RD_INTERMEDIATE_NOTIFICATION14=null;

        Object SMPP_RD_INTERMEDIATE_NOTIFICATION14_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:384:3: ( SMPP_RD_INTERMEDIATE_NOTIFICATION )
            // /home/dfranusic/SmsFilterScript.g:384:5: SMPP_RD_INTERMEDIATE_NOTIFICATION
            {
            root_0 = (Object)adaptor.nil();

            SMPP_RD_INTERMEDIATE_NOTIFICATION14=(Token)match(input,SMPP_RD_INTERMEDIATE_NOTIFICATION,FOLLOW_SMPP_RD_INTERMEDIATE_NOTIFICATION_in_smpp_eval_rd_in3313); 
            SMPP_RD_INTERMEDIATE_NOTIFICATION14_tree = (Object)adaptor.create(SMPP_RD_INTERMEDIATE_NOTIFICATION14);
            adaptor.addChild(root_0, SMPP_RD_INTERMEDIATE_NOTIFICATION14_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_rd_in"

    public static class eval_string_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_string"
    // /home/dfranusic/SmsFilterScript.g:389:1: eval_string : ( SCCP_GT_CALLED_ADDRESS | SCCP_GT_CALLING_ADDRESS | MAP_SCOA | MAP_SCDA | MAP_IMSI | MAP_MSISDN | SMS_TPDU_ORIGINATING | SMS_TPDU_DESTINATION | SMS_TPDU_UD | HLR_IMSI | HLR_MSISDN | HLR_NNN | HLR_ANNN | HLR_SCA | HLR_RESULT_IMSI | HLR_RESULT_NNN | HLR_RESULT_ANNN | SMPP_IP_SOURCE | SMPP_IP_DESTINATION | SMPP_SYSTEM_ID | SMPP_PASSWORD | SMPP_SERVICE_TYPE | SMPP_ORIGINATOR_ADDRESS | SMPP_RECIPIENT_ADDRESS | SMPP_SM );
    public final SmsFilterScriptParser.eval_string_return eval_string() throws RecognitionException {
        SmsFilterScriptParser.eval_string_return retval = new SmsFilterScriptParser.eval_string_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set15=null;

        Object set15_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:390:3: ( SCCP_GT_CALLED_ADDRESS | SCCP_GT_CALLING_ADDRESS | MAP_SCOA | MAP_SCDA | MAP_IMSI | MAP_MSISDN | SMS_TPDU_ORIGINATING | SMS_TPDU_DESTINATION | SMS_TPDU_UD | HLR_IMSI | HLR_MSISDN | HLR_NNN | HLR_ANNN | HLR_SCA | HLR_RESULT_IMSI | HLR_RESULT_NNN | HLR_RESULT_ANNN | SMPP_IP_SOURCE | SMPP_IP_DESTINATION | SMPP_SYSTEM_ID | SMPP_PASSWORD | SMPP_SERVICE_TYPE | SMPP_ORIGINATOR_ADDRESS | SMPP_RECIPIENT_ADDRESS | SMPP_SM )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set15=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_IP_SOURCE && input.LA(1)<=SMPP_IP_DESTINATION)||(input.LA(1)>=SMPP_SYSTEM_ID && input.LA(1)<=SMPP_SERVICE_TYPE)||input.LA(1)==SMPP_ORIGINATOR_ADDRESS||input.LA(1)==SMPP_RECIPIENT_ADDRESS||input.LA(1)==SMPP_SM||(input.LA(1)>=SCCP_GT_CALLED_ADDRESS && input.LA(1)<=SCCP_GT_CALLING_ADDRESS)||(input.LA(1)>=MAP_SCOA && input.LA(1)<=MAP_MSISDN)||input.LA(1)==SMS_TPDU_ORIGINATING||input.LA(1)==SMS_TPDU_DESTINATION||input.LA(1)==SMS_TPDU_UD||(input.LA(1)>=HLR_IMSI && input.LA(1)<=HLR_RESULT_ANNN) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set15));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_string"

    public static class eval_number_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_number"
    // /home/dfranusic/SmsFilterScript.g:420:1: eval_number : ( M3UA_DPC | M3UA_OPC | DICT_SMS_TPDU_UD | SPAM_SMS_TPDU_UD | RP_SMS_TPDU_UD | QUARANTINE_SMS_TPDU_UD | MD5_SMS_TPDU_UD | SCCP_GT_CALLED_TT | SCCP_GT_CALLING_TT | SMPP_TCP_SOURCE | SMPP_TCP_DESTINATION | SMPP_DELIVERY_TIME | SMPP_VALIDITY_PERIOD | SMPP_SM_DEFAULT_MSG_ID | SMPP_SM_LENGTH | SMPP_PROTOCOL_ID );
    public final SmsFilterScriptParser.eval_number_return eval_number() throws RecognitionException {
        SmsFilterScriptParser.eval_number_return retval = new SmsFilterScriptParser.eval_number_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set16=null;

        Object set16_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:421:3: ( M3UA_DPC | M3UA_OPC | DICT_SMS_TPDU_UD | SPAM_SMS_TPDU_UD | RP_SMS_TPDU_UD | QUARANTINE_SMS_TPDU_UD | MD5_SMS_TPDU_UD | SCCP_GT_CALLED_TT | SCCP_GT_CALLING_TT | SMPP_TCP_SOURCE | SMPP_TCP_DESTINATION | SMPP_DELIVERY_TIME | SMPP_VALIDITY_PERIOD | SMPP_SM_DEFAULT_MSG_ID | SMPP_SM_LENGTH | SMPP_PROTOCOL_ID )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set16=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_TCP_SOURCE && input.LA(1)<=SMPP_TCP_DESTINATION)||input.LA(1)==SMPP_PROTOCOL_ID||(input.LA(1)>=SMPP_DELIVERY_TIME && input.LA(1)<=SMPP_VALIDITY_PERIOD)||(input.LA(1)>=SMPP_SM_DEFAULT_MSG_ID && input.LA(1)<=SMPP_SM_LENGTH)||(input.LA(1)>=SCCP_GT_CALLED_TT && input.LA(1)<=SCCP_GT_CALLING_TT)||(input.LA(1)>=M3UA_DPC && input.LA(1)<=M3UA_OPC)||input.LA(1)==DICT_SMS_TPDU_UD||(input.LA(1)>=SPAM_SMS_TPDU_UD && input.LA(1)<=MD5_SMS_TPDU_UD) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set16));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_number"

    public static class eval_ton_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_ton"
    // /home/dfranusic/SmsFilterScript.g:440:1: eval_ton : ( SMS_TPDU_ORIGINATING_ENC | SMS_TPDU_DESTINATION_ENC );
    public final SmsFilterScriptParser.eval_ton_return eval_ton() throws RecognitionException {
        SmsFilterScriptParser.eval_ton_return retval = new SmsFilterScriptParser.eval_ton_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set17=null;

        Object set17_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:441:3: ( SMS_TPDU_ORIGINATING_ENC | SMS_TPDU_DESTINATION_ENC )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set17=(Token)input.LT(1);
            if ( input.LA(1)==SMS_TPDU_ORIGINATING_ENC||input.LA(1)==SMS_TPDU_DESTINATION_ENC ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set17));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_ton"

    public static class smpp_eval_ton_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_eval_ton"
    // /home/dfranusic/SmsFilterScript.g:445:1: smpp_eval_ton : ( SMPP_ORIGINATOR_TON | SMPP_RECIPIENT_TON );
    public final SmsFilterScriptParser.smpp_eval_ton_return smpp_eval_ton() throws RecognitionException {
        SmsFilterScriptParser.smpp_eval_ton_return retval = new SmsFilterScriptParser.smpp_eval_ton_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set18=null;

        Object set18_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:446:3: ( SMPP_ORIGINATOR_TON | SMPP_RECIPIENT_TON )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set18=(Token)input.LT(1);
            if ( input.LA(1)==SMPP_ORIGINATOR_TON||input.LA(1)==SMPP_RECIPIENT_TON ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set18));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_eval_ton"

    public static class eval_msg_type_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval_msg_type"
    // /home/dfranusic/SmsFilterScript.g:451:1: eval_msg_type : SMS_MSG_TYPE ;
    public final SmsFilterScriptParser.eval_msg_type_return eval_msg_type() throws RecognitionException {
        SmsFilterScriptParser.eval_msg_type_return retval = new SmsFilterScriptParser.eval_msg_type_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token SMS_MSG_TYPE19=null;

        Object SMS_MSG_TYPE19_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:452:3: ( SMS_MSG_TYPE )
            // /home/dfranusic/SmsFilterScript.g:452:5: SMS_MSG_TYPE
            {
            root_0 = (Object)adaptor.nil();

            SMS_MSG_TYPE19=(Token)match(input,SMS_MSG_TYPE,FOLLOW_SMS_MSG_TYPE_in_eval_msg_type3640); 
            SMS_MSG_TYPE19_tree = (Object)adaptor.create(SMS_MSG_TYPE19);
            adaptor.addChild(root_0, SMS_MSG_TYPE19_tree);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval_msg_type"

    public static class evalsimple_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "evalsimple"
    // /home/dfranusic/SmsFilterScript.g:455:1: evalsimple : ( L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* (b= SCCP_GT_CALLED_WL | b= SCCP_GT_CALLING_WL | b= MAP_SCOA_WL | b= MAP_SCDA_WL ) ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $b) | L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $bstr) | L_PAREN ( WS )* w1= eval_dcs ( WS )* a= comparison ( WS )* br1= dcs ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w1 $a $br1) | L_PAREN ( WS )* w2= eval_number ( WS )* a= comparison ( WS )* br2= DIGITS ( PERCENT )* ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w2 $a $br2) | L_PAREN ( WS )* w3= eval_ton ( WS )* a= comparison ( WS )* br3= typeofnum ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w3 $a $br3) | L_PAREN ( WS )* w4= eval_msg_type ( WS )* a= comparison ( WS )* br4= msgtype ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w4 $a $br4) | L_PAREN ( WS )* w5= eval_string ( WS )* a= comparison ( WS )* br5= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w5 $a $br5) | L_PAREN ( WS )* w6= eval_number ( WS )* a= comparison ( WS )* br6= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w6 $a $br6) | L_PAREN ( WS )* w7= eval_nai ( WS )* a= comparison ( WS )* br7= nai ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w7 $a $br7) | L_PAREN ( WS )* w8= eval_np ( WS )* a= comparison ( WS )* br8= np ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w8 $a $br8) | L_PAREN ( WS )* w9= eval_gti ( WS )* a= comparison ( WS )* br9= gti ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w9 $a $br9) | L_PAREN ( WS )* w10= smpp_eval_np ( WS )* a= comparison ( WS )* br10= smpp_np ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w10 $a $br10) | L_PAREN ( WS )* w11= smpp_eval_ton ( WS )* a= comparison ( WS )* br11= smpp_typeofnum ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w11 $a $br11) | L_PAREN ( WS )* w12= smpp_eval_esm_mm ( WS )* a= comparison ( WS )* br12= smpp_esm_mm ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w12 $a $br12) | L_PAREN ( WS )* w13= smpp_eval_esm_mt ( WS )* a= comparison ( WS )* br13= smpp_esm_mt ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w13 $a $br13) | L_PAREN ( WS )* w14= smpp_eval_esm_gf ( WS )* a= comparison ( WS )* br14= smpp_esm_gf ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w14 $a $br14) | L_PAREN ( WS )* w15= smpp_eval_rd_smscdr ( WS )* a= comparison ( WS )* br15= smpp_rd_smscdr ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w15 $a $br15) | L_PAREN ( WS )* w16= smpp_eval_rd_smeoa ( WS )* a= comparison ( WS )* br16= smpp_rd_smeoa ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w16 $a $br16) | L_PAREN ( WS )* w17= smpp_eval_rd_in ( WS )* a= comparison ( WS )* br17= smpp_rd_in ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w17 $a $br17) | L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fvar2= floodmethod ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $fvar $a $fvar2) | L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fdigs= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $fvar $a $fdigs) | L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* lvar= listmethod ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $lvar) );
    public final SmsFilterScriptParser.evalsimple_return evalsimple() throws RecognitionException {
        SmsFilterScriptParser.evalsimple_return retval = new SmsFilterScriptParser.evalsimple_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token b=null;
        Token bstr=null;
        Token br2=null;
        Token fdigs=null;
        Token L_PAREN20=null;
        Token WS21=null;
        Token WS22=null;
        Token WS23=null;
        Token WS24=null;
        Token R_PAREN25=null;
        Token L_PAREN26=null;
        Token WS27=null;
        Token WS28=null;
        Token WS29=null;
        Token WS30=null;
        Token R_PAREN31=null;
        Token L_PAREN32=null;
        Token WS33=null;
        Token WS34=null;
        Token WS35=null;
        Token WS36=null;
        Token R_PAREN37=null;
        Token L_PAREN38=null;
        Token WS39=null;
        Token WS40=null;
        Token WS41=null;
        Token PERCENT42=null;
        Token WS43=null;
        Token R_PAREN44=null;
        Token L_PAREN45=null;
        Token WS46=null;
        Token WS47=null;
        Token WS48=null;
        Token WS49=null;
        Token R_PAREN50=null;
        Token L_PAREN51=null;
        Token WS52=null;
        Token WS53=null;
        Token WS54=null;
        Token WS55=null;
        Token R_PAREN56=null;
        Token L_PAREN57=null;
        Token WS58=null;
        Token WS59=null;
        Token WS60=null;
        Token WS61=null;
        Token R_PAREN62=null;
        Token L_PAREN63=null;
        Token WS64=null;
        Token WS65=null;
        Token WS66=null;
        Token WS67=null;
        Token R_PAREN68=null;
        Token L_PAREN69=null;
        Token WS70=null;
        Token WS71=null;
        Token WS72=null;
        Token WS73=null;
        Token R_PAREN74=null;
        Token L_PAREN75=null;
        Token WS76=null;
        Token WS77=null;
        Token WS78=null;
        Token WS79=null;
        Token R_PAREN80=null;
        Token L_PAREN81=null;
        Token WS82=null;
        Token WS83=null;
        Token WS84=null;
        Token WS85=null;
        Token R_PAREN86=null;
        Token L_PAREN87=null;
        Token WS88=null;
        Token WS89=null;
        Token WS90=null;
        Token WS91=null;
        Token R_PAREN92=null;
        Token L_PAREN93=null;
        Token WS94=null;
        Token WS95=null;
        Token WS96=null;
        Token WS97=null;
        Token R_PAREN98=null;
        Token L_PAREN99=null;
        Token WS100=null;
        Token WS101=null;
        Token WS102=null;
        Token WS103=null;
        Token R_PAREN104=null;
        Token L_PAREN105=null;
        Token WS106=null;
        Token WS107=null;
        Token WS108=null;
        Token WS109=null;
        Token R_PAREN110=null;
        Token L_PAREN111=null;
        Token WS112=null;
        Token WS113=null;
        Token WS114=null;
        Token WS115=null;
        Token R_PAREN116=null;
        Token L_PAREN117=null;
        Token WS118=null;
        Token WS119=null;
        Token WS120=null;
        Token WS121=null;
        Token R_PAREN122=null;
        Token L_PAREN123=null;
        Token WS124=null;
        Token WS125=null;
        Token WS126=null;
        Token WS127=null;
        Token R_PAREN128=null;
        Token L_PAREN129=null;
        Token WS130=null;
        Token WS131=null;
        Token WS132=null;
        Token WS133=null;
        Token R_PAREN134=null;
        Token L_PAREN135=null;
        Token WS136=null;
        Token WS137=null;
        Token WS138=null;
        Token WS139=null;
        Token R_PAREN140=null;
        Token L_PAREN141=null;
        Token WS142=null;
        Token WS143=null;
        Token WS144=null;
        Token WS145=null;
        Token R_PAREN146=null;
        Token L_PAREN147=null;
        Token WS148=null;
        Token WS149=null;
        Token WS150=null;
        Token WS151=null;
        Token R_PAREN152=null;
        SmsFilterScriptParser.eval_string_return w = null;

        SmsFilterScriptParser.comparison_return a = null;

        SmsFilterScriptParser.eval_dcs_return w1 = null;

        SmsFilterScriptParser.dcs_return br1 = null;

        SmsFilterScriptParser.eval_number_return w2 = null;

        SmsFilterScriptParser.eval_ton_return w3 = null;

        SmsFilterScriptParser.typeofnum_return br3 = null;

        SmsFilterScriptParser.eval_msg_type_return w4 = null;

        SmsFilterScriptParser.msgtype_return br4 = null;

        SmsFilterScriptParser.eval_string_return w5 = null;

        SmsFilterScriptParser.eval_string_return br5 = null;

        SmsFilterScriptParser.eval_number_return w6 = null;

        SmsFilterScriptParser.eval_number_return br6 = null;

        SmsFilterScriptParser.eval_nai_return w7 = null;

        SmsFilterScriptParser.nai_return br7 = null;

        SmsFilterScriptParser.eval_np_return w8 = null;

        SmsFilterScriptParser.np_return br8 = null;

        SmsFilterScriptParser.eval_gti_return w9 = null;

        SmsFilterScriptParser.gti_return br9 = null;

        SmsFilterScriptParser.smpp_eval_np_return w10 = null;

        SmsFilterScriptParser.smpp_np_return br10 = null;

        SmsFilterScriptParser.smpp_eval_ton_return w11 = null;

        SmsFilterScriptParser.smpp_typeofnum_return br11 = null;

        SmsFilterScriptParser.smpp_eval_esm_mm_return w12 = null;

        SmsFilterScriptParser.smpp_esm_mm_return br12 = null;

        SmsFilterScriptParser.smpp_eval_esm_mt_return w13 = null;

        SmsFilterScriptParser.smpp_esm_mt_return br13 = null;

        SmsFilterScriptParser.smpp_eval_esm_gf_return w14 = null;

        SmsFilterScriptParser.smpp_esm_gf_return br14 = null;

        SmsFilterScriptParser.smpp_eval_rd_smscdr_return w15 = null;

        SmsFilterScriptParser.smpp_rd_smscdr_return br15 = null;

        SmsFilterScriptParser.smpp_eval_rd_smeoa_return w16 = null;

        SmsFilterScriptParser.smpp_rd_smeoa_return br16 = null;

        SmsFilterScriptParser.smpp_eval_rd_in_return w17 = null;

        SmsFilterScriptParser.smpp_rd_in_return br17 = null;

        SmsFilterScriptParser.floodmethod_return fvar = null;

        SmsFilterScriptParser.floodmethod_return fvar2 = null;

        SmsFilterScriptParser.listmethod_return lvar = null;


        Object b_tree=null;
        Object bstr_tree=null;
        Object br2_tree=null;
        Object fdigs_tree=null;
        Object L_PAREN20_tree=null;
        Object WS21_tree=null;
        Object WS22_tree=null;
        Object WS23_tree=null;
        Object WS24_tree=null;
        Object R_PAREN25_tree=null;
        Object L_PAREN26_tree=null;
        Object WS27_tree=null;
        Object WS28_tree=null;
        Object WS29_tree=null;
        Object WS30_tree=null;
        Object R_PAREN31_tree=null;
        Object L_PAREN32_tree=null;
        Object WS33_tree=null;
        Object WS34_tree=null;
        Object WS35_tree=null;
        Object WS36_tree=null;
        Object R_PAREN37_tree=null;
        Object L_PAREN38_tree=null;
        Object WS39_tree=null;
        Object WS40_tree=null;
        Object WS41_tree=null;
        Object PERCENT42_tree=null;
        Object WS43_tree=null;
        Object R_PAREN44_tree=null;
        Object L_PAREN45_tree=null;
        Object WS46_tree=null;
        Object WS47_tree=null;
        Object WS48_tree=null;
        Object WS49_tree=null;
        Object R_PAREN50_tree=null;
        Object L_PAREN51_tree=null;
        Object WS52_tree=null;
        Object WS53_tree=null;
        Object WS54_tree=null;
        Object WS55_tree=null;
        Object R_PAREN56_tree=null;
        Object L_PAREN57_tree=null;
        Object WS58_tree=null;
        Object WS59_tree=null;
        Object WS60_tree=null;
        Object WS61_tree=null;
        Object R_PAREN62_tree=null;
        Object L_PAREN63_tree=null;
        Object WS64_tree=null;
        Object WS65_tree=null;
        Object WS66_tree=null;
        Object WS67_tree=null;
        Object R_PAREN68_tree=null;
        Object L_PAREN69_tree=null;
        Object WS70_tree=null;
        Object WS71_tree=null;
        Object WS72_tree=null;
        Object WS73_tree=null;
        Object R_PAREN74_tree=null;
        Object L_PAREN75_tree=null;
        Object WS76_tree=null;
        Object WS77_tree=null;
        Object WS78_tree=null;
        Object WS79_tree=null;
        Object R_PAREN80_tree=null;
        Object L_PAREN81_tree=null;
        Object WS82_tree=null;
        Object WS83_tree=null;
        Object WS84_tree=null;
        Object WS85_tree=null;
        Object R_PAREN86_tree=null;
        Object L_PAREN87_tree=null;
        Object WS88_tree=null;
        Object WS89_tree=null;
        Object WS90_tree=null;
        Object WS91_tree=null;
        Object R_PAREN92_tree=null;
        Object L_PAREN93_tree=null;
        Object WS94_tree=null;
        Object WS95_tree=null;
        Object WS96_tree=null;
        Object WS97_tree=null;
        Object R_PAREN98_tree=null;
        Object L_PAREN99_tree=null;
        Object WS100_tree=null;
        Object WS101_tree=null;
        Object WS102_tree=null;
        Object WS103_tree=null;
        Object R_PAREN104_tree=null;
        Object L_PAREN105_tree=null;
        Object WS106_tree=null;
        Object WS107_tree=null;
        Object WS108_tree=null;
        Object WS109_tree=null;
        Object R_PAREN110_tree=null;
        Object L_PAREN111_tree=null;
        Object WS112_tree=null;
        Object WS113_tree=null;
        Object WS114_tree=null;
        Object WS115_tree=null;
        Object R_PAREN116_tree=null;
        Object L_PAREN117_tree=null;
        Object WS118_tree=null;
        Object WS119_tree=null;
        Object WS120_tree=null;
        Object WS121_tree=null;
        Object R_PAREN122_tree=null;
        Object L_PAREN123_tree=null;
        Object WS124_tree=null;
        Object WS125_tree=null;
        Object WS126_tree=null;
        Object WS127_tree=null;
        Object R_PAREN128_tree=null;
        Object L_PAREN129_tree=null;
        Object WS130_tree=null;
        Object WS131_tree=null;
        Object WS132_tree=null;
        Object WS133_tree=null;
        Object R_PAREN134_tree=null;
        Object L_PAREN135_tree=null;
        Object WS136_tree=null;
        Object WS137_tree=null;
        Object WS138_tree=null;
        Object WS139_tree=null;
        Object R_PAREN140_tree=null;
        Object L_PAREN141_tree=null;
        Object WS142_tree=null;
        Object WS143_tree=null;
        Object WS144_tree=null;
        Object WS145_tree=null;
        Object R_PAREN146_tree=null;
        Object L_PAREN147_tree=null;
        Object WS148_tree=null;
        Object WS149_tree=null;
        Object WS150_tree=null;
        Object WS151_tree=null;
        Object R_PAREN152_tree=null;
        RewriteRuleTokenStream stream_SCCP_GT_CALLING_WL=new RewriteRuleTokenStream(adaptor,"token SCCP_GT_CALLING_WL");
        RewriteRuleTokenStream stream_MAP_SCOA_WL=new RewriteRuleTokenStream(adaptor,"token MAP_SCOA_WL");
        RewriteRuleTokenStream stream_MAP_SCDA_WL=new RewriteRuleTokenStream(adaptor,"token MAP_SCDA_WL");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_PERCENT=new RewriteRuleTokenStream(adaptor,"token PERCENT");
        RewriteRuleTokenStream stream_SCCP_GT_CALLED_WL=new RewriteRuleTokenStream(adaptor,"token SCCP_GT_CALLED_WL");
        RewriteRuleTokenStream stream_STRINGLITERAL=new RewriteRuleTokenStream(adaptor,"token STRINGLITERAL");
        RewriteRuleTokenStream stream_DIGITS=new RewriteRuleTokenStream(adaptor,"token DIGITS");
        RewriteRuleTokenStream stream_R_PAREN=new RewriteRuleTokenStream(adaptor,"token R_PAREN");
        RewriteRuleTokenStream stream_L_PAREN=new RewriteRuleTokenStream(adaptor,"token L_PAREN");
        RewriteRuleSubtreeStream stream_smpp_eval_rd_smeoa=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_rd_smeoa");
        RewriteRuleSubtreeStream stream_smpp_rd_smscdr=new RewriteRuleSubtreeStream(adaptor,"rule smpp_rd_smscdr");
        RewriteRuleSubtreeStream stream_np=new RewriteRuleSubtreeStream(adaptor,"rule np");
        RewriteRuleSubtreeStream stream_eval_gti=new RewriteRuleSubtreeStream(adaptor,"rule eval_gti");
        RewriteRuleSubtreeStream stream_smpp_rd_smeoa=new RewriteRuleSubtreeStream(adaptor,"rule smpp_rd_smeoa");
        RewriteRuleSubtreeStream stream_nai=new RewriteRuleSubtreeStream(adaptor,"rule nai");
        RewriteRuleSubtreeStream stream_smpp_eval_ton=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_ton");
        RewriteRuleSubtreeStream stream_smpp_eval_esm_mm=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_esm_mm");
        RewriteRuleSubtreeStream stream_eval_dcs=new RewriteRuleSubtreeStream(adaptor,"rule eval_dcs");
        RewriteRuleSubtreeStream stream_typeofnum=new RewriteRuleSubtreeStream(adaptor,"rule typeofnum");
        RewriteRuleSubtreeStream stream_eval_number=new RewriteRuleSubtreeStream(adaptor,"rule eval_number");
        RewriteRuleSubtreeStream stream_smpp_esm_mm=new RewriteRuleSubtreeStream(adaptor,"rule smpp_esm_mm");
        RewriteRuleSubtreeStream stream_msgtype=new RewriteRuleSubtreeStream(adaptor,"rule msgtype");
        RewriteRuleSubtreeStream stream_floodmethod=new RewriteRuleSubtreeStream(adaptor,"rule floodmethod");
        RewriteRuleSubtreeStream stream_smpp_rd_in=new RewriteRuleSubtreeStream(adaptor,"rule smpp_rd_in");
        RewriteRuleSubtreeStream stream_eval_string=new RewriteRuleSubtreeStream(adaptor,"rule eval_string");
        RewriteRuleSubtreeStream stream_eval_msg_type=new RewriteRuleSubtreeStream(adaptor,"rule eval_msg_type");
        RewriteRuleSubtreeStream stream_listmethod=new RewriteRuleSubtreeStream(adaptor,"rule listmethod");
        RewriteRuleSubtreeStream stream_eval_np=new RewriteRuleSubtreeStream(adaptor,"rule eval_np");
        RewriteRuleSubtreeStream stream_smpp_esm_mt=new RewriteRuleSubtreeStream(adaptor,"rule smpp_esm_mt");
        RewriteRuleSubtreeStream stream_smpp_eval_np=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_np");
        RewriteRuleSubtreeStream stream_smpp_typeofnum=new RewriteRuleSubtreeStream(adaptor,"rule smpp_typeofnum");
        RewriteRuleSubtreeStream stream_comparison=new RewriteRuleSubtreeStream(adaptor,"rule comparison");
        RewriteRuleSubtreeStream stream_smpp_esm_gf=new RewriteRuleSubtreeStream(adaptor,"rule smpp_esm_gf");
        RewriteRuleSubtreeStream stream_eval_ton=new RewriteRuleSubtreeStream(adaptor,"rule eval_ton");
        RewriteRuleSubtreeStream stream_smpp_np=new RewriteRuleSubtreeStream(adaptor,"rule smpp_np");
        RewriteRuleSubtreeStream stream_dcs=new RewriteRuleSubtreeStream(adaptor,"rule dcs");
        RewriteRuleSubtreeStream stream_gti=new RewriteRuleSubtreeStream(adaptor,"rule gti");
        RewriteRuleSubtreeStream stream_smpp_eval_esm_gf=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_esm_gf");
        RewriteRuleSubtreeStream stream_smpp_eval_esm_mt=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_esm_mt");
        RewriteRuleSubtreeStream stream_smpp_eval_rd_in=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_rd_in");
        RewriteRuleSubtreeStream stream_eval_nai=new RewriteRuleSubtreeStream(adaptor,"rule eval_nai");
        RewriteRuleSubtreeStream stream_smpp_eval_rd_smscdr=new RewriteRuleSubtreeStream(adaptor,"rule smpp_eval_rd_smscdr");
        try {
            // /home/dfranusic/SmsFilterScript.g:456:3: ( L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* (b= SCCP_GT_CALLED_WL | b= SCCP_GT_CALLING_WL | b= MAP_SCOA_WL | b= MAP_SCDA_WL ) ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $b) | L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $bstr) | L_PAREN ( WS )* w1= eval_dcs ( WS )* a= comparison ( WS )* br1= dcs ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w1 $a $br1) | L_PAREN ( WS )* w2= eval_number ( WS )* a= comparison ( WS )* br2= DIGITS ( PERCENT )* ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w2 $a $br2) | L_PAREN ( WS )* w3= eval_ton ( WS )* a= comparison ( WS )* br3= typeofnum ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w3 $a $br3) | L_PAREN ( WS )* w4= eval_msg_type ( WS )* a= comparison ( WS )* br4= msgtype ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w4 $a $br4) | L_PAREN ( WS )* w5= eval_string ( WS )* a= comparison ( WS )* br5= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w5 $a $br5) | L_PAREN ( WS )* w6= eval_number ( WS )* a= comparison ( WS )* br6= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w6 $a $br6) | L_PAREN ( WS )* w7= eval_nai ( WS )* a= comparison ( WS )* br7= nai ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w7 $a $br7) | L_PAREN ( WS )* w8= eval_np ( WS )* a= comparison ( WS )* br8= np ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w8 $a $br8) | L_PAREN ( WS )* w9= eval_gti ( WS )* a= comparison ( WS )* br9= gti ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w9 $a $br9) | L_PAREN ( WS )* w10= smpp_eval_np ( WS )* a= comparison ( WS )* br10= smpp_np ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w10 $a $br10) | L_PAREN ( WS )* w11= smpp_eval_ton ( WS )* a= comparison ( WS )* br11= smpp_typeofnum ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w11 $a $br11) | L_PAREN ( WS )* w12= smpp_eval_esm_mm ( WS )* a= comparison ( WS )* br12= smpp_esm_mm ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w12 $a $br12) | L_PAREN ( WS )* w13= smpp_eval_esm_mt ( WS )* a= comparison ( WS )* br13= smpp_esm_mt ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w13 $a $br13) | L_PAREN ( WS )* w14= smpp_eval_esm_gf ( WS )* a= comparison ( WS )* br14= smpp_esm_gf ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w14 $a $br14) | L_PAREN ( WS )* w15= smpp_eval_rd_smscdr ( WS )* a= comparison ( WS )* br15= smpp_rd_smscdr ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w15 $a $br15) | L_PAREN ( WS )* w16= smpp_eval_rd_smeoa ( WS )* a= comparison ( WS )* br16= smpp_rd_smeoa ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w16 $a $br16) | L_PAREN ( WS )* w17= smpp_eval_rd_in ( WS )* a= comparison ( WS )* br17= smpp_rd_in ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w17 $a $br17) | L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fvar2= floodmethod ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $fvar $a $fvar2) | L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fdigs= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $fvar $a $fdigs) | L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* lvar= listmethod ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $lvar) )
            int alt92=22;
            alt92 = dfa92.predict(input);
            switch (alt92) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:456:5: L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* (b= SCCP_GT_CALLED_WL | b= SCCP_GT_CALLING_WL | b= MAP_SCOA_WL | b= MAP_SCDA_WL ) ( WS )* R_PAREN
                    {
                    L_PAREN20=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3653);  
                    stream_L_PAREN.add(L_PAREN20);

                    // /home/dfranusic/SmsFilterScript.g:456:13: ( WS )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==WS) ) {
                            alt2=1;
                        }


                        switch (alt2) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:456:13: WS
                    	    {
                    	    WS21=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3655);  
                    	    stream_WS.add(WS21);


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_evalsimple3660);
                    w=eval_string();

                    state._fsp--;

                    stream_eval_string.add(w.getTree());
                    // /home/dfranusic/SmsFilterScript.g:456:31: ( WS )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==WS) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:456:31: WS
                    	    {
                    	    WS22=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3662);  
                    	    stream_WS.add(WS22);


                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3667);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:456:48: ( WS )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==WS) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:456:48: WS
                    	    {
                    	    WS23=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3669);  
                    	    stream_WS.add(WS23);


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:456:52: (b= SCCP_GT_CALLED_WL | b= SCCP_GT_CALLING_WL | b= MAP_SCOA_WL | b= MAP_SCDA_WL )
                    int alt5=4;
                    switch ( input.LA(1) ) {
                    case SCCP_GT_CALLED_WL:
                        {
                        alt5=1;
                        }
                        break;
                    case SCCP_GT_CALLING_WL:
                        {
                        alt5=2;
                        }
                        break;
                    case MAP_SCOA_WL:
                        {
                        alt5=3;
                        }
                        break;
                    case MAP_SCDA_WL:
                        {
                        alt5=4;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 5, 0, input);

                        throw nvae;
                    }

                    switch (alt5) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:456:53: b= SCCP_GT_CALLED_WL
                            {
                            b=(Token)match(input,SCCP_GT_CALLED_WL,FOLLOW_SCCP_GT_CALLED_WL_in_evalsimple3675);  
                            stream_SCCP_GT_CALLED_WL.add(b);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:456:73: b= SCCP_GT_CALLING_WL
                            {
                            b=(Token)match(input,SCCP_GT_CALLING_WL,FOLLOW_SCCP_GT_CALLING_WL_in_evalsimple3679);  
                            stream_SCCP_GT_CALLING_WL.add(b);


                            }
                            break;
                        case 3 :
                            // /home/dfranusic/SmsFilterScript.g:456:94: b= MAP_SCOA_WL
                            {
                            b=(Token)match(input,MAP_SCOA_WL,FOLLOW_MAP_SCOA_WL_in_evalsimple3683);  
                            stream_MAP_SCOA_WL.add(b);


                            }
                            break;
                        case 4 :
                            // /home/dfranusic/SmsFilterScript.g:456:108: b= MAP_SCDA_WL
                            {
                            b=(Token)match(input,MAP_SCDA_WL,FOLLOW_MAP_SCDA_WL_in_evalsimple3687);  
                            stream_MAP_SCDA_WL.add(b);


                            }
                            break;

                    }

                    // /home/dfranusic/SmsFilterScript.g:456:123: ( WS )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==WS) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:456:123: WS
                    	    {
                    	    WS24=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3690);  
                    	    stream_WS.add(WS24);


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    R_PAREN25=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3693);  
                    stream_R_PAREN.add(R_PAREN25);



                    // AST REWRITE
                    // elements: w, b, a
                    // token labels: b
                    // rule labels: w, retval, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
                    RewriteRuleSubtreeStream stream_w=new RewriteRuleSubtreeStream(adaptor,"rule w",w!=null?w.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 456:135: -> ^( RULE_EVAL_ATOM $w $a $b)
                    {
                        // /home/dfranusic/SmsFilterScript.g:456:138: ^( RULE_EVAL_ATOM $w $a $b)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_b.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:457:5: L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN
                    {
                    L_PAREN26=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3714);  
                    stream_L_PAREN.add(L_PAREN26);

                    // /home/dfranusic/SmsFilterScript.g:457:13: ( WS )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==WS) ) {
                            alt7=1;
                        }


                        switch (alt7) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:457:13: WS
                    	    {
                    	    WS27=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3716);  
                    	    stream_WS.add(WS27);


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_evalsimple3721);
                    w=eval_string();

                    state._fsp--;

                    stream_eval_string.add(w.getTree());
                    // /home/dfranusic/SmsFilterScript.g:457:31: ( WS )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==WS) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:457:31: WS
                    	    {
                    	    WS28=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3723);  
                    	    stream_WS.add(WS28);


                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3728);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:457:48: ( WS )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==WS) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:457:48: WS
                    	    {
                    	    WS29=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3730);  
                    	    stream_WS.add(WS29);


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    bstr=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_evalsimple3735);  
                    stream_STRINGLITERAL.add(bstr);

                    // /home/dfranusic/SmsFilterScript.g:457:71: ( WS )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==WS) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:457:71: WS
                    	    {
                    	    WS30=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3737);  
                    	    stream_WS.add(WS30);


                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    R_PAREN31=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3740);  
                    stream_R_PAREN.add(R_PAREN31);



                    // AST REWRITE
                    // elements: bstr, a, w
                    // token labels: bstr
                    // rule labels: w, retval, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_bstr=new RewriteRuleTokenStream(adaptor,"token bstr",bstr);
                    RewriteRuleSubtreeStream stream_w=new RewriteRuleSubtreeStream(adaptor,"rule w",w!=null?w.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 457:83: -> ^( RULE_EVAL_ATOM $w $a $bstr)
                    {
                        // /home/dfranusic/SmsFilterScript.g:457:86: ^( RULE_EVAL_ATOM $w $a $bstr)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_bstr.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:458:5: L_PAREN ( WS )* w1= eval_dcs ( WS )* a= comparison ( WS )* br1= dcs ( WS )* R_PAREN
                    {
                    L_PAREN32=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3761);  
                    stream_L_PAREN.add(L_PAREN32);

                    // /home/dfranusic/SmsFilterScript.g:458:13: ( WS )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==WS) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:458:13: WS
                    	    {
                    	    WS33=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3763);  
                    	    stream_WS.add(WS33);


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_dcs_in_evalsimple3768);
                    w1=eval_dcs();

                    state._fsp--;

                    stream_eval_dcs.add(w1.getTree());
                    // /home/dfranusic/SmsFilterScript.g:458:29: ( WS )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==WS) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:458:29: WS
                    	    {
                    	    WS34=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3770);  
                    	    stream_WS.add(WS34);


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3775);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:458:46: ( WS )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==WS) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:458:46: WS
                    	    {
                    	    WS35=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3777);  
                    	    stream_WS.add(WS35);


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    pushFollow(FOLLOW_dcs_in_evalsimple3782);
                    br1=dcs();

                    state._fsp--;

                    stream_dcs.add(br1.getTree());
                    // /home/dfranusic/SmsFilterScript.g:458:58: ( WS )*
                    loop14:
                    do {
                        int alt14=2;
                        int LA14_0 = input.LA(1);

                        if ( (LA14_0==WS) ) {
                            alt14=1;
                        }


                        switch (alt14) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:458:58: WS
                    	    {
                    	    WS36=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3784);  
                    	    stream_WS.add(WS36);


                    	    }
                    	    break;

                    	default :
                    	    break loop14;
                        }
                    } while (true);

                    R_PAREN37=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3787);  
                    stream_R_PAREN.add(R_PAREN37);



                    // AST REWRITE
                    // elements: br1, w1, a
                    // token labels: 
                    // rule labels: retval, br1, a, w1
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br1=new RewriteRuleSubtreeStream(adaptor,"rule br1",br1!=null?br1.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w1=new RewriteRuleSubtreeStream(adaptor,"rule w1",w1!=null?w1.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 458:70: -> ^( RULE_EVAL_ATOM $w1 $a $br1)
                    {
                        // /home/dfranusic/SmsFilterScript.g:458:73: ^( RULE_EVAL_ATOM $w1 $a $br1)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w1.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br1.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:459:5: L_PAREN ( WS )* w2= eval_number ( WS )* a= comparison ( WS )* br2= DIGITS ( PERCENT )* ( WS )* R_PAREN
                    {
                    L_PAREN38=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3808);  
                    stream_L_PAREN.add(L_PAREN38);

                    // /home/dfranusic/SmsFilterScript.g:459:13: ( WS )*
                    loop15:
                    do {
                        int alt15=2;
                        int LA15_0 = input.LA(1);

                        if ( (LA15_0==WS) ) {
                            alt15=1;
                        }


                        switch (alt15) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:459:13: WS
                    	    {
                    	    WS39=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3810);  
                    	    stream_WS.add(WS39);


                    	    }
                    	    break;

                    	default :
                    	    break loop15;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_number_in_evalsimple3815);
                    w2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(w2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:459:32: ( WS )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==WS) ) {
                            alt16=1;
                        }


                        switch (alt16) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:459:32: WS
                    	    {
                    	    WS40=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3817);  
                    	    stream_WS.add(WS40);


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3822);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:459:49: ( WS )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==WS) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:459:49: WS
                    	    {
                    	    WS41=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3824);  
                    	    stream_WS.add(WS41);


                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);

                    br2=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_evalsimple3829);  
                    stream_DIGITS.add(br2);

                    // /home/dfranusic/SmsFilterScript.g:459:64: ( PERCENT )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==PERCENT) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:459:64: PERCENT
                    	    {
                    	    PERCENT42=(Token)match(input,PERCENT,FOLLOW_PERCENT_in_evalsimple3831);  
                    	    stream_PERCENT.add(PERCENT42);


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:459:73: ( WS )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==WS) ) {
                            alt19=1;
                        }


                        switch (alt19) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:459:73: WS
                    	    {
                    	    WS43=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3834);  
                    	    stream_WS.add(WS43);


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);

                    R_PAREN44=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3837);  
                    stream_R_PAREN.add(R_PAREN44);



                    // AST REWRITE
                    // elements: br2, w2, a
                    // token labels: br2
                    // rule labels: retval, a, w2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_br2=new RewriteRuleTokenStream(adaptor,"token br2",br2);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w2=new RewriteRuleSubtreeStream(adaptor,"rule w2",w2!=null?w2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 459:85: -> ^( RULE_EVAL_ATOM $w2 $a $br2)
                    {
                        // /home/dfranusic/SmsFilterScript.g:459:88: ^( RULE_EVAL_ATOM $w2 $a $br2)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w2.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br2.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:460:5: L_PAREN ( WS )* w3= eval_ton ( WS )* a= comparison ( WS )* br3= typeofnum ( WS )* R_PAREN
                    {
                    L_PAREN45=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3858);  
                    stream_L_PAREN.add(L_PAREN45);

                    // /home/dfranusic/SmsFilterScript.g:460:13: ( WS )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==WS) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:460:13: WS
                    	    {
                    	    WS46=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3860);  
                    	    stream_WS.add(WS46);


                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_ton_in_evalsimple3865);
                    w3=eval_ton();

                    state._fsp--;

                    stream_eval_ton.add(w3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:460:29: ( WS )*
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==WS) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:460:29: WS
                    	    {
                    	    WS47=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3867);  
                    	    stream_WS.add(WS47);


                    	    }
                    	    break;

                    	default :
                    	    break loop21;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3872);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:460:46: ( WS )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==WS) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:460:46: WS
                    	    {
                    	    WS48=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3874);  
                    	    stream_WS.add(WS48);


                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    pushFollow(FOLLOW_typeofnum_in_evalsimple3879);
                    br3=typeofnum();

                    state._fsp--;

                    stream_typeofnum.add(br3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:460:64: ( WS )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==WS) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:460:64: WS
                    	    {
                    	    WS49=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3881);  
                    	    stream_WS.add(WS49);


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);

                    R_PAREN50=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3884);  
                    stream_R_PAREN.add(R_PAREN50);



                    // AST REWRITE
                    // elements: br3, a, w3
                    // token labels: 
                    // rule labels: br3, retval, a, w3
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_br3=new RewriteRuleSubtreeStream(adaptor,"rule br3",br3!=null?br3.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w3=new RewriteRuleSubtreeStream(adaptor,"rule w3",w3!=null?w3.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 460:76: -> ^( RULE_EVAL_ATOM $w3 $a $br3)
                    {
                        // /home/dfranusic/SmsFilterScript.g:460:79: ^( RULE_EVAL_ATOM $w3 $a $br3)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w3.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br3.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 6 :
                    // /home/dfranusic/SmsFilterScript.g:461:5: L_PAREN ( WS )* w4= eval_msg_type ( WS )* a= comparison ( WS )* br4= msgtype ( WS )* R_PAREN
                    {
                    L_PAREN51=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3905);  
                    stream_L_PAREN.add(L_PAREN51);

                    // /home/dfranusic/SmsFilterScript.g:461:13: ( WS )*
                    loop24:
                    do {
                        int alt24=2;
                        int LA24_0 = input.LA(1);

                        if ( (LA24_0==WS) ) {
                            alt24=1;
                        }


                        switch (alt24) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:461:13: WS
                    	    {
                    	    WS52=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3907);  
                    	    stream_WS.add(WS52);


                    	    }
                    	    break;

                    	default :
                    	    break loop24;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_msg_type_in_evalsimple3912);
                    w4=eval_msg_type();

                    state._fsp--;

                    stream_eval_msg_type.add(w4.getTree());
                    // /home/dfranusic/SmsFilterScript.g:461:34: ( WS )*
                    loop25:
                    do {
                        int alt25=2;
                        int LA25_0 = input.LA(1);

                        if ( (LA25_0==WS) ) {
                            alt25=1;
                        }


                        switch (alt25) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:461:34: WS
                    	    {
                    	    WS53=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3914);  
                    	    stream_WS.add(WS53);


                    	    }
                    	    break;

                    	default :
                    	    break loop25;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3919);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:461:51: ( WS )*
                    loop26:
                    do {
                        int alt26=2;
                        int LA26_0 = input.LA(1);

                        if ( (LA26_0==WS) ) {
                            alt26=1;
                        }


                        switch (alt26) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:461:51: WS
                    	    {
                    	    WS54=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3921);  
                    	    stream_WS.add(WS54);


                    	    }
                    	    break;

                    	default :
                    	    break loop26;
                        }
                    } while (true);

                    pushFollow(FOLLOW_msgtype_in_evalsimple3926);
                    br4=msgtype();

                    state._fsp--;

                    stream_msgtype.add(br4.getTree());
                    // /home/dfranusic/SmsFilterScript.g:461:67: ( WS )*
                    loop27:
                    do {
                        int alt27=2;
                        int LA27_0 = input.LA(1);

                        if ( (LA27_0==WS) ) {
                            alt27=1;
                        }


                        switch (alt27) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:461:67: WS
                    	    {
                    	    WS55=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3928);  
                    	    stream_WS.add(WS55);


                    	    }
                    	    break;

                    	default :
                    	    break loop27;
                        }
                    } while (true);

                    R_PAREN56=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3931);  
                    stream_R_PAREN.add(R_PAREN56);



                    // AST REWRITE
                    // elements: a, br4, w4
                    // token labels: 
                    // rule labels: retval, br4, a, w4
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br4=new RewriteRuleSubtreeStream(adaptor,"rule br4",br4!=null?br4.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w4=new RewriteRuleSubtreeStream(adaptor,"rule w4",w4!=null?w4.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 461:79: -> ^( RULE_EVAL_ATOM $w4 $a $br4)
                    {
                        // /home/dfranusic/SmsFilterScript.g:461:82: ^( RULE_EVAL_ATOM $w4 $a $br4)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w4.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br4.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 7 :
                    // /home/dfranusic/SmsFilterScript.g:462:5: L_PAREN ( WS )* w5= eval_string ( WS )* a= comparison ( WS )* br5= eval_string ( WS )* R_PAREN
                    {
                    L_PAREN57=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple3953);  
                    stream_L_PAREN.add(L_PAREN57);

                    // /home/dfranusic/SmsFilterScript.g:462:13: ( WS )*
                    loop28:
                    do {
                        int alt28=2;
                        int LA28_0 = input.LA(1);

                        if ( (LA28_0==WS) ) {
                            alt28=1;
                        }


                        switch (alt28) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:462:13: WS
                    	    {
                    	    WS58=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3955);  
                    	    stream_WS.add(WS58);


                    	    }
                    	    break;

                    	default :
                    	    break loop28;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_evalsimple3960);
                    w5=eval_string();

                    state._fsp--;

                    stream_eval_string.add(w5.getTree());
                    // /home/dfranusic/SmsFilterScript.g:462:32: ( WS )*
                    loop29:
                    do {
                        int alt29=2;
                        int LA29_0 = input.LA(1);

                        if ( (LA29_0==WS) ) {
                            alt29=1;
                        }


                        switch (alt29) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:462:32: WS
                    	    {
                    	    WS59=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3962);  
                    	    stream_WS.add(WS59);


                    	    }
                    	    break;

                    	default :
                    	    break loop29;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple3967);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:462:49: ( WS )*
                    loop30:
                    do {
                        int alt30=2;
                        int LA30_0 = input.LA(1);

                        if ( (LA30_0==WS) ) {
                            alt30=1;
                        }


                        switch (alt30) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:462:49: WS
                    	    {
                    	    WS60=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3969);  
                    	    stream_WS.add(WS60);


                    	    }
                    	    break;

                    	default :
                    	    break loop30;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_evalsimple3974);
                    br5=eval_string();

                    state._fsp--;

                    stream_eval_string.add(br5.getTree());
                    // /home/dfranusic/SmsFilterScript.g:462:69: ( WS )*
                    loop31:
                    do {
                        int alt31=2;
                        int LA31_0 = input.LA(1);

                        if ( (LA31_0==WS) ) {
                            alt31=1;
                        }


                        switch (alt31) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:462:69: WS
                    	    {
                    	    WS61=(Token)match(input,WS,FOLLOW_WS_in_evalsimple3976);  
                    	    stream_WS.add(WS61);


                    	    }
                    	    break;

                    	default :
                    	    break loop31;
                        }
                    } while (true);

                    R_PAREN62=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple3979);  
                    stream_R_PAREN.add(R_PAREN62);



                    // AST REWRITE
                    // elements: a, br5, w5
                    // token labels: 
                    // rule labels: retval, br5, a, w5
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br5=new RewriteRuleSubtreeStream(adaptor,"rule br5",br5!=null?br5.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w5=new RewriteRuleSubtreeStream(adaptor,"rule w5",w5!=null?w5.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 462:81: -> ^( RULE_EVAL_ATOM $w5 $a $br5)
                    {
                        // /home/dfranusic/SmsFilterScript.g:462:84: ^( RULE_EVAL_ATOM $w5 $a $br5)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w5.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br5.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 8 :
                    // /home/dfranusic/SmsFilterScript.g:463:5: L_PAREN ( WS )* w6= eval_number ( WS )* a= comparison ( WS )* br6= eval_number ( WS )* R_PAREN
                    {
                    L_PAREN63=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4001);  
                    stream_L_PAREN.add(L_PAREN63);

                    // /home/dfranusic/SmsFilterScript.g:463:13: ( WS )*
                    loop32:
                    do {
                        int alt32=2;
                        int LA32_0 = input.LA(1);

                        if ( (LA32_0==WS) ) {
                            alt32=1;
                        }


                        switch (alt32) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:463:13: WS
                    	    {
                    	    WS64=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4003);  
                    	    stream_WS.add(WS64);


                    	    }
                    	    break;

                    	default :
                    	    break loop32;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_number_in_evalsimple4008);
                    w6=eval_number();

                    state._fsp--;

                    stream_eval_number.add(w6.getTree());
                    // /home/dfranusic/SmsFilterScript.g:463:32: ( WS )*
                    loop33:
                    do {
                        int alt33=2;
                        int LA33_0 = input.LA(1);

                        if ( (LA33_0==WS) ) {
                            alt33=1;
                        }


                        switch (alt33) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:463:32: WS
                    	    {
                    	    WS65=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4010);  
                    	    stream_WS.add(WS65);


                    	    }
                    	    break;

                    	default :
                    	    break loop33;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4015);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:463:49: ( WS )*
                    loop34:
                    do {
                        int alt34=2;
                        int LA34_0 = input.LA(1);

                        if ( (LA34_0==WS) ) {
                            alt34=1;
                        }


                        switch (alt34) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:463:49: WS
                    	    {
                    	    WS66=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4017);  
                    	    stream_WS.add(WS66);


                    	    }
                    	    break;

                    	default :
                    	    break loop34;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_number_in_evalsimple4022);
                    br6=eval_number();

                    state._fsp--;

                    stream_eval_number.add(br6.getTree());
                    // /home/dfranusic/SmsFilterScript.g:463:69: ( WS )*
                    loop35:
                    do {
                        int alt35=2;
                        int LA35_0 = input.LA(1);

                        if ( (LA35_0==WS) ) {
                            alt35=1;
                        }


                        switch (alt35) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:463:69: WS
                    	    {
                    	    WS67=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4024);  
                    	    stream_WS.add(WS67);


                    	    }
                    	    break;

                    	default :
                    	    break loop35;
                        }
                    } while (true);

                    R_PAREN68=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4027);  
                    stream_R_PAREN.add(R_PAREN68);



                    // AST REWRITE
                    // elements: w6, br6, a
                    // token labels: 
                    // rule labels: retval, br6, a, w6
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br6=new RewriteRuleSubtreeStream(adaptor,"rule br6",br6!=null?br6.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w6=new RewriteRuleSubtreeStream(adaptor,"rule w6",w6!=null?w6.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 463:81: -> ^( RULE_EVAL_ATOM $w6 $a $br6)
                    {
                        // /home/dfranusic/SmsFilterScript.g:463:84: ^( RULE_EVAL_ATOM $w6 $a $br6)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w6.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br6.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 9 :
                    // /home/dfranusic/SmsFilterScript.g:465:5: L_PAREN ( WS )* w7= eval_nai ( WS )* a= comparison ( WS )* br7= nai ( WS )* R_PAREN
                    {
                    L_PAREN69=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4052);  
                    stream_L_PAREN.add(L_PAREN69);

                    // /home/dfranusic/SmsFilterScript.g:465:13: ( WS )*
                    loop36:
                    do {
                        int alt36=2;
                        int LA36_0 = input.LA(1);

                        if ( (LA36_0==WS) ) {
                            alt36=1;
                        }


                        switch (alt36) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:465:13: WS
                    	    {
                    	    WS70=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4054);  
                    	    stream_WS.add(WS70);


                    	    }
                    	    break;

                    	default :
                    	    break loop36;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_nai_in_evalsimple4059);
                    w7=eval_nai();

                    state._fsp--;

                    stream_eval_nai.add(w7.getTree());
                    // /home/dfranusic/SmsFilterScript.g:465:29: ( WS )*
                    loop37:
                    do {
                        int alt37=2;
                        int LA37_0 = input.LA(1);

                        if ( (LA37_0==WS) ) {
                            alt37=1;
                        }


                        switch (alt37) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:465:29: WS
                    	    {
                    	    WS71=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4061);  
                    	    stream_WS.add(WS71);


                    	    }
                    	    break;

                    	default :
                    	    break loop37;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4066);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:465:46: ( WS )*
                    loop38:
                    do {
                        int alt38=2;
                        int LA38_0 = input.LA(1);

                        if ( (LA38_0==WS) ) {
                            alt38=1;
                        }


                        switch (alt38) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:465:46: WS
                    	    {
                    	    WS72=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4068);  
                    	    stream_WS.add(WS72);


                    	    }
                    	    break;

                    	default :
                    	    break loop38;
                        }
                    } while (true);

                    pushFollow(FOLLOW_nai_in_evalsimple4073);
                    br7=nai();

                    state._fsp--;

                    stream_nai.add(br7.getTree());
                    // /home/dfranusic/SmsFilterScript.g:465:58: ( WS )*
                    loop39:
                    do {
                        int alt39=2;
                        int LA39_0 = input.LA(1);

                        if ( (LA39_0==WS) ) {
                            alt39=1;
                        }


                        switch (alt39) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:465:58: WS
                    	    {
                    	    WS73=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4075);  
                    	    stream_WS.add(WS73);


                    	    }
                    	    break;

                    	default :
                    	    break loop39;
                        }
                    } while (true);

                    R_PAREN74=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4078);  
                    stream_R_PAREN.add(R_PAREN74);



                    // AST REWRITE
                    // elements: w7, a, br7
                    // token labels: 
                    // rule labels: retval, br7, a, w7
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br7=new RewriteRuleSubtreeStream(adaptor,"rule br7",br7!=null?br7.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w7=new RewriteRuleSubtreeStream(adaptor,"rule w7",w7!=null?w7.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 465:70: -> ^( RULE_EVAL_ATOM $w7 $a $br7)
                    {
                        // /home/dfranusic/SmsFilterScript.g:465:73: ^( RULE_EVAL_ATOM $w7 $a $br7)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w7.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br7.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 10 :
                    // /home/dfranusic/SmsFilterScript.g:467:5: L_PAREN ( WS )* w8= eval_np ( WS )* a= comparison ( WS )* br8= np ( WS )* R_PAREN
                    {
                    L_PAREN75=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4102);  
                    stream_L_PAREN.add(L_PAREN75);

                    // /home/dfranusic/SmsFilterScript.g:467:13: ( WS )*
                    loop40:
                    do {
                        int alt40=2;
                        int LA40_0 = input.LA(1);

                        if ( (LA40_0==WS) ) {
                            alt40=1;
                        }


                        switch (alt40) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:467:13: WS
                    	    {
                    	    WS76=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4104);  
                    	    stream_WS.add(WS76);


                    	    }
                    	    break;

                    	default :
                    	    break loop40;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_np_in_evalsimple4109);
                    w8=eval_np();

                    state._fsp--;

                    stream_eval_np.add(w8.getTree());
                    // /home/dfranusic/SmsFilterScript.g:467:28: ( WS )*
                    loop41:
                    do {
                        int alt41=2;
                        int LA41_0 = input.LA(1);

                        if ( (LA41_0==WS) ) {
                            alt41=1;
                        }


                        switch (alt41) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:467:28: WS
                    	    {
                    	    WS77=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4111);  
                    	    stream_WS.add(WS77);


                    	    }
                    	    break;

                    	default :
                    	    break loop41;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4116);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:467:45: ( WS )*
                    loop42:
                    do {
                        int alt42=2;
                        int LA42_0 = input.LA(1);

                        if ( (LA42_0==WS) ) {
                            alt42=1;
                        }


                        switch (alt42) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:467:45: WS
                    	    {
                    	    WS78=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4118);  
                    	    stream_WS.add(WS78);


                    	    }
                    	    break;

                    	default :
                    	    break loop42;
                        }
                    } while (true);

                    pushFollow(FOLLOW_np_in_evalsimple4123);
                    br8=np();

                    state._fsp--;

                    stream_np.add(br8.getTree());
                    // /home/dfranusic/SmsFilterScript.g:467:56: ( WS )*
                    loop43:
                    do {
                        int alt43=2;
                        int LA43_0 = input.LA(1);

                        if ( (LA43_0==WS) ) {
                            alt43=1;
                        }


                        switch (alt43) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:467:56: WS
                    	    {
                    	    WS79=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4125);  
                    	    stream_WS.add(WS79);


                    	    }
                    	    break;

                    	default :
                    	    break loop43;
                        }
                    } while (true);

                    R_PAREN80=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4128);  
                    stream_R_PAREN.add(R_PAREN80);



                    // AST REWRITE
                    // elements: a, w8, br8
                    // token labels: 
                    // rule labels: retval, w8, a, br8
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_w8=new RewriteRuleSubtreeStream(adaptor,"rule w8",w8!=null?w8.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_br8=new RewriteRuleSubtreeStream(adaptor,"rule br8",br8!=null?br8.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 467:68: -> ^( RULE_EVAL_ATOM $w8 $a $br8)
                    {
                        // /home/dfranusic/SmsFilterScript.g:467:71: ^( RULE_EVAL_ATOM $w8 $a $br8)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w8.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br8.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 11 :
                    // /home/dfranusic/SmsFilterScript.g:469:5: L_PAREN ( WS )* w9= eval_gti ( WS )* a= comparison ( WS )* br9= gti ( WS )* R_PAREN
                    {
                    L_PAREN81=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4152);  
                    stream_L_PAREN.add(L_PAREN81);

                    // /home/dfranusic/SmsFilterScript.g:469:13: ( WS )*
                    loop44:
                    do {
                        int alt44=2;
                        int LA44_0 = input.LA(1);

                        if ( (LA44_0==WS) ) {
                            alt44=1;
                        }


                        switch (alt44) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:469:13: WS
                    	    {
                    	    WS82=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4154);  
                    	    stream_WS.add(WS82);


                    	    }
                    	    break;

                    	default :
                    	    break loop44;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_gti_in_evalsimple4159);
                    w9=eval_gti();

                    state._fsp--;

                    stream_eval_gti.add(w9.getTree());
                    // /home/dfranusic/SmsFilterScript.g:469:29: ( WS )*
                    loop45:
                    do {
                        int alt45=2;
                        int LA45_0 = input.LA(1);

                        if ( (LA45_0==WS) ) {
                            alt45=1;
                        }


                        switch (alt45) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:469:29: WS
                    	    {
                    	    WS83=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4161);  
                    	    stream_WS.add(WS83);


                    	    }
                    	    break;

                    	default :
                    	    break loop45;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4166);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:469:46: ( WS )*
                    loop46:
                    do {
                        int alt46=2;
                        int LA46_0 = input.LA(1);

                        if ( (LA46_0==WS) ) {
                            alt46=1;
                        }


                        switch (alt46) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:469:46: WS
                    	    {
                    	    WS84=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4168);  
                    	    stream_WS.add(WS84);


                    	    }
                    	    break;

                    	default :
                    	    break loop46;
                        }
                    } while (true);

                    pushFollow(FOLLOW_gti_in_evalsimple4173);
                    br9=gti();

                    state._fsp--;

                    stream_gti.add(br9.getTree());
                    // /home/dfranusic/SmsFilterScript.g:469:58: ( WS )*
                    loop47:
                    do {
                        int alt47=2;
                        int LA47_0 = input.LA(1);

                        if ( (LA47_0==WS) ) {
                            alt47=1;
                        }


                        switch (alt47) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:469:58: WS
                    	    {
                    	    WS85=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4175);  
                    	    stream_WS.add(WS85);


                    	    }
                    	    break;

                    	default :
                    	    break loop47;
                        }
                    } while (true);

                    R_PAREN86=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4178);  
                    stream_R_PAREN.add(R_PAREN86);



                    // AST REWRITE
                    // elements: w9, br9, a
                    // token labels: 
                    // rule labels: retval, w9, a, br9
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_w9=new RewriteRuleSubtreeStream(adaptor,"rule w9",w9!=null?w9.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_br9=new RewriteRuleSubtreeStream(adaptor,"rule br9",br9!=null?br9.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 469:70: -> ^( RULE_EVAL_ATOM $w9 $a $br9)
                    {
                        // /home/dfranusic/SmsFilterScript.g:469:73: ^( RULE_EVAL_ATOM $w9 $a $br9)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w9.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br9.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 12 :
                    // /home/dfranusic/SmsFilterScript.g:471:5: L_PAREN ( WS )* w10= smpp_eval_np ( WS )* a= comparison ( WS )* br10= smpp_np ( WS )* R_PAREN
                    {
                    L_PAREN87=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4202);  
                    stream_L_PAREN.add(L_PAREN87);

                    // /home/dfranusic/SmsFilterScript.g:471:13: ( WS )*
                    loop48:
                    do {
                        int alt48=2;
                        int LA48_0 = input.LA(1);

                        if ( (LA48_0==WS) ) {
                            alt48=1;
                        }


                        switch (alt48) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:471:13: WS
                    	    {
                    	    WS88=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4204);  
                    	    stream_WS.add(WS88);


                    	    }
                    	    break;

                    	default :
                    	    break loop48;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_np_in_evalsimple4209);
                    w10=smpp_eval_np();

                    state._fsp--;

                    stream_smpp_eval_np.add(w10.getTree());
                    // /home/dfranusic/SmsFilterScript.g:471:34: ( WS )*
                    loop49:
                    do {
                        int alt49=2;
                        int LA49_0 = input.LA(1);

                        if ( (LA49_0==WS) ) {
                            alt49=1;
                        }


                        switch (alt49) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:471:34: WS
                    	    {
                    	    WS89=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4211);  
                    	    stream_WS.add(WS89);


                    	    }
                    	    break;

                    	default :
                    	    break loop49;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4216);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:471:51: ( WS )*
                    loop50:
                    do {
                        int alt50=2;
                        int LA50_0 = input.LA(1);

                        if ( (LA50_0==WS) ) {
                            alt50=1;
                        }


                        switch (alt50) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:471:51: WS
                    	    {
                    	    WS90=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4218);  
                    	    stream_WS.add(WS90);


                    	    }
                    	    break;

                    	default :
                    	    break loop50;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_np_in_evalsimple4223);
                    br10=smpp_np();

                    state._fsp--;

                    stream_smpp_np.add(br10.getTree());
                    // /home/dfranusic/SmsFilterScript.g:471:68: ( WS )*
                    loop51:
                    do {
                        int alt51=2;
                        int LA51_0 = input.LA(1);

                        if ( (LA51_0==WS) ) {
                            alt51=1;
                        }


                        switch (alt51) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:471:68: WS
                    	    {
                    	    WS91=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4225);  
                    	    stream_WS.add(WS91);


                    	    }
                    	    break;

                    	default :
                    	    break loop51;
                        }
                    } while (true);

                    R_PAREN92=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4228);  
                    stream_R_PAREN.add(R_PAREN92);



                    // AST REWRITE
                    // elements: a, br10, w10
                    // token labels: 
                    // rule labels: retval, a, w10, br10
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w10=new RewriteRuleSubtreeStream(adaptor,"rule w10",w10!=null?w10.tree:null);
                    RewriteRuleSubtreeStream stream_br10=new RewriteRuleSubtreeStream(adaptor,"rule br10",br10!=null?br10.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 471:80: -> ^( RULE_EVAL_ATOM $w10 $a $br10)
                    {
                        // /home/dfranusic/SmsFilterScript.g:471:83: ^( RULE_EVAL_ATOM $w10 $a $br10)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w10.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br10.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 13 :
                    // /home/dfranusic/SmsFilterScript.g:473:5: L_PAREN ( WS )* w11= smpp_eval_ton ( WS )* a= comparison ( WS )* br11= smpp_typeofnum ( WS )* R_PAREN
                    {
                    L_PAREN93=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4252);  
                    stream_L_PAREN.add(L_PAREN93);

                    // /home/dfranusic/SmsFilterScript.g:473:13: ( WS )*
                    loop52:
                    do {
                        int alt52=2;
                        int LA52_0 = input.LA(1);

                        if ( (LA52_0==WS) ) {
                            alt52=1;
                        }


                        switch (alt52) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:473:13: WS
                    	    {
                    	    WS94=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4254);  
                    	    stream_WS.add(WS94);


                    	    }
                    	    break;

                    	default :
                    	    break loop52;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_ton_in_evalsimple4259);
                    w11=smpp_eval_ton();

                    state._fsp--;

                    stream_smpp_eval_ton.add(w11.getTree());
                    // /home/dfranusic/SmsFilterScript.g:473:35: ( WS )*
                    loop53:
                    do {
                        int alt53=2;
                        int LA53_0 = input.LA(1);

                        if ( (LA53_0==WS) ) {
                            alt53=1;
                        }


                        switch (alt53) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:473:35: WS
                    	    {
                    	    WS95=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4261);  
                    	    stream_WS.add(WS95);


                    	    }
                    	    break;

                    	default :
                    	    break loop53;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4266);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:473:52: ( WS )*
                    loop54:
                    do {
                        int alt54=2;
                        int LA54_0 = input.LA(1);

                        if ( (LA54_0==WS) ) {
                            alt54=1;
                        }


                        switch (alt54) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:473:52: WS
                    	    {
                    	    WS96=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4268);  
                    	    stream_WS.add(WS96);


                    	    }
                    	    break;

                    	default :
                    	    break loop54;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_typeofnum_in_evalsimple4273);
                    br11=smpp_typeofnum();

                    state._fsp--;

                    stream_smpp_typeofnum.add(br11.getTree());
                    // /home/dfranusic/SmsFilterScript.g:473:76: ( WS )*
                    loop55:
                    do {
                        int alt55=2;
                        int LA55_0 = input.LA(1);

                        if ( (LA55_0==WS) ) {
                            alt55=1;
                        }


                        switch (alt55) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:473:76: WS
                    	    {
                    	    WS97=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4275);  
                    	    stream_WS.add(WS97);


                    	    }
                    	    break;

                    	default :
                    	    break loop55;
                        }
                    } while (true);

                    R_PAREN98=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4278);  
                    stream_R_PAREN.add(R_PAREN98);



                    // AST REWRITE
                    // elements: w11, br11, a
                    // token labels: 
                    // rule labels: br11, retval, a, w11
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_br11=new RewriteRuleSubtreeStream(adaptor,"rule br11",br11!=null?br11.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_w11=new RewriteRuleSubtreeStream(adaptor,"rule w11",w11!=null?w11.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 473:88: -> ^( RULE_EVAL_ATOM $w11 $a $br11)
                    {
                        // /home/dfranusic/SmsFilterScript.g:473:91: ^( RULE_EVAL_ATOM $w11 $a $br11)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w11.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br11.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 14 :
                    // /home/dfranusic/SmsFilterScript.g:475:5: L_PAREN ( WS )* w12= smpp_eval_esm_mm ( WS )* a= comparison ( WS )* br12= smpp_esm_mm ( WS )* R_PAREN
                    {
                    L_PAREN99=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4302);  
                    stream_L_PAREN.add(L_PAREN99);

                    // /home/dfranusic/SmsFilterScript.g:475:13: ( WS )*
                    loop56:
                    do {
                        int alt56=2;
                        int LA56_0 = input.LA(1);

                        if ( (LA56_0==WS) ) {
                            alt56=1;
                        }


                        switch (alt56) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:475:13: WS
                    	    {
                    	    WS100=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4304);  
                    	    stream_WS.add(WS100);


                    	    }
                    	    break;

                    	default :
                    	    break loop56;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_esm_mm_in_evalsimple4309);
                    w12=smpp_eval_esm_mm();

                    state._fsp--;

                    stream_smpp_eval_esm_mm.add(w12.getTree());
                    // /home/dfranusic/SmsFilterScript.g:475:38: ( WS )*
                    loop57:
                    do {
                        int alt57=2;
                        int LA57_0 = input.LA(1);

                        if ( (LA57_0==WS) ) {
                            alt57=1;
                        }


                        switch (alt57) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:475:38: WS
                    	    {
                    	    WS101=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4311);  
                    	    stream_WS.add(WS101);


                    	    }
                    	    break;

                    	default :
                    	    break loop57;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4316);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:475:55: ( WS )*
                    loop58:
                    do {
                        int alt58=2;
                        int LA58_0 = input.LA(1);

                        if ( (LA58_0==WS) ) {
                            alt58=1;
                        }


                        switch (alt58) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:475:55: WS
                    	    {
                    	    WS102=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4318);  
                    	    stream_WS.add(WS102);


                    	    }
                    	    break;

                    	default :
                    	    break loop58;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_esm_mm_in_evalsimple4323);
                    br12=smpp_esm_mm();

                    state._fsp--;

                    stream_smpp_esm_mm.add(br12.getTree());
                    // /home/dfranusic/SmsFilterScript.g:475:76: ( WS )*
                    loop59:
                    do {
                        int alt59=2;
                        int LA59_0 = input.LA(1);

                        if ( (LA59_0==WS) ) {
                            alt59=1;
                        }


                        switch (alt59) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:475:76: WS
                    	    {
                    	    WS103=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4325);  
                    	    stream_WS.add(WS103);


                    	    }
                    	    break;

                    	default :
                    	    break loop59;
                        }
                    } while (true);

                    R_PAREN104=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4328);  
                    stream_R_PAREN.add(R_PAREN104);



                    // AST REWRITE
                    // elements: br12, a, w12
                    // token labels: 
                    // rule labels: br12, w12, retval, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_br12=new RewriteRuleSubtreeStream(adaptor,"rule br12",br12!=null?br12.tree:null);
                    RewriteRuleSubtreeStream stream_w12=new RewriteRuleSubtreeStream(adaptor,"rule w12",w12!=null?w12.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 475:88: -> ^( RULE_EVAL_ATOM $w12 $a $br12)
                    {
                        // /home/dfranusic/SmsFilterScript.g:475:91: ^( RULE_EVAL_ATOM $w12 $a $br12)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w12.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br12.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 15 :
                    // /home/dfranusic/SmsFilterScript.g:477:5: L_PAREN ( WS )* w13= smpp_eval_esm_mt ( WS )* a= comparison ( WS )* br13= smpp_esm_mt ( WS )* R_PAREN
                    {
                    L_PAREN105=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4352);  
                    stream_L_PAREN.add(L_PAREN105);

                    // /home/dfranusic/SmsFilterScript.g:477:13: ( WS )*
                    loop60:
                    do {
                        int alt60=2;
                        int LA60_0 = input.LA(1);

                        if ( (LA60_0==WS) ) {
                            alt60=1;
                        }


                        switch (alt60) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:477:13: WS
                    	    {
                    	    WS106=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4354);  
                    	    stream_WS.add(WS106);


                    	    }
                    	    break;

                    	default :
                    	    break loop60;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_esm_mt_in_evalsimple4359);
                    w13=smpp_eval_esm_mt();

                    state._fsp--;

                    stream_smpp_eval_esm_mt.add(w13.getTree());
                    // /home/dfranusic/SmsFilterScript.g:477:38: ( WS )*
                    loop61:
                    do {
                        int alt61=2;
                        int LA61_0 = input.LA(1);

                        if ( (LA61_0==WS) ) {
                            alt61=1;
                        }


                        switch (alt61) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:477:38: WS
                    	    {
                    	    WS107=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4361);  
                    	    stream_WS.add(WS107);


                    	    }
                    	    break;

                    	default :
                    	    break loop61;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4366);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:477:55: ( WS )*
                    loop62:
                    do {
                        int alt62=2;
                        int LA62_0 = input.LA(1);

                        if ( (LA62_0==WS) ) {
                            alt62=1;
                        }


                        switch (alt62) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:477:55: WS
                    	    {
                    	    WS108=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4368);  
                    	    stream_WS.add(WS108);


                    	    }
                    	    break;

                    	default :
                    	    break loop62;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_esm_mt_in_evalsimple4373);
                    br13=smpp_esm_mt();

                    state._fsp--;

                    stream_smpp_esm_mt.add(br13.getTree());
                    // /home/dfranusic/SmsFilterScript.g:477:76: ( WS )*
                    loop63:
                    do {
                        int alt63=2;
                        int LA63_0 = input.LA(1);

                        if ( (LA63_0==WS) ) {
                            alt63=1;
                        }


                        switch (alt63) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:477:76: WS
                    	    {
                    	    WS109=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4375);  
                    	    stream_WS.add(WS109);


                    	    }
                    	    break;

                    	default :
                    	    break loop63;
                        }
                    } while (true);

                    R_PAREN110=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4378);  
                    stream_R_PAREN.add(R_PAREN110);



                    // AST REWRITE
                    // elements: w13, br13, a
                    // token labels: 
                    // rule labels: w13, retval, br13, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_w13=new RewriteRuleSubtreeStream(adaptor,"rule w13",w13!=null?w13.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br13=new RewriteRuleSubtreeStream(adaptor,"rule br13",br13!=null?br13.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 477:88: -> ^( RULE_EVAL_ATOM $w13 $a $br13)
                    {
                        // /home/dfranusic/SmsFilterScript.g:477:91: ^( RULE_EVAL_ATOM $w13 $a $br13)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w13.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br13.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 16 :
                    // /home/dfranusic/SmsFilterScript.g:479:5: L_PAREN ( WS )* w14= smpp_eval_esm_gf ( WS )* a= comparison ( WS )* br14= smpp_esm_gf ( WS )* R_PAREN
                    {
                    L_PAREN111=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4402);  
                    stream_L_PAREN.add(L_PAREN111);

                    // /home/dfranusic/SmsFilterScript.g:479:13: ( WS )*
                    loop64:
                    do {
                        int alt64=2;
                        int LA64_0 = input.LA(1);

                        if ( (LA64_0==WS) ) {
                            alt64=1;
                        }


                        switch (alt64) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:479:13: WS
                    	    {
                    	    WS112=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4404);  
                    	    stream_WS.add(WS112);


                    	    }
                    	    break;

                    	default :
                    	    break loop64;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_esm_gf_in_evalsimple4409);
                    w14=smpp_eval_esm_gf();

                    state._fsp--;

                    stream_smpp_eval_esm_gf.add(w14.getTree());
                    // /home/dfranusic/SmsFilterScript.g:479:38: ( WS )*
                    loop65:
                    do {
                        int alt65=2;
                        int LA65_0 = input.LA(1);

                        if ( (LA65_0==WS) ) {
                            alt65=1;
                        }


                        switch (alt65) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:479:38: WS
                    	    {
                    	    WS113=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4411);  
                    	    stream_WS.add(WS113);


                    	    }
                    	    break;

                    	default :
                    	    break loop65;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4416);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:479:55: ( WS )*
                    loop66:
                    do {
                        int alt66=2;
                        int LA66_0 = input.LA(1);

                        if ( (LA66_0==WS) ) {
                            alt66=1;
                        }


                        switch (alt66) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:479:55: WS
                    	    {
                    	    WS114=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4418);  
                    	    stream_WS.add(WS114);


                    	    }
                    	    break;

                    	default :
                    	    break loop66;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_esm_gf_in_evalsimple4423);
                    br14=smpp_esm_gf();

                    state._fsp--;

                    stream_smpp_esm_gf.add(br14.getTree());
                    // /home/dfranusic/SmsFilterScript.g:479:76: ( WS )*
                    loop67:
                    do {
                        int alt67=2;
                        int LA67_0 = input.LA(1);

                        if ( (LA67_0==WS) ) {
                            alt67=1;
                        }


                        switch (alt67) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:479:76: WS
                    	    {
                    	    WS115=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4425);  
                    	    stream_WS.add(WS115);


                    	    }
                    	    break;

                    	default :
                    	    break loop67;
                        }
                    } while (true);

                    R_PAREN116=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4428);  
                    stream_R_PAREN.add(R_PAREN116);



                    // AST REWRITE
                    // elements: a, br14, w14
                    // token labels: 
                    // rule labels: retval, br14, w14, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br14=new RewriteRuleSubtreeStream(adaptor,"rule br14",br14!=null?br14.tree:null);
                    RewriteRuleSubtreeStream stream_w14=new RewriteRuleSubtreeStream(adaptor,"rule w14",w14!=null?w14.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 479:88: -> ^( RULE_EVAL_ATOM $w14 $a $br14)
                    {
                        // /home/dfranusic/SmsFilterScript.g:479:91: ^( RULE_EVAL_ATOM $w14 $a $br14)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w14.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br14.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 17 :
                    // /home/dfranusic/SmsFilterScript.g:481:5: L_PAREN ( WS )* w15= smpp_eval_rd_smscdr ( WS )* a= comparison ( WS )* br15= smpp_rd_smscdr ( WS )* R_PAREN
                    {
                    L_PAREN117=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4452);  
                    stream_L_PAREN.add(L_PAREN117);

                    // /home/dfranusic/SmsFilterScript.g:481:13: ( WS )*
                    loop68:
                    do {
                        int alt68=2;
                        int LA68_0 = input.LA(1);

                        if ( (LA68_0==WS) ) {
                            alt68=1;
                        }


                        switch (alt68) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:481:13: WS
                    	    {
                    	    WS118=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4454);  
                    	    stream_WS.add(WS118);


                    	    }
                    	    break;

                    	default :
                    	    break loop68;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_rd_smscdr_in_evalsimple4459);
                    w15=smpp_eval_rd_smscdr();

                    state._fsp--;

                    stream_smpp_eval_rd_smscdr.add(w15.getTree());
                    // /home/dfranusic/SmsFilterScript.g:481:41: ( WS )*
                    loop69:
                    do {
                        int alt69=2;
                        int LA69_0 = input.LA(1);

                        if ( (LA69_0==WS) ) {
                            alt69=1;
                        }


                        switch (alt69) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:481:41: WS
                    	    {
                    	    WS119=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4461);  
                    	    stream_WS.add(WS119);


                    	    }
                    	    break;

                    	default :
                    	    break loop69;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4466);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:481:58: ( WS )*
                    loop70:
                    do {
                        int alt70=2;
                        int LA70_0 = input.LA(1);

                        if ( (LA70_0==WS) ) {
                            alt70=1;
                        }


                        switch (alt70) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:481:58: WS
                    	    {
                    	    WS120=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4468);  
                    	    stream_WS.add(WS120);


                    	    }
                    	    break;

                    	default :
                    	    break loop70;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_rd_smscdr_in_evalsimple4473);
                    br15=smpp_rd_smscdr();

                    state._fsp--;

                    stream_smpp_rd_smscdr.add(br15.getTree());
                    // /home/dfranusic/SmsFilterScript.g:481:82: ( WS )*
                    loop71:
                    do {
                        int alt71=2;
                        int LA71_0 = input.LA(1);

                        if ( (LA71_0==WS) ) {
                            alt71=1;
                        }


                        switch (alt71) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:481:82: WS
                    	    {
                    	    WS121=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4475);  
                    	    stream_WS.add(WS121);


                    	    }
                    	    break;

                    	default :
                    	    break loop71;
                        }
                    } while (true);

                    R_PAREN122=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4478);  
                    stream_R_PAREN.add(R_PAREN122);



                    // AST REWRITE
                    // elements: w15, a, br15
                    // token labels: 
                    // rule labels: retval, w15, br15, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_w15=new RewriteRuleSubtreeStream(adaptor,"rule w15",w15!=null?w15.tree:null);
                    RewriteRuleSubtreeStream stream_br15=new RewriteRuleSubtreeStream(adaptor,"rule br15",br15!=null?br15.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 481:94: -> ^( RULE_EVAL_ATOM $w15 $a $br15)
                    {
                        // /home/dfranusic/SmsFilterScript.g:481:97: ^( RULE_EVAL_ATOM $w15 $a $br15)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w15.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br15.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 18 :
                    // /home/dfranusic/SmsFilterScript.g:483:5: L_PAREN ( WS )* w16= smpp_eval_rd_smeoa ( WS )* a= comparison ( WS )* br16= smpp_rd_smeoa ( WS )* R_PAREN
                    {
                    L_PAREN123=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4502);  
                    stream_L_PAREN.add(L_PAREN123);

                    // /home/dfranusic/SmsFilterScript.g:483:13: ( WS )*
                    loop72:
                    do {
                        int alt72=2;
                        int LA72_0 = input.LA(1);

                        if ( (LA72_0==WS) ) {
                            alt72=1;
                        }


                        switch (alt72) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:483:13: WS
                    	    {
                    	    WS124=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4504);  
                    	    stream_WS.add(WS124);


                    	    }
                    	    break;

                    	default :
                    	    break loop72;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_rd_smeoa_in_evalsimple4509);
                    w16=smpp_eval_rd_smeoa();

                    state._fsp--;

                    stream_smpp_eval_rd_smeoa.add(w16.getTree());
                    // /home/dfranusic/SmsFilterScript.g:483:40: ( WS )*
                    loop73:
                    do {
                        int alt73=2;
                        int LA73_0 = input.LA(1);

                        if ( (LA73_0==WS) ) {
                            alt73=1;
                        }


                        switch (alt73) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:483:40: WS
                    	    {
                    	    WS125=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4511);  
                    	    stream_WS.add(WS125);


                    	    }
                    	    break;

                    	default :
                    	    break loop73;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4516);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:483:57: ( WS )*
                    loop74:
                    do {
                        int alt74=2;
                        int LA74_0 = input.LA(1);

                        if ( (LA74_0==WS) ) {
                            alt74=1;
                        }


                        switch (alt74) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:483:57: WS
                    	    {
                    	    WS126=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4518);  
                    	    stream_WS.add(WS126);


                    	    }
                    	    break;

                    	default :
                    	    break loop74;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_rd_smeoa_in_evalsimple4523);
                    br16=smpp_rd_smeoa();

                    state._fsp--;

                    stream_smpp_rd_smeoa.add(br16.getTree());
                    // /home/dfranusic/SmsFilterScript.g:483:80: ( WS )*
                    loop75:
                    do {
                        int alt75=2;
                        int LA75_0 = input.LA(1);

                        if ( (LA75_0==WS) ) {
                            alt75=1;
                        }


                        switch (alt75) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:483:80: WS
                    	    {
                    	    WS127=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4525);  
                    	    stream_WS.add(WS127);


                    	    }
                    	    break;

                    	default :
                    	    break loop75;
                        }
                    } while (true);

                    R_PAREN128=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4528);  
                    stream_R_PAREN.add(R_PAREN128);



                    // AST REWRITE
                    // elements: br16, w16, a
                    // token labels: 
                    // rule labels: retval, br16, w16, a
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_br16=new RewriteRuleSubtreeStream(adaptor,"rule br16",br16!=null?br16.tree:null);
                    RewriteRuleSubtreeStream stream_w16=new RewriteRuleSubtreeStream(adaptor,"rule w16",w16!=null?w16.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 483:92: -> ^( RULE_EVAL_ATOM $w16 $a $br16)
                    {
                        // /home/dfranusic/SmsFilterScript.g:483:95: ^( RULE_EVAL_ATOM $w16 $a $br16)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w16.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br16.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 19 :
                    // /home/dfranusic/SmsFilterScript.g:485:5: L_PAREN ( WS )* w17= smpp_eval_rd_in ( WS )* a= comparison ( WS )* br17= smpp_rd_in ( WS )* R_PAREN
                    {
                    L_PAREN129=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4552);  
                    stream_L_PAREN.add(L_PAREN129);

                    // /home/dfranusic/SmsFilterScript.g:485:13: ( WS )*
                    loop76:
                    do {
                        int alt76=2;
                        int LA76_0 = input.LA(1);

                        if ( (LA76_0==WS) ) {
                            alt76=1;
                        }


                        switch (alt76) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:485:13: WS
                    	    {
                    	    WS130=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4554);  
                    	    stream_WS.add(WS130);


                    	    }
                    	    break;

                    	default :
                    	    break loop76;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_eval_rd_in_in_evalsimple4559);
                    w17=smpp_eval_rd_in();

                    state._fsp--;

                    stream_smpp_eval_rd_in.add(w17.getTree());
                    // /home/dfranusic/SmsFilterScript.g:485:37: ( WS )*
                    loop77:
                    do {
                        int alt77=2;
                        int LA77_0 = input.LA(1);

                        if ( (LA77_0==WS) ) {
                            alt77=1;
                        }


                        switch (alt77) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:485:37: WS
                    	    {
                    	    WS131=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4561);  
                    	    stream_WS.add(WS131);


                    	    }
                    	    break;

                    	default :
                    	    break loop77;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4566);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:485:54: ( WS )*
                    loop78:
                    do {
                        int alt78=2;
                        int LA78_0 = input.LA(1);

                        if ( (LA78_0==WS) ) {
                            alt78=1;
                        }


                        switch (alt78) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:485:54: WS
                    	    {
                    	    WS132=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4568);  
                    	    stream_WS.add(WS132);


                    	    }
                    	    break;

                    	default :
                    	    break loop78;
                        }
                    } while (true);

                    pushFollow(FOLLOW_smpp_rd_in_in_evalsimple4573);
                    br17=smpp_rd_in();

                    state._fsp--;

                    stream_smpp_rd_in.add(br17.getTree());
                    // /home/dfranusic/SmsFilterScript.g:485:74: ( WS )*
                    loop79:
                    do {
                        int alt79=2;
                        int LA79_0 = input.LA(1);

                        if ( (LA79_0==WS) ) {
                            alt79=1;
                        }


                        switch (alt79) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:485:74: WS
                    	    {
                    	    WS133=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4575);  
                    	    stream_WS.add(WS133);


                    	    }
                    	    break;

                    	default :
                    	    break loop79;
                        }
                    } while (true);

                    R_PAREN134=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4578);  
                    stream_R_PAREN.add(R_PAREN134);



                    // AST REWRITE
                    // elements: br17, a, w17
                    // token labels: 
                    // rule labels: retval, w17, a, br17
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_w17=new RewriteRuleSubtreeStream(adaptor,"rule w17",w17!=null?w17.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_br17=new RewriteRuleSubtreeStream(adaptor,"rule br17",br17!=null?br17.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 485:86: -> ^( RULE_EVAL_ATOM $w17 $a $br17)
                    {
                        // /home/dfranusic/SmsFilterScript.g:485:89: ^( RULE_EVAL_ATOM $w17 $a $br17)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w17.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_br17.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 20 :
                    // /home/dfranusic/SmsFilterScript.g:489:5: L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fvar2= floodmethod ( WS )* R_PAREN
                    {
                    L_PAREN135=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4606);  
                    stream_L_PAREN.add(L_PAREN135);

                    // /home/dfranusic/SmsFilterScript.g:489:13: ( WS )*
                    loop80:
                    do {
                        int alt80=2;
                        int LA80_0 = input.LA(1);

                        if ( (LA80_0==WS) ) {
                            alt80=1;
                        }


                        switch (alt80) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:489:13: WS
                    	    {
                    	    WS136=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4608);  
                    	    stream_WS.add(WS136);


                    	    }
                    	    break;

                    	default :
                    	    break loop80;
                        }
                    } while (true);

                    pushFollow(FOLLOW_floodmethod_in_evalsimple4613);
                    fvar=floodmethod();

                    state._fsp--;

                    stream_floodmethod.add(fvar.getTree());
                    // /home/dfranusic/SmsFilterScript.g:489:34: ( WS )*
                    loop81:
                    do {
                        int alt81=2;
                        int LA81_0 = input.LA(1);

                        if ( (LA81_0==WS) ) {
                            alt81=1;
                        }


                        switch (alt81) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:489:34: WS
                    	    {
                    	    WS137=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4615);  
                    	    stream_WS.add(WS137);


                    	    }
                    	    break;

                    	default :
                    	    break loop81;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4620);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:489:51: ( WS )*
                    loop82:
                    do {
                        int alt82=2;
                        int LA82_0 = input.LA(1);

                        if ( (LA82_0==WS) ) {
                            alt82=1;
                        }


                        switch (alt82) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:489:51: WS
                    	    {
                    	    WS138=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4622);  
                    	    stream_WS.add(WS138);


                    	    }
                    	    break;

                    	default :
                    	    break loop82;
                        }
                    } while (true);

                    pushFollow(FOLLOW_floodmethod_in_evalsimple4627);
                    fvar2=floodmethod();

                    state._fsp--;

                    stream_floodmethod.add(fvar2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:489:73: ( WS )*
                    loop83:
                    do {
                        int alt83=2;
                        int LA83_0 = input.LA(1);

                        if ( (LA83_0==WS) ) {
                            alt83=1;
                        }


                        switch (alt83) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:489:73: WS
                    	    {
                    	    WS139=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4629);  
                    	    stream_WS.add(WS139);


                    	    }
                    	    break;

                    	default :
                    	    break loop83;
                        }
                    } while (true);

                    R_PAREN140=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4632);  
                    stream_R_PAREN.add(R_PAREN140);



                    // AST REWRITE
                    // elements: a, fvar2, fvar
                    // token labels: 
                    // rule labels: retval, fvar2, a, fvar
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_fvar2=new RewriteRuleSubtreeStream(adaptor,"rule fvar2",fvar2!=null?fvar2.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_fvar=new RewriteRuleSubtreeStream(adaptor,"rule fvar",fvar!=null?fvar.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 489:85: -> ^( RULE_EVAL_ATOM $fvar $a $fvar2)
                    {
                        // /home/dfranusic/SmsFilterScript.g:489:88: ^( RULE_EVAL_ATOM $fvar $a $fvar2)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_fvar.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_fvar2.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 21 :
                    // /home/dfranusic/SmsFilterScript.g:490:5: L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fdigs= DIGITS ( WS )* R_PAREN
                    {
                    L_PAREN141=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4653);  
                    stream_L_PAREN.add(L_PAREN141);

                    // /home/dfranusic/SmsFilterScript.g:490:13: ( WS )*
                    loop84:
                    do {
                        int alt84=2;
                        int LA84_0 = input.LA(1);

                        if ( (LA84_0==WS) ) {
                            alt84=1;
                        }


                        switch (alt84) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:490:13: WS
                    	    {
                    	    WS142=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4655);  
                    	    stream_WS.add(WS142);


                    	    }
                    	    break;

                    	default :
                    	    break loop84;
                        }
                    } while (true);

                    pushFollow(FOLLOW_floodmethod_in_evalsimple4660);
                    fvar=floodmethod();

                    state._fsp--;

                    stream_floodmethod.add(fvar.getTree());
                    // /home/dfranusic/SmsFilterScript.g:490:34: ( WS )*
                    loop85:
                    do {
                        int alt85=2;
                        int LA85_0 = input.LA(1);

                        if ( (LA85_0==WS) ) {
                            alt85=1;
                        }


                        switch (alt85) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:490:34: WS
                    	    {
                    	    WS143=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4662);  
                    	    stream_WS.add(WS143);


                    	    }
                    	    break;

                    	default :
                    	    break loop85;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4667);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:490:51: ( WS )*
                    loop86:
                    do {
                        int alt86=2;
                        int LA86_0 = input.LA(1);

                        if ( (LA86_0==WS) ) {
                            alt86=1;
                        }


                        switch (alt86) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:490:51: WS
                    	    {
                    	    WS144=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4669);  
                    	    stream_WS.add(WS144);


                    	    }
                    	    break;

                    	default :
                    	    break loop86;
                        }
                    } while (true);

                    fdigs=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_evalsimple4674);  
                    stream_DIGITS.add(fdigs);

                    // /home/dfranusic/SmsFilterScript.g:490:68: ( WS )*
                    loop87:
                    do {
                        int alt87=2;
                        int LA87_0 = input.LA(1);

                        if ( (LA87_0==WS) ) {
                            alt87=1;
                        }


                        switch (alt87) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:490:68: WS
                    	    {
                    	    WS145=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4676);  
                    	    stream_WS.add(WS145);


                    	    }
                    	    break;

                    	default :
                    	    break loop87;
                        }
                    } while (true);

                    R_PAREN146=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4679);  
                    stream_R_PAREN.add(R_PAREN146);



                    // AST REWRITE
                    // elements: a, fdigs, fvar
                    // token labels: fdigs
                    // rule labels: retval, a, fvar
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_fdigs=new RewriteRuleTokenStream(adaptor,"token fdigs",fdigs);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_fvar=new RewriteRuleSubtreeStream(adaptor,"rule fvar",fvar!=null?fvar.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 490:80: -> ^( RULE_EVAL_ATOM $fvar $a $fdigs)
                    {
                        // /home/dfranusic/SmsFilterScript.g:490:83: ^( RULE_EVAL_ATOM $fvar $a $fdigs)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_fvar.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_fdigs.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 22 :
                    // /home/dfranusic/SmsFilterScript.g:492:5: L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* lvar= listmethod ( WS )* R_PAREN
                    {
                    L_PAREN147=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_evalsimple4703);  
                    stream_L_PAREN.add(L_PAREN147);

                    // /home/dfranusic/SmsFilterScript.g:492:13: ( WS )*
                    loop88:
                    do {
                        int alt88=2;
                        int LA88_0 = input.LA(1);

                        if ( (LA88_0==WS) ) {
                            alt88=1;
                        }


                        switch (alt88) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:492:13: WS
                    	    {
                    	    WS148=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4705);  
                    	    stream_WS.add(WS148);


                    	    }
                    	    break;

                    	default :
                    	    break loop88;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_evalsimple4710);
                    w=eval_string();

                    state._fsp--;

                    stream_eval_string.add(w.getTree());
                    // /home/dfranusic/SmsFilterScript.g:492:31: ( WS )*
                    loop89:
                    do {
                        int alt89=2;
                        int LA89_0 = input.LA(1);

                        if ( (LA89_0==WS) ) {
                            alt89=1;
                        }


                        switch (alt89) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:492:31: WS
                    	    {
                    	    WS149=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4712);  
                    	    stream_WS.add(WS149);


                    	    }
                    	    break;

                    	default :
                    	    break loop89;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_evalsimple4717);
                    a=comparison();

                    state._fsp--;

                    stream_comparison.add(a.getTree());
                    // /home/dfranusic/SmsFilterScript.g:492:48: ( WS )*
                    loop90:
                    do {
                        int alt90=2;
                        int LA90_0 = input.LA(1);

                        if ( (LA90_0==WS) ) {
                            alt90=1;
                        }


                        switch (alt90) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:492:48: WS
                    	    {
                    	    WS150=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4719);  
                    	    stream_WS.add(WS150);


                    	    }
                    	    break;

                    	default :
                    	    break loop90;
                        }
                    } while (true);

                    pushFollow(FOLLOW_listmethod_in_evalsimple4724);
                    lvar=listmethod();

                    state._fsp--;

                    stream_listmethod.add(lvar.getTree());
                    // /home/dfranusic/SmsFilterScript.g:492:68: ( WS )*
                    loop91:
                    do {
                        int alt91=2;
                        int LA91_0 = input.LA(1);

                        if ( (LA91_0==WS) ) {
                            alt91=1;
                        }


                        switch (alt91) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:492:68: WS
                    	    {
                    	    WS151=(Token)match(input,WS,FOLLOW_WS_in_evalsimple4726);  
                    	    stream_WS.add(WS151);


                    	    }
                    	    break;

                    	default :
                    	    break loop91;
                        }
                    } while (true);

                    R_PAREN152=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_evalsimple4729);  
                    stream_R_PAREN.add(R_PAREN152);



                    // AST REWRITE
                    // elements: w, lvar, a
                    // token labels: 
                    // rule labels: w, retval, a, lvar
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_w=new RewriteRuleSubtreeStream(adaptor,"rule w",w!=null?w.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_a=new RewriteRuleSubtreeStream(adaptor,"rule a",a!=null?a.tree:null);
                    RewriteRuleSubtreeStream stream_lvar=new RewriteRuleSubtreeStream(adaptor,"rule lvar",lvar!=null?lvar.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 492:80: -> ^( RULE_EVAL_ATOM $w $a $lvar)
                    {
                        // /home/dfranusic/SmsFilterScript.g:492:83: ^( RULE_EVAL_ATOM $w $a $lvar)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        adaptor.addChild(root_1, stream_w.nextTree());
                        adaptor.addChild(root_1, stream_a.nextTree());
                        adaptor.addChild(root_1, stream_lvar.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "evalsimple"

    public static class listmethod_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "listmethod"
    // /home/dfranusic/SmsFilterScript.g:495:1: listmethod : s= LIST ( WS )* L_PAREN ( WS )* sl= STRINGLITERAL ( WS )* R_PAREN -> ^( $s $sl) ;
    public final SmsFilterScriptParser.listmethod_return listmethod() throws RecognitionException {
        SmsFilterScriptParser.listmethod_return retval = new SmsFilterScriptParser.listmethod_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token s=null;
        Token sl=null;
        Token WS153=null;
        Token L_PAREN154=null;
        Token WS155=null;
        Token WS156=null;
        Token R_PAREN157=null;

        Object s_tree=null;
        Object sl_tree=null;
        Object WS153_tree=null;
        Object L_PAREN154_tree=null;
        Object WS155_tree=null;
        Object WS156_tree=null;
        Object R_PAREN157_tree=null;
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_LIST=new RewriteRuleTokenStream(adaptor,"token LIST");
        RewriteRuleTokenStream stream_STRINGLITERAL=new RewriteRuleTokenStream(adaptor,"token STRINGLITERAL");
        RewriteRuleTokenStream stream_R_PAREN=new RewriteRuleTokenStream(adaptor,"token R_PAREN");
        RewriteRuleTokenStream stream_L_PAREN=new RewriteRuleTokenStream(adaptor,"token L_PAREN");

        try {
            // /home/dfranusic/SmsFilterScript.g:496:3: (s= LIST ( WS )* L_PAREN ( WS )* sl= STRINGLITERAL ( WS )* R_PAREN -> ^( $s $sl) )
            // /home/dfranusic/SmsFilterScript.g:496:5: s= LIST ( WS )* L_PAREN ( WS )* sl= STRINGLITERAL ( WS )* R_PAREN
            {
            s=(Token)match(input,LIST,FOLLOW_LIST_in_listmethod4759);  
            stream_LIST.add(s);

            // /home/dfranusic/SmsFilterScript.g:496:12: ( WS )*
            loop93:
            do {
                int alt93=2;
                int LA93_0 = input.LA(1);

                if ( (LA93_0==WS) ) {
                    alt93=1;
                }


                switch (alt93) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:496:12: WS
            	    {
            	    WS153=(Token)match(input,WS,FOLLOW_WS_in_listmethod4761);  
            	    stream_WS.add(WS153);


            	    }
            	    break;

            	default :
            	    break loop93;
                }
            } while (true);

            L_PAREN154=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_listmethod4764);  
            stream_L_PAREN.add(L_PAREN154);

            // /home/dfranusic/SmsFilterScript.g:496:24: ( WS )*
            loop94:
            do {
                int alt94=2;
                int LA94_0 = input.LA(1);

                if ( (LA94_0==WS) ) {
                    alt94=1;
                }


                switch (alt94) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:496:24: WS
            	    {
            	    WS155=(Token)match(input,WS,FOLLOW_WS_in_listmethod4766);  
            	    stream_WS.add(WS155);


            	    }
            	    break;

            	default :
            	    break loop94;
                }
            } while (true);

            sl=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_listmethod4771);  
            stream_STRINGLITERAL.add(sl);

            // /home/dfranusic/SmsFilterScript.g:496:45: ( WS )*
            loop95:
            do {
                int alt95=2;
                int LA95_0 = input.LA(1);

                if ( (LA95_0==WS) ) {
                    alt95=1;
                }


                switch (alt95) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:496:45: WS
            	    {
            	    WS156=(Token)match(input,WS,FOLLOW_WS_in_listmethod4773);  
            	    stream_WS.add(WS156);


            	    }
            	    break;

            	default :
            	    break loop95;
                }
            } while (true);

            R_PAREN157=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_listmethod4776);  
            stream_R_PAREN.add(R_PAREN157);



            // AST REWRITE
            // elements: sl, s
            // token labels: sl, s
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_sl=new RewriteRuleTokenStream(adaptor,"token sl",sl);
            RewriteRuleTokenStream stream_s=new RewriteRuleTokenStream(adaptor,"token s",s);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 496:57: -> ^( $s $sl)
            {
                // /home/dfranusic/SmsFilterScript.g:496:60: ^( $s $sl)
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot(stream_s.nextNode(), root_1);

                adaptor.addChild(root_1, stream_sl.nextNode());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "listmethod"

    public static class floodmethod_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "floodmethod"
    // /home/dfranusic/SmsFilterScript.g:499:1: floodmethod : (s= FLOOD ( WS )* L_PAREN ( WS )* e= eval_string ( WS )* COMMA ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $e $fi) | s= FLOOD_GLOBAL ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $fi) | s= FLOOD_MAX ( WS )* L_PAREN ( WS )* e= eval_string ( WS )* COMMA ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $e $fi) | s= FLOOD_GLOBAL_MAX ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $fi) | s= FLOOD_ALL_MAX ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $fi) );
    public final SmsFilterScriptParser.floodmethod_return floodmethod() throws RecognitionException {
        SmsFilterScriptParser.floodmethod_return retval = new SmsFilterScriptParser.floodmethod_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token s=null;
        Token fi=null;
        Token WS158=null;
        Token L_PAREN159=null;
        Token WS160=null;
        Token WS161=null;
        Token COMMA162=null;
        Token WS163=null;
        Token WS164=null;
        Token R_PAREN165=null;
        Token WS166=null;
        Token L_PAREN167=null;
        Token WS168=null;
        Token WS169=null;
        Token R_PAREN170=null;
        Token WS171=null;
        Token L_PAREN172=null;
        Token WS173=null;
        Token WS174=null;
        Token COMMA175=null;
        Token WS176=null;
        Token WS177=null;
        Token R_PAREN178=null;
        Token WS179=null;
        Token L_PAREN180=null;
        Token WS181=null;
        Token WS182=null;
        Token R_PAREN183=null;
        Token WS184=null;
        Token L_PAREN185=null;
        Token WS186=null;
        Token WS187=null;
        Token R_PAREN188=null;
        SmsFilterScriptParser.eval_string_return e = null;


        Object s_tree=null;
        Object fi_tree=null;
        Object WS158_tree=null;
        Object L_PAREN159_tree=null;
        Object WS160_tree=null;
        Object WS161_tree=null;
        Object COMMA162_tree=null;
        Object WS163_tree=null;
        Object WS164_tree=null;
        Object R_PAREN165_tree=null;
        Object WS166_tree=null;
        Object L_PAREN167_tree=null;
        Object WS168_tree=null;
        Object WS169_tree=null;
        Object R_PAREN170_tree=null;
        Object WS171_tree=null;
        Object L_PAREN172_tree=null;
        Object WS173_tree=null;
        Object WS174_tree=null;
        Object COMMA175_tree=null;
        Object WS176_tree=null;
        Object WS177_tree=null;
        Object R_PAREN178_tree=null;
        Object WS179_tree=null;
        Object L_PAREN180_tree=null;
        Object WS181_tree=null;
        Object WS182_tree=null;
        Object R_PAREN183_tree=null;
        Object WS184_tree=null;
        Object L_PAREN185_tree=null;
        Object WS186_tree=null;
        Object WS187_tree=null;
        Object R_PAREN188_tree=null;
        RewriteRuleTokenStream stream_FLOOD_MINUTE=new RewriteRuleTokenStream(adaptor,"token FLOOD_MINUTE");
        RewriteRuleTokenStream stream_FLOOD_HOUR=new RewriteRuleTokenStream(adaptor,"token FLOOD_HOUR");
        RewriteRuleTokenStream stream_FLOOD_DAY=new RewriteRuleTokenStream(adaptor,"token FLOOD_DAY");
        RewriteRuleTokenStream stream_FLOOD_GLOBAL_MAX=new RewriteRuleTokenStream(adaptor,"token FLOOD_GLOBAL_MAX");
        RewriteRuleTokenStream stream_FLOOD_GLOBAL=new RewriteRuleTokenStream(adaptor,"token FLOOD_GLOBAL");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_FLOOD_ALL_MAX=new RewriteRuleTokenStream(adaptor,"token FLOOD_ALL_MAX");
        RewriteRuleTokenStream stream_FLOOD_MAX=new RewriteRuleTokenStream(adaptor,"token FLOOD_MAX");
        RewriteRuleTokenStream stream_FLOOD=new RewriteRuleTokenStream(adaptor,"token FLOOD");
        RewriteRuleTokenStream stream_R_PAREN=new RewriteRuleTokenStream(adaptor,"token R_PAREN");
        RewriteRuleTokenStream stream_L_PAREN=new RewriteRuleTokenStream(adaptor,"token L_PAREN");
        RewriteRuleSubtreeStream stream_eval_string=new RewriteRuleSubtreeStream(adaptor,"rule eval_string");
        try {
            // /home/dfranusic/SmsFilterScript.g:500:3: (s= FLOOD ( WS )* L_PAREN ( WS )* e= eval_string ( WS )* COMMA ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $e $fi) | s= FLOOD_GLOBAL ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $fi) | s= FLOOD_MAX ( WS )* L_PAREN ( WS )* e= eval_string ( WS )* COMMA ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $e $fi) | s= FLOOD_GLOBAL_MAX ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $fi) | s= FLOOD_ALL_MAX ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN -> ^( $s $fi) )
            int alt120=5;
            switch ( input.LA(1) ) {
            case FLOOD:
                {
                alt120=1;
                }
                break;
            case FLOOD_GLOBAL:
                {
                alt120=2;
                }
                break;
            case FLOOD_MAX:
                {
                alt120=3;
                }
                break;
            case FLOOD_GLOBAL_MAX:
                {
                alt120=4;
                }
                break;
            case FLOOD_ALL_MAX:
                {
                alt120=5;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 120, 0, input);

                throw nvae;
            }

            switch (alt120) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:500:5: s= FLOOD ( WS )* L_PAREN ( WS )* e= eval_string ( WS )* COMMA ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN
                    {
                    s=(Token)match(input,FLOOD,FOLLOW_FLOOD_in_floodmethod4802);  
                    stream_FLOOD.add(s);

                    // /home/dfranusic/SmsFilterScript.g:500:13: ( WS )*
                    loop96:
                    do {
                        int alt96=2;
                        int LA96_0 = input.LA(1);

                        if ( (LA96_0==WS) ) {
                            alt96=1;
                        }


                        switch (alt96) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:500:13: WS
                    	    {
                    	    WS158=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4804);  
                    	    stream_WS.add(WS158);


                    	    }
                    	    break;

                    	default :
                    	    break loop96;
                        }
                    } while (true);

                    L_PAREN159=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_floodmethod4807);  
                    stream_L_PAREN.add(L_PAREN159);

                    // /home/dfranusic/SmsFilterScript.g:500:25: ( WS )*
                    loop97:
                    do {
                        int alt97=2;
                        int LA97_0 = input.LA(1);

                        if ( (LA97_0==WS) ) {
                            alt97=1;
                        }


                        switch (alt97) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:500:25: WS
                    	    {
                    	    WS160=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4809);  
                    	    stream_WS.add(WS160);


                    	    }
                    	    break;

                    	default :
                    	    break loop97;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_floodmethod4814);
                    e=eval_string();

                    state._fsp--;

                    stream_eval_string.add(e.getTree());
                    // /home/dfranusic/SmsFilterScript.g:500:43: ( WS )*
                    loop98:
                    do {
                        int alt98=2;
                        int LA98_0 = input.LA(1);

                        if ( (LA98_0==WS) ) {
                            alt98=1;
                        }


                        switch (alt98) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:500:43: WS
                    	    {
                    	    WS161=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4816);  
                    	    stream_WS.add(WS161);


                    	    }
                    	    break;

                    	default :
                    	    break loop98;
                        }
                    } while (true);

                    COMMA162=(Token)match(input,COMMA,FOLLOW_COMMA_in_floodmethod4819);  
                    stream_COMMA.add(COMMA162);

                    // /home/dfranusic/SmsFilterScript.g:500:53: ( WS )*
                    loop99:
                    do {
                        int alt99=2;
                        int LA99_0 = input.LA(1);

                        if ( (LA99_0==WS) ) {
                            alt99=1;
                        }


                        switch (alt99) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:500:53: WS
                    	    {
                    	    WS163=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4821);  
                    	    stream_WS.add(WS163);


                    	    }
                    	    break;

                    	default :
                    	    break loop99;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:500:57: (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY )
                    int alt100=3;
                    switch ( input.LA(1) ) {
                    case FLOOD_MINUTE:
                        {
                        alt100=1;
                        }
                        break;
                    case FLOOD_HOUR:
                        {
                        alt100=2;
                        }
                        break;
                    case FLOOD_DAY:
                        {
                        alt100=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 100, 0, input);

                        throw nvae;
                    }

                    switch (alt100) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:500:58: fi= FLOOD_MINUTE
                            {
                            fi=(Token)match(input,FLOOD_MINUTE,FOLLOW_FLOOD_MINUTE_in_floodmethod4827);  
                            stream_FLOOD_MINUTE.add(fi);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:500:76: fi= FLOOD_HOUR
                            {
                            fi=(Token)match(input,FLOOD_HOUR,FOLLOW_FLOOD_HOUR_in_floodmethod4833);  
                            stream_FLOOD_HOUR.add(fi);


                            }
                            break;
                        case 3 :
                            // /home/dfranusic/SmsFilterScript.g:500:92: fi= FLOOD_DAY
                            {
                            fi=(Token)match(input,FLOOD_DAY,FOLLOW_FLOOD_DAY_in_floodmethod4839);  
                            stream_FLOOD_DAY.add(fi);


                            }
                            break;

                    }

                    // /home/dfranusic/SmsFilterScript.g:500:106: ( WS )*
                    loop101:
                    do {
                        int alt101=2;
                        int LA101_0 = input.LA(1);

                        if ( (LA101_0==WS) ) {
                            alt101=1;
                        }


                        switch (alt101) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:500:106: WS
                    	    {
                    	    WS164=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4842);  
                    	    stream_WS.add(WS164);


                    	    }
                    	    break;

                    	default :
                    	    break loop101;
                        }
                    } while (true);

                    R_PAREN165=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_floodmethod4845);  
                    stream_R_PAREN.add(R_PAREN165);



                    // AST REWRITE
                    // elements: fi, e, s
                    // token labels: s, fi
                    // rule labels: retval, e
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_s=new RewriteRuleTokenStream(adaptor,"token s",s);
                    RewriteRuleTokenStream stream_fi=new RewriteRuleTokenStream(adaptor,"token fi",fi);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"rule e",e!=null?e.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 500:118: -> ^( $s $e $fi)
                    {
                        // /home/dfranusic/SmsFilterScript.g:500:121: ^( $s $e $fi)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_s.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_e.nextTree());
                        adaptor.addChild(root_1, stream_fi.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:501:5: s= FLOOD_GLOBAL ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN
                    {
                    s=(Token)match(input,FLOOD_GLOBAL,FOLLOW_FLOOD_GLOBAL_in_floodmethod4866);  
                    stream_FLOOD_GLOBAL.add(s);

                    // /home/dfranusic/SmsFilterScript.g:501:20: ( WS )*
                    loop102:
                    do {
                        int alt102=2;
                        int LA102_0 = input.LA(1);

                        if ( (LA102_0==WS) ) {
                            alt102=1;
                        }


                        switch (alt102) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:501:20: WS
                    	    {
                    	    WS166=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4868);  
                    	    stream_WS.add(WS166);


                    	    }
                    	    break;

                    	default :
                    	    break loop102;
                        }
                    } while (true);

                    L_PAREN167=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_floodmethod4871);  
                    stream_L_PAREN.add(L_PAREN167);

                    // /home/dfranusic/SmsFilterScript.g:501:32: ( WS )*
                    loop103:
                    do {
                        int alt103=2;
                        int LA103_0 = input.LA(1);

                        if ( (LA103_0==WS) ) {
                            alt103=1;
                        }


                        switch (alt103) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:501:32: WS
                    	    {
                    	    WS168=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4873);  
                    	    stream_WS.add(WS168);


                    	    }
                    	    break;

                    	default :
                    	    break loop103;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:501:36: (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY )
                    int alt104=3;
                    switch ( input.LA(1) ) {
                    case FLOOD_MINUTE:
                        {
                        alt104=1;
                        }
                        break;
                    case FLOOD_HOUR:
                        {
                        alt104=2;
                        }
                        break;
                    case FLOOD_DAY:
                        {
                        alt104=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 104, 0, input);

                        throw nvae;
                    }

                    switch (alt104) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:501:37: fi= FLOOD_MINUTE
                            {
                            fi=(Token)match(input,FLOOD_MINUTE,FOLLOW_FLOOD_MINUTE_in_floodmethod4879);  
                            stream_FLOOD_MINUTE.add(fi);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:501:55: fi= FLOOD_HOUR
                            {
                            fi=(Token)match(input,FLOOD_HOUR,FOLLOW_FLOOD_HOUR_in_floodmethod4885);  
                            stream_FLOOD_HOUR.add(fi);


                            }
                            break;
                        case 3 :
                            // /home/dfranusic/SmsFilterScript.g:501:71: fi= FLOOD_DAY
                            {
                            fi=(Token)match(input,FLOOD_DAY,FOLLOW_FLOOD_DAY_in_floodmethod4891);  
                            stream_FLOOD_DAY.add(fi);


                            }
                            break;

                    }

                    // /home/dfranusic/SmsFilterScript.g:501:85: ( WS )*
                    loop105:
                    do {
                        int alt105=2;
                        int LA105_0 = input.LA(1);

                        if ( (LA105_0==WS) ) {
                            alt105=1;
                        }


                        switch (alt105) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:501:85: WS
                    	    {
                    	    WS169=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4894);  
                    	    stream_WS.add(WS169);


                    	    }
                    	    break;

                    	default :
                    	    break loop105;
                        }
                    } while (true);

                    R_PAREN170=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_floodmethod4897);  
                    stream_R_PAREN.add(R_PAREN170);



                    // AST REWRITE
                    // elements: s, fi
                    // token labels: s, fi
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_s=new RewriteRuleTokenStream(adaptor,"token s",s);
                    RewriteRuleTokenStream stream_fi=new RewriteRuleTokenStream(adaptor,"token fi",fi);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 501:97: -> ^( $s $fi)
                    {
                        // /home/dfranusic/SmsFilterScript.g:501:100: ^( $s $fi)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_s.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_fi.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:502:5: s= FLOOD_MAX ( WS )* L_PAREN ( WS )* e= eval_string ( WS )* COMMA ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN
                    {
                    s=(Token)match(input,FLOOD_MAX,FOLLOW_FLOOD_MAX_in_floodmethod4915);  
                    stream_FLOOD_MAX.add(s);

                    // /home/dfranusic/SmsFilterScript.g:502:18: ( WS )*
                    loop106:
                    do {
                        int alt106=2;
                        int LA106_0 = input.LA(1);

                        if ( (LA106_0==WS) ) {
                            alt106=1;
                        }


                        switch (alt106) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:502:18: WS
                    	    {
                    	    WS171=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4918);  
                    	    stream_WS.add(WS171);


                    	    }
                    	    break;

                    	default :
                    	    break loop106;
                        }
                    } while (true);

                    L_PAREN172=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_floodmethod4921);  
                    stream_L_PAREN.add(L_PAREN172);

                    // /home/dfranusic/SmsFilterScript.g:502:30: ( WS )*
                    loop107:
                    do {
                        int alt107=2;
                        int LA107_0 = input.LA(1);

                        if ( (LA107_0==WS) ) {
                            alt107=1;
                        }


                        switch (alt107) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:502:30: WS
                    	    {
                    	    WS173=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4923);  
                    	    stream_WS.add(WS173);


                    	    }
                    	    break;

                    	default :
                    	    break loop107;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_floodmethod4928);
                    e=eval_string();

                    state._fsp--;

                    stream_eval_string.add(e.getTree());
                    // /home/dfranusic/SmsFilterScript.g:502:48: ( WS )*
                    loop108:
                    do {
                        int alt108=2;
                        int LA108_0 = input.LA(1);

                        if ( (LA108_0==WS) ) {
                            alt108=1;
                        }


                        switch (alt108) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:502:48: WS
                    	    {
                    	    WS174=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4930);  
                    	    stream_WS.add(WS174);


                    	    }
                    	    break;

                    	default :
                    	    break loop108;
                        }
                    } while (true);

                    COMMA175=(Token)match(input,COMMA,FOLLOW_COMMA_in_floodmethod4933);  
                    stream_COMMA.add(COMMA175);

                    // /home/dfranusic/SmsFilterScript.g:502:58: ( WS )*
                    loop109:
                    do {
                        int alt109=2;
                        int LA109_0 = input.LA(1);

                        if ( (LA109_0==WS) ) {
                            alt109=1;
                        }


                        switch (alt109) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:502:58: WS
                    	    {
                    	    WS176=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4935);  
                    	    stream_WS.add(WS176);


                    	    }
                    	    break;

                    	default :
                    	    break loop109;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:502:62: (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY )
                    int alt110=3;
                    switch ( input.LA(1) ) {
                    case FLOOD_MINUTE:
                        {
                        alt110=1;
                        }
                        break;
                    case FLOOD_HOUR:
                        {
                        alt110=2;
                        }
                        break;
                    case FLOOD_DAY:
                        {
                        alt110=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 110, 0, input);

                        throw nvae;
                    }

                    switch (alt110) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:502:63: fi= FLOOD_MINUTE
                            {
                            fi=(Token)match(input,FLOOD_MINUTE,FOLLOW_FLOOD_MINUTE_in_floodmethod4941);  
                            stream_FLOOD_MINUTE.add(fi);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:502:81: fi= FLOOD_HOUR
                            {
                            fi=(Token)match(input,FLOOD_HOUR,FOLLOW_FLOOD_HOUR_in_floodmethod4947);  
                            stream_FLOOD_HOUR.add(fi);


                            }
                            break;
                        case 3 :
                            // /home/dfranusic/SmsFilterScript.g:502:97: fi= FLOOD_DAY
                            {
                            fi=(Token)match(input,FLOOD_DAY,FOLLOW_FLOOD_DAY_in_floodmethod4953);  
                            stream_FLOOD_DAY.add(fi);


                            }
                            break;

                    }

                    // /home/dfranusic/SmsFilterScript.g:502:111: ( WS )*
                    loop111:
                    do {
                        int alt111=2;
                        int LA111_0 = input.LA(1);

                        if ( (LA111_0==WS) ) {
                            alt111=1;
                        }


                        switch (alt111) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:502:111: WS
                    	    {
                    	    WS177=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4956);  
                    	    stream_WS.add(WS177);


                    	    }
                    	    break;

                    	default :
                    	    break loop111;
                        }
                    } while (true);

                    R_PAREN178=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_floodmethod4959);  
                    stream_R_PAREN.add(R_PAREN178);



                    // AST REWRITE
                    // elements: e, s, fi
                    // token labels: s, fi
                    // rule labels: retval, e
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_s=new RewriteRuleTokenStream(adaptor,"token s",s);
                    RewriteRuleTokenStream stream_fi=new RewriteRuleTokenStream(adaptor,"token fi",fi);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_e=new RewriteRuleSubtreeStream(adaptor,"rule e",e!=null?e.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 502:123: -> ^( $s $e $fi)
                    {
                        // /home/dfranusic/SmsFilterScript.g:502:126: ^( $s $e $fi)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_s.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_e.nextTree());
                        adaptor.addChild(root_1, stream_fi.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:503:5: s= FLOOD_GLOBAL_MAX ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN
                    {
                    s=(Token)match(input,FLOOD_GLOBAL_MAX,FOLLOW_FLOOD_GLOBAL_MAX_in_floodmethod4980);  
                    stream_FLOOD_GLOBAL_MAX.add(s);

                    // /home/dfranusic/SmsFilterScript.g:503:24: ( WS )*
                    loop112:
                    do {
                        int alt112=2;
                        int LA112_0 = input.LA(1);

                        if ( (LA112_0==WS) ) {
                            alt112=1;
                        }


                        switch (alt112) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:503:24: WS
                    	    {
                    	    WS179=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4982);  
                    	    stream_WS.add(WS179);


                    	    }
                    	    break;

                    	default :
                    	    break loop112;
                        }
                    } while (true);

                    L_PAREN180=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_floodmethod4985);  
                    stream_L_PAREN.add(L_PAREN180);

                    // /home/dfranusic/SmsFilterScript.g:503:36: ( WS )*
                    loop113:
                    do {
                        int alt113=2;
                        int LA113_0 = input.LA(1);

                        if ( (LA113_0==WS) ) {
                            alt113=1;
                        }


                        switch (alt113) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:503:36: WS
                    	    {
                    	    WS181=(Token)match(input,WS,FOLLOW_WS_in_floodmethod4987);  
                    	    stream_WS.add(WS181);


                    	    }
                    	    break;

                    	default :
                    	    break loop113;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:503:40: (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY )
                    int alt114=3;
                    switch ( input.LA(1) ) {
                    case FLOOD_MINUTE:
                        {
                        alt114=1;
                        }
                        break;
                    case FLOOD_HOUR:
                        {
                        alt114=2;
                        }
                        break;
                    case FLOOD_DAY:
                        {
                        alt114=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 114, 0, input);

                        throw nvae;
                    }

                    switch (alt114) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:503:41: fi= FLOOD_MINUTE
                            {
                            fi=(Token)match(input,FLOOD_MINUTE,FOLLOW_FLOOD_MINUTE_in_floodmethod4993);  
                            stream_FLOOD_MINUTE.add(fi);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:503:59: fi= FLOOD_HOUR
                            {
                            fi=(Token)match(input,FLOOD_HOUR,FOLLOW_FLOOD_HOUR_in_floodmethod4999);  
                            stream_FLOOD_HOUR.add(fi);


                            }
                            break;
                        case 3 :
                            // /home/dfranusic/SmsFilterScript.g:503:75: fi= FLOOD_DAY
                            {
                            fi=(Token)match(input,FLOOD_DAY,FOLLOW_FLOOD_DAY_in_floodmethod5005);  
                            stream_FLOOD_DAY.add(fi);


                            }
                            break;

                    }

                    // /home/dfranusic/SmsFilterScript.g:503:89: ( WS )*
                    loop115:
                    do {
                        int alt115=2;
                        int LA115_0 = input.LA(1);

                        if ( (LA115_0==WS) ) {
                            alt115=1;
                        }


                        switch (alt115) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:503:89: WS
                    	    {
                    	    WS182=(Token)match(input,WS,FOLLOW_WS_in_floodmethod5008);  
                    	    stream_WS.add(WS182);


                    	    }
                    	    break;

                    	default :
                    	    break loop115;
                        }
                    } while (true);

                    R_PAREN183=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_floodmethod5011);  
                    stream_R_PAREN.add(R_PAREN183);



                    // AST REWRITE
                    // elements: s, fi
                    // token labels: s, fi
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_s=new RewriteRuleTokenStream(adaptor,"token s",s);
                    RewriteRuleTokenStream stream_fi=new RewriteRuleTokenStream(adaptor,"token fi",fi);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 503:101: -> ^( $s $fi)
                    {
                        // /home/dfranusic/SmsFilterScript.g:503:104: ^( $s $fi)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_s.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_fi.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:504:5: s= FLOOD_ALL_MAX ( WS )* L_PAREN ( WS )* (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY ) ( WS )* R_PAREN
                    {
                    s=(Token)match(input,FLOOD_ALL_MAX,FOLLOW_FLOOD_ALL_MAX_in_floodmethod5029);  
                    stream_FLOOD_ALL_MAX.add(s);

                    // /home/dfranusic/SmsFilterScript.g:504:21: ( WS )*
                    loop116:
                    do {
                        int alt116=2;
                        int LA116_0 = input.LA(1);

                        if ( (LA116_0==WS) ) {
                            alt116=1;
                        }


                        switch (alt116) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:504:21: WS
                    	    {
                    	    WS184=(Token)match(input,WS,FOLLOW_WS_in_floodmethod5031);  
                    	    stream_WS.add(WS184);


                    	    }
                    	    break;

                    	default :
                    	    break loop116;
                        }
                    } while (true);

                    L_PAREN185=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_floodmethod5034);  
                    stream_L_PAREN.add(L_PAREN185);

                    // /home/dfranusic/SmsFilterScript.g:504:33: ( WS )*
                    loop117:
                    do {
                        int alt117=2;
                        int LA117_0 = input.LA(1);

                        if ( (LA117_0==WS) ) {
                            alt117=1;
                        }


                        switch (alt117) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:504:33: WS
                    	    {
                    	    WS186=(Token)match(input,WS,FOLLOW_WS_in_floodmethod5036);  
                    	    stream_WS.add(WS186);


                    	    }
                    	    break;

                    	default :
                    	    break loop117;
                        }
                    } while (true);

                    // /home/dfranusic/SmsFilterScript.g:504:37: (fi= FLOOD_MINUTE | fi= FLOOD_HOUR | fi= FLOOD_DAY )
                    int alt118=3;
                    switch ( input.LA(1) ) {
                    case FLOOD_MINUTE:
                        {
                        alt118=1;
                        }
                        break;
                    case FLOOD_HOUR:
                        {
                        alt118=2;
                        }
                        break;
                    case FLOOD_DAY:
                        {
                        alt118=3;
                        }
                        break;
                    default:
                        NoViableAltException nvae =
                            new NoViableAltException("", 118, 0, input);

                        throw nvae;
                    }

                    switch (alt118) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:504:38: fi= FLOOD_MINUTE
                            {
                            fi=(Token)match(input,FLOOD_MINUTE,FOLLOW_FLOOD_MINUTE_in_floodmethod5042);  
                            stream_FLOOD_MINUTE.add(fi);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:504:56: fi= FLOOD_HOUR
                            {
                            fi=(Token)match(input,FLOOD_HOUR,FOLLOW_FLOOD_HOUR_in_floodmethod5048);  
                            stream_FLOOD_HOUR.add(fi);


                            }
                            break;
                        case 3 :
                            // /home/dfranusic/SmsFilterScript.g:504:72: fi= FLOOD_DAY
                            {
                            fi=(Token)match(input,FLOOD_DAY,FOLLOW_FLOOD_DAY_in_floodmethod5054);  
                            stream_FLOOD_DAY.add(fi);


                            }
                            break;

                    }

                    // /home/dfranusic/SmsFilterScript.g:504:86: ( WS )*
                    loop119:
                    do {
                        int alt119=2;
                        int LA119_0 = input.LA(1);

                        if ( (LA119_0==WS) ) {
                            alt119=1;
                        }


                        switch (alt119) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:504:86: WS
                    	    {
                    	    WS187=(Token)match(input,WS,FOLLOW_WS_in_floodmethod5057);  
                    	    stream_WS.add(WS187);


                    	    }
                    	    break;

                    	default :
                    	    break loop119;
                        }
                    } while (true);

                    R_PAREN188=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_floodmethod5060);  
                    stream_R_PAREN.add(R_PAREN188);



                    // AST REWRITE
                    // elements: s, fi
                    // token labels: s, fi
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_s=new RewriteRuleTokenStream(adaptor,"token s",s);
                    RewriteRuleTokenStream stream_fi=new RewriteRuleTokenStream(adaptor,"token fi",fi);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 504:98: -> ^( $s $fi)
                    {
                        // /home/dfranusic/SmsFilterScript.g:504:101: ^( $s $fi)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_s.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_fi.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "floodmethod"

    public static class regexdef_return extends ParserRuleReturnScope {
        public String res;
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "regexdef"
    // /home/dfranusic/SmsFilterScript.g:510:1: regexdef returns [String res] : ( WORD | DIGITS | '^' | '$' | '[' | ']' | '(' | ')' | '.' | '*' | '?' | '+' | '\\\\' )* ;
    public final SmsFilterScriptParser.regexdef_return regexdef() throws RecognitionException {
        SmsFilterScriptParser.regexdef_return retval = new SmsFilterScriptParser.regexdef_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token WORD189=null;
        Token DIGITS190=null;
        Token char_literal191=null;
        Token char_literal192=null;
        Token char_literal193=null;
        Token char_literal194=null;
        Token char_literal195=null;
        Token char_literal196=null;
        Token char_literal197=null;
        Token char_literal198=null;
        Token char_literal199=null;
        Token char_literal200=null;
        Token char_literal201=null;

        Object WORD189_tree=null;
        Object DIGITS190_tree=null;
        Object char_literal191_tree=null;
        Object char_literal192_tree=null;
        Object char_literal193_tree=null;
        Object char_literal194_tree=null;
        Object char_literal195_tree=null;
        Object char_literal196_tree=null;
        Object char_literal197_tree=null;
        Object char_literal198_tree=null;
        Object char_literal199_tree=null;
        Object char_literal200_tree=null;
        Object char_literal201_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:511:3: ( ( WORD | DIGITS | '^' | '$' | '[' | ']' | '(' | ')' | '.' | '*' | '?' | '+' | '\\\\' )* )
            // /home/dfranusic/SmsFilterScript.g:511:5: ( WORD | DIGITS | '^' | '$' | '[' | ']' | '(' | ')' | '.' | '*' | '?' | '+' | '\\\\' )*
            {
            root_0 = (Object)adaptor.nil();

             retval.res = ""; 
            // /home/dfranusic/SmsFilterScript.g:511:19: ( WORD | DIGITS | '^' | '$' | '[' | ']' | '(' | ')' | '.' | '*' | '?' | '+' | '\\\\' )*
            loop121:
            do {
                int alt121=14;
                switch ( input.LA(1) ) {
                case WORD:
                    {
                    alt121=1;
                    }
                    break;
                case DIGITS:
                    {
                    alt121=2;
                    }
                    break;
                case 257:
                    {
                    alt121=3;
                    }
                    break;
                case 258:
                    {
                    alt121=4;
                    }
                    break;
                case L_SQ_B:
                    {
                    alt121=5;
                    }
                    break;
                case R_SQ_B:
                    {
                    alt121=6;
                    }
                    break;
                case L_PAREN:
                    {
                    alt121=7;
                    }
                    break;
                case R_PAREN:
                    {
                    alt121=8;
                    }
                    break;
                case 259:
                    {
                    alt121=9;
                    }
                    break;
                case ASTERISK:
                    {
                    alt121=10;
                    }
                    break;
                case 260:
                    {
                    alt121=11;
                    }
                    break;
                case PLUS:
                    {
                    alt121=12;
                    }
                    break;
                case 261:
                    {
                    alt121=13;
                    }
                    break;

                }

                switch (alt121) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:511:20: WORD
            	    {
            	    WORD189=(Token)match(input,WORD,FOLLOW_WORD_in_regexdef5092); 
            	    WORD189_tree = (Object)adaptor.create(WORD189);
            	    adaptor.addChild(root_0, WORD189_tree);

            	     retval.res += (WORD189!=null?WORD189.getText():null); 

            	    }
            	    break;
            	case 2 :
            	    // /home/dfranusic/SmsFilterScript.g:511:48: DIGITS
            	    {
            	    DIGITS190=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_regexdef5095); 
            	    DIGITS190_tree = (Object)adaptor.create(DIGITS190);
            	    adaptor.addChild(root_0, DIGITS190_tree);

            	     retval.res += (DIGITS190!=null?DIGITS190.getText():null); 

            	    }
            	    break;
            	case 3 :
            	    // /home/dfranusic/SmsFilterScript.g:511:80: '^'
            	    {
            	    char_literal191=(Token)match(input,257,FOLLOW_257_in_regexdef5098); 
            	    char_literal191_tree = (Object)adaptor.create(char_literal191);
            	    adaptor.addChild(root_0, char_literal191_tree);

            	     retval.res += '^';

            	    }
            	    break;
            	case 4 :
            	    // /home/dfranusic/SmsFilterScript.g:511:99: '$'
            	    {
            	    char_literal192=(Token)match(input,258,FOLLOW_258_in_regexdef5101); 
            	    char_literal192_tree = (Object)adaptor.create(char_literal192);
            	    adaptor.addChild(root_0, char_literal192_tree);

            	     retval.res += '$'; 

            	    }
            	    break;
            	case 5 :
            	    // /home/dfranusic/SmsFilterScript.g:511:119: '['
            	    {
            	    char_literal193=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_regexdef5104); 
            	    char_literal193_tree = (Object)adaptor.create(char_literal193);
            	    adaptor.addChild(root_0, char_literal193_tree);

            	     retval.res += '['; 

            	    }
            	    break;
            	case 6 :
            	    // /home/dfranusic/SmsFilterScript.g:511:139: ']'
            	    {
            	    char_literal194=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_regexdef5107); 
            	    char_literal194_tree = (Object)adaptor.create(char_literal194);
            	    adaptor.addChild(root_0, char_literal194_tree);

            	     retval.res += ']'; 

            	    }
            	    break;
            	case 7 :
            	    // /home/dfranusic/SmsFilterScript.g:511:159: '('
            	    {
            	    char_literal195=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regexdef5110); 
            	    char_literal195_tree = (Object)adaptor.create(char_literal195);
            	    adaptor.addChild(root_0, char_literal195_tree);

            	     retval.res += '('; 

            	    }
            	    break;
            	case 8 :
            	    // /home/dfranusic/SmsFilterScript.g:511:179: ')'
            	    {
            	    char_literal196=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regexdef5113); 
            	    char_literal196_tree = (Object)adaptor.create(char_literal196);
            	    adaptor.addChild(root_0, char_literal196_tree);

            	     retval.res += ')'; 

            	    }
            	    break;
            	case 9 :
            	    // /home/dfranusic/SmsFilterScript.g:511:199: '.'
            	    {
            	    char_literal197=(Token)match(input,259,FOLLOW_259_in_regexdef5116); 
            	    char_literal197_tree = (Object)adaptor.create(char_literal197);
            	    adaptor.addChild(root_0, char_literal197_tree);

            	     retval.res += '.'; 

            	    }
            	    break;
            	case 10 :
            	    // /home/dfranusic/SmsFilterScript.g:511:219: '*'
            	    {
            	    char_literal198=(Token)match(input,ASTERISK,FOLLOW_ASTERISK_in_regexdef5119); 
            	    char_literal198_tree = (Object)adaptor.create(char_literal198);
            	    adaptor.addChild(root_0, char_literal198_tree);

            	     retval.res += '*'; 

            	    }
            	    break;
            	case 11 :
            	    // /home/dfranusic/SmsFilterScript.g:511:239: '?'
            	    {
            	    char_literal199=(Token)match(input,260,FOLLOW_260_in_regexdef5122); 
            	    char_literal199_tree = (Object)adaptor.create(char_literal199);
            	    adaptor.addChild(root_0, char_literal199_tree);

            	     retval.res += '?'; 

            	    }
            	    break;
            	case 12 :
            	    // /home/dfranusic/SmsFilterScript.g:511:259: '+'
            	    {
            	    char_literal200=(Token)match(input,PLUS,FOLLOW_PLUS_in_regexdef5125); 
            	    char_literal200_tree = (Object)adaptor.create(char_literal200);
            	    adaptor.addChild(root_0, char_literal200_tree);

            	     retval.res += '+'; 

            	    }
            	    break;
            	case 13 :
            	    // /home/dfranusic/SmsFilterScript.g:511:279: '\\\\'
            	    {
            	    char_literal201=(Token)match(input,261,FOLLOW_261_in_regexdef5128); 
            	    char_literal201_tree = (Object)adaptor.create(char_literal201);
            	    adaptor.addChild(root_0, char_literal201_tree);

            	     retval.res += '\\'; 

            	    }
            	    break;

            	default :
            	    break loop121;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "regexdef"

    public static class regex_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "regex"
    // /home/dfranusic/SmsFilterScript.g:514:1: regex : ( L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $bstr) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d3) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bb2= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bb2) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d2) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bn3) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK b2= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $b2) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d2) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bstr) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d3) );
    public final SmsFilterScriptParser.regex_return regex() throws RecognitionException {
        SmsFilterScriptParser.regex_return retval = new SmsFilterScriptParser.regex_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token bstr=null;
        Token d2=null;
        Token L_PAREN202=null;
        Token WS203=null;
        Token REGEX_BLOCK204=null;
        Token REGEX_BLOCK205=null;
        Token WS206=null;
        Token WS207=null;
        Token WS208=null;
        Token R_PAREN209=null;
        Token L_PAREN210=null;
        Token WS211=null;
        Token REGEX_BLOCK212=null;
        Token REGEX_BLOCK213=null;
        Token WS214=null;
        Token WS215=null;
        Token WS216=null;
        Token R_PAREN217=null;
        Token L_PAREN218=null;
        Token WS219=null;
        Token REGEX_BLOCK220=null;
        Token REGEX_BLOCK221=null;
        Token WS222=null;
        Token WS223=null;
        Token REGEX_BLOCK224=null;
        Token REGEX_BLOCK225=null;
        Token WS226=null;
        Token R_PAREN227=null;
        Token L_PAREN228=null;
        Token WS229=null;
        Token REGEX_BLOCK230=null;
        Token REGEX_BLOCK231=null;
        Token WS232=null;
        Token WS233=null;
        Token WS234=null;
        Token R_PAREN235=null;
        Token L_PAREN236=null;
        Token WS237=null;
        Token REGEX_BLOCK238=null;
        Token REGEX_BLOCK239=null;
        Token WS240=null;
        Token WS241=null;
        Token WS242=null;
        Token R_PAREN243=null;
        Token L_PAREN244=null;
        Token WS245=null;
        Token REGEX_BLOCK246=null;
        Token REGEX_BLOCK247=null;
        Token WS248=null;
        Token WS249=null;
        Token REGEX_BLOCK250=null;
        Token REGEX_BLOCK251=null;
        Token WS252=null;
        Token R_PAREN253=null;
        Token L_PAREN254=null;
        Token WS255=null;
        Token REGEX_BLOCK256=null;
        Token REGEX_BLOCK257=null;
        Token WS258=null;
        Token WS259=null;
        Token REGEX_BLOCK260=null;
        Token REGEX_BLOCK261=null;
        Token WS262=null;
        Token R_PAREN263=null;
        Token L_PAREN264=null;
        Token WS265=null;
        Token REGEX_BLOCK266=null;
        Token REGEX_BLOCK267=null;
        Token WS268=null;
        Token WS269=null;
        Token REGEX_BLOCK270=null;
        Token REGEX_BLOCK271=null;
        Token WS272=null;
        Token R_PAREN273=null;
        Token L_PAREN274=null;
        Token WS275=null;
        Token REGEX_BLOCK276=null;
        Token REGEX_BLOCK277=null;
        Token WS278=null;
        Token WS279=null;
        Token WS280=null;
        Token R_PAREN281=null;
        Token L_PAREN282=null;
        Token WS283=null;
        Token REGEX_BLOCK284=null;
        Token REGEX_BLOCK285=null;
        Token WS286=null;
        Token WS287=null;
        Token WS288=null;
        Token R_PAREN289=null;
        Token L_PAREN290=null;
        Token WS291=null;
        Token REGEX_BLOCK292=null;
        Token REGEX_BLOCK293=null;
        Token WS294=null;
        Token WS295=null;
        Token WS296=null;
        Token R_PAREN297=null;
        SmsFilterScriptParser.regexdef_return r = null;

        SmsFilterScriptParser.eval_string_return b2 = null;

        SmsFilterScriptParser.comparison_return c2 = null;

        SmsFilterScriptParser.eval_string_return d3 = null;

        SmsFilterScriptParser.regexdef_return r2 = null;

        SmsFilterScriptParser.eval_string_return bb2 = null;

        SmsFilterScriptParser.eval_number_return bn2 = null;

        SmsFilterScriptParser.eval_number_return bn3 = null;


        Object bstr_tree=null;
        Object d2_tree=null;
        Object L_PAREN202_tree=null;
        Object WS203_tree=null;
        Object REGEX_BLOCK204_tree=null;
        Object REGEX_BLOCK205_tree=null;
        Object WS206_tree=null;
        Object WS207_tree=null;
        Object WS208_tree=null;
        Object R_PAREN209_tree=null;
        Object L_PAREN210_tree=null;
        Object WS211_tree=null;
        Object REGEX_BLOCK212_tree=null;
        Object REGEX_BLOCK213_tree=null;
        Object WS214_tree=null;
        Object WS215_tree=null;
        Object WS216_tree=null;
        Object R_PAREN217_tree=null;
        Object L_PAREN218_tree=null;
        Object WS219_tree=null;
        Object REGEX_BLOCK220_tree=null;
        Object REGEX_BLOCK221_tree=null;
        Object WS222_tree=null;
        Object WS223_tree=null;
        Object REGEX_BLOCK224_tree=null;
        Object REGEX_BLOCK225_tree=null;
        Object WS226_tree=null;
        Object R_PAREN227_tree=null;
        Object L_PAREN228_tree=null;
        Object WS229_tree=null;
        Object REGEX_BLOCK230_tree=null;
        Object REGEX_BLOCK231_tree=null;
        Object WS232_tree=null;
        Object WS233_tree=null;
        Object WS234_tree=null;
        Object R_PAREN235_tree=null;
        Object L_PAREN236_tree=null;
        Object WS237_tree=null;
        Object REGEX_BLOCK238_tree=null;
        Object REGEX_BLOCK239_tree=null;
        Object WS240_tree=null;
        Object WS241_tree=null;
        Object WS242_tree=null;
        Object R_PAREN243_tree=null;
        Object L_PAREN244_tree=null;
        Object WS245_tree=null;
        Object REGEX_BLOCK246_tree=null;
        Object REGEX_BLOCK247_tree=null;
        Object WS248_tree=null;
        Object WS249_tree=null;
        Object REGEX_BLOCK250_tree=null;
        Object REGEX_BLOCK251_tree=null;
        Object WS252_tree=null;
        Object R_PAREN253_tree=null;
        Object L_PAREN254_tree=null;
        Object WS255_tree=null;
        Object REGEX_BLOCK256_tree=null;
        Object REGEX_BLOCK257_tree=null;
        Object WS258_tree=null;
        Object WS259_tree=null;
        Object REGEX_BLOCK260_tree=null;
        Object REGEX_BLOCK261_tree=null;
        Object WS262_tree=null;
        Object R_PAREN263_tree=null;
        Object L_PAREN264_tree=null;
        Object WS265_tree=null;
        Object REGEX_BLOCK266_tree=null;
        Object REGEX_BLOCK267_tree=null;
        Object WS268_tree=null;
        Object WS269_tree=null;
        Object REGEX_BLOCK270_tree=null;
        Object REGEX_BLOCK271_tree=null;
        Object WS272_tree=null;
        Object R_PAREN273_tree=null;
        Object L_PAREN274_tree=null;
        Object WS275_tree=null;
        Object REGEX_BLOCK276_tree=null;
        Object REGEX_BLOCK277_tree=null;
        Object WS278_tree=null;
        Object WS279_tree=null;
        Object WS280_tree=null;
        Object R_PAREN281_tree=null;
        Object L_PAREN282_tree=null;
        Object WS283_tree=null;
        Object REGEX_BLOCK284_tree=null;
        Object REGEX_BLOCK285_tree=null;
        Object WS286_tree=null;
        Object WS287_tree=null;
        Object WS288_tree=null;
        Object R_PAREN289_tree=null;
        Object L_PAREN290_tree=null;
        Object WS291_tree=null;
        Object REGEX_BLOCK292_tree=null;
        Object REGEX_BLOCK293_tree=null;
        Object WS294_tree=null;
        Object WS295_tree=null;
        Object WS296_tree=null;
        Object R_PAREN297_tree=null;
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_STRINGLITERAL=new RewriteRuleTokenStream(adaptor,"token STRINGLITERAL");
        RewriteRuleTokenStream stream_DIGITS=new RewriteRuleTokenStream(adaptor,"token DIGITS");
        RewriteRuleTokenStream stream_R_PAREN=new RewriteRuleTokenStream(adaptor,"token R_PAREN");
        RewriteRuleTokenStream stream_L_PAREN=new RewriteRuleTokenStream(adaptor,"token L_PAREN");
        RewriteRuleTokenStream stream_REGEX_BLOCK=new RewriteRuleTokenStream(adaptor,"token REGEX_BLOCK");
        RewriteRuleSubtreeStream stream_eval_number=new RewriteRuleSubtreeStream(adaptor,"rule eval_number");
        RewriteRuleSubtreeStream stream_regexdef=new RewriteRuleSubtreeStream(adaptor,"rule regexdef");
        RewriteRuleSubtreeStream stream_eval_string=new RewriteRuleSubtreeStream(adaptor,"rule eval_string");
        RewriteRuleSubtreeStream stream_comparison=new RewriteRuleSubtreeStream(adaptor,"rule comparison");
        try {
            // /home/dfranusic/SmsFilterScript.g:516:3: ( L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $bstr) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d3) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bb2= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bb2) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d2) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bn3) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK b2= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $b2) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d2) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bstr) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d3) )
            int alt166=11;
            alt166 = dfa166.predict(input);
            switch (alt166) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:516:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN
                    {
                    L_PAREN202=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5150);  
                    stream_L_PAREN.add(L_PAREN202);

                    // /home/dfranusic/SmsFilterScript.g:516:13: ( WS )*
                    loop122:
                    do {
                        int alt122=2;
                        int LA122_0 = input.LA(1);

                        if ( (LA122_0==WS) ) {
                            alt122=1;
                        }


                        switch (alt122) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:516:13: WS
                    	    {
                    	    WS203=(Token)match(input,WS,FOLLOW_WS_in_regex5152);  
                    	    stream_WS.add(WS203);


                    	    }
                    	    break;

                    	default :
                    	    break loop122;
                        }
                    } while (true);

                    REGEX_BLOCK204=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5155);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK204);

                    pushFollow(FOLLOW_regexdef_in_regex5159);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK205=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5161);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK205);

                    pushFollow(FOLLOW_eval_string_in_regex5165);
                    b2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(b2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:516:67: ( WS )*
                    loop123:
                    do {
                        int alt123=2;
                        int LA123_0 = input.LA(1);

                        if ( (LA123_0==WS) ) {
                            alt123=1;
                        }


                        switch (alt123) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:516:67: WS
                    	    {
                    	    WS206=(Token)match(input,WS,FOLLOW_WS_in_regex5167);  
                    	    stream_WS.add(WS206);


                    	    }
                    	    break;

                    	default :
                    	    break loop123;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5172);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:516:85: ( WS )*
                    loop124:
                    do {
                        int alt124=2;
                        int LA124_0 = input.LA(1);

                        if ( (LA124_0==WS) ) {
                            alt124=1;
                        }


                        switch (alt124) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:516:85: WS
                    	    {
                    	    WS207=(Token)match(input,WS,FOLLOW_WS_in_regex5174);  
                    	    stream_WS.add(WS207);


                    	    }
                    	    break;

                    	default :
                    	    break loop124;
                        }
                    } while (true);

                    bstr=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_regex5179);  
                    stream_STRINGLITERAL.add(bstr);

                    // /home/dfranusic/SmsFilterScript.g:516:108: ( WS )*
                    loop125:
                    do {
                        int alt125=2;
                        int LA125_0 = input.LA(1);

                        if ( (LA125_0==WS) ) {
                            alt125=1;
                        }


                        switch (alt125) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:516:108: WS
                    	    {
                    	    WS208=(Token)match(input,WS,FOLLOW_WS_in_regex5181);  
                    	    stream_WS.add(WS208);


                    	    }
                    	    break;

                    	default :
                    	    break loop125;
                        }
                    } while (true);

                    R_PAREN209=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5184);  
                    stream_R_PAREN.add(R_PAREN209);



                    // AST REWRITE
                    // elements: bstr, b2, c2
                    // token labels: bstr
                    // rule labels: retval, c2, b2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_bstr=new RewriteRuleTokenStream(adaptor,"token bstr",bstr);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_b2=new RewriteRuleSubtreeStream(adaptor,"rule b2",b2!=null?b2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 516:120: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $bstr)
                    {
                        // /home/dfranusic/SmsFilterScript.g:516:123: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $bstr)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:516:139: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:516:151: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_b2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_bstr.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:518:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN
                    {
                    L_PAREN210=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5215);  
                    stream_L_PAREN.add(L_PAREN210);

                    // /home/dfranusic/SmsFilterScript.g:518:13: ( WS )*
                    loop126:
                    do {
                        int alt126=2;
                        int LA126_0 = input.LA(1);

                        if ( (LA126_0==WS) ) {
                            alt126=1;
                        }


                        switch (alt126) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:518:13: WS
                    	    {
                    	    WS211=(Token)match(input,WS,FOLLOW_WS_in_regex5217);  
                    	    stream_WS.add(WS211);


                    	    }
                    	    break;

                    	default :
                    	    break loop126;
                        }
                    } while (true);

                    REGEX_BLOCK212=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5220);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK212);

                    pushFollow(FOLLOW_regexdef_in_regex5224);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK213=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5226);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK213);

                    pushFollow(FOLLOW_eval_string_in_regex5230);
                    b2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(b2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:518:67: ( WS )*
                    loop127:
                    do {
                        int alt127=2;
                        int LA127_0 = input.LA(1);

                        if ( (LA127_0==WS) ) {
                            alt127=1;
                        }


                        switch (alt127) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:518:67: WS
                    	    {
                    	    WS214=(Token)match(input,WS,FOLLOW_WS_in_regex5232);  
                    	    stream_WS.add(WS214);


                    	    }
                    	    break;

                    	default :
                    	    break loop127;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5237);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:518:85: ( WS )*
                    loop128:
                    do {
                        int alt128=2;
                        int LA128_0 = input.LA(1);

                        if ( (LA128_0==WS) ) {
                            alt128=1;
                        }


                        switch (alt128) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:518:85: WS
                    	    {
                    	    WS215=(Token)match(input,WS,FOLLOW_WS_in_regex5239);  
                    	    stream_WS.add(WS215);


                    	    }
                    	    break;

                    	default :
                    	    break loop128;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_regex5244);
                    d3=eval_string();

                    state._fsp--;

                    stream_eval_string.add(d3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:518:104: ( WS )*
                    loop129:
                    do {
                        int alt129=2;
                        int LA129_0 = input.LA(1);

                        if ( (LA129_0==WS) ) {
                            alt129=1;
                        }


                        switch (alt129) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:518:104: WS
                    	    {
                    	    WS216=(Token)match(input,WS,FOLLOW_WS_in_regex5246);  
                    	    stream_WS.add(WS216);


                    	    }
                    	    break;

                    	default :
                    	    break loop129;
                        }
                    } while (true);

                    R_PAREN217=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5249);  
                    stream_R_PAREN.add(R_PAREN217);



                    // AST REWRITE
                    // elements: d3, c2, b2
                    // token labels: 
                    // rule labels: retval, d3, c2, b2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_d3=new RewriteRuleSubtreeStream(adaptor,"rule d3",d3!=null?d3.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_b2=new RewriteRuleSubtreeStream(adaptor,"rule b2",b2!=null?b2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 518:116: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d3)
                    {
                        // /home/dfranusic/SmsFilterScript.g:518:119: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d3)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:518:135: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:518:147: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_b2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_d3.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:520:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bb2= eval_string ( WS )* R_PAREN
                    {
                    L_PAREN218=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5280);  
                    stream_L_PAREN.add(L_PAREN218);

                    // /home/dfranusic/SmsFilterScript.g:520:13: ( WS )*
                    loop130:
                    do {
                        int alt130=2;
                        int LA130_0 = input.LA(1);

                        if ( (LA130_0==WS) ) {
                            alt130=1;
                        }


                        switch (alt130) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:520:13: WS
                    	    {
                    	    WS219=(Token)match(input,WS,FOLLOW_WS_in_regex5282);  
                    	    stream_WS.add(WS219);


                    	    }
                    	    break;

                    	default :
                    	    break loop130;
                        }
                    } while (true);

                    REGEX_BLOCK220=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5285);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK220);

                    pushFollow(FOLLOW_regexdef_in_regex5289);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK221=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5291);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK221);

                    pushFollow(FOLLOW_eval_string_in_regex5295);
                    b2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(b2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:520:67: ( WS )*
                    loop131:
                    do {
                        int alt131=2;
                        int LA131_0 = input.LA(1);

                        if ( (LA131_0==WS) ) {
                            alt131=1;
                        }


                        switch (alt131) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:520:67: WS
                    	    {
                    	    WS222=(Token)match(input,WS,FOLLOW_WS_in_regex5297);  
                    	    stream_WS.add(WS222);


                    	    }
                    	    break;

                    	default :
                    	    break loop131;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5302);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:520:85: ( WS )*
                    loop132:
                    do {
                        int alt132=2;
                        int LA132_0 = input.LA(1);

                        if ( (LA132_0==WS) ) {
                            alt132=1;
                        }


                        switch (alt132) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:520:85: WS
                    	    {
                    	    WS223=(Token)match(input,WS,FOLLOW_WS_in_regex5304);  
                    	    stream_WS.add(WS223);


                    	    }
                    	    break;

                    	default :
                    	    break loop132;
                        }
                    } while (true);

                    REGEX_BLOCK224=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5307);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK224);

                    pushFollow(FOLLOW_regexdef_in_regex5311);
                    r2=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r2.getTree());
                    REGEX_BLOCK225=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5313);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK225);

                    pushFollow(FOLLOW_eval_string_in_regex5317);
                    bb2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(bb2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:520:141: ( WS )*
                    loop133:
                    do {
                        int alt133=2;
                        int LA133_0 = input.LA(1);

                        if ( (LA133_0==WS) ) {
                            alt133=1;
                        }


                        switch (alt133) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:520:141: WS
                    	    {
                    	    WS226=(Token)match(input,WS,FOLLOW_WS_in_regex5319);  
                    	    stream_WS.add(WS226);


                    	    }
                    	    break;

                    	default :
                    	    break loop133;
                        }
                    } while (true);

                    R_PAREN227=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5322);  
                    stream_R_PAREN.add(R_PAREN227);



                    // AST REWRITE
                    // elements: bb2, b2, c2
                    // token labels: 
                    // rule labels: retval, c2, b2, bb2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_b2=new RewriteRuleSubtreeStream(adaptor,"rule b2",b2!=null?b2.tree:null);
                    RewriteRuleSubtreeStream stream_bb2=new RewriteRuleSubtreeStream(adaptor,"rule bb2",bb2!=null?bb2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 520:153: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bb2) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:520:156: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bb2) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:520:172: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:520:184: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_b2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:520:220: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bb2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:520:232: ^( RULE_REGEX_EXPR[$r2.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r2!=null?r2.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bb2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:522:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN
                    {
                    L_PAREN228=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5361);  
                    stream_L_PAREN.add(L_PAREN228);

                    // /home/dfranusic/SmsFilterScript.g:522:13: ( WS )*
                    loop134:
                    do {
                        int alt134=2;
                        int LA134_0 = input.LA(1);

                        if ( (LA134_0==WS) ) {
                            alt134=1;
                        }


                        switch (alt134) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:522:13: WS
                    	    {
                    	    WS229=(Token)match(input,WS,FOLLOW_WS_in_regex5363);  
                    	    stream_WS.add(WS229);


                    	    }
                    	    break;

                    	default :
                    	    break loop134;
                        }
                    } while (true);

                    REGEX_BLOCK230=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5366);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK230);

                    pushFollow(FOLLOW_regexdef_in_regex5370);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK231=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5372);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK231);

                    pushFollow(FOLLOW_eval_number_in_regex5376);
                    bn2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:522:68: ( WS )*
                    loop135:
                    do {
                        int alt135=2;
                        int LA135_0 = input.LA(1);

                        if ( (LA135_0==WS) ) {
                            alt135=1;
                        }


                        switch (alt135) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:522:68: WS
                    	    {
                    	    WS232=(Token)match(input,WS,FOLLOW_WS_in_regex5378);  
                    	    stream_WS.add(WS232);


                    	    }
                    	    break;

                    	default :
                    	    break loop135;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5383);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:522:86: ( WS )*
                    loop136:
                    do {
                        int alt136=2;
                        int LA136_0 = input.LA(1);

                        if ( (LA136_0==WS) ) {
                            alt136=1;
                        }


                        switch (alt136) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:522:86: WS
                    	    {
                    	    WS233=(Token)match(input,WS,FOLLOW_WS_in_regex5385);  
                    	    stream_WS.add(WS233);


                    	    }
                    	    break;

                    	default :
                    	    break loop136;
                        }
                    } while (true);

                    d2=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_regex5390);  
                    stream_DIGITS.add(d2);

                    // /home/dfranusic/SmsFilterScript.g:522:100: ( WS )*
                    loop137:
                    do {
                        int alt137=2;
                        int LA137_0 = input.LA(1);

                        if ( (LA137_0==WS) ) {
                            alt137=1;
                        }


                        switch (alt137) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:522:100: WS
                    	    {
                    	    WS234=(Token)match(input,WS,FOLLOW_WS_in_regex5392);  
                    	    stream_WS.add(WS234);


                    	    }
                    	    break;

                    	default :
                    	    break loop137;
                        }
                    } while (true);

                    R_PAREN235=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5395);  
                    stream_R_PAREN.add(R_PAREN235);



                    // AST REWRITE
                    // elements: bn2, d2, c2
                    // token labels: d2
                    // rule labels: retval, c2, bn2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_d2=new RewriteRuleTokenStream(adaptor,"token d2",d2);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_bn2=new RewriteRuleSubtreeStream(adaptor,"rule bn2",bn2!=null?bn2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 522:112: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d2)
                    {
                        // /home/dfranusic/SmsFilterScript.g:522:115: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d2)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:522:131: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:522:143: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_d2.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:524:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bn3= eval_number ( WS )* R_PAREN
                    {
                    L_PAREN236=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5426);  
                    stream_L_PAREN.add(L_PAREN236);

                    // /home/dfranusic/SmsFilterScript.g:524:13: ( WS )*
                    loop138:
                    do {
                        int alt138=2;
                        int LA138_0 = input.LA(1);

                        if ( (LA138_0==WS) ) {
                            alt138=1;
                        }


                        switch (alt138) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:524:13: WS
                    	    {
                    	    WS237=(Token)match(input,WS,FOLLOW_WS_in_regex5428);  
                    	    stream_WS.add(WS237);


                    	    }
                    	    break;

                    	default :
                    	    break loop138;
                        }
                    } while (true);

                    REGEX_BLOCK238=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5431);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK238);

                    pushFollow(FOLLOW_regexdef_in_regex5435);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK239=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5437);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK239);

                    pushFollow(FOLLOW_eval_number_in_regex5441);
                    bn2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:524:68: ( WS )*
                    loop139:
                    do {
                        int alt139=2;
                        int LA139_0 = input.LA(1);

                        if ( (LA139_0==WS) ) {
                            alt139=1;
                        }


                        switch (alt139) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:524:68: WS
                    	    {
                    	    WS240=(Token)match(input,WS,FOLLOW_WS_in_regex5443);  
                    	    stream_WS.add(WS240);


                    	    }
                    	    break;

                    	default :
                    	    break loop139;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5448);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:524:86: ( WS )*
                    loop140:
                    do {
                        int alt140=2;
                        int LA140_0 = input.LA(1);

                        if ( (LA140_0==WS) ) {
                            alt140=1;
                        }


                        switch (alt140) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:524:86: WS
                    	    {
                    	    WS241=(Token)match(input,WS,FOLLOW_WS_in_regex5450);  
                    	    stream_WS.add(WS241);


                    	    }
                    	    break;

                    	default :
                    	    break loop140;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_number_in_regex5455);
                    bn3=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:524:106: ( WS )*
                    loop141:
                    do {
                        int alt141=2;
                        int LA141_0 = input.LA(1);

                        if ( (LA141_0==WS) ) {
                            alt141=1;
                        }


                        switch (alt141) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:524:106: WS
                    	    {
                    	    WS242=(Token)match(input,WS,FOLLOW_WS_in_regex5457);  
                    	    stream_WS.add(WS242);


                    	    }
                    	    break;

                    	default :
                    	    break loop141;
                        }
                    } while (true);

                    R_PAREN243=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5460);  
                    stream_R_PAREN.add(R_PAREN243);



                    // AST REWRITE
                    // elements: c2, bn3, bn2
                    // token labels: 
                    // rule labels: retval, c2, bn2, bn3
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_bn2=new RewriteRuleSubtreeStream(adaptor,"rule bn2",bn2!=null?bn2.tree:null);
                    RewriteRuleSubtreeStream stream_bn3=new RewriteRuleSubtreeStream(adaptor,"rule bn3",bn3!=null?bn3.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 524:118: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bn3)
                    {
                        // /home/dfranusic/SmsFilterScript.g:524:121: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bn3)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:524:137: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:524:149: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_bn3.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 6 :
                    // /home/dfranusic/SmsFilterScript.g:526:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN
                    {
                    L_PAREN244=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5491);  
                    stream_L_PAREN.add(L_PAREN244);

                    // /home/dfranusic/SmsFilterScript.g:526:13: ( WS )*
                    loop142:
                    do {
                        int alt142=2;
                        int LA142_0 = input.LA(1);

                        if ( (LA142_0==WS) ) {
                            alt142=1;
                        }


                        switch (alt142) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:526:13: WS
                    	    {
                    	    WS245=(Token)match(input,WS,FOLLOW_WS_in_regex5493);  
                    	    stream_WS.add(WS245);


                    	    }
                    	    break;

                    	default :
                    	    break loop142;
                        }
                    } while (true);

                    REGEX_BLOCK246=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5496);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK246);

                    pushFollow(FOLLOW_regexdef_in_regex5500);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK247=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5502);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK247);

                    pushFollow(FOLLOW_eval_number_in_regex5506);
                    bn2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:526:68: ( WS )*
                    loop143:
                    do {
                        int alt143=2;
                        int LA143_0 = input.LA(1);

                        if ( (LA143_0==WS) ) {
                            alt143=1;
                        }


                        switch (alt143) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:526:68: WS
                    	    {
                    	    WS248=(Token)match(input,WS,FOLLOW_WS_in_regex5508);  
                    	    stream_WS.add(WS248);


                    	    }
                    	    break;

                    	default :
                    	    break loop143;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5513);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:526:86: ( WS )*
                    loop144:
                    do {
                        int alt144=2;
                        int LA144_0 = input.LA(1);

                        if ( (LA144_0==WS) ) {
                            alt144=1;
                        }


                        switch (alt144) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:526:86: WS
                    	    {
                    	    WS249=(Token)match(input,WS,FOLLOW_WS_in_regex5515);  
                    	    stream_WS.add(WS249);


                    	    }
                    	    break;

                    	default :
                    	    break loop144;
                        }
                    } while (true);

                    REGEX_BLOCK250=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5518);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK250);

                    pushFollow(FOLLOW_regexdef_in_regex5522);
                    r2=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r2.getTree());
                    REGEX_BLOCK251=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5524);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK251);

                    pushFollow(FOLLOW_eval_number_in_regex5528);
                    bn3=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:526:142: ( WS )*
                    loop145:
                    do {
                        int alt145=2;
                        int LA145_0 = input.LA(1);

                        if ( (LA145_0==WS) ) {
                            alt145=1;
                        }


                        switch (alt145) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:526:142: WS
                    	    {
                    	    WS252=(Token)match(input,WS,FOLLOW_WS_in_regex5530);  
                    	    stream_WS.add(WS252);


                    	    }
                    	    break;

                    	default :
                    	    break loop145;
                        }
                    } while (true);

                    R_PAREN253=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5533);  
                    stream_R_PAREN.add(R_PAREN253);



                    // AST REWRITE
                    // elements: c2, bn3, bn2
                    // token labels: 
                    // rule labels: retval, c2, bn2, bn3
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_bn2=new RewriteRuleSubtreeStream(adaptor,"rule bn2",bn2!=null?bn2.tree:null);
                    RewriteRuleSubtreeStream stream_bn3=new RewriteRuleSubtreeStream(adaptor,"rule bn3",bn3!=null?bn3.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 526:154: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:526:157: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:526:173: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:526:185: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:526:222: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:526:234: ^( RULE_REGEX_EXPR[$r2.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r2!=null?r2.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn3.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 7 :
                    // /home/dfranusic/SmsFilterScript.g:528:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN
                    {
                    L_PAREN254=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5572);  
                    stream_L_PAREN.add(L_PAREN254);

                    // /home/dfranusic/SmsFilterScript.g:528:13: ( WS )*
                    loop146:
                    do {
                        int alt146=2;
                        int LA146_0 = input.LA(1);

                        if ( (LA146_0==WS) ) {
                            alt146=1;
                        }


                        switch (alt146) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:528:13: WS
                    	    {
                    	    WS255=(Token)match(input,WS,FOLLOW_WS_in_regex5574);  
                    	    stream_WS.add(WS255);


                    	    }
                    	    break;

                    	default :
                    	    break loop146;
                        }
                    } while (true);

                    REGEX_BLOCK256=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5577);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK256);

                    pushFollow(FOLLOW_regexdef_in_regex5581);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK257=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5583);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK257);

                    pushFollow(FOLLOW_eval_string_in_regex5587);
                    b2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(b2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:528:67: ( WS )*
                    loop147:
                    do {
                        int alt147=2;
                        int LA147_0 = input.LA(1);

                        if ( (LA147_0==WS) ) {
                            alt147=1;
                        }


                        switch (alt147) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:528:67: WS
                    	    {
                    	    WS258=(Token)match(input,WS,FOLLOW_WS_in_regex5589);  
                    	    stream_WS.add(WS258);


                    	    }
                    	    break;

                    	default :
                    	    break loop147;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5594);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:528:85: ( WS )*
                    loop148:
                    do {
                        int alt148=2;
                        int LA148_0 = input.LA(1);

                        if ( (LA148_0==WS) ) {
                            alt148=1;
                        }


                        switch (alt148) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:528:85: WS
                    	    {
                    	    WS259=(Token)match(input,WS,FOLLOW_WS_in_regex5596);  
                    	    stream_WS.add(WS259);


                    	    }
                    	    break;

                    	default :
                    	    break loop148;
                        }
                    } while (true);

                    REGEX_BLOCK260=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5599);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK260);

                    pushFollow(FOLLOW_regexdef_in_regex5603);
                    r2=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r2.getTree());
                    REGEX_BLOCK261=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5605);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK261);

                    pushFollow(FOLLOW_eval_number_in_regex5609);
                    bn3=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:528:141: ( WS )*
                    loop149:
                    do {
                        int alt149=2;
                        int LA149_0 = input.LA(1);

                        if ( (LA149_0==WS) ) {
                            alt149=1;
                        }


                        switch (alt149) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:528:141: WS
                    	    {
                    	    WS262=(Token)match(input,WS,FOLLOW_WS_in_regex5611);  
                    	    stream_WS.add(WS262);


                    	    }
                    	    break;

                    	default :
                    	    break loop149;
                        }
                    } while (true);

                    R_PAREN263=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5614);  
                    stream_R_PAREN.add(R_PAREN263);



                    // AST REWRITE
                    // elements: bn3, c2, b2
                    // token labels: 
                    // rule labels: retval, c2, b2, bn3
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_b2=new RewriteRuleSubtreeStream(adaptor,"rule b2",b2!=null?b2.tree:null);
                    RewriteRuleSubtreeStream stream_bn3=new RewriteRuleSubtreeStream(adaptor,"rule bn3",bn3!=null?bn3.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 528:153: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:528:156: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:528:172: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:528:184: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_b2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:528:220: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:528:232: ^( RULE_REGEX_EXPR[$r2.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r2!=null?r2.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn3.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 8 :
                    // /home/dfranusic/SmsFilterScript.g:530:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK b2= eval_string ( WS )* R_PAREN
                    {
                    L_PAREN264=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5653);  
                    stream_L_PAREN.add(L_PAREN264);

                    // /home/dfranusic/SmsFilterScript.g:530:13: ( WS )*
                    loop150:
                    do {
                        int alt150=2;
                        int LA150_0 = input.LA(1);

                        if ( (LA150_0==WS) ) {
                            alt150=1;
                        }


                        switch (alt150) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:530:13: WS
                    	    {
                    	    WS265=(Token)match(input,WS,FOLLOW_WS_in_regex5655);  
                    	    stream_WS.add(WS265);


                    	    }
                    	    break;

                    	default :
                    	    break loop150;
                        }
                    } while (true);

                    REGEX_BLOCK266=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5658);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK266);

                    pushFollow(FOLLOW_regexdef_in_regex5662);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK267=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5664);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK267);

                    pushFollow(FOLLOW_eval_number_in_regex5668);
                    bn2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:530:68: ( WS )*
                    loop151:
                    do {
                        int alt151=2;
                        int LA151_0 = input.LA(1);

                        if ( (LA151_0==WS) ) {
                            alt151=1;
                        }


                        switch (alt151) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:530:68: WS
                    	    {
                    	    WS268=(Token)match(input,WS,FOLLOW_WS_in_regex5670);  
                    	    stream_WS.add(WS268);


                    	    }
                    	    break;

                    	default :
                    	    break loop151;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5675);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:530:86: ( WS )*
                    loop152:
                    do {
                        int alt152=2;
                        int LA152_0 = input.LA(1);

                        if ( (LA152_0==WS) ) {
                            alt152=1;
                        }


                        switch (alt152) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:530:86: WS
                    	    {
                    	    WS269=(Token)match(input,WS,FOLLOW_WS_in_regex5677);  
                    	    stream_WS.add(WS269);


                    	    }
                    	    break;

                    	default :
                    	    break loop152;
                        }
                    } while (true);

                    REGEX_BLOCK270=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5680);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK270);

                    pushFollow(FOLLOW_regexdef_in_regex5684);
                    r2=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r2.getTree());
                    REGEX_BLOCK271=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5686);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK271);

                    pushFollow(FOLLOW_eval_string_in_regex5690);
                    b2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(b2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:530:141: ( WS )*
                    loop153:
                    do {
                        int alt153=2;
                        int LA153_0 = input.LA(1);

                        if ( (LA153_0==WS) ) {
                            alt153=1;
                        }


                        switch (alt153) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:530:141: WS
                    	    {
                    	    WS272=(Token)match(input,WS,FOLLOW_WS_in_regex5692);  
                    	    stream_WS.add(WS272);


                    	    }
                    	    break;

                    	default :
                    	    break loop153;
                        }
                    } while (true);

                    R_PAREN273=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5695);  
                    stream_R_PAREN.add(R_PAREN273);



                    // AST REWRITE
                    // elements: b2, bn2, c2
                    // token labels: 
                    // rule labels: retval, c2, b2, bn2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_b2=new RewriteRuleSubtreeStream(adaptor,"rule b2",b2!=null?b2.tree:null);
                    RewriteRuleSubtreeStream stream_bn2=new RewriteRuleSubtreeStream(adaptor,"rule bn2",bn2!=null?bn2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 530:153: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $b2) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:530:156: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $b2) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:530:172: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:530:184: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:530:221: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $b2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:530:233: ^( RULE_REGEX_EXPR[$r2.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r2!=null?r2.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_b2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 9 :
                    // /home/dfranusic/SmsFilterScript.g:532:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN
                    {
                    L_PAREN274=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5734);  
                    stream_L_PAREN.add(L_PAREN274);

                    // /home/dfranusic/SmsFilterScript.g:532:13: ( WS )*
                    loop154:
                    do {
                        int alt154=2;
                        int LA154_0 = input.LA(1);

                        if ( (LA154_0==WS) ) {
                            alt154=1;
                        }


                        switch (alt154) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:532:13: WS
                    	    {
                    	    WS275=(Token)match(input,WS,FOLLOW_WS_in_regex5736);  
                    	    stream_WS.add(WS275);


                    	    }
                    	    break;

                    	default :
                    	    break loop154;
                        }
                    } while (true);

                    REGEX_BLOCK276=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5739);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK276);

                    pushFollow(FOLLOW_regexdef_in_regex5743);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK277=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5745);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK277);

                    pushFollow(FOLLOW_eval_string_in_regex5749);
                    b2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(b2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:532:67: ( WS )*
                    loop155:
                    do {
                        int alt155=2;
                        int LA155_0 = input.LA(1);

                        if ( (LA155_0==WS) ) {
                            alt155=1;
                        }


                        switch (alt155) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:532:67: WS
                    	    {
                    	    WS278=(Token)match(input,WS,FOLLOW_WS_in_regex5751);  
                    	    stream_WS.add(WS278);


                    	    }
                    	    break;

                    	default :
                    	    break loop155;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5756);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:532:85: ( WS )*
                    loop156:
                    do {
                        int alt156=2;
                        int LA156_0 = input.LA(1);

                        if ( (LA156_0==WS) ) {
                            alt156=1;
                        }


                        switch (alt156) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:532:85: WS
                    	    {
                    	    WS279=(Token)match(input,WS,FOLLOW_WS_in_regex5758);  
                    	    stream_WS.add(WS279);


                    	    }
                    	    break;

                    	default :
                    	    break loop156;
                        }
                    } while (true);

                    d2=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_regex5763);  
                    stream_DIGITS.add(d2);

                    // /home/dfranusic/SmsFilterScript.g:532:99: ( WS )*
                    loop157:
                    do {
                        int alt157=2;
                        int LA157_0 = input.LA(1);

                        if ( (LA157_0==WS) ) {
                            alt157=1;
                        }


                        switch (alt157) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:532:99: WS
                    	    {
                    	    WS280=(Token)match(input,WS,FOLLOW_WS_in_regex5765);  
                    	    stream_WS.add(WS280);


                    	    }
                    	    break;

                    	default :
                    	    break loop157;
                        }
                    } while (true);

                    R_PAREN281=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5768);  
                    stream_R_PAREN.add(R_PAREN281);



                    // AST REWRITE
                    // elements: c2, d2, b2
                    // token labels: d2
                    // rule labels: retval, c2, b2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_d2=new RewriteRuleTokenStream(adaptor,"token d2",d2);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_b2=new RewriteRuleSubtreeStream(adaptor,"rule b2",b2!=null?b2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 532:111: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d2)
                    {
                        // /home/dfranusic/SmsFilterScript.g:532:114: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d2)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:532:130: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:532:142: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_b2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_d2.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 10 :
                    // /home/dfranusic/SmsFilterScript.g:534:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN
                    {
                    L_PAREN282=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5799);  
                    stream_L_PAREN.add(L_PAREN282);

                    // /home/dfranusic/SmsFilterScript.g:534:13: ( WS )*
                    loop158:
                    do {
                        int alt158=2;
                        int LA158_0 = input.LA(1);

                        if ( (LA158_0==WS) ) {
                            alt158=1;
                        }


                        switch (alt158) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:534:13: WS
                    	    {
                    	    WS283=(Token)match(input,WS,FOLLOW_WS_in_regex5801);  
                    	    stream_WS.add(WS283);


                    	    }
                    	    break;

                    	default :
                    	    break loop158;
                        }
                    } while (true);

                    REGEX_BLOCK284=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5804);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK284);

                    pushFollow(FOLLOW_regexdef_in_regex5808);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK285=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5810);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK285);

                    pushFollow(FOLLOW_eval_number_in_regex5814);
                    bn2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:534:68: ( WS )*
                    loop159:
                    do {
                        int alt159=2;
                        int LA159_0 = input.LA(1);

                        if ( (LA159_0==WS) ) {
                            alt159=1;
                        }


                        switch (alt159) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:534:68: WS
                    	    {
                    	    WS286=(Token)match(input,WS,FOLLOW_WS_in_regex5816);  
                    	    stream_WS.add(WS286);


                    	    }
                    	    break;

                    	default :
                    	    break loop159;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5821);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:534:86: ( WS )*
                    loop160:
                    do {
                        int alt160=2;
                        int LA160_0 = input.LA(1);

                        if ( (LA160_0==WS) ) {
                            alt160=1;
                        }


                        switch (alt160) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:534:86: WS
                    	    {
                    	    WS287=(Token)match(input,WS,FOLLOW_WS_in_regex5823);  
                    	    stream_WS.add(WS287);


                    	    }
                    	    break;

                    	default :
                    	    break loop160;
                        }
                    } while (true);

                    bstr=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_regex5828);  
                    stream_STRINGLITERAL.add(bstr);

                    // /home/dfranusic/SmsFilterScript.g:534:109: ( WS )*
                    loop161:
                    do {
                        int alt161=2;
                        int LA161_0 = input.LA(1);

                        if ( (LA161_0==WS) ) {
                            alt161=1;
                        }


                        switch (alt161) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:534:109: WS
                    	    {
                    	    WS288=(Token)match(input,WS,FOLLOW_WS_in_regex5830);  
                    	    stream_WS.add(WS288);


                    	    }
                    	    break;

                    	default :
                    	    break loop161;
                        }
                    } while (true);

                    R_PAREN289=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5833);  
                    stream_R_PAREN.add(R_PAREN289);



                    // AST REWRITE
                    // elements: bn2, c2, bstr
                    // token labels: bstr
                    // rule labels: retval, c2, bn2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_bstr=new RewriteRuleTokenStream(adaptor,"token bstr",bstr);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_bn2=new RewriteRuleSubtreeStream(adaptor,"rule bn2",bn2!=null?bn2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 534:121: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bstr)
                    {
                        // /home/dfranusic/SmsFilterScript.g:534:124: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bstr)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:534:140: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:534:152: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_bstr.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 11 :
                    // /home/dfranusic/SmsFilterScript.g:536:5: L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN
                    {
                    L_PAREN290=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_regex5864);  
                    stream_L_PAREN.add(L_PAREN290);

                    // /home/dfranusic/SmsFilterScript.g:536:13: ( WS )*
                    loop162:
                    do {
                        int alt162=2;
                        int LA162_0 = input.LA(1);

                        if ( (LA162_0==WS) ) {
                            alt162=1;
                        }


                        switch (alt162) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:536:13: WS
                    	    {
                    	    WS291=(Token)match(input,WS,FOLLOW_WS_in_regex5866);  
                    	    stream_WS.add(WS291);


                    	    }
                    	    break;

                    	default :
                    	    break loop162;
                        }
                    } while (true);

                    REGEX_BLOCK292=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5869);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK292);

                    pushFollow(FOLLOW_regexdef_in_regex5873);
                    r=regexdef();

                    state._fsp--;

                    stream_regexdef.add(r.getTree());
                    REGEX_BLOCK293=(Token)match(input,REGEX_BLOCK,FOLLOW_REGEX_BLOCK_in_regex5875);  
                    stream_REGEX_BLOCK.add(REGEX_BLOCK293);

                    pushFollow(FOLLOW_eval_number_in_regex5879);
                    bn2=eval_number();

                    state._fsp--;

                    stream_eval_number.add(bn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:536:68: ( WS )*
                    loop163:
                    do {
                        int alt163=2;
                        int LA163_0 = input.LA(1);

                        if ( (LA163_0==WS) ) {
                            alt163=1;
                        }


                        switch (alt163) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:536:68: WS
                    	    {
                    	    WS294=(Token)match(input,WS,FOLLOW_WS_in_regex5881);  
                    	    stream_WS.add(WS294);


                    	    }
                    	    break;

                    	default :
                    	    break loop163;
                        }
                    } while (true);

                    pushFollow(FOLLOW_comparison_in_regex5886);
                    c2=comparison();

                    state._fsp--;

                    stream_comparison.add(c2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:536:86: ( WS )*
                    loop164:
                    do {
                        int alt164=2;
                        int LA164_0 = input.LA(1);

                        if ( (LA164_0==WS) ) {
                            alt164=1;
                        }


                        switch (alt164) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:536:86: WS
                    	    {
                    	    WS295=(Token)match(input,WS,FOLLOW_WS_in_regex5888);  
                    	    stream_WS.add(WS295);


                    	    }
                    	    break;

                    	default :
                    	    break loop164;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_regex5893);
                    d3=eval_string();

                    state._fsp--;

                    stream_eval_string.add(d3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:536:105: ( WS )*
                    loop165:
                    do {
                        int alt165=2;
                        int LA165_0 = input.LA(1);

                        if ( (LA165_0==WS) ) {
                            alt165=1;
                        }


                        switch (alt165) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:536:105: WS
                    	    {
                    	    WS296=(Token)match(input,WS,FOLLOW_WS_in_regex5895);  
                    	    stream_WS.add(WS296);


                    	    }
                    	    break;

                    	default :
                    	    break loop165;
                        }
                    } while (true);

                    R_PAREN297=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_regex5898);  
                    stream_R_PAREN.add(R_PAREN297);



                    // AST REWRITE
                    // elements: c2, d3, bn2
                    // token labels: 
                    // rule labels: retval, d3, c2, bn2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_d3=new RewriteRuleSubtreeStream(adaptor,"rule d3",d3!=null?d3.tree:null);
                    RewriteRuleSubtreeStream stream_c2=new RewriteRuleSubtreeStream(adaptor,"rule c2",c2!=null?c2.tree:null);
                    RewriteRuleSubtreeStream stream_bn2=new RewriteRuleSubtreeStream(adaptor,"rule bn2",bn2!=null?bn2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 536:117: -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d3)
                    {
                        // /home/dfranusic/SmsFilterScript.g:536:120: ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d3)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_ATOM, "RULE_EVAL_ATOM"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:536:136: ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX, "RULE_REGEX"), root_2);

                        // /home/dfranusic/SmsFilterScript.g:536:148: ^( RULE_REGEX_EXPR[$r.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_REGEX_EXPR, (r!=null?r.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }
                        adaptor.addChild(root_2, stream_bn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_c2.nextTree());
                        adaptor.addChild(root_1, stream_d3.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "regex"

    public static class smpp_esm_mm_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_esm_mm"
    // /home/dfranusic/SmsFilterScript.g:540:1: smpp_esm_mm : ( SMPP_ESM_MM_DEFAULT | SMPP_ESM_MM_DATAGRAM | SMPP_ESM_MM_FORWARD | SMPP_ESM_MM_STORE_FORWARD );
    public final SmsFilterScriptParser.smpp_esm_mm_return smpp_esm_mm() throws RecognitionException {
        SmsFilterScriptParser.smpp_esm_mm_return retval = new SmsFilterScriptParser.smpp_esm_mm_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set298=null;

        Object set298_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:541:3: ( SMPP_ESM_MM_DEFAULT | SMPP_ESM_MM_DATAGRAM | SMPP_ESM_MM_FORWARD | SMPP_ESM_MM_STORE_FORWARD )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set298=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_ESM_MM_DEFAULT && input.LA(1)<=SMPP_ESM_MM_STORE_FORWARD) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set298));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_esm_mm"

    public static class smpp_esm_mt_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_esm_mt"
    // /home/dfranusic/SmsFilterScript.g:547:1: smpp_esm_mt : ( SMPP_ESM_MT_DEFAULT | SMPP_ESM_MT_DELIVERY_ACK | SMPP_ESM_MT_MANUAL_USER_ACK );
    public final SmsFilterScriptParser.smpp_esm_mt_return smpp_esm_mt() throws RecognitionException {
        SmsFilterScriptParser.smpp_esm_mt_return retval = new SmsFilterScriptParser.smpp_esm_mt_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set299=null;

        Object set299_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:548:3: ( SMPP_ESM_MT_DEFAULT | SMPP_ESM_MT_DELIVERY_ACK | SMPP_ESM_MT_MANUAL_USER_ACK )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set299=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_ESM_MT_DEFAULT && input.LA(1)<=SMPP_ESM_MT_MANUAL_USER_ACK) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set299));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_esm_mt"

    public static class smpp_esm_gf_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_esm_gf"
    // /home/dfranusic/SmsFilterScript.g:553:1: smpp_esm_gf : ( SMPP_ESM_GF_NO | SMPP_ESM_GF_UDHI_INDICATOR | SMPP_ESM_GF_SET_REPLY_PATH | SMPP_ESM_GF_SET_BOTH );
    public final SmsFilterScriptParser.smpp_esm_gf_return smpp_esm_gf() throws RecognitionException {
        SmsFilterScriptParser.smpp_esm_gf_return retval = new SmsFilterScriptParser.smpp_esm_gf_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set300=null;

        Object set300_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:554:3: ( SMPP_ESM_GF_NO | SMPP_ESM_GF_UDHI_INDICATOR | SMPP_ESM_GF_SET_REPLY_PATH | SMPP_ESM_GF_SET_BOTH )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set300=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_ESM_GF_NO && input.LA(1)<=SMPP_ESM_GF_SET_BOTH) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set300));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_esm_gf"

    public static class smpp_rd_smscdr_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_rd_smscdr"
    // /home/dfranusic/SmsFilterScript.g:560:1: smpp_rd_smscdr : ( SMPP_RD_SMSCDR_NO | SMPP_RD_SMSCDR_SUCCESS_FAILURE | SMPP_RD_SMSCDR_FAILURE );
    public final SmsFilterScriptParser.smpp_rd_smscdr_return smpp_rd_smscdr() throws RecognitionException {
        SmsFilterScriptParser.smpp_rd_smscdr_return retval = new SmsFilterScriptParser.smpp_rd_smscdr_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set301=null;

        Object set301_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:561:3: ( SMPP_RD_SMSCDR_NO | SMPP_RD_SMSCDR_SUCCESS_FAILURE | SMPP_RD_SMSCDR_FAILURE )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set301=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_RD_SMSCDR_NO && input.LA(1)<=SMPP_RD_SMSCDR_FAILURE) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set301));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_rd_smscdr"

    public static class smpp_rd_smeoa_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_rd_smeoa"
    // /home/dfranusic/SmsFilterScript.g:566:1: smpp_rd_smeoa : ( SMPP_RD_SMEOA_NO | SMPP_RD_SMEOA_ACK | SMPP_RD_SMEOA_MANUAL_USER_ACK | SMPP_RD_SMEOA_BOTH );
    public final SmsFilterScriptParser.smpp_rd_smeoa_return smpp_rd_smeoa() throws RecognitionException {
        SmsFilterScriptParser.smpp_rd_smeoa_return retval = new SmsFilterScriptParser.smpp_rd_smeoa_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set302=null;

        Object set302_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:567:3: ( SMPP_RD_SMEOA_NO | SMPP_RD_SMEOA_ACK | SMPP_RD_SMEOA_MANUAL_USER_ACK | SMPP_RD_SMEOA_BOTH )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set302=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_RD_SMEOA_NO && input.LA(1)<=SMPP_RD_SMEOA_BOTH) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set302));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_rd_smeoa"

    public static class smpp_rd_in_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_rd_in"
    // /home/dfranusic/SmsFilterScript.g:573:1: smpp_rd_in : ( SMPP_RD_IN_NO | SMPP_RD_IN_YES );
    public final SmsFilterScriptParser.smpp_rd_in_return smpp_rd_in() throws RecognitionException {
        SmsFilterScriptParser.smpp_rd_in_return retval = new SmsFilterScriptParser.smpp_rd_in_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set303=null;

        Object set303_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:574:3: ( SMPP_RD_IN_NO | SMPP_RD_IN_YES )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set303=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_RD_IN_NO && input.LA(1)<=SMPP_RD_IN_YES) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set303));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_rd_in"

    public static class nai_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "nai"
    // /home/dfranusic/SmsFilterScript.g:579:1: nai : ( NAI_UNKNOWN | NAI_SUBSCRIBER_NUMBER | NAI_RESERVED_FOR_NATIONAL_USE | NAI_NATIONAL_SIGNIFICANT_NUMBER | NAI_INTERNATIONAL );
    public final SmsFilterScriptParser.nai_return nai() throws RecognitionException {
        SmsFilterScriptParser.nai_return retval = new SmsFilterScriptParser.nai_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set304=null;

        Object set304_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:580:3: ( NAI_UNKNOWN | NAI_SUBSCRIBER_NUMBER | NAI_RESERVED_FOR_NATIONAL_USE | NAI_NATIONAL_SIGNIFICANT_NUMBER | NAI_INTERNATIONAL )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set304=(Token)input.LT(1);
            if ( (input.LA(1)>=NAI_UNKNOWN && input.LA(1)<=NAI_INTERNATIONAL) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set304));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "nai"

    public static class np_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "np"
    // /home/dfranusic/SmsFilterScript.g:587:1: np : ( NP_UNKNOWN | NP_ISDN_TELEPHONE | NP_GENERIC | NP_DATA_X121 | NP_TELEX | NP_MARITIME | NP_LAND_MOBILE | NP_ISDN_MOBILE | NP_PRIVATE );
    public final SmsFilterScriptParser.np_return np() throws RecognitionException {
        SmsFilterScriptParser.np_return retval = new SmsFilterScriptParser.np_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set305=null;

        Object set305_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:588:3: ( NP_UNKNOWN | NP_ISDN_TELEPHONE | NP_GENERIC | NP_DATA_X121 | NP_TELEX | NP_MARITIME | NP_LAND_MOBILE | NP_ISDN_MOBILE | NP_PRIVATE )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set305=(Token)input.LT(1);
            if ( (input.LA(1)>=NP_UNKNOWN && input.LA(1)<=NP_PRIVATE) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set305));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "np"

    public static class smpp_np_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_np"
    // /home/dfranusic/SmsFilterScript.g:599:1: smpp_np : ( SMPP_NP_UNKNOWN | SMPP_NP_ISDN_TELEPHONE | SMPP_NP_DATA_X121 | SMPP_NP_TELEX | SMPP_NP_LAND_MOBILE | SMPP_NP_NATIONAL | SMPP_NP_PRIVATE | SMPP_NP_ERMES | SMPP_NP_INTERNET_IP | SMPP_NP_WAP_CLIENT_ID );
    public final SmsFilterScriptParser.smpp_np_return smpp_np() throws RecognitionException {
        SmsFilterScriptParser.smpp_np_return retval = new SmsFilterScriptParser.smpp_np_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set306=null;

        Object set306_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:600:3: ( SMPP_NP_UNKNOWN | SMPP_NP_ISDN_TELEPHONE | SMPP_NP_DATA_X121 | SMPP_NP_TELEX | SMPP_NP_LAND_MOBILE | SMPP_NP_NATIONAL | SMPP_NP_PRIVATE | SMPP_NP_ERMES | SMPP_NP_INTERNET_IP | SMPP_NP_WAP_CLIENT_ID )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set306=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_NP_UNKNOWN && input.LA(1)<=SMPP_NP_WAP_CLIENT_ID) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set306));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_np"

    public static class gti_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "gti"
    // /home/dfranusic/SmsFilterScript.g:613:1: gti : ( GTI_NONE | GTI_NAI | GTI_TT | GTI_TTNPE | GTI_TTNPENOA );
    public final SmsFilterScriptParser.gti_return gti() throws RecognitionException {
        SmsFilterScriptParser.gti_return retval = new SmsFilterScriptParser.gti_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set307=null;

        Object set307_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:614:3: ( GTI_NONE | GTI_NAI | GTI_TT | GTI_TTNPE | GTI_TTNPENOA )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set307=(Token)input.LT(1);
            if ( (input.LA(1)>=GTI_NONE && input.LA(1)<=GTI_TTNPENOA) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set307));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "gti"

    public static class smpp_dcs_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_dcs"
    // /home/dfranusic/SmsFilterScript.g:621:1: smpp_dcs : ( SMPP_DC_DEFAULT | SMPP_DC_IA5_ASCII | SMPP_DC_8BIT_BINARY_1 | SMPP_DC_ISO_8859_1 | SMPP_DC_8BIT_BINARY_2 | SMPP_DC_JIS | SMPP_DC_ISO_8859_5 | SMPP_DC_ISO_8859_8 | SMPP_DC_UCS2 | SMPP_DC_PICTOGRAM | SMPP_DC_ISO_2011_JP | SMPP_DC_EXTENDED_KANJI | SMPP_DC_KS_C_5601 );
    public final SmsFilterScriptParser.smpp_dcs_return smpp_dcs() throws RecognitionException {
        SmsFilterScriptParser.smpp_dcs_return retval = new SmsFilterScriptParser.smpp_dcs_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set308=null;

        Object set308_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:622:3: ( SMPP_DC_DEFAULT | SMPP_DC_IA5_ASCII | SMPP_DC_8BIT_BINARY_1 | SMPP_DC_ISO_8859_1 | SMPP_DC_8BIT_BINARY_2 | SMPP_DC_JIS | SMPP_DC_ISO_8859_5 | SMPP_DC_ISO_8859_8 | SMPP_DC_UCS2 | SMPP_DC_PICTOGRAM | SMPP_DC_ISO_2011_JP | SMPP_DC_EXTENDED_KANJI | SMPP_DC_KS_C_5601 )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set308=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_DC_DEFAULT && input.LA(1)<=SMPP_DC_KS_C_5601) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set308));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_dcs"

    public static class dcs_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "dcs"
    // /home/dfranusic/SmsFilterScript.g:637:1: dcs : ( DCS_DEFAULT | DCS_8BIT | DCS_UCS2 );
    public final SmsFilterScriptParser.dcs_return dcs() throws RecognitionException {
        SmsFilterScriptParser.dcs_return retval = new SmsFilterScriptParser.dcs_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set309=null;

        Object set309_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:638:3: ( DCS_DEFAULT | DCS_8BIT | DCS_UCS2 )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set309=(Token)input.LT(1);
            if ( (input.LA(1)>=DCS_DEFAULT && input.LA(1)<=DCS_UCS2) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set309));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "dcs"

    public static class smpp_typeofnum_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "smpp_typeofnum"
    // /home/dfranusic/SmsFilterScript.g:644:1: smpp_typeofnum : ( SMPP_TON_UNKNOWN | SMPP_TON_INTERNATIONAL | SMPP_TON_NATIONAL | SMPP_TON_NETWORK_SPECIFIC | SMPP_TON_SUBSCRIBER_NUMBER | SMPP_TON_ALPHANUMERIC | SMPP_TON_ABBREVIATED );
    public final SmsFilterScriptParser.smpp_typeofnum_return smpp_typeofnum() throws RecognitionException {
        SmsFilterScriptParser.smpp_typeofnum_return retval = new SmsFilterScriptParser.smpp_typeofnum_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set310=null;

        Object set310_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:645:3: ( SMPP_TON_UNKNOWN | SMPP_TON_INTERNATIONAL | SMPP_TON_NATIONAL | SMPP_TON_NETWORK_SPECIFIC | SMPP_TON_SUBSCRIBER_NUMBER | SMPP_TON_ALPHANUMERIC | SMPP_TON_ABBREVIATED )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set310=(Token)input.LT(1);
            if ( (input.LA(1)>=SMPP_TON_UNKNOWN && input.LA(1)<=SMPP_TON_ABBREVIATED) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set310));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "smpp_typeofnum"

    public static class typeofnum_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "typeofnum"
    // /home/dfranusic/SmsFilterScript.g:654:1: typeofnum : ( TON_UNKNOWN | TON_INTERNATIONAL | TON_NATIONAL | TON_NETWORK_SPECIFIC | TON_SUBSCRIBER_NUMBER | TON_ALPHANUMERIC | TON_ABBREVIATED );
    public final SmsFilterScriptParser.typeofnum_return typeofnum() throws RecognitionException {
        SmsFilterScriptParser.typeofnum_return retval = new SmsFilterScriptParser.typeofnum_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set311=null;

        Object set311_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:655:3: ( TON_UNKNOWN | TON_INTERNATIONAL | TON_NATIONAL | TON_NETWORK_SPECIFIC | TON_SUBSCRIBER_NUMBER | TON_ALPHANUMERIC | TON_ABBREVIATED )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set311=(Token)input.LT(1);
            if ( (input.LA(1)>=TON_UNKNOWN && input.LA(1)<=TON_ABBREVIATED) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set311));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "typeofnum"

    public static class msgtype_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "msgtype"
    // /home/dfranusic/SmsFilterScript.g:664:1: msgtype : ( MSG_TYPE_SINGLE | MSG_TYPE_CONCATENATED );
    public final SmsFilterScriptParser.msgtype_return msgtype() throws RecognitionException {
        SmsFilterScriptParser.msgtype_return retval = new SmsFilterScriptParser.msgtype_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set312=null;

        Object set312_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:665:3: ( MSG_TYPE_SINGLE | MSG_TYPE_CONCATENATED )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set312=(Token)input.LT(1);
            if ( (input.LA(1)>=MSG_TYPE_SINGLE && input.LA(1)<=MSG_TYPE_CONCATENATED) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set312));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "msgtype"

    public static class evalobj_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "evalobj"
    // /home/dfranusic/SmsFilterScript.g:669:1: evalobj : ( eval_dcs | eval_string | eval_number | eval_ton | eval_msg_type | eval_nai | eval_np | eval_gti );
    public final SmsFilterScriptParser.evalobj_return evalobj() throws RecognitionException {
        SmsFilterScriptParser.evalobj_return retval = new SmsFilterScriptParser.evalobj_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        SmsFilterScriptParser.eval_dcs_return eval_dcs313 = null;

        SmsFilterScriptParser.eval_string_return eval_string314 = null;

        SmsFilterScriptParser.eval_number_return eval_number315 = null;

        SmsFilterScriptParser.eval_ton_return eval_ton316 = null;

        SmsFilterScriptParser.eval_msg_type_return eval_msg_type317 = null;

        SmsFilterScriptParser.eval_nai_return eval_nai318 = null;

        SmsFilterScriptParser.eval_np_return eval_np319 = null;

        SmsFilterScriptParser.eval_gti_return eval_gti320 = null;



        try {
            // /home/dfranusic/SmsFilterScript.g:670:3: ( eval_dcs | eval_string | eval_number | eval_ton | eval_msg_type | eval_nai | eval_np | eval_gti )
            int alt167=8;
            switch ( input.LA(1) ) {
            case SMS_TPDU_DCS:
                {
                alt167=1;
                }
                break;
            case SMPP_IP_SOURCE:
            case SMPP_IP_DESTINATION:
            case SMPP_SYSTEM_ID:
            case SMPP_PASSWORD:
            case SMPP_SERVICE_TYPE:
            case SMPP_ORIGINATOR_ADDRESS:
            case SMPP_RECIPIENT_ADDRESS:
            case SMPP_SM:
            case SCCP_GT_CALLED_ADDRESS:
            case SCCP_GT_CALLING_ADDRESS:
            case MAP_SCOA:
            case MAP_SCDA:
            case MAP_IMSI:
            case MAP_MSISDN:
            case SMS_TPDU_ORIGINATING:
            case SMS_TPDU_DESTINATION:
            case SMS_TPDU_UD:
            case HLR_IMSI:
            case HLR_MSISDN:
            case HLR_NNN:
            case HLR_ANNN:
            case HLR_SCA:
            case HLR_RESULT_IMSI:
            case HLR_RESULT_NNN:
            case HLR_RESULT_ANNN:
                {
                alt167=2;
                }
                break;
            case SMPP_TCP_SOURCE:
            case SMPP_TCP_DESTINATION:
            case SMPP_PROTOCOL_ID:
            case SMPP_DELIVERY_TIME:
            case SMPP_VALIDITY_PERIOD:
            case SMPP_SM_DEFAULT_MSG_ID:
            case SMPP_SM_LENGTH:
            case SCCP_GT_CALLED_TT:
            case SCCP_GT_CALLING_TT:
            case M3UA_DPC:
            case M3UA_OPC:
            case DICT_SMS_TPDU_UD:
            case SPAM_SMS_TPDU_UD:
            case RP_SMS_TPDU_UD:
            case QUARANTINE_SMS_TPDU_UD:
            case MD5_SMS_TPDU_UD:
                {
                alt167=3;
                }
                break;
            case SMS_TPDU_ORIGINATING_ENC:
            case SMS_TPDU_DESTINATION_ENC:
                {
                alt167=4;
                }
                break;
            case SMS_MSG_TYPE:
                {
                alt167=5;
                }
                break;
            case SCCP_GT_CALLED_NAI:
            case SCCP_GT_CALLING_NAI:
                {
                alt167=6;
                }
                break;
            case SCCP_GT_CALLED_NP:
            case SCCP_GT_CALLING_NP:
                {
                alt167=7;
                }
                break;
            case SCCP_GT_CALLED_GTI:
            case SCCP_GT_CALLING_GTI:
                {
                alt167=8;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 167, 0, input);

                throw nvae;
            }

            switch (alt167) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:670:5: eval_dcs
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_dcs_in_evalobj6530);
                    eval_dcs313=eval_dcs();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_dcs313.getTree());

                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:671:5: eval_string
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_string_in_evalobj6536);
                    eval_string314=eval_string();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_string314.getTree());

                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:672:5: eval_number
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_number_in_evalobj6542);
                    eval_number315=eval_number();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_number315.getTree());

                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:673:5: eval_ton
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_ton_in_evalobj6548);
                    eval_ton316=eval_ton();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_ton316.getTree());

                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:674:5: eval_msg_type
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_msg_type_in_evalobj6554);
                    eval_msg_type317=eval_msg_type();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_msg_type317.getTree());

                    }
                    break;
                case 6 :
                    // /home/dfranusic/SmsFilterScript.g:675:5: eval_nai
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_nai_in_evalobj6560);
                    eval_nai318=eval_nai();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_nai318.getTree());

                    }
                    break;
                case 7 :
                    // /home/dfranusic/SmsFilterScript.g:676:5: eval_np
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_np_in_evalobj6566);
                    eval_np319=eval_np();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_np319.getTree());

                    }
                    break;
                case 8 :
                    // /home/dfranusic/SmsFilterScript.g:677:5: eval_gti
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_eval_gti_in_evalobj6572);
                    eval_gti320=eval_gti();

                    state._fsp--;

                    adaptor.addChild(root_0, eval_gti320.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "evalobj"

    public static class eval_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "eval"
    // /home/dfranusic/SmsFilterScript.g:680:1: eval : ( evalsimple | regex );
    public final SmsFilterScriptParser.eval_return eval() throws RecognitionException {
        SmsFilterScriptParser.eval_return retval = new SmsFilterScriptParser.eval_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        SmsFilterScriptParser.evalsimple_return evalsimple321 = null;

        SmsFilterScriptParser.regex_return regex322 = null;



        try {
            // /home/dfranusic/SmsFilterScript.g:681:3: ( evalsimple | regex )
            int alt168=2;
            alt168 = dfa168.predict(input);
            switch (alt168) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:681:5: evalsimple
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_evalsimple_in_eval6585);
                    evalsimple321=evalsimple();

                    state._fsp--;

                    adaptor.addChild(root_0, evalsimple321.getTree());

                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:682:5: regex
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_regex_in_eval6591);
                    regex322=regex();

                    state._fsp--;

                    adaptor.addChild(root_0, regex322.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "eval"

    public static class curlyblock_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "curlyblock"
    // /home/dfranusic/SmsFilterScript.g:685:1: curlyblock : L_CR_B ( curlyblock | WS | rule | rulebody | modify | modifybody | m3ua )* R_CR_B ;
    public final SmsFilterScriptParser.curlyblock_return curlyblock() throws RecognitionException {
        SmsFilterScriptParser.curlyblock_return retval = new SmsFilterScriptParser.curlyblock_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token L_CR_B323=null;
        Token WS325=null;
        Token R_CR_B331=null;
        SmsFilterScriptParser.curlyblock_return curlyblock324 = null;

        SmsFilterScriptParser.rule_return rule326 = null;

        SmsFilterScriptParser.rulebody_return rulebody327 = null;

        SmsFilterScriptParser.modify_return modify328 = null;

        SmsFilterScriptParser.modifybody_return modifybody329 = null;

        SmsFilterScriptParser.m3ua_return m3ua330 = null;


        Object L_CR_B323_tree=null;
        Object WS325_tree=null;
        Object R_CR_B331_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:686:3: ( L_CR_B ( curlyblock | WS | rule | rulebody | modify | modifybody | m3ua )* R_CR_B )
            // /home/dfranusic/SmsFilterScript.g:686:5: L_CR_B ( curlyblock | WS | rule | rulebody | modify | modifybody | m3ua )* R_CR_B
            {
            root_0 = (Object)adaptor.nil();

            L_CR_B323=(Token)match(input,L_CR_B,FOLLOW_L_CR_B_in_curlyblock6604); 
            // /home/dfranusic/SmsFilterScript.g:686:13: ( curlyblock | WS | rule | rulebody | modify | modifybody | m3ua )*
            loop169:
            do {
                int alt169=8;
                alt169 = dfa169.predict(input);
                switch (alt169) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:686:14: curlyblock
            	    {
            	    pushFollow(FOLLOW_curlyblock_in_curlyblock6608);
            	    curlyblock324=curlyblock();

            	    state._fsp--;

            	    adaptor.addChild(root_0, curlyblock324.getTree());

            	    }
            	    break;
            	case 2 :
            	    // /home/dfranusic/SmsFilterScript.g:686:27: WS
            	    {
            	    WS325=(Token)match(input,WS,FOLLOW_WS_in_curlyblock6612); 
            	    WS325_tree = (Object)adaptor.create(WS325);
            	    adaptor.addChild(root_0, WS325_tree);


            	    }
            	    break;
            	case 3 :
            	    // /home/dfranusic/SmsFilterScript.g:686:32: rule
            	    {
            	    pushFollow(FOLLOW_rule_in_curlyblock6616);
            	    rule326=rule();

            	    state._fsp--;

            	    adaptor.addChild(root_0, rule326.getTree());

            	    }
            	    break;
            	case 4 :
            	    // /home/dfranusic/SmsFilterScript.g:686:39: rulebody
            	    {
            	    pushFollow(FOLLOW_rulebody_in_curlyblock6620);
            	    rulebody327=rulebody();

            	    state._fsp--;

            	    adaptor.addChild(root_0, rulebody327.getTree());

            	    }
            	    break;
            	case 5 :
            	    // /home/dfranusic/SmsFilterScript.g:686:50: modify
            	    {
            	    pushFollow(FOLLOW_modify_in_curlyblock6624);
            	    modify328=modify();

            	    state._fsp--;

            	    adaptor.addChild(root_0, modify328.getTree());

            	    }
            	    break;
            	case 6 :
            	    // /home/dfranusic/SmsFilterScript.g:686:59: modifybody
            	    {
            	    pushFollow(FOLLOW_modifybody_in_curlyblock6628);
            	    modifybody329=modifybody();

            	    state._fsp--;

            	    adaptor.addChild(root_0, modifybody329.getTree());

            	    }
            	    break;
            	case 7 :
            	    // /home/dfranusic/SmsFilterScript.g:686:72: m3ua
            	    {
            	    pushFollow(FOLLOW_m3ua_in_curlyblock6632);
            	    m3ua330=m3ua();

            	    state._fsp--;

            	    adaptor.addChild(root_0, m3ua330.getTree());

            	    }
            	    break;

            	default :
            	    break loop169;
                }
            } while (true);

            R_CR_B331=(Token)match(input,R_CR_B,FOLLOW_R_CR_B_in_curlyblock6636); 

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "curlyblock"

    public static class m3ua_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "m3ua"
    // /home/dfranusic/SmsFilterScript.g:690:1: m3ua : COLON ( WS )* L_PAREN ( WS )* lbl= WORD (def= ASTERISK )* ( WS )* COLON ( WS )* lip= IP ( WS )* COLON ( WS )* lp= DIGITS ( WS )* COLON ( WS )* rip= IP ( WS )* COLON ( WS )* rp= DIGITS ( WS )* COLON ( WS )* dpc= DIGITS ( WS )* COLON ( WS )* opc= DIGITS ( WS )* COLON ( WS )* napp= DIGITS ( WS )* COLON ( WS )* rc= DIGITS ( WS )* COLON ( WS )* sc= DIGITS ( WS )* R_PAREN ( WS )* STMTSEP -> ^( M3UA_CONNECTION ^( CONNECTION_LABEL $lbl ( $def)? ) ^( LOCAL_IP $lip) ^( LOCAL_PORT $lp) ^( REMOTE_IP $rip) ^( REMOTE_PORT $rp) ^( CONN_DPC $dpc) ^( CONN_OPC $opc) ^( CONN_N_APP $napp) ^( CONN_RC $rc) ^( CONN_SC $sc) ) ;
    public final SmsFilterScriptParser.m3ua_return m3ua() throws RecognitionException {
        SmsFilterScriptParser.m3ua_return retval = new SmsFilterScriptParser.m3ua_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token lbl=null;
        Token def=null;
        Token lip=null;
        Token lp=null;
        Token rip=null;
        Token rp=null;
        Token dpc=null;
        Token opc=null;
        Token napp=null;
        Token rc=null;
        Token sc=null;
        Token COLON332=null;
        Token WS333=null;
        Token L_PAREN334=null;
        Token WS335=null;
        Token WS336=null;
        Token COLON337=null;
        Token WS338=null;
        Token WS339=null;
        Token COLON340=null;
        Token WS341=null;
        Token WS342=null;
        Token COLON343=null;
        Token WS344=null;
        Token WS345=null;
        Token COLON346=null;
        Token WS347=null;
        Token WS348=null;
        Token COLON349=null;
        Token WS350=null;
        Token WS351=null;
        Token COLON352=null;
        Token WS353=null;
        Token WS354=null;
        Token COLON355=null;
        Token WS356=null;
        Token WS357=null;
        Token COLON358=null;
        Token WS359=null;
        Token WS360=null;
        Token COLON361=null;
        Token WS362=null;
        Token WS363=null;
        Token R_PAREN364=null;
        Token WS365=null;
        Token STMTSEP366=null;

        Object lbl_tree=null;
        Object def_tree=null;
        Object lip_tree=null;
        Object lp_tree=null;
        Object rip_tree=null;
        Object rp_tree=null;
        Object dpc_tree=null;
        Object opc_tree=null;
        Object napp_tree=null;
        Object rc_tree=null;
        Object sc_tree=null;
        Object COLON332_tree=null;
        Object WS333_tree=null;
        Object L_PAREN334_tree=null;
        Object WS335_tree=null;
        Object WS336_tree=null;
        Object COLON337_tree=null;
        Object WS338_tree=null;
        Object WS339_tree=null;
        Object COLON340_tree=null;
        Object WS341_tree=null;
        Object WS342_tree=null;
        Object COLON343_tree=null;
        Object WS344_tree=null;
        Object WS345_tree=null;
        Object COLON346_tree=null;
        Object WS347_tree=null;
        Object WS348_tree=null;
        Object COLON349_tree=null;
        Object WS350_tree=null;
        Object WS351_tree=null;
        Object COLON352_tree=null;
        Object WS353_tree=null;
        Object WS354_tree=null;
        Object COLON355_tree=null;
        Object WS356_tree=null;
        Object WS357_tree=null;
        Object COLON358_tree=null;
        Object WS359_tree=null;
        Object WS360_tree=null;
        Object COLON361_tree=null;
        Object WS362_tree=null;
        Object WS363_tree=null;
        Object R_PAREN364_tree=null;
        Object WS365_tree=null;
        Object STMTSEP366_tree=null;
        RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_WORD=new RewriteRuleTokenStream(adaptor,"token WORD");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_IP=new RewriteRuleTokenStream(adaptor,"token IP");
        RewriteRuleTokenStream stream_DIGITS=new RewriteRuleTokenStream(adaptor,"token DIGITS");
        RewriteRuleTokenStream stream_R_PAREN=new RewriteRuleTokenStream(adaptor,"token R_PAREN");
        RewriteRuleTokenStream stream_STMTSEP=new RewriteRuleTokenStream(adaptor,"token STMTSEP");
        RewriteRuleTokenStream stream_ASTERISK=new RewriteRuleTokenStream(adaptor,"token ASTERISK");
        RewriteRuleTokenStream stream_L_PAREN=new RewriteRuleTokenStream(adaptor,"token L_PAREN");

        try {
            // /home/dfranusic/SmsFilterScript.g:691:3: ( COLON ( WS )* L_PAREN ( WS )* lbl= WORD (def= ASTERISK )* ( WS )* COLON ( WS )* lip= IP ( WS )* COLON ( WS )* lp= DIGITS ( WS )* COLON ( WS )* rip= IP ( WS )* COLON ( WS )* rp= DIGITS ( WS )* COLON ( WS )* dpc= DIGITS ( WS )* COLON ( WS )* opc= DIGITS ( WS )* COLON ( WS )* napp= DIGITS ( WS )* COLON ( WS )* rc= DIGITS ( WS )* COLON ( WS )* sc= DIGITS ( WS )* R_PAREN ( WS )* STMTSEP -> ^( M3UA_CONNECTION ^( CONNECTION_LABEL $lbl ( $def)? ) ^( LOCAL_IP $lip) ^( LOCAL_PORT $lp) ^( REMOTE_IP $rip) ^( REMOTE_PORT $rp) ^( CONN_DPC $dpc) ^( CONN_OPC $opc) ^( CONN_N_APP $napp) ^( CONN_RC $rc) ^( CONN_SC $sc) ) )
            // /home/dfranusic/SmsFilterScript.g:691:5: COLON ( WS )* L_PAREN ( WS )* lbl= WORD (def= ASTERISK )* ( WS )* COLON ( WS )* lip= IP ( WS )* COLON ( WS )* lp= DIGITS ( WS )* COLON ( WS )* rip= IP ( WS )* COLON ( WS )* rp= DIGITS ( WS )* COLON ( WS )* dpc= DIGITS ( WS )* COLON ( WS )* opc= DIGITS ( WS )* COLON ( WS )* napp= DIGITS ( WS )* COLON ( WS )* rc= DIGITS ( WS )* COLON ( WS )* sc= DIGITS ( WS )* R_PAREN ( WS )* STMTSEP
            {
            COLON332=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6651);  
            stream_COLON.add(COLON332);

            // /home/dfranusic/SmsFilterScript.g:691:11: ( WS )*
            loop170:
            do {
                int alt170=2;
                int LA170_0 = input.LA(1);

                if ( (LA170_0==WS) ) {
                    alt170=1;
                }


                switch (alt170) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:11: WS
            	    {
            	    WS333=(Token)match(input,WS,FOLLOW_WS_in_m3ua6653);  
            	    stream_WS.add(WS333);


            	    }
            	    break;

            	default :
            	    break loop170;
                }
            } while (true);

            L_PAREN334=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_m3ua6656);  
            stream_L_PAREN.add(L_PAREN334);

            // /home/dfranusic/SmsFilterScript.g:691:23: ( WS )*
            loop171:
            do {
                int alt171=2;
                int LA171_0 = input.LA(1);

                if ( (LA171_0==WS) ) {
                    alt171=1;
                }


                switch (alt171) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:23: WS
            	    {
            	    WS335=(Token)match(input,WS,FOLLOW_WS_in_m3ua6658);  
            	    stream_WS.add(WS335);


            	    }
            	    break;

            	default :
            	    break loop171;
                }
            } while (true);

            lbl=(Token)match(input,WORD,FOLLOW_WORD_in_m3ua6663);  
            stream_WORD.add(lbl);

            // /home/dfranusic/SmsFilterScript.g:691:39: (def= ASTERISK )*
            loop172:
            do {
                int alt172=2;
                int LA172_0 = input.LA(1);

                if ( (LA172_0==ASTERISK) ) {
                    alt172=1;
                }


                switch (alt172) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:39: def= ASTERISK
            	    {
            	    def=(Token)match(input,ASTERISK,FOLLOW_ASTERISK_in_m3ua6667);  
            	    stream_ASTERISK.add(def);


            	    }
            	    break;

            	default :
            	    break loop172;
                }
            } while (true);

            // /home/dfranusic/SmsFilterScript.g:691:50: ( WS )*
            loop173:
            do {
                int alt173=2;
                int LA173_0 = input.LA(1);

                if ( (LA173_0==WS) ) {
                    alt173=1;
                }


                switch (alt173) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:50: WS
            	    {
            	    WS336=(Token)match(input,WS,FOLLOW_WS_in_m3ua6670);  
            	    stream_WS.add(WS336);


            	    }
            	    break;

            	default :
            	    break loop173;
                }
            } while (true);

            COLON337=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6673);  
            stream_COLON.add(COLON337);

            // /home/dfranusic/SmsFilterScript.g:691:60: ( WS )*
            loop174:
            do {
                int alt174=2;
                int LA174_0 = input.LA(1);

                if ( (LA174_0==WS) ) {
                    alt174=1;
                }


                switch (alt174) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:60: WS
            	    {
            	    WS338=(Token)match(input,WS,FOLLOW_WS_in_m3ua6675);  
            	    stream_WS.add(WS338);


            	    }
            	    break;

            	default :
            	    break loop174;
                }
            } while (true);

            lip=(Token)match(input,IP,FOLLOW_IP_in_m3ua6680);  
            stream_IP.add(lip);

            // /home/dfranusic/SmsFilterScript.g:691:71: ( WS )*
            loop175:
            do {
                int alt175=2;
                int LA175_0 = input.LA(1);

                if ( (LA175_0==WS) ) {
                    alt175=1;
                }


                switch (alt175) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:71: WS
            	    {
            	    WS339=(Token)match(input,WS,FOLLOW_WS_in_m3ua6682);  
            	    stream_WS.add(WS339);


            	    }
            	    break;

            	default :
            	    break loop175;
                }
            } while (true);

            COLON340=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6685);  
            stream_COLON.add(COLON340);

            // /home/dfranusic/SmsFilterScript.g:691:81: ( WS )*
            loop176:
            do {
                int alt176=2;
                int LA176_0 = input.LA(1);

                if ( (LA176_0==WS) ) {
                    alt176=1;
                }


                switch (alt176) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:81: WS
            	    {
            	    WS341=(Token)match(input,WS,FOLLOW_WS_in_m3ua6687);  
            	    stream_WS.add(WS341);


            	    }
            	    break;

            	default :
            	    break loop176;
                }
            } while (true);

            lp=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6692);  
            stream_DIGITS.add(lp);

            // /home/dfranusic/SmsFilterScript.g:691:95: ( WS )*
            loop177:
            do {
                int alt177=2;
                int LA177_0 = input.LA(1);

                if ( (LA177_0==WS) ) {
                    alt177=1;
                }


                switch (alt177) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:95: WS
            	    {
            	    WS342=(Token)match(input,WS,FOLLOW_WS_in_m3ua6694);  
            	    stream_WS.add(WS342);


            	    }
            	    break;

            	default :
            	    break loop177;
                }
            } while (true);

            COLON343=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6697);  
            stream_COLON.add(COLON343);

            // /home/dfranusic/SmsFilterScript.g:691:105: ( WS )*
            loop178:
            do {
                int alt178=2;
                int LA178_0 = input.LA(1);

                if ( (LA178_0==WS) ) {
                    alt178=1;
                }


                switch (alt178) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:105: WS
            	    {
            	    WS344=(Token)match(input,WS,FOLLOW_WS_in_m3ua6699);  
            	    stream_WS.add(WS344);


            	    }
            	    break;

            	default :
            	    break loop178;
                }
            } while (true);

            rip=(Token)match(input,IP,FOLLOW_IP_in_m3ua6704);  
            stream_IP.add(rip);

            // /home/dfranusic/SmsFilterScript.g:691:116: ( WS )*
            loop179:
            do {
                int alt179=2;
                int LA179_0 = input.LA(1);

                if ( (LA179_0==WS) ) {
                    alt179=1;
                }


                switch (alt179) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:116: WS
            	    {
            	    WS345=(Token)match(input,WS,FOLLOW_WS_in_m3ua6706);  
            	    stream_WS.add(WS345);


            	    }
            	    break;

            	default :
            	    break loop179;
                }
            } while (true);

            COLON346=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6709);  
            stream_COLON.add(COLON346);

            // /home/dfranusic/SmsFilterScript.g:691:126: ( WS )*
            loop180:
            do {
                int alt180=2;
                int LA180_0 = input.LA(1);

                if ( (LA180_0==WS) ) {
                    alt180=1;
                }


                switch (alt180) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:126: WS
            	    {
            	    WS347=(Token)match(input,WS,FOLLOW_WS_in_m3ua6711);  
            	    stream_WS.add(WS347);


            	    }
            	    break;

            	default :
            	    break loop180;
                }
            } while (true);

            rp=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6716);  
            stream_DIGITS.add(rp);

            // /home/dfranusic/SmsFilterScript.g:691:140: ( WS )*
            loop181:
            do {
                int alt181=2;
                int LA181_0 = input.LA(1);

                if ( (LA181_0==WS) ) {
                    alt181=1;
                }


                switch (alt181) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:140: WS
            	    {
            	    WS348=(Token)match(input,WS,FOLLOW_WS_in_m3ua6718);  
            	    stream_WS.add(WS348);


            	    }
            	    break;

            	default :
            	    break loop181;
                }
            } while (true);

            COLON349=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6721);  
            stream_COLON.add(COLON349);

            // /home/dfranusic/SmsFilterScript.g:691:150: ( WS )*
            loop182:
            do {
                int alt182=2;
                int LA182_0 = input.LA(1);

                if ( (LA182_0==WS) ) {
                    alt182=1;
                }


                switch (alt182) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:150: WS
            	    {
            	    WS350=(Token)match(input,WS,FOLLOW_WS_in_m3ua6723);  
            	    stream_WS.add(WS350);


            	    }
            	    break;

            	default :
            	    break loop182;
                }
            } while (true);

            dpc=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6728);  
            stream_DIGITS.add(dpc);

            // /home/dfranusic/SmsFilterScript.g:691:165: ( WS )*
            loop183:
            do {
                int alt183=2;
                int LA183_0 = input.LA(1);

                if ( (LA183_0==WS) ) {
                    alt183=1;
                }


                switch (alt183) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:165: WS
            	    {
            	    WS351=(Token)match(input,WS,FOLLOW_WS_in_m3ua6730);  
            	    stream_WS.add(WS351);


            	    }
            	    break;

            	default :
            	    break loop183;
                }
            } while (true);

            COLON352=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6733);  
            stream_COLON.add(COLON352);

            // /home/dfranusic/SmsFilterScript.g:691:175: ( WS )*
            loop184:
            do {
                int alt184=2;
                int LA184_0 = input.LA(1);

                if ( (LA184_0==WS) ) {
                    alt184=1;
                }


                switch (alt184) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:175: WS
            	    {
            	    WS353=(Token)match(input,WS,FOLLOW_WS_in_m3ua6735);  
            	    stream_WS.add(WS353);


            	    }
            	    break;

            	default :
            	    break loop184;
                }
            } while (true);

            opc=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6740);  
            stream_DIGITS.add(opc);

            // /home/dfranusic/SmsFilterScript.g:691:190: ( WS )*
            loop185:
            do {
                int alt185=2;
                int LA185_0 = input.LA(1);

                if ( (LA185_0==WS) ) {
                    alt185=1;
                }


                switch (alt185) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:190: WS
            	    {
            	    WS354=(Token)match(input,WS,FOLLOW_WS_in_m3ua6742);  
            	    stream_WS.add(WS354);


            	    }
            	    break;

            	default :
            	    break loop185;
                }
            } while (true);

            COLON355=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6745);  
            stream_COLON.add(COLON355);

            // /home/dfranusic/SmsFilterScript.g:691:200: ( WS )*
            loop186:
            do {
                int alt186=2;
                int LA186_0 = input.LA(1);

                if ( (LA186_0==WS) ) {
                    alt186=1;
                }


                switch (alt186) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:200: WS
            	    {
            	    WS356=(Token)match(input,WS,FOLLOW_WS_in_m3ua6747);  
            	    stream_WS.add(WS356);


            	    }
            	    break;

            	default :
            	    break loop186;
                }
            } while (true);

            napp=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6752);  
            stream_DIGITS.add(napp);

            // /home/dfranusic/SmsFilterScript.g:691:216: ( WS )*
            loop187:
            do {
                int alt187=2;
                int LA187_0 = input.LA(1);

                if ( (LA187_0==WS) ) {
                    alt187=1;
                }


                switch (alt187) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:216: WS
            	    {
            	    WS357=(Token)match(input,WS,FOLLOW_WS_in_m3ua6754);  
            	    stream_WS.add(WS357);


            	    }
            	    break;

            	default :
            	    break loop187;
                }
            } while (true);

            COLON358=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6757);  
            stream_COLON.add(COLON358);

            // /home/dfranusic/SmsFilterScript.g:691:226: ( WS )*
            loop188:
            do {
                int alt188=2;
                int LA188_0 = input.LA(1);

                if ( (LA188_0==WS) ) {
                    alt188=1;
                }


                switch (alt188) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:226: WS
            	    {
            	    WS359=(Token)match(input,WS,FOLLOW_WS_in_m3ua6759);  
            	    stream_WS.add(WS359);


            	    }
            	    break;

            	default :
            	    break loop188;
                }
            } while (true);

            rc=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6764);  
            stream_DIGITS.add(rc);

            // /home/dfranusic/SmsFilterScript.g:691:240: ( WS )*
            loop189:
            do {
                int alt189=2;
                int LA189_0 = input.LA(1);

                if ( (LA189_0==WS) ) {
                    alt189=1;
                }


                switch (alt189) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:240: WS
            	    {
            	    WS360=(Token)match(input,WS,FOLLOW_WS_in_m3ua6766);  
            	    stream_WS.add(WS360);


            	    }
            	    break;

            	default :
            	    break loop189;
                }
            } while (true);

            COLON361=(Token)match(input,COLON,FOLLOW_COLON_in_m3ua6769);  
            stream_COLON.add(COLON361);

            // /home/dfranusic/SmsFilterScript.g:691:250: ( WS )*
            loop190:
            do {
                int alt190=2;
                int LA190_0 = input.LA(1);

                if ( (LA190_0==WS) ) {
                    alt190=1;
                }


                switch (alt190) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:250: WS
            	    {
            	    WS362=(Token)match(input,WS,FOLLOW_WS_in_m3ua6771);  
            	    stream_WS.add(WS362);


            	    }
            	    break;

            	default :
            	    break loop190;
                }
            } while (true);

            sc=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_m3ua6776);  
            stream_DIGITS.add(sc);

            // /home/dfranusic/SmsFilterScript.g:691:264: ( WS )*
            loop191:
            do {
                int alt191=2;
                int LA191_0 = input.LA(1);

                if ( (LA191_0==WS) ) {
                    alt191=1;
                }


                switch (alt191) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:264: WS
            	    {
            	    WS363=(Token)match(input,WS,FOLLOW_WS_in_m3ua6778);  
            	    stream_WS.add(WS363);


            	    }
            	    break;

            	default :
            	    break loop191;
                }
            } while (true);

            R_PAREN364=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_m3ua6781);  
            stream_R_PAREN.add(R_PAREN364);

            // /home/dfranusic/SmsFilterScript.g:691:276: ( WS )*
            loop192:
            do {
                int alt192=2;
                int LA192_0 = input.LA(1);

                if ( (LA192_0==WS) ) {
                    alt192=1;
                }


                switch (alt192) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:691:276: WS
            	    {
            	    WS365=(Token)match(input,WS,FOLLOW_WS_in_m3ua6783);  
            	    stream_WS.add(WS365);


            	    }
            	    break;

            	default :
            	    break loop192;
                }
            } while (true);

            STMTSEP366=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_m3ua6786);  
            stream_STMTSEP.add(STMTSEP366);



            // AST REWRITE
            // elements: rc, opc, lip, dpc, lbl, napp, sc, lp, rip, def, rp
            // token labels: rip, rp, dpc, def, lip, lbl, lp, napp, sc, opc, rc
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_rip=new RewriteRuleTokenStream(adaptor,"token rip",rip);
            RewriteRuleTokenStream stream_rp=new RewriteRuleTokenStream(adaptor,"token rp",rp);
            RewriteRuleTokenStream stream_dpc=new RewriteRuleTokenStream(adaptor,"token dpc",dpc);
            RewriteRuleTokenStream stream_def=new RewriteRuleTokenStream(adaptor,"token def",def);
            RewriteRuleTokenStream stream_lip=new RewriteRuleTokenStream(adaptor,"token lip",lip);
            RewriteRuleTokenStream stream_lbl=new RewriteRuleTokenStream(adaptor,"token lbl",lbl);
            RewriteRuleTokenStream stream_lp=new RewriteRuleTokenStream(adaptor,"token lp",lp);
            RewriteRuleTokenStream stream_napp=new RewriteRuleTokenStream(adaptor,"token napp",napp);
            RewriteRuleTokenStream stream_sc=new RewriteRuleTokenStream(adaptor,"token sc",sc);
            RewriteRuleTokenStream stream_opc=new RewriteRuleTokenStream(adaptor,"token opc",opc);
            RewriteRuleTokenStream stream_rc=new RewriteRuleTokenStream(adaptor,"token rc",rc);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 691:288: -> ^( M3UA_CONNECTION ^( CONNECTION_LABEL $lbl ( $def)? ) ^( LOCAL_IP $lip) ^( LOCAL_PORT $lp) ^( REMOTE_IP $rip) ^( REMOTE_PORT $rp) ^( CONN_DPC $dpc) ^( CONN_OPC $opc) ^( CONN_N_APP $napp) ^( CONN_RC $rc) ^( CONN_SC $sc) )
            {
                // /home/dfranusic/SmsFilterScript.g:691:291: ^( M3UA_CONNECTION ^( CONNECTION_LABEL $lbl ( $def)? ) ^( LOCAL_IP $lip) ^( LOCAL_PORT $lp) ^( REMOTE_IP $rip) ^( REMOTE_PORT $rp) ^( CONN_DPC $dpc) ^( CONN_OPC $opc) ^( CONN_N_APP $napp) ^( CONN_RC $rc) ^( CONN_SC $sc) )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(M3UA_CONNECTION, "M3UA_CONNECTION"), root_1);

                // /home/dfranusic/SmsFilterScript.g:691:308: ^( CONNECTION_LABEL $lbl ( $def)? )
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONNECTION_LABEL, "CONNECTION_LABEL"), root_2);

                adaptor.addChild(root_2, stream_lbl.nextNode());
                // /home/dfranusic/SmsFilterScript.g:691:332: ( $def)?
                if ( stream_def.hasNext() ) {
                    adaptor.addChild(root_2, stream_def.nextNode());

                }
                stream_def.reset();

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:339: ^( LOCAL_IP $lip)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(LOCAL_IP, "LOCAL_IP"), root_2);

                adaptor.addChild(root_2, stream_lip.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:356: ^( LOCAL_PORT $lp)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(LOCAL_PORT, "LOCAL_PORT"), root_2);

                adaptor.addChild(root_2, stream_lp.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:374: ^( REMOTE_IP $rip)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(REMOTE_IP, "REMOTE_IP"), root_2);

                adaptor.addChild(root_2, stream_rip.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:392: ^( REMOTE_PORT $rp)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(REMOTE_PORT, "REMOTE_PORT"), root_2);

                adaptor.addChild(root_2, stream_rp.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:411: ^( CONN_DPC $dpc)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONN_DPC, "CONN_DPC"), root_2);

                adaptor.addChild(root_2, stream_dpc.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:428: ^( CONN_OPC $opc)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONN_OPC, "CONN_OPC"), root_2);

                adaptor.addChild(root_2, stream_opc.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:445: ^( CONN_N_APP $napp)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONN_N_APP, "CONN_N_APP"), root_2);

                adaptor.addChild(root_2, stream_napp.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:465: ^( CONN_RC $rc)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONN_RC, "CONN_RC"), root_2);

                adaptor.addChild(root_2, stream_rc.nextNode());

                adaptor.addChild(root_1, root_2);
                }
                // /home/dfranusic/SmsFilterScript.g:691:480: ^( CONN_SC $sc)
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CONN_SC, "CONN_SC"), root_2);

                adaptor.addChild(root_2, stream_sc.nextNode());

                adaptor.addChild(root_1, root_2);
                }

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "m3ua"

    public static class modifier_method_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "modifier_method"
    // /home/dfranusic/SmsFilterScript.g:699:1: modifier_method : ( SPAM_UPDATE_LST | SPAM_REMOVE_LST | QUARANTINE_UPDATE_LST | QUARANTINE_REMOVE_LST | MD5_UPDATE_LST | MD5_REMOVE_LST | NO_DR | CONVERT_SS7 | CONVERT_SMPP | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn= STRINGLITERAL ( WS )* COMMA ( WS )* sca= STRINGLITERAL ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn) ^( HLR_SCA $sca) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn1= eval_string ( WS )* COMMA ( WS )* sca1= eval_string ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn1) ^( HLR_SCA $sca1) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn2= eval_string ( WS )* COMMA ( WS )* sca2= STRINGLITERAL ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn2) ^( HLR_SCA $sca2) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn3= STRINGLITERAL ( WS )* COMMA ( WS )* sca3= eval_string ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn3) ^( HLR_SCA $sca3) ) );
    public final SmsFilterScriptParser.modifier_method_return modifier_method() throws RecognitionException {
        SmsFilterScriptParser.modifier_method_return retval = new SmsFilterScriptParser.modifier_method_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token r=null;
        Token msisdn=null;
        Token sca=null;
        Token sca2=null;
        Token msisdn3=null;
        Token SPAM_UPDATE_LST367=null;
        Token SPAM_REMOVE_LST368=null;
        Token QUARANTINE_UPDATE_LST369=null;
        Token QUARANTINE_REMOVE_LST370=null;
        Token MD5_UPDATE_LST371=null;
        Token MD5_REMOVE_LST372=null;
        Token NO_DR373=null;
        Token CONVERT_SS7374=null;
        Token CONVERT_SMPP375=null;
        Token WS376=null;
        Token L_PAREN377=null;
        Token WS378=null;
        Token WS379=null;
        Token COMMA380=null;
        Token WS381=null;
        Token WS382=null;
        Token R_PAREN383=null;
        Token WS384=null;
        Token L_PAREN385=null;
        Token WS386=null;
        Token WS387=null;
        Token COMMA388=null;
        Token WS389=null;
        Token WS390=null;
        Token R_PAREN391=null;
        Token WS392=null;
        Token L_PAREN393=null;
        Token WS394=null;
        Token WS395=null;
        Token COMMA396=null;
        Token WS397=null;
        Token WS398=null;
        Token R_PAREN399=null;
        Token WS400=null;
        Token L_PAREN401=null;
        Token WS402=null;
        Token WS403=null;
        Token COMMA404=null;
        Token WS405=null;
        Token WS406=null;
        Token R_PAREN407=null;
        SmsFilterScriptParser.eval_string_return msisdn1 = null;

        SmsFilterScriptParser.eval_string_return sca1 = null;

        SmsFilterScriptParser.eval_string_return msisdn2 = null;

        SmsFilterScriptParser.eval_string_return sca3 = null;


        Object r_tree=null;
        Object msisdn_tree=null;
        Object sca_tree=null;
        Object sca2_tree=null;
        Object msisdn3_tree=null;
        Object SPAM_UPDATE_LST367_tree=null;
        Object SPAM_REMOVE_LST368_tree=null;
        Object QUARANTINE_UPDATE_LST369_tree=null;
        Object QUARANTINE_REMOVE_LST370_tree=null;
        Object MD5_UPDATE_LST371_tree=null;
        Object MD5_REMOVE_LST372_tree=null;
        Object NO_DR373_tree=null;
        Object CONVERT_SS7374_tree=null;
        Object CONVERT_SMPP375_tree=null;
        Object WS376_tree=null;
        Object L_PAREN377_tree=null;
        Object WS378_tree=null;
        Object WS379_tree=null;
        Object COMMA380_tree=null;
        Object WS381_tree=null;
        Object WS382_tree=null;
        Object R_PAREN383_tree=null;
        Object WS384_tree=null;
        Object L_PAREN385_tree=null;
        Object WS386_tree=null;
        Object WS387_tree=null;
        Object COMMA388_tree=null;
        Object WS389_tree=null;
        Object WS390_tree=null;
        Object R_PAREN391_tree=null;
        Object WS392_tree=null;
        Object L_PAREN393_tree=null;
        Object WS394_tree=null;
        Object WS395_tree=null;
        Object COMMA396_tree=null;
        Object WS397_tree=null;
        Object WS398_tree=null;
        Object R_PAREN399_tree=null;
        Object WS400_tree=null;
        Object L_PAREN401_tree=null;
        Object WS402_tree=null;
        Object WS403_tree=null;
        Object COMMA404_tree=null;
        Object WS405_tree=null;
        Object WS406_tree=null;
        Object R_PAREN407_tree=null;
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_COMMA=new RewriteRuleTokenStream(adaptor,"token COMMA");
        RewriteRuleTokenStream stream_HLR_REQUEST=new RewriteRuleTokenStream(adaptor,"token HLR_REQUEST");
        RewriteRuleTokenStream stream_STRINGLITERAL=new RewriteRuleTokenStream(adaptor,"token STRINGLITERAL");
        RewriteRuleTokenStream stream_R_PAREN=new RewriteRuleTokenStream(adaptor,"token R_PAREN");
        RewriteRuleTokenStream stream_L_PAREN=new RewriteRuleTokenStream(adaptor,"token L_PAREN");
        RewriteRuleSubtreeStream stream_eval_string=new RewriteRuleSubtreeStream(adaptor,"rule eval_string");
        try {
            // /home/dfranusic/SmsFilterScript.g:700:3: ( SPAM_UPDATE_LST | SPAM_REMOVE_LST | QUARANTINE_UPDATE_LST | QUARANTINE_REMOVE_LST | MD5_UPDATE_LST | MD5_REMOVE_LST | NO_DR | CONVERT_SS7 | CONVERT_SMPP | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn= STRINGLITERAL ( WS )* COMMA ( WS )* sca= STRINGLITERAL ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn) ^( HLR_SCA $sca) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn1= eval_string ( WS )* COMMA ( WS )* sca1= eval_string ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn1) ^( HLR_SCA $sca1) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn2= eval_string ( WS )* COMMA ( WS )* sca2= STRINGLITERAL ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn2) ^( HLR_SCA $sca2) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn3= STRINGLITERAL ( WS )* COMMA ( WS )* sca3= eval_string ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn3) ^( HLR_SCA $sca3) ) )
            int alt213=13;
            alt213 = dfa213.predict(input);
            switch (alt213) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:700:5: SPAM_UPDATE_LST
                    {
                    root_0 = (Object)adaptor.nil();

                    SPAM_UPDATE_LST367=(Token)match(input,SPAM_UPDATE_LST,FOLLOW_SPAM_UPDATE_LST_in_modifier_method6904); 
                    SPAM_UPDATE_LST367_tree = (Object)adaptor.create(SPAM_UPDATE_LST367);
                    adaptor.addChild(root_0, SPAM_UPDATE_LST367_tree);


                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:701:5: SPAM_REMOVE_LST
                    {
                    root_0 = (Object)adaptor.nil();

                    SPAM_REMOVE_LST368=(Token)match(input,SPAM_REMOVE_LST,FOLLOW_SPAM_REMOVE_LST_in_modifier_method6910); 
                    SPAM_REMOVE_LST368_tree = (Object)adaptor.create(SPAM_REMOVE_LST368);
                    adaptor.addChild(root_0, SPAM_REMOVE_LST368_tree);


                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:702:5: QUARANTINE_UPDATE_LST
                    {
                    root_0 = (Object)adaptor.nil();

                    QUARANTINE_UPDATE_LST369=(Token)match(input,QUARANTINE_UPDATE_LST,FOLLOW_QUARANTINE_UPDATE_LST_in_modifier_method6916); 
                    QUARANTINE_UPDATE_LST369_tree = (Object)adaptor.create(QUARANTINE_UPDATE_LST369);
                    adaptor.addChild(root_0, QUARANTINE_UPDATE_LST369_tree);


                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:703:5: QUARANTINE_REMOVE_LST
                    {
                    root_0 = (Object)adaptor.nil();

                    QUARANTINE_REMOVE_LST370=(Token)match(input,QUARANTINE_REMOVE_LST,FOLLOW_QUARANTINE_REMOVE_LST_in_modifier_method6922); 
                    QUARANTINE_REMOVE_LST370_tree = (Object)adaptor.create(QUARANTINE_REMOVE_LST370);
                    adaptor.addChild(root_0, QUARANTINE_REMOVE_LST370_tree);


                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:704:5: MD5_UPDATE_LST
                    {
                    root_0 = (Object)adaptor.nil();

                    MD5_UPDATE_LST371=(Token)match(input,MD5_UPDATE_LST,FOLLOW_MD5_UPDATE_LST_in_modifier_method6928); 
                    MD5_UPDATE_LST371_tree = (Object)adaptor.create(MD5_UPDATE_LST371);
                    adaptor.addChild(root_0, MD5_UPDATE_LST371_tree);


                    }
                    break;
                case 6 :
                    // /home/dfranusic/SmsFilterScript.g:705:5: MD5_REMOVE_LST
                    {
                    root_0 = (Object)adaptor.nil();

                    MD5_REMOVE_LST372=(Token)match(input,MD5_REMOVE_LST,FOLLOW_MD5_REMOVE_LST_in_modifier_method6934); 
                    MD5_REMOVE_LST372_tree = (Object)adaptor.create(MD5_REMOVE_LST372);
                    adaptor.addChild(root_0, MD5_REMOVE_LST372_tree);


                    }
                    break;
                case 7 :
                    // /home/dfranusic/SmsFilterScript.g:706:5: NO_DR
                    {
                    root_0 = (Object)adaptor.nil();

                    NO_DR373=(Token)match(input,NO_DR,FOLLOW_NO_DR_in_modifier_method6941); 
                    NO_DR373_tree = (Object)adaptor.create(NO_DR373);
                    adaptor.addChild(root_0, NO_DR373_tree);


                    }
                    break;
                case 8 :
                    // /home/dfranusic/SmsFilterScript.g:707:5: CONVERT_SS7
                    {
                    root_0 = (Object)adaptor.nil();

                    CONVERT_SS7374=(Token)match(input,CONVERT_SS7,FOLLOW_CONVERT_SS7_in_modifier_method6947); 
                    CONVERT_SS7374_tree = (Object)adaptor.create(CONVERT_SS7374);
                    adaptor.addChild(root_0, CONVERT_SS7374_tree);


                    }
                    break;
                case 9 :
                    // /home/dfranusic/SmsFilterScript.g:708:5: CONVERT_SMPP
                    {
                    root_0 = (Object)adaptor.nil();

                    CONVERT_SMPP375=(Token)match(input,CONVERT_SMPP,FOLLOW_CONVERT_SMPP_in_modifier_method6953); 
                    CONVERT_SMPP375_tree = (Object)adaptor.create(CONVERT_SMPP375);
                    adaptor.addChild(root_0, CONVERT_SMPP375_tree);


                    }
                    break;
                case 10 :
                    // /home/dfranusic/SmsFilterScript.g:710:5: r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn= STRINGLITERAL ( WS )* COMMA ( WS )* sca= STRINGLITERAL ( WS )* R_PAREN
                    {
                    r=(Token)match(input,HLR_REQUEST,FOLLOW_HLR_REQUEST_in_modifier_method6964);  
                    stream_HLR_REQUEST.add(r);

                    // /home/dfranusic/SmsFilterScript.g:710:19: ( WS )*
                    loop193:
                    do {
                        int alt193=2;
                        int LA193_0 = input.LA(1);

                        if ( (LA193_0==WS) ) {
                            alt193=1;
                        }


                        switch (alt193) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:710:19: WS
                    	    {
                    	    WS376=(Token)match(input,WS,FOLLOW_WS_in_modifier_method6966);  
                    	    stream_WS.add(WS376);


                    	    }
                    	    break;

                    	default :
                    	    break loop193;
                        }
                    } while (true);

                    L_PAREN377=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_modifier_method6969);  
                    stream_L_PAREN.add(L_PAREN377);

                    // /home/dfranusic/SmsFilterScript.g:710:31: ( WS )*
                    loop194:
                    do {
                        int alt194=2;
                        int LA194_0 = input.LA(1);

                        if ( (LA194_0==WS) ) {
                            alt194=1;
                        }


                        switch (alt194) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:710:31: WS
                    	    {
                    	    WS378=(Token)match(input,WS,FOLLOW_WS_in_modifier_method6971);  
                    	    stream_WS.add(WS378);


                    	    }
                    	    break;

                    	default :
                    	    break loop194;
                        }
                    } while (true);

                    msisdn=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_modifier_method6976);  
                    stream_STRINGLITERAL.add(msisdn);

                    // /home/dfranusic/SmsFilterScript.g:710:56: ( WS )*
                    loop195:
                    do {
                        int alt195=2;
                        int LA195_0 = input.LA(1);

                        if ( (LA195_0==WS) ) {
                            alt195=1;
                        }


                        switch (alt195) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:710:56: WS
                    	    {
                    	    WS379=(Token)match(input,WS,FOLLOW_WS_in_modifier_method6978);  
                    	    stream_WS.add(WS379);


                    	    }
                    	    break;

                    	default :
                    	    break loop195;
                        }
                    } while (true);

                    COMMA380=(Token)match(input,COMMA,FOLLOW_COMMA_in_modifier_method6981);  
                    stream_COMMA.add(COMMA380);

                    // /home/dfranusic/SmsFilterScript.g:710:66: ( WS )*
                    loop196:
                    do {
                        int alt196=2;
                        int LA196_0 = input.LA(1);

                        if ( (LA196_0==WS) ) {
                            alt196=1;
                        }


                        switch (alt196) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:710:66: WS
                    	    {
                    	    WS381=(Token)match(input,WS,FOLLOW_WS_in_modifier_method6983);  
                    	    stream_WS.add(WS381);


                    	    }
                    	    break;

                    	default :
                    	    break loop196;
                        }
                    } while (true);

                    sca=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_modifier_method6988);  
                    stream_STRINGLITERAL.add(sca);

                    // /home/dfranusic/SmsFilterScript.g:710:88: ( WS )*
                    loop197:
                    do {
                        int alt197=2;
                        int LA197_0 = input.LA(1);

                        if ( (LA197_0==WS) ) {
                            alt197=1;
                        }


                        switch (alt197) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:710:88: WS
                    	    {
                    	    WS382=(Token)match(input,WS,FOLLOW_WS_in_modifier_method6990);  
                    	    stream_WS.add(WS382);


                    	    }
                    	    break;

                    	default :
                    	    break loop197;
                        }
                    } while (true);

                    R_PAREN383=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_modifier_method6993);  
                    stream_R_PAREN.add(R_PAREN383);



                    // AST REWRITE
                    // elements: sca, msisdn, r
                    // token labels: sca, r, msisdn
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_sca=new RewriteRuleTokenStream(adaptor,"token sca",sca);
                    RewriteRuleTokenStream stream_r=new RewriteRuleTokenStream(adaptor,"token r",r);
                    RewriteRuleTokenStream stream_msisdn=new RewriteRuleTokenStream(adaptor,"token msisdn",msisdn);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 710:100: -> ^( $r ^( HLR_MSISDN $msisdn) ^( HLR_SCA $sca) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:710:103: ^( $r ^( HLR_MSISDN $msisdn) ^( HLR_SCA $sca) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_r.nextNode(), root_1);

                        // /home/dfranusic/SmsFilterScript.g:710:108: ^( HLR_MSISDN $msisdn)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_MSISDN, "HLR_MSISDN"), root_2);

                        adaptor.addChild(root_2, stream_msisdn.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/SmsFilterScript.g:710:130: ^( HLR_SCA $sca)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_SCA, "HLR_SCA"), root_2);

                        adaptor.addChild(root_2, stream_sca.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 11 :
                    // /home/dfranusic/SmsFilterScript.g:711:5: r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn1= eval_string ( WS )* COMMA ( WS )* sca1= eval_string ( WS )* R_PAREN
                    {
                    r=(Token)match(input,HLR_REQUEST,FOLLOW_HLR_REQUEST_in_modifier_method7022);  
                    stream_HLR_REQUEST.add(r);

                    // /home/dfranusic/SmsFilterScript.g:711:19: ( WS )*
                    loop198:
                    do {
                        int alt198=2;
                        int LA198_0 = input.LA(1);

                        if ( (LA198_0==WS) ) {
                            alt198=1;
                        }


                        switch (alt198) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:711:19: WS
                    	    {
                    	    WS384=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7024);  
                    	    stream_WS.add(WS384);


                    	    }
                    	    break;

                    	default :
                    	    break loop198;
                        }
                    } while (true);

                    L_PAREN385=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_modifier_method7027);  
                    stream_L_PAREN.add(L_PAREN385);

                    // /home/dfranusic/SmsFilterScript.g:711:31: ( WS )*
                    loop199:
                    do {
                        int alt199=2;
                        int LA199_0 = input.LA(1);

                        if ( (LA199_0==WS) ) {
                            alt199=1;
                        }


                        switch (alt199) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:711:31: WS
                    	    {
                    	    WS386=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7029);  
                    	    stream_WS.add(WS386);


                    	    }
                    	    break;

                    	default :
                    	    break loop199;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_modifier_method7034);
                    msisdn1=eval_string();

                    state._fsp--;

                    stream_eval_string.add(msisdn1.getTree());
                    // /home/dfranusic/SmsFilterScript.g:711:55: ( WS )*
                    loop200:
                    do {
                        int alt200=2;
                        int LA200_0 = input.LA(1);

                        if ( (LA200_0==WS) ) {
                            alt200=1;
                        }


                        switch (alt200) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:711:55: WS
                    	    {
                    	    WS387=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7036);  
                    	    stream_WS.add(WS387);


                    	    }
                    	    break;

                    	default :
                    	    break loop200;
                        }
                    } while (true);

                    COMMA388=(Token)match(input,COMMA,FOLLOW_COMMA_in_modifier_method7039);  
                    stream_COMMA.add(COMMA388);

                    // /home/dfranusic/SmsFilterScript.g:711:65: ( WS )*
                    loop201:
                    do {
                        int alt201=2;
                        int LA201_0 = input.LA(1);

                        if ( (LA201_0==WS) ) {
                            alt201=1;
                        }


                        switch (alt201) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:711:65: WS
                    	    {
                    	    WS389=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7041);  
                    	    stream_WS.add(WS389);


                    	    }
                    	    break;

                    	default :
                    	    break loop201;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_modifier_method7046);
                    sca1=eval_string();

                    state._fsp--;

                    stream_eval_string.add(sca1.getTree());
                    // /home/dfranusic/SmsFilterScript.g:711:86: ( WS )*
                    loop202:
                    do {
                        int alt202=2;
                        int LA202_0 = input.LA(1);

                        if ( (LA202_0==WS) ) {
                            alt202=1;
                        }


                        switch (alt202) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:711:86: WS
                    	    {
                    	    WS390=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7048);  
                    	    stream_WS.add(WS390);


                    	    }
                    	    break;

                    	default :
                    	    break loop202;
                        }
                    } while (true);

                    R_PAREN391=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_modifier_method7051);  
                    stream_R_PAREN.add(R_PAREN391);



                    // AST REWRITE
                    // elements: sca1, r, msisdn1
                    // token labels: r
                    // rule labels: retval, sca1, msisdn1
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_r=new RewriteRuleTokenStream(adaptor,"token r",r);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_sca1=new RewriteRuleSubtreeStream(adaptor,"rule sca1",sca1!=null?sca1.tree:null);
                    RewriteRuleSubtreeStream stream_msisdn1=new RewriteRuleSubtreeStream(adaptor,"rule msisdn1",msisdn1!=null?msisdn1.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 711:98: -> ^( $r ^( HLR_MSISDN $msisdn1) ^( HLR_SCA $sca1) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:711:101: ^( $r ^( HLR_MSISDN $msisdn1) ^( HLR_SCA $sca1) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_r.nextNode(), root_1);

                        // /home/dfranusic/SmsFilterScript.g:711:106: ^( HLR_MSISDN $msisdn1)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_MSISDN, "HLR_MSISDN"), root_2);

                        adaptor.addChild(root_2, stream_msisdn1.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/SmsFilterScript.g:711:129: ^( HLR_SCA $sca1)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_SCA, "HLR_SCA"), root_2);

                        adaptor.addChild(root_2, stream_sca1.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 12 :
                    // /home/dfranusic/SmsFilterScript.g:712:5: r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn2= eval_string ( WS )* COMMA ( WS )* sca2= STRINGLITERAL ( WS )* R_PAREN
                    {
                    r=(Token)match(input,HLR_REQUEST,FOLLOW_HLR_REQUEST_in_modifier_method7080);  
                    stream_HLR_REQUEST.add(r);

                    // /home/dfranusic/SmsFilterScript.g:712:19: ( WS )*
                    loop203:
                    do {
                        int alt203=2;
                        int LA203_0 = input.LA(1);

                        if ( (LA203_0==WS) ) {
                            alt203=1;
                        }


                        switch (alt203) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:712:19: WS
                    	    {
                    	    WS392=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7082);  
                    	    stream_WS.add(WS392);


                    	    }
                    	    break;

                    	default :
                    	    break loop203;
                        }
                    } while (true);

                    L_PAREN393=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_modifier_method7085);  
                    stream_L_PAREN.add(L_PAREN393);

                    // /home/dfranusic/SmsFilterScript.g:712:31: ( WS )*
                    loop204:
                    do {
                        int alt204=2;
                        int LA204_0 = input.LA(1);

                        if ( (LA204_0==WS) ) {
                            alt204=1;
                        }


                        switch (alt204) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:712:31: WS
                    	    {
                    	    WS394=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7087);  
                    	    stream_WS.add(WS394);


                    	    }
                    	    break;

                    	default :
                    	    break loop204;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_modifier_method7092);
                    msisdn2=eval_string();

                    state._fsp--;

                    stream_eval_string.add(msisdn2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:712:55: ( WS )*
                    loop205:
                    do {
                        int alt205=2;
                        int LA205_0 = input.LA(1);

                        if ( (LA205_0==WS) ) {
                            alt205=1;
                        }


                        switch (alt205) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:712:55: WS
                    	    {
                    	    WS395=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7094);  
                    	    stream_WS.add(WS395);


                    	    }
                    	    break;

                    	default :
                    	    break loop205;
                        }
                    } while (true);

                    COMMA396=(Token)match(input,COMMA,FOLLOW_COMMA_in_modifier_method7097);  
                    stream_COMMA.add(COMMA396);

                    // /home/dfranusic/SmsFilterScript.g:712:65: ( WS )*
                    loop206:
                    do {
                        int alt206=2;
                        int LA206_0 = input.LA(1);

                        if ( (LA206_0==WS) ) {
                            alt206=1;
                        }


                        switch (alt206) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:712:65: WS
                    	    {
                    	    WS397=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7099);  
                    	    stream_WS.add(WS397);


                    	    }
                    	    break;

                    	default :
                    	    break loop206;
                        }
                    } while (true);

                    sca2=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_modifier_method7104);  
                    stream_STRINGLITERAL.add(sca2);

                    // /home/dfranusic/SmsFilterScript.g:712:88: ( WS )*
                    loop207:
                    do {
                        int alt207=2;
                        int LA207_0 = input.LA(1);

                        if ( (LA207_0==WS) ) {
                            alt207=1;
                        }


                        switch (alt207) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:712:88: WS
                    	    {
                    	    WS398=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7106);  
                    	    stream_WS.add(WS398);


                    	    }
                    	    break;

                    	default :
                    	    break loop207;
                        }
                    } while (true);

                    R_PAREN399=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_modifier_method7109);  
                    stream_R_PAREN.add(R_PAREN399);



                    // AST REWRITE
                    // elements: sca2, r, msisdn2
                    // token labels: r, sca2
                    // rule labels: retval, msisdn2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_r=new RewriteRuleTokenStream(adaptor,"token r",r);
                    RewriteRuleTokenStream stream_sca2=new RewriteRuleTokenStream(adaptor,"token sca2",sca2);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_msisdn2=new RewriteRuleSubtreeStream(adaptor,"rule msisdn2",msisdn2!=null?msisdn2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 712:100: -> ^( $r ^( HLR_MSISDN $msisdn2) ^( HLR_SCA $sca2) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:712:103: ^( $r ^( HLR_MSISDN $msisdn2) ^( HLR_SCA $sca2) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_r.nextNode(), root_1);

                        // /home/dfranusic/SmsFilterScript.g:712:108: ^( HLR_MSISDN $msisdn2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_MSISDN, "HLR_MSISDN"), root_2);

                        adaptor.addChild(root_2, stream_msisdn2.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/SmsFilterScript.g:712:131: ^( HLR_SCA $sca2)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_SCA, "HLR_SCA"), root_2);

                        adaptor.addChild(root_2, stream_sca2.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 13 :
                    // /home/dfranusic/SmsFilterScript.g:713:5: r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn3= STRINGLITERAL ( WS )* COMMA ( WS )* sca3= eval_string ( WS )* R_PAREN
                    {
                    r=(Token)match(input,HLR_REQUEST,FOLLOW_HLR_REQUEST_in_modifier_method7138);  
                    stream_HLR_REQUEST.add(r);

                    // /home/dfranusic/SmsFilterScript.g:713:19: ( WS )*
                    loop208:
                    do {
                        int alt208=2;
                        int LA208_0 = input.LA(1);

                        if ( (LA208_0==WS) ) {
                            alt208=1;
                        }


                        switch (alt208) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:713:19: WS
                    	    {
                    	    WS400=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7140);  
                    	    stream_WS.add(WS400);


                    	    }
                    	    break;

                    	default :
                    	    break loop208;
                        }
                    } while (true);

                    L_PAREN401=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_modifier_method7143);  
                    stream_L_PAREN.add(L_PAREN401);

                    // /home/dfranusic/SmsFilterScript.g:713:31: ( WS )*
                    loop209:
                    do {
                        int alt209=2;
                        int LA209_0 = input.LA(1);

                        if ( (LA209_0==WS) ) {
                            alt209=1;
                        }


                        switch (alt209) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:713:31: WS
                    	    {
                    	    WS402=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7145);  
                    	    stream_WS.add(WS402);


                    	    }
                    	    break;

                    	default :
                    	    break loop209;
                        }
                    } while (true);

                    msisdn3=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_modifier_method7150);  
                    stream_STRINGLITERAL.add(msisdn3);

                    // /home/dfranusic/SmsFilterScript.g:713:57: ( WS )*
                    loop210:
                    do {
                        int alt210=2;
                        int LA210_0 = input.LA(1);

                        if ( (LA210_0==WS) ) {
                            alt210=1;
                        }


                        switch (alt210) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:713:57: WS
                    	    {
                    	    WS403=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7152);  
                    	    stream_WS.add(WS403);


                    	    }
                    	    break;

                    	default :
                    	    break loop210;
                        }
                    } while (true);

                    COMMA404=(Token)match(input,COMMA,FOLLOW_COMMA_in_modifier_method7155);  
                    stream_COMMA.add(COMMA404);

                    // /home/dfranusic/SmsFilterScript.g:713:67: ( WS )*
                    loop211:
                    do {
                        int alt211=2;
                        int LA211_0 = input.LA(1);

                        if ( (LA211_0==WS) ) {
                            alt211=1;
                        }


                        switch (alt211) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:713:67: WS
                    	    {
                    	    WS405=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7157);  
                    	    stream_WS.add(WS405);


                    	    }
                    	    break;

                    	default :
                    	    break loop211;
                        }
                    } while (true);

                    pushFollow(FOLLOW_eval_string_in_modifier_method7162);
                    sca3=eval_string();

                    state._fsp--;

                    stream_eval_string.add(sca3.getTree());
                    // /home/dfranusic/SmsFilterScript.g:713:88: ( WS )*
                    loop212:
                    do {
                        int alt212=2;
                        int LA212_0 = input.LA(1);

                        if ( (LA212_0==WS) ) {
                            alt212=1;
                        }


                        switch (alt212) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:713:88: WS
                    	    {
                    	    WS406=(Token)match(input,WS,FOLLOW_WS_in_modifier_method7164);  
                    	    stream_WS.add(WS406);


                    	    }
                    	    break;

                    	default :
                    	    break loop212;
                        }
                    } while (true);

                    R_PAREN407=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_modifier_method7167);  
                    stream_R_PAREN.add(R_PAREN407);



                    // AST REWRITE
                    // elements: msisdn3, r, sca3
                    // token labels: r, msisdn3
                    // rule labels: retval, sca3
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_r=new RewriteRuleTokenStream(adaptor,"token r",r);
                    RewriteRuleTokenStream stream_msisdn3=new RewriteRuleTokenStream(adaptor,"token msisdn3",msisdn3);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_sca3=new RewriteRuleSubtreeStream(adaptor,"rule sca3",sca3!=null?sca3.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 713:100: -> ^( $r ^( HLR_MSISDN $msisdn3) ^( HLR_SCA $sca3) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:713:103: ^( $r ^( HLR_MSISDN $msisdn3) ^( HLR_SCA $sca3) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_r.nextNode(), root_1);

                        // /home/dfranusic/SmsFilterScript.g:713:108: ^( HLR_MSISDN $msisdn3)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_MSISDN, "HLR_MSISDN"), root_2);

                        adaptor.addChild(root_2, stream_msisdn3.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/SmsFilterScript.g:713:131: ^( HLR_SCA $sca3)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(HLR_SCA, "HLR_SCA"), root_2);

                        adaptor.addChild(root_2, stream_sca3.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "modifier_method"

    public static class modify_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "modify"
    // /home/dfranusic/SmsFilterScript.g:716:1: modify : ( L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON MODIFY ( WS )* curlyblock -> ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON MODIFY ( WS )* curlyblock -> ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock ^( MODIFIER_LABEL $lbl) ) );
    public final SmsFilterScriptParser.modify_return modify() throws RecognitionException {
        SmsFilterScriptParser.modify_return retval = new SmsFilterScriptParser.modify_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token ra=null;
        Token lbl=null;
        Token L_SQ_B408=null;
        Token ANNT409=null;
        Token R_SQ_B410=null;
        Token COLON411=null;
        Token MODIFY412=null;
        Token WS413=null;
        Token COLON415=null;
        Token L_SQ_B416=null;
        Token ANNT417=null;
        Token R_SQ_B418=null;
        Token COLON419=null;
        Token MODIFY420=null;
        Token WS421=null;
        SmsFilterScriptParser.curlyblock_return curlyblock414 = null;

        SmsFilterScriptParser.curlyblock_return curlyblock422 = null;


        Object ra_tree=null;
        Object lbl_tree=null;
        Object L_SQ_B408_tree=null;
        Object ANNT409_tree=null;
        Object R_SQ_B410_tree=null;
        Object COLON411_tree=null;
        Object MODIFY412_tree=null;
        Object WS413_tree=null;
        Object COLON415_tree=null;
        Object L_SQ_B416_tree=null;
        Object ANNT417_tree=null;
        Object R_SQ_B418_tree=null;
        Object COLON419_tree=null;
        Object MODIFY420_tree=null;
        Object WS421_tree=null;
        RewriteRuleTokenStream stream_WORD=new RewriteRuleTokenStream(adaptor,"token WORD");
        RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_ON=new RewriteRuleTokenStream(adaptor,"token ON");
        RewriteRuleTokenStream stream_MODIFY=new RewriteRuleTokenStream(adaptor,"token MODIFY");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_L_SQ_B=new RewriteRuleTokenStream(adaptor,"token L_SQ_B");
        RewriteRuleTokenStream stream_ANNT=new RewriteRuleTokenStream(adaptor,"token ANNT");
        RewriteRuleTokenStream stream_OFF=new RewriteRuleTokenStream(adaptor,"token OFF");
        RewriteRuleTokenStream stream_R_SQ_B=new RewriteRuleTokenStream(adaptor,"token R_SQ_B");
        RewriteRuleSubtreeStream stream_curlyblock=new RewriteRuleSubtreeStream(adaptor,"rule curlyblock");
        try {
            // /home/dfranusic/SmsFilterScript.g:717:3: ( L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON MODIFY ( WS )* curlyblock -> ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON MODIFY ( WS )* curlyblock -> ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock ^( MODIFIER_LABEL $lbl) ) )
            int alt218=2;
            int LA218_0 = input.LA(1);

            if ( (LA218_0==L_SQ_B) ) {
                alt218=1;
            }
            else if ( (LA218_0==WORD) ) {
                alt218=2;
            }
            else {
                NoViableAltException nvae =
                    new NoViableAltException("", 218, 0, input);

                throw nvae;
            }
            switch (alt218) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:717:5: L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON MODIFY ( WS )* curlyblock
                    {
                    L_SQ_B408=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_modify7201);  
                    stream_L_SQ_B.add(L_SQ_B408);

                    ANNT409=(Token)match(input,ANNT,FOLLOW_ANNT_in_modify7203);  
                    stream_ANNT.add(ANNT409);

                    // /home/dfranusic/SmsFilterScript.g:717:17: (ra= ON | ra= OFF )
                    int alt214=2;
                    int LA214_0 = input.LA(1);

                    if ( (LA214_0==ON) ) {
                        alt214=1;
                    }
                    else if ( (LA214_0==OFF) ) {
                        alt214=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 214, 0, input);

                        throw nvae;
                    }
                    switch (alt214) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:717:18: ra= ON
                            {
                            ra=(Token)match(input,ON,FOLLOW_ON_in_modify7208);  
                            stream_ON.add(ra);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:717:26: ra= OFF
                            {
                            ra=(Token)match(input,OFF,FOLLOW_OFF_in_modify7214);  
                            stream_OFF.add(ra);


                            }
                            break;

                    }

                    R_SQ_B410=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_modify7217);  
                    stream_R_SQ_B.add(R_SQ_B410);

                    COLON411=(Token)match(input,COLON,FOLLOW_COLON_in_modify7219);  
                    stream_COLON.add(COLON411);

                    MODIFY412=(Token)match(input,MODIFY,FOLLOW_MODIFY_in_modify7221);  
                    stream_MODIFY.add(MODIFY412);

                    // /home/dfranusic/SmsFilterScript.g:717:54: ( WS )*
                    loop215:
                    do {
                        int alt215=2;
                        int LA215_0 = input.LA(1);

                        if ( (LA215_0==WS) ) {
                            alt215=1;
                        }


                        switch (alt215) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:717:54: WS
                    	    {
                    	    WS413=(Token)match(input,WS,FOLLOW_WS_in_modify7223);  
                    	    stream_WS.add(WS413);


                    	    }
                    	    break;

                    	default :
                    	    break loop215;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_modify7226);
                    curlyblock414=curlyblock();

                    state._fsp--;

                    stream_curlyblock.add(curlyblock414.getTree());


                    // AST REWRITE
                    // elements: curlyblock, ra
                    // token labels: ra
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ra=new RewriteRuleTokenStream(adaptor,"token ra",ra);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 717:70: -> ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock )
                    {
                        // /home/dfranusic/SmsFilterScript.g:717:73: ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MODIFIER, "MODIFIER"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:717:83: ^( MODIFIER_STATUS $ra)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(MODIFIER_STATUS, "MODIFIER_STATUS"), root_2);

                        adaptor.addChild(root_2, stream_ra.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_curlyblock.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:718:5: lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON MODIFY ( WS )* curlyblock
                    {
                    lbl=(Token)match(input,WORD,FOLLOW_WORD_in_modify7249);  
                    stream_WORD.add(lbl);

                    COLON415=(Token)match(input,COLON,FOLLOW_COLON_in_modify7251);  
                    stream_COLON.add(COLON415);

                    L_SQ_B416=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_modify7253);  
                    stream_L_SQ_B.add(L_SQ_B416);

                    ANNT417=(Token)match(input,ANNT,FOLLOW_ANNT_in_modify7255);  
                    stream_ANNT.add(ANNT417);

                    // /home/dfranusic/SmsFilterScript.g:718:32: (ra= ON | ra= OFF )
                    int alt216=2;
                    int LA216_0 = input.LA(1);

                    if ( (LA216_0==ON) ) {
                        alt216=1;
                    }
                    else if ( (LA216_0==OFF) ) {
                        alt216=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 216, 0, input);

                        throw nvae;
                    }
                    switch (alt216) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:718:33: ra= ON
                            {
                            ra=(Token)match(input,ON,FOLLOW_ON_in_modify7260);  
                            stream_ON.add(ra);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:718:41: ra= OFF
                            {
                            ra=(Token)match(input,OFF,FOLLOW_OFF_in_modify7266);  
                            stream_OFF.add(ra);


                            }
                            break;

                    }

                    R_SQ_B418=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_modify7269);  
                    stream_R_SQ_B.add(R_SQ_B418);

                    COLON419=(Token)match(input,COLON,FOLLOW_COLON_in_modify7271);  
                    stream_COLON.add(COLON419);

                    MODIFY420=(Token)match(input,MODIFY,FOLLOW_MODIFY_in_modify7273);  
                    stream_MODIFY.add(MODIFY420);

                    // /home/dfranusic/SmsFilterScript.g:718:69: ( WS )*
                    loop217:
                    do {
                        int alt217=2;
                        int LA217_0 = input.LA(1);

                        if ( (LA217_0==WS) ) {
                            alt217=1;
                        }


                        switch (alt217) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:718:69: WS
                    	    {
                    	    WS421=(Token)match(input,WS,FOLLOW_WS_in_modify7275);  
                    	    stream_WS.add(WS421);


                    	    }
                    	    break;

                    	default :
                    	    break loop217;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_modify7278);
                    curlyblock422=curlyblock();

                    state._fsp--;

                    stream_curlyblock.add(curlyblock422.getTree());


                    // AST REWRITE
                    // elements: curlyblock, ra, lbl
                    // token labels: ra, lbl
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ra=new RewriteRuleTokenStream(adaptor,"token ra",ra);
                    RewriteRuleTokenStream stream_lbl=new RewriteRuleTokenStream(adaptor,"token lbl",lbl);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 718:85: -> ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock ^( MODIFIER_LABEL $lbl) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:718:88: ^( MODIFIER ^( MODIFIER_STATUS $ra) curlyblock ^( MODIFIER_LABEL $lbl) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MODIFIER, "MODIFIER"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:718:98: ^( MODIFIER_STATUS $ra)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(MODIFIER_STATUS, "MODIFIER_STATUS"), root_2);

                        adaptor.addChild(root_2, stream_ra.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_curlyblock.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:718:132: ^( MODIFIER_LABEL $lbl)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(MODIFIER_LABEL, "MODIFIER_LABEL"), root_2);

                        adaptor.addChild(root_2, stream_lbl.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "modify"

    public static class modifybody_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "modifybody"
    // /home/dfranusic/SmsFilterScript.g:721:1: modifybody : ( COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* b= STRINGLITERAL ( WS )* STMTSEP -> ^( $w $b) | COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* w2= evalobj ( WS )* STMTSEP -> ^( $w $w2) | COLON ( WS )* wm= modifier_method ( WS )* STMTSEP -> ^( MODIFIER_METHOD $wm) );
    public final SmsFilterScriptParser.modifybody_return modifybody() throws RecognitionException {
        SmsFilterScriptParser.modifybody_return retval = new SmsFilterScriptParser.modifybody_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token b=null;
        Token COLON423=null;
        Token WS424=null;
        Token WS425=null;
        Token ASSIGN426=null;
        Token WS427=null;
        Token WS428=null;
        Token STMTSEP429=null;
        Token COLON430=null;
        Token WS431=null;
        Token WS432=null;
        Token ASSIGN433=null;
        Token WS434=null;
        Token WS435=null;
        Token STMTSEP436=null;
        Token COLON437=null;
        Token WS438=null;
        Token WS439=null;
        Token STMTSEP440=null;
        SmsFilterScriptParser.evalobj_return w = null;

        SmsFilterScriptParser.evalobj_return w2 = null;

        SmsFilterScriptParser.modifier_method_return wm = null;


        Object b_tree=null;
        Object COLON423_tree=null;
        Object WS424_tree=null;
        Object WS425_tree=null;
        Object ASSIGN426_tree=null;
        Object WS427_tree=null;
        Object WS428_tree=null;
        Object STMTSEP429_tree=null;
        Object COLON430_tree=null;
        Object WS431_tree=null;
        Object WS432_tree=null;
        Object ASSIGN433_tree=null;
        Object WS434_tree=null;
        Object WS435_tree=null;
        Object STMTSEP436_tree=null;
        Object COLON437_tree=null;
        Object WS438_tree=null;
        Object WS439_tree=null;
        Object STMTSEP440_tree=null;
        RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_STRINGLITERAL=new RewriteRuleTokenStream(adaptor,"token STRINGLITERAL");
        RewriteRuleTokenStream stream_STMTSEP=new RewriteRuleTokenStream(adaptor,"token STMTSEP");
        RewriteRuleTokenStream stream_ASSIGN=new RewriteRuleTokenStream(adaptor,"token ASSIGN");
        RewriteRuleSubtreeStream stream_modifier_method=new RewriteRuleSubtreeStream(adaptor,"rule modifier_method");
        RewriteRuleSubtreeStream stream_evalobj=new RewriteRuleSubtreeStream(adaptor,"rule evalobj");
        try {
            // /home/dfranusic/SmsFilterScript.g:722:3: ( COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* b= STRINGLITERAL ( WS )* STMTSEP -> ^( $w $b) | COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* w2= evalobj ( WS )* STMTSEP -> ^( $w $w2) | COLON ( WS )* wm= modifier_method ( WS )* STMTSEP -> ^( MODIFIER_METHOD $wm) )
            int alt229=3;
            alt229 = dfa229.predict(input);
            switch (alt229) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:722:5: COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* b= STRINGLITERAL ( WS )* STMTSEP
                    {
                    COLON423=(Token)match(input,COLON,FOLLOW_COLON_in_modifybody7313);  
                    stream_COLON.add(COLON423);

                    // /home/dfranusic/SmsFilterScript.g:722:11: ( WS )*
                    loop219:
                    do {
                        int alt219=2;
                        int LA219_0 = input.LA(1);

                        if ( (LA219_0==WS) ) {
                            alt219=1;
                        }


                        switch (alt219) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:722:11: WS
                    	    {
                    	    WS424=(Token)match(input,WS,FOLLOW_WS_in_modifybody7315);  
                    	    stream_WS.add(WS424);


                    	    }
                    	    break;

                    	default :
                    	    break loop219;
                        }
                    } while (true);

                    pushFollow(FOLLOW_evalobj_in_modifybody7320);
                    w=evalobj();

                    state._fsp--;

                    stream_evalobj.add(w.getTree());
                    // /home/dfranusic/SmsFilterScript.g:722:25: ( WS )*
                    loop220:
                    do {
                        int alt220=2;
                        int LA220_0 = input.LA(1);

                        if ( (LA220_0==WS) ) {
                            alt220=1;
                        }


                        switch (alt220) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:722:25: WS
                    	    {
                    	    WS425=(Token)match(input,WS,FOLLOW_WS_in_modifybody7322);  
                    	    stream_WS.add(WS425);


                    	    }
                    	    break;

                    	default :
                    	    break loop220;
                        }
                    } while (true);

                    ASSIGN426=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_modifybody7325);  
                    stream_ASSIGN.add(ASSIGN426);

                    // /home/dfranusic/SmsFilterScript.g:722:36: ( WS )*
                    loop221:
                    do {
                        int alt221=2;
                        int LA221_0 = input.LA(1);

                        if ( (LA221_0==WS) ) {
                            alt221=1;
                        }


                        switch (alt221) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:722:36: WS
                    	    {
                    	    WS427=(Token)match(input,WS,FOLLOW_WS_in_modifybody7327);  
                    	    stream_WS.add(WS427);


                    	    }
                    	    break;

                    	default :
                    	    break loop221;
                        }
                    } while (true);

                    b=(Token)match(input,STRINGLITERAL,FOLLOW_STRINGLITERAL_in_modifybody7332);  
                    stream_STRINGLITERAL.add(b);

                    // /home/dfranusic/SmsFilterScript.g:722:56: ( WS )*
                    loop222:
                    do {
                        int alt222=2;
                        int LA222_0 = input.LA(1);

                        if ( (LA222_0==WS) ) {
                            alt222=1;
                        }


                        switch (alt222) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:722:56: WS
                    	    {
                    	    WS428=(Token)match(input,WS,FOLLOW_WS_in_modifybody7334);  
                    	    stream_WS.add(WS428);


                    	    }
                    	    break;

                    	default :
                    	    break loop222;
                        }
                    } while (true);

                    STMTSEP429=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_modifybody7337);  
                    stream_STMTSEP.add(STMTSEP429);



                    // AST REWRITE
                    // elements: w, b
                    // token labels: b
                    // rule labels: w, retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_b=new RewriteRuleTokenStream(adaptor,"token b",b);
                    RewriteRuleSubtreeStream stream_w=new RewriteRuleSubtreeStream(adaptor,"rule w",w!=null?w.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 722:68: -> ^( $w $b)
                    {
                        // /home/dfranusic/SmsFilterScript.g:722:71: ^( $w $b)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_w.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_b.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:723:5: COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* w2= evalobj ( WS )* STMTSEP
                    {
                    COLON430=(Token)match(input,COLON,FOLLOW_COLON_in_modifybody7353);  
                    stream_COLON.add(COLON430);

                    // /home/dfranusic/SmsFilterScript.g:723:11: ( WS )*
                    loop223:
                    do {
                        int alt223=2;
                        int LA223_0 = input.LA(1);

                        if ( (LA223_0==WS) ) {
                            alt223=1;
                        }


                        switch (alt223) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:723:11: WS
                    	    {
                    	    WS431=(Token)match(input,WS,FOLLOW_WS_in_modifybody7355);  
                    	    stream_WS.add(WS431);


                    	    }
                    	    break;

                    	default :
                    	    break loop223;
                        }
                    } while (true);

                    pushFollow(FOLLOW_evalobj_in_modifybody7360);
                    w=evalobj();

                    state._fsp--;

                    stream_evalobj.add(w.getTree());
                    // /home/dfranusic/SmsFilterScript.g:723:25: ( WS )*
                    loop224:
                    do {
                        int alt224=2;
                        int LA224_0 = input.LA(1);

                        if ( (LA224_0==WS) ) {
                            alt224=1;
                        }


                        switch (alt224) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:723:25: WS
                    	    {
                    	    WS432=(Token)match(input,WS,FOLLOW_WS_in_modifybody7362);  
                    	    stream_WS.add(WS432);


                    	    }
                    	    break;

                    	default :
                    	    break loop224;
                        }
                    } while (true);

                    ASSIGN433=(Token)match(input,ASSIGN,FOLLOW_ASSIGN_in_modifybody7365);  
                    stream_ASSIGN.add(ASSIGN433);

                    // /home/dfranusic/SmsFilterScript.g:723:36: ( WS )*
                    loop225:
                    do {
                        int alt225=2;
                        int LA225_0 = input.LA(1);

                        if ( (LA225_0==WS) ) {
                            alt225=1;
                        }


                        switch (alt225) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:723:36: WS
                    	    {
                    	    WS434=(Token)match(input,WS,FOLLOW_WS_in_modifybody7367);  
                    	    stream_WS.add(WS434);


                    	    }
                    	    break;

                    	default :
                    	    break loop225;
                        }
                    } while (true);

                    pushFollow(FOLLOW_evalobj_in_modifybody7372);
                    w2=evalobj();

                    state._fsp--;

                    stream_evalobj.add(w2.getTree());
                    // /home/dfranusic/SmsFilterScript.g:723:51: ( WS )*
                    loop226:
                    do {
                        int alt226=2;
                        int LA226_0 = input.LA(1);

                        if ( (LA226_0==WS) ) {
                            alt226=1;
                        }


                        switch (alt226) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:723:51: WS
                    	    {
                    	    WS435=(Token)match(input,WS,FOLLOW_WS_in_modifybody7374);  
                    	    stream_WS.add(WS435);


                    	    }
                    	    break;

                    	default :
                    	    break loop226;
                        }
                    } while (true);

                    STMTSEP436=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_modifybody7377);  
                    stream_STMTSEP.add(STMTSEP436);



                    // AST REWRITE
                    // elements: w, w2
                    // token labels: 
                    // rule labels: w, retval, w2
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_w=new RewriteRuleSubtreeStream(adaptor,"rule w",w!=null?w.tree:null);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_w2=new RewriteRuleSubtreeStream(adaptor,"rule w2",w2!=null?w2.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 723:63: -> ^( $w $w2)
                    {
                        // /home/dfranusic/SmsFilterScript.g:723:66: ^( $w $w2)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_w.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_w2.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:724:5: COLON ( WS )* wm= modifier_method ( WS )* STMTSEP
                    {
                    COLON437=(Token)match(input,COLON,FOLLOW_COLON_in_modifybody7393);  
                    stream_COLON.add(COLON437);

                    // /home/dfranusic/SmsFilterScript.g:724:11: ( WS )*
                    loop227:
                    do {
                        int alt227=2;
                        int LA227_0 = input.LA(1);

                        if ( (LA227_0==WS) ) {
                            alt227=1;
                        }


                        switch (alt227) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:724:11: WS
                    	    {
                    	    WS438=(Token)match(input,WS,FOLLOW_WS_in_modifybody7395);  
                    	    stream_WS.add(WS438);


                    	    }
                    	    break;

                    	default :
                    	    break loop227;
                        }
                    } while (true);

                    pushFollow(FOLLOW_modifier_method_in_modifybody7400);
                    wm=modifier_method();

                    state._fsp--;

                    stream_modifier_method.add(wm.getTree());
                    // /home/dfranusic/SmsFilterScript.g:724:34: ( WS )*
                    loop228:
                    do {
                        int alt228=2;
                        int LA228_0 = input.LA(1);

                        if ( (LA228_0==WS) ) {
                            alt228=1;
                        }


                        switch (alt228) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:724:34: WS
                    	    {
                    	    WS439=(Token)match(input,WS,FOLLOW_WS_in_modifybody7402);  
                    	    stream_WS.add(WS439);


                    	    }
                    	    break;

                    	default :
                    	    break loop228;
                        }
                    } while (true);

                    STMTSEP440=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_modifybody7405);  
                    stream_STMTSEP.add(STMTSEP440);



                    // AST REWRITE
                    // elements: wm
                    // token labels: 
                    // rule labels: retval, wm
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);
                    RewriteRuleSubtreeStream stream_wm=new RewriteRuleSubtreeStream(adaptor,"rule wm",wm!=null?wm.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 724:46: -> ^( MODIFIER_METHOD $wm)
                    {
                        // /home/dfranusic/SmsFilterScript.g:724:49: ^( MODIFIER_METHOD $wm)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(MODIFIER_METHOD, "MODIFIER_METHOD"), root_1);

                        adaptor.addChild(root_1, stream_wm.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "modifybody"

    public static class ruleeval_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "ruleeval"
    // /home/dfranusic/SmsFilterScript.g:727:1: ruleeval : COLON ( WS )* eval ( WS )* STMTSEP ;
    public final SmsFilterScriptParser.ruleeval_return ruleeval() throws RecognitionException {
        SmsFilterScriptParser.ruleeval_return retval = new SmsFilterScriptParser.ruleeval_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token COLON441=null;
        Token WS442=null;
        Token WS444=null;
        Token STMTSEP445=null;
        SmsFilterScriptParser.eval_return eval443 = null;


        Object COLON441_tree=null;
        Object WS442_tree=null;
        Object WS444_tree=null;
        Object STMTSEP445_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:728:3: ( COLON ( WS )* eval ( WS )* STMTSEP )
            // /home/dfranusic/SmsFilterScript.g:728:5: COLON ( WS )* eval ( WS )* STMTSEP
            {
            root_0 = (Object)adaptor.nil();

            COLON441=(Token)match(input,COLON,FOLLOW_COLON_in_ruleeval7427); 
            // /home/dfranusic/SmsFilterScript.g:728:12: ( WS )*
            loop230:
            do {
                int alt230=2;
                int LA230_0 = input.LA(1);

                if ( (LA230_0==WS) ) {
                    alt230=1;
                }


                switch (alt230) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:728:12: WS
            	    {
            	    WS442=(Token)match(input,WS,FOLLOW_WS_in_ruleeval7430); 
            	    WS442_tree = (Object)adaptor.create(WS442);
            	    adaptor.addChild(root_0, WS442_tree);


            	    }
            	    break;

            	default :
            	    break loop230;
                }
            } while (true);

            pushFollow(FOLLOW_eval_in_ruleeval7433);
            eval443=eval();

            state._fsp--;

            adaptor.addChild(root_0, eval443.getTree());
            // /home/dfranusic/SmsFilterScript.g:728:21: ( WS )*
            loop231:
            do {
                int alt231=2;
                int LA231_0 = input.LA(1);

                if ( (LA231_0==WS) ) {
                    alt231=1;
                }


                switch (alt231) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:728:21: WS
            	    {
            	    WS444=(Token)match(input,WS,FOLLOW_WS_in_ruleeval7435); 
            	    WS444_tree = (Object)adaptor.create(WS444);
            	    adaptor.addChild(root_0, WS444_tree);


            	    }
            	    break;

            	default :
            	    break loop231;
                }
            } while (true);

            STMTSEP445=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_ruleeval7438); 

            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "ruleeval"

    public static class rulealw_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "rulealw"
    // /home/dfranusic/SmsFilterScript.g:731:1: rulealw : PLUS ( WS )* action ( WS )* STMTSEP -> ^( RULE_EVAL_TRUE action ) ;
    public final SmsFilterScriptParser.rulealw_return rulealw() throws RecognitionException {
        SmsFilterScriptParser.rulealw_return retval = new SmsFilterScriptParser.rulealw_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token PLUS446=null;
        Token WS447=null;
        Token WS449=null;
        Token STMTSEP450=null;
        SmsFilterScriptParser.action_return action448 = null;


        Object PLUS446_tree=null;
        Object WS447_tree=null;
        Object WS449_tree=null;
        Object STMTSEP450_tree=null;
        RewriteRuleTokenStream stream_PLUS=new RewriteRuleTokenStream(adaptor,"token PLUS");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_STMTSEP=new RewriteRuleTokenStream(adaptor,"token STMTSEP");
        RewriteRuleSubtreeStream stream_action=new RewriteRuleSubtreeStream(adaptor,"rule action");
        try {
            // /home/dfranusic/SmsFilterScript.g:732:3: ( PLUS ( WS )* action ( WS )* STMTSEP -> ^( RULE_EVAL_TRUE action ) )
            // /home/dfranusic/SmsFilterScript.g:732:5: PLUS ( WS )* action ( WS )* STMTSEP
            {
            PLUS446=(Token)match(input,PLUS,FOLLOW_PLUS_in_rulealw7453);  
            stream_PLUS.add(PLUS446);

            // /home/dfranusic/SmsFilterScript.g:732:10: ( WS )*
            loop232:
            do {
                int alt232=2;
                int LA232_0 = input.LA(1);

                if ( (LA232_0==WS) ) {
                    alt232=1;
                }


                switch (alt232) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:732:10: WS
            	    {
            	    WS447=(Token)match(input,WS,FOLLOW_WS_in_rulealw7455);  
            	    stream_WS.add(WS447);


            	    }
            	    break;

            	default :
            	    break loop232;
                }
            } while (true);

            pushFollow(FOLLOW_action_in_rulealw7458);
            action448=action();

            state._fsp--;

            stream_action.add(action448.getTree());
            // /home/dfranusic/SmsFilterScript.g:732:21: ( WS )*
            loop233:
            do {
                int alt233=2;
                int LA233_0 = input.LA(1);

                if ( (LA233_0==WS) ) {
                    alt233=1;
                }


                switch (alt233) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:732:21: WS
            	    {
            	    WS449=(Token)match(input,WS,FOLLOW_WS_in_rulealw7460);  
            	    stream_WS.add(WS449);


            	    }
            	    break;

            	default :
            	    break loop233;
                }
            } while (true);

            STMTSEP450=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_rulealw7463);  
            stream_STMTSEP.add(STMTSEP450);



            // AST REWRITE
            // elements: action
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 733:3: -> ^( RULE_EVAL_TRUE action )
            {
                // /home/dfranusic/SmsFilterScript.g:733:6: ^( RULE_EVAL_TRUE action )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_TRUE, "RULE_EVAL_TRUE"), root_1);

                adaptor.addChild(root_1, stream_action.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "rulealw"

    public static class ruledny_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "ruledny"
    // /home/dfranusic/SmsFilterScript.g:736:1: ruledny : MINUS ( WS )* action ( WS )* STMTSEP -> ^( RULE_EVAL_FALSE action ) ;
    public final SmsFilterScriptParser.ruledny_return ruledny() throws RecognitionException {
        SmsFilterScriptParser.ruledny_return retval = new SmsFilterScriptParser.ruledny_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token MINUS451=null;
        Token WS452=null;
        Token WS454=null;
        Token STMTSEP455=null;
        SmsFilterScriptParser.action_return action453 = null;


        Object MINUS451_tree=null;
        Object WS452_tree=null;
        Object WS454_tree=null;
        Object STMTSEP455_tree=null;
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_MINUS=new RewriteRuleTokenStream(adaptor,"token MINUS");
        RewriteRuleTokenStream stream_STMTSEP=new RewriteRuleTokenStream(adaptor,"token STMTSEP");
        RewriteRuleSubtreeStream stream_action=new RewriteRuleSubtreeStream(adaptor,"rule action");
        try {
            // /home/dfranusic/SmsFilterScript.g:737:3: ( MINUS ( WS )* action ( WS )* STMTSEP -> ^( RULE_EVAL_FALSE action ) )
            // /home/dfranusic/SmsFilterScript.g:737:5: MINUS ( WS )* action ( WS )* STMTSEP
            {
            MINUS451=(Token)match(input,MINUS,FOLLOW_MINUS_in_ruledny7486);  
            stream_MINUS.add(MINUS451);

            // /home/dfranusic/SmsFilterScript.g:737:11: ( WS )*
            loop234:
            do {
                int alt234=2;
                int LA234_0 = input.LA(1);

                if ( (LA234_0==WS) ) {
                    alt234=1;
                }


                switch (alt234) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:737:11: WS
            	    {
            	    WS452=(Token)match(input,WS,FOLLOW_WS_in_ruledny7488);  
            	    stream_WS.add(WS452);


            	    }
            	    break;

            	default :
            	    break loop234;
                }
            } while (true);

            pushFollow(FOLLOW_action_in_ruledny7491);
            action453=action();

            state._fsp--;

            stream_action.add(action453.getTree());
            // /home/dfranusic/SmsFilterScript.g:737:22: ( WS )*
            loop235:
            do {
                int alt235=2;
                int LA235_0 = input.LA(1);

                if ( (LA235_0==WS) ) {
                    alt235=1;
                }


                switch (alt235) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:737:22: WS
            	    {
            	    WS454=(Token)match(input,WS,FOLLOW_WS_in_ruledny7493);  
            	    stream_WS.add(WS454);


            	    }
            	    break;

            	default :
            	    break loop235;
                }
            } while (true);

            STMTSEP455=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_ruledny7496);  
            stream_STMTSEP.add(STMTSEP455);



            // AST REWRITE
            // elements: action
            // token labels: 
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 738:3: -> ^( RULE_EVAL_FALSE action )
            {
                // /home/dfranusic/SmsFilterScript.g:738:6: ^( RULE_EVAL_FALSE action )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_FALSE, "RULE_EVAL_FALSE"), root_1);

                adaptor.addChild(root_1, stream_action.nextTree());

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "ruledny"

    public static class rulebody_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "rulebody"
    // /home/dfranusic/SmsFilterScript.g:742:1: rulebody : ( ruleeval ( WS )* rulealw ( WS )* ruledny | COLON ( WS )* L_PAREN ( WS )* action ( WS )* R_PAREN ( WS )* STMTSEP );
    public final SmsFilterScriptParser.rulebody_return rulebody() throws RecognitionException {
        SmsFilterScriptParser.rulebody_return retval = new SmsFilterScriptParser.rulebody_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token WS457=null;
        Token WS459=null;
        Token COLON461=null;
        Token WS462=null;
        Token L_PAREN463=null;
        Token WS464=null;
        Token WS466=null;
        Token R_PAREN467=null;
        Token WS468=null;
        Token STMTSEP469=null;
        SmsFilterScriptParser.ruleeval_return ruleeval456 = null;

        SmsFilterScriptParser.rulealw_return rulealw458 = null;

        SmsFilterScriptParser.ruledny_return ruledny460 = null;

        SmsFilterScriptParser.action_return action465 = null;


        Object WS457_tree=null;
        Object WS459_tree=null;
        Object COLON461_tree=null;
        Object WS462_tree=null;
        Object L_PAREN463_tree=null;
        Object WS464_tree=null;
        Object WS466_tree=null;
        Object R_PAREN467_tree=null;
        Object WS468_tree=null;
        Object STMTSEP469_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:743:3: ( ruleeval ( WS )* rulealw ( WS )* ruledny | COLON ( WS )* L_PAREN ( WS )* action ( WS )* R_PAREN ( WS )* STMTSEP )
            int alt242=2;
            alt242 = dfa242.predict(input);
            switch (alt242) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:743:5: ruleeval ( WS )* rulealw ( WS )* ruledny
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_ruleeval_in_rulebody7520);
                    ruleeval456=ruleeval();

                    state._fsp--;

                    adaptor.addChild(root_0, ruleeval456.getTree());
                    // /home/dfranusic/SmsFilterScript.g:743:14: ( WS )*
                    loop236:
                    do {
                        int alt236=2;
                        int LA236_0 = input.LA(1);

                        if ( (LA236_0==WS) ) {
                            alt236=1;
                        }


                        switch (alt236) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:743:14: WS
                    	    {
                    	    WS457=(Token)match(input,WS,FOLLOW_WS_in_rulebody7522); 
                    	    WS457_tree = (Object)adaptor.create(WS457);
                    	    adaptor.addChild(root_0, WS457_tree);


                    	    }
                    	    break;

                    	default :
                    	    break loop236;
                        }
                    } while (true);

                    pushFollow(FOLLOW_rulealw_in_rulebody7525);
                    rulealw458=rulealw();

                    state._fsp--;

                    adaptor.addChild(root_0, rulealw458.getTree());
                    // /home/dfranusic/SmsFilterScript.g:743:26: ( WS )*
                    loop237:
                    do {
                        int alt237=2;
                        int LA237_0 = input.LA(1);

                        if ( (LA237_0==WS) ) {
                            alt237=1;
                        }


                        switch (alt237) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:743:26: WS
                    	    {
                    	    WS459=(Token)match(input,WS,FOLLOW_WS_in_rulebody7527); 
                    	    WS459_tree = (Object)adaptor.create(WS459);
                    	    adaptor.addChild(root_0, WS459_tree);


                    	    }
                    	    break;

                    	default :
                    	    break loop237;
                        }
                    } while (true);

                    pushFollow(FOLLOW_ruledny_in_rulebody7530);
                    ruledny460=ruledny();

                    state._fsp--;

                    adaptor.addChild(root_0, ruledny460.getTree());

                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:744:5: COLON ( WS )* L_PAREN ( WS )* action ( WS )* R_PAREN ( WS )* STMTSEP
                    {
                    root_0 = (Object)adaptor.nil();

                    COLON461=(Token)match(input,COLON,FOLLOW_COLON_in_rulebody7536); 
                    // /home/dfranusic/SmsFilterScript.g:744:12: ( WS )*
                    loop238:
                    do {
                        int alt238=2;
                        int LA238_0 = input.LA(1);

                        if ( (LA238_0==WS) ) {
                            alt238=1;
                        }


                        switch (alt238) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:744:12: WS
                    	    {
                    	    WS462=(Token)match(input,WS,FOLLOW_WS_in_rulebody7539); 
                    	    WS462_tree = (Object)adaptor.create(WS462);
                    	    adaptor.addChild(root_0, WS462_tree);


                    	    }
                    	    break;

                    	default :
                    	    break loop238;
                        }
                    } while (true);

                    L_PAREN463=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_rulebody7542); 
                    // /home/dfranusic/SmsFilterScript.g:744:25: ( WS )*
                    loop239:
                    do {
                        int alt239=2;
                        int LA239_0 = input.LA(1);

                        if ( (LA239_0==WS) ) {
                            alt239=1;
                        }


                        switch (alt239) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:744:25: WS
                    	    {
                    	    WS464=(Token)match(input,WS,FOLLOW_WS_in_rulebody7545); 
                    	    WS464_tree = (Object)adaptor.create(WS464);
                    	    adaptor.addChild(root_0, WS464_tree);


                    	    }
                    	    break;

                    	default :
                    	    break loop239;
                        }
                    } while (true);

                    pushFollow(FOLLOW_action_in_rulebody7548);
                    action465=action();

                    state._fsp--;

                    adaptor.addChild(root_0, action465.getTree());
                    // /home/dfranusic/SmsFilterScript.g:744:36: ( WS )*
                    loop240:
                    do {
                        int alt240=2;
                        int LA240_0 = input.LA(1);

                        if ( (LA240_0==WS) ) {
                            alt240=1;
                        }


                        switch (alt240) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:744:36: WS
                    	    {
                    	    WS466=(Token)match(input,WS,FOLLOW_WS_in_rulebody7550); 
                    	    WS466_tree = (Object)adaptor.create(WS466);
                    	    adaptor.addChild(root_0, WS466_tree);


                    	    }
                    	    break;

                    	default :
                    	    break loop240;
                        }
                    } while (true);

                    R_PAREN467=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_rulebody7553); 
                    // /home/dfranusic/SmsFilterScript.g:744:49: ( WS )*
                    loop241:
                    do {
                        int alt241=2;
                        int LA241_0 = input.LA(1);

                        if ( (LA241_0==WS) ) {
                            alt241=1;
                        }


                        switch (alt241) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:744:49: WS
                    	    {
                    	    WS468=(Token)match(input,WS,FOLLOW_WS_in_rulebody7556); 
                    	    WS468_tree = (Object)adaptor.create(WS468);
                    	    adaptor.addChild(root_0, WS468_tree);


                    	    }
                    	    break;

                    	default :
                    	    break loop241;
                        }
                    } while (true);

                    STMTSEP469=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_rulebody7559); 

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "rulebody"

    public static class rule_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "rule"
    // /home/dfranusic/SmsFilterScript.g:747:1: rule : ( L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) ) | L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) ) );
    public final SmsFilterScriptParser.rule_return rule() throws RecognitionException {
        SmsFilterScriptParser.rule_return retval = new SmsFilterScriptParser.rule_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token ra=null;
        Token d=null;
        Token lbl=null;
        Token L_SQ_B470=null;
        Token ANNT471=null;
        Token R_SQ_B472=null;
        Token COLON473=null;
        Token RULE474=null;
        Token L_SQ_B475=null;
        Token R_SQ_B476=null;
        Token WS477=null;
        Token COLON479=null;
        Token L_SQ_B480=null;
        Token ANNT481=null;
        Token R_SQ_B482=null;
        Token COLON483=null;
        Token RULE484=null;
        Token L_SQ_B485=null;
        Token R_SQ_B486=null;
        Token WS487=null;
        Token L_SQ_B489=null;
        Token ANNT490=null;
        Token R_SQ_B491=null;
        Token COLON492=null;
        Token RULE493=null;
        Token WS494=null;
        Token COLON496=null;
        Token L_SQ_B497=null;
        Token ANNT498=null;
        Token R_SQ_B499=null;
        Token COLON500=null;
        Token RULE501=null;
        Token WS502=null;
        SmsFilterScriptParser.curlyblock_return curlyblock478 = null;

        SmsFilterScriptParser.curlyblock_return curlyblock488 = null;

        SmsFilterScriptParser.curlyblock_return curlyblock495 = null;

        SmsFilterScriptParser.curlyblock_return curlyblock503 = null;


        Object ra_tree=null;
        Object d_tree=null;
        Object lbl_tree=null;
        Object L_SQ_B470_tree=null;
        Object ANNT471_tree=null;
        Object R_SQ_B472_tree=null;
        Object COLON473_tree=null;
        Object RULE474_tree=null;
        Object L_SQ_B475_tree=null;
        Object R_SQ_B476_tree=null;
        Object WS477_tree=null;
        Object COLON479_tree=null;
        Object L_SQ_B480_tree=null;
        Object ANNT481_tree=null;
        Object R_SQ_B482_tree=null;
        Object COLON483_tree=null;
        Object RULE484_tree=null;
        Object L_SQ_B485_tree=null;
        Object R_SQ_B486_tree=null;
        Object WS487_tree=null;
        Object L_SQ_B489_tree=null;
        Object ANNT490_tree=null;
        Object R_SQ_B491_tree=null;
        Object COLON492_tree=null;
        Object RULE493_tree=null;
        Object WS494_tree=null;
        Object COLON496_tree=null;
        Object L_SQ_B497_tree=null;
        Object ANNT498_tree=null;
        Object R_SQ_B499_tree=null;
        Object COLON500_tree=null;
        Object RULE501_tree=null;
        Object WS502_tree=null;
        RewriteRuleTokenStream stream_WORD=new RewriteRuleTokenStream(adaptor,"token WORD");
        RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_ON=new RewriteRuleTokenStream(adaptor,"token ON");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_RULE=new RewriteRuleTokenStream(adaptor,"token RULE");
        RewriteRuleTokenStream stream_L_SQ_B=new RewriteRuleTokenStream(adaptor,"token L_SQ_B");
        RewriteRuleTokenStream stream_ANNT=new RewriteRuleTokenStream(adaptor,"token ANNT");
        RewriteRuleTokenStream stream_OFF=new RewriteRuleTokenStream(adaptor,"token OFF");
        RewriteRuleTokenStream stream_R_SQ_B=new RewriteRuleTokenStream(adaptor,"token R_SQ_B");
        RewriteRuleTokenStream stream_DIGITS=new RewriteRuleTokenStream(adaptor,"token DIGITS");
        RewriteRuleSubtreeStream stream_curlyblock=new RewriteRuleSubtreeStream(adaptor,"rule curlyblock");
        try {
            // /home/dfranusic/SmsFilterScript.g:749:3: ( L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) ) | L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) ) )
            int alt253=4;
            alt253 = dfa253.predict(input);
            switch (alt253) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:749:5: L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock
                    {
                    L_SQ_B470=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_rule7576);  
                    stream_L_SQ_B.add(L_SQ_B470);

                    ANNT471=(Token)match(input,ANNT,FOLLOW_ANNT_in_rule7578);  
                    stream_ANNT.add(ANNT471);

                    // /home/dfranusic/SmsFilterScript.g:749:17: (ra= ON | ra= OFF )
                    int alt243=2;
                    int LA243_0 = input.LA(1);

                    if ( (LA243_0==ON) ) {
                        alt243=1;
                    }
                    else if ( (LA243_0==OFF) ) {
                        alt243=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 243, 0, input);

                        throw nvae;
                    }
                    switch (alt243) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:749:18: ra= ON
                            {
                            ra=(Token)match(input,ON,FOLLOW_ON_in_rule7583);  
                            stream_ON.add(ra);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:749:26: ra= OFF
                            {
                            ra=(Token)match(input,OFF,FOLLOW_OFF_in_rule7589);  
                            stream_OFF.add(ra);


                            }
                            break;

                    }

                    R_SQ_B472=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_rule7592);  
                    stream_R_SQ_B.add(R_SQ_B472);

                    COLON473=(Token)match(input,COLON,FOLLOW_COLON_in_rule7594);  
                    stream_COLON.add(COLON473);

                    RULE474=(Token)match(input,RULE,FOLLOW_RULE_in_rule7596);  
                    stream_RULE.add(RULE474);

                    L_SQ_B475=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_rule7598);  
                    stream_L_SQ_B.add(L_SQ_B475);

                    // /home/dfranusic/SmsFilterScript.g:749:60: (d= DIGITS )*
                    loop244:
                    do {
                        int alt244=2;
                        int LA244_0 = input.LA(1);

                        if ( (LA244_0==DIGITS) ) {
                            alt244=1;
                        }


                        switch (alt244) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:749:60: d= DIGITS
                    	    {
                    	    d=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_rule7602);  
                    	    stream_DIGITS.add(d);


                    	    }
                    	    break;

                    	default :
                    	    break loop244;
                        }
                    } while (true);

                    R_SQ_B476=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_rule7605);  
                    stream_R_SQ_B.add(R_SQ_B476);

                    // /home/dfranusic/SmsFilterScript.g:749:76: ( WS )*
                    loop245:
                    do {
                        int alt245=2;
                        int LA245_0 = input.LA(1);

                        if ( (LA245_0==WS) ) {
                            alt245=1;
                        }


                        switch (alt245) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:749:76: WS
                    	    {
                    	    WS477=(Token)match(input,WS,FOLLOW_WS_in_rule7607);  
                    	    stream_WS.add(WS477);


                    	    }
                    	    break;

                    	default :
                    	    break loop245;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_rule7610);
                    curlyblock478=curlyblock();

                    state._fsp--;

                    stream_curlyblock.add(curlyblock478.getTree());


                    // AST REWRITE
                    // elements: ra, d, curlyblock
                    // token labels: ra, d
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ra=new RewriteRuleTokenStream(adaptor,"token ra",ra);
                    RewriteRuleTokenStream stream_d=new RewriteRuleTokenStream(adaptor,"token d",d);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 749:91: -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock )
                    {
                        // /home/dfranusic/SmsFilterScript.g:749:94: ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_DEF, "RULE_DEF"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:749:104: ^( RULE_EVAL_POINTS $d)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_POINTS, "RULE_EVAL_POINTS"), root_2);

                        adaptor.addChild(root_2, stream_d.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/SmsFilterScript.g:749:127: ^( RULE_STATUS $ra)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_STATUS, "RULE_STATUS"), root_2);

                        adaptor.addChild(root_2, stream_ra.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_curlyblock.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:750:5: lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock
                    {
                    lbl=(Token)match(input,WORD,FOLLOW_WORD_in_rule7639);  
                    stream_WORD.add(lbl);

                    COLON479=(Token)match(input,COLON,FOLLOW_COLON_in_rule7641);  
                    stream_COLON.add(COLON479);

                    L_SQ_B480=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_rule7643);  
                    stream_L_SQ_B.add(L_SQ_B480);

                    ANNT481=(Token)match(input,ANNT,FOLLOW_ANNT_in_rule7645);  
                    stream_ANNT.add(ANNT481);

                    // /home/dfranusic/SmsFilterScript.g:750:32: (ra= ON | ra= OFF )
                    int alt246=2;
                    int LA246_0 = input.LA(1);

                    if ( (LA246_0==ON) ) {
                        alt246=1;
                    }
                    else if ( (LA246_0==OFF) ) {
                        alt246=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 246, 0, input);

                        throw nvae;
                    }
                    switch (alt246) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:750:33: ra= ON
                            {
                            ra=(Token)match(input,ON,FOLLOW_ON_in_rule7650);  
                            stream_ON.add(ra);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:750:41: ra= OFF
                            {
                            ra=(Token)match(input,OFF,FOLLOW_OFF_in_rule7656);  
                            stream_OFF.add(ra);


                            }
                            break;

                    }

                    R_SQ_B482=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_rule7659);  
                    stream_R_SQ_B.add(R_SQ_B482);

                    COLON483=(Token)match(input,COLON,FOLLOW_COLON_in_rule7661);  
                    stream_COLON.add(COLON483);

                    RULE484=(Token)match(input,RULE,FOLLOW_RULE_in_rule7663);  
                    stream_RULE.add(RULE484);

                    L_SQ_B485=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_rule7665);  
                    stream_L_SQ_B.add(L_SQ_B485);

                    // /home/dfranusic/SmsFilterScript.g:750:75: (d= DIGITS )*
                    loop247:
                    do {
                        int alt247=2;
                        int LA247_0 = input.LA(1);

                        if ( (LA247_0==DIGITS) ) {
                            alt247=1;
                        }


                        switch (alt247) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:750:75: d= DIGITS
                    	    {
                    	    d=(Token)match(input,DIGITS,FOLLOW_DIGITS_in_rule7669);  
                    	    stream_DIGITS.add(d);


                    	    }
                    	    break;

                    	default :
                    	    break loop247;
                        }
                    } while (true);

                    R_SQ_B486=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_rule7672);  
                    stream_R_SQ_B.add(R_SQ_B486);

                    // /home/dfranusic/SmsFilterScript.g:750:91: ( WS )*
                    loop248:
                    do {
                        int alt248=2;
                        int LA248_0 = input.LA(1);

                        if ( (LA248_0==WS) ) {
                            alt248=1;
                        }


                        switch (alt248) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:750:91: WS
                    	    {
                    	    WS487=(Token)match(input,WS,FOLLOW_WS_in_rule7674);  
                    	    stream_WS.add(WS487);


                    	    }
                    	    break;

                    	default :
                    	    break loop248;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_rule7677);
                    curlyblock488=curlyblock();

                    state._fsp--;

                    stream_curlyblock.add(curlyblock488.getTree());


                    // AST REWRITE
                    // elements: lbl, curlyblock, ra, d
                    // token labels: ra, d, lbl
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ra=new RewriteRuleTokenStream(adaptor,"token ra",ra);
                    RewriteRuleTokenStream stream_d=new RewriteRuleTokenStream(adaptor,"token d",d);
                    RewriteRuleTokenStream stream_lbl=new RewriteRuleTokenStream(adaptor,"token lbl",lbl);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 750:106: -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:750:109: ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_DEF, "RULE_DEF"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:750:119: ^( RULE_EVAL_POINTS $d)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_EVAL_POINTS, "RULE_EVAL_POINTS"), root_2);

                        adaptor.addChild(root_2, stream_d.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/SmsFilterScript.g:750:142: ^( RULE_STATUS $ra)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_STATUS, "RULE_STATUS"), root_2);

                        adaptor.addChild(root_2, stream_ra.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_curlyblock.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:750:172: ^( RULE_LABEL $lbl)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_LABEL, "RULE_LABEL"), root_2);

                        adaptor.addChild(root_2, stream_lbl.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:752:5: L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock
                    {
                    L_SQ_B489=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_rule7714);  
                    stream_L_SQ_B.add(L_SQ_B489);

                    ANNT490=(Token)match(input,ANNT,FOLLOW_ANNT_in_rule7716);  
                    stream_ANNT.add(ANNT490);

                    // /home/dfranusic/SmsFilterScript.g:752:17: (ra= ON | ra= OFF )
                    int alt249=2;
                    int LA249_0 = input.LA(1);

                    if ( (LA249_0==ON) ) {
                        alt249=1;
                    }
                    else if ( (LA249_0==OFF) ) {
                        alt249=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 249, 0, input);

                        throw nvae;
                    }
                    switch (alt249) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:752:18: ra= ON
                            {
                            ra=(Token)match(input,ON,FOLLOW_ON_in_rule7721);  
                            stream_ON.add(ra);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:752:26: ra= OFF
                            {
                            ra=(Token)match(input,OFF,FOLLOW_OFF_in_rule7727);  
                            stream_OFF.add(ra);


                            }
                            break;

                    }

                    R_SQ_B491=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_rule7730);  
                    stream_R_SQ_B.add(R_SQ_B491);

                    COLON492=(Token)match(input,COLON,FOLLOW_COLON_in_rule7732);  
                    stream_COLON.add(COLON492);

                    RULE493=(Token)match(input,RULE,FOLLOW_RULE_in_rule7734);  
                    stream_RULE.add(RULE493);

                    // /home/dfranusic/SmsFilterScript.g:752:52: ( WS )*
                    loop250:
                    do {
                        int alt250=2;
                        int LA250_0 = input.LA(1);

                        if ( (LA250_0==WS) ) {
                            alt250=1;
                        }


                        switch (alt250) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:752:52: WS
                    	    {
                    	    WS494=(Token)match(input,WS,FOLLOW_WS_in_rule7736);  
                    	    stream_WS.add(WS494);


                    	    }
                    	    break;

                    	default :
                    	    break loop250;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_rule7739);
                    curlyblock495=curlyblock();

                    state._fsp--;

                    stream_curlyblock.add(curlyblock495.getTree());


                    // AST REWRITE
                    // elements: curlyblock, ra
                    // token labels: ra
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ra=new RewriteRuleTokenStream(adaptor,"token ra",ra);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 752:67: -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock )
                    {
                        // /home/dfranusic/SmsFilterScript.g:752:70: ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DUMMY_RULE_DEF, "DUMMY_RULE_DEF"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:752:86: ^( RULE_STATUS $ra)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_STATUS, "RULE_STATUS"), root_2);

                        adaptor.addChild(root_2, stream_ra.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_curlyblock.nextTree());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:753:5: lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock
                    {
                    lbl=(Token)match(input,WORD,FOLLOW_WORD_in_rule7761);  
                    stream_WORD.add(lbl);

                    COLON496=(Token)match(input,COLON,FOLLOW_COLON_in_rule7763);  
                    stream_COLON.add(COLON496);

                    L_SQ_B497=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_rule7765);  
                    stream_L_SQ_B.add(L_SQ_B497);

                    ANNT498=(Token)match(input,ANNT,FOLLOW_ANNT_in_rule7767);  
                    stream_ANNT.add(ANNT498);

                    // /home/dfranusic/SmsFilterScript.g:753:32: (ra= ON | ra= OFF )
                    int alt251=2;
                    int LA251_0 = input.LA(1);

                    if ( (LA251_0==ON) ) {
                        alt251=1;
                    }
                    else if ( (LA251_0==OFF) ) {
                        alt251=2;
                    }
                    else {
                        NoViableAltException nvae =
                            new NoViableAltException("", 251, 0, input);

                        throw nvae;
                    }
                    switch (alt251) {
                        case 1 :
                            // /home/dfranusic/SmsFilterScript.g:753:33: ra= ON
                            {
                            ra=(Token)match(input,ON,FOLLOW_ON_in_rule7772);  
                            stream_ON.add(ra);


                            }
                            break;
                        case 2 :
                            // /home/dfranusic/SmsFilterScript.g:753:41: ra= OFF
                            {
                            ra=(Token)match(input,OFF,FOLLOW_OFF_in_rule7778);  
                            stream_OFF.add(ra);


                            }
                            break;

                    }

                    R_SQ_B499=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_rule7781);  
                    stream_R_SQ_B.add(R_SQ_B499);

                    COLON500=(Token)match(input,COLON,FOLLOW_COLON_in_rule7783);  
                    stream_COLON.add(COLON500);

                    RULE501=(Token)match(input,RULE,FOLLOW_RULE_in_rule7785);  
                    stream_RULE.add(RULE501);

                    // /home/dfranusic/SmsFilterScript.g:753:67: ( WS )*
                    loop252:
                    do {
                        int alt252=2;
                        int LA252_0 = input.LA(1);

                        if ( (LA252_0==WS) ) {
                            alt252=1;
                        }


                        switch (alt252) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:753:67: WS
                    	    {
                    	    WS502=(Token)match(input,WS,FOLLOW_WS_in_rule7787);  
                    	    stream_WS.add(WS502);


                    	    }
                    	    break;

                    	default :
                    	    break loop252;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_rule7790);
                    curlyblock503=curlyblock();

                    state._fsp--;

                    stream_curlyblock.add(curlyblock503.getTree());


                    // AST REWRITE
                    // elements: curlyblock, lbl, ra
                    // token labels: ra, lbl
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_ra=new RewriteRuleTokenStream(adaptor,"token ra",ra);
                    RewriteRuleTokenStream stream_lbl=new RewriteRuleTokenStream(adaptor,"token lbl",lbl);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 753:82: -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) )
                    {
                        // /home/dfranusic/SmsFilterScript.g:753:85: ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(DUMMY_RULE_DEF, "DUMMY_RULE_DEF"), root_1);

                        // /home/dfranusic/SmsFilterScript.g:753:101: ^( RULE_STATUS $ra)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_STATUS, "RULE_STATUS"), root_2);

                        adaptor.addChild(root_2, stream_ra.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }
                        adaptor.addChild(root_1, stream_curlyblock.nextTree());
                        // /home/dfranusic/SmsFilterScript.g:753:131: ^( RULE_LABEL $lbl)
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(RULE_LABEL, "RULE_LABEL"), root_2);

                        adaptor.addChild(root_2, stream_lbl.nextNode());

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "rule"

    public static class filter_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "filter"
    // /home/dfranusic/SmsFilterScript.g:756:1: filter : (ftype= F_MO | ftype= F_MT | ftype= F_SMPP_MO | ftype= F_SMPP_MT | ftype= F_HLR | ftype= F_M3UA ) ( WS )* curlyblock ( WS )* -> ^( FILTER_NODE ^( $ftype curlyblock ) ) ;
    public final SmsFilterScriptParser.filter_return filter() throws RecognitionException {
        SmsFilterScriptParser.filter_return retval = new SmsFilterScriptParser.filter_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token ftype=null;
        Token WS504=null;
        Token WS506=null;
        SmsFilterScriptParser.curlyblock_return curlyblock505 = null;


        Object ftype_tree=null;
        Object WS504_tree=null;
        Object WS506_tree=null;
        RewriteRuleTokenStream stream_F_HLR=new RewriteRuleTokenStream(adaptor,"token F_HLR");
        RewriteRuleTokenStream stream_F_SMPP_MO=new RewriteRuleTokenStream(adaptor,"token F_SMPP_MO");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_F_M3UA=new RewriteRuleTokenStream(adaptor,"token F_M3UA");
        RewriteRuleTokenStream stream_F_MT=new RewriteRuleTokenStream(adaptor,"token F_MT");
        RewriteRuleTokenStream stream_F_SMPP_MT=new RewriteRuleTokenStream(adaptor,"token F_SMPP_MT");
        RewriteRuleTokenStream stream_F_MO=new RewriteRuleTokenStream(adaptor,"token F_MO");
        RewriteRuleSubtreeStream stream_curlyblock=new RewriteRuleSubtreeStream(adaptor,"rule curlyblock");
        try {
            // /home/dfranusic/SmsFilterScript.g:757:3: ( (ftype= F_MO | ftype= F_MT | ftype= F_SMPP_MO | ftype= F_SMPP_MT | ftype= F_HLR | ftype= F_M3UA ) ( WS )* curlyblock ( WS )* -> ^( FILTER_NODE ^( $ftype curlyblock ) ) )
            // /home/dfranusic/SmsFilterScript.g:757:5: (ftype= F_MO | ftype= F_MT | ftype= F_SMPP_MO | ftype= F_SMPP_MT | ftype= F_HLR | ftype= F_M3UA ) ( WS )* curlyblock ( WS )*
            {
            // /home/dfranusic/SmsFilterScript.g:757:5: (ftype= F_MO | ftype= F_MT | ftype= F_SMPP_MO | ftype= F_SMPP_MT | ftype= F_HLR | ftype= F_M3UA )
            int alt254=6;
            switch ( input.LA(1) ) {
            case F_MO:
                {
                alt254=1;
                }
                break;
            case F_MT:
                {
                alt254=2;
                }
                break;
            case F_SMPP_MO:
                {
                alt254=3;
                }
                break;
            case F_SMPP_MT:
                {
                alt254=4;
                }
                break;
            case F_HLR:
                {
                alt254=5;
                }
                break;
            case F_M3UA:
                {
                alt254=6;
                }
                break;
            default:
                NoViableAltException nvae =
                    new NoViableAltException("", 254, 0, input);

                throw nvae;
            }

            switch (alt254) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:757:6: ftype= F_MO
                    {
                    ftype=(Token)match(input,F_MO,FOLLOW_F_MO_in_filter7827);  
                    stream_F_MO.add(ftype);


                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:757:19: ftype= F_MT
                    {
                    ftype=(Token)match(input,F_MT,FOLLOW_F_MT_in_filter7833);  
                    stream_F_MT.add(ftype);


                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:757:32: ftype= F_SMPP_MO
                    {
                    ftype=(Token)match(input,F_SMPP_MO,FOLLOW_F_SMPP_MO_in_filter7839);  
                    stream_F_SMPP_MO.add(ftype);


                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:757:50: ftype= F_SMPP_MT
                    {
                    ftype=(Token)match(input,F_SMPP_MT,FOLLOW_F_SMPP_MT_in_filter7845);  
                    stream_F_SMPP_MT.add(ftype);


                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:757:68: ftype= F_HLR
                    {
                    ftype=(Token)match(input,F_HLR,FOLLOW_F_HLR_in_filter7851);  
                    stream_F_HLR.add(ftype);


                    }
                    break;
                case 6 :
                    // /home/dfranusic/SmsFilterScript.g:757:82: ftype= F_M3UA
                    {
                    ftype=(Token)match(input,F_M3UA,FOLLOW_F_M3UA_in_filter7857);  
                    stream_F_M3UA.add(ftype);


                    }
                    break;

            }

            // /home/dfranusic/SmsFilterScript.g:757:96: ( WS )*
            loop255:
            do {
                int alt255=2;
                int LA255_0 = input.LA(1);

                if ( (LA255_0==WS) ) {
                    alt255=1;
                }


                switch (alt255) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:757:96: WS
            	    {
            	    WS504=(Token)match(input,WS,FOLLOW_WS_in_filter7860);  
            	    stream_WS.add(WS504);


            	    }
            	    break;

            	default :
            	    break loop255;
                }
            } while (true);

            pushFollow(FOLLOW_curlyblock_in_filter7863);
            curlyblock505=curlyblock();

            state._fsp--;

            stream_curlyblock.add(curlyblock505.getTree());
            // /home/dfranusic/SmsFilterScript.g:757:111: ( WS )*
            loop256:
            do {
                int alt256=2;
                int LA256_0 = input.LA(1);

                if ( (LA256_0==WS) ) {
                    alt256=1;
                }


                switch (alt256) {
            	case 1 :
            	    // /home/dfranusic/SmsFilterScript.g:757:111: WS
            	    {
            	    WS506=(Token)match(input,WS,FOLLOW_WS_in_filter7865);  
            	    stream_WS.add(WS506);


            	    }
            	    break;

            	default :
            	    break loop256;
                }
            } while (true);



            // AST REWRITE
            // elements: curlyblock, ftype
            // token labels: ftype
            // rule labels: retval
            // token list labels: 
            // rule list labels: 
            // wildcard labels: 
            retval.tree = root_0;
            RewriteRuleTokenStream stream_ftype=new RewriteRuleTokenStream(adaptor,"token ftype",ftype);
            RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

            root_0 = (Object)adaptor.nil();
            // 757:115: -> ^( FILTER_NODE ^( $ftype curlyblock ) )
            {
                // /home/dfranusic/SmsFilterScript.g:757:118: ^( FILTER_NODE ^( $ftype curlyblock ) )
                {
                Object root_1 = (Object)adaptor.nil();
                root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(FILTER_NODE, "FILTER_NODE"), root_1);

                // /home/dfranusic/SmsFilterScript.g:757:131: ^( $ftype curlyblock )
                {
                Object root_2 = (Object)adaptor.nil();
                root_2 = (Object)adaptor.becomeRoot(stream_ftype.nextNode(), root_2);

                adaptor.addChild(root_2, stream_curlyblock.nextTree());

                adaptor.addChild(root_1, root_2);
                }

                adaptor.addChild(root_0, root_1);
                }

            }

            retval.tree = root_0;
            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "filter"

    public static class comparison_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "comparison"
    // /home/dfranusic/SmsFilterScript.g:776:1: comparison : ( EQUAL | NEQUAL | LT | GT | LTE | GTE | IN | NOT_IN );
    public final SmsFilterScriptParser.comparison_return comparison() throws RecognitionException {
        SmsFilterScriptParser.comparison_return retval = new SmsFilterScriptParser.comparison_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token set507=null;

        Object set507_tree=null;

        try {
            // /home/dfranusic/SmsFilterScript.g:777:3: ( EQUAL | NEQUAL | LT | GT | LTE | GTE | IN | NOT_IN )
            // /home/dfranusic/SmsFilterScript.g:
            {
            root_0 = (Object)adaptor.nil();

            set507=(Token)input.LT(1);
            if ( (input.LA(1)>=IN && input.LA(1)<=NOT_IN)||input.LA(1)==EQUAL||input.LA(1)==NEQUAL||(input.LA(1)>=LT && input.LA(1)<=GTE) ) {
                input.consume();
                adaptor.addChild(root_0, (Object)adaptor.create(set507));
                state.errorRecovery=false;
            }
            else {
                MismatchedSetException mse = new MismatchedSetException(null,input);
                throw mse;
            }


            }

            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "comparison"

    public static class action_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "action"
    // /home/dfranusic/SmsFilterScript.g:787:1: action : ( CONT | CONT_Q | ALW | ALW ( COLON WORD )+ | ALW_Q | ALW_Q ( COLON WORD )+ | ALWU | ALWU ( COLON WORD )+ | ALWU_Q | ALWU_Q ( COLON WORD )+ | DNY | DNY ( COLON WORD )+ | DNY_Q | DNY_Q ( COLON WORD )+ | g= GOTO ( WS )* d= WORD -> ^( $g $d) );
    public final SmsFilterScriptParser.action_return action() throws RecognitionException {
        SmsFilterScriptParser.action_return retval = new SmsFilterScriptParser.action_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token g=null;
        Token d=null;
        Token CONT508=null;
        Token CONT_Q509=null;
        Token ALW510=null;
        Token ALW511=null;
        Token COLON512=null;
        Token WORD513=null;
        Token ALW_Q514=null;
        Token ALW_Q515=null;
        Token COLON516=null;
        Token WORD517=null;
        Token ALWU518=null;
        Token ALWU519=null;
        Token COLON520=null;
        Token WORD521=null;
        Token ALWU_Q522=null;
        Token ALWU_Q523=null;
        Token COLON524=null;
        Token WORD525=null;
        Token DNY526=null;
        Token DNY527=null;
        Token COLON528=null;
        Token WORD529=null;
        Token DNY_Q530=null;
        Token DNY_Q531=null;
        Token COLON532=null;
        Token WORD533=null;
        Token WS534=null;

        Object g_tree=null;
        Object d_tree=null;
        Object CONT508_tree=null;
        Object CONT_Q509_tree=null;
        Object ALW510_tree=null;
        Object ALW511_tree=null;
        Object COLON512_tree=null;
        Object WORD513_tree=null;
        Object ALW_Q514_tree=null;
        Object ALW_Q515_tree=null;
        Object COLON516_tree=null;
        Object WORD517_tree=null;
        Object ALWU518_tree=null;
        Object ALWU519_tree=null;
        Object COLON520_tree=null;
        Object WORD521_tree=null;
        Object ALWU_Q522_tree=null;
        Object ALWU_Q523_tree=null;
        Object COLON524_tree=null;
        Object WORD525_tree=null;
        Object DNY526_tree=null;
        Object DNY527_tree=null;
        Object COLON528_tree=null;
        Object WORD529_tree=null;
        Object DNY_Q530_tree=null;
        Object DNY_Q531_tree=null;
        Object COLON532_tree=null;
        Object WORD533_tree=null;
        Object WS534_tree=null;
        RewriteRuleTokenStream stream_WORD=new RewriteRuleTokenStream(adaptor,"token WORD");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_GOTO=new RewriteRuleTokenStream(adaptor,"token GOTO");

        try {
            // /home/dfranusic/SmsFilterScript.g:788:3: ( CONT | CONT_Q | ALW | ALW ( COLON WORD )+ | ALW_Q | ALW_Q ( COLON WORD )+ | ALWU | ALWU ( COLON WORD )+ | ALWU_Q | ALWU_Q ( COLON WORD )+ | DNY | DNY ( COLON WORD )+ | DNY_Q | DNY_Q ( COLON WORD )+ | g= GOTO ( WS )* d= WORD -> ^( $g $d) )
            int alt264=15;
            alt264 = dfa264.predict(input);
            switch (alt264) {
                case 1 :
                    // /home/dfranusic/SmsFilterScript.g:788:5: CONT
                    {
                    root_0 = (Object)adaptor.nil();

                    CONT508=(Token)match(input,CONT,FOLLOW_CONT_in_action8042); 
                    CONT508_tree = (Object)adaptor.create(CONT508);
                    adaptor.addChild(root_0, CONT508_tree);


                    }
                    break;
                case 2 :
                    // /home/dfranusic/SmsFilterScript.g:789:5: CONT_Q
                    {
                    root_0 = (Object)adaptor.nil();

                    CONT_Q509=(Token)match(input,CONT_Q,FOLLOW_CONT_Q_in_action8048); 
                    CONT_Q509_tree = (Object)adaptor.create(CONT_Q509);
                    adaptor.addChild(root_0, CONT_Q509_tree);


                    }
                    break;
                case 3 :
                    // /home/dfranusic/SmsFilterScript.g:790:5: ALW
                    {
                    root_0 = (Object)adaptor.nil();

                    ALW510=(Token)match(input,ALW,FOLLOW_ALW_in_action8054); 
                    ALW510_tree = (Object)adaptor.create(ALW510);
                    adaptor.addChild(root_0, ALW510_tree);


                    }
                    break;
                case 4 :
                    // /home/dfranusic/SmsFilterScript.g:791:5: ALW ( COLON WORD )+
                    {
                    root_0 = (Object)adaptor.nil();

                    ALW511=(Token)match(input,ALW,FOLLOW_ALW_in_action8060); 
                    ALW511_tree = (Object)adaptor.create(ALW511);
                    adaptor.addChild(root_0, ALW511_tree);

                    // /home/dfranusic/SmsFilterScript.g:791:9: ( COLON WORD )+
                    int cnt257=0;
                    loop257:
                    do {
                        int alt257=2;
                        int LA257_0 = input.LA(1);

                        if ( (LA257_0==COLON) ) {
                            alt257=1;
                        }


                        switch (alt257) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:791:10: COLON WORD
                    	    {
                    	    COLON512=(Token)match(input,COLON,FOLLOW_COLON_in_action8063); 
                    	    WORD513=(Token)match(input,WORD,FOLLOW_WORD_in_action8066); 
                    	    WORD513_tree = (Object)adaptor.create(WORD513);
                    	    adaptor.addChild(root_0, WORD513_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt257 >= 1 ) break loop257;
                                EarlyExitException eee =
                                    new EarlyExitException(257, input);
                                throw eee;
                        }
                        cnt257++;
                    } while (true);


                    }
                    break;
                case 5 :
                    // /home/dfranusic/SmsFilterScript.g:792:5: ALW_Q
                    {
                    root_0 = (Object)adaptor.nil();

                    ALW_Q514=(Token)match(input,ALW_Q,FOLLOW_ALW_Q_in_action8074); 
                    ALW_Q514_tree = (Object)adaptor.create(ALW_Q514);
                    adaptor.addChild(root_0, ALW_Q514_tree);


                    }
                    break;
                case 6 :
                    // /home/dfranusic/SmsFilterScript.g:793:5: ALW_Q ( COLON WORD )+
                    {
                    root_0 = (Object)adaptor.nil();

                    ALW_Q515=(Token)match(input,ALW_Q,FOLLOW_ALW_Q_in_action8080); 
                    ALW_Q515_tree = (Object)adaptor.create(ALW_Q515);
                    adaptor.addChild(root_0, ALW_Q515_tree);

                    // /home/dfranusic/SmsFilterScript.g:793:11: ( COLON WORD )+
                    int cnt258=0;
                    loop258:
                    do {
                        int alt258=2;
                        int LA258_0 = input.LA(1);

                        if ( (LA258_0==COLON) ) {
                            alt258=1;
                        }


                        switch (alt258) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:793:12: COLON WORD
                    	    {
                    	    COLON516=(Token)match(input,COLON,FOLLOW_COLON_in_action8083); 
                    	    WORD517=(Token)match(input,WORD,FOLLOW_WORD_in_action8086); 
                    	    WORD517_tree = (Object)adaptor.create(WORD517);
                    	    adaptor.addChild(root_0, WORD517_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt258 >= 1 ) break loop258;
                                EarlyExitException eee =
                                    new EarlyExitException(258, input);
                                throw eee;
                        }
                        cnt258++;
                    } while (true);


                    }
                    break;
                case 7 :
                    // /home/dfranusic/SmsFilterScript.g:794:5: ALWU
                    {
                    root_0 = (Object)adaptor.nil();

                    ALWU518=(Token)match(input,ALWU,FOLLOW_ALWU_in_action8094); 
                    ALWU518_tree = (Object)adaptor.create(ALWU518);
                    adaptor.addChild(root_0, ALWU518_tree);


                    }
                    break;
                case 8 :
                    // /home/dfranusic/SmsFilterScript.g:795:5: ALWU ( COLON WORD )+
                    {
                    root_0 = (Object)adaptor.nil();

                    ALWU519=(Token)match(input,ALWU,FOLLOW_ALWU_in_action8100); 
                    ALWU519_tree = (Object)adaptor.create(ALWU519);
                    adaptor.addChild(root_0, ALWU519_tree);

                    // /home/dfranusic/SmsFilterScript.g:795:10: ( COLON WORD )+
                    int cnt259=0;
                    loop259:
                    do {
                        int alt259=2;
                        int LA259_0 = input.LA(1);

                        if ( (LA259_0==COLON) ) {
                            alt259=1;
                        }


                        switch (alt259) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:795:11: COLON WORD
                    	    {
                    	    COLON520=(Token)match(input,COLON,FOLLOW_COLON_in_action8103); 
                    	    WORD521=(Token)match(input,WORD,FOLLOW_WORD_in_action8106); 
                    	    WORD521_tree = (Object)adaptor.create(WORD521);
                    	    adaptor.addChild(root_0, WORD521_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt259 >= 1 ) break loop259;
                                EarlyExitException eee =
                                    new EarlyExitException(259, input);
                                throw eee;
                        }
                        cnt259++;
                    } while (true);


                    }
                    break;
                case 9 :
                    // /home/dfranusic/SmsFilterScript.g:796:5: ALWU_Q
                    {
                    root_0 = (Object)adaptor.nil();

                    ALWU_Q522=(Token)match(input,ALWU_Q,FOLLOW_ALWU_Q_in_action8114); 
                    ALWU_Q522_tree = (Object)adaptor.create(ALWU_Q522);
                    adaptor.addChild(root_0, ALWU_Q522_tree);


                    }
                    break;
                case 10 :
                    // /home/dfranusic/SmsFilterScript.g:797:5: ALWU_Q ( COLON WORD )+
                    {
                    root_0 = (Object)adaptor.nil();

                    ALWU_Q523=(Token)match(input,ALWU_Q,FOLLOW_ALWU_Q_in_action8120); 
                    ALWU_Q523_tree = (Object)adaptor.create(ALWU_Q523);
                    adaptor.addChild(root_0, ALWU_Q523_tree);

                    // /home/dfranusic/SmsFilterScript.g:797:12: ( COLON WORD )+
                    int cnt260=0;
                    loop260:
                    do {
                        int alt260=2;
                        int LA260_0 = input.LA(1);

                        if ( (LA260_0==COLON) ) {
                            alt260=1;
                        }


                        switch (alt260) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:797:13: COLON WORD
                    	    {
                    	    COLON524=(Token)match(input,COLON,FOLLOW_COLON_in_action8123); 
                    	    WORD525=(Token)match(input,WORD,FOLLOW_WORD_in_action8126); 
                    	    WORD525_tree = (Object)adaptor.create(WORD525);
                    	    adaptor.addChild(root_0, WORD525_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt260 >= 1 ) break loop260;
                                EarlyExitException eee =
                                    new EarlyExitException(260, input);
                                throw eee;
                        }
                        cnt260++;
                    } while (true);


                    }
                    break;
                case 11 :
                    // /home/dfranusic/SmsFilterScript.g:798:5: DNY
                    {
                    root_0 = (Object)adaptor.nil();

                    DNY526=(Token)match(input,DNY,FOLLOW_DNY_in_action8134); 
                    DNY526_tree = (Object)adaptor.create(DNY526);
                    adaptor.addChild(root_0, DNY526_tree);


                    }
                    break;
                case 12 :
                    // /home/dfranusic/SmsFilterScript.g:799:5: DNY ( COLON WORD )+
                    {
                    root_0 = (Object)adaptor.nil();

                    DNY527=(Token)match(input,DNY,FOLLOW_DNY_in_action8140); 
                    DNY527_tree = (Object)adaptor.create(DNY527);
                    adaptor.addChild(root_0, DNY527_tree);

                    // /home/dfranusic/SmsFilterScript.g:799:9: ( COLON WORD )+
                    int cnt261=0;
                    loop261:
                    do {
                        int alt261=2;
                        int LA261_0 = input.LA(1);

                        if ( (LA261_0==COLON) ) {
                            alt261=1;
                        }


                        switch (alt261) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:799:10: COLON WORD
                    	    {
                    	    COLON528=(Token)match(input,COLON,FOLLOW_COLON_in_action8143); 
                    	    WORD529=(Token)match(input,WORD,FOLLOW_WORD_in_action8146); 
                    	    WORD529_tree = (Object)adaptor.create(WORD529);
                    	    adaptor.addChild(root_0, WORD529_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt261 >= 1 ) break loop261;
                                EarlyExitException eee =
                                    new EarlyExitException(261, input);
                                throw eee;
                        }
                        cnt261++;
                    } while (true);


                    }
                    break;
                case 13 :
                    // /home/dfranusic/SmsFilterScript.g:800:5: DNY_Q
                    {
                    root_0 = (Object)adaptor.nil();

                    DNY_Q530=(Token)match(input,DNY_Q,FOLLOW_DNY_Q_in_action8154); 
                    DNY_Q530_tree = (Object)adaptor.create(DNY_Q530);
                    adaptor.addChild(root_0, DNY_Q530_tree);


                    }
                    break;
                case 14 :
                    // /home/dfranusic/SmsFilterScript.g:801:5: DNY_Q ( COLON WORD )+
                    {
                    root_0 = (Object)adaptor.nil();

                    DNY_Q531=(Token)match(input,DNY_Q,FOLLOW_DNY_Q_in_action8160); 
                    DNY_Q531_tree = (Object)adaptor.create(DNY_Q531);
                    adaptor.addChild(root_0, DNY_Q531_tree);

                    // /home/dfranusic/SmsFilterScript.g:801:11: ( COLON WORD )+
                    int cnt262=0;
                    loop262:
                    do {
                        int alt262=2;
                        int LA262_0 = input.LA(1);

                        if ( (LA262_0==COLON) ) {
                            alt262=1;
                        }


                        switch (alt262) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:801:12: COLON WORD
                    	    {
                    	    COLON532=(Token)match(input,COLON,FOLLOW_COLON_in_action8163); 
                    	    WORD533=(Token)match(input,WORD,FOLLOW_WORD_in_action8166); 
                    	    WORD533_tree = (Object)adaptor.create(WORD533);
                    	    adaptor.addChild(root_0, WORD533_tree);


                    	    }
                    	    break;

                    	default :
                    	    if ( cnt262 >= 1 ) break loop262;
                                EarlyExitException eee =
                                    new EarlyExitException(262, input);
                                throw eee;
                        }
                        cnt262++;
                    } while (true);


                    }
                    break;
                case 15 :
                    // /home/dfranusic/SmsFilterScript.g:802:5: g= GOTO ( WS )* d= WORD
                    {
                    g=(Token)match(input,GOTO,FOLLOW_GOTO_in_action8176);  
                    stream_GOTO.add(g);

                    // /home/dfranusic/SmsFilterScript.g:802:12: ( WS )*
                    loop263:
                    do {
                        int alt263=2;
                        int LA263_0 = input.LA(1);

                        if ( (LA263_0==WS) ) {
                            alt263=1;
                        }


                        switch (alt263) {
                    	case 1 :
                    	    // /home/dfranusic/SmsFilterScript.g:802:12: WS
                    	    {
                    	    WS534=(Token)match(input,WS,FOLLOW_WS_in_action8178);  
                    	    stream_WS.add(WS534);


                    	    }
                    	    break;

                    	default :
                    	    break loop263;
                        }
                    } while (true);

                    d=(Token)match(input,WORD,FOLLOW_WORD_in_action8183);  
                    stream_WORD.add(d);



                    // AST REWRITE
                    // elements: d, g
                    // token labels: g, d
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_g=new RewriteRuleTokenStream(adaptor,"token g",g);
                    RewriteRuleTokenStream stream_d=new RewriteRuleTokenStream(adaptor,"token d",d);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 802:23: -> ^( $g $d)
                    {
                        // /home/dfranusic/SmsFilterScript.g:802:26: ^( $g $d)
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot(stream_g.nextNode(), root_1);

                        adaptor.addChild(root_1, stream_d.nextNode());

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);

        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "action"

    // Delegated rules


    protected DFA92 dfa92 = new DFA92(this);
    protected DFA166 dfa166 = new DFA166(this);
    protected DFA168 dfa168 = new DFA168(this);
    protected DFA169 dfa169 = new DFA169(this);
    protected DFA213 dfa213 = new DFA213(this);
    protected DFA229 dfa229 = new DFA229(this);
    protected DFA242 dfa242 = new DFA242(this);
    protected DFA253 dfa253 = new DFA253(this);
    protected DFA264 dfa264 = new DFA264(this);
    static final String DFA92_eotS =
        "\131\uffff";
    static final String DFA92_eofS =
        "\131\uffff";
    static final String DFA92_minS =
        "\1\u00b8\2\10\1\u00ae\4\uffff\1\u00ae\1\uffff\5\u00b8\11\uffff\1"+
        "\u00ae\1\4\1\u00ae\1\12\1\u00b8\1\10\1\u00b8\1\u00a8\1\u00b8\1\10"+
        "\1\u00b8\1\u00a8\1\u00b8\1\u00a8\1\4\4\uffff\1\12\2\uffff\1\10\1"+
        "\u00dc\1\u00a8\3\u00b9\1\10\1\u00dc\1\u00a8\3\u00b9\1\u00a8\3\u00b9"+
        "\1\u00dc\1\u00a8\1\u00b9\1\u00ae\1\u00dc\1\u00a8\1\u00b9\1\u00ae"+
        "\1\u00b9\1\u00ae\1\u00a8\3\u00b9\1\u00ae\1\u00a3\1\u00a8\4\u00b9"+
        "\1\u00ae\1\u00a3\2\uffff\1\u00b9\1\u00ae";
    static final String DFA92_maxS =
        "\1\u00b8\3\u00f8\4\uffff\1\u00f8\1\uffff\5\u00f8\11\uffff\1\u00f8"+
        "\1\u00f9\1\u00f8\1\u00fa\12\u00f8\1\u00f9\4\uffff\1\u00fa\2\uffff"+
        "\37\u00f8\1\u00fa\6\u00f8\1\u00fa\2\uffff\2\u00f8";
    static final String DFA92_acceptS =
        "\4\uffff\1\11\1\13\1\12\1\14\1\uffff\1\15\5\uffff\1\20\1\22\1\3"+
        "\1\23\1\21\1\6\1\17\1\16\1\5\17\uffff\1\26\1\1\1\2\1\7\1\uffff\1"+
        "\10\1\4\47\uffff\1\25\1\24\2\uffff";
    static final String DFA92_specialS =
        "\131\uffff}>";
    static final String[] DFA92_transitionS = {
            "\1\1",
            "\2\3\2\10\3\3\1\11\1\7\1\3\1\11\1\7\1\3\1\26\1\25\1\17\1\10"+
            "\1\uffff\2\10\1\23\1\20\1\22\2\uffff\2\10\1\3\62\uffff\2\3\2"+
            "\10\2\4\2\6\2\5\4\3\2\10\1\3\1\27\1\3\1\27\1\3\1\10\10\3\4\10"+
            "\1\21\1\24\51\uffff\1\12\1\14\1\13\1\15\1\16\120\uffff\1\2",
            "\2\3\2\10\3\3\1\11\1\7\1\3\1\11\1\7\1\3\1\26\1\25\1\17\1\10"+
            "\1\uffff\2\10\1\23\1\20\1\22\2\uffff\2\10\1\3\62\uffff\2\3\2"+
            "\10\2\4\2\6\2\5\4\3\2\10\1\3\1\27\1\3\1\27\1\3\1\10\10\3\4\10"+
            "\1\21\1\24\51\uffff\1\12\1\14\1\13\1\15\1\16\120\uffff\1\2",
            "\2\31\27\uffff\1\31\2\uffff\1\31\4\uffff\4\31\45\uffff\1\30",
            "",
            "",
            "",
            "",
            "\2\33\27\uffff\1\33\2\uffff\1\33\4\uffff\4\33\45\uffff\1\32",
            "",
            "\1\35\77\uffff\1\34",
            "\1\37\77\uffff\1\36",
            "\1\41\77\uffff\1\40",
            "\1\43\77\uffff\1\42",
            "\1\45\77\uffff\1\44",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\2\31\27\uffff\1\31\2\uffff\1\31\4\uffff\4\31\45\uffff\1\30",
            "\4\50\2\52\2\uffff\3\52\2\uffff\1\52\2\uffff\1\52\16\uffff"+
            "\1\52\62\uffff\2\52\10\uffff\4\52\2\uffff\1\52\1\uffff\1\52"+
            "\1\uffff\1\52\1\uffff\10\52\70\uffff\1\47\113\uffff\1\46\1\51",
            "\2\33\27\uffff\1\33\2\uffff\1\33\4\uffff\4\33\45\uffff\1\32",
            "\2\54\14\uffff\1\54\1\uffff\2\54\5\uffff\2\54\65\uffff\2\54"+
            "\12\uffff\2\54\5\uffff\1\54\10\uffff\4\54\u0080\uffff\1\53\1"+
            "\uffff\1\55",
            "\1\35\77\uffff\1\34",
            "\2\57\2\uffff\3\57\2\uffff\1\57\2\uffff\1\57\16\uffff\1\57"+
            "\62\uffff\2\57\10\uffff\4\57\2\uffff\1\57\1\uffff\1\57\1\uffff"+
            "\1\57\1\uffff\10\57\u0084\uffff\1\56",
            "\1\37\77\uffff\1\36",
            "\1\62\1\61\1\63\115\uffff\1\60",
            "\1\41\77\uffff\1\40",
            "\2\65\2\uffff\3\65\2\uffff\1\65\2\uffff\1\65\16\uffff\1\65"+
            "\62\uffff\2\65\10\uffff\4\65\2\uffff\1\65\1\uffff\1\65\1\uffff"+
            "\1\65\1\uffff\10\65\u0084\uffff\1\64",
            "\1\43\77\uffff\1\42",
            "\1\70\1\67\1\71\115\uffff\1\66",
            "\1\45\77\uffff\1\44",
            "\1\74\1\73\1\75\115\uffff\1\72",
            "\4\50\2\52\2\uffff\3\52\2\uffff\1\52\2\uffff\1\52\16\uffff"+
            "\1\52\62\uffff\2\52\10\uffff\4\52\2\uffff\1\52\1\uffff\1\52"+
            "\1\uffff\1\52\1\uffff\10\52\70\uffff\1\47\113\uffff\1\46\1\51",
            "",
            "",
            "",
            "",
            "\2\54\14\uffff\1\54\1\uffff\2\54\5\uffff\2\54\65\uffff\2\54"+
            "\12\uffff\2\54\5\uffff\1\54\10\uffff\4\54\u0080\uffff\1\53\1"+
            "\uffff\1\55",
            "",
            "",
            "\2\57\2\uffff\3\57\2\uffff\1\57\2\uffff\1\57\16\uffff\1\57"+
            "\62\uffff\2\57\10\uffff\4\57\2\uffff\1\57\1\uffff\1\57\1\uffff"+
            "\1\57\1\uffff\10\57\u0084\uffff\1\56",
            "\1\77\33\uffff\1\76",
            "\1\62\1\61\1\63\115\uffff\1\60",
            "\1\101\76\uffff\1\100",
            "\1\101\76\uffff\1\100",
            "\1\101\76\uffff\1\100",
            "\2\65\2\uffff\3\65\2\uffff\1\65\2\uffff\1\65\16\uffff\1\65"+
            "\62\uffff\2\65\10\uffff\4\65\2\uffff\1\65\1\uffff\1\65\1\uffff"+
            "\1\65\1\uffff\10\65\u0084\uffff\1\64",
            "\1\103\33\uffff\1\102",
            "\1\70\1\67\1\71\115\uffff\1\66",
            "\1\105\76\uffff\1\104",
            "\1\105\76\uffff\1\104",
            "\1\105\76\uffff\1\104",
            "\1\74\1\73\1\75\115\uffff\1\72",
            "\1\107\76\uffff\1\106",
            "\1\107\76\uffff\1\106",
            "\1\107\76\uffff\1\106",
            "\1\77\33\uffff\1\76",
            "\1\112\1\111\1\113\115\uffff\1\110",
            "\1\101\76\uffff\1\100",
            "\2\115\27\uffff\1\115\2\uffff\1\115\4\uffff\4\115\45\uffff"+
            "\1\114",
            "\1\103\33\uffff\1\102",
            "\1\120\1\117\1\121\115\uffff\1\116",
            "\1\105\76\uffff\1\104",
            "\2\115\27\uffff\1\115\2\uffff\1\115\4\uffff\4\115\45\uffff"+
            "\1\114",
            "\1\107\76\uffff\1\106",
            "\2\115\27\uffff\1\115\2\uffff\1\115\4\uffff\4\115\45\uffff"+
            "\1\114",
            "\1\112\1\111\1\113\115\uffff\1\110",
            "\1\123\76\uffff\1\122",
            "\1\123\76\uffff\1\122",
            "\1\123\76\uffff\1\122",
            "\2\115\27\uffff\1\115\2\uffff\1\115\4\uffff\4\115\45\uffff"+
            "\1\114",
            "\5\126\120\uffff\1\124\1\uffff\1\125",
            "\1\120\1\117\1\121\115\uffff\1\116",
            "\1\130\76\uffff\1\127",
            "\1\130\76\uffff\1\127",
            "\1\130\76\uffff\1\127",
            "\1\123\76\uffff\1\122",
            "\2\115\27\uffff\1\115\2\uffff\1\115\4\uffff\4\115\45\uffff"+
            "\1\114",
            "\5\126\120\uffff\1\124\1\uffff\1\125",
            "",
            "",
            "\1\130\76\uffff\1\127",
            "\2\115\27\uffff\1\115\2\uffff\1\115\4\uffff\4\115\45\uffff"+
            "\1\114"
    };

    static final short[] DFA92_eot = DFA.unpackEncodedString(DFA92_eotS);
    static final short[] DFA92_eof = DFA.unpackEncodedString(DFA92_eofS);
    static final char[] DFA92_min = DFA.unpackEncodedStringToUnsignedChars(DFA92_minS);
    static final char[] DFA92_max = DFA.unpackEncodedStringToUnsignedChars(DFA92_maxS);
    static final short[] DFA92_accept = DFA.unpackEncodedString(DFA92_acceptS);
    static final short[] DFA92_special = DFA.unpackEncodedString(DFA92_specialS);
    static final short[][] DFA92_transition;

    static {
        int numStates = DFA92_transitionS.length;
        DFA92_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA92_transition[i] = DFA.unpackEncodedString(DFA92_transitionS[i]);
        }
    }

    class DFA92 extends DFA {

        public DFA92(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 92;
            this.eot = DFA92_eot;
            this.eof = DFA92_eof;
            this.min = DFA92_min;
            this.max = DFA92_max;
            this.accept = DFA92_accept;
            this.special = DFA92_special;
            this.transition = DFA92_transition;
        }
        public String getDescription() {
            return "455:1: evalsimple : ( L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* (b= SCCP_GT_CALLED_WL | b= SCCP_GT_CALLING_WL | b= MAP_SCOA_WL | b= MAP_SCDA_WL ) ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $b) | L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $bstr) | L_PAREN ( WS )* w1= eval_dcs ( WS )* a= comparison ( WS )* br1= dcs ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w1 $a $br1) | L_PAREN ( WS )* w2= eval_number ( WS )* a= comparison ( WS )* br2= DIGITS ( PERCENT )* ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w2 $a $br2) | L_PAREN ( WS )* w3= eval_ton ( WS )* a= comparison ( WS )* br3= typeofnum ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w3 $a $br3) | L_PAREN ( WS )* w4= eval_msg_type ( WS )* a= comparison ( WS )* br4= msgtype ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w4 $a $br4) | L_PAREN ( WS )* w5= eval_string ( WS )* a= comparison ( WS )* br5= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w5 $a $br5) | L_PAREN ( WS )* w6= eval_number ( WS )* a= comparison ( WS )* br6= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w6 $a $br6) | L_PAREN ( WS )* w7= eval_nai ( WS )* a= comparison ( WS )* br7= nai ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w7 $a $br7) | L_PAREN ( WS )* w8= eval_np ( WS )* a= comparison ( WS )* br8= np ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w8 $a $br8) | L_PAREN ( WS )* w9= eval_gti ( WS )* a= comparison ( WS )* br9= gti ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w9 $a $br9) | L_PAREN ( WS )* w10= smpp_eval_np ( WS )* a= comparison ( WS )* br10= smpp_np ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w10 $a $br10) | L_PAREN ( WS )* w11= smpp_eval_ton ( WS )* a= comparison ( WS )* br11= smpp_typeofnum ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w11 $a $br11) | L_PAREN ( WS )* w12= smpp_eval_esm_mm ( WS )* a= comparison ( WS )* br12= smpp_esm_mm ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w12 $a $br12) | L_PAREN ( WS )* w13= smpp_eval_esm_mt ( WS )* a= comparison ( WS )* br13= smpp_esm_mt ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w13 $a $br13) | L_PAREN ( WS )* w14= smpp_eval_esm_gf ( WS )* a= comparison ( WS )* br14= smpp_esm_gf ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w14 $a $br14) | L_PAREN ( WS )* w15= smpp_eval_rd_smscdr ( WS )* a= comparison ( WS )* br15= smpp_rd_smscdr ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w15 $a $br15) | L_PAREN ( WS )* w16= smpp_eval_rd_smeoa ( WS )* a= comparison ( WS )* br16= smpp_rd_smeoa ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w16 $a $br16) | L_PAREN ( WS )* w17= smpp_eval_rd_in ( WS )* a= comparison ( WS )* br17= smpp_rd_in ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w17 $a $br17) | L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fvar2= floodmethod ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $fvar $a $fvar2) | L_PAREN ( WS )* fvar= floodmethod ( WS )* a= comparison ( WS )* fdigs= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $fvar $a $fdigs) | L_PAREN ( WS )* w= eval_string ( WS )* a= comparison ( WS )* lvar= listmethod ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM $w $a $lvar) );";
        }
    }
    static final String DFA166_eotS =
        "\103\uffff";
    static final String DFA166_eofS =
        "\103\uffff";
    static final String DFA166_minS =
        "\1\u00b8\2\u00da\16\u00b8\1\10\3\u00ae\1\10\1\u00ae\2\10\1\uffff"+
        "\1\u00b8\2\uffff\1\10\1\u00b8\4\uffff\15\u00b8\1\10\15\u00b8\1\10"+
        "\4\uffff";
    static final String DFA166_maxS =
        "\1\u00b8\2\u00f8\16\u0105\1\167\3\u00f8\1\u00fa\1\u00f8\2\u00fa"+
        "\1\uffff\1\u0105\2\uffff\1\u00fa\1\u0105\4\uffff\15\u0105\1\167"+
        "\15\u0105\1\167\4\uffff";
    static final String DFA166_acceptS =
        "\31\uffff\1\2\1\uffff\1\11\1\1\2\uffff\1\5\1\4\1\13\1\12\34\uffff"+
        "\1\7\1\3\1\6\1\10";
    static final String DFA166_specialS =
        "\103\uffff}>";
    static final String[] DFA166_transitionS = {
            "\1\1",
            "\1\3\35\uffff\1\2",
            "\1\3\35\uffff\1\2",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\1\12\1\13\1\11\1\10\27\uffff\1\17\6\uffff\1\21\1\15\36\uffff"+
            "\1\5\1\4\5\uffff\1\6\1\7\1\14\1\16\1\20",
            "\2\22\2\23\3\22\2\uffff\1\22\2\uffff\1\22\3\uffff\1\23\1\uffff"+
            "\2\23\5\uffff\2\23\1\22\62\uffff\2\22\2\23\6\uffff\4\22\2\23"+
            "\1\22\1\uffff\1\22\1\uffff\1\22\1\23\10\22\4\23",
            "\2\25\27\uffff\1\25\2\uffff\1\25\4\uffff\4\25\45\uffff\1\24",
            "\2\27\27\uffff\1\27\2\uffff\1\27\4\uffff\4\27\45\uffff\1\26",
            "\2\25\27\uffff\1\25\2\uffff\1\25\4\uffff\4\25\45\uffff\1\24",
            "\2\31\2\uffff\3\31\2\uffff\1\31\2\uffff\1\31\16\uffff\1\31"+
            "\62\uffff\2\31\10\uffff\4\31\2\uffff\1\31\1\uffff\1\31\1\uffff"+
            "\1\31\1\uffff\10\31\146\uffff\1\32\35\uffff\1\30\1\34\1\33",
            "\2\27\27\uffff\1\27\2\uffff\1\27\4\uffff\4\27\45\uffff\1\26",
            "\2\41\2\37\3\41\2\uffff\1\41\2\uffff\1\41\3\uffff\1\37\1\uffff"+
            "\2\37\5\uffff\2\37\1\41\62\uffff\2\41\2\37\6\uffff\4\41\2\37"+
            "\1\41\1\uffff\1\41\1\uffff\1\41\1\37\10\41\4\37\142\uffff\1"+
            "\36\35\uffff\1\35\1\42\1\40",
            "\2\31\2\uffff\3\31\2\uffff\1\31\2\uffff\1\31\16\uffff\1\31"+
            "\62\uffff\2\31\10\uffff\4\31\2\uffff\1\31\1\uffff\1\31\1\uffff"+
            "\1\31\1\uffff\10\31\146\uffff\1\32\35\uffff\1\30\1\34\1\33",
            "",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "",
            "",
            "\2\41\2\37\3\41\2\uffff\1\41\2\uffff\1\41\3\uffff\1\37\1\uffff"+
            "\2\37\5\uffff\2\37\1\41\62\uffff\2\41\2\37\6\uffff\4\41\2\37"+
            "\1\41\1\uffff\1\41\1\uffff\1\41\1\37\10\41\4\37\142\uffff\1"+
            "\36\35\uffff\1\35\1\42\1\40",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "",
            "",
            "",
            "",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\1\51\1\52\1\50\1\47\27\uffff\1\56\6\uffff\1\60\1\54\36\uffff"+
            "\1\44\1\43\5\uffff\1\45\1\46\1\53\1\55\1\57",
            "\2\100\2\77\3\100\2\uffff\1\100\2\uffff\1\100\3\uffff\1\77"+
            "\1\uffff\2\77\5\uffff\2\77\1\100\62\uffff\2\100\2\77\6\uffff"+
            "\4\100\2\77\1\100\1\uffff\1\100\1\uffff\1\100\1\77\10\100\4"+
            "\77",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\1\67\1\70\1\66\1\65\27\uffff\1\74\6\uffff\1\76\1\72\36\uffff"+
            "\1\62\1\61\5\uffff\1\63\1\64\1\71\1\73\1\75",
            "\2\102\2\101\3\102\2\uffff\1\102\2\uffff\1\102\3\uffff\1\101"+
            "\1\uffff\2\101\5\uffff\2\101\1\102\62\uffff\2\102\2\101\6\uffff"+
            "\4\102\2\101\1\102\1\uffff\1\102\1\uffff\1\102\1\101\10\102"+
            "\4\101",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA166_eot = DFA.unpackEncodedString(DFA166_eotS);
    static final short[] DFA166_eof = DFA.unpackEncodedString(DFA166_eofS);
    static final char[] DFA166_min = DFA.unpackEncodedStringToUnsignedChars(DFA166_minS);
    static final char[] DFA166_max = DFA.unpackEncodedStringToUnsignedChars(DFA166_maxS);
    static final short[] DFA166_accept = DFA.unpackEncodedString(DFA166_acceptS);
    static final short[] DFA166_special = DFA.unpackEncodedString(DFA166_specialS);
    static final short[][] DFA166_transition;

    static {
        int numStates = DFA166_transitionS.length;
        DFA166_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA166_transition[i] = DFA.unpackEncodedString(DFA166_transitionS[i]);
        }
    }

    class DFA166 extends DFA {

        public DFA166(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 166;
            this.eot = DFA166_eot;
            this.eof = DFA166_eof;
            this.min = DFA166_min;
            this.max = DFA166_max;
            this.accept = DFA166_accept;
            this.special = DFA166_special;
            this.transition = DFA166_transition;
        }
        public String getDescription() {
            return "514:1: regex : ( L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $bstr) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d3) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bb2= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bb2) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d2) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bn3) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK bn3= eval_number ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $bn3) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* REGEX_BLOCK r2= regexdef REGEX_BLOCK b2= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r2.res] ) $b2) ) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK b2= eval_string ( WS )* c2= comparison ( WS )* d2= DIGITS ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $b2) $c2 $d2) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* bstr= STRINGLITERAL ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $bstr) | L_PAREN ( WS )* REGEX_BLOCK r= regexdef REGEX_BLOCK bn2= eval_number ( WS )* c2= comparison ( WS )* d3= eval_string ( WS )* R_PAREN -> ^( RULE_EVAL_ATOM ^( RULE_REGEX ^( RULE_REGEX_EXPR[$r.res] ) $bn2) $c2 $d3) );";
        }
    }
    static final String DFA168_eotS =
        "\5\uffff";
    static final String DFA168_eofS =
        "\5\uffff";
    static final String DFA168_minS =
        "\1\u00b8\2\10\2\uffff";
    static final String DFA168_maxS =
        "\1\u00b8\2\u00f8\2\uffff";
    static final String DFA168_acceptS =
        "\3\uffff\1\1\1\2";
    static final String DFA168_specialS =
        "\5\uffff}>";
    static final String[] DFA168_transitionS = {
            "\1\1",
            "\21\3\1\uffff\5\3\2\uffff\3\3\62\uffff\44\3\51\uffff\5\3\62"+
            "\uffff\1\4\35\uffff\1\2",
            "\21\3\1\uffff\5\3\2\uffff\3\3\62\uffff\44\3\51\uffff\5\3\62"+
            "\uffff\1\4\35\uffff\1\2",
            "",
            ""
    };

    static final short[] DFA168_eot = DFA.unpackEncodedString(DFA168_eotS);
    static final short[] DFA168_eof = DFA.unpackEncodedString(DFA168_eofS);
    static final char[] DFA168_min = DFA.unpackEncodedStringToUnsignedChars(DFA168_minS);
    static final char[] DFA168_max = DFA.unpackEncodedStringToUnsignedChars(DFA168_maxS);
    static final short[] DFA168_accept = DFA.unpackEncodedString(DFA168_acceptS);
    static final short[] DFA168_special = DFA.unpackEncodedString(DFA168_specialS);
    static final short[][] DFA168_transition;

    static {
        int numStates = DFA168_transitionS.length;
        DFA168_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA168_transition[i] = DFA.unpackEncodedString(DFA168_transitionS[i]);
        }
    }

    class DFA168 extends DFA {

        public DFA168(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 168;
            this.eot = DFA168_eot;
            this.eof = DFA168_eof;
            this.min = DFA168_min;
            this.max = DFA168_max;
            this.accept = DFA168_accept;
            this.special = DFA168_special;
            this.transition = DFA168_transition;
        }
        public String getDescription() {
            return "680:1: eval : ( evalsimple | regex );";
        }
    }
    static final String DFA169_eotS =
        "\33\uffff";
    static final String DFA169_eofS =
        "\33\uffff";
    static final String DFA169_minS =
        "\1\u00bb\3\uffff\1\u00c6\1\u00cb\1\10\1\u00c4\1\u00bb\1\10\1\uffff"+
        "\1\10\2\u00ba\1\u00c6\1\10\2\uffff\1\u00cb\1\u00c4\1\u00ad\2\u00ba"+
        "\2\uffff\1\u00cb\1\u00ad";
    static final String DFA169_maxS =
        "\1\u00fb\3\uffff\1\u00c6\1\u00cb\1\u00f8\1\u00c5\1\u00bb\1\u00f8"+
        "\1\uffff\1\u00fb\2\u00ba\1\u00c6\1\u00fb\2\uffff\1\u00cb\1\u00c5"+
        "\1\u00d6\2\u00ba\2\uffff\1\u00cb\1\u00d6";
    static final String DFA169_acceptS =
        "\1\uffff\1\10\1\1\1\2\6\uffff\1\6\5\uffff\1\4\1\7\5\uffff\1\3\1"+
        "\5\2\uffff";
    static final String DFA169_specialS =
        "\33\uffff}>";
    static final String[] DFA169_transitionS = {
            "\1\4\1\2\1\1\15\uffff\1\6\54\uffff\1\3\2\uffff\1\5",
            "",
            "",
            "",
            "\1\7",
            "\1\10",
            "\7\12\2\uffff\1\12\2\uffff\1\12\3\uffff\1\12\1\uffff\2\12\5"+
            "\uffff\3\12\62\uffff\44\12\37\uffff\12\12\25\uffff\1\13\77\uffff"+
            "\1\11",
            "\1\14\1\15",
            "\1\16",
            "\7\12\2\uffff\1\12\2\uffff\1\12\3\uffff\1\12\1\uffff\2\12\5"+
            "\uffff\3\12\62\uffff\44\12\37\uffff\12\12\25\uffff\1\13\77\uffff"+
            "\1\11",
            "",
            "\21\20\1\uffff\5\20\2\uffff\3\20\62\uffff\44\20\51\uffff\5"+
            "\20\10\uffff\10\20\41\uffff\2\20\35\uffff\1\17\2\uffff\1\21",
            "\1\22",
            "\1\22",
            "\1\23",
            "\21\20\1\uffff\5\20\2\uffff\3\20\62\uffff\44\20\51\uffff\5"+
            "\20\10\uffff\10\20\41\uffff\2\20\35\uffff\1\17\2\uffff\1\21",
            "",
            "",
            "\1\24",
            "\1\25\1\26",
            "\1\27\50\uffff\1\30",
            "\1\31",
            "\1\31",
            "",
            "",
            "\1\32",
            "\1\27\50\uffff\1\30"
    };

    static final short[] DFA169_eot = DFA.unpackEncodedString(DFA169_eotS);
    static final short[] DFA169_eof = DFA.unpackEncodedString(DFA169_eofS);
    static final char[] DFA169_min = DFA.unpackEncodedStringToUnsignedChars(DFA169_minS);
    static final char[] DFA169_max = DFA.unpackEncodedStringToUnsignedChars(DFA169_maxS);
    static final short[] DFA169_accept = DFA.unpackEncodedString(DFA169_acceptS);
    static final short[] DFA169_special = DFA.unpackEncodedString(DFA169_specialS);
    static final short[][] DFA169_transition;

    static {
        int numStates = DFA169_transitionS.length;
        DFA169_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA169_transition[i] = DFA.unpackEncodedString(DFA169_transitionS[i]);
        }
    }

    class DFA169 extends DFA {

        public DFA169(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 169;
            this.eot = DFA169_eot;
            this.eof = DFA169_eof;
            this.min = DFA169_min;
            this.max = DFA169_max;
            this.accept = DFA169_accept;
            this.special = DFA169_special;
            this.transition = DFA169_transition;
        }
        public String getDescription() {
            return "()* loopback of 686:13: ( curlyblock | WS | rule | rulebody | modify | modifybody | m3ua )*";
        }
    }
    static final String DFA213_eotS =
        "\32\uffff";
    static final String DFA213_eofS =
        "\32\uffff";
    static final String DFA213_minS =
        "\1\u0099\11\uffff\2\u00b8\2\10\3\u00dc\1\10\1\u00dc\2\10\2\uffff"+
        "\1\10\2\uffff";
    static final String DFA213_maxS =
        "\1\u00a2\11\uffff\2\u00f8\2\u00f9\3\u00f8\1\u00f9\1\u00f8\2\u00f9"+
        "\2\uffff\1\u00f9\2\uffff";
    static final String DFA213_acceptS =
        "\1\uffff\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\1\11\13\uffff\1\12\1\15"+
        "\1\uffff\1\14\1\13";
    static final String DFA213_specialS =
        "\32\uffff}>";
    static final String[] DFA213_transitionS = {
            "\1\1\1\2\1\3\1\4\1\5\1\6\1\12\1\7\1\10\1\11",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "\1\14\77\uffff\1\13",
            "\1\14\77\uffff\1\13",
            "\2\17\2\uffff\3\17\2\uffff\1\17\2\uffff\1\17\16\uffff\1\17"+
            "\62\uffff\2\17\10\uffff\4\17\2\uffff\1\17\1\uffff\1\17\1\uffff"+
            "\1\17\1\uffff\10\17\u0084\uffff\1\15\1\16",
            "\2\17\2\uffff\3\17\2\uffff\1\17\2\uffff\1\17\16\uffff\1\17"+
            "\62\uffff\2\17\10\uffff\4\17\2\uffff\1\17\1\uffff\1\17\1\uffff"+
            "\1\17\1\uffff\10\17\u0084\uffff\1\15\1\16",
            "\1\21\33\uffff\1\20",
            "\1\23\33\uffff\1\22",
            "\1\21\33\uffff\1\20",
            "\2\26\2\uffff\3\26\2\uffff\1\26\2\uffff\1\26\16\uffff\1\26"+
            "\62\uffff\2\26\10\uffff\4\26\2\uffff\1\26\1\uffff\1\26\1\uffff"+
            "\1\26\1\uffff\10\26\u0084\uffff\1\24\1\25",
            "\1\23\33\uffff\1\22",
            "\2\31\2\uffff\3\31\2\uffff\1\31\2\uffff\1\31\16\uffff\1\31"+
            "\62\uffff\2\31\10\uffff\4\31\2\uffff\1\31\1\uffff\1\31\1\uffff"+
            "\1\31\1\uffff\10\31\u0084\uffff\1\27\1\30",
            "\2\26\2\uffff\3\26\2\uffff\1\26\2\uffff\1\26\16\uffff\1\26"+
            "\62\uffff\2\26\10\uffff\4\26\2\uffff\1\26\1\uffff\1\26\1\uffff"+
            "\1\26\1\uffff\10\26\u0084\uffff\1\24\1\25",
            "",
            "",
            "\2\31\2\uffff\3\31\2\uffff\1\31\2\uffff\1\31\16\uffff\1\31"+
            "\62\uffff\2\31\10\uffff\4\31\2\uffff\1\31\1\uffff\1\31\1\uffff"+
            "\1\31\1\uffff\10\31\u0084\uffff\1\27\1\30",
            "",
            ""
    };

    static final short[] DFA213_eot = DFA.unpackEncodedString(DFA213_eotS);
    static final short[] DFA213_eof = DFA.unpackEncodedString(DFA213_eofS);
    static final char[] DFA213_min = DFA.unpackEncodedStringToUnsignedChars(DFA213_minS);
    static final char[] DFA213_max = DFA.unpackEncodedStringToUnsignedChars(DFA213_maxS);
    static final short[] DFA213_accept = DFA.unpackEncodedString(DFA213_acceptS);
    static final short[] DFA213_special = DFA.unpackEncodedString(DFA213_specialS);
    static final short[][] DFA213_transition;

    static {
        int numStates = DFA213_transitionS.length;
        DFA213_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA213_transition[i] = DFA.unpackEncodedString(DFA213_transitionS[i]);
        }
    }

    class DFA213 extends DFA {

        public DFA213(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 213;
            this.eot = DFA213_eot;
            this.eof = DFA213_eof;
            this.min = DFA213_min;
            this.max = DFA213_max;
            this.accept = DFA213_accept;
            this.special = DFA213_special;
            this.transition = DFA213_transition;
        }
        public String getDescription() {
            return "699:1: modifier_method : ( SPAM_UPDATE_LST | SPAM_REMOVE_LST | QUARANTINE_UPDATE_LST | QUARANTINE_REMOVE_LST | MD5_UPDATE_LST | MD5_REMOVE_LST | NO_DR | CONVERT_SS7 | CONVERT_SMPP | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn= STRINGLITERAL ( WS )* COMMA ( WS )* sca= STRINGLITERAL ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn) ^( HLR_SCA $sca) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn1= eval_string ( WS )* COMMA ( WS )* sca1= eval_string ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn1) ^( HLR_SCA $sca1) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn2= eval_string ( WS )* COMMA ( WS )* sca2= STRINGLITERAL ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn2) ^( HLR_SCA $sca2) ) | r= HLR_REQUEST ( WS )* L_PAREN ( WS )* msisdn3= STRINGLITERAL ( WS )* COMMA ( WS )* sca3= eval_string ( WS )* R_PAREN -> ^( $r ^( HLR_MSISDN $msisdn3) ^( HLR_SCA $sca3) ) );";
        }
    }
    static final String DFA229_eotS =
        "\21\uffff";
    static final String DFA229_eofS =
        "\21\uffff";
    static final String DFA229_minS =
        "\1\u00cb\2\10\1\uffff\11\u00c8\2\10\2\uffff";
    static final String DFA229_maxS =
        "\1\u00cb\2\u00f8\1\uffff\11\u00f8\2\u00f9\2\uffff";
    static final String DFA229_acceptS =
        "\3\uffff\1\3\13\uffff\1\2\1\1";
    static final String DFA229_specialS =
        "\21\uffff}>";
    static final String[] DFA229_transitionS = {
            "\1\1",
            "\2\5\2\6\3\5\2\uffff\1\5\2\uffff\1\5\3\uffff\1\6\1\uffff\2"+
            "\6\5\uffff\2\6\1\5\62\uffff\2\5\2\6\2\11\2\12\2\13\4\5\2\6\1"+
            "\5\1\7\1\5\1\7\1\5\1\6\10\5\4\6\1\4\1\10\37\uffff\12\3\125\uffff"+
            "\1\2",
            "\2\5\2\6\3\5\2\uffff\1\5\2\uffff\1\5\3\uffff\1\6\1\uffff\2"+
            "\6\5\uffff\2\6\1\5\62\uffff\2\5\2\6\2\11\2\12\2\13\4\5\2\6\1"+
            "\5\1\7\1\5\1\7\1\5\1\6\10\5\4\6\1\4\1\10\37\uffff\12\3\125\uffff"+
            "\1\2",
            "",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\1\15\57\uffff\1\14",
            "\7\17\2\uffff\1\17\2\uffff\1\17\3\uffff\1\17\1\uffff\2\17\5"+
            "\uffff\3\17\62\uffff\44\17\176\uffff\1\16\1\20",
            "\7\17\2\uffff\1\17\2\uffff\1\17\3\uffff\1\17\1\uffff\2\17\5"+
            "\uffff\3\17\62\uffff\44\17\176\uffff\1\16\1\20",
            "",
            ""
    };

    static final short[] DFA229_eot = DFA.unpackEncodedString(DFA229_eotS);
    static final short[] DFA229_eof = DFA.unpackEncodedString(DFA229_eofS);
    static final char[] DFA229_min = DFA.unpackEncodedStringToUnsignedChars(DFA229_minS);
    static final char[] DFA229_max = DFA.unpackEncodedStringToUnsignedChars(DFA229_maxS);
    static final short[] DFA229_accept = DFA.unpackEncodedString(DFA229_acceptS);
    static final short[] DFA229_special = DFA.unpackEncodedString(DFA229_specialS);
    static final short[][] DFA229_transition;

    static {
        int numStates = DFA229_transitionS.length;
        DFA229_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA229_transition[i] = DFA.unpackEncodedString(DFA229_transitionS[i]);
        }
    }

    class DFA229 extends DFA {

        public DFA229(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 229;
            this.eot = DFA229_eot;
            this.eof = DFA229_eof;
            this.min = DFA229_min;
            this.max = DFA229_max;
            this.accept = DFA229_accept;
            this.special = DFA229_special;
            this.transition = DFA229_transition;
        }
        public String getDescription() {
            return "721:1: modifybody : ( COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* b= STRINGLITERAL ( WS )* STMTSEP -> ^( $w $b) | COLON ( WS )* w= evalobj ( WS )* ASSIGN ( WS )* w2= evalobj ( WS )* STMTSEP -> ^( $w $w2) | COLON ( WS )* wm= modifier_method ( WS )* STMTSEP -> ^( MODIFIER_METHOD $wm) );";
        }
    }
    static final String DFA242_eotS =
        "\7\uffff";
    static final String DFA242_eofS =
        "\7\uffff";
    static final String DFA242_minS =
        "\1\u00cb\2\u00b8\2\10\2\uffff";
    static final String DFA242_maxS =
        "\1\u00cb\4\u00f8\2\uffff";
    static final String DFA242_acceptS =
        "\5\uffff\1\1\1\2";
    static final String DFA242_specialS =
        "\7\uffff}>";
    static final String[] DFA242_transitionS = {
            "\1\1",
            "\1\3\77\uffff\1\2",
            "\1\3\77\uffff\1\2",
            "\21\5\1\uffff\5\5\2\uffff\3\5\62\uffff\44\5\51\uffff\5\5\10"+
            "\uffff\10\6\41\uffff\1\6\1\5\35\uffff\1\4",
            "\21\5\1\uffff\5\5\2\uffff\3\5\62\uffff\44\5\51\uffff\5\5\10"+
            "\uffff\10\6\41\uffff\1\6\1\5\35\uffff\1\4",
            "",
            ""
    };

    static final short[] DFA242_eot = DFA.unpackEncodedString(DFA242_eotS);
    static final short[] DFA242_eof = DFA.unpackEncodedString(DFA242_eofS);
    static final char[] DFA242_min = DFA.unpackEncodedStringToUnsignedChars(DFA242_minS);
    static final char[] DFA242_max = DFA.unpackEncodedStringToUnsignedChars(DFA242_maxS);
    static final short[] DFA242_accept = DFA.unpackEncodedString(DFA242_acceptS);
    static final short[] DFA242_special = DFA.unpackEncodedString(DFA242_specialS);
    static final short[][] DFA242_transition;

    static {
        int numStates = DFA242_transitionS.length;
        DFA242_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA242_transition[i] = DFA.unpackEncodedString(DFA242_transitionS[i]);
        }
    }

    class DFA242 extends DFA {

        public DFA242(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 242;
            this.eot = DFA242_eot;
            this.eof = DFA242_eof;
            this.min = DFA242_min;
            this.max = DFA242_max;
            this.accept = DFA242_accept;
            this.special = DFA242_special;
            this.transition = DFA242_transition;
        }
        public String getDescription() {
            return "742:1: rulebody : ( ruleeval ( WS )* rulealw ( WS )* ruledny | COLON ( WS )* L_PAREN ( WS )* action ( WS )* R_PAREN ( WS )* STMTSEP );";
        }
    }
    static final String DFA253_eotS =
        "\25\uffff";
    static final String DFA253_eofS =
        "\25\uffff";
    static final String DFA253_minS =
        "\1\u00bb\1\u00c6\1\u00cb\1\u00c4\1\u00bb\2\u00ba\1\u00c6\1\u00cb"+
        "\1\u00c4\1\u00ad\2\u00ba\1\u00bb\1\u00cb\2\uffff\1\u00ad\1\u00bb"+
        "\2\uffff";
    static final String DFA253_maxS =
        "\1\u00fb\1\u00c6\1\u00cb\1\u00c5\1\u00bb\2\u00ba\1\u00c6\1\u00cb"+
        "\1\u00c5\1\u00ad\2\u00ba\1\u00f8\1\u00cb\2\uffff\1\u00ad\1\u00f8"+
        "\2\uffff";
    static final String DFA253_acceptS =
        "\17\uffff\1\1\1\3\2\uffff\1\2\1\4";
    static final String DFA253_specialS =
        "\25\uffff}>";
    static final String[] DFA253_transitionS = {
            "\1\1\77\uffff\1\2",
            "\1\3",
            "\1\4",
            "\1\5\1\6",
            "\1\7",
            "\1\10",
            "\1\10",
            "\1\11",
            "\1\12",
            "\1\13\1\14",
            "\1\15",
            "\1\16",
            "\1\16",
            "\1\17\1\20\73\uffff\1\20",
            "\1\21",
            "",
            "",
            "\1\22",
            "\1\23\1\24\73\uffff\1\24",
            "",
            ""
    };

    static final short[] DFA253_eot = DFA.unpackEncodedString(DFA253_eotS);
    static final short[] DFA253_eof = DFA.unpackEncodedString(DFA253_eofS);
    static final char[] DFA253_min = DFA.unpackEncodedStringToUnsignedChars(DFA253_minS);
    static final char[] DFA253_max = DFA.unpackEncodedStringToUnsignedChars(DFA253_maxS);
    static final short[] DFA253_accept = DFA.unpackEncodedString(DFA253_acceptS);
    static final short[] DFA253_special = DFA.unpackEncodedString(DFA253_specialS);
    static final short[][] DFA253_transition;

    static {
        int numStates = DFA253_transitionS.length;
        DFA253_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA253_transition[i] = DFA.unpackEncodedString(DFA253_transitionS[i]);
        }
    }

    class DFA253 extends DFA {

        public DFA253(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 253;
            this.eot = DFA253_eot;
            this.eof = DFA253_eof;
            this.min = DFA253_min;
            this.max = DFA253_max;
            this.accept = DFA253_accept;
            this.special = DFA253_special;
            this.transition = DFA253_transition;
        }
        public String getDescription() {
            return "747:1: rule : ( L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE L_SQ_B (d= DIGITS )* R_SQ_B ( WS )* curlyblock -> ^( RULE_DEF ^( RULE_EVAL_POINTS $d) ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) ) | L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ) | lbl= WORD COLON L_SQ_B ANNT (ra= ON | ra= OFF ) R_SQ_B COLON RULE ( WS )* curlyblock -> ^( DUMMY_RULE_DEF ^( RULE_STATUS $ra) curlyblock ^( RULE_LABEL $lbl) ) );";
        }
    }
    static final String DFA264_eotS =
        "\26\uffff";
    static final String DFA264_eofS =
        "\26\uffff";
    static final String DFA264_minS =
        "\1\u00b0\2\uffff\6\u00b9\15\uffff";
    static final String DFA264_maxS =
        "\1\u00d9\2\uffff\6\u00f8\15\uffff";
    static final String DFA264_acceptS =
        "\1\uffff\1\1\1\2\6\uffff\1\17\1\3\1\4\1\5\1\6\1\10\1\7\1\11\1\12"+
        "\1\14\1\13\1\16\1\15";
    static final String DFA264_specialS =
        "\26\uffff}>";
    static final String[] DFA264_transitionS = {
            "\1\1\1\2\1\3\1\4\1\5\1\6\1\7\1\10\41\uffff\1\11",
            "",
            "",
            "\1\12\21\uffff\1\13\1\12\53\uffff\1\12",
            "\1\14\21\uffff\1\15\1\14\53\uffff\1\14",
            "\1\17\21\uffff\1\16\1\17\53\uffff\1\17",
            "\1\20\21\uffff\1\21\1\20\53\uffff\1\20",
            "\1\23\21\uffff\1\22\1\23\53\uffff\1\23",
            "\1\25\21\uffff\1\24\1\25\53\uffff\1\25",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            "",
            ""
    };

    static final short[] DFA264_eot = DFA.unpackEncodedString(DFA264_eotS);
    static final short[] DFA264_eof = DFA.unpackEncodedString(DFA264_eofS);
    static final char[] DFA264_min = DFA.unpackEncodedStringToUnsignedChars(DFA264_minS);
    static final char[] DFA264_max = DFA.unpackEncodedStringToUnsignedChars(DFA264_maxS);
    static final short[] DFA264_accept = DFA.unpackEncodedString(DFA264_acceptS);
    static final short[] DFA264_special = DFA.unpackEncodedString(DFA264_specialS);
    static final short[][] DFA264_transition;

    static {
        int numStates = DFA264_transitionS.length;
        DFA264_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA264_transition[i] = DFA.unpackEncodedString(DFA264_transitionS[i]);
        }
    }

    class DFA264 extends DFA {

        public DFA264(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 264;
            this.eot = DFA264_eot;
            this.eof = DFA264_eof;
            this.min = DFA264_min;
            this.max = DFA264_max;
            this.accept = DFA264_accept;
            this.special = DFA264_special;
            this.transition = DFA264_transition;
        }
        public String getDescription() {
            return "787:1: action : ( CONT | CONT_Q | ALW | ALW ( COLON WORD )+ | ALW_Q | ALW_Q ( COLON WORD )+ | ALWU | ALWU ( COLON WORD )+ | ALWU_Q | ALWU_Q ( COLON WORD )+ | DNY | DNY ( COLON WORD )+ | DNY_Q | DNY_Q ( COLON WORD )+ | g= GOTO ( WS )* d= WORD -> ^( $g $d) );";
        }
    }
 

    public static final BitSet FOLLOW_filter_in_input3127 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0xC000000000000000L,0x000000000000000FL});
    public static final BitSet FOLLOW_EOF_in_input3131 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_eval_nai0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_eval_np0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_eval_np0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_eval_gti0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMS_TPDU_DCS_in_eval_dcs3222 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_DATA_CODING_in_smpp_eval_dcs3235 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_ESM_MESSAGE_MODE_in_smpp_eval_esm_mm3248 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_ESM_MESSAGE_TYPE_in_smpp_eval_esm_mt3261 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_ESM_GSM_FEATURES_in_smpp_eval_esm_gf3274 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_RD_SMSC_RECEIPT_in_smpp_eval_rd_smscdr3287 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_RD_SME_ACK_in_smpp_eval_rd_smeoa3300 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMPP_RD_INTERMEDIATE_NOTIFICATION_in_smpp_eval_rd_in3313 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_eval_string0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_eval_number0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_eval_ton0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_eval_ton0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SMS_MSG_TYPE_in_eval_msg_type3640 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3653 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3655 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_evalsimple3660 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3662 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3667 = new BitSet(new long[]{0x00000000000000F0L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3669 = new BitSet(new long[]{0x00000000000000F0L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_SCCP_GT_CALLED_WL_in_evalsimple3675 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_SCCP_GT_CALLING_WL_in_evalsimple3679 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_MAP_SCOA_WL_in_evalsimple3683 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_MAP_SCDA_WL_in_evalsimple3687 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3690 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3693 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3714 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3716 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_evalsimple3721 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3723 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3728 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3730 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_evalsimple3735 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3737 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3740 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3761 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3763 = new BitSet(new long[]{0x0000000000000000L,0x0100000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_dcs_in_evalsimple3768 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3770 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3775 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000700000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3777 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000700000L,0x0100000000000000L});
    public static final BitSet FOLLOW_dcs_in_evalsimple3782 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3784 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3787 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3808 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3810 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_evalsimple3815 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3817 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3822 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3824 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_evalsimple3829 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000002000L});
    public static final BitSet FOLLOW_PERCENT_in_evalsimple3831 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000002000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3834 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3837 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3858 = new BitSet(new long[]{0x0000000000000000L,0x0000028000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3860 = new BitSet(new long[]{0x0000000000000000L,0x0000028000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_ton_in_evalsimple3865 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3867 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3872 = new BitSet(new long[]{0x0000000000000000L,0xFC00000000000000L,0x0000000000000001L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3874 = new BitSet(new long[]{0x0000000000000000L,0xFC00000000000000L,0x0000000000000001L,0x0100000000000000L});
    public static final BitSet FOLLOW_typeofnum_in_evalsimple3879 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3881 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3884 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3905 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3907 = new BitSet(new long[]{0x0000000000000000L,0x0200000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_msg_type_in_evalsimple3912 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3914 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3919 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000001800000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3921 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000001800000L,0x0100000000000000L});
    public static final BitSet FOLLOW_msgtype_in_evalsimple3926 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3928 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3931 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple3953 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3955 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_evalsimple3960 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple3962 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple3967 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3969 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_evalsimple3974 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple3976 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple3979 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4001 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4003 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_evalsimple4008 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4010 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4015 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4017 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_evalsimple4022 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4024 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4027 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4052 = new BitSet(new long[]{0x0000000000000000L,0x000000000C000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4054 = new BitSet(new long[]{0x0000000000000000L,0x000000000C000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_nai_in_evalsimple4059 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4061 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4066 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000007C00L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4068 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000007C00L,0x0100000000000000L});
    public static final BitSet FOLLOW_nai_in_evalsimple4073 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4075 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4078 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4102 = new BitSet(new long[]{0x0000000000000000L,0x0000000030000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4104 = new BitSet(new long[]{0x0000000000000000L,0x0000000030000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_np_in_evalsimple4109 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4111 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4116 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000000000003FEL,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4118 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000000000003FEL,0x0100000000000000L});
    public static final BitSet FOLLOW_np_in_evalsimple4123 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4125 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4128 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4152 = new BitSet(new long[]{0x0000000000000000L,0x00000000C0000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4154 = new BitSet(new long[]{0x0000000000000000L,0x00000000C0000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_gti_in_evalsimple4159 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4161 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4166 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000000000F8000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4168 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000000000F8000L,0x0100000000000000L});
    public static final BitSet FOLLOW_gti_in_evalsimple4173 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4175 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4178 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4202 = new BitSet(new long[]{0x0000000000090000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4204 = new BitSet(new long[]{0x0000000000090000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_np_in_evalsimple4209 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4211 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4216 = new BitSet(new long[]{0xFFC0000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4218 = new BitSet(new long[]{0xFFC0000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_np_in_evalsimple4223 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4225 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4228 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4252 = new BitSet(new long[]{0x0000000000048000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4254 = new BitSet(new long[]{0x0000000000048000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_ton_in_evalsimple4259 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4261 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4266 = new BitSet(new long[]{0x003F800000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4268 = new BitSet(new long[]{0x003F800000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_typeofnum_in_evalsimple4273 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4275 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4278 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4302 = new BitSet(new long[]{0x0000000000200000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4304 = new BitSet(new long[]{0x0000000000200000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_esm_mm_in_evalsimple4309 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4311 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4316 = new BitSet(new long[]{0x000000F000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4318 = new BitSet(new long[]{0x000000F000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_esm_mm_in_evalsimple4323 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4325 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4328 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4352 = new BitSet(new long[]{0x0000000000400000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4354 = new BitSet(new long[]{0x0000000000400000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_esm_mt_in_evalsimple4359 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4361 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4366 = new BitSet(new long[]{0x0000070000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4368 = new BitSet(new long[]{0x0000070000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_esm_mt_in_evalsimple4373 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4375 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4378 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4402 = new BitSet(new long[]{0x0000000000800000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4404 = new BitSet(new long[]{0x0000000000800000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_esm_gf_in_evalsimple4409 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4411 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4416 = new BitSet(new long[]{0x0000780000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4418 = new BitSet(new long[]{0x0000780000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_esm_gf_in_evalsimple4423 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4425 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4428 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4452 = new BitSet(new long[]{0x0000000010000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4454 = new BitSet(new long[]{0x0000000010000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_rd_smscdr_in_evalsimple4459 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4461 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4466 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000007L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4468 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000007L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_rd_smscdr_in_evalsimple4473 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4475 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4478 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4502 = new BitSet(new long[]{0x0000000020000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4504 = new BitSet(new long[]{0x0000000020000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_rd_smeoa_in_evalsimple4509 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4511 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4516 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000078L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4518 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000078L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_rd_smeoa_in_evalsimple4523 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4525 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4528 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4552 = new BitSet(new long[]{0x0000000040000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4554 = new BitSet(new long[]{0x0000000040000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_eval_rd_in_in_evalsimple4559 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4561 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4566 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000180L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4568 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000180L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_smpp_rd_in_in_evalsimple4573 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4575 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4578 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4606 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x000000F800000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4608 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x000000F800000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_floodmethod_in_evalsimple4613 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4615 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4620 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x000000F800000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4622 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x000000F800000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_floodmethod_in_evalsimple4627 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4629 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4632 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4653 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x000000F800000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4655 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x000000F800000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_floodmethod_in_evalsimple4660 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4662 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4667 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4669 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_evalsimple4674 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4676 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4679 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_evalsimple4703 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4705 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_evalsimple4710 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_evalsimple4712 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_evalsimple4717 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000100000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4719 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000100000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_listmethod_in_evalsimple4724 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_evalsimple4726 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_evalsimple4729 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_LIST_in_listmethod4759 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_listmethod4761 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_listmethod4764 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_listmethod4766 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_listmethod4771 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_listmethod4773 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_listmethod4776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOOD_in_floodmethod4802 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4804 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_floodmethod4807 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4809 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_floodmethod4814 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4816 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_COMMA_in_floodmethod4819 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4821 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_MINUTE_in_floodmethod4827 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_HOUR_in_floodmethod4833 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_DAY_in_floodmethod4839 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4842 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_floodmethod4845 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOOD_GLOBAL_in_floodmethod4866 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4868 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_floodmethod4871 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4873 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_MINUTE_in_floodmethod4879 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_HOUR_in_floodmethod4885 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_DAY_in_floodmethod4891 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4894 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_floodmethod4897 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOOD_MAX_in_floodmethod4915 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4918 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_floodmethod4921 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4923 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_floodmethod4928 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4930 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_COMMA_in_floodmethod4933 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4935 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_MINUTE_in_floodmethod4941 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_HOUR_in_floodmethod4947 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_DAY_in_floodmethod4953 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4956 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_floodmethod4959 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOOD_GLOBAL_MAX_in_floodmethod4980 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4982 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_floodmethod4985 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod4987 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_MINUTE_in_floodmethod4993 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_HOUR_in_floodmethod4999 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_DAY_in_floodmethod5005 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod5008 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_floodmethod5011 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_FLOOD_ALL_MAX_in_floodmethod5029 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod5031 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_floodmethod5034 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod5036 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000070000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_MINUTE_in_floodmethod5042 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_HOUR_in_floodmethod5048 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_FLOOD_DAY_in_floodmethod5054 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_floodmethod5057 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_floodmethod5060 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_regexdef5092 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_DIGITS_in_regexdef5095 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_257_in_regexdef5098 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_258_in_regexdef5101 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_L_SQ_B_in_regexdef5104 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_R_SQ_B_in_regexdef5107 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_L_PAREN_in_regexdef5110 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_R_PAREN_in_regexdef5113 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_259_in_regexdef5116 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_ASTERISK_in_regexdef5119 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_260_in_regexdef5122 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_PLUS_in_regexdef5125 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_261_in_regexdef5128 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0F00000000000000L,0x0C00000008080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_L_PAREN_in_regex5150 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5152 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5155 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5159 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5161 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5165 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5167 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5172 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5174 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_regex5179 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5181 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5184 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5215 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5217 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5220 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5224 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5226 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5230 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5232 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5237 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5239 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5244 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5246 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5249 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5280 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5282 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5285 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5289 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5291 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5295 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5297 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5302 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5304 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5307 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5311 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5313 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5317 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5319 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5322 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5361 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5363 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5366 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5370 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5372 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5376 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5378 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5383 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5385 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_regex5390 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5392 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5395 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5426 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5428 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5431 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5435 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5437 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5441 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5443 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5448 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5450 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5455 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5457 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5460 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5491 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5493 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5496 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5500 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5502 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5506 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5508 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5513 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5515 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5518 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5522 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5524 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5528 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5530 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5533 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5572 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5574 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5577 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5581 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5583 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5587 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5589 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5594 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5596 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5599 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5603 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5605 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5609 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5611 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5614 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5653 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5655 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5658 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5662 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5664 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5668 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5670 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5675 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5677 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5680 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5684 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5686 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5690 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5692 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5695 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5734 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5736 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5739 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5743 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5745 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5749 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5751 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5756 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5758 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_regex5763 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5765 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5768 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5799 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5801 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5804 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5808 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5810 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5814 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5816 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5821 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5823 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_regex5828 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5830 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5833 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_regex5864 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_WS_in_regex5866 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5869 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0F00000000000000L,0x0C0000000C080000L,0x000000000000003EL});
    public static final BitSet FOLLOW_regexdef_in_regex5873 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000004000000L});
    public static final BitSet FOLLOW_REGEX_BLOCK_in_regex5875 = new BitSet(new long[]{0x000000060D000C00L,0x00F0083003000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_number_in_regex5879 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_WS_in_regex5881 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000C00000000000L,0x0100000000078480L});
    public static final BitSet FOLLOW_comparison_in_regex5886 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5888 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_regex5893 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_regex5895 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_regex5898 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_esm_mm0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_esm_mt0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_esm_gf0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_rd_smscdr0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_rd_smeoa0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_rd_in0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_nai0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_np0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_np0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_gti0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_dcs0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_dcs0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_smpp_typeofnum0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_typeofnum0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_set_in_msgtype0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_dcs_in_evalobj6530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_string_in_evalobj6536 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_number_in_evalobj6542 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_ton_in_evalobj6548 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_msg_type_in_evalobj6554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_nai_in_evalobj6560 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_np_in_evalobj6566 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_eval_gti_in_evalobj6572 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_evalsimple_in_eval6585 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_regex_in_eval6591 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_CR_B_in_curlyblock6604 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_curlyblock_in_curlyblock6608 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_WS_in_curlyblock6612 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_rule_in_curlyblock6616 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_rulebody_in_curlyblock6620 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_modify_in_curlyblock6624 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_modifybody_in_curlyblock6628 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_m3ua_in_curlyblock6632 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x3800000000000000L,0x0900000000000800L});
    public static final BitSet FOLLOW_R_CR_B_in_curlyblock6636 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_m3ua6651 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6653 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_m3ua6656 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0900000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6658 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0900000000000000L});
    public static final BitSet FOLLOW_WORD_in_m3ua6663 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000008000800L});
    public static final BitSet FOLLOW_ASTERISK_in_m3ua6667 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000008000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6670 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6673 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x1100000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6675 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x1100000000000000L});
    public static final BitSet FOLLOW_IP_in_m3ua6680 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6682 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6685 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6687 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6692 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6694 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6697 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x1100000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6699 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x1100000000000000L});
    public static final BitSet FOLLOW_IP_in_m3ua6704 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6706 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6709 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6711 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6716 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6718 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6721 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6723 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6728 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6730 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6733 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6735 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6740 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6742 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6745 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6747 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6752 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6754 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6757 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6759 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6764 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_WS_in_m3ua6766 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000800L});
    public static final BitSet FOLLOW_COLON_in_m3ua6769 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6771 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0500000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_m3ua6776 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_m3ua6778 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_m3ua6781 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_m3ua6783 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_m3ua6786 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPAM_UPDATE_LST_in_modifier_method6904 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_SPAM_REMOVE_LST_in_modifier_method6910 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUARANTINE_UPDATE_LST_in_modifier_method6916 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_QUARANTINE_REMOVE_LST_in_modifier_method6922 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MD5_UPDATE_LST_in_modifier_method6928 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MD5_REMOVE_LST_in_modifier_method6934 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_NO_DR_in_modifier_method6941 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONVERT_SS7_in_modifier_method6947 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONVERT_SMPP_in_modifier_method6953 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HLR_REQUEST_in_modifier_method6964 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method6966 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_modifier_method6969 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method6971 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_modifier_method6976 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method6978 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_COMMA_in_modifier_method6981 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method6983 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_modifier_method6988 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method6990 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_modifier_method6993 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HLR_REQUEST_in_modifier_method7022 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7024 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_modifier_method7027 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7029 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_modifier_method7034 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7036 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_COMMA_in_modifier_method7039 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7041 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_modifier_method7046 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7048 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_modifier_method7051 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HLR_REQUEST_in_modifier_method7080 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7082 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_modifier_method7085 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7087 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_modifier_method7092 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7094 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_COMMA_in_modifier_method7097 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7099 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_modifier_method7104 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7106 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_modifier_method7109 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_HLR_REQUEST_in_modifier_method7138 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7140 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_modifier_method7143 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7145 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_modifier_method7150 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7152 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000010000000L});
    public static final BitSet FOLLOW_COMMA_in_modifier_method7155 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7157 = new BitSet(new long[]{0x0000000800127300L,0x000FF54F00C00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_string_in_modifier_method7162 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifier_method7164 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_modifier_method7167 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_SQ_B_in_modify7201 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_ANNT_in_modify7203 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000030L});
    public static final BitSet FOLLOW_ON_in_modify7208 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_OFF_in_modify7214 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_modify7217 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_modify7219 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_MODIFY_in_modify7221 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modify7223 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_modify7226 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_modify7249 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_modify7251 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_L_SQ_B_in_modify7253 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_ANNT_in_modify7255 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000030L});
    public static final BitSet FOLLOW_ON_in_modify7260 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_OFF_in_modify7266 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_modify7269 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_modify7271 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000400000L});
    public static final BitSet FOLLOW_MODIFY_in_modify7273 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modify7275 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_modify7278 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_modifybody7313 = new BitSet(new long[]{0x0000000E0D127F00L,0x03FFFFFFFFC00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifybody7315 = new BitSet(new long[]{0x0000000E0D127F00L,0x03FFFFFFFFC00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_evalobj_in_modifybody7320 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000100L});
    public static final BitSet FOLLOW_WS_in_modifybody7322 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000100L});
    public static final BitSet FOLLOW_ASSIGN_in_modifybody7325 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_WS_in_modifybody7327 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0300000000000000L});
    public static final BitSet FOLLOW_STRINGLITERAL_in_modifybody7332 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_modifybody7334 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_modifybody7337 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_modifybody7353 = new BitSet(new long[]{0x0000000E0D127F00L,0x03FFFFFFFFC00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifybody7355 = new BitSet(new long[]{0x0000000E0D127F00L,0x03FFFFFFFFC00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_evalobj_in_modifybody7360 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000100L});
    public static final BitSet FOLLOW_WS_in_modifybody7362 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000000100L});
    public static final BitSet FOLLOW_ASSIGN_in_modifybody7365 = new BitSet(new long[]{0x0000000E0D127F00L,0x03FFFFFFFFC00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifybody7367 = new BitSet(new long[]{0x0000000E0D127F00L,0x03FFFFFFFFC00000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_evalobj_in_modifybody7372 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_modifybody7374 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_modifybody7377 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_modifybody7393 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000007FE000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_modifybody7395 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00000007FE000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_modifier_method_in_modifybody7400 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_modifybody7402 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_modifybody7405 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_ruleeval7427 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_ruleeval7430 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_eval_in_ruleeval7433 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_ruleeval7435 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_ruleeval7438 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_PLUS_in_rulealw7453 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00FF000000000000L,0x0100000002000000L});
    public static final BitSet FOLLOW_WS_in_rulealw7455 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00FF000000000000L,0x0100000002000000L});
    public static final BitSet FOLLOW_action_in_rulealw7458 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_rulealw7460 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_rulealw7463 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_MINUS_in_ruledny7486 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00FF000000000000L,0x0100000002000000L});
    public static final BitSet FOLLOW_WS_in_ruledny7488 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00FF000000000000L,0x0100000002000000L});
    public static final BitSet FOLLOW_action_in_ruledny7491 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_ruledny7493 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_ruledny7496 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ruleeval_in_rulebody7520 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000080000L});
    public static final BitSet FOLLOW_WS_in_rulebody7522 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000080000L});
    public static final BitSet FOLLOW_rulealw_in_rulebody7525 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000100000L});
    public static final BitSet FOLLOW_WS_in_rulebody7527 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000100000L});
    public static final BitSet FOLLOW_ruledny_in_rulebody7530 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_COLON_in_rulebody7536 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_rulebody7539 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0100000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_L_PAREN_in_rulebody7542 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00FF000000000000L,0x0100000002000000L});
    public static final BitSet FOLLOW_WS_in_rulebody7545 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x00FF000000000000L,0x0100000002000000L});
    public static final BitSet FOLLOW_action_in_rulebody7548 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_rulebody7550 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0200000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_R_PAREN_in_rulebody7553 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_WS_in_rulebody7556 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0100000000001000L});
    public static final BitSet FOLLOW_STMTSEP_in_rulebody7559 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_SQ_B_in_rule7576 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_ANNT_in_rule7578 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000030L});
    public static final BitSet FOLLOW_ON_in_rule7583 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_OFF_in_rule7589 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_rule7592 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_rule7594 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_RULE_in_rule7596 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_L_SQ_B_in_rule7598 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_rule7602 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_rule7605 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_rule7607 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_rule7610 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_rule7639 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_rule7641 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_L_SQ_B_in_rule7643 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_ANNT_in_rule7645 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000030L});
    public static final BitSet FOLLOW_ON_in_rule7650 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_OFF_in_rule7656 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_rule7659 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_rule7661 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_RULE_in_rule7663 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_L_SQ_B_in_rule7665 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_DIGITS_in_rule7669 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_rule7672 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_rule7674 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_rule7677 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_SQ_B_in_rule7714 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_ANNT_in_rule7716 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000030L});
    public static final BitSet FOLLOW_ON_in_rule7721 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_OFF_in_rule7727 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_rule7730 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_rule7732 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_RULE_in_rule7734 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_rule7736 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_rule7739 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_rule7761 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_rule7763 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_L_SQ_B_in_rule7765 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000040L});
    public static final BitSet FOLLOW_ANNT_in_rule7767 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000030L});
    public static final BitSet FOLLOW_ON_in_rule7772 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_OFF_in_rule7778 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0400000000000000L});
    public static final BitSet FOLLOW_R_SQ_B_in_rule7781 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_rule7783 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000200000000000L});
    public static final BitSet FOLLOW_RULE_in_rule7785 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_rule7787 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_rule7790 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_F_MO_in_filter7827 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_F_MT_in_filter7833 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_F_SMPP_MO_in_filter7839 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_F_SMPP_MT_in_filter7845 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_F_HLR_in_filter7851 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_F_M3UA_in_filter7857 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_filter7860 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x1000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_curlyblock_in_filter7863 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_WS_in_filter7865 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0100000000000000L});
    public static final BitSet FOLLOW_set_in_comparison0 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONT_in_action8042 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_CONT_Q_in_action8048 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALW_in_action8054 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALW_in_action8060 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_action8063 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8066 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_ALW_Q_in_action8074 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALW_Q_in_action8080 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_action8083 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8086 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_ALWU_in_action8094 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALWU_in_action8100 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_action8103 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8106 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_ALWU_Q_in_action8114 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_ALWU_Q_in_action8120 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_action8123 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8126 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_DNY_in_action8134 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DNY_in_action8140 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_action8143 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8146 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_DNY_Q_in_action8154 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_DNY_Q_in_action8160 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_COLON_in_action8163 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0800000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8166 = new BitSet(new long[]{0x0000000000000002L,0x0000000000000000L,0x0000000000000000L,0x0000000000000800L});
    public static final BitSet FOLLOW_GOTO_in_action8176 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0900000000000000L});
    public static final BitSet FOLLOW_WS_in_action8178 = new BitSet(new long[]{0x0000000000000000L,0x0000000000000000L,0x0000000000000000L,0x0900000000000000L});
    public static final BitSet FOLLOW_WORD_in_action8183 = new BitSet(new long[]{0x0000000000000002L});

}