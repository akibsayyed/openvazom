package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAConnectionReferenceList extends SEQUENCE{

	public CDMAConnectionReferenceListInner new_child(){
		return new CDMAConnectionReferenceListInner();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMAConnectionReferenceListInner getChild(int index){
		return (CDMAConnectionReferenceListInner)of_children.get(index);
	}
	public CDMAConnectionReferenceList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
