package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuditReturnParameter extends CHOICE{

	public ErrorDescriptor get_errorDescriptor(){
		return (ErrorDescriptor)elements.get(0).data;
	}
	public ErrorDescriptor new_errorDescriptor(){
		return new ErrorDescriptor();
	}

	public MediaDescriptor get_mediaDescriptor(){
		return (MediaDescriptor)elements.get(1).data;
	}
	public MediaDescriptor new_mediaDescriptor(){
		return new MediaDescriptor();
	}

	public ModemDescriptor get_modemDescriptor(){
		return (ModemDescriptor)elements.get(2).data;
	}
	public ModemDescriptor new_modemDescriptor(){
		return new ModemDescriptor();
	}

	public MuxDescriptor get_muxDescriptor(){
		return (MuxDescriptor)elements.get(3).data;
	}
	public MuxDescriptor new_muxDescriptor(){
		return new MuxDescriptor();
	}

	public EventsDescriptor get_eventsDescriptor(){
		return (EventsDescriptor)elements.get(4).data;
	}
	public EventsDescriptor new_eventsDescriptor(){
		return new EventsDescriptor();
	}

	public EventBufferDescriptor get_eventBufferDescriptor(){
		return (EventBufferDescriptor)elements.get(5).data;
	}
	public EventBufferDescriptor new_eventBufferDescriptor(){
		return new EventBufferDescriptor();
	}

	public SignalsDescriptor get_signalsDescriptor(){
		return (SignalsDescriptor)elements.get(6).data;
	}
	public SignalsDescriptor new_signalsDescriptor(){
		return new SignalsDescriptor();
	}

	public DigitMapDescriptor get_digitMapDescriptor(){
		return (DigitMapDescriptor)elements.get(7).data;
	}
	public DigitMapDescriptor new_digitMapDescriptor(){
		return new DigitMapDescriptor();
	}

	public ObservedEventsDescriptor get_observedEventsDescriptor(){
		return (ObservedEventsDescriptor)elements.get(8).data;
	}
	public ObservedEventsDescriptor new_observedEventsDescriptor(){
		return new ObservedEventsDescriptor();
	}

	public StatisticsDescriptor get_statisticsDescriptor(){
		return (StatisticsDescriptor)elements.get(9).data;
	}
	public StatisticsDescriptor new_statisticsDescriptor(){
		return new StatisticsDescriptor();
	}

	public PackagesDescriptor get_packagesDescriptor(){
		return (PackagesDescriptor)elements.get(10).data;
	}
	public PackagesDescriptor new_packagesDescriptor(){
		return new PackagesDescriptor();
	}

	public AuditDescriptor get_emptyDescriptors(){
		return (AuditDescriptor)elements.get(11).data;
	}
	public AuditDescriptor new_emptyDescriptors(){
		return new AuditDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_errorDescriptor(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mediaDescriptor(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_modemDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_muxDescriptor(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventsDescriptor(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventBufferDescriptor(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalsDescriptor(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digitMapDescriptor(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_observedEventsDescriptor(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_statisticsDescriptor(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_packagesDescriptor(); }});
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_emptyDescriptors(); }});
	}
	public AuditReturnParameter(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
