package net.ber;

import java.lang.reflect.Field;
import java.util.ArrayList;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagClass;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;
import net.asn1.compiler.ElementDescriptor;

public class BerTag {
	public ASNTagComplexity asn_pc;
	public ASNTagClass asn_class;
	public int tag;
	public int tag_length = 1;
	public Field fld;
	public boolean present = false;
	public boolean optional = false;
	
	// structure, important for encoder
	public ASNType node;
	public ArrayList<BerTag> children;
	public BerTag parent;
	public boolean hasData = true;
	public BerLengthType lengthType = BerLengthType.DEFINITE_SHORT;
	public int lengthSize = 1;
	
	public void setBerTag(BerTag ntag){
		if(parent != null){
			for(int i = 0; i<parent.children.size(); i++) if(parent.children.get(i) == this){
				parent.children.set(i, ntag);
				this.node.berTag = ntag;
				ntag.parent = parent;
				break;
			}
			
		}
		
	}
	public void setBerTag(BerTag ntag, ElementDescriptor ed){
		if(parent != null){
			for(int i = 0; i<parent.children.size(); i++) if(parent.children.get(i) == this){
				parent.children.set(i, ntag);
				this.node.berTag = ntag;
				ntag.parent = parent;
				if(ed != null){
					ntag.node.tag = ed.tagValue;
					ntag.node.asn_class = ed.tagClass;
					
				}
				break;
			}
			
		}
		
	}
	public void delete(){
		for(int i = 0; i<parent.children.size(); i++) if(parent.children.get(i) == this){
			parent.children.remove(i);
			this.node.berTag = null;
			break;
		}
		
	}

	public static void BerTagReplace(BerTag src, BerTag dst){
		if(src.parent != null){
			for(int i = 0; i<src.parent.children.size(); i++) if(src.parent.children.get(i) == src){
				src.parent.children.set(i, dst);
				src.node.berTag = dst;
				dst.parent = src.parent;
				break;
			}
		}
	}

	public static BerTag createNew(ASNType new_elem, BerTag parent, ElementDescriptor ed){
		BerTag btag = new BerTag();
		btag.children = new ArrayList<BerTag>();
		if(parent != null){
			parent.children.add(btag);
			btag.parent = parent;
		}
		btag.node = new_elem;
		if(ASNTag.fromId((byte)new_elem.tag) == ASNTag.CHOICE) btag.hasData = false;
		if(ed != null){
			new_elem.tag = ed.tagValue;
			new_elem.asn_class = ed.tagClass;
		}
		return btag;
	} 
	
	public static BerTag createNew(ASNType new_elem, BerTag parent, int index, ElementDescriptor ed){
		BerTag btag = new BerTag();
		btag.children = new ArrayList<BerTag>();
		if(parent != null){
			parent.children.add(index, btag);
			btag.parent = parent;
		}
		btag.node = new_elem;
		if(ASNTag.fromId((byte)new_elem.tag) == ASNTag.CHOICE) btag.hasData = false;
		if(ed != null){
			new_elem.tag = ed.tagValue;
			new_elem.asn_class = ed.tagClass;
		}
		return btag;
	} 
	
}
