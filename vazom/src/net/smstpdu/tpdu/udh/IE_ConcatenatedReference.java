package net.smstpdu.tpdu.udh;

public abstract class IE_ConcatenatedReference extends InformationElement {
	public int messageIdentifier;
	public int parts;
	public int partNumber;

	public IE_ConcatenatedReference(){
		super();
	}


}
