package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ConferenceCallingIndicator extends OCTET_STRING{

	public ConferenceCallingIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
