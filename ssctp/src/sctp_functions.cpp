#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <unistd.h>
#include <sys/socket.h>
#include <sys/types.h>
#include <netinet/in.h>
#include <netinet/sctp.h>
#include <errno.h>


/*
 * If everything successful return connSock koji koristis kao
 * connection ID za slanje i primanje poruka
 */
int get_client(int serverSock){
	  return accept(serverSock, NULL, NULL);

}
int shutdown_sctp_server(int socket){
	//int res = shutdown(socket, SHUT_RDWR);
	int res = close(socket);
	//printf("shutdown server: %d\n", res);
	/*
	if(res != 0){
		printf("shutdown_sctp_server error: %s\n", strerror( errno ));
	    fflush(stdout);

	}
	*/
}

int init_sctp_server(unsigned long addr, int local_port){
	  int ret, serverSock, clientSock;
	  struct sockaddr_in servaddr;
	  struct sctp_event_subscribe events;

	  /* Create an SCTP TCP-Style Socket */
	  serverSock = socket( AF_INET, SOCK_STREAM, IPPROTO_SCTP );

	  /* Specify the peer endpoint to which we'll connect */
	  bzero( (void *)&servaddr, sizeof(servaddr) );
	  servaddr.sin_family = AF_INET;
	  servaddr.sin_port = htons(local_port);
	  if(addr == 0) servaddr.sin_addr.s_addr = htonl(INADDR_ANY);
	  else servaddr.sin_addr.s_addr = addr;

	  /* Bind to the wildcard address (all) and MY_PORT_NUM */
	  ret = bind( serverSock, (struct sockaddr *)&servaddr, sizeof(servaddr) );
	  if(ret == 0){
		  /* Enable receipt of SCTP Snd/Rcv Data via sctp_recvmsg */
		  memset( (void *)&events, 0, sizeof(events) );
		  events.sctp_data_io_event = 1;
		  ret = setsockopt( serverSock, SOL_SCTP, SCTP_EVENTS,
		                     (const void *)&events, sizeof(events) );


		  listen(serverSock, 5);
		  return serverSock;
	  }
	  return -1;
}

int init_sctp_client_bind(unsigned long addr, unsigned long local_addr, int local_port, int remote_port, int stream_count)
{
  int in, i, ret, flags, connSock;
  struct sockaddr_in servaddr;
  struct sctp_status status;
  struct sctp_event_subscribe events;
  struct sctp_initmsg initmsg;
  sockaddr_in local_bind;

  /* Create an SCTP TCP-Style Socket */
  connSock = socket( AF_INET, SOCK_STREAM, IPPROTO_SCTP );
  if(connSock < 0){
    //printf("init_sctp_client_bind: SOCKET ERROR - %s\n", strerror( errno ));
    //fflush(stdout);
    return -1;
  }
  // local bind
  bzero( (void *)&local_bind, sizeof(sockaddr_in) );
  local_bind.sin_family = AF_INET;
  local_bind.sin_addr.s_addr = local_addr;
  local_bind.sin_port = htons(local_port);
  ret = bind(connSock, (sockaddr *)&local_bind, sizeof(sockaddr_in));
  if(ret < 0){
	//printf("init_sctp_client_bind: BIND ERROR - %s\n", strerror( errno ));
    //fflush(stdout);
    shutdown(connSock, SHUT_RDWR);
    close(connSock);
    return -1;
  }

  /* Specify that a maximum of 5 streams will be available per socket */
  memset( &initmsg, 0, sizeof(initmsg) );
  initmsg.sinit_num_ostreams = stream_count;
  initmsg.sinit_max_instreams = stream_count;
  initmsg.sinit_max_attempts = 4;
  ret = setsockopt( connSock, IPPROTO_SCTP, SCTP_INITMSG,
                     &initmsg, sizeof(initmsg) );

  /* Specify the peer endpoint to which we'll connect */
  bzero( (void *)&servaddr, sizeof(servaddr) );
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(remote_port);
  servaddr.sin_addr.s_addr = addr;


  /* Connect to the server */
  ret = connect( connSock, (struct sockaddr *)&servaddr, sizeof(servaddr) );
  if(ret == 0){
	  /* Enable receipt of SCTP Snd/Rcv Data via sctp_recvmsg */
	  memset( (void *)&events, 0, sizeof(events) );
	  events.sctp_data_io_event = 1;
	  ret = setsockopt( connSock, SOL_SCTP, SCTP_EVENTS,
	                     (const void *)&events, sizeof(events) );

	  /* Read and emit the status of the Socket (optional step) */
	  in = sizeof(status);
	  ret = getsockopt( connSock, SOL_SCTP, SCTP_STATUS,
	                     (void *)&status, (socklen_t *)&in );
	  return connSock;

  }else{
    //printf("init_sctp_client_bind - %s\n", "SCTP CONNECT ERROR");
    //fflush(stdout);
    shutdown(connSock, SHUT_RDWR);
	close(connSock);
  }
  return -1;
}

int init_sctp_client(unsigned long addr, int remote_port, int stream_count)
{
  int in, i, ret, flags, connSock;
  struct sockaddr_in servaddr;
  struct sctp_status status;
  struct sctp_event_subscribe events;
  struct sctp_initmsg initmsg;

  /* Create an SCTP TCP-Style Socket */
  connSock = socket( AF_INET, SOCK_STREAM, IPPROTO_SCTP );
  if(connSock < 0){
    //printf("init_sctp_client - SOCKET ERROR - %s\n", strerror( errno ));
    //fflush(stdout);
    return -1;
  }
  /* Specify that a maximum of 5 streams will be available per socket */
  memset( &initmsg, 0, sizeof(initmsg) );

  initmsg.sinit_num_ostreams = stream_count;
  initmsg.sinit_max_instreams = stream_count;
  initmsg.sinit_max_attempts = 4;
  ret = setsockopt( connSock, IPPROTO_SCTP, SCTP_INITMSG,
                     &initmsg, sizeof(initmsg) );

  /* Specify the peer endpoint to which we'll connect */
  bzero( (void *)&servaddr, sizeof(servaddr) );
  servaddr.sin_family = AF_INET;
  servaddr.sin_port = htons(remote_port);
  servaddr.sin_addr.s_addr = addr;


  /* Connect to the server */
  ret = connect( connSock, (struct sockaddr *)&servaddr, sizeof(servaddr) );
  if(ret == 0){
	  /* Enable receipt of SCTP Snd/Rcv Data via sctp_recvmsg */
	  memset( (void *)&events, 0, sizeof(events) );
	  events.sctp_data_io_event = 1;
	  ret = setsockopt( connSock, SOL_SCTP, SCTP_EVENTS,
	                     (const void *)&events, sizeof(events) );

	  /* Read and emit the status of the Socket (optional step) */
	  in = sizeof(status);
	  ret = getsockopt( connSock, SOL_SCTP, SCTP_STATUS,
	                     (void *)&status, (socklen_t *)&in );
	  return connSock;

  }else{
    //printf("init_sctp_client - %s\n", "SCTP CONNECT ERROR");
    //fflush(stdout);
    shutdown(connSock, SHUT_RDWR);
	close(connSock);
  }
  return -1;
}

/*
 * RETURN 0 if sucessfully sent a message
 * RETURN 1 if unsucesfull!
 */
int send_sctp(int connSock, const void *msg, size_t *msg_len, unsigned long *ppid, int stream_id)
{
    if(sctp_sendmsg(connSock, msg, *msg_len, NULL, 0, *ppid, 0, stream_id, 0, 0))
        return (EXIT_SUCCESS);
    else
        return (EXIT_FAILURE);
}


/*
 * RETURN Number of bytes received.
 * Damire pazi samo na to da msg_buffer ce biti ukupne velicine 512, cisto napomena!
 */
int rcv_sctp(int connSock, const void *msg_buffer, int *flags, struct sctp_sndrcvinfo *sndrcvinfo)
{
	int r = sctp_recvmsg(connSock, (void *)msg_buffer, 8192, (struct sockaddr *)NULL, 0, sndrcvinfo, flags);
	//printf("KITA: %d\n", sndrcvinfo->sinfo_ppid);
	/*
	if(r < 0){
		printf("rcv_sctp error: %s\n", strerror( errno ));
	    fflush(stdout);
	}
	*/
    return r;

}


/*
 * SCTP Client SHUTDOWN
 */
void shutdown_sctp_client(int *connSock)
{
	int res = shutdown(*connSock, SHUT_RDWR);
	close(*connSock);
	//printf("shutdown: %d\n", res);
}

