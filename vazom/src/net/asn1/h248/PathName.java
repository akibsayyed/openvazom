package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PathName extends IA5STRING{

	public PathName(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 22;
	}
}
