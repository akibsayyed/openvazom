package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ActionsSequenceOf extends SEQUENCE{

	public ActionRequest new_child(){
		return new ActionRequest();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ActionRequest getChild(int index){
		return (ActionRequest)of_children.get(index);
	}
	public ActionsSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
