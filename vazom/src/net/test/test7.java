package net.test;

import net.ds.DataSourceType;
import net.smstpdu.GSMAlphabet;
import net.utils.Utils;

import net.vstp.HeaderDescriptor;
import net.vstp.LocationType;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTP;
import net.vstp.VSTPDataItemType;

public class test7 {
	public static void main(String[] args) {
		String str = "FN:n1:SCTP:SGN:sgn1:HLR:38670000261.60120000519.620801806.0\n" +
					"FILTER.RESULT=1\n" + 
					"END\n";
		
		MessageDescriptor mdd = VSTP.decode(str.getBytes());
		System.out.println(mdd.filter_result);
		System.exit(0);
		
		
		HeaderDescriptor hdr = new HeaderDescriptor();
		hdr.destination = LocationType.FN;
		hdr.ds =DataSourceType.NA;
		hdr.source_id = "SOURCE_1";
		hdr.destination_id = "DESTINATION_1";
		hdr.msg_id = "111";
		hdr.msg_type = MessageType.SMS_MO;
		hdr.source = LocationType.SGN;
		MessageDescriptor md = new MessageDescriptor();
		md.header = hdr;
		md.values.put(VSTPDataItemType.HLR_ANNN.getId(), "AAA");
		
		byte[] data = md.encode();
		
		md = VSTP.decode(data);
		for(String s : md.values.keySet()){
			System.out.println(s + "->" + md.values.get(s));
		}
		//System.out.println(new String(md.encode()));

		
	}

}
