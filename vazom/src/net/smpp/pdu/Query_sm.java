package net.smpp.pdu;

import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.utils.Utils;

public class Query_sm extends PDUBase {
	public String message_id;
	public TypeOfNumber source_addr_ton;
	public NumberingPlan source_addr_npi;
	public String source_addr;

	public Query_sm(){
		type = PDUType.QUERY_SM;
	}

	public void init(byte[] data) {
		message_id = Utils.Cstr_decode(data, byte_pos);
		byte_pos += message_id.length() + 1;
		// Source addr ton
		source_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Source addr npi
		source_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Source addr
		source_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += source_addr.length() + 1;

	}

}
