package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Record_Type extends OCTET_STRING{

	public Record_Type(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
