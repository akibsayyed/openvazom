package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AmmDescriptor extends CHOICE{

	public MediaDescriptor get_mediaDescriptor(){
		return (MediaDescriptor)elements.get(0).data;
	}
	public MediaDescriptor new_mediaDescriptor(){
		return new MediaDescriptor();
	}

	public ModemDescriptor get_modemDescriptor(){
		return (ModemDescriptor)elements.get(1).data;
	}
	public ModemDescriptor new_modemDescriptor(){
		return new ModemDescriptor();
	}

	public MuxDescriptor get_muxDescriptor(){
		return (MuxDescriptor)elements.get(2).data;
	}
	public MuxDescriptor new_muxDescriptor(){
		return new MuxDescriptor();
	}

	public EventsDescriptor get_eventsDescriptor(){
		return (EventsDescriptor)elements.get(3).data;
	}
	public EventsDescriptor new_eventsDescriptor(){
		return new EventsDescriptor();
	}

	public EventBufferDescriptor get_eventBufferDescriptor(){
		return (EventBufferDescriptor)elements.get(4).data;
	}
	public EventBufferDescriptor new_eventBufferDescriptor(){
		return new EventBufferDescriptor();
	}

	public SignalsDescriptor get_signalsDescriptor(){
		return (SignalsDescriptor)elements.get(5).data;
	}
	public SignalsDescriptor new_signalsDescriptor(){
		return new SignalsDescriptor();
	}

	public DigitMapDescriptor get_digitMapDescriptor(){
		return (DigitMapDescriptor)elements.get(6).data;
	}
	public DigitMapDescriptor new_digitMapDescriptor(){
		return new DigitMapDescriptor();
	}

	public AuditDescriptor get_auditDescriptor(){
		return (AuditDescriptor)elements.get(7).data;
	}
	public AuditDescriptor new_auditDescriptor(){
		return new AuditDescriptor();
	}

	public StatisticsDescriptor get_statisticsDescriptor(){
		return (StatisticsDescriptor)elements.get(8).data;
	}
	public StatisticsDescriptor new_statisticsDescriptor(){
		return new StatisticsDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mediaDescriptor(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_modemDescriptor(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_muxDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventsDescriptor(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventBufferDescriptor(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalsDescriptor(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digitMapDescriptor(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_auditDescriptor(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, true){public void set(){ data = new_statisticsDescriptor(); }});
	}
	public AmmDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
