package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TerminationListChoice extends CHOICE{

	public IntersystemTermination get_intersystemTermination(){
		return (IntersystemTermination)elements.get(0).data;
	}
	public IntersystemTermination new_intersystemTermination(){
		return new IntersystemTermination();
	}

	public LocalTermination get_localTermination(){
		return (LocalTermination)elements.get(1).data;
	}
	public LocalTermination new_localTermination(){
		return new LocalTermination();
	}

	public PSTNTermination get_pstnTermination(){
		return (PSTNTermination)elements.get(2).data;
	}
	public PSTNTermination new_pstnTermination(){
		return new PSTNTermination();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(89, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_intersystemTermination(); }});
		elements.add(new ElementDescriptor(91, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_localTermination(); }});
		elements.add(new ElementDescriptor(95, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_pstnTermination(); }});
	}
	public TerminationListChoice(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
