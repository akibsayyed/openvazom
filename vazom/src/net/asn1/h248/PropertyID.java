package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class PropertyID extends OCTET_STRING{

	public PropertyID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
