package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudLocalControlDescriptor extends SEQUENCE{

	public NULL get_streamMode(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_streamMode(){
		return new NULL();
	}

	public NULL get_reserveValue(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_reserveValue(){
		return new NULL();
	}

	public NULL get_reserveGroup(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_reserveGroup(){
		return new NULL();
	}

	public IndAudPropertyParmSequenceOf get_propertyParms(){
		return (IndAudPropertyParmSequenceOf)elements.get(3).data;
	}
	public IndAudPropertyParmSequenceOf new_propertyParms(){
		return new IndAudPropertyParmSequenceOf();
	}

	public StreamMode get_streamModeSel(){
		return (StreamMode)elements.get(4).data;
	}
	public StreamMode new_streamModeSel(){
		return new StreamMode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamMode(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reserveValue(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_reserveGroup(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_propertyParms(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_streamModeSel(); }});
	}
	public IndAudLocalControlDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
