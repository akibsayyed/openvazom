package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TriggerType extends ENUMERATED{

	public static final int _all_Calls = 1;
	public static final int _double_Introducing_Star = 2;
	public static final int _single_Introducing_Star = 3;
	public static final int _reserved_for_Home_System_Feature_Code = 4;
	public static final int _double_Introducing_Pound = 5;
	public static final int _single_Introducing_Pound = 6;
	public static final int _revertive_Call = 7;
	public static final int _a0_Digit = 8;
	public static final int _a1_Digit = 9;
	public static final int _a2_Digit = 10;
	public static final int _a3_Digit = 11;
	public static final int _a4_Digit = 12;
	public static final int _a5_Digit = 13;
	public static final int _a6_Digit = 14;
	public static final int _a7_Digit = 15;
	public static final int _a8_Digit = 16;
	public static final int _a9_Digit = 17;
	public static final int _a10_Digit = 18;
	public static final int _a11_Digit = 19;
	public static final int _a12_Digit = 20;
	public static final int _a13_Digit = 21;
	public static final int _a14_Digit = 22;
	public static final int _a15_Digit = 23;
	public static final int _local_Call = 24;
	public static final int _intra_LATA_Toll_Call = 25;
	public static final int _inter_LATA_Toll_Call = 26;
	public static final int _world_Zone_Call = 27;
	public static final int _international_Call = 28;
	public static final int _unrecognized_Number = 29;
	public static final int _prior_Agreement = 30;
	public static final int _specific_Called_Party_Digit_String = 31;
	public static final int _mobile_Termination = 32;
	public static final int _advanced_Termination = 33;
	public static final int _location = 34;
	public static final int _locally_Allowed_Specific_Digit_String = 35;
	public static final int _origination_Attempt_Authorized = 36;
	public static final int _calling_Routing_Address_Available = 37;
	public static final int _initial_Termination = 38;
	public static final int _called_Routing_Address_Available = 39;
	public static final int _o_Answer = 40;
	public static final int _o_Disconnect = 41;
	public static final int _o_Called_Party_Busy = 42;
	public static final int _o_No_Answer = 43;
	public static final int _terminating_Resource_Available = 64;
	public static final int _t_Busy = 65;
	public static final int _t_No_Answer = 66;
	public static final int _t_No_Page_Response = 67;
	public static final int _t_Routable = 68;
	public static final int _t_Answer = 69;
	public static final int _t_Disconnect = 70;
	public static final int _reserved_for_TDP_R_DP_Type_value = 220;
	public static final int _reserved_for_TDP_N_DP_Type_value = 221;
	public static final int _reserved_for_EDP_R_DP_Type_value = 222;
	public static final int _reserved_for_EDP_N_DP_Type_value = 223;
	public TriggerType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
