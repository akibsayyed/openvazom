package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceState extends ENUMERATED{

	public static final int _test = 0;
	public static final int _outOfSvc = 1;
	public static final int _inSvc = 2;
	public ServiceState(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
