package net.smstpdu.tpdcs.store;

import java.util.HashMap;


public enum IndicationType {

	VOICE_MAIL_MESSAGE_WAITING(0x00),
	FAX_MESSAGE_WAITING(0x01),
	EMAIL_MESSAGE_WAITING(0x02),
	OTHER_MESSAGE_WAITING(0x03);
	
	// BITS 1 and 0  = 000000XX
	
	private int id;
	private static final HashMap<Integer, IndicationType> lookup = new HashMap<Integer, IndicationType>();
	static{
		for(IndicationType td : IndicationType.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static IndicationType get(int id){ return lookup.get(id); }
	private IndicationType(int _id){ id = _id; }
	
}
