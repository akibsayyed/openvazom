package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class DataAccessElementListInner extends SEQUENCE{

	public DataAccessElement get_dataAccessElement1(){
		return (DataAccessElement)elements.get(0).data;
	}
	public DataAccessElement new_dataAccessElement1(){
		return new DataAccessElement();
	}

	public DataAccessElement get_dataAccessElement2(){
		return (DataAccessElement)elements.get(1).data;
	}
	public DataAccessElement new_dataAccessElement2(){
		return new DataAccessElement();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(249, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dataAccessElement1(); }});
		elements.add(new ElementDescriptor(249, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataAccessElement2(); }});
	}
	public DataAccessElementListInner(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
