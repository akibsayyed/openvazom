package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventParameterExtraInfoChoiceWrapper extends SEQUENCE{

	public EventParameterExtraInfoChoice get_eventParameterExtraInfoChoice(){
		return (EventParameterExtraInfoChoice)elements.get(0).data;
	}
	public EventParameterExtraInfoChoice new_eventParameterExtraInfoChoice(){
		return new EventParameterExtraInfoChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_eventParameterExtraInfoChoice(); }});
	}
	public EventParameterExtraInfoChoiceWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
