package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class RandomVariable extends OCTET_STRING{

	public RandomVariable(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
