package net.sgn.cli;

import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import net.cli.CliConnectionBase;
import net.config.SGNConfigData;
import net.sgn.cli.CLIManager;
import net.utils.Utils;

public class CLIConnection extends CliConnectionBase{
	public int halt_counter;

	public CLIConnection(Socket socket, ConcurrentHashMap<String, CliConnectionBase> connList) {
		super(new CLIManager(), SGNConfigData.sgn_id, "SG", socket, connList);
	}


	public void cli_execute() {
		// echo
		out.println();
		out.print((char)13);
		out.flush();

		for(int j = 0; j<last_cmd_lst.size(); j++) if(last_cmd_lst.get(j).equals(cmd)){
			history_found = true;
			last_cmd_index = j;
		}
		if(!history_found){
			last_cmd_lst.add(cmd);
			last_cmd_index = last_cmd_lst.size() - 1;
		}
		history_found = false;

		his_lst = rebuildCLI(cmd);
		res = "";
		if(his_lst.size() > 0){
			Object[] tmp_params;
			his = his_lst.get(his_lst.size() - 1);
			if(his.descriptor != null) {
				if(his.params != null){
					tmp_params = new Object[his.params.size() + 1];
					for(int j = 0; j<his.params.size(); j++) tmp_params[j] = his.params.get(j);
					tmp_params[his.params.size()] = this.current_conn;
					res += Utils.ansi_paint(his.descriptor.execute(tmp_params));
				}
				else{
					tmp_params = new Object[1];
					tmp_params[0] = this.current_conn;
					res += Utils.ansi_paint(his.descriptor.execute(tmp_params));
				}
			}
			
		}			
		if(res != null) out.println(res.replaceAll("\n", "\n" + (char)13) + (char)13);
		out.print("\u001B[1;33m");
		out.print("\u001B[1;33mVAZOM-SG[\u001B[1;34m" + SGNConfigData.sgn_id + "\u001B[1;33m]> ");
		out.print("\u001B[0m");
		out.print("\u001B7"); // save cursor pos
		res = "";
		out.flush();
		cmd = "";		
	}
	
	
	
}
