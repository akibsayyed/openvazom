package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TerminationRestrictionCode extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _termination_denied =  1 ;
	public static final int _unrestricted =  2 ;
	public static final int _the_treatment_for_this_value_is_not_specified =  3 ;
	public TerminationRestrictionCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
