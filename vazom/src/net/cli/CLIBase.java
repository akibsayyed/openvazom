package net.cli;

import java.util.ArrayList;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

import net.utils.Utils;

public class CLIBase {
	public ArrayList<DescriptorBase> method_lst;
	public ArrayList<DescriptorBase> group_lst;
	
	public CLIBase(){
		// create lists
		method_lst = new ArrayList<DescriptorBase>();
		group_lst = new ArrayList<DescriptorBase>();

	}
	
	public ArrayList<String> getParams(String starts_with, DescriptorBase d){
		if(d.params != null){
			ArrayList<String> res = new ArrayList<String>();
			for(int i = 0;  i<d.params.length; i++) if(d.params[i].startsWith(starts_with)) res.add(d.params[i]);
			return res;
			
		}
			
		return null;
		
	}
	public void displayLst(ArrayList<DescriptorBase> lst){
		DescriptorBase db = null;
		for(int i = 0; i<lst.size(); i++){
			db = lst.get(i);
			if(db.type == MethodType.CONSTRUCTED){
				System.out.println("GROUP: " + db.name);
				if(db.params != null){
					for(int j = 0; j<db.params.length; j++){
						System.out.println("	|-> PARAM: " + db.params[j]);
						
					}
					
				}
				
			}else if(db.type == MethodType.FINAL){
				System.out.println("METHOD: " + db.name);
				if(db.params != null){
					for(int j = 0; j<db.params.length; j++){
						System.out.println("	|-> PARAM: " + db.params[j]);
						
					}
					
				}

			}
		}
	}
	public int getTotalTokenSize(ArrayList<CLIHistoryItem> lst){
		int res = 0;
		CLIHistoryItem his = null;
		for(int i = 0; i<lst.size(); i++){
			res++;
			his = lst.get(i);
			if(his.params != null){
				for(int j = 0; j<his.params.size(); j++){
					if(his.params.get(j).value != null) res+=2;
				}
			}
		}
		
		return res;
	}
	
	public String generateLst(ArrayList<DescriptorBase> lst){
		DescriptorBase db = null;
		String res = "";
		int max_l = 0;
		
		// find max method/group length
		for(int i = 0; i<lst.size(); i++) if(lst.get(i).name.length() > max_l) max_l = lst.get(i).name.length();
		
		// generate list
		for(int i = 0; i<lst.size(); i++){
			db = lst.get(i);
			if(db.type == MethodType.CONSTRUCTED){
				res += " [BLUE]" + String.format("%" + max_l + "s", db.name) + " -> [WHITE]" + db.description;// + "\n";
				if(db.params != null){
					res += " ([YELLOW]";
					for(int j = 0; j<db.params.length; j++){
						res += db.params[j] + ", ";
						
					}
					res = res.substring(0, res.length() - 2);
					res += "[BLUE])\n";
					
				}else res += "\n";
				
			}else if(db.type == MethodType.FINAL){
				res += " [GREEN]" + String.format("%" + max_l + "s", db.name) + " -> [WHITE]" + db.description;// + "\n";
				if(db.params != null){
					res += " ([YELLOW]";
					for(int j = 0; j<db.params.length; j++){
						res += db.params[j] + ", ";
						
					}
					res = res.substring(0, res.length() - 2);
					res += "[GREEN])\n";
					
				}else res += "\n";

			}
		}
		return Utils.ansi_paint(res);
	}

	
	
	public ArrayList<DescriptorBase> getLst(String starts_with, String parent){
		GroupDescriptor gd = null;
		MethodDescriptor md = null;
		ArrayList<DescriptorBase> res = new ArrayList<DescriptorBase>();
		parent = parent.replaceAll(" ", "");
		
		// params
		
		// groups
		for(int i = 0; i<group_lst.size(); i++){
			gd = (GroupDescriptor)group_lst.get(i);
			if(gd.name.toUpperCase().startsWith(starts_with.toUpperCase()) && gd.parent.toUpperCase().equals(parent.toUpperCase())){
				res.add(gd);
				//System.out.println("GROUP: " + gd.name);
				if(gd.params != null){
					for(int j = 0; j<gd.params.length; j++){
						//System.out.println("	|-> PARAM: " + gd.params[j]);
						
					}
					
				}
			}
		}
		// methods
		for(int i = 0; i<method_lst.size(); i++){
			md = (MethodDescriptor)method_lst.get(i);
			//tmp = md.parent.split(",");
			if(md.name.toUpperCase().startsWith(starts_with.toUpperCase())){
				//System.out.println(md.parent);
				//for(int j = 0; j<tmp.length; j++) if(tmp[j].trim().equals(parent)){
				if(md.parent.toUpperCase().equals(parent.toUpperCase())){
					res.add(md);
					//System.out.println(tmp[j].trim());
					//System.out.println("METHOD: " + md.name);
					if(md.params != null){
						for(int j = 0; j<md.params.length; j++){
							//System.out.println("	|-> PARAM: " + md.params[j]);
							
						}
						
					}
					//break;
				}
			}
		}
		return res;
	}
	public ArrayList<String> getParamLst(DescriptorBase db, String starts_with){
		if(db.params != null){
			ArrayList<String> res = new ArrayList<String>();
			for(int i = 0; i<db.params.length; i++){
				if(db.params[i].toUpperCase().startsWith(starts_with.toUpperCase())) res.add(db.params[i]);
			}
			return res;
		}
		return null;
		
	}

	public ArrayList<ParamDescriptor> getParams(DescriptorBase db){
		if(db.params != null){
			ArrayList<ParamDescriptor> res = new ArrayList<ParamDescriptor>();
			for(int i = 0; i<db.params.length; i++) res.add(new ParamDescriptor(db.params[i], null));
			return res;
		}
		return null;
		
	}
	
	public String getParamHint(DescriptorBase db, String param){
		String res = null;
		//int c = 0;
		if(db.params != null){
			//System.out.println("----------" + db.name + " -------------");
			for(int i = 0; i<db.params.length; i++){ 
				//System.out.println(db.params[i].toUpperCase() + "----" + param.toUpperCase());
				if(db.params[i].toUpperCase().startsWith(param.toUpperCase())){
					res = db.params[i];
					return res;
					//c++;
					//System.out.println("FOUND: " + res);
				}
			}			
			//System.out.println("--------------------------------------");

			//if(c == 1) return res;
		}
		return null;
		
	}		

	
	public ArrayList<ParamDescriptor> validateParams(DescriptorBase gd, String[] tokens, int start){
		// SHOW STATS stats_mode 0 stats_id
		String param;
		String tmp;
		ArrayList<ParamDescriptor> res = null;
		ParamDescriptor paramd = null;
		if(gd.params != null){
			res = new ArrayList<ParamDescriptor>();
			for(int i = 0; i<gd.params.length; i++){
				param = gd.params[i];
				paramd = new ParamDescriptor(param, null);
				res.add(paramd);
			}
			
			int j = start;
			while(j < tokens.length){
				//System.out.println(i + ":" + j + ":" + tokens[j]);
				if((tmp = getParamHint(gd, tokens[j])) != null && tokens.length > j + 1){
					
					for(int k = 0; k<res.size(); k++){
						paramd = res.get(k);
						if(paramd.name.equalsIgnoreCase(tmp)){
							paramd.value = tokens[j+1];
							if(paramd.value.contains(" ")) paramd.group = true;
							//found = true;
							j++;
							break;
						}
					}
				}
				j++;
			}
			/*
			for(int k = 0; k<res.size(); k++){
				paramd = res.get(k);
				if(paramd.value == null) System.out.println("[" + gd.name + "], PARAM [" + paramd.name + "] missing!");
				
			}
			*/
		}
		return res;
	}
	
	public boolean paramExists(String name, ArrayList<DescriptorBase> lst){
		DescriptorBase db = null;
		for(int i = 0; i<lst.size(); i++){
			db = lst.get(i);
			if(db.params != null){
				for(int j = 0; j<db.params.length; j++) if(db.params[j].equalsIgnoreCase(name)) return true;
			}
		}
		return false;
	}

	public int paramsComplete(ArrayList<ParamDescriptor> lst){
		int pc = 0;
		if(lst != null) for(int i = 0; i<lst.size(); i++) if(lst.get(i).value != null) pc++;
		return pc;
	}
	
	public DescriptorBase getHint(String name, String parent, ArrayList<DescriptorBase> lst){
		DescriptorBase res = null;
		int c = 0;
		for(int i = 0; i<lst.size(); i++) 
			if(lst.get(i).name.equalsIgnoreCase(name) && lst.get(i).parent.equalsIgnoreCase(parent)){
				res = lst.get(i);
				return res;
			}else if(lst.get(i).name.toUpperCase().startsWith(name.toUpperCase()) && lst.get(i).parent.equalsIgnoreCase(parent)){
				c++;
				res = lst.get(i);
			}
		
		if(c == 1) return res; else return null;
	}
	
	public String[] tab_process(String line){
		String[] res = new String[2];
		String[] tokens = cli_tokenize(line);// cmd.split("[ \t]+");
		ArrayList<CLIHistoryItem> his_lst = processLine(line);
		CLIHistoryItem his = null;
		ArrayList<DescriptorBase> tmp_lst = null;
		ArrayList<String> tmp_str = null;
		String tmp_parent = "";
		res[0] = "";
		res[1] = "\n";
		if(his_lst.size() > 0){
			res[0] = "";
			for(int j = 0; j<his_lst.size(); j++){
				his = his_lst.get(j);
				res[0] += his.cmd + " ";
				if(his.params != null){
					for(int k = 0; k<his.params.size(); k++){
						if(his.params.get(k).value != null){
							if(his.params.get(k).group){
								res[0] += his.params.get(k).name + " ";
								res[0] += "\"" + his.params.get(k).value +  "\" ";
								
							}else{
								res[0] += his.params.get(k).name + " ";
								res[0] += his.params.get(k).value + " ";
								
							}
						}
					}
				}
				
			}
			res[0] = res[0].toLowerCase();
			
			
			// param check
			ParamDescriptor pd = null;
			for(int j = 0; j<his_lst.size(); j++) if(his_lst.get(j).incomplete){
				his = his_lst.get(j);
				if(his.params != null){
					for(int k = 0; k<his.params.size(); k++){
						pd = his.params.get(k);
						if(pd.value == null) res[1] += Utils.ansi_paint("[RED] " + his.cmd + ": parameter [ [WHITE]" + pd.name + "[RED] ] missing!\n");
					}
				}
			}
			
			boolean hint_found = false;
			//System.out.println(tokens.length + ":" + cli.getTotalTokenSize(his_lst));
			//for(int bla = 0; bla < tokens.length; bla++) System.out.println(bla + "->" + tokens[bla]);
			if(tokens.length != getTotalTokenSize(his_lst)){
				//System.out.println("AAAAAAAAAAAAAa");
				// param hints
				tmp_lst = getLst(his_lst.get(his_lst.size() - 1).cmd, his_lst.get(his_lst.size() - 1).parent);
				tmp_str = getParamLst(tmp_lst.get(tmp_lst.size() - 1), tokens[tokens.length - 1]);
				if(tmp_str != null){
					int j = 0;
					int k = 0;
					if(tmp_str.size() > 0){
						//cmd += tmp_str.get(0) + " ";
						his = his_lst.get(his_lst.size() - 1);
						while(j < tmp_str.size()){
							k = 0;
							while(k < his.params.size()){
								if(his.params.get(k).name.equalsIgnoreCase(tmp_str.get(j)) && his.params.get(k).value == null){
									res[0] += tmp_str.get(j) + " ";
									k = his.params.size();
									j = tmp_str.size();
									hint_found = true;
								}
								k++;
							}
							j++;
						}
					}
				}
				// no param hint found, check group/method
				if(!hint_found){
					// group/method hint
					for(int j = 0; j<his_lst.size(); j++) tmp_parent = his_lst.get(j).cmd + "," + tmp_parent;
					tmp_parent = tmp_parent.substring(0, tmp_parent.length() - 1);
					//System.out.println("PARENT: " + tmp_parent);
					tmp_lst = getLst(tokens[tokens.length - 1], tmp_parent);
					res[1] += generateLst(tmp_lst);
					if(tmp_lst.size() == 0){
						res[1] += Utils.ansi_paint("[RED]Unknown group/method [[GREEN]" + tokens[tokens.length - 1] + "[RED]]!\n");
						tmp_lst = getLst("", tmp_parent);
						res[1] += generateLst(tmp_lst);
						
					}
					//System.out.println("AAAAAAAAAAAAA");
					res[0] += tokens[tokens.length - 1];
					
				}

				
				
			}else{
				if(his_lst.size() > 1)
					res[1] += generateLst(getLst("", his_lst.get(his_lst.size() - 1).cmd + "," + his_lst.get(his_lst.size() - 1).parent));
				else
					res[1] += generateLst(getLst("", his_lst.get(his_lst.size() - 1).cmd));
			}
		
		}else{
			tmp_lst = getLst(line, "");
			if(tmp_lst.size() == 1){
				//cmd = tmp_lst.get(0).name;
			}
			res[1] += generateLst(tmp_lst);
		}
		
		return res;
		
	}

	
	public String[] cli_tokenize(String line){
		String[] res = null;
		ArrayList<String> buff = new ArrayList<String>();
		Pattern pt = Pattern.compile("\"(.*?[^\\\\])\"");
		Matcher mt = pt.matcher(line);
		String[] tmp;
		String tmp_line;
		int p = 0;
		while(mt.find()){
			if(mt.start() - p > 1){
				tmp_line = line.substring(p, mt.start()).trim();
				if(tmp_line.length() > 0){
					tmp = tmp_line.split("[ \t]+");
					for(int i = 0; i<tmp.length; i++) buff.add(tmp[i]);
				}
			}
			p = mt.end();
			buff.add(mt.group(1));
		}
		if(p < line.length()){
			tmp = line.substring(p, line.length()).trim().split("[ \t]+");
			for(int i = 0; i<tmp.length; i++) if(tmp[i].trim().length() > 0) buff.add(tmp[i]);
			
		}

		res = new String[buff.size()];
		for(int i = 0; i<buff.size(); i++) res[i] = buff.get(i);
		return res;
	}
	
	public ArrayList<CLIHistoryItem> processLine(String line){
		// SHOW STATS stats_mode 0 stats_i DB
		ArrayList<CLIHistoryItem> res = new ArrayList<CLIHistoryItem>();
		CLIHistoryItem his = null;
		String[] tokens = cli_tokenize(line); //line.split("[ \t]+");
		GroupDescriptor gd = null;
		MethodDescriptor md = null;
		String parent = "";
		int valid;
		int i = 0;
		boolean err = false;
		ArrayList<ParamDescriptor> param_lst = null;
		ArrayList<DescriptorBase> tmp_lst = new ArrayList<DescriptorBase>();
		DescriptorBase dbtmp = null;
		// combine
		for(int j = 0; j<group_lst.size(); j++) tmp_lst.add(group_lst.get(j));
		for(int j = 0; j<method_lst.size(); j++) tmp_lst.add(method_lst.get(j));
		// loop
		while(i < tokens.length && !err){
			dbtmp = getHint(tokens[i], parent, tmp_lst);
			if(dbtmp != null){
				if(dbtmp.type == MethodType.CONSTRUCTED) gd = (GroupDescriptor)dbtmp;
				else md = (MethodDescriptor)dbtmp;

				// METHOD
				if(md != null){
					param_lst = validateParams(md, tokens, i + 1);
					valid = paramsComplete(param_lst);
					err = (valid != (md.params == null ? 0 : md.params.length));
					i += valid*2;
					his = new CLIHistoryItem();
					his.cmd = md.name;
					his.parent = parent;
					his.params = param_lst;
					his.incomplete = err;
					his.descriptor = md;
					res.add(his);
				// GROUP
				}else if(gd != null){
					param_lst = validateParams(gd, tokens, i + 1);
					valid = paramsComplete(param_lst);
					err = (valid != (gd.params == null ? 0 : gd.params.length));
					his = new CLIHistoryItem();
					his.cmd = gd.name;
					his.parent = parent;
					his.params = param_lst;
					his.incomplete = err;
					res.add(his);
					parent = gd.name + (parent.length() == 0 ? "" : "," + parent);
					i += valid*2;
						
				}	
				gd = null;
				md = null;
			}

			
	
			i++;
		}
		return res;
		
		
	}
	public GroupDescriptor getGroup(String name){
		for(int i = 0; i<group_lst.size(); i++) if(group_lst.get(i).name.equalsIgnoreCase(name)) return (GroupDescriptor)group_lst.get(i);
		return null;
	}
	public GroupDescriptor getGroup(String name, String parent){
		for(int i = 0; i<group_lst.size(); i++) 
			if(group_lst.get(i).name.equalsIgnoreCase(name) && group_lst.get(i).parent.equalsIgnoreCase(parent)) return (GroupDescriptor)group_lst.get(i);
		return null;
	}
	public MethodDescriptor getMethod(String name){
		for(int i = 0; i<method_lst.size(); i++) if(method_lst.get(i).name.equalsIgnoreCase(name)) return (MethodDescriptor)method_lst.get(i);
		return null;
	}

}
