package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Problem extends INTEGER{

	public static final int _general_unrecognisedComponentType = 257;
	public static final int _general_incorrectComponentPortion = 258;
	public static final int _general_badlyStructuredCompPortion = 259;
	public static final int _general_incorrectComponentCoding = 260;
	public static final int _invoke_duplicateInvocation = 513;
	public static final int _invoke_unrecognisedOperation = 514;
	public static final int _invoke_incorrectParameter = 515;
	public static final int _invoke_unrecognisedCorrelationID = 516;
	public static final int _returnResult_unrecognisedCorrelationID = 769;
	public static final int _returnResult_unexpectedReturnResult = 770;
	public static final int _returnResult_incorrectParameter = 771;
	public static final int _returnError_unrecognisedCorrelationID = 1025;
	public static final int _returnError_unexpectedReturnError = 1026;
	public static final int _returnError_unrecognisedError = 1027;
	public static final int _returnError_unexpectedError = 1028;
	public static final int _returnError_incorrectParameter = 1029;
	public static final int _transaction_unrecognizedPackageType = 1281;
	public static final int _transaction_incorrectTransPortion = 1282;
	public static final int _transaction_badlyStructuredTransPortion = 1283;
	public static final int _transaction_unassignedRespondingTransID = 1284;
	public static final int _transaction_permissionToReleaseProblem = 1285;
	public static final int _transaction_resourceUnavailable = 1286;
	public Problem(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
