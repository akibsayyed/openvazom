package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TargetCellID extends OCTET_STRING{

	public TargetCellID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
