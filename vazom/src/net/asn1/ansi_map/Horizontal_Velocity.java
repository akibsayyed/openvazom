package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Horizontal_Velocity extends OCTET_STRING{

	public Horizontal_Velocity(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
