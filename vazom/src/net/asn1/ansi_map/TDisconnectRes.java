package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TDisconnectRes extends SET{

	public DMH_ServiceID get_dmh_ServiceID(){
		return (DMH_ServiceID)elements.get(0).data;
	}
	public DMH_ServiceID new_dmh_ServiceID(){
		return new DMH_ServiceID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(305, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_ServiceID(); }});
	}
	public TDisconnectRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
