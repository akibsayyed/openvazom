package net.asn1.compiler;

public enum ASNTag {
	EOC,
	BOOLEAN,
	INTEGER,
	BIT_STRING,
	OCTET_STRING,
	NULL,
	OBJECT_IDENTIFIER,
	OBJECT_DESCRIPTOR,
	EXTERNAL,
	REAL,
	ENUMERATED,
	EMBEDDED_PDV,
	UTF8STRING,
	RELATIVE_OID,
	SEQUENCE,
	SEQUENCE_OF,
	SET,
	SET_OF,
	NUMERICSTRING,
	PRINTABLESTRING,
	T61STRING,
	VIDEOTEXSTRING,
	IA5STRING,
	UTCTIME,
	GENERALIZEDTIME,
	GRAPHICSTRING,
	VISIBLESTRING,
	GENERALSTRING,
	UNIVERSALSTRING,
	CHARACTER_STRING,
	BMPSTRING,
	ANY, //added, ANY = OPEN TYPE
	CHOICE; // added
	
	public synchronized static int name2Tag(String name){
		int res = -1;
		if(name.equalsIgnoreCase(ANY.toString())) res = -2; else
		if(name.equalsIgnoreCase(CHOICE.toString())) res = -3; else
			
		if(name.equalsIgnoreCase(EOC.toString())) res = 0x00; else
		if(name.equalsIgnoreCase(BOOLEAN.toString())) res = 0x01; else
		if(name.equalsIgnoreCase(INTEGER.toString())) res = 0x02; else
		if(name.equalsIgnoreCase(BIT_STRING.toString())) res = 0x03; else
		if(name.equalsIgnoreCase(OCTET_STRING.toString())) res = 0x04; else
		if(name.equalsIgnoreCase(NULL.toString())) res = 0x05; else
		if(name.equalsIgnoreCase(OBJECT_IDENTIFIER.toString())) res = 0x06; else 
		if(name.equalsIgnoreCase(OBJECT_DESCRIPTOR.toString())) res = 0x07; else
		if(name.equalsIgnoreCase(EXTERNAL.toString())) res = 0x08; else
		if(name.equalsIgnoreCase(REAL.toString())) res = 0x09; else
		if(name.equalsIgnoreCase(ENUMERATED.toString())) res = 0x0A; else 
		if(name.equalsIgnoreCase(EMBEDDED_PDV.toString())) res = 0x0B; else
		if(name.equalsIgnoreCase(UTF8STRING.toString())) res = 0x0C; else
		if(name.equalsIgnoreCase(RELATIVE_OID.toString())) res = 0x0D; else
		if(name.equalsIgnoreCase(SEQUENCE.toString())) res = 0x10; else
		if(name.equalsIgnoreCase(SEQUENCE_OF.toString())) res = 0x10; else
		if(name.equalsIgnoreCase(SET.toString())) res = 0x11; else
		if(name.equalsIgnoreCase(SET_OF.toString())) res = 0x11; else
		if(name.equalsIgnoreCase(NUMERICSTRING.toString())) res = 0x12; else 
		if(name.equalsIgnoreCase(PRINTABLESTRING.toString())) res = 0x13; else
		if(name.equalsIgnoreCase(T61STRING.toString())) res = 0x14; else
		if(name.equalsIgnoreCase(VIDEOTEXSTRING.toString())) res = 0x15; else 
		if(name.equalsIgnoreCase(IA5STRING.toString())) res = 0x16; else
		if(name.equalsIgnoreCase(UTCTIME.toString())) res = 0x17; else
		if(name.equalsIgnoreCase(GENERALIZEDTIME.toString())) res = 0x18; else 
		if(name.equalsIgnoreCase(GRAPHICSTRING.toString())) res = 0x19; else
		if(name.equalsIgnoreCase(VISIBLESTRING.toString())) res = 0x1A; else
		if(name.equalsIgnoreCase(GENERALSTRING.toString())) res = 0x1B; else
		if(name.equalsIgnoreCase(UNIVERSALSTRING.toString())) res = 0x1C; else
		if(name.equalsIgnoreCase(CHARACTER_STRING.toString())) res = 0x1D; else
		if(name.equalsIgnoreCase(BMPSTRING.toString())) res = 0x0E;
			
		return res;
		
	}
	public synchronized static ASNTag fromId(byte id){
		ASNTag res = null;
		switch(id){
			case -3: res = CHOICE; break;
			case -2: res = ANY; break;
			case 0x00: res = EOC; break;
			case 0x01: res = BOOLEAN; break;
			case 0x02: res = INTEGER; break;
			case 0x03: res = BIT_STRING; break;
			case 0x04: res = OCTET_STRING; break;
			case 0x05: res = NULL; break;
			case 0x06: res = OBJECT_IDENTIFIER; break;
			case 0x07: res = OBJECT_DESCRIPTOR; break;
			case 0x08: res = EXTERNAL; break;
			case 0x09: res = REAL; break;
			case 0x0A: res = ENUMERATED; break;
			case 0x0B: res = EMBEDDED_PDV; break;
			case 0x0C: res = UTF8STRING; break;
			case 0x0D: res = RELATIVE_OID; break;
			case 0x10: res = SEQUENCE; break;
			case 0x11: res = SET; break;
			case 0x12: res = NUMERICSTRING; break;
			case 0x13: res = PRINTABLESTRING; break;
			case 0x14: res = T61STRING; break;
			case 0x15: res = VIDEOTEXSTRING; break;
			case 0x16: res = IA5STRING; break;
			case 0x17: res = UTCTIME; break;
			case 0x18: res = GENERALIZEDTIME; break;
			case 0x19: res = GRAPHICSTRING; break;
			case 0x1A: res = VISIBLESTRING; break;
			case 0x1B: res = GENERALSTRING; break;
			case 0x1C: res = UNIVERSALSTRING; break;
			case 0x1D: res = CHARACTER_STRING; break;
			case 0x1E: res = BMPSTRING; break;
			
		}		
		
		return res;
		
	}

}


