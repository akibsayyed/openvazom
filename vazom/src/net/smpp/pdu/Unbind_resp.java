package net.smpp.pdu;

import java.util.ArrayList;

import net.smpp.PDUType;
import net.utils.Utils;

public class Unbind_resp extends PDUBase {

	public Unbind_resp(){
		type = PDUType.UNBIND_RESP;
	}

	public byte[] encode(){
		//command_length += system_id.length() + 1 + (sc_interface_version != null ? sc_interface_version.length : 0);
		byte[] enc_common = super.encode();
		ArrayList<Byte> buff = new ArrayList<Byte>();
		Utils.bytesToLst(buff, enc_common);
		command_length = buff.size();
		buff.set(0, (byte)(command_length >> 24));
		buff.set(1, (byte)(command_length >> 16));
		buff.set(2, (byte)(command_length >> 8));
		buff.set(3, (byte)(command_length));
		
		return Utils.list2array(buff);

	}
	public void init(byte[] data) {
		// no data
	}

}
