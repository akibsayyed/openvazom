package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceChangeReasonStr extends IA5STRING{

	public ServiceChangeReasonStr(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 22;
	}
}
