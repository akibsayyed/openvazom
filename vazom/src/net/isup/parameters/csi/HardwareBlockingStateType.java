package net.isup.parameters.csi;

import java.util.HashMap;

public enum HardwareBlockingStateType {
	NO_BLOCKING(0x00),
	LOCALLY_BLOCKED(0x10),
	REMOTELY_BLOCKED(0x20),
	LOCALLY_AND_REMOTELY_BLOCKED(0x30);
	
	
	private int id;
	private static final HashMap<Integer, HardwareBlockingStateType> lookup = new HashMap<Integer, HardwareBlockingStateType>();
	static{
		for(HardwareBlockingStateType td :HardwareBlockingStateType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static HardwareBlockingStateType get(int id){ return lookup.get(id); }
	private HardwareBlockingStateType(int _id){ id = _id; }		
}
