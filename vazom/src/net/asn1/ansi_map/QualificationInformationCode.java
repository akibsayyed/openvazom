package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class QualificationInformationCode extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _no_information =  1 ;
	public static final int _validation_only =  2 ;
	public static final int _validation_and_profile =  3 ;
	public static final int _profile_only =  4 ;
	public QualificationInformationCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
