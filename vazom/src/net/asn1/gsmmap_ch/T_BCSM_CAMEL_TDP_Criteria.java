package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class T_BCSM_CAMEL_TDP_Criteria extends SEQUENCE{

	public T_BcsmTriggerDetectionPoint get_t_BCSM_TriggerDetectionPoint(){
		return (T_BcsmTriggerDetectionPoint)elements.get(0).data;
	}
	public T_BcsmTriggerDetectionPoint new_t_BCSM_TriggerDetectionPoint(){
		return new T_BcsmTriggerDetectionPoint();
	}

	public BasicServiceCriteria get_basicServiceCriteria(){
		return (BasicServiceCriteria)elements.get(1).data;
	}
	public BasicServiceCriteria new_basicServiceCriteria(){
		return new BasicServiceCriteria();
	}

	public T_CauseValueCriteria get_t_CauseValueCriteria(){
		return (T_CauseValueCriteria)elements.get(2).data;
	}
	public T_CauseValueCriteria new_t_CauseValueCriteria(){
		return new T_CauseValueCriteria();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_t_BCSM_TriggerDetectionPoint(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_basicServiceCriteria(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_t_CauseValueCriteria(); }});
	}
	public T_BCSM_CAMEL_TDP_Criteria(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
