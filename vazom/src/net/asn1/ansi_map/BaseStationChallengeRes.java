package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class BaseStationChallengeRes extends SET{

	public AuthenticationResponseBaseStation get_authenticationResponseBaseStation(){
		return (AuthenticationResponseBaseStation)elements.get(0).data;
	}
	public AuthenticationResponseBaseStation new_authenticationResponseBaseStation(){
		return new AuthenticationResponseBaseStation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(36, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_authenticationResponseBaseStation(); }});
	}
	public BaseStationChallengeRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
