package net.isup.parameters;

import java.util.Arrays;

import net.isup.ParameterType;

public class ConnectionRequest extends ParameterBase {
	public byte[] local_reference;
	public int signalling_point_code;
	public int protocol_class;
	public int credit;
	
	public ConnectionRequest(){
		type = ParameterType.CONNECTION_REQUEST;
	}

	public void init(byte[] data) {
		if(data != null){
			local_reference = Arrays.copyOfRange(data, byte_position, 3);
			byte_position += 3;
			signalling_point_code = (data[byte_position] & 0x18) >> 3;
			byte_position++;
			protocol_class = data[byte_position++] & 0xff;
			credit = data[byte_position++] & 0xff;
		}
	}

}
