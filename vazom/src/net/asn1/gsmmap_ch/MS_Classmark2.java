package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class MS_Classmark2 extends OCTET_STRING{

	public MS_Classmark2(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
