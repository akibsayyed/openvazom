package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class OTASPRequestRes extends SET{

	public AKeyProtocolVersion get_aKeyProtocolVersion(){
		return (AKeyProtocolVersion)elements.get(0).data;
	}
	public AKeyProtocolVersion new_aKeyProtocolVersion(){
		return new AKeyProtocolVersion();
	}

	public AuthenticationResponseBaseStation get_authenticationResponseBaseStation(){
		return (AuthenticationResponseBaseStation)elements.get(1).data;
	}
	public AuthenticationResponseBaseStation new_authenticationResponseBaseStation(){
		return new AuthenticationResponseBaseStation();
	}

	public BaseStationPartialKey get_baseStationPartialKey(){
		return (BaseStationPartialKey)elements.get(2).data;
	}
	public BaseStationPartialKey new_baseStationPartialKey(){
		return new BaseStationPartialKey();
	}

	public DenyAccess get_denyAccess(){
		return (DenyAccess)elements.get(3).data;
	}
	public DenyAccess new_denyAccess(){
		return new DenyAccess();
	}

	public ModulusValue get_modulusValue(){
		return (ModulusValue)elements.get(4).data;
	}
	public ModulusValue new_modulusValue(){
		return new ModulusValue();
	}

	public OTASP_ResultCode get_otasp_ResultCode(){
		return (OTASP_ResultCode)elements.get(5).data;
	}
	public OTASP_ResultCode new_otasp_ResultCode(){
		return new OTASP_ResultCode();
	}

	public PrimitiveValue get_primitiveValue(){
		return (PrimitiveValue)elements.get(6).data;
	}
	public PrimitiveValue new_primitiveValue(){
		return new PrimitiveValue();
	}

	public SignalingMessageEncryptionReport get_signalingMessageEncryptionReport(){
		return (SignalingMessageEncryptionReport)elements.get(7).data;
	}
	public SignalingMessageEncryptionReport new_signalingMessageEncryptionReport(){
		return new SignalingMessageEncryptionReport();
	}

	public SSDUpdateReport get_ssdUpdateReport(){
		return (SSDUpdateReport)elements.get(8).data;
	}
	public SSDUpdateReport new_ssdUpdateReport(){
		return new SSDUpdateReport();
	}

	public UniqueChallengeReport get_uniqueChallengeReport(){
		return (UniqueChallengeReport)elements.get(9).data;
	}
	public UniqueChallengeReport new_uniqueChallengeReport(){
		return new UniqueChallengeReport();
	}

	public VoicePrivacyReport get_voicePrivacyReport(){
		return (VoicePrivacyReport)elements.get(10).data;
	}
	public VoicePrivacyReport new_voicePrivacyReport(){
		return new VoicePrivacyReport();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(181, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_aKeyProtocolVersion(); }});
		elements.add(new ElementDescriptor(36, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponseBaseStation(); }});
		elements.add(new ElementDescriptor(183, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_baseStationPartialKey(); }});
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_denyAccess(); }});
		elements.add(new ElementDescriptor(186, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_modulusValue(); }});
		elements.add(new ElementDescriptor(189, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_otasp_ResultCode(); }});
		elements.add(new ElementDescriptor(190, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_primitiveValue(); }});
		elements.add(new ElementDescriptor(194, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionReport(); }});
		elements.add(new ElementDescriptor(156, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ssdUpdateReport(); }});
		elements.add(new ElementDescriptor(124, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_uniqueChallengeReport(); }});
		elements.add(new ElementDescriptor(196, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyReport(); }});
	}
	public OTASPRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
