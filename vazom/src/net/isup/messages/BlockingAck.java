package net.isup.messages;

import net.isup.MessageType;

public class BlockingAck extends MessageBase {
	public BlockingAck(){
		super();
		type = MessageType.BLOCKING_ACKNOWLEDGEMENT;
	}
	public void init(byte[] data) {
		// no data
	}

}
