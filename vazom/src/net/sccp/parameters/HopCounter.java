package net.sccp.parameters;

public class HopCounter extends ParamBase {
	public int value;
	public HopCounter(){
		super();
		type = ParamType.HOP_COUNTER;
	}
	
	public void init(byte[] data) {
		value = data[0] & 0xFF;
	}

}
