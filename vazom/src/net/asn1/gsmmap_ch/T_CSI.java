package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class T_CSI extends SEQUENCE{

	public T_BcsmCamelTDPDataList get_t_BcsmCamelTDPDataList(){
		return (T_BcsmCamelTDPDataList)elements.get(0).data;
	}
	public T_BcsmCamelTDPDataList new_t_BcsmCamelTDPDataList(){
		return new T_BcsmCamelTDPDataList();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(1).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public CamelCapabilityHandling get_camelCapabilityHandling(){
		return (CamelCapabilityHandling)elements.get(2).data;
	}
	public CamelCapabilityHandling new_camelCapabilityHandling(){
		return new CamelCapabilityHandling();
	}

	public NULL get_notificationToCSE(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_notificationToCSE(){
		return new NULL();
	}

	public NULL get_csi_Active(){
		return (NULL)elements.get(4).data;
	}
	public NULL new_csi_Active(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_t_BcsmCamelTDPDataList(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_camelCapabilityHandling(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_notificationToCSE(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_csi_Active(); }});
	}
	public T_CSI(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
