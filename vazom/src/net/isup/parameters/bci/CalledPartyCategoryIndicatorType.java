package net.isup.parameters.bci;

import java.util.HashMap;

public enum CalledPartyCategoryIndicatorType {
	NO_INDICATION(0x00),
	ORDINARY_SUBSCRIBER(0x10),
	PAYPHONE(0x20);
	
	private int id;
	private static final HashMap<Integer, CalledPartyCategoryIndicatorType> lookup = new HashMap<Integer, CalledPartyCategoryIndicatorType>();
	static{
		for(CalledPartyCategoryIndicatorType td : CalledPartyCategoryIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static CalledPartyCategoryIndicatorType get(int id){ return lookup.get(id); }
	private CalledPartyCategoryIndicatorType(int _id){ id = _id; }
}
