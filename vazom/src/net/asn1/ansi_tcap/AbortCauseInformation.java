package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AbortCauseInformation extends CHOICE{

	public P_Abort_cause get_abortCause(){
		return (P_Abort_cause)elements.get(0).data;
	}
	public P_Abort_cause new_abortCause(){
		return new P_Abort_cause();
	}

	public UserAbortInformation get_userInformation(){
		return (UserAbortInformation)elements.get(1).data;
	}
	public UserAbortInformation new_userInformation(){
		return new UserAbortInformation();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(23, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_abortCause(); }});
		elements.add(new ElementDescriptor(24, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_userInformation(); }});
	}
	public AbortCauseInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
