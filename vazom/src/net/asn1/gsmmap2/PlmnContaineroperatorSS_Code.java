package net.asn1.gsmmap2;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PlmnContaineroperatorSS_Code extends SEQUENCE{

	public OCTET_STRING new_child(){
		return new OCTET_STRING();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public OCTET_STRING getChild(int index){
		return (OCTET_STRING)of_children.get(index);
	}
	public PlmnContaineroperatorSS_Code(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
