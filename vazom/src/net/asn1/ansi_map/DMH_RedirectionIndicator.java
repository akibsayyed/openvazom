package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DMH_RedirectionIndicator extends ENUMERATED{

	public static final int _not_specified = 0;
	public static final int _cfu = 1;
	public static final int _cfb = 2;
	public static final int _cfna = 3;
	public static final int _cfo = 4;
	public static final int _cd_Unspecified = 5;
	public static final int _cd_PSTN = 6;
	public static final int _cd_Private = 7;
	public static final int _pstn_Tandem = 8;
	public static final int _private = 9;
	public static final int _busy = 10;
	public static final int _inactive = 11;
	public static final int _unassigned = 12;
	public static final int _termination_denied = 13;
	public static final int _cd_failure = 14;
	public static final int _ect = 15;
	public static final int _mah = 16;
	public static final int _fa = 17;
	public static final int _abandoned_call_leg = 18;
	public static final int _pca_call_refused = 19;
	public static final int _sca_call_refused = 20;
	public static final int _dialogue = 21;
	public static final int _cfd = 22;
	public static final int _cd_local = 23;
	public static final int _voice_mail_retrieval = 24;
	public DMH_RedirectionIndicator(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
