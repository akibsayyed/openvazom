package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class FeatureResult extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _unsuccessful =  1 ;
	public static final int _successful =  2 ;
	public FeatureResult(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
