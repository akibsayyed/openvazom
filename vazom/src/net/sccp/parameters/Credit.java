package net.sccp.parameters;

public class Credit extends ParamBase {
	public int windowSize;
	
	public Credit(){
		super();
		type = ParamType.CREDIT;
	}
	public void init(byte[] data) {
		windowSize = data[0] & 0xFF;
	}

}
