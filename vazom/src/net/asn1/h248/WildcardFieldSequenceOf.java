package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class WildcardFieldSequenceOf extends SEQUENCE{

	public WildcardField new_child(){
		return new WildcardField();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public WildcardField getChild(int index){
		return (WildcardField)of_children.get(index);
	}
	public WildcardFieldSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
