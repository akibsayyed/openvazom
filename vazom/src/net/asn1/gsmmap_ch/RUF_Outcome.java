package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class RUF_Outcome extends ENUMERATED{

	public static final int _accepted = 0;
	public static final int _rejected = 1;
	public static final int _noResponseFromFreeMS = 2;
	public static final int _noResponseFromBusyMS = 3;
	public static final int _udubFromFreeMS = 4;
	public static final int _udubFromBusyMS = 5;
	public RUF_Outcome(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
