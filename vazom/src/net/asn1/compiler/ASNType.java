package net.asn1.compiler;

import java.util.ArrayList;
import java.util.Arrays;

import net.ber.BerLengthType;
import net.ber.BerTag;


public class ASNType {
	public ASNTag universalTag;
	public ASNTagClass asn_class;
	public ASNTagComplexity asn_pc;
	public int tag;
	public String name;
	public String type;
	public String def;
	public String constraint;
	public boolean optional;
	public boolean implicit;
	public boolean explicit;
	public boolean of_type;
	public boolean multi_line;
	public byte[] dataPacket;
	public byte[] value;
	
	// decoder
	public int length;
	public int byte_position;
	//public int lengthSize;
	//public BerLengthType lengthType;
	public BerTag berTag;
	
	// version 2
	public ASNType choiceSelection;
	public ArrayList<ASNType> of_children;
	public ArrayList<ElementDescriptor> elements;
	public ASNTagClass of_class;
	public ASNTagComplexity of_pc;
	public int of_tag;
	public void addChild(){}
	
	public ASNType(){
		asn_class = ASNTagClass.UNIVERSAL;
		// new, v2
		asn_pc = ASNTagComplexity.Unknown;
	}
	public void reset(){
		if(elements != null) elements.clear();
	}
	
	public ASNType(ASNTagComplexity _asn_pc, int _tag, ASNTagClass _asn_class){
		asn_class = _asn_class;
		asn_pc = _asn_pc;
		tag = _tag;
	}
	
	public byte[] getValueBytes(){
		return Arrays.copyOfRange(dataPacket, byte_position, byte_position + length);
		
	}
	public byte[] getDataBytes(){
		int s =  byte_position - berTag.lengthSize - 1;
		return Arrays.copyOfRange(dataPacket, s, s + length + berTag.lengthSize + 1);
		
	}
	public String getHex(){
		String res = "";
		for(int i = 0; i<length; i++) res += String.format("%02x", dataPacket[byte_position + i]);
		return res;
		
	}
}

