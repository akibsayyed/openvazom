package net.isup.parameters;

import net.isup.ParameterType;

public class PivotRoutingBackwardInformation extends ParameterBase {
	public PivotRoutingBackwardInformation(){
		type = ParameterType.PIVOT_ROUTING_BACKWARD_INFORMATION;
	}
}
