package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ActionRepliesSequenceOf extends SEQUENCE{

	public ActionReply new_child(){
		return new ActionReply();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public ActionReply getChild(int index){
		return (ActionReply)of_children.get(index);
	}
	public ActionRepliesSequenceOf(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
