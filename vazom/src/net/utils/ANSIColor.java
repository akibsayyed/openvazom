package net.utils;

public enum ANSIColor {
	WHITE,
	RED,
	GREEN,
	YELLOW,
	BLUE,
	BLACK,
	MAGENTA,
	CYAN,
	RESET;
}
