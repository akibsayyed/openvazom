package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class PrepareHO_ResOld extends SEQUENCE{

	public ISDN_AddressString get_handoverNumber(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_handoverNumber(){
		return new ISDN_AddressString();
	}

	public Bss_APDU get_bss_APDU(){
		return (Bss_APDU)elements.get(1).data;
	}
	public Bss_APDU new_bss_APDU(){
		return new Bss_APDU();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_handoverNumber(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_bss_APDU(); }});
	}
	public PrepareHO_ResOld(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
