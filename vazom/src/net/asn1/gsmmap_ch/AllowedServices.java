package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AllowedServices extends BIT_STRING{

	public static final int _firstServiceAllowed = 0;
	public static final int _secondServiceAllowed = 1;
	public AllowedServices(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 3;
	}
}
