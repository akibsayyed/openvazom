package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class TransactionID extends OCTET_STRING{

	public TransactionID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 7;
		asn_class = ASNTagClass.PRIVATE;
	}
}
