package net.isup.parameters;

import net.isup.ParameterType;

public class SuspendResumeIndicators extends ParameterBase {
	public SuspendResumeIndicators(){
		type = ParameterType.SUSPEND_RESUME_INDICATORS;
	}
}
