package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceManagementSystemGapInterval extends ENUMERATED{

	public static final int _not_used = 0;
	public ServiceManagementSystemGapInterval(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
