package net.sccp.messages;

import java.util.Arrays;

import net.sccp.MessageType;
import net.sccp.parameters.Dlr;

public class EA_ExpeditedDataAcknowledgment extends MessageBase {
	public Dlr destinationLocalReference;
	
	public EA_ExpeditedDataAcknowledgment(){
		super();
		type = MessageType.EA_EXPEDITED_DATA_ACKNOWLEDGMENT;
	}
	
	public void init(byte[] _data) {
		byte_pos++;
		
		// Fixed mandatory
		destinationLocalReference = new Dlr();
		destinationLocalReference.init(Arrays.copyOfRange(_data, byte_pos, byte_pos + 3));
		byte_pos += 3;
		
	}

}
