package net.logging;

import org.apache.log4j.Logger;

public class LoggingManager {
	public static Logger logger=Logger.getLogger(LoggingManager.class);

	public static void warn(Logger lg, String line){
		lg.warn(line);
	}	
	public static void error(Logger lg, String line){
		lg.error(line);
	}	
	public static void info(Logger lg, String line){
		lg.info(line);
	}	
	public static void debug(Logger lg, String line){
		lg.debug(line);
	}	
	public static void debug(boolean on_off, Logger lg, String line){
		lg.debug(line);
	}	
	
	public static void init(){
		logger.info("Starting Logging Manager...");
	}
}
