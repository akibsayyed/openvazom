package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.user_cause.CauseType;

public class UserCause extends M3UAParameter {
	public int user;
	public CauseType cause;
	
	public UserCause(){
		type = M3UAParameterType.USER_CAUSE;
	}

	public void init(byte[] data) {
		value = data;
		user = (data[0] << 8) + (data[1] & 0xFF);
		cause = CauseType.get((data[2] << 8) + (data[3] & 0xFF));
	}



}
