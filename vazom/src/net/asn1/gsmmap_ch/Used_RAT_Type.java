package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Used_RAT_Type extends ENUMERATED{

	public static final int _utran = 0;
	public static final int _geran = 1;
	public static final int _gan = 2;
	public static final int _i_hspa_evolution = 3;
	public static final int _e_utran = 4;
	public Used_RAT_Type(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
