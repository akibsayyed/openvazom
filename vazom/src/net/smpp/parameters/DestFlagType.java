package net.smpp.parameters;

import java.util.HashMap;
	
public enum DestFlagType {
	SME_ADDRESS(1),
	DLN(2);
	private int id;
	private static final HashMap<Integer, DestFlagType> lookup = new HashMap<Integer, DestFlagType>();
	static{
		for(DestFlagType td : DestFlagType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static DestFlagType get(int id){ return lookup.get(id); }
	private DestFlagType(int _id){ id = _id; }	
}
