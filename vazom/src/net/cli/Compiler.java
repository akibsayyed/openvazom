package net.cli;

import java.io.File;
import java.io.FileReader;
import java.io.InputStream;
import java.io.InputStreamReader;
import java.net.URI;
import java.net.URL;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;

public class Compiler {
	public static void main(String[] args) {
		try{
			InputStream is = ClassLoader.getSystemResourceAsStream("net/cli/CLIManager.st");
			InputStreamReader gr = new InputStreamReader(is);
			StringTemplateGroup templates = new StringTemplateGroup(gr);
			gr.close();
			if(args.length > 0){
				ANTLRFileStream input = new ANTLRFileStream(args[0], "UTF8");
				cliLexer lexer = new cliLexer(input);
				CommonTokenStream tokens = new CommonTokenStream(lexer);
				cliParser parser = new cliParser(tokens);
				cliParser.input_return r = parser.input();
				CommonTree t = (CommonTree)r.getTree();
				
				CommonTreeNodeStream nodes = new CommonTreeNodeStream(t);
				nodes.setTokenStream(tokens);
				cliwalker walker = new cliwalker(nodes);
			
				
				walker.setTemplateLib(templates);
				cliwalker.input_return wr = walker.input();
				
				StringTemplate output = (StringTemplate)wr.getTemplate();
				System.out.println(output.toString());				
			}else{
				System.out.println("Input file missing!");
			}

			
			

		}catch(Exception e){
			e.printStackTrace();
		}

	}

}
