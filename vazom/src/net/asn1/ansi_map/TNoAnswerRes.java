package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TNoAnswerRes extends SET{

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(0).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public ActionCode get_actionCode(){
		return (ActionCode)elements.get(1).data;
	}
	public ActionCode new_actionCode(){
		return new ActionCode();
	}

	public AnnouncementList get_announcementList(){
		return (AnnouncementList)elements.get(2).data;
	}
	public AnnouncementList new_announcementList(){
		return new AnnouncementList();
	}

	public CallingPartyNumberString1 get_callingPartyNumberString1(){
		return (CallingPartyNumberString1)elements.get(3).data;
	}
	public CallingPartyNumberString1 new_callingPartyNumberString1(){
		return new CallingPartyNumberString1();
	}

	public CallingPartyNumberString2 get_callingPartyNumberString2(){
		return (CallingPartyNumberString2)elements.get(4).data;
	}
	public CallingPartyNumberString2 new_callingPartyNumberString2(){
		return new CallingPartyNumberString2();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(5).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public DisplayText get_displayText(){
		return (DisplayText)elements.get(6).data;
	}
	public DisplayText new_displayText(){
		return new DisplayText();
	}

	public DMH_AccountCodeDigits get_dmh_AccountCodeDigits(){
		return (DMH_AccountCodeDigits)elements.get(7).data;
	}
	public DMH_AccountCodeDigits new_dmh_AccountCodeDigits(){
		return new DMH_AccountCodeDigits();
	}

	public DMH_AlternateBillingDigits get_dmh_AlternateBillingDigits(){
		return (DMH_AlternateBillingDigits)elements.get(8).data;
	}
	public DMH_AlternateBillingDigits new_dmh_AlternateBillingDigits(){
		return new DMH_AlternateBillingDigits();
	}

	public DMH_BillingDigits get_dmh_BillingDigits(){
		return (DMH_BillingDigits)elements.get(9).data;
	}
	public DMH_BillingDigits new_dmh_BillingDigits(){
		return new DMH_BillingDigits();
	}

	public DMH_RedirectionIndicator get_dmh_RedirectionIndicator(){
		return (DMH_RedirectionIndicator)elements.get(10).data;
	}
	public DMH_RedirectionIndicator new_dmh_RedirectionIndicator(){
		return new DMH_RedirectionIndicator();
	}

	public DMH_ServiceID get_dmh_ServiceID(){
		return (DMH_ServiceID)elements.get(11).data;
	}
	public DMH_ServiceID new_dmh_ServiceID(){
		return new DMH_ServiceID();
	}

	public GroupInformation get_groupInformation(){
		return (GroupInformation)elements.get(12).data;
	}
	public GroupInformation new_groupInformation(){
		return new GroupInformation();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(13).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PilotNumber get_pilotNumber(){
		return (PilotNumber)elements.get(14).data;
	}
	public PilotNumber new_pilotNumber(){
		return new PilotNumber();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(15).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public RedirectingNumberDigits get_redirectingNumberDigits(){
		return (RedirectingNumberDigits)elements.get(16).data;
	}
	public RedirectingNumberDigits new_redirectingNumberDigits(){
		return new RedirectingNumberDigits();
	}

	public ResumePIC get_resumePIC(){
		return (ResumePIC)elements.get(17).data;
	}
	public ResumePIC new_resumePIC(){
		return new ResumePIC();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(18).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public TerminationList get_terminationList(){
		return (TerminationList)elements.get(19).data;
	}
	public TerminationList new_terminationList(){
		return new TerminationList();
	}

	public TerminationTriggers get_terminationTriggers(){
		return (TerminationTriggers)elements.get(20).data;
	}
	public TerminationTriggers new_terminationTriggers(){
		return new TerminationTriggers();
	}

	public TriggerAddressList get_triggerAddressList(){
		return (TriggerAddressList)elements.get(21).data;
	}
	public TriggerAddressList new_triggerAddressList(){
		return new TriggerAddressList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(128, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_actionCode(); }});
		elements.add(new ElementDescriptor(130, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_announcementList(); }});
		elements.add(new ElementDescriptor(82, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString1(); }});
		elements.add(new ElementDescriptor(83, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberString2(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(244, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_displayText(); }});
		elements.add(new ElementDescriptor(140, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AccountCodeDigits(); }});
		elements.add(new ElementDescriptor(141, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_AlternateBillingDigits(); }});
		elements.add(new ElementDescriptor(142, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_BillingDigits(); }});
		elements.add(new ElementDescriptor(88, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_RedirectionIndicator(); }});
		elements.add(new ElementDescriptor(305, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dmh_ServiceID(); }});
		elements.add(new ElementDescriptor(163, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_groupInformation(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(168, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pilotNumber(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(100, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_redirectingNumberDigits(); }});
		elements.add(new ElementDescriptor(266, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_resumePIC(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(120, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationList(); }});
		elements.add(new ElementDescriptor(122, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationTriggers(); }});
		elements.add(new ElementDescriptor(276, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerAddressList(); }});
	}
	public TNoAnswerRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
