package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CCBS_Indicators extends SEQUENCE{

	public NULL get_ccbs_Possible(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_ccbs_Possible(){
		return new NULL();
	}

	public NULL get_keepCCBS_CallIndicator(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_keepCCBS_CallIndicator(){
		return new NULL();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(2).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ccbs_Possible(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_keepCCBS_CallIndicator(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public CCBS_Indicators(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
