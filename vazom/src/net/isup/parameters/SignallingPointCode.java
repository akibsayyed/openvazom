package net.isup.parameters;

import net.isup.ParameterType;

public class SignallingPointCode extends ParameterBase {
	public SignallingPointCode(){
		type = ParameterType.SIGNALING_POINT_CODE;
	}
}
