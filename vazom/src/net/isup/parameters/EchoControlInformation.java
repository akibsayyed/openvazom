package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.eci.IncomingECDeviceInformationINdicatorType;
import net.isup.parameters.eci.IncomingECDeviceRequestIndicatorType;
import net.isup.parameters.eci.OutgoingECDeviceInformationIndicatorType;
import net.isup.parameters.eci.OutgoingECDeviceRequestIndicatorType;

public class EchoControlInformation extends ParameterBase {
	public OutgoingECDeviceInformationIndicatorType outgoing_device_information_indicator;
	public IncomingECDeviceInformationINdicatorType incoming_device_information_indicator;
	public OutgoingECDeviceRequestIndicatorType outgoing_device_request_indicator;
	public IncomingECDeviceRequestIndicatorType incoming_device_request_indicator;
	
	public EchoControlInformation(){
		type = ParameterType.ECHO_CONTROL_INFORMATION;
	}
	public void init(byte[] data) {
		if(data != null){
			outgoing_device_information_indicator = OutgoingECDeviceInformationIndicatorType.get(data[byte_position] & 0x03);
			incoming_device_information_indicator = IncomingECDeviceInformationINdicatorType.get(data[byte_position] & 0x0c);
			outgoing_device_request_indicator = OutgoingECDeviceRequestIndicatorType.get(data[byte_position] & 0x30);
			incoming_device_request_indicator = IncomingECDeviceRequestIndicatorType.get(data[byte_position] & 0xc0);
		}
	}

}
