package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SMS_TransactionID extends OCTET_STRING{

	public SMS_TransactionID(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
