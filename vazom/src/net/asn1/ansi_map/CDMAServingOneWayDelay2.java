package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMAServingOneWayDelay2 extends OCTET_STRING{

	public CDMAServingOneWayDelay2(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
