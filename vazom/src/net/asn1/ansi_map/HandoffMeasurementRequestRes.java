package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffMeasurementRequestRes extends SET{

	public SignalQuality get_signalQuality(){
		return (SignalQuality)elements.get(0).data;
	}
	public SignalQuality new_signalQuality(){
		return new SignalQuality();
	}

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(1).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(11, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signalQuality(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_targetCellID(); }});
	}
	public HandoffMeasurementRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
