package net.smstpdu.tpdcs;

import net.smstpdu.tpdcs.general.Alphabet;
import net.smstpdu.tpdcs.store.IndicationType;

public class TPDCS_Store extends TPDCS {
	public boolean indication;
	public IndicationType indicationType;
	
	public TPDCS_Store(){
		super();
		group = TPDCSGroup.MESSAGE_WAITING_STORE;
	}
	public byte encode(){
		byte tmp_res = (byte)group.getId();
		tmp_res += (indication ? 0x08 : 0);
		tmp_res += indicationType.getId();
		return tmp_res;
	}
	public void init(byte data) {
		// bit 3
		indication = (data & 0x08) == 0x08;
		// bit 2 reserved
		// bits 1, 0
		indicationType = IndicationType.get(data & 0x03);
		
		encoding = Alphabet.DEFAULT;
		
		
	}
}
