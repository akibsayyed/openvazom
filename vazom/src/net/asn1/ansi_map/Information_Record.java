package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Information_Record extends OCTET_STRING{

	public Information_Record(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
