package net.db;

import net.sccp.NatureOfAddress;
import net.sccp.parameters.global_title.GlobalTitleIndicator;
import net.sccp.parameters.global_title.NumberingPlan;
import net.smstpdu.SmsDirection;
import net.smstpdu.SmsPduType;
import net.smstpdu.SmsType;
import net.utils.Utils;
import net.vstp.MessageDescriptor;

public class DBRecordSMS extends DBRecordBase{
	public SmsDirection direction;
	public SmsType type;
	public SmsPduType pdu_type;
	
	// -------- SMPP ------- 
	public long sequence_number;
	public String ip_source;
	public String ip_destination;
	public Integer tcp_source;
	public Integer tcp_destination;
	public String system_id;
	public int service_type;
	public String password;
	public int originator_np;
	public int recipient_np;
	public int esm_message_mode;
	public int esm_message_type;
	public int esm_gsm_features;
	public int protocol_id;
	public int priority_flag;
	public String delivery_time;
	public String validity_period;
	public int rd_smsc_receipt;
	public int rd_sme_ack;
	public int rd_intermediate_notification;
	public int replace_if_present;
	public int sm_default_msg_id;
	public int sm_msg_length;
	// ------------------
	
	// ****** SMS CONCATENATED ****
	public Integer sms_conc_partnum;
	public Integer sms_conc_parts;
	public Integer sms_conc_msgid;
	// **********************
	
	// *********** SS7 ***********
	public String gt_called;
	public String gt_calling;
	public int gt_called_tt;
	public int gt_calling_tt;
	public NatureOfAddress gt_called_nai;
	public NatureOfAddress gt_calling_nai;
	public NumberingPlan gt_called_np;
	public NumberingPlan gt_calling_np;
	public GlobalTitleIndicator gt_called_gti;
	public GlobalTitleIndicator gt_calling_gti;
	public String scda;
	public String scoa;
	public String imsi;
	public String msisdn;
	public int sms_destination_enc;
	public int sms_originating_enc;
	public int sms_text_enc;
	public String sms_destination;
	public String sms_originating;
	public int m3ua_data_dpc;
	public int m3ua_data_opc;
	// *******************
	public Long id;
	
	// SS7/SMPP common
	public String sms_text = "";
	
	
	// quarantine
	public boolean quarantine;
	
	public static DBRecordSMS VSTP_to_DBR(MessageDescriptor md){
		switch(md.header.msg_type){
			case SMPP_MO:
			case SMPP_MT: return Utils.convert_VSTP_SMPP2DBR(md);
			
			case SMS_MO:
			case SMS_MT: return Utils.convert_VSTP_TPDU2DBR(md);
		}

		return null;
	}
	
	
	public DBRecordSMS(){
		recordType = DBRecordType.SMS;
		pdu_type = SmsPduType.SMS_TPDU;
	}
}
