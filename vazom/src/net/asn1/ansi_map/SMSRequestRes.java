package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSRequestRes extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public SMS_AccessDeniedReason get_sms_AccessDeniedReason(){
		return (SMS_AccessDeniedReason)elements.get(2).data;
	}
	public SMS_AccessDeniedReason new_sms_AccessDeniedReason(){
		return new SMS_AccessDeniedReason();
	}

	public SMS_Address get_sms_Address(){
		return (SMS_Address)elements.get(3).data;
	}
	public SMS_Address new_sms_Address(){
		return new SMS_Address();
	}

	public SMS_CauseCode get_sms_CauseCode(){
		return (SMS_CauseCode)elements.get(4).data;
	}
	public SMS_CauseCode new_sms_CauseCode(){
		return new SMS_CauseCode();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(5).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(152, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_AccessDeniedReason(); }});
		elements.add(new ElementDescriptor(104, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_Address(); }});
		elements.add(new ElementDescriptor(153, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_CauseCode(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
	}
	public SMSRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
