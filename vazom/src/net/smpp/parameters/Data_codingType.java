package net.smpp.parameters;

import java.util.HashMap;


public enum Data_codingType {
	DEFAULT(0x00),
	IA5_ASCII(0x01),
	_8BIT_BINARY_1(0x02),
	ISO_8859_1(0x03),
	_8BIT_BINARY_2(0x04),
	JIS(0x05),
	ISO_8859_5(0x06),
	ISO_8859_8(0x07),
	UCS2(0x08),
	PICTOGRAM(0x09),
	ISO_2011_JP(0x0a),
	EXTENDED_KANJI(0x0d),
	KS_C_5601(0x0e);
	
	
	private int id;
	private static final HashMap<Integer, Data_codingType> lookup = new HashMap<Integer, Data_codingType>();
	static{
		for(Data_codingType td : Data_codingType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static Data_codingType get(int id){ return lookup.get(id); }
	private Data_codingType(int _id){ id = _id; }
}
