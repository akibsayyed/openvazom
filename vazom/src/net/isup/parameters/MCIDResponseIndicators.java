package net.isup.parameters;

import net.isup.ParameterType;

public class MCIDResponseIndicators extends ParameterBase {
	public MCIDResponseIndicators(){
		type = ParameterType.MCID_RESPONSE_INDICATOR;
	}
}
