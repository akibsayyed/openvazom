package net.test;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileOutputStream;
import java.lang.annotation.Annotation;

import net.ber.BerTranscoderv2;
import net.ds.DS_SPCAP_E1;
import net.fn.cli.CLIService;
import net.m3ua.M3UA;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.sccp.SCCP;
import net.sccp.messages.MessageBase;
import net.sccp.messages.UDT_UnitData;
import net.sgn.SGNode;
import net.smstpdu.GSMAlphabet;
import net.smstpdu.MessageDirection;
import net.smstpdu.TBCD;
import net.smstpdu.tpdu.SmsDeliver;
import net.smstpdu.tpdu.SmsSubmit;
import net.smstpdu.tpdu.TPDU;
import net.utils.Utils;

public class test14 {
	public static void main(String[] args) {
		
		net.fn.cli.CLIService.init(20000);
		
		
		
		try{
			Thread.sleep(200000);
		}catch(Exception e){}
		System.exit(0);
		File f = new File("/tmp/1302598460742.raw");
		byte[] data = new byte[(int)f.length()];
		int i = 0;
		try{
			FileInputStream fi = new FileInputStream(f);
			int a;
			while( (a = fi.read()) != -1){
				data[i++] = (byte)a;
			}
		}catch(Exception e){
			e.printStackTrace();
		}

		System.out.println(SGNode.class.getPackage().getImplementationVendor());
		System.out.println(SGNode.class.getPackage().getSpecificationVersion());
		System.out.println(SGNode.class.getPackage().getImplementationVersion());
		System.exit(0);
		
		M3UAPacket m3ua = M3UA.decode(data);
		DATA m3ua_data = (DATA)m3ua.message;
		MessageBase sccp_msg = SCCP.decode(m3ua_data.protocolData.userProtocolData);
		UDT_UnitData udt = (UDT_UnitData)sccp_msg;
		//System.out.println(Utils.bytes2hexstr(udt.data, " "));
		
		BerTranscoderv2 ber2 = new BerTranscoderv2();
		
		net.asn1.tcap2.TCMessage ntcm = (net.asn1.tcap2.TCMessage)ber2.decode(new net.asn1.tcap2.TCMessage(), udt.data);

		ber2.showTree(ntcm.berTag, 0);
		byte[] iid = Utils.get_INVOKE_ID(ntcm);
		System.out.println(Utils.bytes2num(iid));
		System.exit(0);
		
		
		
		
		/*
		boolean[] bits = Utils.int2bits(84, 7);
		for(int j = 0; j<bits.length; j++) System.out.println(bits[j]);
		
		System.exit(0);
*/
		TPDU tpdu = TPDU.decode(data, MessageDirection.MS_SC);
		SmsSubmit s = (SmsSubmit)tpdu;
		//SmsDeliver d = (SmsDeliver)tpdu;
		s.TP_UDL = 0;
		s.TP_VP = 0xff;
		//System.out.println(d.TP_SCTS.month);
		//System.out.println("AAA: " + Utils.semiOctetEncode(1));
		
		byte[] sms_enc = s.encode();
		File ooo = new File("/tmp/newenc.raw");
		try{
			FileOutputStream fo = new FileOutputStream(ooo);
			for(int k = 0; k<sms_enc.length; k++) fo.write(sms_enc[k]);
			fo.close();
		}catch(Exception e){
			e.printStackTrace();
		}		
		
		
		/*
		System.out.println("TP-VP : " + s.TP_VP);
		System.out.println(TBCD.decode(s.TP_DA.digits));
		
		byte[] enc = GSMAlphabet.encode("hello world", GSMAlphabet.getPrePaddingBits(0));
		//byte[] enc_2 = Utils.strhex2bytes("9c613539ed9aa741d3e614847a9341b019cc26bbc962b8940e1456874170799aec7e83c8e535bb0cd2a514ea39081e0691c720b8bc7c0eabc36d103c0c728741ed301be40e83c6ef36bc0ed2a514eb30089e9e97e965503b4c2fb7c3f4f4fa0dfa2950ceb7fd0d82cbd3a0e65b9ca697d97516c85e1e83dc611d084da7c375af57dbd57e8bd3");

		//System.out.println(GSMAlphabet.decodePadded(enc_2, 6));
		
		System.out.println("TEST = " + GSMAlphabet.decode(enc));
		
		System.out.println(Utils.bytes2hexstr(enc, " "));
		
		
		
		s.TP_VP = 0xff;
		byte[] sms_enc = s.encode();
		File ooo = new File("/tmp/newenc.raw");
		try{
			FileOutputStream fo = new FileOutputStream(ooo);
			for(int k = 0; k<sms_enc.length; k++) fo.write(sms_enc[k]);
			fo.close();
		}catch(Exception e){
			e.printStackTrace();
		}		
		*/
	}

}
