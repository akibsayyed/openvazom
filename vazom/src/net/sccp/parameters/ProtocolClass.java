package net.sccp.parameters;

import net.sccp.parameters.protocol_class.MessageHandling;
import net.sccp.parameters.protocol_class.ProtocolClassType;


public class ProtocolClass extends ParamBase {
	public ProtocolClassType protocolClass;
	public MessageHandling messageHandling;
	
	public ProtocolClass(){
		super();
		type = ParamType.PROTOCOL_CLASS;
	}
	public void init(byte[] data) {
		protocolClass = ProtocolClassType.get(data[0] & 0x0F);
		messageHandling = MessageHandling.get(data[0] & 0xF0);
	}

}
