package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMATargetMeasurementList extends SEQUENCE{

	public CDMATargetMeasurementInformationType new_child(){
		return new CDMATargetMeasurementInformationType();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public CDMATargetMeasurementInformationType getChild(int index){
		return (CDMATargetMeasurementInformationType)of_children.get(index);
	}
	public CDMATargetMeasurementList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
