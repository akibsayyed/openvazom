package net.ds;

import net.config.SGNConfigData;
import net.sgn.FNManager;
import net.spcap.SLLHeader;
import net.spcap.SPcap;

public class DS_SPCAP_E1 extends DSBase {
	int tmp_id;

	public DS_SPCAP_E1(DSDescriptor _dsd){
		super(_dsd);
		type = DataSourceType.SPCAP_E1;
		if(dsd.if_lst.length > 0){
			dsd.spcap_id = new int[dsd.if_lst.length];
			for(int i = 0; i<dsd.if_lst.length; i++){
				tmp_id = SPcap.initCapture(dsd.if_lst[i], SGNConfigData.spcap_snaplen, SGNConfigData.spcap_queue_max);
				if(tmp_id > -1){
					dsd.spcap_id[i] = tmp_id;
					logger.info("DS_SPCAP_E1: Starting capture for IF [" + dsd.if_lst[i] + "]!");
					if(dsd.bpf[i].trim().length() > 0){
						logger.info("DS_SPCAP_E1: Setting BPF = [" + dsd.bpf[i] + "] for IF = [" + dsd.if_lst[i] + "!]");
						SPcap.setBPF(dsd.bpf[i], tmp_id);
					}
					SPcap.startCapture(tmp_id);
				}else logger.warn("DS_SPCAP_E1: Error while starting capture for IF [" + dsd.if_lst[i] + "]!");
			}
			// start reader thread
			if(isReady()) reader_t.start();
			
		}else logger.error("DS_SPCAP_E1 requires at lest one IF for capture!");
	}

	public boolean isReady(){
		if(dsd.spcap_id != null){
			int tc = dsd.spcap_id.length;
			int err_c = 0;
			for(int i = 0; i<dsd.spcap_id.length; i++) if(dsd.spcap_id[i] <= 0) err_c++;
			return (err_c != tc);
		}else return false;
	}

	private void process(byte[] packet){
		DSPacket dsp = null;
		SLLHeader sll = null;
		if(packet != null){
			if(FNManager.fn_connected){
				// SLL(Linux cooked)
				sll = SPcap.getSLLHeader(packet);
				// payload
				if(sll != null) packet = sll.payload;
				if(packet != null){
					// DS Packet
					dsp = new DSPacket(DataSourceType.SPCAP_E1, packet);
					// send to queue
					if(queue.size() < SGNConfigData.sgn_global_max_queue_size) queue.offer(dsp);
					else{
						QUEUE_FULL++;
						//logger.warn("DS_SPCAP.queue: maximum queue size reached: [" + SGNConfigData.sgn_global_max_queue_size + "]!");
					}
				}

			}
		}

	}
	
	protected void reader_method(Object[] params) {
		byte[] packet = null;
		boolean found;

		found = false;
		// get packets
		for(int i = 0; i<dsd.spcap_id.length; i++) if(dsd.spcap_id[i] > -1){
			packet = SPcap.getPacket(dsd.spcap_id[i]);
			if(packet != null){
				found = true;
				process(packet);
			}
		}
		// pause if none found
		if(!found){
			try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
		}
		
	}

}
