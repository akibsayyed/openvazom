package net.smpp.pdu;

import net.smpp.NumberingPlan;
import net.smpp.PDUType;
import net.smpp.TypeOfNumber;
import net.utils.Utils;

public class Alert_notification extends PDUBase {
	public TypeOfNumber source_addr_ton;
	public NumberingPlan source_addr_npi;
	public String source_addr;
	public TypeOfNumber esme_addr_ton;
	public NumberingPlan esme_addr_npi;
	public String esme_addr;
	public byte[] ms_availability_status;

	public Alert_notification(){
		type = PDUType.ALERT_NOTIFICATION;
	}
	

	public void init(byte[] data) {
		// Source addr ton
		source_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// Source addr npi
		source_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// Source addr
		source_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += source_addr.length() + 1;

		// esme addr ton
		esme_addr_ton = TypeOfNumber.get(data[byte_pos++] & 0xff);
		// esme addr npi
		esme_addr_npi = NumberingPlan.get(data[byte_pos++] & 0xff);
		// esme addr
		esme_addr = Utils.Cstr_decode(data, byte_pos);
		byte_pos += esme_addr.length() + 1;
		
		// optional
		initOptional(data);


		
	}

}
