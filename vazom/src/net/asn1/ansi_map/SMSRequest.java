package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSRequest extends SET{

	public MSID get_msid(){
		return (MSID)elements.get(0).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(2).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public ServiceIndicator get_serviceIndicator(){
		return (ServiceIndicator)elements.get(3).data;
	}
	public ServiceIndicator new_serviceIndicator(){
		return new ServiceIndicator();
	}

	public SMS_NotificationIndicator get_sms_NotificationIndicator(){
		return (SMS_NotificationIndicator)elements.get(4).data;
	}
	public SMS_NotificationIndicator new_sms_NotificationIndicator(){
		return new SMS_NotificationIndicator();
	}

	public SMS_TeleserviceIdentifier get_sms_TeleserviceIdentifier(){
		return (SMS_TeleserviceIdentifier)elements.get(5).data;
	}
	public SMS_TeleserviceIdentifier new_sms_TeleserviceIdentifier(){
		return new SMS_TeleserviceIdentifier();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(193, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceIndicator(); }});
		elements.add(new ElementDescriptor(109, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_NotificationIndicator(); }});
		elements.add(new ElementDescriptor(116, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_TeleserviceIdentifier(); }});
	}
	public SMSRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
