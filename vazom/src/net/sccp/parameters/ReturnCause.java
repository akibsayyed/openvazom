package net.sccp.parameters;

import net.sccp.parameters.return_cause.ReturnCauseType;

public class ReturnCause extends ParamBase {
	public ReturnCauseType returnCause;
	
	public ReturnCause(){
		super();
		type = ParamType.RETURN_CAUSE;
	}

	
	public void init(byte[] data) {
		returnCause = ReturnCauseType.get(data[0] & 0xFF);
	}

}
