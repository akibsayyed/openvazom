package net.isup.parameters.eci;

import java.util.HashMap;

public enum IncomingECDeviceInformationINdicatorType {
	NO_INFORMATION(0x00),
	NOT_INCLUDED_NOT_AVAILABLE(0x04),
	INCLUDED(0x08),
	NOT_INCLUDED_AVAILABLE(0x0c);

	
	
	private int id;
	private static final HashMap<Integer, IncomingECDeviceInformationINdicatorType> lookup = new HashMap<Integer, IncomingECDeviceInformationINdicatorType>();
	static{
		for(IncomingECDeviceInformationINdicatorType td :IncomingECDeviceInformationINdicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static IncomingECDeviceInformationINdicatorType get(int id){ return lookup.get(id); }
	private IncomingECDeviceInformationINdicatorType(int _id){ id = _id; }		
}
