package net.isup.parameters.fci;

import java.util.HashMap;


public enum NationalInternationlCallIndicatorType {

	TREATED_AS_NATIONAL(0x00),
	TREATED_AS_INTERNATIONAL(0x01);
	
	
	private int id;
	private static final HashMap<Integer, NationalInternationlCallIndicatorType> lookup = new HashMap<Integer, NationalInternationlCallIndicatorType>();
	static{
		for(NationalInternationlCallIndicatorType td :NationalInternationlCallIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NationalInternationlCallIndicatorType get(int id){ return lookup.get(id); }
	private NationalInternationlCallIndicatorType(int _id){ id = _id; }		
}
