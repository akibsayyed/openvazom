package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;

public class Delivery_failure_reason extends OptionalParameterBase {
	public enum ValueType{
		DESTINATION_UNAVAILABLE(0x00),
		DESTINATION_ADDRESS_INVALID(0x01),
		PERMANENT_NETWORK_ERROR(0x02),
		TEMPORARY_NETWORK_ERROR(0x03);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}	
	public Delivery_failure_reason(){
		type = OptionalParameterType.DELIVERY_FAILURE_REASON;
	}
	public ValueType value;
	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
