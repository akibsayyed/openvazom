package net.smpp.parameters.optional;

import java.util.HashMap;

import net.smpp.OptionalParameterType;


public class Display_time extends OptionalParameterBase {
	public enum ValueType{
		TEMPORARY(0x00),
		DEFAULT(0x01),
		INVOKE(0x02);
		private int id;
		private static final HashMap<Integer, ValueType> lookup = new HashMap<Integer, ValueType>();
		static{
			for(ValueType td : ValueType.values()){
				lookup.put(td.id, td);
			}
		}
		public int getId(){ return id; }
		public static ValueType get(int id){ return lookup.get(id); }
		private ValueType(int _id){ id = _id; }			
	}
	public Display_time(){
		type = OptionalParameterType.DISPLAY_TIME;
	}
	public ValueType value;
	public void init(byte[] data) {
		value = ValueType.get(data[0] & 0xff);
	}

}
