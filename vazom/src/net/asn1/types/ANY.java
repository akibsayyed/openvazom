package net.asn1.types;

import java.util.ArrayList;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNTagComplexity;
import net.asn1.compiler.ASNType;

public class ANY extends ASNType{
	public ArrayList<ASNType> children;

	public ANY(){
		super();
		universalTag = ASNTag.ANY;
		tag = ASNTag.name2Tag(universalTag.toString());
		asn_pc = ASNTagComplexity.Unknown;
		
	}
}
