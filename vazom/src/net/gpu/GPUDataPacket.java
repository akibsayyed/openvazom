package net.gpu;

public class GPUDataPacket {
	public int lst_id;
	public int action_type;
	public int action_param;
	public String data;
	public byte[] hash_data;
	public NodeType type;
}
