package net.flood;
import java.io.BufferedReader;
import java.io.File;
import java.io.FileInputStream;
import java.io.InputStreamReader;
import java.io.PrintWriter;
import java.util.concurrent.ConcurrentHashMap;
import java.util.concurrent.ConcurrentLinkedQueue;

import net.config.FNConfigData;
import net.db.DBManager;
import net.dr.DRManager;
import net.smsfs.FilterType;
import net.vstp.MessageType;
import org.apache.log4j.Logger;

public class FloodManager {
	public static Logger logger=Logger.getLogger(FloodManager.class);
	
	public static ConcurrentHashMap<String, FloodItem> flood_map;
	private static ConcurrentHashMap<String, Long> tmp_flood_map;
	public static ConcurrentHashMap<String, FloodItem> flood_max_map;
	private static ConcurrentLinkedQueue<String> update_queue;
	private static boolean stopping;
	private static FloodGlobalDescriptor globalDescriptor;
	
	private static Calculator calc_r;
	private static Thread calc_t;
	private static Updater updater_r;
	private static Thread updater_t;
	
	
	private static void processGlobalHourDay(){
		long tmp;
		// global mo hourly
		if(globalDescriptor.tmp_mo_minute.size() >= 60){
			tmp = 0;
			for(int i = 0; i<globalDescriptor.tmp_mo_minute.size(); i++) tmp += globalDescriptor.tmp_mo_minute.get(i);
			globalDescriptor.CURRENT_MO_HOUR = tmp;
			globalDescriptor.tmp_mo_hour.add(tmp);
			globalDescriptor.tmp_mo_minute.clear();
			
		}
		// global mo daily
		if(globalDescriptor.tmp_mo_hour.size() >= 24){
			tmp = 0;
			for(int i = 0; i<globalDescriptor.tmp_mo_hour.size(); i++) tmp += globalDescriptor.tmp_mo_hour.get(i);
			globalDescriptor.CURRENT_MO_DAY = tmp;
			globalDescriptor.tmp_mo_hour.clear();
			
		}
		
		
		// global mt hourly
		if(globalDescriptor.tmp_mt_minute.size() >= 60){
			tmp = 0;
			for(int i = 0; i<globalDescriptor.tmp_mt_minute.size(); i++) tmp += globalDescriptor.tmp_mt_minute.get(i);
			globalDescriptor.CURRENT_MT_HOUR = tmp;
			globalDescriptor.tmp_mt_hour.add(tmp);
			globalDescriptor.tmp_mt_minute.clear();
			
		}
		// global mt daily
		if(globalDescriptor.tmp_mt_hour.size() >= 24){
			tmp = 0;
			for(int i = 0; i<globalDescriptor.tmp_mt_hour.size(); i++) tmp += globalDescriptor.tmp_mt_hour.get(i);
			globalDescriptor.CURRENT_MT_DAY = tmp;
			globalDescriptor.tmp_mt_hour.clear();
			
		}
		
		
		
	}
	
	private static void processHourDay(FloodItem fi){
		long tmp;
		// calculate hourly
		if(fi.tmp_minute.size() >= 60){
			tmp = 0;
			for(int i = 0; i<fi.tmp_minute.size(); i++) tmp += fi.tmp_minute.get(i);
			fi.hour = tmp;
			// add tmp hour
			fi.tmp_hour.add(fi.hour);
			// reset
			fi.tmp_minute.clear();
		}
		
		// calculate daily
		if(fi.tmp_hour.size() >= 24){
			tmp = 0;
			for(int i = 0; i<fi.tmp_hour.size(); i++) tmp += fi.tmp_hour.get(i);
			fi.day = tmp;
			// reset
			fi.tmp_hour.clear();
		}
		
		
	}
	private static class Updater implements Runnable{
		public void run() {
			String tmp;
			Long l;
			logger.info("Starting...");
			while(!stopping){
				tmp = update_queue.poll();
				if(tmp != null){
					l = tmp_flood_map.get(tmp);
					if(l != null){
						l++;
						tmp_flood_map.put(tmp, l);
					}else{
						if(tmp_flood_map.size() < FNConfigData.flood_max_list_size){
							tmp_flood_map.put(tmp, 1L);
						}
					}
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }
					
				}
			}
			logger.info("Ending...");
			
		}
		
	}
	
	private static class Calculator implements Runnable{
		FloodItem fi = null;
		public void run() {
			logger.info("Starting...");
			while(!stopping){
				try{ Thread.sleep(60000); }catch(Exception e){ e.printStackTrace(); }
				//logger.debug("Calculating flood: " + System.currentTimeMillis());
				//logger.debug("TMP MO: " + globalDescriptor.TMP_MO);
				//logger.debug("TMP MT: " + globalDescriptor.TMP_MT);
				//logger.debug("DB: " + DBManager.TEST);
				
				// globals
				globalDescriptor.tmp_mo_minute.add(globalDescriptor.TMP_MO);
				globalDescriptor.CURRENT_MO_MINUTE = globalDescriptor.TMP_MO;
				globalDescriptor.TMP_MO = 0;
				globalDescriptor.tmp_mt_minute.add(globalDescriptor.TMP_MT);
				globalDescriptor.CURRENT_MT_MINUTE = globalDescriptor.TMP_MT;
				globalDescriptor.TMP_MT = 0;

				//logger.debug("Calculating flood start: " + System.currentTimeMillis());
				//logger.debug("TMP MO: " + globalDescriptor.TMP_MO);
				//logger.debug("CURRENT_MO_MINUTE: " + globalDescriptor.CURRENT_MO_MINUTE);
				//logger.debug("TMP MT: " + globalDescriptor.TMP_MT);
				//logger.debug("CURRENT_MT_MINUTE: " + globalDescriptor.CURRENT_MT_MINUTE);

				
				processGlobalHourDay();
				
				for(String s : tmp_flood_map.keySet()){
					fi = flood_map.get(s);
					if(fi == null){
						if(flood_map.size() < FNConfigData.flood_max_list_size){
							fi = new FloodItem();
							fi.item = s;
							fi.minute = tmp_flood_map.get(s);
							fi.tmp_minute.add(fi.minute);
							flood_map.put(s, fi);
						}
					}else{
						fi.minute = tmp_flood_map.get(s);
						fi.tmp_minute.add(fi.minute);
						// hour/day stats
						processHourDay(fi);
					}
				}
				// clear tmp map
				tmp_flood_map.clear();
				//logger.debug("Calculating flood end: " + System.currentTimeMillis());
				
			}
			logger.info("Ending...");

		}
		
	}
	public static void decItem(String item, MessageType mt){
		Long item_val = tmp_flood_map.get(mt + ":" + item);
		if(item_val != null){
			item_val--;
			tmp_flood_map.put(mt + ":" + item, item_val);
		}
	}
	
	public static void calcDec(MessageType mt){
		switch(mt){
			case SMS_MO:
			case SMPP_MO:
				globalDescriptor.TMP_MO--;
				break;
			case SMPP_MT:
			case SMS_MT:
				globalDescriptor.TMP_MT--;
				break;
		}
	
	}

	public static void calcInc(String item, MessageType mt, boolean update_global){
		// queue
		update_queue.offer(mt + ":" + item);
		
		
		// global
		if(update_global){
			switch(mt){
				case SMS_MO:
				case SMPP_MO:
					globalDescriptor.TMP_MO++;
					break;
				case SMPP_MT:
				case SMS_MT:
					globalDescriptor.TMP_MT++;
					break;
			}
			
		}
	}
	
	public static void remove(String item){
		flood_map.remove(item);
	}
	public static boolean exists(String item){
		return flood_map.get(item) != null;
	}
	
	public static FloodItem get(String item, MessageType mt){
		return flood_map.get(mt.toString() + ":" + item);
	}
	public static FloodItem get(String item, FilterType ft){
		String prefix = "";
		switch(ft){
			case MO: prefix = "SMS_MO"; break;
			case MT: prefix = "SMS_MT"; break;
			case SMPP_MO: prefix = "SMS_MO"; break;
			case SMPP_MT: prefix = "SMS_MT"; break;
		}
		return flood_map.get(prefix + ":" + item);
	}
	public static FloodItem get_max(String item, MessageType mt){
		return flood_max_map.get(mt.toString() + ":" + item);
	}
	public static FloodItem get_max(String item, FilterType ft){
		String prefix = "";
		switch(ft){
			case MO: prefix = "SMS_MO"; break;
			case MT: prefix = "SMS_MT"; break;
			case SMPP_MO: prefix = "SMS_MO"; break;
			case SMPP_MT: prefix = "SMS_MT"; break;
		}
		return flood_max_map.get(prefix + ":" + item);
	}
	
	public static long get_global_current(GlobalMaxType type){
		switch(type){
			case GLOBAL_MO_MINUTE: return globalDescriptor.CURRENT_MO_MINUTE;
			case GLOBAL_MO_HOUR: return globalDescriptor.CURRENT_MO_HOUR;
			case GLOBAL_MO_DAY: return globalDescriptor.CURRENT_MO_DAY;
			
			case GLOBAL_MT_MINUTE: return globalDescriptor.CURRENT_MT_MINUTE;
			case GLOBAL_MT_HOUR: return globalDescriptor.CURRENT_MT_HOUR;
			case GLOBAL_MT_DAY: return globalDescriptor.CURRENT_MT_DAY;
			
		}
		return 0;
	}

	public static long get_global_max(GlobalMaxType type){
		switch(type){
			case GLOBAL_MO_MINUTE: return globalDescriptor.MAX_MO_MINUTE;
			case GLOBAL_MO_HOUR: return globalDescriptor.MAX_MO_HOUR;
			case GLOBAL_MO_DAY: return globalDescriptor.MAX_MO_DAY;
			
			case GLOBAL_MT_MINUTE: return globalDescriptor.MAX_MT_MINUTE;
			case GLOBAL_MT_HOUR: return globalDescriptor.MAX_MT_HOUR;
			case GLOBAL_MT_DAY: return globalDescriptor.MAX_MT_DAY;
			
			case GLOBAL_ALL_DEST_MO_MINUTE: return globalDescriptor.MAX_ALL_MO_MINUTE;
			case GLOBAL_ALL_DEST_MO_HOUR: return globalDescriptor.MAX_ALL_MO_HOUR;
			case GLOBAL_ALL_DEST_MO_DAY: return globalDescriptor.MAX_ALL_MO_DAY;
			
			case GLOBAL_ALL_DEST_MT_MINUTE: return globalDescriptor.MAX_ALL_MT_MINUTE;
			case GLOBAL_ALL_DEST_MT_HOUR: return globalDescriptor.MAX_ALL_MT_HOUR;
			case GLOBAL_ALL_DEST_MT_DAY: return globalDescriptor.MAX_ALL_MT_DAY;
			
			
		}
		return 0;
	}
	
	public static void add(String item){
		FloodItem fi = new FloodItem();
		fi.item = item;
		flood_map.put(item, fi);
	}
	
	public static void save(String fname){
		File f = new File(fname);
		long mo_m_max = 0;
		long mo_h_max = 0;
		long mo_d_max = 0;
		
		long mt_m_max = 0;
		long mt_h_max = 0;
		long mt_d_max = 0;
		FloodItem fi = null;
		String tmp = null;
		String[] item_lst = null;
		try{
			PrintWriter pw = new PrintWriter(f);
			// global
			pw.println("# global");
			pw.println("global.mo = " + (globalDescriptor.CURRENT_MO_MINUTE == 9223372036854775807L ? "*" : globalDescriptor.CURRENT_MO_MINUTE) + ":" + 
										(globalDescriptor.CURRENT_MO_HOUR == 9223372036854775807L ? "*" : globalDescriptor.CURRENT_MO_HOUR) + ":" + 
										(globalDescriptor.CURRENT_MO_DAY == 9223372036854775807L ? "*" : globalDescriptor.CURRENT_MO_DAY));
			
			pw.println("global.mt = " + (globalDescriptor.CURRENT_MT_MINUTE == 9223372036854775807L ? "*" : globalDescriptor.CURRENT_MT_MINUTE) + ":" + 
										(globalDescriptor.CURRENT_MT_HOUR == 9223372036854775807L ? "*" : globalDescriptor.CURRENT_MT_HOUR) + ":" + 
										(globalDescriptor.CURRENT_MT_DAY == 9223372036854775807L ? "*" : globalDescriptor.CURRENT_MT_DAY));

			pw.println();
			pw.println("# global per destination");
			// find max
			for(String s : flood_map.keySet()){
				fi = flood_map.get(s);
				if(fi != null){
					item_lst = s.split(":");
					if(item_lst[0].equalsIgnoreCase(MessageType.SMS_MO.toString())){
						if(fi.minute > mo_m_max) mo_m_max = fi.minute;
						if(fi.hour > mo_h_max) mo_h_max = fi.hour;
						if(fi.day > mo_d_max) mo_d_max = fi.day;
						
					}else if(item_lst[0].equalsIgnoreCase(MessageType.SMS_MT.toString())){
						if(fi.minute > mt_m_max) mt_m_max = fi.minute;
						if(fi.hour > mt_h_max) mt_h_max = fi.hour;
						if(fi.day > mt_d_max) mt_d_max = fi.day;
						
					}
					
				}
			}
			
			
			pw.println("*.mo = " + 	(mo_m_max == 0 ? "*" : mo_m_max) + ":" + 
									(mo_h_max == 0 ? "*" : mo_h_max) + ":" + 
									(mo_d_max == 0 ? "*" : mo_d_max));
			
			pw.println("*.mt = " + 	(mt_m_max == 0 ? "*" : mt_m_max) + ":" + 
									(mt_h_max == 0 ? "*" : mt_h_max) + ":" + 
									(mt_d_max == 0 ? "*" : mt_d_max));

			pw.println();
			pw.println("# destination");
			for(String s : flood_map.keySet()){
				fi = flood_map.get(s);
				if(fi != null){
					tmp = 	(fi.minute == 9223372036854775807L ? "*" : Long.toString(fi.minute)) + ":" +
							(fi.hour == 9223372036854775807L ? "*" : Long.toString(fi.hour)) + ":" +
							(fi.day == 9223372036854775807L ? "*" : Long.toString(fi.day));
					item_lst = s.split(":");
					if(item_lst[0].equalsIgnoreCase(MessageType.SMS_MO.toString())){
						pw.println("dst." + item_lst[1] + ".mo = " + tmp);
						
					}else if(item_lst[0].equalsIgnoreCase(MessageType.SMS_MT.toString())){
						pw.println("dst." + item_lst[1] + ".mt = " + tmp);
						
					}
					
				}
			}
			pw.close();
			
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	private static void init_max(){
		File f = new File("conf/flood.properties");
		String line = null;
		String[] tmp_lst = null;
		String[] item_lst = null;
		FloodItem fi = null;
		if(f.exists()){
			try{
				BufferedReader props_reader = new BufferedReader(new InputStreamReader(new FileInputStream(f)));
				while((line = props_reader.readLine()) != null){
					tmp_lst = line.split("=");
					if(tmp_lst.length > 1){
						if(tmp_lst[0].toUpperCase().trim().startsWith("GLOBAL.")){
							item_lst = tmp_lst[0].trim().split("\\.");
							if(item_lst[1].trim().equalsIgnoreCase("MO")){
								tmp_lst = tmp_lst[1].split(":");
								globalDescriptor.MAX_MO_MINUTE = (tmp_lst[0].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[0].trim()));
								globalDescriptor.MAX_MO_HOUR = (tmp_lst[1].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[1].trim()));
								globalDescriptor.MAX_MO_DAY = (tmp_lst[2].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[2].trim()));
								
							}else if(item_lst[1].trim().equalsIgnoreCase("MT")){
								tmp_lst = tmp_lst[1].split(":");
								globalDescriptor.MAX_MT_MINUTE = (tmp_lst[0].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[0].trim()));
								globalDescriptor.MAX_MT_HOUR = (tmp_lst[1].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[1].trim()));
								globalDescriptor.MAX_MT_DAY = (tmp_lst[2].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[2].trim()));
								
							}
							
						}else if(tmp_lst[0].trim().startsWith("*.")){
							item_lst = tmp_lst[0].trim().split("\\.");
							if(item_lst[1].trim().equalsIgnoreCase("MO")){
								tmp_lst = tmp_lst[1].split(":");
								globalDescriptor.MAX_ALL_MO_MINUTE = (tmp_lst[0].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[0].trim()));
								globalDescriptor.MAX_ALL_MO_HOUR = (tmp_lst[1].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[1].trim()));
								globalDescriptor.MAX_ALL_MO_DAY = (tmp_lst[2].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[2].trim()));
								
							}else if(item_lst[1].trim().equalsIgnoreCase("MT")){
								tmp_lst = tmp_lst[1].split(":");
								globalDescriptor.MAX_ALL_MT_MINUTE = (tmp_lst[0].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[0].trim()));
								globalDescriptor.MAX_ALL_MT_HOUR = (tmp_lst[1].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[1].trim()));
								globalDescriptor.MAX_ALL_MT_DAY = (tmp_lst[2].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[2].trim()));
							
							}
						}else if(tmp_lst[0].toUpperCase().trim().startsWith("DST.")){
							fi = new FloodItem();
							item_lst = tmp_lst[0].trim().split("\\.");
							fi.item = item_lst[1];
							tmp_lst = tmp_lst[1].split(":");

							// minute:hour:day
							fi.minute = (tmp_lst[0].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[0].trim()));
							fi.hour = (tmp_lst[1].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[1].trim()));
							fi.day = (tmp_lst[2].trim().equals("*") ? 9223372036854775807L : Long.parseLong(tmp_lst[2].trim()));
							// add to map
							if(item_lst[2].trim().equalsIgnoreCase("MO")){
								flood_max_map.put(MessageType.SMS_MO + ":" + fi.item, fi);
							}else if(item_lst[2].trim().equalsIgnoreCase("MT")){
								flood_max_map.put(MessageType.SMS_MT + ":" + fi.item, fi);
								
							}
							
						}
					}
				}
				props_reader.close();
			}catch(Exception e){
				e.printStackTrace();
			}
		}else{
			logger.warn("Flood detection definition config file does not exist!");
		}
	}
	
	public static void init(){
		logger.info("Starting Flood Manager...");
		flood_map = new ConcurrentHashMap<String, FloodItem>();
		tmp_flood_map = new ConcurrentHashMap<String, Long>();
		flood_max_map = new ConcurrentHashMap<String, FloodItem>();
		update_queue = new ConcurrentLinkedQueue<String>();
		globalDescriptor = new FloodGlobalDescriptor();

		init_max();
		
		calc_r = new Calculator();
		calc_t = new Thread(calc_r, "FLOOD_CALCULATOR");
		calc_t.start();
		
		updater_r = new Updater();
		updater_t = new Thread(updater_r, "FLOOD_UPDATER");
		updater_t.start();
		
	}

}
