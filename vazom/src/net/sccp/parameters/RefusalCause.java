package net.sccp.parameters;

import net.sccp.parameters.refusal_cause.RefusalCauseType;

public class RefusalCause extends ParamBase{
	public RefusalCauseType refusalCause;
	
	public RefusalCause(){
		super();
		type = ParamType.REFUSAL_CAUSE;
	}
	
	public void init(byte[] data) {
		refusalCause = RefusalCauseType.get(data[0] & 0xFF);
	}

}
