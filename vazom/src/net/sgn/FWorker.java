package net.sgn;

import java.util.ArrayList;

import net.asn1.tcap2.TCMessage;
import net.ber.BerTranscoderv2;
import net.config.SGNConfigData;
import net.hlr.HLRManager;
import net.hlr.HLRPacket;
import net.hlr.HLR_REQ_CALLBACK;
import net.hplmnr.HPLMNRPacket;
import net.logging.LoggingManager;
import net.m3ua.M3UAPacket;
import net.m3ua.messages.DATA;
import net.m3ua_conn.M3UAPayload;
import net.mtp3.MTP3Packet;
import net.mtp3.messages.normal.SCCPDataMessage;
import net.routing.RoutingConnection;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingManager;
import net.routing.RoutingConnectionType;
import net.sccp.messages.UDT_UnitData;
import net.sctp.SSCTPDescriptor;
import net.security.SecurityManager;
import net.sgc.SGCManager;
import net.smpp.pdu.PDUBase;
import net.smpp2ss7.SMPP2SS7Manager;
import net.smpp_conn.ProxyCorrelation;
import net.smpp_conn.SMPPCallback;
import net.smpp_conn.SMPPConnManager;
import net.smpp_conn.SMPPPacket;
import net.smpp_conn.SMPPPayload;
import net.smsfs.bindings.BindingDescriptor;
import net.smsfs.bindings.Bindings;
import net.smstpdu.tpdu.TPDU;
import net.utils.Utils;
import net.vstp.MessageDescriptor;
import net.vstp.MessageType;
import net.vstp.VSTPDataItemType;
import net.vstp.VSTP_SGF_CorrelationPacket;
import net.vstp.VSTP_SGF_CorrelationManager;

import org.apache.log4j.Logger;

public class FWorker {
	public static Logger logger=Logger.getLogger(FWorker.class);
	public int id;
	private Worker_r worker_r;
	private Thread worker_t;
	private boolean stopping;
	private long last_callback_tcap_id;
	
	public void processModifiers(String modifiers, int start_index, MessageDescriptor md, VSTP_SGF_CorrelationPacket vsd){
		String[] tmp_lst = null;
		BindingDescriptor tmp_dsc = null;
		MessageDescriptor md_new = null;
		// HLR modifiers
		tmp_lst = modifiers.split(":");
		//System.out.println("Modifiers: " + modifiers);
		//System.out.println(new String(md.encode()));
		
		if(start_index < tmp_lst.length){
			// HLR modifiers
			if(md.header.msg_type == MessageType.HLR){
				// get list of modifer fields
				for(int i = start_index; i<tmp_lst.length; i++){
					// get descriptor
					tmp_dsc = Bindings.get_hlr(tmp_lst[i]);
					if(tmp_dsc != null){
						// modifier method
						tmp_dsc.modify(vsd.sri_packet, md.values.get(tmp_lst[i]));
					}
				}
			// SMS TPDU modifiers
			}else if(md.header.msg_type == MessageType.SMS_MO || md.header.msg_type == MessageType.SMS_MT){
				// get list of modifer fields
				for(int i = start_index; i<tmp_lst.length; i++){
					if(tmp_lst[i].equalsIgnoreCase(VSTPDataItemType.CONVERT_SMPP.getId())){

						
						//TODO
						// converted to SMPP, exit SS7 conversion loop
						vsd.type = RoutingConnectionType.SMPP;
						vsd.smpp_packet = new SMPPPacket();
						vsd.smpp_packet_data = null;

						
						// process oher modifiers
						processModifiers(modifiers, i+1, md, vsd);
						break;
						
					
					}else{
						// get descriptor
						//System.out.println("GETTING DESC FOR: " + tmp_lst[i]);
						tmp_dsc = Bindings.get(tmp_lst[i]);
						if(tmp_dsc != null){
							// modifier method
							tmp_dsc.modify(vsd.sri_packet, md.values.get(tmp_lst[i]));
							
						}
						
					}							
				}
			// SMPP modifiers
			}else if(md.header.msg_type == MessageType.SMPP_MO || md.header.msg_type == MessageType.SMPP_MT){
				// get list of modifer fields
				for(int i = start_index; i<tmp_lst.length; i++){
					if(tmp_lst[i].equalsIgnoreCase(VSTPDataItemType.CONVERT_SS7.getId())){
						//System.out.println("CONVERTING TO ss7!!!!!!!!!!!!!!!!!!");
						// converted to SS7, exit SMPP conversion loop
						vsd.type = RoutingConnectionType.M3UA;
						vsd.sri_packet = new HPLMNRPacket();
						vsd.sri_packet.m3ua_packet = Utils.generate_default_M3UA();
						vsd.sri_packet.sccp_udt = Utils.generate_default_SCCP();
						switch(md.header.msg_type){
							// SMPP MO -> SS7 MT
							case SMPP_MO:
								TPDU tpdu = Utils.convert_SMPP_TPDU_reverse(vsd.smpp_packet.smpp_pdu);
								if (tpdu != null) {
									last_callback_tcap_id = HLRManager.consume_TCAP_ID();
									byte[] tmp = Utils.generate_MT_TCAP_MAP(	"0.4.0.0.1.0.25.3", 
																				last_callback_tcap_id, 
																				1, 
																				md.values.get(VSTPDataItemType.MAP_IMSI.getId()), 
																				md.values.get(VSTPDataItemType.MAP_SCOA.getId()), 
																				tpdu.encode());
									vsd.sri_packet.sccp_udt.data = tmp;
									((DATA)vsd.sri_packet.m3ua_packet.message).protocolData.setUserProtocolData(vsd.sri_packet.sccp_udt.encode());
									BerTranscoderv2 ber = new BerTranscoderv2();
									vsd.sri_packet.mt_tcm = (TCMessage)ber.decode(new TCMessage(), tmp);
								
								}								
								break;
							// SMPP MT -> SS7 MO
							case SMPP_MT:
								// TODO
								vsd.sri_packet.mt_tcm = null; 
								break;
						}
						vsd.sd = new SSCTPDescriptor(1, null);
						
						// change MD
						md_new = new MessageDescriptor();
						md_new.header.msg_id = md.header.msg_id;
						md_new.header.destination = md.header.destination;
						md_new.header.destination_id = md.header.destination_id;
						md_new.header.ds = md.header.ds;
						md_new.header.msg_type = MessageType.SMS_MT;
						md_new.header.source = md.header.source;
						md_new.header.source_id = md.header.source_id;
						md_new.values = md.values;
						// process oher modifiers
						processModifiers(modifiers, i+1, md_new, vsd);
						break;
					}else{
						// get descriptor
						tmp_dsc = Bindings.get_smpp(tmp_lst[i]);
						if(tmp_dsc != null){
							// modifier method
							tmp_dsc.modify(vsd.sri_packet, md.values.get(tmp_lst[i]));
							
						}
						
					}
				}
				
			}			
		}


	}

	
	private class Worker_r implements Runnable{
		MessageDescriptor md = null;
		VSTP_SGF_CorrelationPacket vsd = null;
		UDT_UnitData udt = null;
		RoutingConnectionPayload rpl = null;
		M3UAPacket m3ua_packet;
		MTP3Packet mtp3_packet;
		DATA m3ua_data = null;
		SCCPDataMessage mtp3_data = null;
		RoutingConnection r_conn = null;
		String tmp = null;
		//String[] tmp_lst = null;
		//BindingDescriptor tmp_dsc = null;
		TCMessage tcm = null;
		SMPPPacket smppp;
		long invoke_id;
		//HLR_REQ_CALLBACK hlr_callback = null;
		
		public void run() {
			LoggingManager.info(logger, "Starting...");
			while(!stopping){
				md = SGNode.in_poll();
				if(md != null){
					/*
					System.out.println("--------- VSTP START-----------");
					System.out.println(new String(md.encode()));
					System.out.println("--------- VSTP END-----------");
					*/
					try{
						// SGC from F Node
						// processed by SGCManager
						// forward to SGCManager
						if(md.header.msg_type == MessageType.SGC){
							SGCManager.in_offer(md);
							
						// DB REPLY
						}else if(md.header.msg_type == MessageType.DB_REPLY){
							if(md.values.get(VSTPDataItemType.DB_REPLY_TYPE.getId()) != null){
								tmp = md.values.get(VSTPDataItemType.DB_REPLY_TYPE.getId());
								// ss7->smpp ec
								if(tmp.equals(VSTPDataItemType.DB_SS7_SMPP_EC_CONVERSION.getId())){
									SMPP2SS7Manager.process_sync_reply(md);
								// smpp users sync
								}else if(tmp.equals(VSTPDataItemType.DB_SMPP_USER_SYNC.getId())){
									SMPPConnManager.process_smpp_user_sync_reply(md);
								}
							}
							
						// SMSFS HLR REQ
						}else if(md.header.msg_type == MessageType.HLRREQ){
						
							//System.out.println(new String(md.encode()));
							HLRPacket hlrp = HLRManager.generateRequest(	md.values.get(VSTPDataItemType.HLR_MSISDN.getId()), 
																			md.values.get(VSTPDataItemType.HLR_SCA.getId()), 
																			RoutingManager.getDefault(RoutingConnectionType.M3UA, null));
							
							
							if(hlrp != null){
								rpl = RoutingManager.createPayload(RoutingConnectionType.M3UA);
								rpl.setParams(new Object[]{new SSCTPDescriptor(1, hlrp.packet), hlrp.m3ua_packet});
								r_conn = RoutingManager.getDefault(RoutingConnectionType.M3UA, null);
								rpl.callback_required = true;
								rpl.callback = new HLR_REQ_CALLBACK(md.header.msg_id, md.header.source_id, hlrp.tcap_id);
								if(r_conn != null){
									r_conn.out_offer(rpl);
									LoggingManager.debug(SGNConfigData.sgn_debug, logger, "HLR REQUEST sent to default M3UA Connection!");
								}else LoggingManager.warn(logger, "Routing Connection [" + r_conn.id + "] does not exist!");
							}
						
						
						// regular SGN-FGN communication
						}else{
							vsd = VSTP_SGF_CorrelationManager.get(md.header.msg_id);
							if(vsd != null){
								// FILTER RESULT
								switch(md.header.msg_type){
									// SS7/SIGTRAN
									case SMS_MO: tcm = vsd.sri_packet.mo_tcm; break;
									case SMS_MT: tcm = vsd.sri_packet.mt_tcm; break;
									case HLR: tcm = vsd.sri_packet.sri_tcm; break;
									// SMPP
									case SMPP_MO:
									case SMPP_MT: smppp = vsd.smpp_packet; break;
								}

								
								
								// check if M3UA/SCCP modifiers present
								if((tmp = md.values.get(VSTPDataItemType.PENDING_MODIFIERS.getId())) != null){
									//System.out.println("DEBUG START");
									//System.out.println(new String(md.encode()));
									//System.out.println("DEBUG END");
									processModifiers(md.values.get(VSTPDataItemType.PENDING_MODIFIERS.getId()), 0, md, vsd);
	
								}

								// assign params to payload
								rpl = RoutingManager.createPayload(vsd.type);
								rpl.setParamsFromVSTP_SGF(vsd);
								rpl.extra_conversion_params = new ArrayList<Object>();
								rpl.extra_conversion_params.add(last_callback_tcap_id);


								// check if filter result is REJECTED
								if(Integer.parseInt(md.values.get(VSTPDataItemType.FILTER_RESULT.getId())) == 0){
									// create returnError with M3UA header
									// m3ua - sctp
									switch(rpl.type){
										// m3ua - sctp
										case M3UA:
											m3ua_packet = vsd.sri_packet.m3ua_packet;
											m3ua_data = (DATA)vsd.sri_packet.m3ua_packet.message;
											// sccp
											udt = vsd.sri_packet.sccp_udt;
											// TCAP
											// generate abort
											// set sccp udt data
											invoke_id = Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
											/*
											udt.data = Utils.generate_TCAP_Abort(	Utils.getTcapDialogueAppCtx(tcm), 
																					Utils.getTcap_sid(tcm));
																					*/
											udt.data = Utils.generate_SMS_Abort(Utils.getTcapDialogueAppCtx(tcm), Utils.getTcap_sid(tcm), invoke_id);
											// set m3ua user data
											m3ua_data.protocolData.setUserProtocolData(udt.encode());
											// encode m3ua and change sctp payload
											((M3UAPayload)rpl).m3ua_packet = m3ua_packet;
											((M3UAPayload)rpl).sd.payload = m3ua_packet.encode();
											break;
										// mtp3 - e1
										case MTP3:
											mtp3_packet = vsd.sri_packet.mtp3_packet;
											mtp3_data = (SCCPDataMessage)mtp3_packet.message;
											// sccp
											udt = vsd.sri_packet.sccp_udt;
											// TCAP
											// generate abort
											// set sccp udt data
											invoke_id = Utils.bytes2num(Utils.get_INVOKE_ID(tcm));
											udt.data = Utils.generate_SMS_Abort(Utils.getTcapDialogueAppCtx(tcm), Utils.getTcap_sid(tcm), invoke_id);
											// set m3ua user data
											mtp3_data.data = udt.encode();
											// encode mtp3
											break;
										// smpp - tcp
										case SMPP:
											SMPPPayload smpppld = (SMPPPayload)rpl;
											PDUBase smpp_spam_reply = Utils.genereate_SMPP_Spam_reply(smpppld.smpp_packet);
											if(smpp_spam_reply != null){
												// smpp proxy mode, callback_required = false
												if(!vsd.callback_required){
													ProxyCorrelation pc = SMPPConnManager.correlation_get(vsd.smpp_packet.client_ip + ":" + vsd.smpp_packet.client_port);
													if(pc != null){
														pc.client_conn.out_offer(smpp_spam_reply.encode());
														// set rpl to null
														// if null, it won't get routed to end point connection
														rpl = null;
													}
												// smpp server mode, callback_required = true
												}else{
													if(vsd.callback != null){
														((SMPPCallback)vsd.callback).client_connection.out_offer(smpp_spam_reply.encode());
														// set rpl to null
														// if null, it won't get routed to end point connection
														rpl = null;
													}
												}
												
											}
											break;
									}
									
									LoggingManager.debug(SGNConfigData.sgn_debug, logger, "Message REJECTED: [" + md.header.msg_type + "]!");
								}else{
									
									LoggingManager.debug(SGNConfigData.sgn_debug, logger, "Message ACCEPTED: [" + md.header.msg_type + "]!");
								}

								
								// connection check
								// use default connection
								if(md.values.get(VSTPDataItemType.ROUTING_CONNECTION.getId()) == null){
									r_conn = RoutingManager.getDefault(vsd.type, vsd);
									if(r_conn != null){
										if(r_conn.active){
											// send to out queue
											if(rpl != null) r_conn.out_offer(rpl);
											//logger.debug("Message sent to default [" + vsd.type + "] Connection!");
										}else{
											LoggingManager.warn(logger, "Default routing connection for [" + vsd.type + "] is inactive!");
											
										}

									}else{
										LoggingManager.warn(logger, "Default routing connection for [" + vsd.type + "] does not exist!");
									}
									
								// route to explicit connection/s
								}else{
									// Feature check for FEATURE_STP
									//if(SecurityManager.check_feature(SecurityManager.FEATURE_STP)){
										RoutingManager.send_fail_check(rpl, md.values.get(VSTPDataItemType.ROUTING_CONNECTION.getId()));

									// feature disabled
									/*
									}else{
										r_conn = RoutingManager.getDefault(vsd.type, vsd);
										if(r_conn != null){
											// send to out queue
											if(rpl != null) r_conn.out_offer(rpl);
										}else{
											LoggingManager.warn(logger, "Default routing connection for [" + vsd.type + "] does not exist!");
										}
										
									}
								*/
								}
								
								// remove correlation
								VSTP_SGF_CorrelationManager.remove(md.header.msg_id);
								// distribute remove
								SGCManager.distribute_vstp_crl_remove(md.header.msg_id, md.header.ds);
								
			
							}else{
								LoggingManager.warn(logger, "VSTP-SRI Correlation not found: [" + md.header.msg_id + "]!");
							}
							
							
							
						}

					}catch(Exception e){
						e.printStackTrace();
					}
					
				}else{
					try{ Thread.sleep(1); }catch(Exception e){ e.printStackTrace(); }

				}
				
			}
			LoggingManager.info(logger, "Ending...");
		}
		
	}
	
	public FWorker(int _id){
		_id = id;
		LoggingManager.info(logger, "Strting FWorker: [" + _id + "]!");
		// worker thread
		worker_r = new Worker_r();
		worker_t = new Thread(worker_r, "F_WORKER_" + id);
		worker_t.start();
	}
}
