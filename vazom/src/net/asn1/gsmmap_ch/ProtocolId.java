package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ProtocolId extends ENUMERATED{

	public static final int _gsm_0408 = 1;
	public static final int _gsm_0806 = 2;
	public static final int _gsm_BSSMAP = 3;
	public static final int _ets_300102_1 = 4;
	public ProtocolId(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
