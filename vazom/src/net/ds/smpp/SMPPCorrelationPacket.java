package net.ds.smpp;

import java.util.HashMap;

public class SMPPCorrelationPacket {
	public String ip_source;
	public String ip_destination;
	public int tcp_source;
	public int tcp_destination;
	public String system_id;
	public String password;
	public int sequence_id;
	public HashMap<Long, SMPPSequenceDescriptor> sequence_map;
	public long ts;

	
	public SMPPCorrelationPacket(){
		sequence_map = new HashMap<Long, SMPPSequenceDescriptor>();
	}
}
