__global__ void _MD5_KERNEL(char *data_lst, int *data_lst_size, char *hash, char *result){
// message
// MD5 hash list 16bytes each
        volatile unsigned int thread_id = (blockIdx.y * gridDim.x*blockDim.x) + (blockIdx.x*blockDim.x) + threadIdx.x;
        volatile unsigned int lst_hash_id = thread_id * 16;
	
        // check if current thread id is >= data_lst_size
        if(thread_id >= *data_lst_size) return;
	// check if MD5 hashes match
	for(int i = 0; i<16; i++) if(data_lst[lst_hash_id + i] != hash[i]){
		result[thread_id] = 0;
		return;
	}
	
	// if all the above checks out, messages are 100% match
	result[thread_id] = 100;

}

void MD5_KERNEL(char *data_lst, int *data_lst_size, char *hash, char *result, dim3 blocks, dim3 threads){
	_MD5_KERNEL<<<blocks, threads>>>(data_lst, data_lst_size, hash, result);

}
