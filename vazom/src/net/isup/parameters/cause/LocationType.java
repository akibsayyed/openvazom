package net.isup.parameters.cause;

import java.util.HashMap;

public enum LocationType {
	U(0x00),
	LPN(0x01),
	LN(0x02),
	TN(0x03),
	RLN(0x04),
	RPN(0x05),
	INTL(0x07),
	BI(0x0a);
	
	
	private int id;
	private static final HashMap<Integer, LocationType> lookup = new HashMap<Integer, LocationType>();
	static{
		for(LocationType td :LocationType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static LocationType get(int id){ return lookup.get(id); }
	private LocationType(int _id){ id = _id; }			
}
