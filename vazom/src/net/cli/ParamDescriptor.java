package net.cli;

public class ParamDescriptor {
	public String name;
	public String value;
	public boolean group;
	
	
	public ParamDescriptor(String _name, String _value){
		name = _name;
		value = _value;
	}
}
