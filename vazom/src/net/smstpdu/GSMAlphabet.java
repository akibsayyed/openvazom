package net.smstpdu;

import java.util.ArrayList;
import java.util.HashMap;

import net.utils.Utils;


public class GSMAlphabet {
	private static HashMap<Integer, Character> map = new HashMap<Integer, Character>();
	
	static{
		map.put(0x00, '@');		map.put(0x01, '£');			map.put(0x02, '$');		map.put(0x03, '¥');			map.put(0x04, 'è');			map.put(0x05, 'é');
		map.put(0x06, 'ù');		map.put(0x07, 'ì');			map.put(0x08, 'ò');		map.put(0x09, 'Ç');			map.put(0x0A, (char)0x0A);	map.put(0x0B, 'Ø');
		map.put(0x0C, 'ø');		map.put(0x0D, (char)0x0A);	map.put(0x0E, 'Å');		map.put(0x0F, 'å');			map.put(0x10, 'Δ');			map.put(0x11, '_');
		map.put(0x12, 'Φ');		map.put(0x13, 'Γ');			map.put(0x14, 'Λ');		map.put(0x15, 'Ω');			map.put(0x16, 'Π');			map.put(0x17, 'Ψ');
		map.put(0x18, 'Σ');		map.put(0x19, 'Θ');			map.put(0x1A, 'Ξ');		map.put(0x1B, (char)0x1B);	map.put(0x1C, 'Æ');			map.put(0x1D, 'æ');
		map.put(0x1E, 'ß');		map.put(0x1F, 'É');			map.put(0x20, ' ');		map.put(0x21, '!');			map.put(0x22, '"');			map.put(0x23, '#');
		map.put(0x24, '¤');		map.put(0x25, '%');			map.put(0x26, '&');		map.put(0x27, '\'');		map.put(0x28, '(');			map.put(0x29, ')');
		map.put(0x2A, '*');		map.put(0x2B, '+');			map.put(0x2C, ',');		map.put(0x2D, '-');			map.put(0x2E, '.');			map.put(0x2F, '/');
		map.put(0x30, '0');		map.put(0x31, '1');			map.put(0x32, '2');		map.put(0x33, '3');			map.put(0x34, '4');			map.put(0x35, '5');
		map.put(0x36, '6');		map.put(0x37, '7');			map.put(0x38, '8');		map.put(0x39, '9');			map.put(0x3A, ':');			map.put(0x3B, ';');
		map.put(0x3C, '<');		map.put(0x3D, '=');			map.put(0x3E, '>');		map.put(0x3F, '?');			map.put(0x40, '¡');			map.put(0x41, 'A');
		map.put(0x42, 'B');		map.put(0x43, 'C');			map.put(0x44, 'D');		map.put(0x45, 'E');			map.put(0x46, 'F');			map.put(0x47, 'G');
		map.put(0x48, 'H');		map.put(0x49, 'I');			map.put(0x4A, 'J');		map.put(0x4B, 'K');			map.put(0x4C, 'L');			map.put(0x4D, 'M');
		map.put(0x4E, 'N');		map.put(0x4F, 'O');			map.put(0x50, 'P');		map.put(0x51, 'Q');			map.put(0x52, 'R');			map.put(0x53, 'S');
		map.put(0x54, 'T');		map.put(0x55, 'U');			map.put(0x56, 'V');		map.put(0x57, 'W');			map.put(0x58, 'X');			map.put(0x59, 'Y');
		map.put(0x5A, 'Z');		map.put(0x5B, 'Ä');			map.put(0x5C, 'Ö');		map.put(0x5D, 'Ñ');			map.put(0x5E, 'Ü');			map.put(0x5F, '§');
		map.put(0x60, '¿');		map.put(0x61, 'a');			map.put(0x62, 'b');		map.put(0x63, 'c');			map.put(0x64, 'd');			map.put(0x65, 'e');
		map.put(0x66, 'f');		map.put(0x67, 'g');			map.put(0x68, 'h');		map.put(0x69, 'i');			map.put(0x6A, 'j');			map.put(0x6B, 'k');
		map.put(0x6C, 'l');		map.put(0x6D, 'm');			map.put(0x6E, 'n');		map.put(0x6F, 'o');			map.put(0x70, 'p');			map.put(0x71, 'q');
		map.put(0x72, 'r');		map.put(0x73, 's');			map.put(0x74, 't');		map.put(0x75, 'u');			map.put(0x76, 'v');			map.put(0x77, 'w');
		map.put(0x78, 'x');		map.put(0x79, 'y');			map.put(0x7A, 'z');		map.put(0x7B, 'ä');			map.put(0x7C, 'ö');			map.put(0x7D, 'ñ');
		map.put(0x7E, 'ü');		map.put(0x7F, 'à');
	
	}
	private static int getCode(Character ch){
		for(Integer i : map.keySet()){
			if(map.get(i) == ch) return i;
		}
		
		return -1;
	}
	public static int getPrePaddingBits(int udh_size_bytes){
		int m = (udh_size_bytes * 8) % 7;
		return (m > 0 ? 7 - ((udh_size_bytes * 8) % 7) : 0);
	}
	public static byte[] encode(String data, int pre_padding_bits){
		int dl = data.length();
		ArrayList<boolean[]> chars = new ArrayList<boolean[]>();
		byte[] res = new byte[(int)Math.ceil(((double)dl * 7) / 8)];
		byte[] str_bytes = data.getBytes();
		boolean[] tmp_bits;
		boolean[] res_bits = null;
		int m;
		int p;
		byte tmp_byte = 0;

		// pre padding bits
		if(pre_padding_bits > 0) res_bits = new boolean[pre_padding_bits];
		
		// process chars
		for(int i = 0; i<str_bytes.length; i++){
			tmp_byte = str_bytes[i];
			tmp_bits = Utils.int2bits(getCode((char)tmp_byte), 7);
			chars.add(tmp_bits);
		}
		// combine bits
		for(int i = 0; i<chars.size(); i++) res_bits = Utils.bitsCombine(chars.get(i), res_bits);
		
		// add extra padding bits if needed
		m = res_bits.length % 8;
		if(m > 0){
			p = 8 - m;
			res_bits = Utils.bitsCombine(new boolean[p], res_bits);
		}
		//for(int i = 0; i<res_bits.length; i++) System.out.print((res_bits[i] ? 1 : 0));
		//System.out.println();

		// bits 2 bytes
		res = Utils.bits2bytes_reverse(res_bits);
		return res;
	}

	public static String decode(byte[] data){
		String res = "";
		int l = 0;
		int b;
		int n = 0;
		int p = 0;
		if(data != null){
			for(int i = 0; i<data.length; i++){
				p |= (int)Math.pow(2, 7 - l);
				b = data[i] & ~p;
				res += map.get(((b << l) + (n>>(8-l))) & 0xFF);
				n = data[i] & p;
				l++;
				if(l == 7){
					l = 0;
					p = 0;
					res += map.get(n >> 1);
				}

			}
		}
		return res;
		
	}
	
	public static String decode(byte[] data, int count){
		String res = "";
		int l = 0;
		int b;
		int n = 0;
		int p = 0;
		int c = 0;
		if(data != null){
			for(int i = 0; i<data.length; i++){
				p |= (int)Math.pow(2, 7 - l);
				b = data[i] & ~p;
				if(c < count) res += map.get(((b << l) + (n>>(8-l))) & 0xFF);
				c++;
				n = data[i] & p;
				l++;
				if(l == 7){
					l = 0;
					p = 0;
					if(c < count) res += map.get(n >> 1);
					c++;
				}

			}
		}
		return res;
		
	}
	/*
	public static String decode(byte[] data, int count){
		String res = "";
		int tmp = 0;
		int i = 0;
		if(data != null){
			boolean[] bits = Utils.bytes2bits(Utils.reverse(data));
			while(i < count){
				tmp = Utils.bits2int(bits, (count - i)*7, 7);
				res += map.get(tmp);
				i++;
				
			}
			
		}
		return res;
		
	}
	*/
	public static String decodePadded(byte[] data, int udh_length){
		byte[] tmp = null;
		String res = "";
		int from;
		int to;
		if(data != null){
			tmp = new byte[data.length + udh_length + 1];
			for(int i = 0; i<data.length; i++) tmp[i + udh_length] = data[i];
			res = decode(tmp);
			if(res.length() > 0 && udh_length > 0){
				from = (int)Math.ceil((double)(udh_length * 8)/7);
				to = res.length() - 1;
				if(to >= from){
					res = res.substring(from, to);
					
				}
			}
		}
		return res;
	
	}


	
}
