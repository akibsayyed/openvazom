package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class MId extends CHOICE{

	public IP4Address get_ip4Address(){
		return (IP4Address)elements.get(0).data;
	}
	public IP4Address new_ip4Address(){
		return new IP4Address();
	}

	public IP6Address get_ip6Address(){
		return (IP6Address)elements.get(1).data;
	}
	public IP6Address new_ip6Address(){
		return new IP6Address();
	}

	public DomainName get_domainName(){
		return (DomainName)elements.get(2).data;
	}
	public DomainName new_domainName(){
		return new DomainName();
	}

	public PathName get_deviceName(){
		return (PathName)elements.get(3).data;
	}
	public PathName new_deviceName(){
		return new PathName();
	}

	public MtpAddress get_mtpAddress(){
		return (MtpAddress)elements.get(4).data;
	}
	public MtpAddress new_mtpAddress(){
		return new MtpAddress();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ip4Address(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_ip6Address(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_domainName(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_deviceName(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mtpAddress(); }});
	}
	public MId(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
