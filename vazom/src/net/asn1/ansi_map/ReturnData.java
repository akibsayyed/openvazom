package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ReturnData extends SEQUENCE{

	public HandoffMeasurementRequestRes get_handoffMeasurementRequestRes(){
		return (HandoffMeasurementRequestRes)elements.get(0).data;
	}
	public HandoffMeasurementRequestRes new_handoffMeasurementRequestRes(){
		return new HandoffMeasurementRequestRes();
	}

	public FacilitiesDirectiveRes get_facilitiesDirectiveRes(){
		return (FacilitiesDirectiveRes)elements.get(1).data;
	}
	public FacilitiesDirectiveRes new_facilitiesDirectiveRes(){
		return new FacilitiesDirectiveRes();
	}

	public HandoffBackRes get_handoffBackRes(){
		return (HandoffBackRes)elements.get(2).data;
	}
	public HandoffBackRes new_handoffBackRes(){
		return new HandoffBackRes();
	}

	public FacilitiesReleaseRes get_facilitiesReleaseRes(){
		return (FacilitiesReleaseRes)elements.get(3).data;
	}
	public FacilitiesReleaseRes new_facilitiesReleaseRes(){
		return new FacilitiesReleaseRes();
	}

	public QualificationDirectiveRes get_qualificationDirectiveRes(){
		return (QualificationDirectiveRes)elements.get(4).data;
	}
	public QualificationDirectiveRes new_qualificationDirectiveRes(){
		return new QualificationDirectiveRes();
	}

	public QualificationRequestRes get_qualificationRequestRes(){
		return (QualificationRequestRes)elements.get(5).data;
	}
	public QualificationRequestRes new_qualificationRequestRes(){
		return new QualificationRequestRes();
	}

	public ResetCircuitRes get_resetCircuitRes(){
		return (ResetCircuitRes)elements.get(6).data;
	}
	public ResetCircuitRes new_resetCircuitRes(){
		return new ResetCircuitRes();
	}

	public RegistrationNotificationRes get_registrationNotificationRes(){
		return (RegistrationNotificationRes)elements.get(7).data;
	}
	public RegistrationNotificationRes new_registrationNotificationRes(){
		return new RegistrationNotificationRes();
	}

	public RegistrationCancellationRes get_registrationCancellationRes(){
		return (RegistrationCancellationRes)elements.get(8).data;
	}
	public RegistrationCancellationRes new_registrationCancellationRes(){
		return new RegistrationCancellationRes();
	}

	public LocationRequestRes get_locationRequestRes(){
		return (LocationRequestRes)elements.get(9).data;
	}
	public LocationRequestRes new_locationRequestRes(){
		return new LocationRequestRes();
	}

	public RoutingRequestRes get_routingRequestRes(){
		return (RoutingRequestRes)elements.get(10).data;
	}
	public RoutingRequestRes new_routingRequestRes(){
		return new RoutingRequestRes();
	}

	public FeatureRequestRes get_featureRequestRes(){
		return (FeatureRequestRes)elements.get(11).data;
	}
	public FeatureRequestRes new_featureRequestRes(){
		return new FeatureRequestRes();
	}

	public TransferToNumberRequestRes get_transferToNumberRequestRes(){
		return (TransferToNumberRequestRes)elements.get(12).data;
	}
	public TransferToNumberRequestRes new_transferToNumberRequestRes(){
		return new TransferToNumberRequestRes();
	}

	public HandoffToThirdRes get_handoffToThirdRes(){
		return (HandoffToThirdRes)elements.get(13).data;
	}
	public HandoffToThirdRes new_handoffToThirdRes(){
		return new HandoffToThirdRes();
	}

	public AuthenticationDirectiveRes get_authenticationDirectiveRes(){
		return (AuthenticationDirectiveRes)elements.get(14).data;
	}
	public AuthenticationDirectiveRes new_authenticationDirectiveRes(){
		return new AuthenticationDirectiveRes();
	}

	public AuthenticationRequestRes get_authenticationRequestRes(){
		return (AuthenticationRequestRes)elements.get(15).data;
	}
	public AuthenticationRequestRes new_authenticationRequestRes(){
		return new AuthenticationRequestRes();
	}

	public BaseStationChallengeRes get_baseStationChallengeRes(){
		return (BaseStationChallengeRes)elements.get(16).data;
	}
	public BaseStationChallengeRes new_baseStationChallengeRes(){
		return new BaseStationChallengeRes();
	}

	public AuthenticationFailureReportRes get_authenticationFailureReportRes(){
		return (AuthenticationFailureReportRes)elements.get(17).data;
	}
	public AuthenticationFailureReportRes new_authenticationFailureReportRes(){
		return new AuthenticationFailureReportRes();
	}

	public CountRequestRes get_countRequestRes(){
		return (CountRequestRes)elements.get(18).data;
	}
	public CountRequestRes new_countRequestRes(){
		return new CountRequestRes();
	}

	public InterSystemPageRes get_interSystemPageRes(){
		return (InterSystemPageRes)elements.get(19).data;
	}
	public InterSystemPageRes new_interSystemPageRes(){
		return new InterSystemPageRes();
	}

	public UnsolicitedResponseRes get_unsolicitedResponseRes(){
		return (UnsolicitedResponseRes)elements.get(20).data;
	}
	public UnsolicitedResponseRes new_unsolicitedResponseRes(){
		return new UnsolicitedResponseRes();
	}

	public HandoffMeasurementRequest2Res get_handoffMeasurementRequest2Res(){
		return (HandoffMeasurementRequest2Res)elements.get(21).data;
	}
	public HandoffMeasurementRequest2Res new_handoffMeasurementRequest2Res(){
		return new HandoffMeasurementRequest2Res();
	}

	public FacilitiesDirective2Res get_facilitiesDirective2Res(){
		return (FacilitiesDirective2Res)elements.get(22).data;
	}
	public FacilitiesDirective2Res new_facilitiesDirective2Res(){
		return new FacilitiesDirective2Res();
	}

	public HandoffBack2Res get_handoffBack2Res(){
		return (HandoffBack2Res)elements.get(23).data;
	}
	public HandoffBack2Res new_handoffBack2Res(){
		return new HandoffBack2Res();
	}

	public HandoffToThird2Res get_handoffToThird2Res(){
		return (HandoffToThird2Res)elements.get(24).data;
	}
	public HandoffToThird2Res new_handoffToThird2Res(){
		return new HandoffToThird2Res();
	}

	public AuthenticationDirectiveForwardRes get_authenticationDirectiveForwardRes(){
		return (AuthenticationDirectiveForwardRes)elements.get(25).data;
	}
	public AuthenticationDirectiveForwardRes new_authenticationDirectiveForwardRes(){
		return new AuthenticationDirectiveForwardRes();
	}

	public AuthenticationStatusReportRes get_authenticationStatusReportRes(){
		return (AuthenticationStatusReportRes)elements.get(26).data;
	}
	public AuthenticationStatusReportRes new_authenticationStatusReportRes(){
		return new AuthenticationStatusReportRes();
	}

	public InformationDirectiveRes get_informationDirectiveRes(){
		return (InformationDirectiveRes)elements.get(27).data;
	}
	public InformationDirectiveRes new_informationDirectiveRes(){
		return new InformationDirectiveRes();
	}

	public InformationForwardRes get_informationForwardRes(){
		return (InformationForwardRes)elements.get(28).data;
	}
	public InformationForwardRes new_informationForwardRes(){
		return new InformationForwardRes();
	}

	public InterSystemPage2Res get_interSystemPage2Res(){
		return (InterSystemPage2Res)elements.get(29).data;
	}
	public InterSystemPage2Res new_interSystemPage2Res(){
		return new InterSystemPage2Res();
	}

	public InterSystemSetupRes get_interSystemSetupRes(){
		return (InterSystemSetupRes)elements.get(30).data;
	}
	public InterSystemSetupRes new_interSystemSetupRes(){
		return new InterSystemSetupRes();
	}

	public OriginationRequestRes get_originationRequestRes(){
		return (OriginationRequestRes)elements.get(31).data;
	}
	public OriginationRequestRes new_originationRequestRes(){
		return new OriginationRequestRes();
	}

	public RandomVariableRequestRes get_randomVariableRequestRes(){
		return (RandomVariableRequestRes)elements.get(32).data;
	}
	public RandomVariableRequestRes new_randomVariableRequestRes(){
		return new RandomVariableRequestRes();
	}

	public RemoteUserInteractionDirectiveRes get_remoteUserInteractionDirectiveRes(){
		return (RemoteUserInteractionDirectiveRes)elements.get(33).data;
	}
	public RemoteUserInteractionDirectiveRes new_remoteUserInteractionDirectiveRes(){
		return new RemoteUserInteractionDirectiveRes();
	}

	public SMSDeliveryBackwardRes get_sMSDeliveryBackwardRes(){
		return (SMSDeliveryBackwardRes)elements.get(34).data;
	}
	public SMSDeliveryBackwardRes new_sMSDeliveryBackwardRes(){
		return new SMSDeliveryBackwardRes();
	}

	public SMSDeliveryForwardRes get_sMSDeliveryForwardRes(){
		return (SMSDeliveryForwardRes)elements.get(35).data;
	}
	public SMSDeliveryForwardRes new_sMSDeliveryForwardRes(){
		return new SMSDeliveryForwardRes();
	}

	public SMSDeliveryPointToPointRes get_sMSDeliveryPointToPointRes(){
		return (SMSDeliveryPointToPointRes)elements.get(36).data;
	}
	public SMSDeliveryPointToPointRes new_sMSDeliveryPointToPointRes(){
		return new SMSDeliveryPointToPointRes();
	}

	public SMSNotificationRes get_sMSNotificationRes(){
		return (SMSNotificationRes)elements.get(37).data;
	}
	public SMSNotificationRes new_sMSNotificationRes(){
		return new SMSNotificationRes();
	}

	public SMSRequestRes get_sMSRequestRes(){
		return (SMSRequestRes)elements.get(38).data;
	}
	public SMSRequestRes new_sMSRequestRes(){
		return new SMSRequestRes();
	}

	public OTASPRequestRes get_oTASPRequestRes(){
		return (OTASPRequestRes)elements.get(39).data;
	}
	public OTASPRequestRes new_oTASPRequestRes(){
		return new OTASPRequestRes();
	}

	public ChangeFacilitiesRes get_changeFacilitiesRes(){
		return (ChangeFacilitiesRes)elements.get(40).data;
	}
	public ChangeFacilitiesRes new_changeFacilitiesRes(){
		return new ChangeFacilitiesRes();
	}

	public ChangeServiceRes get_changeServiceRes(){
		return (ChangeServiceRes)elements.get(41).data;
	}
	public ChangeServiceRes new_changeServiceRes(){
		return new ChangeServiceRes();
	}

	public ParameterRequestRes get_parameterRequestRes(){
		return (ParameterRequestRes)elements.get(42).data;
	}
	public ParameterRequestRes new_parameterRequestRes(){
		return new ParameterRequestRes();
	}

	public TMSIDirectiveRes get_tMSIDirectiveRes(){
		return (TMSIDirectiveRes)elements.get(43).data;
	}
	public TMSIDirectiveRes new_tMSIDirectiveRes(){
		return new TMSIDirectiveRes();
	}

	public NumberPortabilityRequestRes get_numberPortabilityRequestRes(){
		return (NumberPortabilityRequestRes)elements.get(44).data;
	}
	public NumberPortabilityRequestRes new_numberPortabilityRequestRes(){
		return new NumberPortabilityRequestRes();
	}

	public ServiceRequestRes get_serviceRequestRes(){
		return (ServiceRequestRes)elements.get(45).data;
	}
	public ServiceRequestRes new_serviceRequestRes(){
		return new ServiceRequestRes();
	}

	public AnalyzedInformationRes get_analyzedInformationRes(){
		return (AnalyzedInformationRes)elements.get(46).data;
	}
	public AnalyzedInformationRes new_analyzedInformationRes(){
		return new AnalyzedInformationRes();
	}

	public FacilitySelectedAndAvailableRes get_facilitySelectedAndAvailableRes(){
		return (FacilitySelectedAndAvailableRes)elements.get(47).data;
	}
	public FacilitySelectedAndAvailableRes new_facilitySelectedAndAvailableRes(){
		return new FacilitySelectedAndAvailableRes();
	}

	public ModifyRes get_modifyRes(){
		return (ModifyRes)elements.get(48).data;
	}
	public ModifyRes new_modifyRes(){
		return new ModifyRes();
	}

	public SearchRes get_searchRes(){
		return (SearchRes)elements.get(49).data;
	}
	public SearchRes new_searchRes(){
		return new SearchRes();
	}

	public SeizeResourceRes get_seizeResourceRes(){
		return (SeizeResourceRes)elements.get(50).data;
	}
	public SeizeResourceRes new_seizeResourceRes(){
		return new SeizeResourceRes();
	}

	public SRFDirectiveRes get_sRFDirectiveRes(){
		return (SRFDirectiveRes)elements.get(51).data;
	}
	public SRFDirectiveRes new_sRFDirectiveRes(){
		return new SRFDirectiveRes();
	}

	public TBusyRes get_tBusyRes(){
		return (TBusyRes)elements.get(52).data;
	}
	public TBusyRes new_tBusyRes(){
		return new TBusyRes();
	}

	public TNoAnswerRes get_tNoAnswerRes(){
		return (TNoAnswerRes)elements.get(53).data;
	}
	public TNoAnswerRes new_tNoAnswerRes(){
		return new TNoAnswerRes();
	}

	public CallControlDirectiveRes get_callControlDirectiveRes(){
		return (CallControlDirectiveRes)elements.get(54).data;
	}
	public CallControlDirectiveRes new_callControlDirectiveRes(){
		return new CallControlDirectiveRes();
	}

	public ODisconnectRes get_oDisconnectRes(){
		return (ODisconnectRes)elements.get(55).data;
	}
	public ODisconnectRes new_oDisconnectRes(){
		return new ODisconnectRes();
	}

	public TDisconnectRes get_tDisconnectRes(){
		return (TDisconnectRes)elements.get(56).data;
	}
	public TDisconnectRes new_tDisconnectRes(){
		return new TDisconnectRes();
	}

	public OCalledPartyBusyRes get_oCalledPartyBusyRes(){
		return (OCalledPartyBusyRes)elements.get(57).data;
	}
	public OCalledPartyBusyRes new_oCalledPartyBusyRes(){
		return new OCalledPartyBusyRes();
	}

	public ONoAnswerRes get_oNoAnswerRes(){
		return (ONoAnswerRes)elements.get(58).data;
	}
	public ONoAnswerRes new_oNoAnswerRes(){
		return new ONoAnswerRes();
	}

	public PositionRequestRes get_positionRequestRes(){
		return (PositionRequestRes)elements.get(59).data;
	}
	public PositionRequestRes new_positionRequestRes(){
		return new PositionRequestRes();
	}

	public PositionRequestForwardRes get_positionRequestForwardRes(){
		return (PositionRequestForwardRes)elements.get(60).data;
	}
	public PositionRequestForwardRes new_positionRequestForwardRes(){
		return new PositionRequestForwardRes();
	}

	public InterSystemPositionRequestRes get_interSystemPositionRequestRes(){
		return (InterSystemPositionRequestRes)elements.get(61).data;
	}
	public InterSystemPositionRequestRes new_interSystemPositionRequestRes(){
		return new InterSystemPositionRequestRes();
	}

	public InterSystemPositionRequestForwardRes get_interSystemPositionRequestForwardRes(){
		return (InterSystemPositionRequestForwardRes)elements.get(62).data;
	}
	public InterSystemPositionRequestForwardRes new_interSystemPositionRequestForwardRes(){
		return new InterSystemPositionRequestForwardRes();
	}

	public RoamerDatabaseVerificationRequestRes get_roamerDatabaseVerificationRequestRes(){
		return (RoamerDatabaseVerificationRequestRes)elements.get(63).data;
	}
	public RoamerDatabaseVerificationRequestRes new_roamerDatabaseVerificationRequestRes(){
		return new RoamerDatabaseVerificationRequestRes();
	}

	public AddServiceRes get_addServiceRes(){
		return (AddServiceRes)elements.get(64).data;
	}
	public AddServiceRes new_addServiceRes(){
		return new AddServiceRes();
	}

	public DropServiceRes get_dropServiceRes(){
		return (DropServiceRes)elements.get(65).data;
	}
	public DropServiceRes new_dropServiceRes(){
		return new DropServiceRes();
	}

	public InterSystemSMSPage get_interSystemSMSPage(){
		return (InterSystemSMSPage)elements.get(66).data;
	}
	public InterSystemSMSPage new_interSystemSMSPage(){
		return new InterSystemSMSPage();
	}

	public LCSParameterRequestRes get_lcsParameterRequestRes(){
		return (LCSParameterRequestRes)elements.get(67).data;
	}
	public LCSParameterRequestRes new_lcsParameterRequestRes(){
		return new LCSParameterRequestRes();
	}

	public CheckMEIDRes get_checkMEIDRes(){
		return (CheckMEIDRes)elements.get(68).data;
	}
	public CheckMEIDRes new_checkMEIDRes(){
		return new CheckMEIDRes();
	}

	public StatusRequestRes get_statusRequestRes(){
		return (StatusRequestRes)elements.get(69).data;
	}
	public StatusRequestRes new_statusRequestRes(){
		return new StatusRequestRes();
	}

	public InterSystemSMSDeliveryPointToPointRes get_interSystemSMSDeliveryPointToPointRes(){
		return (InterSystemSMSDeliveryPointToPointRes)elements.get(70).data;
	}
	public InterSystemSMSDeliveryPointToPointRes new_interSystemSMSDeliveryPointToPointRes(){
		return new InterSystemSMSDeliveryPointToPointRes();
	}

	public QualificationRequest2Res get_qualificationRequest2Res(){
		return (QualificationRequest2Res)elements.get(71).data;
	}
	public QualificationRequest2Res new_qualificationRequest2Res(){
		return new QualificationRequest2Res();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffMeasurementRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitiesDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffBackRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitiesReleaseRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_qualificationDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_qualificationRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_resetCircuitRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_registrationNotificationRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_registrationCancellationRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_locationRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_routingRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_featureRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_transferToNumberRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffToThirdRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_baseStationChallengeRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationFailureReportRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_countRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPageRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_unsolicitedResponseRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffMeasurementRequest2Res(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitiesDirective2Res(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffBack2Res(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_handoffToThird2Res(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationDirectiveForwardRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_authenticationStatusReportRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_informationDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_informationForwardRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPage2Res(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemSetupRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_originationRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_randomVariableRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_remoteUserInteractionDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSDeliveryBackwardRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSDeliveryForwardRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSDeliveryPointToPointRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSNotificationRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sMSRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oTASPRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_changeFacilitiesRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_changeServiceRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_parameterRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tMSIDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_numberPortabilityRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_serviceRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_analyzedInformationRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_facilitySelectedAndAvailableRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_modifyRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_searchRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_seizeResourceRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_sRFDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tBusyRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tNoAnswerRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_callControlDirectiveRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oDisconnectRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_tDisconnectRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oCalledPartyBusyRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_oNoAnswerRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_positionRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_positionRequestForwardRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPositionRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemPositionRequestForwardRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_roamerDatabaseVerificationRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_addServiceRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_dropServiceRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemSMSPage(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_lcsParameterRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_checkMEIDRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_statusRequestRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_interSystemSMSDeliveryPointToPointRes(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.PRIVATE, false, false){public void set(){ data = new_qualificationRequest2Res(); }});
	}
	public ReturnData(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
