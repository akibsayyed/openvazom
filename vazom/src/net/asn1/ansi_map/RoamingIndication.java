package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class RoamingIndication extends OCTET_STRING{

	public RoamingIndication(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
