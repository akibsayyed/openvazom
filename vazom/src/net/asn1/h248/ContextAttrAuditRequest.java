package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ContextAttrAuditRequest extends SEQUENCE{

	public NULL get_topology(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_topology(){
		return new NULL();
	}

	public NULL get_emergenc(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_emergenc(){
		return new NULL();
	}

	public NULL get_priority(){
		return (NULL)elements.get(2).data;
	}
	public NULL new_priority(){
		return new NULL();
	}

	public NULL get_iepscallind(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_iepscallind(){
		return new NULL();
	}

	public IndAudPropertyParmSequenceOf get_contextPropAud(){
		return (IndAudPropertyParmSequenceOf)elements.get(4).data;
	}
	public IndAudPropertyParmSequenceOf new_contextPropAud(){
		return new IndAudPropertyParmSequenceOf();
	}

	public INTEGER get_selectpriority(){
		return (INTEGER)elements.get(5).data;
	}
	public INTEGER new_selectpriority(){
		return new INTEGER();
	}

	public BOOLEAN get_selectemergency(){
		return (BOOLEAN)elements.get(6).data;
	}
	public BOOLEAN new_selectemergency(){
		return new BOOLEAN();
	}

	public BOOLEAN get_selectiepscallind(){
		return (BOOLEAN)elements.get(7).data;
	}
	public BOOLEAN new_selectiepscallind(){
		return new BOOLEAN();
	}

	public SelectLogicWrapper get_selectLogic(){
		return (SelectLogicWrapper)elements.get(8).data;
	}
	public SelectLogicWrapper new_selectLogic(){
		return new SelectLogicWrapper();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_topology(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_emergenc(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_priority(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_iepscallind(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_contextPropAud(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_selectpriority(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_selectemergency(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_selectiepscallind(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_selectLogic(); }});
	}
	public ContextAttrAuditRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
