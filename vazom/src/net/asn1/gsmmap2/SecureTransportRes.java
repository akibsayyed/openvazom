package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SecureTransportRes extends SEQUENCE{

	public SecurityHeader get_securityHeader(){
		return (SecurityHeader)elements.get(0).data;
	}
	public SecurityHeader new_securityHeader(){
		return new SecurityHeader();
	}

	public ProtectedPayload get_protectedPayload(){
		return (ProtectedPayload)elements.get(1).data;
	}
	public ProtectedPayload new_protectedPayload(){
		return new ProtectedPayload();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_securityHeader(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_protectedPayload(); }});
	}
	public SecureTransportRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
