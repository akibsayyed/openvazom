package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudEventBufferDescriptor extends SEQUENCE{

	public PkgdName get_eventName(){
		return (PkgdName)elements.get(0).data;
	}
	public PkgdName new_eventName(){
		return new PkgdName();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(1).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventName(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamID(); }});
	}
	public IndAudEventBufferDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
