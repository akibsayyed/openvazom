package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ConnectionFailureReport extends SET{

	public FailureType get_failureType(){
		return (FailureType)elements.get(0).data;
	}
	public FailureType new_failureType(){
		return new FailureType();
	}

	public FailureCause get_failureCause(){
		return (FailureCause)elements.get(1).data;
	}
	public FailureCause new_failureCause(){
		return new FailureCause();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(260, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_failureType(); }});
		elements.add(new ElementDescriptor(387, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_failureCause(); }});
	}
	public ConnectionFailureReport(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
