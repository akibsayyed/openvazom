package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.EventInformation;

public class CallProgress extends MessageBase {
	public EventInformation event_information;
	public CallProgress(){
		super();
		type = MessageType.CALL_PROGRESS;
		pointers = new Pointer[1];
	}

	public void init(byte[] data) {
		if(data != null){
			// mandatory fixed
			event_information = new EventInformation();
			event_information.init(new byte[]{data[byte_pos++]});
			
			
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			// optional
			processOptional(data, 0);
			
		}
		
	}

}
