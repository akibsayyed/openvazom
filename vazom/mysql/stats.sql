select 
	s.timestamp, 
	hour(s.timestamp) as hour, 
	count(*) as sms_count, 
	ifnull(fa.name, '0') 
from 
	sms s 
left join 
	filter_action fa on fa.id=s.filter_action_id 
where 
	timestamp between '2011-01-05' and '2011-01-06' 
group by 
	filter_action_id, 
	hour 
order by 
	timestamp;

select
        day(timestamp) as day,
        count(*) as sms_count,
        ifnull(fa.name, 'ACCEPTED') as filter
from sms left join filter_action fa on fa.id=filter_action_id
where year(timestamp) = 2011 and month(timestamp) = 1 and direction_id=1
group by day, filter
order by
        timestamp;
