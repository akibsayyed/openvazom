package net.db;

import java.sql.DriverManager;
import java.sql.ResultSet;
import java.sql.Statement;
import java.sql.Timestamp;
import java.sql.Types;
import java.util.ArrayList;
import java.util.Vector;

import com.mysql.jdbc.CommunicationsException;

import net.config.FNConfigData;
import net.logging.LoggingManager;
import net.smstpdu.SmsDirection;
import net.smstpdu.SmsType;
import net.stats.StatsManager;

public class DBManager_monetdb extends DBManager {

	public DBManager_monetdb(){
		current_db_type = DBType.MONETDB;
		
	}

	protected void init_db_params() {
		current_database_driver = "nl.cwi.monetdb.jdbc.MonetDriver";
		// main DB
		connectionString = "jdbc:monetdb://" + FNConfigData.db_host + ":" + FNConfigData.db_port + "/" +  FNConfigData.db_name;
		

	}

	public boolean _do_db_sms_insert(Vector<DBRecordSMS> lst){
		DBRecordSMS dbr = null;
		String sql;
		java.sql.PreparedStatement stat;
		Statement mstat;
		ResultSet db_res;
		//int l;
		int max_id;
		long ts;
		ArrayList<Integer> q_lst = new ArrayList<Integer>();
		try{
			ts = System.currentTimeMillis();
			// construct query with multiple VALUES blocks
			//sql =  "insert ignore into sms(direction_id, type_id, mode_id, gt_called, gt_calling, scda, scoa, imsi, msisdn, sms_destination_enc_id, sms_originating_enc_id, " +
			//"sms_text_enc_id, sms_destination, sms_originating, sms_text, m3ua_data_dpc, m3ua_data_opc, tcap_sid, tcap_did, timestamp, filter_action_id, filter_total_score) values";

			sql =  "insert into sms(direction_id, type_id, mode_id, gt_called, gt_calling, scda, scoa, imsi, msisdn, sms_destination_enc_id, sms_originating_enc_id, " +
			"sms_text_enc_id, sms_destination, sms_originating, sms_text, m3ua_data_dpc, m3ua_data_opc, tcap_sid, tcap_did, timestamp, filter_action_id, filter_total_score, " +
			"ip_source, ip_destination, tcp_source, tcp_destination, system_id, pdu_id, sms_status_id, sms_conc_partnum, sms_conc_parts, sms_conc_msgid, error_code, filter_exit_point) values" +
			"(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			// prepare statement
			stat = con.prepareStatement(sql);
			stat.clearParameters();
			
			// get max id
			mstat = con.createStatement();
			db_res = mstat.executeQuery("select max(id) from sms");
			db_res.next();
			max_id = db_res.getInt(1);
			mstat.close();
			
			for(int i = 0; i<lst.size(); i++){
				dbr = lst.get(i);
				// save id if quarantined
				if(dbr.quarantine) q_lst.add(i);
				
				stat.setInt(1, (dbr.direction == SmsDirection.MO ? 1 : 2));
				stat.setInt(2, (dbr.type == SmsType.SINGLE ? 1 : 2));
				switch(dbr.dataSource){
					case SCTP:
					case SMPP:
					case E1:
						stat.setInt(3, 1);
						break;
					default:
						stat.setInt(3, 2);
						break;
				}
				if(dbr.gt_called != null) stat.setString(4, dbr.gt_called); else stat.setNull(4, Types.VARCHAR);
				if(dbr.gt_calling != null) stat.setString(5, dbr.gt_calling); else stat.setNull(5, Types.VARCHAR);
				if(dbr.scda != null) stat.setString(6, dbr.scda); else stat.setNull(6, Types.VARCHAR);
				if(dbr.scoa != null) stat.setString(7, dbr.scoa); else stat.setNull(7, Types.VARCHAR);
				if(dbr.imsi != null) stat.setString(8, dbr.imsi); else stat.setNull(8, Types.VARCHAR);
				if(dbr.msisdn != null) stat.setString(9, dbr.msisdn); else stat.setNull(9, Types.VARCHAR);
				
				stat.setInt(10, dbr.sms_destination_enc);
				
				stat.setInt(11, dbr.sms_originating_enc);
				
				stat.setInt(12, dbr.sms_text_enc);
				if(dbr.sms_destination != null) stat.setString(13, dbr.sms_destination); else stat.setNull(13, Types.VARCHAR);
				if(dbr.sms_originating != null) stat.setString(14, dbr.sms_originating); else stat.setNull(14, Types.VARCHAR);
				if(dbr.sms_text != null) stat.setString(15, dbr.sms_text); else stat.setNull(15, Types.VARCHAR);
				stat.setInt(16, dbr.m3ua_data_dpc);
				stat.setInt(17, dbr.m3ua_data_opc);
				
				if(dbr.tcap_sid != null) stat.setLong(18, dbr.tcap_sid);
				else stat.setNull(18, Types.BIGINT);
				
				if(dbr.tcap_did != null) stat.setLong(19, dbr.tcap_did);
				else stat.setNull(19, Types.BIGINT);
				
				stat.setTimestamp(20, new Timestamp(dbr.timestamp));
				
				if(dbr.filter_action_id != null) stat.setInt(21, dbr.filter_action_id);
				else stat.setNull(21, Types.INTEGER);
				
				if(dbr.filter_total_score != null) stat.setInt(22, dbr.filter_total_score);
				else stat.setNull(22, Types.INTEGER);
				
				if(dbr.ip_source != null) stat.setString(23, dbr.ip_source);
				else stat.setNull(23, Types.VARCHAR);
				
				if(dbr.ip_destination != null) stat.setString(24, dbr.ip_destination);
				else stat.setNull(24, Types.VARCHAR);
				
				if(dbr.tcp_source != null) stat.setInt(25, dbr.tcp_source);
				else stat.setNull(25, Types.INTEGER);
				
				if(dbr.tcp_destination != null) stat.setInt(26, dbr.tcp_destination);
				else stat.setNull(26, Types.INTEGER);
				
				if(dbr.system_id != null) stat.setString(27, dbr.system_id);
				else stat.setNull(27, Types.VARCHAR);
				
				stat.setInt(28, dbr.pdu_type.getId());

				if(dbr.no_reply) stat.setNull(29, Types.INTEGER);
				else stat.setInt(29, dbr.sms_status);
				
				// SMS CONC
				if(dbr.sms_conc_partnum != null) stat.setInt(30, dbr.sms_conc_partnum);
				else stat.setNull(30, Types.INTEGER); 
	
				if(dbr.sms_conc_parts != null) stat.setInt(31, dbr.sms_conc_parts);
				else stat.setNull(31, Types.INTEGER); 
	
				if(dbr.sms_conc_msgid != null) stat.setInt(32, dbr.sms_conc_msgid);
				else stat.setNull(32, Types.INTEGER); 

				// error code
				if(dbr.sms_status != 1){
					switch(dbr.sms_status){
						// TCAP Abort error
						case 2: stat.setInt(33, dbr.tcap_abort_error_code); break; 
						// TCAP Abort dialogue error
						case 3: stat.setInt(33, dbr.tcap_dialogue_error_code); break; 
						// GSM MAP error
						case 4: stat.setInt(33, dbr.gsm_map_error_code); break; 
						// SMPP Error
						case 5: stat.setInt(33, dbr.smpp_error_code); break; 
						// unknown
						default: stat.setInt(33, 9999); break; 
					}
					
				}else stat.setNull(33, Types.INTEGER); 
				
				// filter exit point
				if(dbr.filter_exit_point != null) stat.setString(34, dbr.filter_exit_point); else stat.setNull(34, Types.VARCHAR);
			
				stat.addBatch();
			}
			stat.executeBatch();
			con.commit();

			// quarantine
			if(q_lst.size() > 0){
				// construct query with multiple VALUES blocks
				sql =  "insert into quarantine(sms_id) values(?)";
				// prepare statement
				stat = con.prepareStatement(sql);
				stat.clearParameters();
				
				for(int i = 0; i<q_lst.size(); i++){
					stat.setInt(1, max_id + q_lst.get(i) + 1);
					stat.addBatch();
					
				}
				stat.executeBatch();
				con.commit();
				stat.close();
				
			}else stat.close();
			StatsManager.DB_STATS.LAST_SMS_BATCH_ELAPSED = System.currentTimeMillis() - ts;
			StatsManager.DB_STATS.sms_setMaxTS(StatsManager.DB_STATS.LAST_SMS_BATCH_ELAPSED);
			StatsManager.DB_STATS.DB_SMS_BATCH_COUNT++;
			
			return true;
		}catch(CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			_connect();
			return false;
			//do_db_sms_insert(lst);
		}catch(com.mysql.jdbc.exceptions.jdbc4.CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			_connect();
			return false;
		}catch(Exception e){
			e.printStackTrace();
			/*
			for(int i = 0; i<lst.size(); i++){
				System.out.println("-------- SMS " + i + " ----------------");
				dbr = lst.get(i);
				if(dbr != null){
					System.out.println("direction: " + dbr.direction);
					System.out.println("type: " + dbr.type);
					System.out.println("pdu_type: " + dbr.pdu_type);
					System.out.println("ip_source: " + dbr.ip_source);
					System.out.println("ip_destination: " + dbr.ip_destination);
					System.out.println("tcp_source: " + dbr.tcp_source);
					System.out.println("tcp_destination: " + dbr.tcp_destination);
					System.out.println("system_id: " + dbr.system_id);
					System.out.println("sms_conc_partnum: " + dbr.sms_conc_partnum);
					System.out.println("sms_conc_parts: " + dbr.sms_conc_parts);
					System.out.println("sms_conc_msgid: " + dbr.sms_conc_msgid);
					System.out.println("gt_called: " + dbr.gt_called);
					System.out.println("gt_calling: " + dbr.gt_calling);
					System.out.println("gt_called_tt: " + dbr.gt_called_tt);
					System.out.println("gt_calling_tt: " + dbr.gt_calling_tt);
					System.out.println("gt_called_nai: " + dbr.gt_called_nai);
					System.out.println("gt_calling_nai: " + dbr.gt_calling_nai);
					System.out.println("gt_called_np: " + dbr.gt_called_np);
					System.out.println("gt_calling_np: " + dbr.gt_calling_np);
					System.out.println("gt_called_gti: " + dbr.gt_called_gti);
					System.out.println("gt_calling_gti: " + dbr.gt_calling_gti);
					System.out.println("scda: " + dbr.scda);
					System.out.println("scoa: " + dbr.scoa);
					System.out.println("imsi: " + dbr.imsi);
					System.out.println("msisdn: " + dbr.msisdn);
					System.out.println("sms_destination_enc: " + dbr.sms_destination_enc);
					System.out.println("sms_originating_enc: " + dbr.sms_originating_enc);
					System.out.println("sms_text_enc: " + dbr.sms_text_enc);
					System.out.println("sms_destination: " + dbr.sms_destination);
					System.out.println("sms_originating: " + dbr.sms_originating);
					System.out.println("sms_text: " + dbr.sms_text);
					System.out.println("m3ua_data_dpc: " + dbr.m3ua_data_dpc);
					System.out.println("m3ua_data_opc: " + dbr.m3ua_data_opc);
					System.out.println("tcap_sid: " + dbr.tcap_sid);
					System.out.println("tcap_did: " + dbr.tcap_did);
					System.out.println("id: " + dbr.id);
					System.out.println("timestamp: " + dbr.timestamp);
					System.out.println("dataSource: " + dbr.dataSource);
					System.out.println("filter_action_id: " + dbr.filter_action_id);
					System.out.println("filter_total_score: " + dbr.filter_total_score);
				}
			}
			*/
			// unknown error, drop current batch to prevent infinite error loop
			return true;
		}
		
	}

	protected void _connect() {
		try{
			//con = DriverManager.getConnection(connectionString);
			con = DriverManager.getConnection(connectionString, FNConfigData.db_username, FNConfigData.db_password);
			if(con != null){
				LoggingManager.info(logger, "Main DB connection intialized, connection string = [" + connectionString + "]!");

				con.setAutoCommit(false);
			}
		}catch(Exception e){
			logger.warn("Cannot connect to DB, connection parameters = [" + connectionString + "]!");
			con = null;
			//e.printStackTrace();
		}
		
	}

	public boolean _do_db_sri_insert(Vector<DBRecordSRI> lst) {
		DBRecordSRI dbr = null;
		String sql;
		java.sql.PreparedStatement stat;
		long ts;
		try{
			ts = System.currentTimeMillis();
			// construct query with multiple VALUES blocks
			sql =  "insert into sri(" +
											"sri_type_id, " +
											"m3ua_data_opc, " +
											"m3ua_data_dpc, " +
											"gt_called, " +
											"gt_calling, " +
											"tcap_sid, " +
											"tcap_did, " +
											"app_ctx_oid, " +
											"imsi, " +
											"msisdn, " +
											"msisdn_nai, " +
											"nnn, " +
											"annn, " +
											"sca, " +
											"timestamp, " +
											"error_code, " +
											"no_reply, " +
											"tcap_abort_error_code, " +
											"tcap_dialogue_error_code, " +
											"gsmc, " +
											"gsmc_nai, " +
											"vmsc, " +
											"vmsc_nai, " +
											"roaming_number, " +
											"roaming_number_nai, " +
											"forwarded_to_number, " +
											"forwarded_to_number_nai) values"
											+ "(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			// prepare statement
			stat = con.prepareStatement(sql);
			stat.clearParameters();
			
			for(int i = 0; i<lst.size(); i++){
				dbr = lst.get(i);
				stat.setInt(1, dbr.routing_type);
				stat.setInt(2, dbr.m3ua_data_opc);
				stat.setInt(3, dbr.m3ua_data_dpc);
				if(dbr.gt_called != null) stat.setString(4, dbr.gt_called); else stat.setNull(4, Types.VARCHAR);
				if(dbr.gt_calling != null) stat.setString(5, dbr.gt_calling); else stat.setNull(5, Types.VARCHAR);
				stat.setLong(6, (dbr.tcap_sid == null ? 0 : dbr.tcap_sid));
				stat.setLong(7, (dbr.tcap_did == null ? 0 : dbr.tcap_did));
				if(dbr.app_ctx_oid != null) stat.setString(8, dbr.app_ctx_oid); else stat.setNull(8, Types.VARCHAR);
				if(dbr.imsi != null) stat.setString(9, dbr.imsi); else stat.setNull(9, Types.VARCHAR);
				if(dbr.msisdn != null) stat.setString(10, dbr.msisdn); else stat.setNull(10, Types.VARCHAR);
				stat.setInt(11, dbr.msisdn_nai);
				if(dbr.nnn != null) stat.setString(12, dbr.nnn); else stat.setNull(12, Types.VARCHAR);
				if(dbr.annn != null) stat.setString(13, dbr.annn); else stat.setNull(13, Types.VARCHAR);
				if(dbr.sca != null) stat.setString(14, dbr.sca); else stat.setNull(14, Types.VARCHAR);
				stat.setTimestamp(15, new Timestamp(dbr.timestamp));
				// Error Code
				if(dbr.gsm_map_error_code == null) stat.setNull(16, Types.INTEGER);
				else stat.setInt(16, dbr.gsm_map_error_code);
				
				// No reply
				if(dbr.no_reply) stat.setBoolean(17, dbr.no_reply);
				else stat.setNull(17, Types.BOOLEAN);
				
				// TCAP Abort Error
				if(dbr.tcap_abort_error_code != null) stat.setInt(18, dbr.tcap_abort_error_code);
				else stat.setNull(18, Types.INTEGER);
				
				// TCAP Abort Dialogue Error
				if(dbr.tcap_dialogue_error_code != null) stat.setInt(19, dbr.tcap_dialogue_error_code);
				else stat.setNull(19, Types.INTEGER);
				
				if(dbr.gsmc != null) stat.setString(20, dbr.gsmc); else stat.setNull(20, Types.VARCHAR);
				stat.setInt(21, dbr.gsmc_nai);
				
				if(dbr.vmsc != null) stat.setString(22, dbr.vmsc); else stat.setNull(22, Types.VARCHAR);
				stat.setInt(23, dbr.vmsc_nai);

				if(dbr.roaming_number != null) stat.setString(24, dbr.roaming_number); else stat.setNull(24, Types.VARCHAR);
				stat.setInt(25, dbr.roaming_number_nai);
				
				if(dbr.fwd_to_number != null) stat.setString(26, dbr.fwd_to_number); else stat.setNull(26, Types.VARCHAR);
				stat.setInt(27, dbr.fwd_to_number_nai);
				
				stat.addBatch();
				
				
			}
			stat.executeBatch();
			con.commit();
			stat.close();
			StatsManager.DB_STATS.LAST_SRI_BATCH_ELAPSED = System.currentTimeMillis() - ts;
			StatsManager.DB_STATS.sri_setMaxTS(StatsManager.DB_STATS.LAST_SRI_BATCH_ELAPSED);
			StatsManager.DB_STATS.DB_SRI_BATCH_COUNT++;
			return true;
		}catch(CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			_connect();
			return false;
			//do_db_sri_insert(lst);
		}catch(com.mysql.jdbc.exceptions.jdbc4.CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			_connect();
			return false;
			
		}catch(Exception e){
			e.printStackTrace();
			// unknown error, drop batch to prevent infinite error loop
			return true;
		}
		
	}

	public boolean _do_db_isup_insert(Vector<DBRecordISUP> lst) {
		DBRecordISUP dbr = null;
		String sql;
		java.sql.PreparedStatement stat;
		long ts;
		try{
			ts = System.currentTimeMillis();
			// construct query with multiple VALUES blocks
			sql =  "insert into voice(" +
											"dpc, " +
											"opc, " +
											"cic, " +
											"called_party, " +
											"called_party_nai, " +
											"calling_party, " +
											"calling_party_nai, " +
											"ringing_duration, " +
											"call_duration, " +
											"release_cause_code, " +
											"release_cause_class_code," +
											"timestamp) values(?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?, ?)";

			// prepare statement
			stat = con.prepareStatement(sql);
			stat.clearParameters();
			for(int i = 0; i<lst.size(); i++){
				dbr = lst.get(i);
				stat.setInt(1, dbr.dpc);
				stat.setInt(2, dbr.opc);
				stat.setInt(3, dbr.cic);

				if(dbr.called_party != null) stat.setString(4, dbr.called_party); else stat.setNull(4, Types.VARCHAR);
				if(dbr.called_party_nai != null) stat.setInt(5, dbr.called_party_nai);
				else stat.setNull(5, Types.INTEGER);

				if(dbr.calling_party != null) stat.setString(6, dbr.calling_party); else stat.setNull(6, Types.VARCHAR);
				if(dbr.calling_party_nai != null) stat.setInt(7, dbr.calling_party_nai);
				else stat.setNull(7, Types.INTEGER);
				
				stat.setLong(8, dbr.ringing_duration);
				stat.setLong(9, dbr.call_duration);
				stat.setInt(10, dbr.release_cause);
				stat.setInt(11, dbr.release_cause_class);
				stat.setTimestamp(12, new Timestamp(dbr.timestamp));
				
				stat.addBatch();
				
			}			
			stat.executeBatch();
			con.commit();
			stat.close();
			StatsManager.DB_STATS.LAST_ISUP_BATCH_ELAPSED = System.currentTimeMillis() - ts;
			StatsManager.DB_STATS.isup_setMaxTS(StatsManager.DB_STATS.LAST_ISUP_BATCH_ELAPSED);
			StatsManager.DB_STATS.DB_ISUP_BATCH_COUNT++;
			return true;
			
		}catch(CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			_connect();
			return false;
			//do_db_sri_insert(lst);
		}catch(com.mysql.jdbc.exceptions.jdbc4.CommunicationsException e){
			StatsManager.DB_STATS.DB_CONNECTION_LOST++;
			_connect();
			return false;
			
		}catch(Exception e){
			e.printStackTrace();
			// unknown error, drop batch to prevent infinite error loop
			return true;
		}
	}	


}
