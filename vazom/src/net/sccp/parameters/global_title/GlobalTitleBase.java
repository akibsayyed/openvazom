package net.sccp.parameters.global_title;


public abstract class GlobalTitleBase {
	public GlobalTitleIndicator type;
	
	public static GlobalTitleBase prepareNew(GlobalTitleIndicator type){
		GlobalTitleBase res = null;
		switch(type){
			case NATURE_OF_ADDRESS_INDICATOR_ONLY:
				res = new GlobalTitle_NOA();
				break;
			case TRANSLATION_TYPE_ONLY:
				res = new GlobalTitle_TT();
				break;
			case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING:
				res = new GlobalTitle_TTNPE();
				break;
			case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
				res = new GlobalTitle_TTNPENOA();
				break;
		
		}

		return res;
	}
	public abstract byte[] encode();
		//byte[] res = null;
		//return res;
	//}
	public static GlobalTitleBase decode(byte[] data, GlobalTitleIndicator gi){
		GlobalTitleBase res = null;
		if(gi != null){
			switch(gi){
				case NATURE_OF_ADDRESS_INDICATOR_ONLY:
					res = new GlobalTitle_NOA();
					res.init(data);
					break;
				case TRANSLATION_TYPE_ONLY:
					res = new GlobalTitle_TT();
					res.init(data);
					break;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING:
					res = new GlobalTitle_TTNPE();
					res.init(data);
					break;
				case TRANSLATION_TYPE_NUMNBERING_PLAN_ENCODING_NATURE_OF_ADDRESS:
					res = new GlobalTitle_TTNPENOA();
					res.init(data);
					break;
			}
			
		}
		return res;
	}

	public abstract void init(byte[] data);
		
	
	
}
