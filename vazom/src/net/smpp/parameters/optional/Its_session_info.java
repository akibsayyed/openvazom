package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Its_session_info extends OptionalParameterBase {

	public Its_session_info(){
		type = OptionalParameterType.ITS_SESSION_INFO;
	}
	public int sessionNumber;
	public int sequenceNumber;
	public boolean endOfSessionIndicator;
	public void init(byte[] data) {
		sessionNumber = data[0] & 0xff;
		sequenceNumber = (data[1] & 0xfe) >> 1;
		endOfSessionIndicator = (data[1] & 0x01) == 0x01;
	}

}
