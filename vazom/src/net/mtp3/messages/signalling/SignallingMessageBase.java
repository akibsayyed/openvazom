package net.mtp3.messages.signalling;

import net.mtp3.messages.MessageBase;
import net.mtp3.messages.MessageType;
import net.mtp3.messages.SignallingMessageGroupType;
import net.mtp3.messages.SignallingMessageType;

public abstract class SignallingMessageBase extends MessageBase {
	public SignallingMessageGroupType signallingGroupType;
	public SignallingMessageType signallingType;
	
	public SignallingMessageBase(){
		type = MessageType.SIGNALLING;
	}
	public static SignallingMessageType decodeType(byte first_octet){
		SignallingMessageGroupType tmp_group = SignallingMessageGroupType.get(first_octet & 0x0f);
		SignallingMessageType tmp_type = SignallingMessageType.get((first_octet & 0xf0) >> 4, tmp_group.getId());
		return tmp_type;
	}
	public static SignallingMessageBase decode(byte[] data){
		SignallingMessageBase res = null;
		SignallingMessageType type = decodeType(data[0]);
		switch(type){
			case COA:
			case COO: res = new Changeover(); break;
			
			case CBA:
			case CBD: res = new Changeback(); break;
			
			case ECO:
			case ECA: res = new EmergencyChangeover(); break;
			
			case RCT: res = new SignallingRouteSetCongestionTestMessage(); break;
			case TFC: res = new TransferControlled(); break;
			
			case TFP: res = new TransferProhibited(); break;
			case TFR: res = new TransferRestrictedNational(); break;
			case TFA: res = new TransferAllowed(); break;
			
			case RST: res = new SignallingRouteSetTest(); break;
			case RSR: res = new SignallingRouteSetTest(); break;
			
			case TRA: res = new TrafficRestartAllowed(); break;
			
			case DLC: res = new SignallingDataLinkConnectionOrder(); break;
			case UPU: res = new UserPartUnavailable(); break;
		}
		
		return res;
	}
	
	public void init(byte[] data) {
		// H0
		signallingGroupType = SignallingMessageGroupType.get(data[byte_pos] & 0x0f);
		// H1
		signallingType = SignallingMessageType.get((data[byte_pos] & 0xf0) >> 4, signallingGroupType.getId());
		
		byte_pos++;
		
	}

}
