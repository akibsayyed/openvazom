package net.test;

import java.io.File;
import java.io.FileInputStream;
import java.util.ArrayList;

import net.mtp2.MTP2;
import net.mtp2.MTP2Packet;
import net.mtp2.messages.MSU;
import net.mtp3.MTP3;
import net.mtp3.MTP3Packet;
import net.mtp3.messages.signalling_mtn.SignallingMessageBaseMTN;
import net.spcap.SLLHeader;
import net.spcap.SPcap;
import net.utils.Utils;



public class mtp2test {
	
	public static void main(String[] args) {
		File f = new File("/tmp/tmp.raw");
		byte[] data = new byte[(int)f.length()];
		int i = 0;
		try{
			FileInputStream fi = new FileInputStream(f);
			int a;
			while( (a = fi.read()) != -1){
				data[i++] = (byte)a;
			}
		}catch(Exception e){
			e.printStackTrace();
		}
		
		
		MTP2Packet mp = MTP2.decode(data);
		System.out.println(mp.message.type);
		MTP3Packet mtp3 = MTP3.decode(((MSU)mp.message).data);
		System.out.println(((SignallingMessageBaseMTN)mtp3.message).signallingType);
		//System.out.println(Utils.bytes2hexstr(((MSU)mp.message).data, " "));
	}
	
}
