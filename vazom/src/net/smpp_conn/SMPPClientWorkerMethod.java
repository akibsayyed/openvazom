package net.smpp_conn;

import net.routing.RoutingConnectionWorkerMethod;
import net.smpp.ErrorCodeType;
import net.smpp.PDUType;
import net.smpp.SMPP_PDU;
import net.smpp.pdu.Enquire_link_resp;
import net.smpp.pdu.PDUBase;

public class SMPPClientWorkerMethod extends RoutingConnectionWorkerMethod {
	SMPPConnection smpp_conn = null;
	
	public SMPPClientWorkerMethod(SMPPConnection conn){
		super(conn);
		smpp_conn = (SMPPConnection)conn;

	}
	
	public void execute(Object o) {
		byte[] data = null;
		SMPPPayload spl = null;
		SMPP_In_Packet in_packet = null;
		Enquire_link_resp el = null;
		if(o != null){
			in_packet = (SMPP_In_Packet)o;
			data = in_packet.data;
			PDUBase[] pdu = SMPP_PDU.decode(data);
			for(int i = 0; i<pdu.length; i++){
				if(pdu[i].type == PDUType.ENQUIRE_LINK){
					el = new Enquire_link_resp();
					el.sequence_number = pdu[i].sequence_number;
					el.command_status = ErrorCodeType.ESME_ROK;
					smpp_conn.sequence_number = pdu[i].sequence_number + 1;
					
					// send to out queue
					spl = new SMPPPayload();
					spl.setParams(new Object[]{el, el.encode()});
					conn.out_offer(spl);
					

				}else if(pdu[i].type == PDUType.ENQUIRE_LINK_RESP){
					if(pdu[i].command_status != ErrorCodeType.ESME_ROK){
						conn.stop();
					}
				}else if(pdu[i].type == PDUType.UNBIND_RESP){
					smpp_conn.active = false;
					smpp_conn.stopping = true;
					try{
						smpp_conn.socket.close();
					}catch(Exception e){}
					
				}
			}
			
			
		}
		
	}

}
