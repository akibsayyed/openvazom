package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudSignalsDescriptor extends CHOICE{

	public IndAudSignal get_signal(){
		return (IndAudSignal)elements.get(0).data;
	}
	public IndAudSignal new_signal(){
		return new IndAudSignal();
	}

	public IndAudSeqSigList get_seqSigList(){
		return (IndAudSeqSigList)elements.get(1).data;
	}
	public IndAudSeqSigList new_seqSigList(){
		return new IndAudSeqSigList();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_signal(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_seqSigList(); }});
	}
	public IndAudSignalsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
