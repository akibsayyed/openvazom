package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CDMAChannelNumberListInner extends SEQUENCE{

	public CDMAChannelNumber get_cdmaChannelNumber(){
		return (CDMAChannelNumber)elements.get(0).data;
	}
	public CDMAChannelNumber new_cdmaChannelNumber(){
		return new CDMAChannelNumber();
	}

	public CDMAChannelNumber get_cdmaChannelNumber2(){
		return (CDMAChannelNumber)elements.get(1).data;
	}
	public CDMAChannelNumber new_cdmaChannelNumber2(){
		return new CDMAChannelNumber();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(226, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaChannelNumber(); }});
		elements.add(new ElementDescriptor(226, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_cdmaChannelNumber2(); }});
	}
	public CDMAChannelNumberListInner(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
