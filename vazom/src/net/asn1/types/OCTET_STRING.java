package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class OCTET_STRING extends ASNType {

	public OCTET_STRING(){
		super();
		universalTag = ASNTag.OCTET_STRING;
		tag = ASNTag.name2Tag(universalTag.toString());
		
	}
}
