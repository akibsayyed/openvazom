package net.fn;

import java.util.concurrent.ConcurrentLinkedQueue;
import net.config.ConfigManagerV2;
import net.config.FNConfigData;
import net.db.DBManager;
import net.db.DBType;
import net.dr.DRManager;
import net.flood.FloodManager;
import net.fn.cli.CLIService;
import net.fn.cli.VSTP_CLIService;
import net.gpu2.GPUBalancer;
import net.hlr.HLRManager;
import net.logging.LoggingManager;
import net.security.SecurityManager;
import net.smsfs.SmsfsManager;
import net.smsfs.bindings.Bindings;
import net.smsfs.list.ListManagerV2;
import net.stats.StatsManager;
import net.stats.StatsMonitor;
import net.vstp.MessageDescriptor;

import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;

public class FNode {
	private static Logger logger;
	private static ConcurrentLinkedQueue<MessageDescriptor> out_queue;
	private static ConcurrentLinkedQueue<MessageDescriptor> in_queue;
	
	private void initConfig(){
		try{
			// flood
			FNConfigData.flood_status = (ConfigManagerV2.get("flood.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.flood_max_list_size = Integer.parseInt(ConfigManagerV2.get("flood.max.list.size"));
			
			
			// vstp cmdi
			FNConfigData.vstp_cmdi_port = Integer.parseInt(ConfigManagerV2.get("vstp.cmdi.port"));
			
			// hlr
			FNConfigData.hlr_timeout_check_interval = Integer.parseInt(ConfigManagerV2.get("hlr.timeout.check.interval"))*1000;
			FNConfigData.hlr_timeout = Integer.parseInt(ConfigManagerV2.get("hlr.timeout"))*1000;

			// config
			FNConfigData.fn_id = ConfigManagerV2.get("fn.id");
			FNConfigData.fgn_debug = (ConfigManagerV2.get("fgn.debug").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.fn_global_max_queue_size = Integer.parseInt(ConfigManagerV2.get("fn.global.max.queue.size"));
			FNConfigData.sg_workers = Integer.parseInt(ConfigManagerV2.get("sg.workers"));
			FNConfigData.sg_reconnect_status = (ConfigManagerV2.get("sg.reconnect.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.sg_reconnect_interval = Integer.parseInt(ConfigManagerV2.get("sg.reconnect.interval")) * 1000;
			FNConfigData.smsfs_status = (ConfigManagerV2.get("smsfs.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.smsfs_sms_intrusive_status = (ConfigManagerV2.get("smsfs.sms.intrusive.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.smsfs_sms_non_intrusive_status = (ConfigManagerV2.get("smsfs.sms.non_intrusive.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.smsfs_lists_sync_interval = Integer.parseInt(ConfigManagerV2.get("smsfs.lists.sync.interval")) * 1000;
			FNConfigData.smsfs_lists_max = Integer.parseInt(ConfigManagerV2.get("smsfs.lists.max"));

			FNConfigData.smsfs_hlr_intrusive_status = (ConfigManagerV2.get("smsfs.hlr.intrusive.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.smsfs_hlr_non_intrusive_status = (ConfigManagerV2.get("smsfs.hlr.non_intrusive.status").equalsIgnoreCase("ON") ? true : false);

			FNConfigData.smsfs_isup_intrusive_status = (ConfigManagerV2.get("smsfs.isup.intrusive.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.smsfs_isup_non_intrusive_status = (ConfigManagerV2.get("smsfs.isup.non_intrusive.status").equalsIgnoreCase("ON") ? true : false);
			
			// DR
			FNConfigData.dr_status = (ConfigManagerV2.get("dr.status").equalsIgnoreCase("ON") ? true : false);

			FNConfigData.dr_intrusive_sms_status = (ConfigManagerV2.get("dr.intrusive.sms.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_intrusive_hlr_status = (ConfigManagerV2.get("dr.intrusive.hlr.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_intrusive_isup_status = (ConfigManagerV2.get("dr.intrusive.isup.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_non_intrusive_sms_status = (ConfigManagerV2.get("dr.non_intrusive.sms.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_non_intrusive_hlr_status = (ConfigManagerV2.get("dr.non_intrusive.hlr.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_non_intrusive_isup_status = (ConfigManagerV2.get("dr.non_intrusive.isup.status").equalsIgnoreCase("ON") ? true : false);
				
			FNConfigData.dr_sri_errors_status = (ConfigManagerV2.get("dr.sri.errors.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_sms_errors_status = (ConfigManagerV2.get("dr.sms.errors.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.dr_errors_check_interval = Integer.parseInt(ConfigManagerV2.get("dr.errors.check.interval")) * 1000;
			FNConfigData.dr_errors_timeout = Integer.parseInt(ConfigManagerV2.get("dr.errors.timeout")) * 1000;
			FNConfigData.dr_isup_timeout = Integer.parseInt(ConfigManagerV2.get("dr.isup.timeout")) * 1000;



			
			FNConfigData.max_total_score = Integer.parseInt(ConfigManagerV2.get("smsfs.max.total.score"));
			
			FNConfigData.db_packet_size = Integer.parseInt(ConfigManagerV2.get("db.packet.size"));
			FNConfigData.db_packet_sri_size = Integer.parseInt(ConfigManagerV2.get("db.packet.sri.size"));
			FNConfigData.db_packet_isup_size = Integer.parseInt(ConfigManagerV2.get("db.packet.isup.size"));
			FNConfigData.db_type = DBType.valueOf(ConfigManagerV2.get("db.type").toUpperCase());
			FNConfigData.db_host = ConfigManagerV2.get("db.host");
			FNConfigData.db_port = Integer.parseInt(ConfigManagerV2.get("db.port"));
			FNConfigData.db_name = ConfigManagerV2.get("db.name");
			FNConfigData.db_username = ConfigManagerV2.get("db.username");
			FNConfigData.db_password = ConfigManagerV2.get("db.password");
			FNConfigData.db_connection_pool_size = Integer.parseInt(ConfigManagerV2.get("db.connection.pool.size"));
			
			//FNConfigData.db_sim_host = ConfigManagerV2.get("db.sim.host");
			//FNConfigData.db_sim_port = Integer.parseInt(ConfigManagerV2.get("db.sim.port"));
			//FNConfigData.db_sim_name = ConfigManagerV2.get("db.sim.name");
			//FNConfigData.db_sim_username = ConfigManagerV2.get("db.sim.username");
			//FNConfigData.db_sim_password = ConfigManagerV2.get("db.sim.password");
			
			
			FNConfigData.smsfs_filter = ConfigManagerV2.get("smsfs.filter");
			FNConfigData.smsfs_spam_max = Integer.parseInt(ConfigManagerV2.get("smsfs.spam.max"));
			FNConfigData.smsfs_quarantine_max = Integer.parseInt(ConfigManagerV2.get("smsfs.quarantine.max"));
			FNConfigData.smsfs_md5_max = Integer.parseInt(ConfigManagerV2.get("smsfs.md5.max"));
			FNConfigData.smsfs_goto_loop_max = Integer.parseInt(ConfigManagerV2.get("smsfs.goto.loop.max"));

			FNConfigData.cmdi_port = Integer.parseInt(ConfigManagerV2.get("cmdi.port"));
			FNConfigData.smsfs_workers = Integer.parseInt(ConfigManagerV2.get("smsfs.workers"));
			FNConfigData.dr_workers = Integer.parseInt(ConfigManagerV2.get("dr.workers"));
			// STATS
			FNConfigData.stats_monitor_status = (ConfigManagerV2.get("stats.monitor.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.stats_monitor_buffer = Integer.parseInt(ConfigManagerV2.get("stats.monitor.buffer")) * 86400;

			// gpub
			FNConfigData.gpub_status = (ConfigManagerV2.get("gpub.status").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.gpub_debug = (ConfigManagerV2.get("gpub.debug").equalsIgnoreCase("ON") ? true : false);
			FNConfigData.gpub_ld_min_length = Integer.parseInt(ConfigManagerV2.get("gpub.ld.min.length"));
			FNConfigData.gpub_ld_max_length = Integer.parseInt(ConfigManagerV2.get("gpub.ld.max.length"));

			// Refilter
			FNConfigData.refilter_workers = Integer.parseInt(ConfigManagerV2.get("refilter.workers"));
			FNConfigData.refilter_batch_size = Integer.parseInt(ConfigManagerV2.get("refilter.batch.size"));

		}catch(Exception e){
			e.printStackTrace();
			logger.error("Invalid configuration file...");
			System.exit(0);
		}
	}
	public static MessageDescriptor out_poll(){
		return out_queue.poll();
	}
	public static void out_offer(MessageDescriptor  md){
		if(out_queue.size() < FNConfigData.fn_global_max_queue_size) out_queue.offer(md);
		else{
			StatsManager.SYSTEM_STATS.FGN_OUT_QUEUE_MAX++;
			//logger.warn("FNode.out_queue: maximum queue size reached: [" + FNConfigData.fn_global_max_queue_size + "]!");
		}
	}
	public static void in_offer(MessageDescriptor  md){
		if(in_queue.size() < FNConfigData.fn_global_max_queue_size) in_queue.offer(md);
		else{
			StatsManager.SYSTEM_STATS.FGN_IN_QUEUE_MAX++;
			//logger.warn("FNode.in_queue: maximum queue size reached: [" + FNConfigData.fn_global_max_queue_size + "]!");
		}
	}
	public static MessageDescriptor in_poll(){
		return in_queue.poll();
	}
	
	
	public FNode(){
		String tmp;
		String[] tmp_lst;
		String tmp_name;
		SGDescriptor sgd = null;
		
		logger.info("Starting Filter Node...");
		// logging
		LoggingManager.init();

		// security
		/*
		SecurityManager.init();
		SecurityManager.set_critical_feature(SecurityManager.FEATURE_FGN);
		if(!SecurityManager.check_feature(SecurityManager.FEATURE_FGN)){
			LoggingManager.error(logger, "Invalid license, shutting down!");
			System.exit(0);
			
		}
		*/
		
		ConfigManagerV2.init("conf/fn.properties");
		logger.info("Loadig config file...");
		initConfig();

		
		in_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		out_queue = new ConcurrentLinkedQueue<MessageDescriptor>();
		//sg_conn_map = new ConcurrentHashMap<String, SGDescriptor>();

		// SGN manager
		SGNManager.init();
		
		// DR Manager
		if(FNConfigData.dr_status){
			DRManager.init();
			
		}		

		// Stats manager
		StatsManager.init();
		// Stats monitor
		if(FNConfigData.stats_monitor_status) StatsMonitor.init();
		
		// SMSFS
		//ListManager.init();
		ListManagerV2.init();
		SmsfsManager.init();
		SmsfsManager.filterActivate(FNConfigData.smsfs_filter);
		//SmsfsManager.currentMOfinished = ConfigManager.getRaw(FNConfigData.mo_finished);
		//SmsfsManager.currentMTfinished = ConfigManager.getRaw(FNConfigData.mt_finished);
		// set current simulation filter to current acive filter
		SmsfsManager.simulation_filter_data = SmsfsManager.current_filter_data;
		SmsfsManager.simulationFilterMO = SmsfsManager.currentFilterMO;
		SmsfsManager.simulationFilterMT = SmsfsManager.currentFilterMT;
		SmsfsManager.simulationFilterSMPP_MO = SmsfsManager.currentFilterSMPP_MO;
		SmsfsManager.simulationFilterSMPP_MT = SmsfsManager.currentFilterSMPP_MT;
		SmsfsManager.simulationFilterHLR = SmsfsManager.currentFilterHLR;
		//ConfigManagerV2.getList(FNConfigData.smsfs_sccp_gt_called_wl, SmsfsManager.sccp_gt_called_wl);
		//ConfigManagerV2.getList(FNConfigData.smsfs_sccp_gt_calling_wl, SmsfsManager.sccp_gt_calling_wl);
		//ConfigManagerV2.getList(FNConfigData.smsfs_map_scda_wl, SmsfsManager.map_scda_wl);
		//ConfigManagerV2.getList(FNConfigData.smsfs_map_scoa_wl, SmsfsManager.map_scoa_wl);

		
		DBManager.init(FNConfigData.db_type);
		Bindings.init();
		//SpamDict.init();
		//SpamManager.init(FNConfigData.smsfs_spam_max);
		//QuarantineManager.init(FNConfigData.smsfs_quarantine_max);
		//MD5Manager.init(FNConfigData.smsfs_md5_max);
		
		// send spam lst to cuda devices
		/*
		if(FNConfigData.gpub_status){
			ArrayList<String> c_tmp_lst = SpamManager.getSpamLst();
			if(c_tmp_lst.size() > 0){
				logger.info("Sending known SPAM list to GPU nodes...");
				for(int i = 0; i<c_tmp_lst.size(); i++) GPUBalancer.update(CudaManager.LD_SPAM, c_tmp_lst.get(i));
				
			}
			
		}
		*/
		HLRManager.init(FNConfigData.hlr_timeout_check_interval, FNConfigData.hlr_timeout);
		
		// CLI
		CLIService.init(FNConfigData.cmdi_port);
		//CMDIListener.init(FNConfigData.cmdi_port);
		
		// VSTP CLI
		VSTP_CLIService.init(FNConfigData.vstp_cmdi_port);
		//VSTP_CMDIManager.init();
		
		// Feture check for FEATURE_GPU_BALANCER
		//if(SecurityManager.check_feature(SecurityManager.FEATURE_GPU_BALANCER)){
			// GPUB
			if(FNConfigData.gpub_status) GPUBalancer.init();
		//}else FNConfigData.gpub_status = false;

		// Flood Manager
		FloodManager.init();
		
		// SG nodes
		tmp = ConfigManagerV2.get("sg.nodes");
		logger.info("SG Nodes: [" + tmp + "]!");
		tmp_lst = tmp.split(",");
		for(int i = 0; i<tmp_lst.length; i++) if(tmp_lst[i].trim().length() > 0){
			tmp_name = tmp_lst[i].trim();
			sgd = new SGDescriptor(	tmp_name, 
									ConfigManagerV2.get("sg.nodes." + tmp_name + ".address"), 
									Integer.parseInt(ConfigManagerV2.get("sg.nodes." + tmp_name + ".port")));
			SGNManager.addNode(tmp_name, sgd);
			
			
		}
		// SG workers
		logger.info("Number of SG workers = [" + FNConfigData.sg_workers + "]!");
		for(int i = 0; i<FNConfigData.sg_workers; i++) new SGWorker(i);
		
		
	}
	public static void main(String[] args) {
		PropertyConfigurator.configure(FNode.class.getResource("/log4j.fn.properties"));
		logger = Logger.getLogger(FNode.class);
		new FNode();
		Runtime.getRuntime().addShutdownHook(new Thread(){
			public void run(){
				try{ Thread.sleep(5000); }catch(Exception e){e.printStackTrace();}
				logger.info("Shutdown complete...");
			}
		});

	}

}
