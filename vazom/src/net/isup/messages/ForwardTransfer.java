package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;

public class ForwardTransfer extends MessageBase {
	public ForwardTransfer(){
		super();
		type = MessageType.FORWARD_TRANSFER;
		pointers = new Pointer[1];
		
	}

	public void init(byte[] data) {
		if(data != null){
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			processOptional(data, 0);
			
			
		}
		 
	}

}
