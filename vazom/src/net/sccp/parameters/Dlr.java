package net.sccp.parameters;

public class Dlr extends ParamBase {
	private int value;
	
	public Dlr() {
		super();
		type = ParamType.DESTINATION_LOCAL_REFERENCE;
	}

	public int getValue(){ return value; }
	public void setValue(int val){ value = val; }

	// 3 octet field
	public byte[] encode(){
		byte[] res = new byte[3];
		res[0] = (byte)(value >> 16);
		res[1] = (byte)(value >> 8);
		res[2] = (byte)(value);
		return res;
		
	}

	public void init(byte[] data) {
		value = (data[0] << 16) + (data[1] << 8) + data[2];
		
	}
	
}
