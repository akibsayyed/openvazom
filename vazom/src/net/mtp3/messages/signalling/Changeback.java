package net.mtp3.messages.signalling;

public class Changeback extends SignallingMessageBase {
	public int changeBackCode;
	
	public Changeback(){
		super();
	}
	
	public void init(byte[] data) {
		super.init(data);
		changeBackCode = data[byte_pos];
	}
}
