package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.fci.EndToEndMethodIndicatorType;
import net.isup.parameters.fci.ISDNUserParRefIndicatorType;
import net.isup.parameters.fci.NationalInternationlCallIndicatorType;
import net.isup.parameters.fci.SCCPMethodIndicatorType;

public class ForwardCallIndicators extends ParameterBase {
	public NationalInternationlCallIndicatorType national_internationl_call_indicator;
	public EndToEndMethodIndicatorType end_to_end_method_indicator;
	public boolean interworking_encountered;
	public boolean end_to_end_information_available;
	public boolean isdn_part_used_all_the_way;
	public ISDNUserParRefIndicatorType isdn_user_part_reference_indicator;
	public boolean originating_access_non_isdn;
	public boolean originating_access_isd;
	public SCCPMethodIndicatorType sccp_method_indicator;
	
	public ForwardCallIndicators(){
		type = ParameterType.FORWARD_CALL_INDICATORS;
	}

	public void init(byte[] data) {
		if(data != null){
			national_internationl_call_indicator = NationalInternationlCallIndicatorType.get(data[byte_position] & 0x01);
			end_to_end_method_indicator = EndToEndMethodIndicatorType.get(data[byte_position] & 0x06);
			interworking_encountered = (data[byte_position] & 0x08) == 0x08;
			end_to_end_information_available = (data[byte_position] & 0x10) == 0x10;
			isdn_part_used_all_the_way = (data[byte_position] & 0x20) == 0x20;
			isdn_user_part_reference_indicator = ISDNUserParRefIndicatorType.get(data[byte_position] & 0xc0);
			byte_position++;
			originating_access_non_isdn = (data[byte_position] & 0x01) == 0x00;
			originating_access_isd = (data[byte_position] & 0x01) == 0x01;
			sccp_method_indicator = SCCPMethodIndicatorType.get(data[byte_position] & 0x06);
			
		}
	}

}
