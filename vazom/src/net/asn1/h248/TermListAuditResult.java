package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TermListAuditResult extends SEQUENCE{

	public TerminationIDList get_terminationIDList(){
		return (TerminationIDList)elements.get(0).data;
	}
	public TerminationIDList new_terminationIDList(){
		return new TerminationIDList();
	}

	public TerminationAudit get_terminationAuditResult(){
		return (TerminationAudit)elements.get(1).data;
	}
	public TerminationAudit new_terminationAuditResult(){
		return new TerminationAudit();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationIDList(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationAuditResult(); }});
	}
	public TermListAuditResult(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
