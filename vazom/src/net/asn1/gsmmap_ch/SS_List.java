package net.asn1.gsmmap_ch;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SS_List extends SEQUENCE{

	public SS_Code new_child(){
		return new SS_Code();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public SS_Code getChild(int index){
		return (SS_Code)of_children.get(index);
	}
	public SS_List(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
