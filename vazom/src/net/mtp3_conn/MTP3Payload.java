package net.mtp3_conn;

import net.mtp3.MTP3Packet;
import net.routing.RoutingConnectionPayload;
import net.routing.RoutingConnectionType;
import net.vstp.VSTP_SGF_CorrelationPacket;

public class MTP3Payload extends RoutingConnectionPayload {
	public MTP3Packet mtp3_packet;
	
	public MTP3Payload(){
		type = RoutingConnectionType.MTP3;
	}
	
	@Override
	public void setParams(Object[] params) {
		// TODO Auto-generated method stub

	}

	@Override
	public void setParamsFromVSTP_SGF(VSTP_SGF_CorrelationPacket vstpSgf) {
		// TODO Auto-generated method stub

	}

}
