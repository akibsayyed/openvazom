package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class RoutingInfo extends CHOICE{

	public ISDN_AddressString get_roamingNumber(){
		return (ISDN_AddressString)elements.get(0).data;
	}
	public ISDN_AddressString new_roamingNumber(){
		return new ISDN_AddressString();
	}

	public ForwardingData get_forwardingData(){
		return (ForwardingData)elements.get(1).data;
	}
	public ForwardingData new_forwardingData(){
		return new ForwardingData();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(4, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_roamingNumber(); }});
		elements.add(new ElementDescriptor(16, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_forwardingData(); }});
	}
	public RoutingInfo(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
