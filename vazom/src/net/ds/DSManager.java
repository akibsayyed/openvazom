package net.ds;

import java.util.concurrent.ConcurrentHashMap;

import net.config.ConfigManagerV2;
import net.config.SGNConfigData;
import net.ds.pds.PDS_DecoderWorker_MTP3_M3UA;
import net.ds.pds.PDS_DecoderWorker_SMPP;
import net.ds.smpp.SMPPCorrelationManager;
import net.spcap.ipfrag.IPFragmentationManager;

import org.apache.log4j.Logger;

public class DSManager {
	private static Logger logger=Logger.getLogger(DSManager.class);
	public static ConcurrentHashMap<String, DSDescriptor> ds_map;
	
	public static void init(){
		String tmp = null;
		String[] tmp_lst = null;
		String[] tmp_lst2 = null;
		DSBase ds = null;
		DSDescriptor dsd = new DSDescriptor();
		
		logger.info("Starting DS Manager...");
		tmp = ConfigManagerV2.get("pds.datasources");
		logger.info("Passive data sources: [" + tmp + "]!");
		//logger.info("Passive DS type: [" + SGNConfigData.pds_type + "]!");
		logger.info("Passive DS active: [" + SGNConfigData.pds_status + "]!");
		ds_map = new ConcurrentHashMap<String, DSDescriptor>();
		
		if(SGNConfigData.pds_status){
			// IP Fragmentation manager
			IPFragmentationManager.init(SGNConfigData.pds_ip_frag_timeout);
			try{
				tmp_lst = tmp.split(",");
				for(int i = 0; i<tmp_lst.length; i++){
					// ds
					dsd = new DSDescriptor();
					dsd.id = tmp_lst[i];
					dsd.workers =  Integer.parseInt(ConfigManagerV2.get("pds." + dsd.id + ".workers"));
					dsd.type = DataSourceType.valueOf(ConfigManagerV2.get("pds." + dsd.id + ".type"));
					// IFs
					tmp_lst2 = ConfigManagerV2.get("pds." + dsd.id + ".if").split(",");
					dsd.if_lst = new String[tmp_lst2.length];
					for(int j = 0; j<tmp_lst2.length; j++) dsd.if_lst[j] = tmp_lst2[j];
					// bfps
					tmp_lst2 = ConfigManagerV2.get("pds." + dsd.id + ".bpf").split(",");
					dsd.bpf = new String[tmp_lst2.length];
					for(int j = 0; j<tmp_lst2.length; j++) dsd.bpf[j] = tmp_lst2[j];
					// add to map
					ds_map.put(dsd.id, dsd);
					
					
					switch(dsd.type){
						case SPCAP_E1:
							ds = new DS_SPCAP_E1(dsd);
							dsd.data_source = ds;
							if(ds.isReady()){
								logger.info("Number of DS workers: [" + dsd.workers + "]!");
								for(int j = 0; j<dsd.workers; j++){
									logger.info("Initializing DS worker [" + j + "]!");
									new PDS_DecoderWorker_MTP3_M3UA(j, ds);
								}
							}else logger.warn("Data source [SPCAP_E1] not ready!");
							break;
						case SPCAP_SCTP: 
							ds = new DS_SPCAP_SCTP(dsd);
							dsd.data_source = ds;
							if(ds.isReady()){
								logger.info("Number of DS workers: [" + dsd.workers + "]!");
								for(int j = 0; j<dsd.workers; j++){
									logger.info("Initializing DS worker [" + j + "]!");
									new PDS_DecoderWorker_MTP3_M3UA(j, ds);
								}
							}else logger.warn("Data source [SPCAP_SCTP] not ready!");
							break;
						case SPCAP_SMPP:
							// start correlation manager
							SMPPCorrelationManager.init();
							// start DS
							ds = new DS_SPCAP_SMPP(dsd);
							dsd.data_source = ds;
							if(ds.isReady()){
								logger.info("Number of DS workers: [" + dsd.workers + "]!");
								for(int j = 0; j<dsd.workers; j++){
									logger.info("Initializing DS worker [" + j + "]!");
									new PDS_DecoderWorker_SMPP(j, ds);
								}
							}else logger.warn("Data source [SPCAP_SMPP] not ready!");
							break;
							
						default: 
							logger.warn("Data source [" + dsd.type + "] not supported in passive mode!");
							break;
					}
				}				
				
				
			}catch(Exception e){
				e.printStackTrace();
				logger.error("Error while initializing DS: type = [" + dsd.type + "], id = [" + dsd.id + "]!");
			}

		}
	}
}
