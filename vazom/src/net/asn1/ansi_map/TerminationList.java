package net.asn1.ansi_map;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TerminationList extends SET{

	public TerminationListChoice new_child(){
		return new TerminationListChoice();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public TerminationListChoice getChild(int index){
		return (TerminationListChoice)of_children.get(index);
	}
	public TerminationList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 17;
		of_children = new ArrayList<ASNType>();
	}
}
