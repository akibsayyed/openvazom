package net.m3ua.parameters.errorcode;

import java.util.HashMap;


public enum ErrorCodeType {
	INVALID_VERSION(0x01),
	UNSUPPORTED_MESSAGE_CLASS(0x03),
	UNSUPPORTED_MESSAGE_TYPE(0x04),
	UNSUPPORTED_TRAFFIC_MODE_TYPE(0x05),
	UNEXPECTED_MESSAGE(0x06),
	PROTOCOL_ERROR(0x07),
	INVALID_STREAM_IDENTIFIER(0x09),
	REFUSED_MANAGEMENT_BLOCKING(0x0d),
	ASP_IDENTIFIER_REQUIRED(0x0e),
	INVALID_ASP_IDENTIFIER(0x0f),
	INVALID_PARAMETER_VALUE(0x11),
	PARAMETER_FIELD_ERROR(0x12),
	UEXPECTED_PARAMETER(0x13),
	DESTINATION_STATUS_UNKNOWN(0x14),
	INVALID_NETWORK_APPEARANCE(0x15),
	MISSING_PARAMETER(0x16),
	INVALID_ROUTING_CONTEXT(0x19),
	NO_CONFIGURED_AS_FOR_ASP(0x1a);
	
	private int id;
	private static final HashMap<Integer, ErrorCodeType> lookup = new HashMap<Integer, ErrorCodeType>();
	static{
		for(ErrorCodeType td : ErrorCodeType.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static ErrorCodeType get(int id){ return lookup.get(id); }
	private ErrorCodeType(int _id){ id = _id; }		
}
