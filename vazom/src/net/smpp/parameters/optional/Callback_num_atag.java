package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;
import net.smpp.parameters.Data_codingType;
import net.utils.Utils;

public class Callback_num_atag extends OptionalParameterBase {

	public Callback_num_atag(){
		type = OptionalParameterType.CALLBACK_NUM_ATAG;
	}
	public Data_codingType dataCoding;
	public String displayCharacters;
	
	public void init(byte[] data) {
		dataCoding = Data_codingType.get(data[0] & 0xff);
		displayCharacters = Utils.Cstr_decode(data, 1);
	}

}
