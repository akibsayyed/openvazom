package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSDeliveryPointToPointRes extends SET{

	public ActionCode get_actionCode(){
		return (ActionCode)elements.get(0).data;
	}
	public ActionCode new_actionCode(){
		return new ActionCode();
	}

	public AuthorizationDenied get_authorizationDenied(){
		return (AuthorizationDenied)elements.get(1).data;
	}
	public AuthorizationDenied new_authorizationDenied(){
		return new AuthorizationDenied();
	}

	public DenyAccess get_denyAccess(){
		return (DenyAccess)elements.get(2).data;
	}
	public DenyAccess new_denyAccess(){
		return new DenyAccess();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(3).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MEID get_meid(){
		return (MEID)elements.get(4).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MobileStationMSID get_mobileStationMSID(){
		return (MobileStationMSID)elements.get(5).data;
	}
	public MobileStationMSID new_mobileStationMSID(){
		return new MobileStationMSID();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(6).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(7).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public SMS_BearerData get_sms_BearerData(){
		return (SMS_BearerData)elements.get(8).data;
	}
	public SMS_BearerData new_sms_BearerData(){
		return new SMS_BearerData();
	}

	public SMS_CauseCode get_sms_CauseCode(){
		return (SMS_CauseCode)elements.get(9).data;
	}
	public SMS_CauseCode new_sms_CauseCode(){
		return new SMS_CauseCode();
	}

	public SystemCapabilities get_systemCapabilities(){
		return (SystemCapabilities)elements.get(10).data;
	}
	public SystemCapabilities new_systemCapabilities(){
		return new SystemCapabilities();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(128, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_actionCode(); }});
		elements.add(new ElementDescriptor(13, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authorizationDenied(); }});
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_denyAccess(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_mobileStationMSID(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(105, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_BearerData(); }});
		elements.add(new ElementDescriptor(153, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_CauseCode(); }});
		elements.add(new ElementDescriptor(49, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemCapabilities(); }});
	}
	public SMSDeliveryPointToPointRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
