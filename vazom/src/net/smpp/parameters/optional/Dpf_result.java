package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Dpf_result extends OptionalParameterBase {

	public Dpf_result(){
		type = OptionalParameterType.DPF_RESULT;
	}
	public boolean dpfSet;
	public void init(byte[] data) {
		dpfSet = data[0] == 1;
	}

}
