package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SM_RP_DA extends CHOICE{

	public IMSI get_imsi(){
		return (IMSI)elements.get(0).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public LMSI get_lmsi(){
		return (LMSI)elements.get(1).data;
	}
	public LMSI new_lmsi(){
		return new LMSI();
	}

	public AddressString get_serviceCentreAddressDA(){
		return (AddressString)elements.get(2).data;
	}
	public AddressString new_serviceCentreAddressDA(){
		return new AddressString();
	}

	public NULL get_noSM_RP_DA(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_noSM_RP_DA(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_lmsi(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_serviceCentreAddressDA(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_noSM_RP_DA(); }});
	}
	public SM_RP_DA(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
