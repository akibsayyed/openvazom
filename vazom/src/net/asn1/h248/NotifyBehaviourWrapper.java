package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NotifyBehaviourWrapper extends SEQUENCE{

	public NotifyBehaviour get_notifyBehaviour(){
		return (NotifyBehaviour)elements.get(0).data;
	}
	public NotifyBehaviour new_notifyBehaviour(){
		return new NotifyBehaviour();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_notifyBehaviour(); }});
	}
	public NotifyBehaviourWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
