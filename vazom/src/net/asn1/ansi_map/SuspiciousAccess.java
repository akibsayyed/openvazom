package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SuspiciousAccess extends ENUMERATED{

	public static final int _not_used = 0;
	public static final int _anomalous_Digits = 1;
	public static final int _unspecified = 2;
	public SuspiciousAccess(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
