package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StreamParms extends SEQUENCE{

	public LocalControlDescriptor get_localControlDescriptor(){
		return (LocalControlDescriptor)elements.get(0).data;
	}
	public LocalControlDescriptor new_localControlDescriptor(){
		return new LocalControlDescriptor();
	}

	public LocalRemoteDescriptor get_localDescriptor(){
		return (LocalRemoteDescriptor)elements.get(1).data;
	}
	public LocalRemoteDescriptor new_localDescriptor(){
		return new LocalRemoteDescriptor();
	}

	public LocalRemoteDescriptor get_remoteDescriptor(){
		return (LocalRemoteDescriptor)elements.get(2).data;
	}
	public LocalRemoteDescriptor new_remoteDescriptor(){
		return new LocalRemoteDescriptor();
	}

	public StatisticsDescriptor get_statisticsDescriptor(){
		return (StatisticsDescriptor)elements.get(3).data;
	}
	public StatisticsDescriptor new_statisticsDescriptor(){
		return new StatisticsDescriptor();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_localControlDescriptor(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_localDescriptor(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_remoteDescriptor(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_statisticsDescriptor(); }});
	}
	public StreamParms(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
