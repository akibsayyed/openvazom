package net.isup.parameters;

import net.isup.ParameterType;

public class OriginationISCPointCode extends ParameterBase {
	public OriginationISCPointCode(){
		type = ParameterType.ORIGINATION_ISC_POINT_CODE;
	}
}
