package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SetReportingStateRes extends SEQUENCE{

	public CCBS_SubscriberStatus get_ccbs_SubscriberStatus(){
		return (CCBS_SubscriberStatus)elements.get(0).data;
	}
	public CCBS_SubscriberStatus new_ccbs_SubscriberStatus(){
		return new CCBS_SubscriberStatus();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(1).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ccbs_SubscriberStatus(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
	}
	public SetReportingStateRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
