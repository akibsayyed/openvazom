package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudSeqSigList extends SEQUENCE{

	public INTEGER get_id(){
		return (INTEGER)elements.get(0).data;
	}
	public INTEGER new_id(){
		return new INTEGER();
	}

	public IndAudSignal get_signalList(){
		return (IndAudSignal)elements.get(1).data;
	}
	public IndAudSignal new_signalList(){
		return new IndAudSignal();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_id(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalList(); }});
	}
	public IndAudSeqSigList(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
