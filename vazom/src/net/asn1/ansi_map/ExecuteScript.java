package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ExecuteScript extends SEQUENCE{

	public ScriptName get_scriptName(){
		return (ScriptName)elements.get(0).data;
	}
	public ScriptName new_scriptName(){
		return new ScriptName();
	}

	public ScriptArgument get_scriptArgument(){
		return (ScriptArgument)elements.get(1).data;
	}
	public ScriptArgument new_scriptArgument(){
		return new ScriptArgument();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(268, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_scriptName(); }});
		elements.add(new ElementDescriptor(267, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_scriptArgument(); }});
	}
	public ExecuteScript(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
