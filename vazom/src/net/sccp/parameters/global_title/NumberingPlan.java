package net.sccp.parameters.global_title;

import java.util.HashMap;

public enum NumberingPlan {
	UNKNOWN(0x00),
	ISDN_TELEPHONE(0x10),
	GENERIC(0x20),
	DATA_X121(0x30),
	TELEX(0x40),
	MARITIME(0x50),
	LAND_MOBILE(0x60),
	ISDN_MOBILE(0x70),
	PRIVATE(0xE0);
	
	
	// BITS 7..4 = XXXX0000
	private int id;
	private static final HashMap<Integer, NumberingPlan> lookup = new HashMap<Integer, NumberingPlan>();
	static{
		for(NumberingPlan td : NumberingPlan.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static NumberingPlan get(int id){ return lookup.get(id); }
	private NumberingPlan(int _id){ id = _id; }
	

}
