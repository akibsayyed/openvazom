package net.isup.parameters.bci;

import java.util.HashMap;

public enum SCCPMethodIndicatorType {
	NO(0x00),
	CONNETIONLESS(0x40),
	CONNECTION(0x80),
	CONNECTIONLESS_AND_CONNECTION(0xc0);
	
	
	private int id;
	private static final HashMap<Integer, SCCPMethodIndicatorType> lookup = new HashMap<Integer, SCCPMethodIndicatorType>();
	static{
		for(SCCPMethodIndicatorType td : SCCPMethodIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static SCCPMethodIndicatorType get(int id){ return lookup.get(id); }
	private SCCPMethodIndicatorType(int _id){ id = _id; }	
}
