package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class RANDValidTime extends OCTET_STRING{

	public RANDValidTime(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
