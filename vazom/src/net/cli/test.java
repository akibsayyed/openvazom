package net.cli;

import java.io.FileReader;
import java.util.ArrayList;

import org.antlr.runtime.ANTLRFileStream;
import org.antlr.runtime.CommonTokenStream;
import org.antlr.runtime.tree.CommonTree;
import org.antlr.runtime.tree.CommonTreeNodeStream;
import org.antlr.runtime.tree.Tree;
import org.antlr.stringtemplate.StringTemplate;
import org.antlr.stringtemplate.StringTemplateGroup;

import antlr.CommonToken;
import antlr.Token;

public class test {
	public static void main(String[] args) {
		try{
			FileReader gr = new FileReader("/home/dfranusic/develp/workspace/vazom/src/net/cli/CLIManager.st");
			StringTemplateGroup templates = new StringTemplateGroup(gr);
			gr.close();

			
			ANTLRFileStream input = new ANTLRFileStream("/home/dfranusic/cli.def", "UTF8");
			cliLexer lexer = new cliLexer(input);
			CommonTokenStream tokens = new CommonTokenStream(lexer);
			cliParser parser = new cliParser(tokens);
			cliParser.input_return r = parser.input();
			CommonTree t = (CommonTree)r.getTree();
			
			CommonTreeNodeStream nodes = new CommonTreeNodeStream(t);
			nodes.setTokenStream(tokens);
			cliwalker walker = new cliwalker(nodes);
		
			
			walker.setTemplateLib(templates);
			cliwalker.input_return wr = walker.input();
			
			StringTemplate output = (StringTemplate)wr.getTemplate();
			System.out.println(output.toString());
			
			

		}catch(Exception e){
			e.printStackTrace();
		}
		//StringTemplate tmpl = new StringTemplate("Hello $var$");
		//tmpl.setAttribute("var", "111");
		//System.out.println(tmpl.toString());
		
	}

}
