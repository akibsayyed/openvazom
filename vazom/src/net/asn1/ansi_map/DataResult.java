package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class DataResult extends ENUMERATED{

	public static final int _not_used =  0 ;
	public static final int _successful =  1 ;
	public static final int _unsuccessful_unspecified =  2 ;
	public static final int _unsuccessful_no_default_value_available =  3 ;
	public static final int _reserved =  4 ;
	public DataResult(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 10;
	}
}
