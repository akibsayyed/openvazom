package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SMSDeliveryBackwardRes extends SET{

	public SMS_BearerData get_sms_BearerData(){
		return (SMS_BearerData)elements.get(0).data;
	}
	public SMS_BearerData new_sms_BearerData(){
		return new SMS_BearerData();
	}

	public SMS_CauseCode get_sms_CauseCode(){
		return (SMS_CauseCode)elements.get(1).data;
	}
	public SMS_CauseCode new_sms_CauseCode(){
		return new SMS_CauseCode();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(105, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_BearerData(); }});
		elements.add(new ElementDescriptor(153, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sms_CauseCode(); }});
	}
	public SMSDeliveryBackwardRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
