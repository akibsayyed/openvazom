package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ExternalPDU extends SEQUENCE{

	public OBJECT_IDENTIFIER get_oid(){
		return (OBJECT_IDENTIFIER)elements.get(0).data;
	}
	public OBJECT_IDENTIFIER new_oid(){
		return new OBJECT_IDENTIFIER();
	}

	public Dialog1 get_dialog(){
		return (Dialog1)elements.get(1).data;
	}
	public Dialog1 new_dialog(){
		return new Dialog1();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(6, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_oid(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_dialog(); }});
	}
	public ExternalPDU(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 8;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
