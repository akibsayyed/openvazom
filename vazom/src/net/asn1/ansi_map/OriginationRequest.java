package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class OriginationRequest extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public Digits get_digits(){
		return (Digits)elements.get(1).data;
	}
	public Digits new_digits(){
		return new Digits();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(2).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(3).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(4).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public MSID get_msid(){
		return (MSID)elements.get(5).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public OriginationTriggers get_originationTriggers(){
		return (OriginationTriggers)elements.get(6).data;
	}
	public OriginationTriggers new_originationTriggers(){
		return new OriginationTriggers();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(7).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public ACGEncountered get_acgencountered(){
		return (ACGEncountered)elements.get(8).data;
	}
	public ACGEncountered new_acgencountered(){
		return new ACGEncountered();
	}

	public CallingPartyName get_callingPartyName(){
		return (CallingPartyName)elements.get(9).data;
	}
	public CallingPartyName new_callingPartyName(){
		return new CallingPartyName();
	}

	public CallingPartyNumberDigits1 get_callingPartyNumberDigits1(){
		return (CallingPartyNumberDigits1)elements.get(10).data;
	}
	public CallingPartyNumberDigits1 new_callingPartyNumberDigits1(){
		return new CallingPartyNumberDigits1();
	}

	public CallingPartyNumberDigits2 get_callingPartyNumberDigits2(){
		return (CallingPartyNumberDigits2)elements.get(11).data;
	}
	public CallingPartyNumberDigits2 new_callingPartyNumberDigits2(){
		return new CallingPartyNumberDigits2();
	}

	public CallingPartySubaddress get_callingPartySubaddress(){
		return (CallingPartySubaddress)elements.get(12).data;
	}
	public CallingPartySubaddress new_callingPartySubaddress(){
		return new CallingPartySubaddress();
	}

	public CDMAServiceOption get_cdmaServiceOption(){
		return (CDMAServiceOption)elements.get(13).data;
	}
	public CDMAServiceOption new_cdmaServiceOption(){
		return new CDMAServiceOption();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(14).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(15).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public FeatureIndicator get_featureIndicator(){
		return (FeatureIndicator)elements.get(16).data;
	}
	public FeatureIndicator new_featureIndicator(){
		return new FeatureIndicator();
	}

	public MSCIdentificationNumber get_mSCIdentificationNumber(){
		return (MSCIdentificationNumber)elements.get(17).data;
	}
	public MSCIdentificationNumber new_mSCIdentificationNumber(){
		return new MSCIdentificationNumber();
	}

	public OneTimeFeatureIndicator get_oneTimeFeatureIndicator(){
		return (OneTimeFeatureIndicator)elements.get(18).data;
	}
	public OneTimeFeatureIndicator new_oneTimeFeatureIndicator(){
		return new OneTimeFeatureIndicator();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(19).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public PreferredLanguageIndicator get_preferredLanguageIndicator(){
		return (PreferredLanguageIndicator)elements.get(20).data;
	}
	public PreferredLanguageIndicator new_preferredLanguageIndicator(){
		return new PreferredLanguageIndicator();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(21).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(22).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(23).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TriggerType get_triggerType(){
		return (TriggerType)elements.get(24).data;
	}
	public TriggerType new_triggerType(){
		return new TriggerType();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(25).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public CallingPartyCategory get_callingPartyCategory(){
		return (CallingPartyCategory)elements.get(26).data;
	}
	public CallingPartyCategory new_callingPartyCategory(){
		return new CallingPartyCategory();
	}

	public MEID get_meid(){
		return (MEID)elements.get(27).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(4, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_digits(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(98, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_originationTriggers(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(340, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_acgencountered(); }});
		elements.add(new ElementDescriptor(243, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyName(); }});
		elements.add(new ElementDescriptor(80, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits1(); }});
		elements.add(new ElementDescriptor(81, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyNumberDigits2(); }});
		elements.add(new ElementDescriptor(84, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartySubaddress(); }});
		elements.add(new ElementDescriptor(175, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOption(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(306, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_featureIndicator(); }});
		elements.add(new ElementDescriptor(94, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mSCIdentificationNumber(); }});
		elements.add(new ElementDescriptor(97, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_oneTimeFeatureIndicator(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
		elements.add(new ElementDescriptor(147, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_preferredLanguageIndicator(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(279, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_triggerType(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(355, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callingPartyCategory(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public OriginationRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
