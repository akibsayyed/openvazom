package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AmmsReply extends SEQUENCE{

	public TerminationIDList get_terminationID(){
		return (TerminationIDList)elements.get(0).data;
	}
	public TerminationIDList new_terminationID(){
		return new TerminationIDList();
	}

	public TerminationAudit get_terminationAudit(){
		return (TerminationAudit)elements.get(1).data;
	}
	public TerminationAudit new_terminationAudit(){
		return new TerminationAudit();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_terminationID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminationAudit(); }});
	}
	public AmmsReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
