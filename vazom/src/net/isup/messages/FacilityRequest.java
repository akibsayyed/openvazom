package net.isup.messages;

import net.isup.MessageType;
import net.isup.Pointer;
import net.isup.parameters.FacilityIndicator;

public class FacilityRequest extends MessageBase {
	public FacilityIndicator facility_indicator;
	public FacilityRequest(){
		super();
		type = MessageType.FACILITY_REQUEST;
		pointers = new Pointer[1];
	}
	public void init(byte[] data) {
		if(data != null){
			facility_indicator = new FacilityIndicator();
			facility_indicator.init(new byte[]{data[byte_pos++]});
			// pointers 
			pointers[0] = new Pointer(data[byte_pos] & 0xFF, (data[byte_pos] & 0xFF) + (byte_pos++));
			
			processOptional(data, 0);
		}

	}

}
