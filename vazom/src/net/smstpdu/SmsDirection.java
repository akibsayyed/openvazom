package net.smstpdu;

import java.util.HashMap;

public enum SmsDirection {
	MO(1),
	MT(2);
	
	private int id;
	private static final HashMap<Integer, SmsDirection> lookup = new HashMap<Integer, SmsDirection>();
	static{
		for(SmsDirection td : SmsDirection.values()){
			lookup.put(td.getId(), td);
		}
	}
	public int getId(){ return id; }
	public static SmsDirection get(int id){ return lookup.get(id); }
	private SmsDirection(int _id){ id = _id; }	
}
