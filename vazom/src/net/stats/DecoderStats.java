package net.stats;

public class DecoderStats {
	// general
	public long SKIPPED_COUNT;
	public long SCCP;
	public long GCP;
	public long ISUP;
	
	//h248
	public long H248;
	// isup
	public long ISUP_IAM;
	public long ISUP_ACM;
	public long ISUP_ANM;
	public long ISUP_REL;
	public long ISUP_RLC;
	
	// sccp
	public long UDT_COUNT;
	// tcap
	public long TCAP_COUNT;
	public long TCAP_BEGIN;
	public long TCAP_END;
	public long TCAP_CONTINUE;
	public long TCAP_ABORT;
	public long TCAP_RETURN_RESULT;
	public long TCAP_RETURN_ERROR;
	// sms
	public long MO_COUNT;
	public long MT_COUNT;
	public long SMS_SUBMIT_COUNT;
	public long SMS_DELIVER_COUNT;
	public long MO_CONCATENATED_COUNT;
	public long MT_CONCATENATED_COUNT;

	// SRI
	public long SRI_BEGIN;
	public long SRI_END;
	//public long SRI_RETURN_ERROR;
	//public long SRI_RETURN_RESULT;
	//public long SRI_NO_REPLY;
	// SMPP
	public long SMPP_SUBMIT_SM;
	public long SMPP_SUBMIT_MULTI;
	public long SMPP_DELIVER_SM;
	public long SMPP_BIND_RECEIVER;
	public long SMPP_BIND_TRANSMITTER;
	public long SMPP_BIND_TRANSCEIVER;
	public long SMPP_UNBIND;
	
}
