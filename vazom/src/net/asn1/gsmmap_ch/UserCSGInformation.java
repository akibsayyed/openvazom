package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class UserCSGInformation extends SEQUENCE{

	public CSG_Id get_csg_Id(){
		return (CSG_Id)elements.get(0).data;
	}
	public CSG_Id new_csg_Id(){
		return new CSG_Id();
	}

	public ExtensionContainer get_extensionContainer(){
		return (ExtensionContainer)elements.get(1).data;
	}
	public ExtensionContainer new_extensionContainer(){
		return new ExtensionContainer();
	}

	public OCTET_STRING get_accessMode(){
		return (OCTET_STRING)elements.get(2).data;
	}
	public OCTET_STRING new_accessMode(){
		return new OCTET_STRING();
	}

	public OCTET_STRING get_cmi(){
		return (OCTET_STRING)elements.get(3).data;
	}
	public OCTET_STRING new_cmi(){
		return new OCTET_STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_csg_Id(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_extensionContainer(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_accessMode(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, true){public void set(){ data = new_cmi(); }});
	}
	public UserCSGInformation(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
