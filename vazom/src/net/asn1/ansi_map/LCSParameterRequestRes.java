package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class LCSParameterRequestRes extends SET{

	public AccessDeniedReason get_accessDeniedReason(){
		return (AccessDeniedReason)elements.get(0).data;
	}
	public AccessDeniedReason new_accessDeniedReason(){
		return new AccessDeniedReason();
	}

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(1).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public IMSI get_imsi(){
		return (IMSI)elements.get(2).data;
	}
	public IMSI new_imsi(){
		return new IMSI();
	}

	public MEID get_meid(){
		return (MEID)elements.get(3).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public MobileDirectoryNumber get_mobileDirectoryNumber(){
		return (MobileDirectoryNumber)elements.get(4).data;
	}
	public MobileDirectoryNumber new_mobileDirectoryNumber(){
		return new MobileDirectoryNumber();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(5).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public MobilePositionCapability get_mobilePositionCapability(){
		return (MobilePositionCapability)elements.get(6).data;
	}
	public MobilePositionCapability new_mobilePositionCapability(){
		return new MobilePositionCapability();
	}

	public MPCAddress get_mpcAddress(){
		return (MPCAddress)elements.get(7).data;
	}
	public MPCAddress new_mpcAddress(){
		return new MPCAddress();
	}

	public MPCAddressList get_mpcAddressList(){
		return (MPCAddressList)elements.get(8).data;
	}
	public MPCAddressList new_mpcAddressList(){
		return new MPCAddressList();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(9).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public PC_SSN get_pc_ssn(){
		return (PC_SSN)elements.get(10).data;
	}
	public PC_SSN new_pc_ssn(){
		return new PC_SSN();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(20, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_accessDeniedReason(); }});
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(242, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_imsi(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
		elements.add(new ElementDescriptor(93, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileDirectoryNumber(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(335, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobilePositionCapability(); }});
		elements.add(new ElementDescriptor(370, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mpcAddress(); }});
		elements.add(new ElementDescriptor(381, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mpcAddressList(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(32, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pc_ssn(); }});
	}
	public LCSParameterRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
