package net.smstpdu.tpdcs.general;

import java.util.HashMap;

public enum Alphabet {
	DEFAULT(0x00),
	_8BIT(0x04),
	UCS2(0x08),
	RESERVED(0x0C);
	
	// BITS 3 and 2 = 0000XX00

	private int id;
	private static final HashMap<Integer, Alphabet> lookup = new HashMap<Integer, Alphabet>();
	static{
		for(Alphabet td : Alphabet.values()){
			lookup.put(td.getId(), td);
		}
	}

	public int getId(){ return id; }
	public static Alphabet get(int id){ return lookup.get(id); }
	private Alphabet(int _id){ id = _id; }

}
