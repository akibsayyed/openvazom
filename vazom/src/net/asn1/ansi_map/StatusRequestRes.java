package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StatusRequestRes extends SET{

	public Information_Record get_information_Record(){
		return (Information_Record)elements.get(0).data;
	}
	public Information_Record new_information_Record(){
		return new Information_Record();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(389, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_information_Record(); }});
	}
	public StatusRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
