package net.asn1.types;

import net.asn1.compiler.ASNTag;
import net.asn1.compiler.ASNType;

public class T61STRING extends ASNType {
	public T61STRING(){
		super();
		universalTag = ASNTag.T61STRING;
		tag = ASNTag.name2Tag(universalTag.toString());
	}
}
