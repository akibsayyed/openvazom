package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class AlertCode extends OCTET_STRING{

	public AlertCode(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
