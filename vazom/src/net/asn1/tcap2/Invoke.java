package net.asn1.tcap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Invoke extends SEQUENCE{

	public InvokeIdType get_invokeID(){
		return (InvokeIdType)elements.get(0).data;
	}
	public InvokeIdType new_invokeID(){
		return new InvokeIdType();
	}

	public InvokeIdType get_linkedID(){
		return (InvokeIdType)elements.get(1).data;
	}
	public InvokeIdType new_linkedID(){
		return new InvokeIdType();
	}

	public OPERATION get_opCode(){
		return (OPERATION)elements.get(2).data;
	}
	public OPERATION new_opCode(){
		return new OPERATION();
	}

	public Parameter get_parameter(){
		return (Parameter)elements.get(3).data;
	}
	public Parameter new_parameter(){
		return new Parameter();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(2, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_invokeID(); }});
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_linkedID(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_opCode(); }});
		elements.add(new ElementDescriptor(-2, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_parameter(); }});
	}
	public Invoke(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
