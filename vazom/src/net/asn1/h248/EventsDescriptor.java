package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class EventsDescriptor extends SEQUENCE{

	public RequestID get_requestID(){
		return (RequestID)elements.get(0).data;
	}
	public RequestID new_requestID(){
		return new RequestID();
	}

	public RequestedEventSequnceOf get_eventList(){
		return (RequestedEventSequnceOf)elements.get(1).data;
	}
	public RequestedEventSequnceOf new_eventList(){
		return new RequestedEventSequnceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_requestID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_eventList(); }});
	}
	public EventsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
