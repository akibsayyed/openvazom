package net.hplmnr;

public class HPLMNRCorrelationPacket {
	public byte[] tcap_sid;
	public byte[] tcap_did;
	public long imsi_correlation_id;
	public String sms_gms_c;
	public String msisdn;
	public String imsi;
	public String nnn;
	public byte[] invoke_id;
	//public boolean hasDialogue;
	//public byte[] app_ctx_oid;
	
	public long ts;
	
	
}
