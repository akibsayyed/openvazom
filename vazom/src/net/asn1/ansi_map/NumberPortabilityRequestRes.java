package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class NumberPortabilityRequestRes extends SET{

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(0).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
	}
	public NumberPortabilityRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
