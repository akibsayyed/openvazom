package net.smpp.parameters.optional;

import net.smpp.OptionalParameterType;

public class Qos_time_to_live extends OptionalParameterBase {
	int value;
	
	public Qos_time_to_live(){
		type = OptionalParameterType.QOS_TIME_TO_LIVE;
	}

	public void init(byte[] data) {
		value = ((data[0] << 24) & 0xff) + ((data[1] << 16) & 0xff) + ((data[2] << 8) & 0xff) + (data[3] & 0xff);

	}

}
