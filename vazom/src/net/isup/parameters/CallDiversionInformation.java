package net.isup.parameters;

import net.isup.ParameterType;
import net.isup.parameters.cdi.NotificationSubscriptionOptionsType;
import net.isup.parameters.cdi.RedirectingReasonType;

public class CallDiversionInformation extends ParameterBase {
	public NotificationSubscriptionOptionsType notification_subscription_options;
	public RedirectingReasonType redirecting_reason;
	
	public CallDiversionInformation(){
		type = ParameterType.CALL_DIVERSION_INFORMATION;
	}

	public void init(byte[] data) {
		if(data != null){
			notification_subscription_options = NotificationSubscriptionOptionsType.get(data[byte_position] & 0x07);
			redirecting_reason = RedirectingReasonType.get(data[byte_position] & 0x78);
		}
	}

}
