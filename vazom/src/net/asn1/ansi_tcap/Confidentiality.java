package net.asn1.ansi_tcap;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class Confidentiality extends SEQUENCE{

	public ConfidentialityIdChoice get_confidentialityId(){
		return (ConfidentialityIdChoice)elements.get(0).data;
	}
	public ConfidentialityIdChoice new_confidentialityId(){
		return new ConfidentialityIdChoice();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_confidentialityId(); }});
	}
	public Confidentiality(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
