package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class ServiceKey extends INTEGER{

	public ServiceKey(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
