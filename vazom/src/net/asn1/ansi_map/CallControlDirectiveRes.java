package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class CallControlDirectiveRes extends SET{

	public CallStatus get_callStatus(){
		return (CallStatus)elements.get(0).data;
	}
	public CallStatus new_callStatus(){
		return new CallStatus();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(310, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callStatus(); }});
	}
	public CallControlDirectiveRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
