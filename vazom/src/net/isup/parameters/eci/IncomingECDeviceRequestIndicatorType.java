package net.isup.parameters.eci;

import java.util.HashMap;

public enum IncomingECDeviceRequestIndicatorType {
	
	NO_INFORMATION(0x00),
	ACTIVATION_REQUEST(0x40),
	DEACTIVATION_REQUEST(0x80);
	
	
	private int id;
	private static final HashMap<Integer, IncomingECDeviceRequestIndicatorType> lookup = new HashMap<Integer, IncomingECDeviceRequestIndicatorType>();
	static{
		for(IncomingECDeviceRequestIndicatorType td :IncomingECDeviceRequestIndicatorType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static IncomingECDeviceRequestIndicatorType get(int id){ return lookup.get(id); }
	private IncomingECDeviceRequestIndicatorType(int _id){ id = _id; }			
}
