package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class TransactionResponseAck extends SEQUENCE{

	public TransactionAck new_child(){
		return new TransactionAck();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public TransactionAck getChild(int index){
		return (TransactionAck)of_children.get(index);
	}
	public TransactionResponseAck(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
