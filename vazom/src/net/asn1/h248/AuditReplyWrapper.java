package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuditReplyWrapper extends SEQUENCE{

	public AuditReply get_auditReply(){
		return (AuditReply)elements.get(0).data;
	}
	public AuditReply new_auditReply(){
		return new AuditReply();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_auditReply(); }});
	}
	public AuditReplyWrapper(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
