package net.asn1.h248;

import java.util.ArrayList;
import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class StatisticsDescriptor extends SEQUENCE{

	public StatisticsParameter new_child(){
		return new StatisticsParameter();
	}
	public void addChild(){
		of_children.add(new_child());
	}
	public StatisticsParameter getChild(int index){
		return (StatisticsParameter)of_children.get(index);
	}
	public StatisticsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		of_children = new ArrayList<ASNType>();
	}
}
