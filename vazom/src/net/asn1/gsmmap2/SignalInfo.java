package net.asn1.gsmmap2;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class SignalInfo extends OCTET_STRING{

	public SignalInfo(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 4;
	}
}
