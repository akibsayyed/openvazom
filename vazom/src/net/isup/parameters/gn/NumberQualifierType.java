package net.isup.parameters.gn;

import java.util.HashMap;


public enum NumberQualifierType {
	DIALLED_DIGITS(0x00),
	ADDITIONAL_CALLED_NUMBER(0x01),
	FAILED_NETWORK_SCREENING(0x02),
	NOT_SCREENED(0x03),
	REDIRECTING_TERMINATING_NUMBER(0x04),
	ADDITIONAL_CONNECTED_NUMBER(0x05),
	ADDITIONAL_CALLING_PARTY_NUMBER(0x06),
	ADDITIONAL_ORIGINAL_CALLED_NUMBER(0x07),
	ADDITIONAL_REDIRECTING_NUMBER(0x08),
	ADDITIONAL_REDIRECTION_NUMBER(0x09);



	
	
	private int id;
	private static final HashMap<Integer, NumberQualifierType> lookup = new HashMap<Integer, NumberQualifierType>();
	static{
		for(NumberQualifierType td :NumberQualifierType.values()){
			lookup.put(td.id, td);
		}
	}
	public int getId(){ return id; }
	public static NumberQualifierType get(int id){ return lookup.get(id); }
	private NumberQualifierType(int _id){ id = _id; }		
}
