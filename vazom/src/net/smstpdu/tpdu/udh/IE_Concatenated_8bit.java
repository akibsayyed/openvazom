package net.smstpdu.tpdu.udh;

public class IE_Concatenated_8bit extends IE_ConcatenatedReference {
	
	public IE_Concatenated_8bit(){
		super();
		type = InformationElementType.CONCATENATED_8bit;
	}
	
	public void init(byte[] data){
		messageIdentifier = data[0] & 0xFF;
		parts = data[1] & 0xFF;
		partNumber = data[2] & 0xFF;
	}

	public byte[] encode() {
		byte[] res = new byte[5];
		res[0] = (byte)type.getId();
		res[1] = 3;
		res[2] = (byte)messageIdentifier;
		res[3] = (byte)parts; 
		res[4] = (byte)partNumber; 
		return res;
	}
}
