package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class Range extends INTEGER{

	public Range(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
