package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class InterSwitchCount extends INTEGER{

	public InterSwitchCount(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 2;
	}
}
