package net.fn.cli;

import java.net.Socket;
import java.util.concurrent.ConcurrentHashMap;
import net.cli.CliConnectionBase;
import net.config.FNConfigData;
import net.smsfs.SmsfsManager;
import net.smsfs.Compiler;
import net.utils.Utils;

public class CLIConnection extends CliConnectionBase{
	public boolean filterMode = false;
	public String tmp_smsfs_filter = "";	
	public int halt_counter;
	
	public CLIConnection(Socket socket, ConcurrentHashMap<String, CliConnectionBase> connList) {
		super(new CLIManager(), FNConfigData.fn_id, "F", socket, connList);

	}



	public void cli_execute() {
		// echo
		out.println();
		out.print((char)13);
		out.flush();

		for(int j = 0; j<last_cmd_lst.size(); j++) if(last_cmd_lst.get(j).equals(cmd)){
			history_found = true;
			last_cmd_index = j;
		}
		if(!history_found){
			last_cmd_lst.add(cmd);
			last_cmd_index = last_cmd_lst.size() - 1;
		}
		history_found = false;
		if(cmd.equalsIgnoreCase("FILTER END")){
			if(filterMode){
				filterMode = false;
				out.println("\u001B[1;32mSMSFS Received!" + (char)13);
				out.println("\u001B[1;33m----FILTER DATA START-----" + (char)13);
				out.println("\u001B[1;34m" + tmp_smsfs_filter.replaceAll("\n", "\n" + (char)13));
				out.println("\u001B[1;33m----FILTER DATA END-----" + (char)13);
				if(Utils.is_ascii(tmp_smsfs_filter.getBytes())){
					boolean valid = Compiler.compile_str(tmp_smsfs_filter);
					out.println("\u001B[1;37mSMSFS Filter valid: [ \u001B[1;32m" + valid + "\u001B[1;37m ]!" + (char)13);
					if(!valid) tmp_smsfs_filter = "";
					// set new simulation filter
					else{
						SmsfsManager.setSimulationFilter(tmp_smsfs_filter);
						out.println("\u001B[1;37mNew simulation SMSFS Filter set!" + (char)13);
						out.flush();
					}
					
				}else{
					out.println("\u001B[1;37mSMSFS Filter valid: [ \u001B[1;32m" + "false" + "\u001B[1;37m ], invalid character(s) detected!" + (char)13);
					tmp_smsfs_filter = "";
					
				}
			}else out.println("\u001B[1;31mFilter mode not started!" + (char)13);
			out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
			out.print("\u001B[0m");
			out.print("\u001B7"); // save cursor pos
			out.flush();
			
		}else if(filterMode){
			tmp_smsfs_filter += cmd + "\n";
			
		}else{
			his_lst = rebuildCLI(cmd);
			res = "";
			if(his_lst.size() > 0){
				Object[] tmp_params;
				his = his_lst.get(his_lst.size() - 1);
				if(his.descriptor != null) {
					if(his.params != null){
						tmp_params = new Object[his.params.size() + 1];
						for(int j = 0; j<his.params.size(); j++) tmp_params[j] = his.params.get(j);
						tmp_params[his.params.size()] = this.current_conn;
						res += Utils.ansi_paint(his.descriptor.execute(tmp_params));
					}
					else{
						tmp_params = new Object[1];
						tmp_params[0] = this.current_conn;
						res += Utils.ansi_paint(his.descriptor.execute(tmp_params));
					}
				}
				
			}			
			if(res != null) out.println(res.replaceAll("\n", "\n" + (char)13) + (char)13);
			if(!filterMode){
				out.print("\u001B[1;33m");
				out.print("\u001B[1;33mVAZOM-F[\u001B[1;34m" + FNConfigData.fn_id + "\u001B[1;33m]> ");
				out.print("\u001B[0m");
				out.print("\u001B7"); // save cursor pos
				
			}
			res = "";
			out.flush();
		}
		cmd = "";		
	}
	
	
	
	
}
