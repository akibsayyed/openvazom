package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;

public class DeregistrationResult extends M3UAParameter {
	
	public DeregistrationResult(){
		type = M3UAParameterType.DEREGISTRATION_RESULT;
	}

	public void init(byte[] data) {
		value = data;
	}


}
