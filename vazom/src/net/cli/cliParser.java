// $ANTLR 3.2 Sep 23, 2009 12:02:23 /home/dfranusic/cli.g 2011-04-30 20:02:18

package net.cli;


import org.antlr.runtime.*;
import java.util.Stack;
import java.util.List;
import java.util.ArrayList;
import java.util.Map;
import java.util.HashMap;

import org.antlr.runtime.tree.*;

public class cliParser extends Parser {
    public static final String[] tokenNames = new String[] {
        "<invalid>", "<EOR>", "<DOWN>", "<UP>", "L_PAREN", "R_PAREN", "R_SQ_B", "L_SQ_B", "L_CR_B", "R_CR_B", "ANNT", "EQUAL", "ASSIGN", "OR", "NEQUAL", "COLON", "STMTSEP", "PERCENT", "HEX_P", "LT", "GT", "LTE", "GTE", "PLUS", "MINUS", "AND", "SQUOTE", "REGEX_BLOCK", "ASTERISK", "COMMA", "CLI_FINAL", "CLI_COMPLEX", "CLI_METHOD", "CLI_DEPTH", "CLI_PARENT", "CLI_PARAMS", "CLI_PARAM", "CLI_DESC", "WORD", "WS", "SL_COMMENT"
    };
    public static final int NEQUAL=14;
    public static final int LT=19;
    public static final int CLI_METHOD=32;
    public static final int PERCENT=17;
    public static final int CLI_PARENT=34;
    public static final int HEX_P=18;
    public static final int REGEX_BLOCK=27;
    public static final int GTE=22;
    public static final int CLI_PARAM=36;
    public static final int L_CR_B=8;
    public static final int SQUOTE=26;
    public static final int MINUS=24;
    public static final int AND=25;
    public static final int EOF=-1;
    public static final int R_CR_B=9;
    public static final int LTE=21;
    public static final int ANNT=10;
    public static final int R_PAREN=5;
    public static final int ASTERISK=28;
    public static final int CLI_PARAMS=35;
    public static final int WORD=38;
    public static final int COLON=15;
    public static final int WS=39;
    public static final int L_SQ_B=7;
    public static final int COMMA=29;
    public static final int R_SQ_B=6;
    public static final int EQUAL=11;
    public static final int SL_COMMENT=40;
    public static final int L_PAREN=4;
    public static final int OR=13;
    public static final int ASSIGN=12;
    public static final int CLI_FINAL=30;
    public static final int GT=20;
    public static final int PLUS=23;
    public static final int CLI_DESC=37;
    public static final int STMTSEP=16;
    public static final int CLI_DEPTH=33;
    public static final int CLI_COMPLEX=31;

    // delegates
    // delegators


        public cliParser(TokenStream input) {
            this(input, new RecognizerSharedState());
        }
        public cliParser(TokenStream input, RecognizerSharedState state) {
            super(input, state);
             
        }
        
    protected TreeAdaptor adaptor = new CommonTreeAdaptor();

    public void setTreeAdaptor(TreeAdaptor adaptor) {
        this.adaptor = adaptor;
    }
    public TreeAdaptor getTreeAdaptor() {
        return adaptor;
    }

    public String[] getTokenNames() { return cliParser.tokenNames; }
    public String getGrammarFileName() { return "/home/dfranusic/cli.g"; }


      int depth = 0;
      String parent = "";
      ArrayList<String> parent_lst = new ArrayList<String>();


    public static class input_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "input"
    // /home/dfranusic/cli.g:68:1: input : ( cliitem )* EOF ;
    public final cliParser.input_return input() throws RecognitionException {
        cliParser.input_return retval = new cliParser.input_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token EOF2=null;
        cliParser.cliitem_return cliitem1 = null;


        Object EOF2_tree=null;

        try {
            // /home/dfranusic/cli.g:69:3: ( ( cliitem )* EOF )
            // /home/dfranusic/cli.g:69:5: ( cliitem )* EOF
            {
            root_0 = (Object)adaptor.nil();

            // /home/dfranusic/cli.g:69:5: ( cliitem )*
            loop1:
            do {
                int alt1=2;
                int LA1_0 = input.LA(1);

                if ( (LA1_0==WORD) ) {
                    alt1=1;
                }


                switch (alt1) {
            	case 1 :
            	    // /home/dfranusic/cli.g:69:6: cliitem
            	    {
            	    pushFollow(FOLLOW_cliitem_in_input435);
            	    cliitem1=cliitem();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, cliitem1.getTree());

            	    }
            	    break;

            	default :
            	    break loop1;
                }
            } while (true);

            EOF2=(Token)match(input,EOF,FOLLOW_EOF_in_input439); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "input"

    public static class finalitem_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "finalitem"
    // /home/dfranusic/cli.g:72:1: finalitem : (cmd= WORD ( WS )* (dsc= desc )* ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) | cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) );
    public final cliParser.finalitem_return finalitem() throws RecognitionException {
        cliParser.finalitem_return retval = new cliParser.finalitem_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token cmd=null;
        Token method=null;
        Token WS3=null;
        Token WS4=null;
        Token COLON5=null;
        Token WS6=null;
        Token WS7=null;
        Token STMTSEP8=null;
        Token WS9=null;
        Token WS10=null;
        Token L_SQ_B11=null;
        Token R_SQ_B13=null;
        Token WS14=null;
        Token COLON15=null;
        Token WS16=null;
        Token WS17=null;
        Token STMTSEP18=null;
        cliParser.desc_return dsc = null;

        cliParser.params_return params12 = null;


        Object cmd_tree=null;
        Object method_tree=null;
        Object WS3_tree=null;
        Object WS4_tree=null;
        Object COLON5_tree=null;
        Object WS6_tree=null;
        Object WS7_tree=null;
        Object STMTSEP8_tree=null;
        Object WS9_tree=null;
        Object WS10_tree=null;
        Object L_SQ_B11_tree=null;
        Object R_SQ_B13_tree=null;
        Object WS14_tree=null;
        Object COLON15_tree=null;
        Object WS16_tree=null;
        Object WS17_tree=null;
        Object STMTSEP18_tree=null;
        RewriteRuleTokenStream stream_WORD=new RewriteRuleTokenStream(adaptor,"token WORD");
        RewriteRuleTokenStream stream_COLON=new RewriteRuleTokenStream(adaptor,"token COLON");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_L_SQ_B=new RewriteRuleTokenStream(adaptor,"token L_SQ_B");
        RewriteRuleTokenStream stream_R_SQ_B=new RewriteRuleTokenStream(adaptor,"token R_SQ_B");
        RewriteRuleTokenStream stream_STMTSEP=new RewriteRuleTokenStream(adaptor,"token STMTSEP");
        RewriteRuleSubtreeStream stream_desc=new RewriteRuleSubtreeStream(adaptor,"rule desc");
        RewriteRuleSubtreeStream stream_params=new RewriteRuleSubtreeStream(adaptor,"rule params");
        try {
            // /home/dfranusic/cli.g:73:3: (cmd= WORD ( WS )* (dsc= desc )* ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) | cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) )
            int alt14=2;
            alt14 = dfa14.predict(input);
            switch (alt14) {
                case 1 :
                    // /home/dfranusic/cli.g:73:5: cmd= WORD ( WS )* (dsc= desc )* ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP
                    {
                    cmd=(Token)match(input,WORD,FOLLOW_WORD_in_finalitem455); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_WORD.add(cmd);

                    // /home/dfranusic/cli.g:73:14: ( WS )*
                    loop2:
                    do {
                        int alt2=2;
                        int LA2_0 = input.LA(1);

                        if ( (LA2_0==WS) ) {
                            int LA2_2 = input.LA(2);

                            if ( (synpred2_cli()) ) {
                                alt2=1;
                            }


                        }


                        switch (alt2) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS3=(Token)match(input,WS,FOLLOW_WS_in_finalitem457); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS3);


                    	    }
                    	    break;

                    	default :
                    	    break loop2;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:73:21: (dsc= desc )*
                    loop3:
                    do {
                        int alt3=2;
                        int LA3_0 = input.LA(1);

                        if ( (LA3_0==L_PAREN) ) {
                            alt3=1;
                        }


                        switch (alt3) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: dsc= desc
                    	    {
                    	    pushFollow(FOLLOW_desc_in_finalitem462);
                    	    dsc=desc();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_desc.add(dsc.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop3;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:73:28: ( WS )*
                    loop4:
                    do {
                        int alt4=2;
                        int LA4_0 = input.LA(1);

                        if ( (LA4_0==WS) ) {
                            alt4=1;
                        }


                        switch (alt4) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS4=(Token)match(input,WS,FOLLOW_WS_in_finalitem465); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS4);


                    	    }
                    	    break;

                    	default :
                    	    break loop4;
                        }
                    } while (true);

                    COLON5=(Token)match(input,COLON,FOLLOW_COLON_in_finalitem468); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_COLON.add(COLON5);

                    // /home/dfranusic/cli.g:73:38: ( WS )*
                    loop5:
                    do {
                        int alt5=2;
                        int LA5_0 = input.LA(1);

                        if ( (LA5_0==WS) ) {
                            alt5=1;
                        }


                        switch (alt5) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS6=(Token)match(input,WS,FOLLOW_WS_in_finalitem470); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS6);


                    	    }
                    	    break;

                    	default :
                    	    break loop5;
                        }
                    } while (true);

                    method=(Token)match(input,WORD,FOLLOW_WORD_in_finalitem475); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_WORD.add(method);

                    // /home/dfranusic/cli.g:73:54: ( WS )*
                    loop6:
                    do {
                        int alt6=2;
                        int LA6_0 = input.LA(1);

                        if ( (LA6_0==WS) ) {
                            alt6=1;
                        }


                        switch (alt6) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS7=(Token)match(input,WS,FOLLOW_WS_in_finalitem477); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS7);


                    	    }
                    	    break;

                    	default :
                    	    break loop6;
                        }
                    } while (true);

                    STMTSEP8=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_finalitem480); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_STMTSEP.add(STMTSEP8);



                    // AST REWRITE
                    // elements: cmd, method
                    // token labels: cmd, method
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_cmd=new RewriteRuleTokenStream(adaptor,"token cmd",cmd);
                    RewriteRuleTokenStream stream_method=new RewriteRuleTokenStream(adaptor,"token method",method);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 74:3: -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                    {
                        // /home/dfranusic/cli.g:74:6: ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_FINAL, "CLI_FINAL"), root_1);

                        adaptor.addChild(root_1, stream_cmd.nextNode());
                        adaptor.addChild(root_1, stream_method.nextNode());
                        // /home/dfranusic/cli.g:74:31: ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_PARENT, "CLI_PARENT"), root_2);

                        adaptor.addChild(root_2, (Object)adaptor.create(CLI_PARENT, parent_lst.toString().replaceAll("[\\[\\] ]", "")));

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:74:107: ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, "CLI_DESC"), root_2);

                        // /home/dfranusic/cli.g:74:118: ^( CLI_DESC[$dsc.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, (dsc!=null?dsc.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;
                case 2 :
                    // /home/dfranusic/cli.g:75:5: cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP
                    {
                    cmd=(Token)match(input,WORD,FOLLOW_WORD_in_finalitem519); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_WORD.add(cmd);

                    // /home/dfranusic/cli.g:75:14: ( WS )*
                    loop7:
                    do {
                        int alt7=2;
                        int LA7_0 = input.LA(1);

                        if ( (LA7_0==WS) ) {
                            int LA7_2 = input.LA(2);

                            if ( (synpred8_cli()) ) {
                                alt7=1;
                            }


                        }


                        switch (alt7) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS9=(Token)match(input,WS,FOLLOW_WS_in_finalitem521); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS9);


                    	    }
                    	    break;

                    	default :
                    	    break loop7;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:75:21: (dsc= desc )*
                    loop8:
                    do {
                        int alt8=2;
                        int LA8_0 = input.LA(1);

                        if ( (LA8_0==L_PAREN) ) {
                            alt8=1;
                        }


                        switch (alt8) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: dsc= desc
                    	    {
                    	    pushFollow(FOLLOW_desc_in_finalitem526);
                    	    dsc=desc();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_desc.add(dsc.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop8;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:75:28: ( WS )*
                    loop9:
                    do {
                        int alt9=2;
                        int LA9_0 = input.LA(1);

                        if ( (LA9_0==WS) ) {
                            alt9=1;
                        }


                        switch (alt9) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS10=(Token)match(input,WS,FOLLOW_WS_in_finalitem529); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS10);


                    	    }
                    	    break;

                    	default :
                    	    break loop9;
                        }
                    } while (true);

                    L_SQ_B11=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_finalitem532); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_L_SQ_B.add(L_SQ_B11);

                    // /home/dfranusic/cli.g:75:39: ( params )*
                    loop10:
                    do {
                        int alt10=2;
                        int LA10_0 = input.LA(1);

                        if ( (LA10_0==WORD) ) {
                            alt10=1;
                        }


                        switch (alt10) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: params
                    	    {
                    	    pushFollow(FOLLOW_params_in_finalitem534);
                    	    params12=params();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_params.add(params12.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop10;
                        }
                    } while (true);

                    R_SQ_B13=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_finalitem537); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_R_SQ_B.add(R_SQ_B13);

                    // /home/dfranusic/cli.g:75:54: ( WS )*
                    loop11:
                    do {
                        int alt11=2;
                        int LA11_0 = input.LA(1);

                        if ( (LA11_0==WS) ) {
                            alt11=1;
                        }


                        switch (alt11) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS14=(Token)match(input,WS,FOLLOW_WS_in_finalitem539); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS14);


                    	    }
                    	    break;

                    	default :
                    	    break loop11;
                        }
                    } while (true);

                    COLON15=(Token)match(input,COLON,FOLLOW_COLON_in_finalitem542); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_COLON.add(COLON15);

                    // /home/dfranusic/cli.g:75:64: ( WS )*
                    loop12:
                    do {
                        int alt12=2;
                        int LA12_0 = input.LA(1);

                        if ( (LA12_0==WS) ) {
                            alt12=1;
                        }


                        switch (alt12) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS16=(Token)match(input,WS,FOLLOW_WS_in_finalitem544); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS16);


                    	    }
                    	    break;

                    	default :
                    	    break loop12;
                        }
                    } while (true);

                    method=(Token)match(input,WORD,FOLLOW_WORD_in_finalitem549); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_WORD.add(method);

                    // /home/dfranusic/cli.g:75:80: ( WS )*
                    loop13:
                    do {
                        int alt13=2;
                        int LA13_0 = input.LA(1);

                        if ( (LA13_0==WS) ) {
                            alt13=1;
                        }


                        switch (alt13) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS17=(Token)match(input,WS,FOLLOW_WS_in_finalitem551); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS17);


                    	    }
                    	    break;

                    	default :
                    	    break loop13;
                        }
                    } while (true);

                    STMTSEP18=(Token)match(input,STMTSEP,FOLLOW_STMTSEP_in_finalitem554); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_STMTSEP.add(STMTSEP18);



                    // AST REWRITE
                    // elements: cmd, method, params
                    // token labels: cmd, method
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_cmd=new RewriteRuleTokenStream(adaptor,"token cmd",cmd);
                    RewriteRuleTokenStream stream_method=new RewriteRuleTokenStream(adaptor,"token method",method);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 76:3: -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                    {
                        // /home/dfranusic/cli.g:76:6: ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_FINAL, "CLI_FINAL"), root_1);

                        adaptor.addChild(root_1, stream_cmd.nextNode());
                        adaptor.addChild(root_1, stream_method.nextNode());
                        // /home/dfranusic/cli.g:76:31: ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_PARENT, "CLI_PARENT"), root_2);

                        adaptor.addChild(root_2, (Object)adaptor.create(CLI_PARENT, parent_lst.toString().replaceAll("[\\[\\] ]", "")));

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:76:107: ^( CLI_PARAMS ( params )* )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_PARAMS, "CLI_PARAMS"), root_2);

                        // /home/dfranusic/cli.g:76:120: ( params )*
                        while ( stream_params.hasNext() ) {
                            adaptor.addChild(root_2, stream_params.nextTree());

                        }
                        stream_params.reset();

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:76:129: ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, "CLI_DESC"), root_2);

                        // /home/dfranusic/cli.g:76:140: ^( CLI_DESC[$dsc.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, (dsc!=null?dsc.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "finalitem"

    public static class params_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "params"
    // /home/dfranusic/cli.g:79:1: params : WORD ( COLON )* ;
    public final cliParser.params_return params() throws RecognitionException {
        cliParser.params_return retval = new cliParser.params_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token WORD19=null;
        Token COLON20=null;

        Object WORD19_tree=null;
        Object COLON20_tree=null;

        try {
            // /home/dfranusic/cli.g:80:3: ( WORD ( COLON )* )
            // /home/dfranusic/cli.g:80:5: WORD ( COLON )*
            {
            root_0 = (Object)adaptor.nil();

            WORD19=(Token)match(input,WORD,FOLLOW_WORD_in_params605); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
            WORD19_tree = (Object)adaptor.create(WORD19);
            adaptor.addChild(root_0, WORD19_tree);
            }
            // /home/dfranusic/cli.g:80:15: ( COLON )*
            loop15:
            do {
                int alt15=2;
                int LA15_0 = input.LA(1);

                if ( (LA15_0==COLON) ) {
                    alt15=1;
                }


                switch (alt15) {
            	case 1 :
            	    // /home/dfranusic/cli.g:0:0: COLON
            	    {
            	    COLON20=(Token)match(input,COLON,FOLLOW_COLON_in_params607); if (state.failed) return retval;

            	    }
            	    break;

            	default :
            	    break loop15;
                }
            } while (true);


            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "params"

    public static class complexitem_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "complexitem"
    // /home/dfranusic/cli.g:83:1: complexitem : (cmd= WORD ( WS )* (dsc= desc )* ( WS )* curlyblock -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) | cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* curlyblock -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) );
    public final cliParser.complexitem_return complexitem() throws RecognitionException {
        cliParser.complexitem_return retval = new cliParser.complexitem_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token cmd=null;
        Token WS21=null;
        Token WS22=null;
        Token WS24=null;
        Token WS25=null;
        Token L_SQ_B26=null;
        Token R_SQ_B28=null;
        Token WS29=null;
        cliParser.desc_return dsc = null;

        cliParser.curlyblock_return curlyblock23 = null;

        cliParser.params_return params27 = null;

        cliParser.curlyblock_return curlyblock30 = null;


        Object cmd_tree=null;
        Object WS21_tree=null;
        Object WS22_tree=null;
        Object WS24_tree=null;
        Object WS25_tree=null;
        Object L_SQ_B26_tree=null;
        Object R_SQ_B28_tree=null;
        Object WS29_tree=null;
        RewriteRuleTokenStream stream_WORD=new RewriteRuleTokenStream(adaptor,"token WORD");
        RewriteRuleTokenStream stream_WS=new RewriteRuleTokenStream(adaptor,"token WS");
        RewriteRuleTokenStream stream_L_SQ_B=new RewriteRuleTokenStream(adaptor,"token L_SQ_B");
        RewriteRuleTokenStream stream_R_SQ_B=new RewriteRuleTokenStream(adaptor,"token R_SQ_B");
        RewriteRuleSubtreeStream stream_desc=new RewriteRuleSubtreeStream(adaptor,"rule desc");
        RewriteRuleSubtreeStream stream_curlyblock=new RewriteRuleSubtreeStream(adaptor,"rule curlyblock");
        RewriteRuleSubtreeStream stream_params=new RewriteRuleSubtreeStream(adaptor,"rule params");
        try {
            // /home/dfranusic/cli.g:84:3: (cmd= WORD ( WS )* (dsc= desc )* ( WS )* curlyblock -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) | cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* curlyblock -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) )
            int alt24=2;
            alt24 = dfa24.predict(input);
            switch (alt24) {
                case 1 :
                    // /home/dfranusic/cli.g:84:5: cmd= WORD ( WS )* (dsc= desc )* ( WS )* curlyblock
                    {
                    cmd=(Token)match(input,WORD,FOLLOW_WORD_in_complexitem624); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_WORD.add(cmd);

                    if ( state.backtracking==0 ) {
                       parent_lst.add(0, (cmd!=null?cmd.getText():null)); 
                    }
                    // /home/dfranusic/cli.g:84:48: ( WS )*
                    loop16:
                    do {
                        int alt16=2;
                        int LA16_0 = input.LA(1);

                        if ( (LA16_0==WS) ) {
                            int LA16_2 = input.LA(2);

                            if ( (synpred16_cli()) ) {
                                alt16=1;
                            }


                        }


                        switch (alt16) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS21=(Token)match(input,WS,FOLLOW_WS_in_complexitem628); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS21);


                    	    }
                    	    break;

                    	default :
                    	    break loop16;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:84:55: (dsc= desc )*
                    loop17:
                    do {
                        int alt17=2;
                        int LA17_0 = input.LA(1);

                        if ( (LA17_0==L_PAREN) ) {
                            alt17=1;
                        }


                        switch (alt17) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: dsc= desc
                    	    {
                    	    pushFollow(FOLLOW_desc_in_complexitem633);
                    	    dsc=desc();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_desc.add(dsc.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop17;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:84:62: ( WS )*
                    loop18:
                    do {
                        int alt18=2;
                        int LA18_0 = input.LA(1);

                        if ( (LA18_0==WS) ) {
                            alt18=1;
                        }


                        switch (alt18) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS22=(Token)match(input,WS,FOLLOW_WS_in_complexitem636); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS22);


                    	    }
                    	    break;

                    	default :
                    	    break loop18;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_complexitem639);
                    curlyblock23=curlyblock();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) stream_curlyblock.add(curlyblock23.getTree());


                    // AST REWRITE
                    // elements: curlyblock, cmd
                    // token labels: cmd
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_cmd=new RewriteRuleTokenStream(adaptor,"token cmd",cmd);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 85:3: -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                    {
                        // /home/dfranusic/cli.g:85:6: ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_COMPLEX, "CLI_COMPLEX"), root_1);

                        // /home/dfranusic/cli.g:85:19: ^( $cmd curlyblock )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot(stream_cmd.nextNode(), root_2);

                        adaptor.addChild(root_2, stream_curlyblock.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:85:38: ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_PARENT, "CLI_PARENT"), root_2);

                        adaptor.addChild(root_2, (Object)adaptor.create(CLI_PARENT, parent_lst.toString().replaceAll("[\\[\\] ]", "")));

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:85:114: ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, "CLI_DESC"), root_2);

                        // /home/dfranusic/cli.g:85:125: ^( CLI_DESC[$dsc.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, (dsc!=null?dsc.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;
                case 2 :
                    // /home/dfranusic/cli.g:86:5: cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* curlyblock
                    {
                    cmd=(Token)match(input,WORD,FOLLOW_WORD_in_complexitem678); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_WORD.add(cmd);

                    if ( state.backtracking==0 ) {
                       parent_lst.add(0, (cmd!=null?cmd.getText():null)); 
                    }
                    // /home/dfranusic/cli.g:86:48: ( WS )*
                    loop19:
                    do {
                        int alt19=2;
                        int LA19_0 = input.LA(1);

                        if ( (LA19_0==WS) ) {
                            int LA19_2 = input.LA(2);

                            if ( (synpred20_cli()) ) {
                                alt19=1;
                            }


                        }


                        switch (alt19) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS24=(Token)match(input,WS,FOLLOW_WS_in_complexitem682); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS24);


                    	    }
                    	    break;

                    	default :
                    	    break loop19;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:86:55: (dsc= desc )*
                    loop20:
                    do {
                        int alt20=2;
                        int LA20_0 = input.LA(1);

                        if ( (LA20_0==L_PAREN) ) {
                            alt20=1;
                        }


                        switch (alt20) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: dsc= desc
                    	    {
                    	    pushFollow(FOLLOW_desc_in_complexitem687);
                    	    dsc=desc();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_desc.add(dsc.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop20;
                        }
                    } while (true);

                    // /home/dfranusic/cli.g:86:62: ( WS )*
                    loop21:
                    do {
                        int alt21=2;
                        int LA21_0 = input.LA(1);

                        if ( (LA21_0==WS) ) {
                            alt21=1;
                        }


                        switch (alt21) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS25=(Token)match(input,WS,FOLLOW_WS_in_complexitem690); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS25);


                    	    }
                    	    break;

                    	default :
                    	    break loop21;
                        }
                    } while (true);

                    L_SQ_B26=(Token)match(input,L_SQ_B,FOLLOW_L_SQ_B_in_complexitem693); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_L_SQ_B.add(L_SQ_B26);

                    // /home/dfranusic/cli.g:86:73: ( params )*
                    loop22:
                    do {
                        int alt22=2;
                        int LA22_0 = input.LA(1);

                        if ( (LA22_0==WORD) ) {
                            alt22=1;
                        }


                        switch (alt22) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: params
                    	    {
                    	    pushFollow(FOLLOW_params_in_complexitem695);
                    	    params27=params();

                    	    state._fsp--;
                    	    if (state.failed) return retval;
                    	    if ( state.backtracking==0 ) stream_params.add(params27.getTree());

                    	    }
                    	    break;

                    	default :
                    	    break loop22;
                        }
                    } while (true);

                    R_SQ_B28=(Token)match(input,R_SQ_B,FOLLOW_R_SQ_B_in_complexitem698); if (state.failed) return retval; 
                    if ( state.backtracking==0 ) stream_R_SQ_B.add(R_SQ_B28);

                    // /home/dfranusic/cli.g:86:88: ( WS )*
                    loop23:
                    do {
                        int alt23=2;
                        int LA23_0 = input.LA(1);

                        if ( (LA23_0==WS) ) {
                            alt23=1;
                        }


                        switch (alt23) {
                    	case 1 :
                    	    // /home/dfranusic/cli.g:0:0: WS
                    	    {
                    	    WS29=(Token)match(input,WS,FOLLOW_WS_in_complexitem700); if (state.failed) return retval; 
                    	    if ( state.backtracking==0 ) stream_WS.add(WS29);


                    	    }
                    	    break;

                    	default :
                    	    break loop23;
                        }
                    } while (true);

                    pushFollow(FOLLOW_curlyblock_in_complexitem703);
                    curlyblock30=curlyblock();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) stream_curlyblock.add(curlyblock30.getTree());


                    // AST REWRITE
                    // elements: cmd, curlyblock, params
                    // token labels: cmd
                    // rule labels: retval
                    // token list labels: 
                    // rule list labels: 
                    // wildcard labels: 
                    if ( state.backtracking==0 ) {
                    retval.tree = root_0;
                    RewriteRuleTokenStream stream_cmd=new RewriteRuleTokenStream(adaptor,"token cmd",cmd);
                    RewriteRuleSubtreeStream stream_retval=new RewriteRuleSubtreeStream(adaptor,"rule retval",retval!=null?retval.tree:null);

                    root_0 = (Object)adaptor.nil();
                    // 87:3: -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                    {
                        // /home/dfranusic/cli.g:87:6: ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) )
                        {
                        Object root_1 = (Object)adaptor.nil();
                        root_1 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_COMPLEX, "CLI_COMPLEX"), root_1);

                        // /home/dfranusic/cli.g:87:19: ^( $cmd curlyblock )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot(stream_cmd.nextNode(), root_2);

                        adaptor.addChild(root_2, stream_curlyblock.nextTree());

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:87:38: ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_PARENT, "CLI_PARENT"), root_2);

                        adaptor.addChild(root_2, (Object)adaptor.create(CLI_PARENT, parent_lst.toString().replaceAll("[\\[\\] ]", "")));

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:87:114: ^( CLI_PARAMS ( params )* )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_PARAMS, "CLI_PARAMS"), root_2);

                        // /home/dfranusic/cli.g:87:127: ( params )*
                        while ( stream_params.hasNext() ) {
                            adaptor.addChild(root_2, stream_params.nextTree());

                        }
                        stream_params.reset();

                        adaptor.addChild(root_1, root_2);
                        }
                        // /home/dfranusic/cli.g:87:136: ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) )
                        {
                        Object root_2 = (Object)adaptor.nil();
                        root_2 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, "CLI_DESC"), root_2);

                        // /home/dfranusic/cli.g:87:147: ^( CLI_DESC[$dsc.res] )
                        {
                        Object root_3 = (Object)adaptor.nil();
                        root_3 = (Object)adaptor.becomeRoot((Object)adaptor.create(CLI_DESC, (dsc!=null?dsc.res:null)), root_3);

                        adaptor.addChild(root_2, root_3);
                        }

                        adaptor.addChild(root_1, root_2);
                        }

                        adaptor.addChild(root_0, root_1);
                        }

                    }

                    retval.tree = root_0;}
                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "complexitem"

    public static class curlyblock_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "curlyblock"
    // /home/dfranusic/cli.g:90:1: curlyblock : L_CR_B ( cliitem )* R_CR_B ;
    public final cliParser.curlyblock_return curlyblock() throws RecognitionException {
        cliParser.curlyblock_return retval = new cliParser.curlyblock_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token L_CR_B31=null;
        Token R_CR_B33=null;
        cliParser.cliitem_return cliitem32 = null;


        Object L_CR_B31_tree=null;
        Object R_CR_B33_tree=null;

        try {
            // /home/dfranusic/cli.g:91:3: ( L_CR_B ( cliitem )* R_CR_B )
            // /home/dfranusic/cli.g:91:5: L_CR_B ( cliitem )* R_CR_B
            {
            root_0 = (Object)adaptor.nil();

            L_CR_B31=(Token)match(input,L_CR_B,FOLLOW_L_CR_B_in_curlyblock754); if (state.failed) return retval;
            // /home/dfranusic/cli.g:91:13: ( cliitem )*
            loop25:
            do {
                int alt25=2;
                int LA25_0 = input.LA(1);

                if ( (LA25_0==WORD) ) {
                    alt25=1;
                }


                switch (alt25) {
            	case 1 :
            	    // /home/dfranusic/cli.g:0:0: cliitem
            	    {
            	    pushFollow(FOLLOW_cliitem_in_curlyblock757);
            	    cliitem32=cliitem();

            	    state._fsp--;
            	    if (state.failed) return retval;
            	    if ( state.backtracking==0 ) adaptor.addChild(root_0, cliitem32.getTree());

            	    }
            	    break;

            	default :
            	    break loop25;
                }
            } while (true);

            R_CR_B33=(Token)match(input,R_CR_B,FOLLOW_R_CR_B_in_curlyblock760); if (state.failed) return retval;
            if ( state.backtracking==0 ) {
               parent_lst.remove(0); 
            }

            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "curlyblock"

    public static class cliitem_return extends ParserRuleReturnScope {
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "cliitem"
    // /home/dfranusic/cli.g:94:1: cliitem : ( finalitem | complexitem );
    public final cliParser.cliitem_return cliitem() throws RecognitionException {
        cliParser.cliitem_return retval = new cliParser.cliitem_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        cliParser.finalitem_return finalitem34 = null;

        cliParser.complexitem_return complexitem35 = null;



        try {
            // /home/dfranusic/cli.g:95:3: ( finalitem | complexitem )
            int alt26=2;
            alt26 = dfa26.predict(input);
            switch (alt26) {
                case 1 :
                    // /home/dfranusic/cli.g:95:5: finalitem
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_finalitem_in_cliitem776);
                    finalitem34=finalitem();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, finalitem34.getTree());

                    }
                    break;
                case 2 :
                    // /home/dfranusic/cli.g:96:5: complexitem
                    {
                    root_0 = (Object)adaptor.nil();

                    pushFollow(FOLLOW_complexitem_in_cliitem783);
                    complexitem35=complexitem();

                    state._fsp--;
                    if (state.failed) return retval;
                    if ( state.backtracking==0 ) adaptor.addChild(root_0, complexitem35.getTree());

                    }
                    break;

            }
            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "cliitem"

    public static class desc_return extends ParserRuleReturnScope {
        public String res;
        Object tree;
        public Object getTree() { return tree; }
    };

    // $ANTLR start "desc"
    // /home/dfranusic/cli.g:103:1: desc returns [String res] : L_PAREN (a= WORD | WS )* R_PAREN ;
    public final cliParser.desc_return desc() throws RecognitionException {
        cliParser.desc_return retval = new cliParser.desc_return();
        retval.start = input.LT(1);

        Object root_0 = null;

        Token a=null;
        Token L_PAREN36=null;
        Token WS37=null;
        Token R_PAREN38=null;

        Object a_tree=null;
        Object L_PAREN36_tree=null;
        Object WS37_tree=null;
        Object R_PAREN38_tree=null;

        try {
            // /home/dfranusic/cli.g:104:3: ( L_PAREN (a= WORD | WS )* R_PAREN )
            // /home/dfranusic/cli.g:104:5: L_PAREN (a= WORD | WS )* R_PAREN
            {
            root_0 = (Object)adaptor.nil();

            if ( state.backtracking==0 ) {
               retval.res = ""; 
            }
            L_PAREN36=(Token)match(input,L_PAREN,FOLLOW_L_PAREN_in_desc832); if (state.failed) return retval;
            // /home/dfranusic/cli.g:104:28: (a= WORD | WS )*
            loop27:
            do {
                int alt27=3;
                int LA27_0 = input.LA(1);

                if ( (LA27_0==WORD) ) {
                    alt27=1;
                }
                else if ( (LA27_0==WS) ) {
                    alt27=2;
                }


                switch (alt27) {
            	case 1 :
            	    // /home/dfranusic/cli.g:104:29: a= WORD
            	    {
            	    a=(Token)match(input,WORD,FOLLOW_WORD_in_desc838); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    a_tree = (Object)adaptor.create(a);
            	    adaptor.addChild(root_0, a_tree);
            	    }
            	    if ( state.backtracking==0 ) {
            	       retval.res += (a!=null?a.getText():null) + " "; 
            	    }

            	    }
            	    break;
            	case 2 :
            	    // /home/dfranusic/cli.g:104:64: WS
            	    {
            	    WS37=(Token)match(input,WS,FOLLOW_WS_in_desc843); if (state.failed) return retval;
            	    if ( state.backtracking==0 ) {
            	    WS37_tree = (Object)adaptor.create(WS37);
            	    adaptor.addChild(root_0, WS37_tree);
            	    }

            	    }
            	    break;

            	default :
            	    break loop27;
                }
            } while (true);

            R_PAREN38=(Token)match(input,R_PAREN,FOLLOW_R_PAREN_in_desc847); if (state.failed) return retval;

            }

            retval.stop = input.LT(-1);

            if ( state.backtracking==0 ) {

            retval.tree = (Object)adaptor.rulePostProcessing(root_0);
            adaptor.setTokenBoundaries(retval.tree, retval.start, retval.stop);
            }
        }
        catch (RecognitionException re) {
            reportError(re);
            recover(input,re);
    	retval.tree = (Object)adaptor.errorNode(input, retval.start, input.LT(-1), re);

        }
        finally {
        }
        return retval;
    }
    // $ANTLR end "desc"

    // $ANTLR start synpred2_cli
    public final void synpred2_cli_fragment() throws RecognitionException {   
        // /home/dfranusic/cli.g:73:14: ( WS )
        // /home/dfranusic/cli.g:73:14: WS
        {
        match(input,WS,FOLLOW_WS_in_synpred2_cli457); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred2_cli

    // $ANTLR start synpred8_cli
    public final void synpred8_cli_fragment() throws RecognitionException {   
        // /home/dfranusic/cli.g:75:14: ( WS )
        // /home/dfranusic/cli.g:75:14: WS
        {
        match(input,WS,FOLLOW_WS_in_synpred8_cli521); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred8_cli

    // $ANTLR start synpred16_cli
    public final void synpred16_cli_fragment() throws RecognitionException {   
        // /home/dfranusic/cli.g:84:48: ( WS )
        // /home/dfranusic/cli.g:84:48: WS
        {
        match(input,WS,FOLLOW_WS_in_synpred16_cli628); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred16_cli

    // $ANTLR start synpred20_cli
    public final void synpred20_cli_fragment() throws RecognitionException {   
        // /home/dfranusic/cli.g:86:48: ( WS )
        // /home/dfranusic/cli.g:86:48: WS
        {
        match(input,WS,FOLLOW_WS_in_synpred20_cli682); if (state.failed) return ;

        }
    }
    // $ANTLR end synpred20_cli

    // Delegated rules

    public final boolean synpred8_cli() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred8_cli_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred20_cli() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred20_cli_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred2_cli() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred2_cli_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }
    public final boolean synpred16_cli() {
        state.backtracking++;
        int start = input.mark();
        try {
            synpred16_cli_fragment(); // can never throw exception
        } catch (RecognitionException re) {
            System.err.println("impossible: "+re);
        }
        boolean success = !state.failed;
        input.rewind(start);
        state.backtracking--;
        state.failed=false;
        return success;
    }


    protected DFA14 dfa14 = new DFA14(this);
    protected DFA24 dfa24 = new DFA24(this);
    protected DFA26 dfa26 = new DFA26(this);
    static final String DFA14_eotS =
        "\12\uffff";
    static final String DFA14_eofS =
        "\12\uffff";
    static final String DFA14_minS =
        "\1\46\2\4\1\5\2\uffff\2\5\1\4\1\7";
    static final String DFA14_maxS =
        "\1\46\3\47\2\uffff\4\47";
    static final String DFA14_acceptS =
        "\4\uffff\1\1\1\2\4\uffff";
    static final String DFA14_specialS =
        "\12\uffff}>";
    static final String[] DFA14_transitionS = {
            "\1\1",
            "\1\3\2\uffff\1\5\7\uffff\1\4\27\uffff\1\2",
            "\1\3\2\uffff\1\5\7\uffff\1\4\27\uffff\1\2",
            "\1\10\40\uffff\1\6\1\7",
            "",
            "",
            "\1\10\40\uffff\1\6\1\7",
            "\1\10\40\uffff\1\6\1\7",
            "\1\3\2\uffff\1\5\7\uffff\1\4\27\uffff\1\11",
            "\1\5\7\uffff\1\4\27\uffff\1\11"
    };

    static final short[] DFA14_eot = DFA.unpackEncodedString(DFA14_eotS);
    static final short[] DFA14_eof = DFA.unpackEncodedString(DFA14_eofS);
    static final char[] DFA14_min = DFA.unpackEncodedStringToUnsignedChars(DFA14_minS);
    static final char[] DFA14_max = DFA.unpackEncodedStringToUnsignedChars(DFA14_maxS);
    static final short[] DFA14_accept = DFA.unpackEncodedString(DFA14_acceptS);
    static final short[] DFA14_special = DFA.unpackEncodedString(DFA14_specialS);
    static final short[][] DFA14_transition;

    static {
        int numStates = DFA14_transitionS.length;
        DFA14_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA14_transition[i] = DFA.unpackEncodedString(DFA14_transitionS[i]);
        }
    }

    class DFA14 extends DFA {

        public DFA14(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 14;
            this.eot = DFA14_eot;
            this.eof = DFA14_eof;
            this.min = DFA14_min;
            this.max = DFA14_max;
            this.accept = DFA14_accept;
            this.special = DFA14_special;
            this.transition = DFA14_transition;
        }
        public String getDescription() {
            return "72:1: finalitem : (cmd= WORD ( WS )* (dsc= desc )* ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) | cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* COLON ( WS )* method= WORD ( WS )* STMTSEP -> ^( CLI_FINAL $cmd $method ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) );";
        }
    }
    static final String DFA24_eotS =
        "\12\uffff";
    static final String DFA24_eofS =
        "\12\uffff";
    static final String DFA24_minS =
        "\1\46\2\4\1\5\2\uffff\2\5\1\4\1\7";
    static final String DFA24_maxS =
        "\1\46\3\47\2\uffff\4\47";
    static final String DFA24_acceptS =
        "\4\uffff\1\2\1\1\4\uffff";
    static final String DFA24_specialS =
        "\12\uffff}>";
    static final String[] DFA24_transitionS = {
            "\1\1",
            "\1\3\2\uffff\1\4\1\5\36\uffff\1\2",
            "\1\3\2\uffff\1\4\1\5\36\uffff\1\2",
            "\1\10\40\uffff\1\6\1\7",
            "",
            "",
            "\1\10\40\uffff\1\6\1\7",
            "\1\10\40\uffff\1\6\1\7",
            "\1\3\2\uffff\1\4\1\5\36\uffff\1\11",
            "\1\4\1\5\36\uffff\1\11"
    };

    static final short[] DFA24_eot = DFA.unpackEncodedString(DFA24_eotS);
    static final short[] DFA24_eof = DFA.unpackEncodedString(DFA24_eofS);
    static final char[] DFA24_min = DFA.unpackEncodedStringToUnsignedChars(DFA24_minS);
    static final char[] DFA24_max = DFA.unpackEncodedStringToUnsignedChars(DFA24_maxS);
    static final short[] DFA24_accept = DFA.unpackEncodedString(DFA24_acceptS);
    static final short[] DFA24_special = DFA.unpackEncodedString(DFA24_specialS);
    static final short[][] DFA24_transition;

    static {
        int numStates = DFA24_transitionS.length;
        DFA24_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA24_transition[i] = DFA.unpackEncodedString(DFA24_transitionS[i]);
        }
    }

    class DFA24 extends DFA {

        public DFA24(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 24;
            this.eot = DFA24_eot;
            this.eof = DFA24_eof;
            this.min = DFA24_min;
            this.max = DFA24_max;
            this.accept = DFA24_accept;
            this.special = DFA24_special;
            this.transition = DFA24_transition;
        }
        public String getDescription() {
            return "83:1: complexitem : (cmd= WORD ( WS )* (dsc= desc )* ( WS )* curlyblock -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) | cmd= WORD ( WS )* (dsc= desc )* ( WS )* L_SQ_B ( params )* R_SQ_B ( WS )* curlyblock -> ^( CLI_COMPLEX ^( $cmd curlyblock ) ^( CLI_PARENT CLI_PARENT[parent_lst.toString().replaceAll(\"[\\\\[\\\\] ]\", \"\")] ) ^( CLI_PARAMS ( params )* ) ^( CLI_DESC ^( CLI_DESC[$dsc.res] ) ) ) );";
        }
    }
    static final String DFA26_eotS =
        "\17\uffff";
    static final String DFA26_eofS =
        "\17\uffff";
    static final String DFA26_minS =
        "\1\46\2\4\1\5\1\6\2\uffff\2\5\1\4\1\6\1\10\1\7\1\6\1\10";
    static final String DFA26_maxS =
        "\1\46\3\47\1\46\2\uffff\3\47\1\46\2\47\1\46\1\47";
    static final String DFA26_acceptS =
        "\5\uffff\1\2\1\1\10\uffff";
    static final String DFA26_specialS =
        "\17\uffff}>";
    static final String[] DFA26_transitionS = {
            "\1\1",
            "\1\3\2\uffff\1\4\1\5\6\uffff\1\6\27\uffff\1\2",
            "\1\3\2\uffff\1\4\1\5\6\uffff\1\6\27\uffff\1\2",
            "\1\11\40\uffff\1\7\1\10",
            "\1\13\37\uffff\1\12",
            "",
            "",
            "\1\11\40\uffff\1\7\1\10",
            "\1\11\40\uffff\1\7\1\10",
            "\1\3\2\uffff\1\4\1\5\6\uffff\1\6\27\uffff\1\14",
            "\1\13\10\uffff\1\15\26\uffff\1\12",
            "\1\5\6\uffff\1\6\27\uffff\1\16",
            "\1\4\1\5\6\uffff\1\6\27\uffff\1\14",
            "\1\13\10\uffff\1\15\26\uffff\1\12",
            "\1\5\6\uffff\1\6\27\uffff\1\16"
    };

    static final short[] DFA26_eot = DFA.unpackEncodedString(DFA26_eotS);
    static final short[] DFA26_eof = DFA.unpackEncodedString(DFA26_eofS);
    static final char[] DFA26_min = DFA.unpackEncodedStringToUnsignedChars(DFA26_minS);
    static final char[] DFA26_max = DFA.unpackEncodedStringToUnsignedChars(DFA26_maxS);
    static final short[] DFA26_accept = DFA.unpackEncodedString(DFA26_acceptS);
    static final short[] DFA26_special = DFA.unpackEncodedString(DFA26_specialS);
    static final short[][] DFA26_transition;

    static {
        int numStates = DFA26_transitionS.length;
        DFA26_transition = new short[numStates][];
        for (int i=0; i<numStates; i++) {
            DFA26_transition[i] = DFA.unpackEncodedString(DFA26_transitionS[i]);
        }
    }

    class DFA26 extends DFA {

        public DFA26(BaseRecognizer recognizer) {
            this.recognizer = recognizer;
            this.decisionNumber = 26;
            this.eot = DFA26_eot;
            this.eof = DFA26_eof;
            this.min = DFA26_min;
            this.max = DFA26_max;
            this.accept = DFA26_accept;
            this.special = DFA26_special;
            this.transition = DFA26_transition;
        }
        public String getDescription() {
            return "94:1: cliitem : ( finalitem | complexitem );";
        }
    }
 

    public static final BitSet FOLLOW_cliitem_in_input435 = new BitSet(new long[]{0x0000004000000000L});
    public static final BitSet FOLLOW_EOF_in_input439 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_finalitem455 = new BitSet(new long[]{0x0000008000008010L});
    public static final BitSet FOLLOW_WS_in_finalitem457 = new BitSet(new long[]{0x0000008000008010L});
    public static final BitSet FOLLOW_desc_in_finalitem462 = new BitSet(new long[]{0x0000008000008010L});
    public static final BitSet FOLLOW_WS_in_finalitem465 = new BitSet(new long[]{0x0000008000008000L});
    public static final BitSet FOLLOW_COLON_in_finalitem468 = new BitSet(new long[]{0x000000C000000000L});
    public static final BitSet FOLLOW_WS_in_finalitem470 = new BitSet(new long[]{0x000000C000000000L});
    public static final BitSet FOLLOW_WORD_in_finalitem475 = new BitSet(new long[]{0x0000008000010000L});
    public static final BitSet FOLLOW_WS_in_finalitem477 = new BitSet(new long[]{0x0000008000010000L});
    public static final BitSet FOLLOW_STMTSEP_in_finalitem480 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_finalitem519 = new BitSet(new long[]{0x0000008000000090L});
    public static final BitSet FOLLOW_WS_in_finalitem521 = new BitSet(new long[]{0x0000008000000090L});
    public static final BitSet FOLLOW_desc_in_finalitem526 = new BitSet(new long[]{0x0000008000000090L});
    public static final BitSet FOLLOW_WS_in_finalitem529 = new BitSet(new long[]{0x0000008000000080L});
    public static final BitSet FOLLOW_L_SQ_B_in_finalitem532 = new BitSet(new long[]{0x0000004000000040L});
    public static final BitSet FOLLOW_params_in_finalitem534 = new BitSet(new long[]{0x0000004000000040L});
    public static final BitSet FOLLOW_R_SQ_B_in_finalitem537 = new BitSet(new long[]{0x0000008000008000L});
    public static final BitSet FOLLOW_WS_in_finalitem539 = new BitSet(new long[]{0x0000008000008000L});
    public static final BitSet FOLLOW_COLON_in_finalitem542 = new BitSet(new long[]{0x000000C000000000L});
    public static final BitSet FOLLOW_WS_in_finalitem544 = new BitSet(new long[]{0x000000C000000000L});
    public static final BitSet FOLLOW_WORD_in_finalitem549 = new BitSet(new long[]{0x0000008000010000L});
    public static final BitSet FOLLOW_WS_in_finalitem551 = new BitSet(new long[]{0x0000008000010000L});
    public static final BitSet FOLLOW_STMTSEP_in_finalitem554 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_params605 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_COLON_in_params607 = new BitSet(new long[]{0x0000000000008002L});
    public static final BitSet FOLLOW_WORD_in_complexitem624 = new BitSet(new long[]{0x0000008000000110L});
    public static final BitSet FOLLOW_WS_in_complexitem628 = new BitSet(new long[]{0x0000008000000110L});
    public static final BitSet FOLLOW_desc_in_complexitem633 = new BitSet(new long[]{0x0000008000000110L});
    public static final BitSet FOLLOW_WS_in_complexitem636 = new BitSet(new long[]{0x0000008000000110L});
    public static final BitSet FOLLOW_curlyblock_in_complexitem639 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WORD_in_complexitem678 = new BitSet(new long[]{0x0000008000000090L});
    public static final BitSet FOLLOW_WS_in_complexitem682 = new BitSet(new long[]{0x0000008000000090L});
    public static final BitSet FOLLOW_desc_in_complexitem687 = new BitSet(new long[]{0x0000008000000090L});
    public static final BitSet FOLLOW_WS_in_complexitem690 = new BitSet(new long[]{0x0000008000000080L});
    public static final BitSet FOLLOW_L_SQ_B_in_complexitem693 = new BitSet(new long[]{0x0000004000000040L});
    public static final BitSet FOLLOW_params_in_complexitem695 = new BitSet(new long[]{0x0000004000000040L});
    public static final BitSet FOLLOW_R_SQ_B_in_complexitem698 = new BitSet(new long[]{0x0000008000000110L});
    public static final BitSet FOLLOW_WS_in_complexitem700 = new BitSet(new long[]{0x0000008000000110L});
    public static final BitSet FOLLOW_curlyblock_in_complexitem703 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_CR_B_in_curlyblock754 = new BitSet(new long[]{0x0000004000000200L});
    public static final BitSet FOLLOW_cliitem_in_curlyblock757 = new BitSet(new long[]{0x0000004000000200L});
    public static final BitSet FOLLOW_R_CR_B_in_curlyblock760 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_finalitem_in_cliitem776 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_complexitem_in_cliitem783 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_L_PAREN_in_desc832 = new BitSet(new long[]{0x000000C000000020L});
    public static final BitSet FOLLOW_WORD_in_desc838 = new BitSet(new long[]{0x000000C000000020L});
    public static final BitSet FOLLOW_WS_in_desc843 = new BitSet(new long[]{0x000000C000000020L});
    public static final BitSet FOLLOW_R_PAREN_in_desc847 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WS_in_synpred2_cli457 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WS_in_synpred8_cli521 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WS_in_synpred16_cli628 = new BitSet(new long[]{0x0000000000000002L});
    public static final BitSet FOLLOW_WS_in_synpred20_cli682 = new BitSet(new long[]{0x0000000000000002L});

}