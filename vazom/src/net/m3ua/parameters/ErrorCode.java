package net.m3ua.parameters;

import net.m3ua.M3UAParameterType;
import net.m3ua.parameters.errorcode.ErrorCodeType;

public class ErrorCode extends M3UAParameter {
	public ErrorCodeType errorCodeType;
	public ErrorCode(){
		type = M3UAParameterType.ERROR_CODE;
	}
	
	public void setErrorCode(int val){
		value = new byte[4];
		value[0] = (byte)(val >> 24);
		value[1] = (byte)(val >> 16);
		value[2] = (byte)(val >> 8);
		value[3] = (byte)(val & 0xFF);
		
	}
	
	public void init(byte[] data) {
		value = data;
		errorCodeType = ErrorCodeType.get((data[0] << 24) + (data[1] << 16) + (data[2] << 8) + (data[3] & 0xFF));
	}


}
