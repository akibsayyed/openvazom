package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class AuthenticationRequestRes extends SET{

	public AnalogRedirectRecord get_analogRedirectRecord(){
		return (AnalogRedirectRecord)elements.get(0).data;
	}
	public AnalogRedirectRecord new_analogRedirectRecord(){
		return new AnalogRedirectRecord();
	}

	public AuthenticationAlgorithmVersion get_authenticationAlgorithmVersion(){
		return (AuthenticationAlgorithmVersion)elements.get(1).data;
	}
	public AuthenticationAlgorithmVersion new_authenticationAlgorithmVersion(){
		return new AuthenticationAlgorithmVersion();
	}

	public AuthenticationResponseUniqueChallenge get_authenticationResponseUniqueChallenge(){
		return (AuthenticationResponseUniqueChallenge)elements.get(2).data;
	}
	public AuthenticationResponseUniqueChallenge new_authenticationResponseUniqueChallenge(){
		return new AuthenticationResponseUniqueChallenge();
	}

	public CallHistoryCount get_callHistoryCount(){
		return (CallHistoryCount)elements.get(3).data;
	}
	public CallHistoryCount new_callHistoryCount(){
		return new CallHistoryCount();
	}

	public CarrierDigits get_carrierDigits(){
		return (CarrierDigits)elements.get(4).data;
	}
	public CarrierDigits new_carrierDigits(){
		return new CarrierDigits();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(5).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMARedirectRecord get_cdmaRedirectRecord(){
		return (CDMARedirectRecord)elements.get(6).data;
	}
	public CDMARedirectRecord new_cdmaRedirectRecord(){
		return new CDMARedirectRecord();
	}

	public DataKey get_dataKey(){
		return (DataKey)elements.get(7).data;
	}
	public DataKey new_dataKey(){
		return new DataKey();
	}

	public DenyAccess get_denyAccess(){
		return (DenyAccess)elements.get(8).data;
	}
	public DenyAccess new_denyAccess(){
		return new DenyAccess();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(9).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public RoamingIndication get_roamingIndication(){
		return (RoamingIndication)elements.get(10).data;
	}
	public RoamingIndication new_roamingIndication(){
		return new RoamingIndication();
	}

	public ServiceRedirectionInfo get_serviceRedirectionInfo(){
		return (ServiceRedirectionInfo)elements.get(11).data;
	}
	public ServiceRedirectionInfo new_serviceRedirectionInfo(){
		return new ServiceRedirectionInfo();
	}

	public DestinationDigits get_destinationDigits(){
		return (DestinationDigits)elements.get(12).data;
	}
	public DestinationDigits new_destinationDigits(){
		return new DestinationDigits();
	}

	public RandomVariableSSD get_randomVariableSSD(){
		return (RandomVariableSSD)elements.get(13).data;
	}
	public RandomVariableSSD new_randomVariableSSD(){
		return new RandomVariableSSD();
	}

	public RandomVariableUniqueChallenge get_randomVariableUniqueChallenge(){
		return (RandomVariableUniqueChallenge)elements.get(14).data;
	}
	public RandomVariableUniqueChallenge new_randomVariableUniqueChallenge(){
		return new RandomVariableUniqueChallenge();
	}

	public RoutingDigits get_routingDigits(){
		return (RoutingDigits)elements.get(15).data;
	}
	public RoutingDigits new_routingDigits(){
		return new RoutingDigits();
	}

	public SharedSecretData get_sharedSecretData(){
		return (SharedSecretData)elements.get(16).data;
	}
	public SharedSecretData new_sharedSecretData(){
		return new SharedSecretData();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(17).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public SSDNotShared get_ssdnotShared(){
		return (SSDNotShared)elements.get(18).data;
	}
	public SSDNotShared new_ssdnotShared(){
		return new SSDNotShared();
	}

	public UpdateCount get_updateCount(){
		return (UpdateCount)elements.get(19).data;
	}
	public UpdateCount new_updateCount(){
		return new UpdateCount();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(20).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(225, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_analogRedirectRecord(); }});
		elements.add(new ElementDescriptor(77, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationAlgorithmVersion(); }});
		elements.add(new ElementDescriptor(37, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_authenticationResponseUniqueChallenge(); }});
		elements.add(new ElementDescriptor(38, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_callHistoryCount(); }});
		elements.add(new ElementDescriptor(86, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_carrierDigits(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(229, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaRedirectRecord(); }});
		elements.add(new ElementDescriptor(215, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataKey(); }});
		elements.add(new ElementDescriptor(50, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_denyAccess(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(239, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_roamingIndication(); }});
		elements.add(new ElementDescriptor(238, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_serviceRedirectionInfo(); }});
		elements.add(new ElementDescriptor(87, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_destinationDigits(); }});
		elements.add(new ElementDescriptor(42, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableSSD(); }});
		elements.add(new ElementDescriptor(43, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariableUniqueChallenge(); }});
		elements.add(new ElementDescriptor(150, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_routingDigits(); }});
		elements.add(new ElementDescriptor(46, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_sharedSecretData(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(52, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ssdnotShared(); }});
		elements.add(new ElementDescriptor(51, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_updateCount(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
	}
	public AuthenticationRequestRes(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
