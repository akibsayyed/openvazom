package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ServiceChangeProfile extends SEQUENCE{

	public IA5STRING get_profileName(){
		return (IA5STRING)elements.get(0).data;
	}
	public IA5STRING new_profileName(){
		return new IA5STRING();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_profileName(); }});
	}
	public ServiceChangeProfile(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
