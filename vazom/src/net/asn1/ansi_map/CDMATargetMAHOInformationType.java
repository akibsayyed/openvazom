package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
public class CDMATargetMAHOInformationType extends CDMATargetMAHOInformation{

	public CDMATargetMAHOInformationType(){
		super();
		asn_pc = ASNTagComplexity.Primitive;
		tag = 135;
		asn_class = ASNTagClass.CONTEXT_SPECIFIC;
	}
}
