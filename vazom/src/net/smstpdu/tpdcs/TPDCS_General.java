package net.smstpdu.tpdcs;

import net.smstpdu.tpdcs.general.Alphabet;
import net.smstpdu.tpdcs.general.MessageClass;

public class TPDCS_General extends TPDCS {
	
	public boolean isCompressed;
	public boolean hasMessageClass;
	public MessageClass messageClass;
	public Alphabet alphabet;

	public byte encode(){
		byte tmp_res = (byte)group.getId();
		if(!isCompressed && !hasMessageClass && alphabet == Alphabet.DEFAULT) return 0x00;
		else{
			tmp_res += (isCompressed ? 0x20 : 0);
			tmp_res += (hasMessageClass ? 0x10 : 0);
			tmp_res += alphabet.getId();
			return tmp_res;
		}
		
	}

	public void init(byte data){
		//NOTE: The special case of bits 7-0 being 0000 0000 indicates
		//the Default GSM Alphabet.
		if(data == 0x00){
			isCompressed = false;
			hasMessageClass = false;
			alphabet = Alphabet.DEFAULT;
		}else{
			// bit 5
			isCompressed = (data & 0x20) == 0x20;
			// bit 4
			hasMessageClass = (data & 0x10) == 0x10;
			// bits 3, 2
			alphabet = Alphabet.get(data & 0x0C);
			// bits 1, 0
			if(hasMessageClass) messageClass = MessageClass.get(data & 0x03);
			
		}
		encoding = alphabet;
		
	}
	public TPDCS_General(){
		super();
		group = TPDCSGroup.GENERAL;
	}

}
