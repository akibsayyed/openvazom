package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class ActionReply extends SEQUENCE{

	public ContextID get_contextId(){
		return (ContextID)elements.get(0).data;
	}
	public ContextID new_contextId(){
		return new ContextID();
	}

	public ErrorDescriptor get_errorDescriptor(){
		return (ErrorDescriptor)elements.get(1).data;
	}
	public ErrorDescriptor new_errorDescriptor(){
		return new ErrorDescriptor();
	}

	public ContextRequest get_contextReply(){
		return (ContextRequest)elements.get(2).data;
	}
	public ContextRequest new_contextReply(){
		return new ContextRequest();
	}

	public CommandReplySequenceOf get_commandReply(){
		return (CommandReplySequenceOf)elements.get(3).data;
	}
	public CommandReplySequenceOf new_commandReply(){
		return new CommandReplySequenceOf();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_contextId(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_errorDescriptor(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_contextReply(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_commandReply(); }});
	}
	public ActionReply(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
