package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class QualificationRequest extends SET{

	public ElectronicSerialNumber get_electronicSerialNumber(){
		return (ElectronicSerialNumber)elements.get(0).data;
	}
	public ElectronicSerialNumber new_electronicSerialNumber(){
		return new ElectronicSerialNumber();
	}

	public MSID get_msid(){
		return (MSID)elements.get(1).data;
	}
	public MSID new_msid(){
		return new MSID();
	}

	public QualificationInformationCode get_qualificationInformationCode(){
		return (QualificationInformationCode)elements.get(2).data;
	}
	public QualificationInformationCode new_qualificationInformationCode(){
		return new QualificationInformationCode();
	}

	public SystemMyTypeCode get_systemMyTypeCode(){
		return (SystemMyTypeCode)elements.get(3).data;
	}
	public SystemMyTypeCode new_systemMyTypeCode(){
		return new SystemMyTypeCode();
	}

	public LocationAreaID get_locationAreaID(){
		return (LocationAreaID)elements.get(4).data;
	}
	public LocationAreaID new_locationAreaID(){
		return new LocationAreaID();
	}

	public CDMANetworkIdentification get_cdmaNetworkIdentification(){
		return (CDMANetworkIdentification)elements.get(5).data;
	}
	public CDMANetworkIdentification new_cdmaNetworkIdentification(){
		return new CDMANetworkIdentification();
	}

	public ControlChannelMode get_controlChannelMode(){
		return (ControlChannelMode)elements.get(6).data;
	}
	public ControlChannelMode new_controlChannelMode(){
		return new ControlChannelMode();
	}

	public MSCID get_mscid(){
		return (MSCID)elements.get(7).data;
	}
	public MSCID new_mscid(){
		return new MSCID();
	}

	public SenderIdentificationNumber get_senderIdentificationNumber(){
		return (SenderIdentificationNumber)elements.get(8).data;
	}
	public SenderIdentificationNumber new_senderIdentificationNumber(){
		return new SenderIdentificationNumber();
	}

	public SystemAccessType get_systemAccessType(){
		return (SystemAccessType)elements.get(9).data;
	}
	public SystemAccessType new_systemAccessType(){
		return new SystemAccessType();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(10).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public TransactionCapability get_transactionCapability(){
		return (TransactionCapability)elements.get(11).data;
	}
	public TransactionCapability new_transactionCapability(){
		return new TransactionCapability();
	}

	public WINCapability get_winCapability(){
		return (WINCapability)elements.get(12).data;
	}
	public WINCapability new_winCapability(){
		return new WINCapability();
	}

	public NonPublicData get_nonPublicData(){
		return (NonPublicData)elements.get(13).data;
	}
	public NonPublicData new_nonPublicData(){
		return new NonPublicData();
	}

	public UserZoneData get_userZoneData(){
		return (UserZoneData)elements.get(14).data;
	}
	public UserZoneData new_userZoneData(){
		return new UserZoneData();
	}

	public MEID get_meid(){
		return (MEID)elements.get(15).data;
	}
	public MEID new_meid(){
		return new MEID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(9, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_electronicSerialNumber(); }});
		elements.add(new ElementDescriptor(-3, ASNTagClass.UNIVERSAL, true, false){public void set(){ data = new_msid(); }});
		elements.add(new ElementDescriptor(17, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_qualificationInformationCode(); }});
		elements.add(new ElementDescriptor(22, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_systemMyTypeCode(); }});
		elements.add(new ElementDescriptor(33, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_locationAreaID(); }});
		elements.add(new ElementDescriptor(232, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaNetworkIdentification(); }});
		elements.add(new ElementDescriptor(199, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_controlChannelMode(); }});
		elements.add(new ElementDescriptor(21, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_mscid(); }});
		elements.add(new ElementDescriptor(103, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_senderIdentificationNumber(); }});
		elements.add(new ElementDescriptor(34, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemAccessType(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(123, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_transactionCapability(); }});
		elements.add(new ElementDescriptor(280, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_winCapability(); }});
		elements.add(new ElementDescriptor(200, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nonPublicData(); }});
		elements.add(new ElementDescriptor(209, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_userZoneData(); }});
		elements.add(new ElementDescriptor(390, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_meid(); }});
	}
	public QualificationRequest(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
