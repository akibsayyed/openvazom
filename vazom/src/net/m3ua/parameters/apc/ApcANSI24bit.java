package net.m3ua.parameters.apc;

public class ApcANSI24bit extends Apc {
	public int network;
	public int cluster;
	public int member;
	
	public ApcANSI24bit(){
		type = ApcType._24BIT;
	}
	
	public void init(byte[] data) {
		mask = data[0] & 0xFF;
		network = data[1] & 0xFF;
		cluster = data[2] & 0xFF;
		member = data[3] & 0xFF;
	}

}
