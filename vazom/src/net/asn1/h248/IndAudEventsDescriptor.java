package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudEventsDescriptor extends SEQUENCE{

	public RequestID get_requestID(){
		return (RequestID)elements.get(0).data;
	}
	public RequestID new_requestID(){
		return new RequestID();
	}

	public PkgdName get_pkgdName(){
		return (PkgdName)elements.get(1).data;
	}
	public PkgdName new_pkgdName(){
		return new PkgdName();
	}

	public StreamID get_streamID(){
		return (StreamID)elements.get(2).data;
	}
	public StreamID new_streamID(){
		return new StreamID();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_requestID(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_pkgdName(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_streamID(); }});
	}
	public IndAudEventsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
