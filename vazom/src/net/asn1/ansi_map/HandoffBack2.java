package net.asn1.ansi_map;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class HandoffBack2 extends SET{

	public BillingID get_billingID(){
		return (BillingID)elements.get(0).data;
	}
	public BillingID new_billingID(){
		return new BillingID();
	}

	public InterMSCCircuitID get_interMSCCircuitID(){
		return (InterMSCCircuitID)elements.get(1).data;
	}
	public InterMSCCircuitID new_interMSCCircuitID(){
		return new InterMSCCircuitID();
	}

	public MobileIdentificationNumber get_mobileIdentificationNumber(){
		return (MobileIdentificationNumber)elements.get(2).data;
	}
	public MobileIdentificationNumber new_mobileIdentificationNumber(){
		return new MobileIdentificationNumber();
	}

	public ServingCellID get_servingCellID(){
		return (ServingCellID)elements.get(3).data;
	}
	public ServingCellID new_servingCellID(){
		return new ServingCellID();
	}

	public BaseStationManufacturerCode get_baseStationManufacturerCode(){
		return (BaseStationManufacturerCode)elements.get(4).data;
	}
	public BaseStationManufacturerCode new_baseStationManufacturerCode(){
		return new BaseStationManufacturerCode();
	}

	public CDMA2000HandoffInvokeIOSData get_cdma2000HandoffInvokeIOSData(){
		return (CDMA2000HandoffInvokeIOSData)elements.get(5).data;
	}
	public CDMA2000HandoffInvokeIOSData new_cdma2000HandoffInvokeIOSData(){
		return new CDMA2000HandoffInvokeIOSData();
	}

	public CDMABandClassList get_cdmaBandClassList(){
		return (CDMABandClassList)elements.get(6).data;
	}
	public CDMABandClassList new_cdmaBandClassList(){
		return new CDMABandClassList();
	}

	public CDMACallMode get_cdmaCallMode(){
		return (CDMACallMode)elements.get(7).data;
	}
	public CDMACallMode new_cdmaCallMode(){
		return new CDMACallMode();
	}

	public CDMAChannelData get_cdmaChannelData(){
		return (CDMAChannelData)elements.get(8).data;
	}
	public CDMAChannelData new_cdmaChannelData(){
		return new CDMAChannelData();
	}

	public CDMAConnectionReferenceList get_cdmaConnectionReferenceList(){
		return (CDMAConnectionReferenceList)elements.get(9).data;
	}
	public CDMAConnectionReferenceList new_cdmaConnectionReferenceList(){
		return new CDMAConnectionReferenceList();
	}

	public CDMAMSMeasuredChannelIdentity get_cdmaMSMeasuredChannelIdentity(){
		return (CDMAMSMeasuredChannelIdentity)elements.get(10).data;
	}
	public CDMAMSMeasuredChannelIdentity new_cdmaMSMeasuredChannelIdentity(){
		return new CDMAMSMeasuredChannelIdentity();
	}

	public CDMAMobileProtocolRevision get_cdmaMobileProtocolRevision(){
		return (CDMAMobileProtocolRevision)elements.get(11).data;
	}
	public CDMAMobileProtocolRevision new_cdmaMobileProtocolRevision(){
		return new CDMAMobileProtocolRevision();
	}

	public CDMAServiceConfigurationRecord get_cdmaServiceConfigurationRecord(){
		return (CDMAServiceConfigurationRecord)elements.get(12).data;
	}
	public CDMAServiceConfigurationRecord new_cdmaServiceConfigurationRecord(){
		return new CDMAServiceConfigurationRecord();
	}

	public CDMAServiceOptionList get_cdmaServiceOptionList(){
		return (CDMAServiceOptionList)elements.get(13).data;
	}
	public CDMAServiceOptionList new_cdmaServiceOptionList(){
		return new CDMAServiceOptionList();
	}

	public CDMAPrivateLongCodeMask get_cdmaPrivateLongCodeMask(){
		return (CDMAPrivateLongCodeMask)elements.get(14).data;
	}
	public CDMAPrivateLongCodeMask new_cdmaPrivateLongCodeMask(){
		return new CDMAPrivateLongCodeMask();
	}

	public CDMAServingOneWayDelay get_cdmaServingOneWayDelay(){
		return (CDMAServingOneWayDelay)elements.get(15).data;
	}
	public CDMAServingOneWayDelay new_cdmaServingOneWayDelay(){
		return new CDMAServingOneWayDelay();
	}

	public CDMAStationClassMark get_cdmaStationClassMark(){
		return (CDMAStationClassMark)elements.get(16).data;
	}
	public CDMAStationClassMark new_cdmaStationClassMark(){
		return new CDMAStationClassMark();
	}

	public CDMAStationClassMark2 get_cdmaStationClassMark2(){
		return (CDMAStationClassMark2)elements.get(17).data;
	}
	public CDMAStationClassMark2 new_cdmaStationClassMark2(){
		return new CDMAStationClassMark2();
	}

	public CDMATargetMAHOList get_cdmaTargetMAHOList(){
		return (CDMATargetMAHOList)elements.get(18).data;
	}
	public CDMATargetMAHOList new_cdmaTargetMAHOList(){
		return new CDMATargetMAHOList();
	}

	public CDMATargetMeasurementList get_cdmaTargetMeasurementList(){
		return (CDMATargetMeasurementList)elements.get(19).data;
	}
	public CDMATargetMeasurementList new_cdmaTargetMeasurementList(){
		return new CDMATargetMeasurementList();
	}

	public ChannelData get_channelData(){
		return (ChannelData)elements.get(20).data;
	}
	public ChannelData new_channelData(){
		return new ChannelData();
	}

	public ConfidentialityModes get_confidentialityModes(){
		return (ConfidentialityModes)elements.get(21).data;
	}
	public ConfidentialityModes new_confidentialityModes(){
		return new ConfidentialityModes();
	}

	public DataKey get_dataKey(){
		return (DataKey)elements.get(22).data;
	}
	public DataKey new_dataKey(){
		return new DataKey();
	}

	public DataPrivacyParameters get_dataPrivacyParameters(){
		return (DataPrivacyParameters)elements.get(23).data;
	}
	public DataPrivacyParameters new_dataPrivacyParameters(){
		return new DataPrivacyParameters();
	}

	public HandoffReason get_handoffReason(){
		return (HandoffReason)elements.get(24).data;
	}
	public HandoffReason new_handoffReason(){
		return new HandoffReason();
	}

	public HandoffState get_handoffState(){
		return (HandoffState)elements.get(25).data;
	}
	public HandoffState new_handoffState(){
		return new HandoffState();
	}

	public InterSwitchCount get_interSwitchCount(){
		return (InterSwitchCount)elements.get(26).data;
	}
	public InterSwitchCount new_interSwitchCount(){
		return new InterSwitchCount();
	}

	public ISLPInformation get_ilspInformation(){
		return (ISLPInformation)elements.get(27).data;
	}
	public ISLPInformation new_ilspInformation(){
		return new ISLPInformation();
	}

	public MSLocation get_msLocation(){
		return (MSLocation)elements.get(28).data;
	}
	public MSLocation new_msLocation(){
		return new MSLocation();
	}

	public NAMPSCallMode get_nampsCallMode(){
		return (NAMPSCallMode)elements.get(29).data;
	}
	public NAMPSCallMode new_nampsCallMode(){
		return new NAMPSCallMode();
	}

	public NAMPSChannelData get_nampsChannelData(){
		return (NAMPSChannelData)elements.get(30).data;
	}
	public NAMPSChannelData new_nampsChannelData(){
		return new NAMPSChannelData();
	}

	public PDSNAddress get_pdsnAddress(){
		return (PDSNAddress)elements.get(31).data;
	}
	public PDSNAddress new_pdsnAddress(){
		return new PDSNAddress();
	}

	public PDSNProtocolType get_pdsnProtocolType(){
		return (PDSNProtocolType)elements.get(32).data;
	}
	public PDSNProtocolType new_pdsnProtocolType(){
		return new PDSNProtocolType();
	}

	public QoSPriority get_qosPriority(){
		return (QoSPriority)elements.get(33).data;
	}
	public QoSPriority new_qosPriority(){
		return new QoSPriority();
	}

	public RandomVariable get_randomVariable(){
		return (RandomVariable)elements.get(34).data;
	}
	public RandomVariable new_randomVariable(){
		return new RandomVariable();
	}

	public SignalingMessageEncryptionKey get_signalingMessageEncryptionKey(){
		return (SignalingMessageEncryptionKey)elements.get(35).data;
	}
	public SignalingMessageEncryptionKey new_signalingMessageEncryptionKey(){
		return new SignalingMessageEncryptionKey();
	}

	public StationClassMark get_stationClassMark(){
		return (StationClassMark)elements.get(36).data;
	}
	public StationClassMark new_stationClassMark(){
		return new StationClassMark();
	}

	public SystemOperatorCode get_systemOperatorCode(){
		return (SystemOperatorCode)elements.get(37).data;
	}
	public SystemOperatorCode new_systemOperatorCode(){
		return new SystemOperatorCode();
	}

	public TargetCellID get_targetCellID(){
		return (TargetCellID)elements.get(38).data;
	}
	public TargetCellID new_targetCellID(){
		return new TargetCellID();
	}

	public TDMABandwidth get_tdmaBandwidth(){
		return (TDMABandwidth)elements.get(39).data;
	}
	public TDMABandwidth new_tdmaBandwidth(){
		return new TDMABandwidth();
	}

	public TDMABurstIndicator get_tdmaBurstIndicator(){
		return (TDMABurstIndicator)elements.get(40).data;
	}
	public TDMABurstIndicator new_tdmaBurstIndicator(){
		return new TDMABurstIndicator();
	}

	public TDMACallMode get_tdmaCallMode(){
		return (TDMACallMode)elements.get(41).data;
	}
	public TDMACallMode new_tdmaCallMode(){
		return new TDMACallMode();
	}

	public TDMAChannelData get_tdmaChannelData(){
		return (TDMAChannelData)elements.get(42).data;
	}
	public TDMAChannelData new_tdmaChannelData(){
		return new TDMAChannelData();
	}

	public TDMAServiceCode get_tdmaServiceCode(){
		return (TDMAServiceCode)elements.get(43).data;
	}
	public TDMAServiceCode new_tdmaServiceCode(){
		return new TDMAServiceCode();
	}

	public TDMATerminalCapability get_tdmaTerminalCapability(){
		return (TDMATerminalCapability)elements.get(44).data;
	}
	public TDMATerminalCapability new_tdmaTerminalCapability(){
		return new TDMATerminalCapability();
	}

	public TDMAVoiceCoder get_tdmaVoiceCoder(){
		return (TDMAVoiceCoder)elements.get(45).data;
	}
	public TDMAVoiceCoder new_tdmaVoiceCoder(){
		return new TDMAVoiceCoder();
	}

	public TerminalType get_terminalType(){
		return (TerminalType)elements.get(46).data;
	}
	public TerminalType new_terminalType(){
		return new TerminalType();
	}

	public VoicePrivacyMask get_voicePrivacyMask(){
		return (VoicePrivacyMask)elements.get(47).data;
	}
	public VoicePrivacyMask new_voicePrivacyMask(){
		return new VoicePrivacyMask();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_billingID(); }});
		elements.add(new ElementDescriptor(6, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_interMSCCircuitID(); }});
		elements.add(new ElementDescriptor(8, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_mobileIdentificationNumber(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_servingCellID(); }});
		elements.add(new ElementDescriptor(197, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_baseStationManufacturerCode(); }});
		elements.add(new ElementDescriptor(356, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdma2000HandoffInvokeIOSData(); }});
		elements.add(new ElementDescriptor(172, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaBandClassList(); }});
		elements.add(new ElementDescriptor(62, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaCallMode(); }});
		elements.add(new ElementDescriptor(63, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaChannelData(); }});
		elements.add(new ElementDescriptor(212, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaConnectionReferenceList(); }});
		elements.add(new ElementDescriptor(351, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMSMeasuredChannelIdentity(); }});
		elements.add(new ElementDescriptor(66, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaMobileProtocolRevision(); }});
		elements.add(new ElementDescriptor(174, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceConfigurationRecord(); }});
		elements.add(new ElementDescriptor(176, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServiceOptionList(); }});
		elements.add(new ElementDescriptor(67, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaPrivateLongCodeMask(); }});
		elements.add(new ElementDescriptor(60, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaServingOneWayDelay(); }});
		elements.add(new ElementDescriptor(59, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark(); }});
		elements.add(new ElementDescriptor(177, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaStationClassMark2(); }});
		elements.add(new ElementDescriptor(136, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMAHOList(); }});
		elements.add(new ElementDescriptor(134, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_cdmaTargetMeasurementList(); }});
		elements.add(new ElementDescriptor(5, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_channelData(); }});
		elements.add(new ElementDescriptor(39, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_confidentialityModes(); }});
		elements.add(new ElementDescriptor(215, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataKey(); }});
		elements.add(new ElementDescriptor(216, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_dataPrivacyParameters(); }});
		elements.add(new ElementDescriptor(30, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_handoffReason(); }});
		elements.add(new ElementDescriptor(164, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_handoffState(); }});
		elements.add(new ElementDescriptor(7, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_interSwitchCount(); }});
		elements.add(new ElementDescriptor(217, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_ilspInformation(); }});
		elements.add(new ElementDescriptor(70, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_msLocation(); }});
		elements.add(new ElementDescriptor(165, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsCallMode(); }});
		elements.add(new ElementDescriptor(74, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_nampsChannelData(); }});
		elements.add(new ElementDescriptor(349, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pdsnAddress(); }});
		elements.add(new ElementDescriptor(350, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_pdsnProtocolType(); }});
		elements.add(new ElementDescriptor(348, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_qosPriority(); }});
		elements.add(new ElementDescriptor(40, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_randomVariable(); }});
		elements.add(new ElementDescriptor(45, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_signalingMessageEncryptionKey(); }});
		elements.add(new ElementDescriptor(18, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_stationClassMark(); }});
		elements.add(new ElementDescriptor(206, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_systemOperatorCode(); }});
		elements.add(new ElementDescriptor(3, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_targetCellID(); }});
		elements.add(new ElementDescriptor(220, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBandwidth(); }});
		elements.add(new ElementDescriptor(31, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaBurstIndicator(); }});
		elements.add(new ElementDescriptor(29, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaCallMode(); }});
		elements.add(new ElementDescriptor(28, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaChannelData(); }});
		elements.add(new ElementDescriptor(178, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaServiceCode(); }});
		elements.add(new ElementDescriptor(179, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaTerminalCapability(); }});
		elements.add(new ElementDescriptor(180, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_tdmaVoiceCoder(); }});
		elements.add(new ElementDescriptor(47, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_terminalType(); }});
		elements.add(new ElementDescriptor(48, ASNTagClass.CONTEXT_SPECIFIC, true, false){public void set(){ data = new_voicePrivacyMask(); }});
	}
	public HandoffBack2(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 18;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
		asn_class = ASNTagClass.PRIVATE;
	}
}
