package net.asn1.h248;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class IndAudStatisticsDescriptor extends SEQUENCE{

	public PkgdName get_statName(){
		return (PkgdName)elements.get(0).data;
	}
	public PkgdName new_statName(){
		return new PkgdName();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_statName(); }});
	}
	public IndAudStatisticsDescriptor(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = 16;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
