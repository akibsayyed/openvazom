package net.asn1.gsmmap_ch;

import net.asn1.compiler.*;
import net.asn1.types.*;
import java.util.ArrayList;
public class SubscriberState extends CHOICE{

	public NULL get_assumedIdle(){
		return (NULL)elements.get(0).data;
	}
	public NULL new_assumedIdle(){
		return new NULL();
	}

	public NULL get_camelBusy(){
		return (NULL)elements.get(1).data;
	}
	public NULL new_camelBusy(){
		return new NULL();
	}

	public NotReachableReason get_netDetNotReachable(){
		return (NotReachableReason)elements.get(2).data;
	}
	public NotReachableReason new_netDetNotReachable(){
		return new NotReachableReason();
	}

	public NULL get_notProvidedFromVLR(){
		return (NULL)elements.get(3).data;
	}
	public NULL new_notProvidedFromVLR(){
		return new NULL();
	}

	public void initElements(){
		elements.add(new ElementDescriptor(0, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_assumedIdle(); }});
		elements.add(new ElementDescriptor(1, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_camelBusy(); }});
		elements.add(new ElementDescriptor(10, ASNTagClass.UNIVERSAL, false, false){public void set(){ data = new_netDetNotReachable(); }});
		elements.add(new ElementDescriptor(2, ASNTagClass.CONTEXT_SPECIFIC, false, false){public void set(){ data = new_notProvidedFromVLR(); }});
	}
	public SubscriberState(){
		super();
		asn_pc = ASNTagComplexity.Constructed;
		tag = -3;
		elements = new ArrayList<ElementDescriptor>();
		initElements();
	}
}
